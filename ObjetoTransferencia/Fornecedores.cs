﻿using System;

namespace ObjetoTransferencia
{
    public class Fornecedores
    {
        public int            idFornecedor	                { get; set; }
        public string         nomeFornecedor	            { get; set; }
        public string         cnpjFornecedor	            { get; set; }
        public string         cpfFornecedor                 { get; set; }
        public string         inscricaoEstadualFornecedor   { get; set; }
        public string         telefoneFornecedor	        { get; set; }
        public string         faxFornecedor	                { get; set; }
        public string         enderecoFornecedor	        { get; set; }
        public string         complementoFornecedor	        { get; set; }
        public string         bairroFornecedor	            { get; set; }
        public string         cepFornecedor	                { get; set; }
        public string         municipioFornecedor	        { get; set; }
        public string         ufFornecedor	                { get; set; }
        public string         emailFornecedor	            { get; set; }
        public string         contatoFornecedor	            { get; set; }
        public DateTime       dataCadastroFornecedor	    { get; set; }
        public string         pontoReferenciaFornecedor     { get; set; }
        public int            usuarioId	                    { get; set; }
    }
}
