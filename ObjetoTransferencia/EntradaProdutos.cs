﻿using System;

namespace ObjetoTransferencia
{
    public class EntradaProdutos
    {
        public int      idEntradaProduto                { get; set; }
        public int      codigoProduto                   { get; set; }
        public string   motivoEntradaProduto            { get; set; }
        public string   descricaoProduto                { get; set; }
        public decimal  valorCompraProduto              { get; set; }
        public decimal  valorVendaProduto               { get; set; }
        public decimal  estoqueProduto                  { get; set; }
        public decimal  quantidadeProdutoEntrada         { get; set; }
        public string   nfEntradaProduto                { get; set; }
        public DateTime dataCadEntradaProduto           { get; set; }
        public int      usuarioId                       { get; set; }        
    }
}
