﻿using System;

namespace ObjetoTransferencia
{
    public class VendaCabecalho : TipoPagamento
    {
        public int idVendaCabecalho { get; set; }
        public int idCliente	{ get; set; }
        public int idCartao { get; set; }
        public int idTipoPagamento	{ get; set; }
        public int idAtendimentoTipo { get; set; }
        public int numeroPessoas { get; set; }
        public string nomeAtendimento { get; set; }
        public int numeroAtendimento { get; set; }
        public DateTime dataVendaCabecalho	{ get; set; }
        public DateTime horaVendaCabecalho { get; set; }
        public decimal valorVendaCabecalho	{ get; set; }
        public decimal descontoVendaCabecalho	{ get; set; }
        public decimal totalVendaCabecalho	{ get; set; }
        public int statusVendaCabecalho	{ get; set; }
        public string descricaoVCTipoPagamento { get; set; }
        public decimal valorVCTipoPagamento { get; set; }
        public int usuarioId { get; set; }

        public string atendenteVendaCabecalho { get; set; }
        public string btn { get; set; }
        public string troco { get; set; }
        public string dinheiro { get; set; }
        public string cartao { get; set; }
        public string ticket { get; set; }
    }
}
