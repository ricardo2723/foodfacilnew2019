﻿using System;

namespace ObjetoTransferencia
{
    public class PermissaoAcesso
    {
        public int permissaoAcessoId { get; set; }
        public int nivelAcessoId { get; set; }
        public string buton { get; set; }
        public bool grupo { get; set; }
        public bool ativo { get; set; }
        public bool cadastrar { get; set; }
        public bool alterar { get; set; }
        public bool excluir { get; set; }
        public bool pesquisar { get; set; }
        public bool financeiro { get; set; }
        public bool relatorios { get; set; }
        public bool graficos { get; set; }
    }
}
