﻿using System;

namespace ObjetoTransferencia
{
    public class NivelAcesso
    {
        public int nivelAcessoId { get; set; }
        public string nivelAcessoDescricao { get; set; }
        public DateTime dataCadAcessoNivel { get; set; }
        public int usuarioId { get; set; }
    }
}
