﻿using System;

namespace ObjetoTransferencia
{
    public class VendaCabecalhoDelivery : Clientes
    {
        public int idEntrega { get; set; }
        public int idVendaCabecalho { get; set; }
        public int idCliente	{ get; set; }
        public int idEntregador { get; set; }
        public int idFormaPagamento { get; set; }
        public decimal trocoPara { get; set; }
        public decimal troco { get; set; }
        public decimal valorFrete { get; set; }
        public int statusEntrega { get; set; }
        public DateTime horaPedido { get; set; }
        public DateTime horaSaida { get; set; }
        public DateTime horaVolta { get; set; }
        public string apelidoEntregador { get; set; }
        public string motivoCancelamento { get; set; }
        public string atendente { get; set; }
        public string totaDelivery { get; set; }
    }
}
