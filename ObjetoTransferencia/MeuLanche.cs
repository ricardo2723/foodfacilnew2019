﻿using System;

namespace ObjetoTransferencia
{
    public class MeuLanche
    {
        public int    idMeuLanche	                { get; set; }
        public string nomeMeuLanche	                { get; set; }
        public string cnpjMeuLanche	                { get; set; }
        public string inscricaoEstadualMeuLanche    { get; set; }
        public string telefoneMeuLanche	            { get; set; }
        public string faxMeuLanche	                { get; set; }
        public string enderecoMeuLanche	            { get; set; }
        public string complementoMeuLanche	        { get; set; }
        public string bairroMeuLanche	            { get; set; }
        public string cepMeuLanche	                { get; set; }
        public string municipioMeuLanche	        { get; set; }
        public string ufMeuLanche	                { get; set; }
        public string emailMeuLanche	            { get; set; }
        public string contatoMeuLanche	            { get; set; }
        public string localLogoMeuLanche            { get; set; }
    }
}
