﻿using System;

namespace ObjetoTransferencia
{
    public class UnidadeMedida
    {
        public int undMedidaId { get; set; }
        public string undMedidaDescricao { get; set; }
        public DateTime undMedidaDataCad { get; set; }
        public int usuarioId { get; set; }
    }
}
