﻿using System;

namespace ObjetoTransferencia
{
    public class Retiradas
    {
        public int idRetirada { get; set; }
        public int idUsuario { get; set; }
        public int idCaixaRetirada { get; set; }      
        public decimal valorRetirada { get; set; }
        public string motivoRetirada { get; set; }
        public string administradorRetirada { get; set; }
        public DateTime dataRetirada { get; set; }

        public string nomeLoginUsuario { get; set; }
    }
}
