﻿using System;

namespace ObjetoTransferencia
{
    public class VendaDetalhes : Produtos
    {
        public int idVendaDetalhes { get; set; }
        public int idVendaCabecalhoDetalhes { get; set; }
        public string especialVendaDetalhes { get; set; }
        public string extraVendaDetalhes { get; set; }
        public decimal qtdExtraVendaDetalhes { get; set; }
        public decimal quantidadeVendaDetalhes { get; set; }
        public decimal valorUnitarioVendaDetalhes { get; set; }
        public decimal valorTotalVendaDetalhes { get; set; }
        public DateTime dataEnvioVendaDetalhes { get; set; }
        public int statusVendaDetalhes { get; set; }

        public int itemPedido { get; set; }
        public string atendimentoVendaCabecalho { get; set; }
        public string atendenteVenda { get; set; }
        public string tipoAtendimento { get; set; }
    }
}
