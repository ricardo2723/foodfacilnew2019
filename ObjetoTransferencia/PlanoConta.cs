﻿using System;

namespace ObjetoTransferencia
{
    public class PlanoConta
    {
        public int idPlanoConta { get; set; }
        public int codPlanoConta { get; set; }
        public string descricaoPlanoConta { get; set; }
        public DateTime dataCadPlanoConta { get; set; }
        public int usuarioId { get; set; }
	
    }
}
