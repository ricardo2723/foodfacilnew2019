﻿using System;

namespace ObjetoTransferencia
{
    public class FormaPagamento
    {
        public int formaPagamentoId { get; set; }
        public string formaPagamentoDescricao { get; set; }
        public DateTime formaPagamentoDataCad { get; set; }
        public int usuarioId { get; set; }
    }
}
