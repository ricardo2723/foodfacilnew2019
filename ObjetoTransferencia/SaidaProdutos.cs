﻿using System;

namespace ObjetoTransferencia
{
    public class SaidaProdutos
    {
        public int      idSaidaProduto                  { get; set; }
        public int      codigoProduto                   { get; set; }
        public decimal  quantidadeProdutoSaida          { get; set; }
        public string   motivoSaida                     { get; set; }
        public DateTime dataCadSaida                    { get; set; }
        public decimal  estoqueProduto                  { get; set; }
        public string   descricaoProduto                { get; set; }
        public int      usuarioId                       { get; set; }        
    }
}
