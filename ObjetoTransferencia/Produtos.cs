﻿using System;

namespace ObjetoTransferencia
{
    public class Produtos
    {
        public int      codigoProduto                   { get; set; }
        public int      undMedidaId                     { get; set; }
        public int      idFornecedor                    { get; set; }
        public int      grupoId                         { get; set; }
        public string   descricaoProduto                { get; set; }
        public decimal  valorCompraProduto              { get; set; }
        public decimal  valorVendaProduto               { get; set; }
        public decimal  estoqueProduto                  { get; set; }
        public decimal  estoqueCriticoProduto           { get; set; }
        public DateTime dataCadastroProduto             { get; set; }
        public char     controlaEstoqueProduto          { get; set; }
        public char     manufaturadoProduto             { get; set; }
        public int      usuarioId                       { get; set; }
        public int      codProdutoManufaturado          { get; set; }
        public decimal  quantidadeProdutoManufaturado   { get; set; }
        public string   descricaoUndMedida              { get; set; }
        public string   descricaoGrupo                  { get; set; }
        public string   nomeFornecedor                  { get; set; }
        public char   tipoProduto                     { get; set; }
    }
}
