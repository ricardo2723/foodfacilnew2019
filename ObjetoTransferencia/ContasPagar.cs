﻿using System;

namespace ObjetoTransferencia
{
    public class ContasPagar
    {
        public int idContaPagar { get; set; }
        public int idPlanoConta { get; set; }
        public int codPlanoConta { get; set; }
        public string nomeContaPagar { get; set; }
        public decimal valorContaPagar { get; set; }
        public decimal valorPagoContaPagar { get; set; }
        public DateTime vencimentoContaPagar { get; set; }
        public DateTime pagamentoContaPagar { get; set; }
        public string statusContaPagar { get; set; }
        public DateTime dataCadContaPagar { get; set; }
        public int usuarioId { get; set; }
    }
}
