﻿using System;

namespace ObjetoTransferencia
{
    public class VendaDetalhesItemExtra
    {
        public int idVendasDetalhesItem { get; set; }
        public int IdVendasDetalhesExtra { get; set; }
        public int idVendaCabecalho { get; set; }
    }
}
