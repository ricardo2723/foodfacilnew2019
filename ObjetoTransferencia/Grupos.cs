﻿using System;

namespace ObjetoTransferencia
{
    public class Grupos
    {
        public int grupoId { get; set; }
        public string grupoDescricao { get; set; }
        public DateTime grupoDataCad { get; set; }
        public int usuarioId { get; set; }
    }
}
