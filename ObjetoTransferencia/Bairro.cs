﻿using System;

namespace ObjetoTransferencia
{
    public class Bairro
    {
        public int idBairro { get; set; }
        public string nomeBairro { get; set; }
        public decimal taxaBairro { get; set; }
        public DateTime dataCadBairro { get; set; }
        public int usuarioId { get; set; }
    }
}
