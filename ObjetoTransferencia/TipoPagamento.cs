﻿using System;

namespace ObjetoTransferencia
{
    public class TipoPagamento
    {
        public int idTipoPagamento { get; set; }
        public int idCaixaPagamento { get; set; }
        public string descricaoTipoPagamento { get; set; }
        public DateTime dataCadTipoPagamento { get; set; }
        public int usuarioId { get; set; }
    }
}
