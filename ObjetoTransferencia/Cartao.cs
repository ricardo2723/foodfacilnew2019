﻿using System;

namespace ObjetoTransferencia
{
    public class Cartao
    {
        public int idCartao { get; set; }
        public string nomeCartao { get; set; }
        public decimal taxaCartao { get; set; }
        public DateTime dataCadCartao { get; set; }
        public int usuarioId { get; set; }
    }
}
