﻿using System;

namespace ObjetoTransferencia
{
    public class Entregador
    {
        public int idEntregador { get; set; }
        public string nomeEntregador { get; set; }
        public string apelidoEntregador { get; set; }
        public string cpfEntregador { get; set; }
        public bool statusEntregador { get; set; }
        public string identidadeEntregador { get; set; }
        public string telefoneEntregador { get; set; }
        public string celularEntregador { get; set; }
        public string cepEntregador { get; set; }
        public string enderecoEntregador { get; set; }
        public string complementoEntregador { get; set; }
        public string bairroEntregador { get; set; }
        public string municipioEntregador { get; set; }
        public string ufEntregador { get; set; }
        public string emailEntregador { get; set; }
        public DateTime dataNascimentoEntregador { get; set; }
        public decimal porcentEntregador { get; set; }
        public DateTime dataCadastroEntregador { get; set; }
        public int usuarioId { get; set; }

    }
}
