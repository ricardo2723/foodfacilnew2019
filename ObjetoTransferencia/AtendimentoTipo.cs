﻿using System;

namespace ObjetoTransferencia
{
    public class AtendimentoTipo
    {
        public int idAtendimento { get; set; }
        public string atendimentoSigla { get; set; }
        public string nomeAtendimento { get; set; }
        public string buton { get; set; }
    }
}
