﻿using System;
using System.ComponentModel;

namespace ObjetoTransferencia
{
    public class Clientes
    {
        public int idCliente { get; set; }
        public string nomeCliente { get; set; }
        public string cpfCliente { get; set; }
        public string cnpjCliente { get; set; }
        public string identidadeCliente { get; set; }
        
        [DefaultValue("0000000000")]
        public string telefoneCliente { get; set; }

        [DefaultValue("0000000-0000")]
        public string celularCliente { get; set; }

        [DefaultValue("69000-000")]
        public string cepCliente { get; set; }
        public string enderecoCliente { get; set; }
        public string complementoCliente { get; set; }
        public string bairroCliente { get; set; }
        public string municipioCliente { get; set; }
        public string ufCliente { get; set; }
        public string emailCliente { get; set; }
        public DateTime dataNascimentoCliente { get; set; }
        public string pontoReferenciaCliente { get; set; }
        public DateTime dataCadastroCliente { get; set; }
        public int usuarioId { get; set; }

    }
}
