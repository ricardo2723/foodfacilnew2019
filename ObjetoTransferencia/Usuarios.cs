﻿using System;

namespace ObjetoTransferencia
{
    public class Usuarios
    {
        public int usuarioId { get; set; }
        public string usuarioNome { get; set; }
        public string usuarioLogin { get; set; }
        public string usuarioSenha { get; set; }
        public string usuarioCpf { get; set; }
        public int nivelAcessoId { get; set; }
        public DateTime usuarioDataCad { get; set; }
        public string nivelAcessoDescricao { get; set; }
        public int usuarioCadUsuarioId { get; set; }
    }
}
