﻿using System;

namespace ObjetoTransferencia
{
    public class Caixa
    {
        public int idCaixa { get; set; }
        public decimal valorCaixaAbre { get; set; }
        public decimal valorCaixaFecha { get; set; }
        public DateTime dataCaixa { get; set; }
        public DateTime horaCaixaAbre { get; set; }
        public DateTime horaCaixaFecha { get; set; }
        public int statusCaixa { get; set; }
        public string nomeUsuario { get; set; }
        public int usuarioId { get; set; }
        public decimal valorRetirada { get; set; }

        public decimal dinheiro { get; set; }
        public decimal desconto { get; set; }
        public decimal cartao { get; set; }
        public decimal ticket { get; set; }
        public decimal totalCaixa { get; set; }
        public decimal abertoCom { get; set; }
        public string descricaoCaixa { get; set; }
        public decimal totalDesconto { get; set; }
    }
}
