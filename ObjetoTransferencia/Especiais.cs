﻿using System;

namespace ObjetoTransferencia
{
    public class Especiais : Grupos
    {
        public int especiaisId { get; set; }
        public string especiaisDescricao { get; set; }
	    public DateTime especiaisDataCad { get; set; }
	    public int usuarioId { get; set; }
    }
}
