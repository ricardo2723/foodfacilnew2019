﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace AcessoBancoDados
{
    public class BackupDB
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public int IniciaBackup(string localDb)
        {
            return acessoDadosSqlServer.ExecutaBackup(localDb);
        }
    }
}
