﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data;
using System.Data.SqlClient;

using AcessoBancoDados.Properties;

namespace AcessoBancoDados
{
    public class AcessoDadosSqlServer
    {
        //Criar conexão
        private SqlConnection criarConexao()
        {
            return new SqlConnection(Settings.Default.stringConexao);
        }

        //Parametros que vão para o banco
        private SqlParameterCollection sqlParameterCollection = new SqlCommand().Parameters;

        public void limparParametros()
        {
            sqlParameterCollection.Clear();
        }

        public void AdicionarParametros(string nomeParametro, object valorParametro)
        {
            sqlParameterCollection.Add(new SqlParameter(nomeParametro, valorParametro));
        }

        public object ExecutaManipulação(CommandType commandType, string procedureOuTextoSql)
        {
            try 
	        {
	            //Criar Conexão
            SqlConnection sqlConnection = criarConexao();
            //Abrir Conexão
            sqlConnection.Open();
            //Criar o Commando que vai levar Informações para o Banco
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            //Colocando as coisas dentro do commando(Informações que vão trafegar na Conexão
            sqlCommand.CommandType = commandType;
            sqlCommand.CommandText = procedureOuTextoSql;
            sqlCommand.CommandTimeout = 7200; // Em segundos
                
            //Adicionar os paramentros no comando
            foreach (SqlParameter sqlParameter in sqlParameterCollection)
                sqlCommand.Parameters.Add(new SqlParameter(sqlParameter.ParameterName, sqlParameter.Value));

            //Executar o comando ir até o banco
            return sqlCommand.ExecuteScalar();		
	        }
	        catch (Exception ex)
	        {
		        throw new Exception(ex.Message);
	        }      
            
        }

        public int ExecutaBackup(string procedureOuTextoSql)
        {
            try
            {
                //Criar Conexão
                SqlConnection sqlConnection = criarConexao();
                //Abrir Conexão
                sqlConnection.Open();
                //Criar o Commando que vai levar Informações para o Banco
                SqlCommand sqlCommand;

                sqlCommand = new SqlCommand(procedureOuTextoSql, sqlConnection);

                sqlCommand.ExecuteNonQuery();

                return 0;
                
            }
            catch (Exception ex)
            {
                return 1;
            }

        }
        //consultar Registros no Banco de Dados
        public DataTable ExecutarConsulta(CommandType commandType, string procedureOuTextoSql)
        {
            try 
	        {
	            //Criar Conexão
                SqlConnection sqlConnection = criarConexao();
                //Abrir Conexão
                sqlConnection.Open();
                //Criar o Commando que vai levar Informações para o Banco
                SqlCommand sqlCommand = sqlConnection.CreateCommand();
                //Colocando as coisas dentro do commando(Informações que vão trafegar na Conexão
                sqlCommand.CommandType = commandType;
                sqlCommand.CommandText = procedureOuTextoSql;
                sqlCommand.CommandTimeout = 7200; // Em segundos
                
                //Adicionar os paramentros no comando
                foreach (SqlParameter sqlParameter in sqlParameterCollection)
                    sqlCommand.Parameters.Add(new SqlParameter(sqlParameter.ParameterName, sqlParameter.Value));

                //Criar Adaptador
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand); 
                
                //Criar Data Table e colocar dados do Banco
                DataTable dataTable = new DataTable();

                //Mandar o commando ir ate o banco buscar os dados e o adaptador preencher o dataTable
                sqlDataAdapter.Fill(dataTable);

                return dataTable;
            }
	        catch (Exception ex)
	        {
		        throw new Exception(ex.Message);
	        }      
        }

        public SqlDataReader ExecutarConsultaReader(CommandType commandType, string procedureOuTextoSql)
        {
            try
            {
                //Criar Conexão
                SqlConnection sqlConnection = criarConexao();
                //Abrir Conexão
                sqlConnection.Open();
                //Criar o Commando que vai levar Informações para o Banco
                SqlCommand sqlCommand = sqlConnection.CreateCommand();
                //Colocando as coisas dentro do commando(Informações que vão trafegar na Conexão
                sqlCommand.CommandType = commandType;
                sqlCommand.CommandText = procedureOuTextoSql;
                sqlCommand.CommandTimeout = 7200; // Em segundos

                //Adicionar os paramentros no comando
                foreach (SqlParameter sqlParameter in sqlParameterCollection)
                    sqlCommand.Parameters.Add(new SqlParameter(sqlParameter.ParameterName, sqlParameter.Value));

                
                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                
                return dataReader;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public object ExecutaConsultaEscalar(CommandType commandType, string procedureOuTextoSql)
        {
            try
            {
                //Criar Conexão
                SqlConnection sqlConnection = criarConexao();
                //Abrir Conexão
                sqlConnection.Open();
                //Criar o Commando que vai levar Informações para o Banco
                SqlCommand sqlCommand = sqlConnection.CreateCommand();
                //Colocando as coisas dentro do commando(Informações que vão trafegar na Conexão
                sqlCommand.CommandType = commandType;
                sqlCommand.CommandText = procedureOuTextoSql;
                sqlCommand.CommandTimeout = 7200; // Em segundos

                //Adicionar os paramentros no comando
                foreach (SqlParameter sqlParameter in sqlParameterCollection)
                    sqlCommand.Parameters.Add(new SqlParameter(sqlParameter.ParameterName, sqlParameter.Value));

                //Executar o comando ir até o banco
                return sqlCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
