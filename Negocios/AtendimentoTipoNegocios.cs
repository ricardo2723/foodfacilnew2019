﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class AtendimentoTipoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(AtendimentoTipo atentimentoTipo)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                
                acessoDadosSqlServer.AdicionarParametros("@atendimento", atentimentoTipo.atendimentoSigla);
                acessoDadosSqlServer.AdicionarParametros("@nomeAtendimento", atentimentoTipo.nomeAtendimento);
                acessoDadosSqlServer.AdicionarParametros("@buton", atentimentoTipo.buton);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", 5);

                string idAtendimento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspAtendimentoTipoCrudInserir").ToString();

                return idAtendimento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(AtendimentoTipo atentimentoTipo)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idAtendimento", atentimentoTipo.idAtendimento);
                acessoDadosSqlServer.AdicionarParametros("@atendimento", atentimentoTipo.atendimentoSigla);
                acessoDadosSqlServer.AdicionarParametros("@nomeAtendimento", atentimentoTipo.nomeAtendimento);
                acessoDadosSqlServer.AdicionarParametros("@buton", atentimentoTipo.buton);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", 5);

                string idAtendimentoTipo = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspAtendimentoTipoCrudAlterar").ToString();

                return idAtendimentoTipo;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(AtendimentoTipo atendimentoTipo)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idAtendimento", atendimentoTipo.idAtendimento);
                string idAtendimentoTipo = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspAtendimentoTipoCrudExcluir").ToString();

                return idAtendimentoTipo;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public AtendimentoTipoCollections GetTipoAtendimento(string descr)
        {
            try
            {
                AtendimentoTipoCollections atendimentoTipoCollections = new AtendimentoTipoCollections();

                acessoDadosSqlServer.limparParametros();
                
                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from AtendimentoTipo where nomeAtendimento like '%" +  descr + "%'");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    AtendimentoTipo atendimentoTipo = new AtendimentoTipo();

                    atendimentoTipo.idAtendimento = Convert.ToInt32(linha["idAtendimento"]);
                    atendimentoTipo.nomeAtendimento = linha["nomeAtendimento"].ToString().ToUpper();
                    atendimentoTipo.atendimentoSigla = linha["atendimento"].ToString();

                    atendimentoTipoCollections.Add(atendimentoTipo); 
                }

                return atendimentoTipoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cartão. Detalhes: " + ex.Message);
            }
        }        
        
    }
}
