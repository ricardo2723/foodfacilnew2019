﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class UsuariosNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Usuarios usuarios)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@usuarioNome", usuarios.usuarioNome);
                acessoDadosSqlServer.AdicionarParametros("@usuarioLogin", usuarios.usuarioLogin);
                acessoDadosSqlServer.AdicionarParametros("@usuarioSenha", usuarios.usuarioSenha);
                acessoDadosSqlServer.AdicionarParametros("@usuarioCpf", usuarios.usuarioCpf);
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoId", usuarios.nivelAcessoId);
                acessoDadosSqlServer.AdicionarParametros("@usuarioDataCad", usuarios.usuarioDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioCadUsuarioId", usuarios.usuarioCadUsuarioId);

                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspUsuariosCrudInserir").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Usuarios usuarios)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", usuarios.usuarioId);
                acessoDadosSqlServer.AdicionarParametros("@usuarioNome", usuarios.usuarioNome);
                acessoDadosSqlServer.AdicionarParametros("@usuarioLogin", usuarios.usuarioLogin);
                acessoDadosSqlServer.AdicionarParametros("@usuarioSenha", usuarios.usuarioSenha);
                acessoDadosSqlServer.AdicionarParametros("@usuarioCpf", usuarios.usuarioCpf);
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoId", usuarios.nivelAcessoId);
                acessoDadosSqlServer.AdicionarParametros("@usuarioDataCad", usuarios.usuarioDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioCadUsuarioId", usuarios.usuarioCadUsuarioId);

                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspUsuariosCrudAlterar").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Usuarios usuarios)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", usuarios.usuarioId);
                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspUsuariosCrudExcluir").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public UsuariosCollections ConsultarPorNome(string nome)
        {
            try
            {
                UsuariosCollections usuariosCollections = new UsuariosCollections();

                acessoDadosSqlServer.limparParametros();
                
                DataTable dataTableUsuarios =  acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_BuscaUsuariosNome where usuarioNome Like '%" + nome + "%'");

                foreach (DataRow linha in dataTableUsuarios.Rows)
                {
                    Usuarios usuarios = new Usuarios();
                    usuarios.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    usuarios.usuarioNome = linha["usuarioNome"].ToString().ToUpper();
                    usuarios.usuarioLogin = linha["usuarioLogin"].ToString().ToUpper();
                    usuarios.usuarioSenha = linha["usuarioSenha"].ToString().ToUpper();
                    usuarios.usuarioCpf = linha["usuarioCpf"].ToString().ToUpper();
                    usuarios.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    usuarios.nivelAcessoDescricao = linha["nivelAcessoDescricao"].ToString().ToUpper();

                    usuariosCollections.Add(usuarios); 
                }

                return usuariosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por NOME. Detalhes: " + ex.Message);
            }
        }

        public UsuariosCollections ConsultarPorCpf(string cpf)
        {
            try
            {
                UsuariosCollections usuariosCollections = new UsuariosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@usuarioCpf", cpf);

                DataTable dataTableUsuarios = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspUsuarioListaCPF");

                foreach (DataRow linha in dataTableUsuarios.Rows)
                {
                    Usuarios usuarios = new Usuarios();
                    usuarios.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    usuarios.usuarioNome = linha["usuarioNome"].ToString().ToUpper();
                    usuarios.usuarioLogin = linha["usuarioLogin"].ToString().ToUpper();
                    usuarios.usuarioSenha = linha["usuarioSenha"].ToString().ToUpper();
                    usuarios.usuarioCpf = linha["usuarioCpf"].ToString();
                    usuarios.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    usuarios.nivelAcessoDescricao = linha["nivelAcessoDescricao"].ToString().ToUpper();

                    usuariosCollections.Add(usuarios);
                }

                return usuariosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por CPF. Detalhes: " + ex.Message);
            }
        }

        public UsuariosCollections ConsultarPorLogin(string Login)
        {
            try
            {
                UsuariosCollections usuariosCollections = new UsuariosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@usuarioLogin", Login);

                DataTable dataTableUsuarios = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspUsuarioListaLogin");

                foreach (DataRow linha in dataTableUsuarios.Rows)
                {
                    Usuarios usuarios = new Usuarios();
                    usuarios.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    usuarios.usuarioNome = linha["usuarioNome"].ToString().ToUpper();
                    usuarios.usuarioLogin = linha["usuarioLogin"].ToString().ToUpper();
                    usuarios.usuarioSenha = linha["usuarioSenha"].ToString().ToUpper();
                    usuarios.usuarioCpf = linha["usuarioCpf"].ToString();
                    usuarios.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    usuarios.nivelAcessoDescricao = linha["nivelAcessoDescricao"].ToString().ToUpper();

                    usuariosCollections.Add(usuarios);
                }

                return usuariosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Usuário por Login. Detalhes: " + ex.Message);
            }
        }

        public UsuariosCollections ChecaLogin(Usuarios LoginSenha)
        {
            try
            {
                UsuariosCollections usuariosCollections = new UsuariosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@usuarioLogin", LoginSenha.usuarioLogin);
                acessoDadosSqlServer.AdicionarParametros("@usuarioSenha", LoginSenha.usuarioSenha);

                DataTable dataTableUsuarios = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspUsuarioChecaLogin");

                foreach (DataRow linha in dataTableUsuarios.Rows)
                {
                    Usuarios usuarios = new Usuarios();
                    usuarios.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    usuarios.usuarioNome = linha["usuarioNome"].ToString().ToUpper();
                    usuarios.usuarioLogin = linha["usuarioLogin"].ToString().ToUpper();
                    usuarios.usuarioSenha = linha["usuarioSenha"].ToString().ToUpper();
                    usuarios.usuarioCpf = linha["usuarioCpf"].ToString();
                    usuarios.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    usuarios.nivelAcessoDescricao = linha["nivelAcessoDescricao"].ToString().ToUpper();

                    usuariosCollections.Add(usuarios);
                }

                return usuariosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Usuário por Login. Detalhes: " + ex.Message);
            }
        }

        public UsuariosCollections getUsuariosAdm()
        {
            try
            {
                UsuariosCollections usuariosCollections = new UsuariosCollections();

                DataTable dataTableUsuarios = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "Select usuarioLogin, usuarioSenha From Usuarios where nivelAcessoId = 1");

                foreach (DataRow linha in dataTableUsuarios.Rows)
                {
                    Usuarios usuarios = new Usuarios();
                    usuarios.usuarioLogin = linha["usuarioLogin"].ToString().ToUpper();
                    usuarios.usuarioSenha = linha["usuarioSenha"].ToString().ToUpper();
                    
                    usuariosCollections.Add(usuarios);
                }

                return usuariosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Usuário. Detalhes: " + ex.Message);
            }
        }
    }
}
