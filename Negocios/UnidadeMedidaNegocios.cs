﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class UnidadeMedidaNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(UnidadeMedida unidadeMedida)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@undMedidaDescricao", unidadeMedida.undMedidaDescricao);
                acessoDadosSqlServer.AdicionarParametros("@undMedidaDataCad", unidadeMedida.undMedidaDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", unidadeMedida.usuarioId);
                string idUnidadeMedida = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspUnidadeMedidaCrudInserir").ToString();

                return idUnidadeMedida;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(UnidadeMedida unidadeMedida)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@undMedidaId", unidadeMedida.undMedidaId);
                acessoDadosSqlServer.AdicionarParametros("@undMedidaDescricao", unidadeMedida.undMedidaDescricao);
                acessoDadosSqlServer.AdicionarParametros("@undMedidaDataCad", unidadeMedida.undMedidaDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", unidadeMedida.usuarioId);
                string idUnidadeMedida = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspUnidadeMedidaCrudAlterar").ToString();

                return idUnidadeMedida;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(UnidadeMedida unidadeMedida)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@undMedidaId", unidadeMedida.undMedidaId);
                string idUnidadeMedida = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspUnidadeMedidaCrudExcluir").ToString();

                return idUnidadeMedida;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public UnidadeMedidaCollections ConsultarDescricao(string descr)
        {
            try
            {
                UnidadeMedidaCollections unidadeMedidaCollections = new UnidadeMedidaCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@undMedidaDescricao", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspUnidadeMedidaListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    UnidadeMedida unidadeMedida = new UnidadeMedida();

                    unidadeMedida.undMedidaId = Convert.ToInt32(linha["undMedidaId"]);
                    unidadeMedida.undMedidaDescricao = linha["undMedidaDescricao"].ToString().ToUpper();
                    unidadeMedida.undMedidaDataCad = Convert.ToDateTime(linha["undMedidaDataCad"]);
                    unidadeMedida.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    unidadeMedidaCollections.Add(unidadeMedida); 
                }

                return unidadeMedidaCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar a Unidade de Pesquisa. Detalhes: " + ex.Message);
            }
        }       
        
    }
}
