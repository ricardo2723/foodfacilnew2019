﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class EspeciaisNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Especiais especiais)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@especiaisDescricao", especiais.especiaisDescricao);
                acessoDadosSqlServer.AdicionarParametros("@especiaisIdGrupo", especiais.grupoId);
                acessoDadosSqlServer.AdicionarParametros("@especiaisDataCad", especiais.especiaisDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", especiais.usuarioId);
                string idEspeciais = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEspeciaisCrudInserir").ToString();

                return idEspeciais;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Especiais especiais)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@especiaisId", especiais.especiaisId);
                acessoDadosSqlServer.AdicionarParametros("@especiaisIdGrupo", especiais.grupoId);
                acessoDadosSqlServer.AdicionarParametros("@especiaisDescricao", especiais.especiaisDescricao);
                acessoDadosSqlServer.AdicionarParametros("@especiaisDataCad", especiais.especiaisDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", especiais.usuarioId);
                string idEspeciais = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEspeciaisCrudAlterar").ToString();

                return idEspeciais;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Especiais especiais)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@especiaisId", especiais.especiaisId);
                string idEspeciais = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEspeciaisCrudExcluir").ToString();

                return idEspeciais;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public EspeciaisCollections ConsultarDescricao(string desc)
        {
            try
            {
                EspeciaisCollections especiaisCollections = new EspeciaisCollections();

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_EspeciaisListaNome where especiaisDescricao like '%" + desc + "%' order by especiaisDescricao ASC");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Especiais especiais = new Especiais();

                    especiais.especiaisId = Convert.ToInt32(linha["especiaisId"]);
                    especiais.especiaisDescricao = linha["especiaisDescricao"].ToString().ToUpper();
                    especiais.especiaisDataCad = Convert.ToDateTime(linha["especiaisDataCad"]);
                    especiais.grupoId = Convert.ToInt32(linha["idGrupo"]);
                    especiais.grupoDescricao = linha["grupoDescricao"].ToString().ToUpper();
                    especiais.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    especiaisCollections.Add(especiais);
                }

                return especiaisCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Especial. Detalhes: " + ex.Message);
            }
        }

        public EspeciaisCollections ConsultarDescricaoAll()
        {
            try
            {
                EspeciaisCollections especiaisCollections = new EspeciaisCollections();

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_EspeciaisListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Especiais especiais = new Especiais();

                    especiais.especiaisId = Convert.ToInt32(linha["especiaisId"]);
                    especiais.especiaisDescricao = linha["especiaisDescricao"].ToString().ToUpper();
                    especiais.especiaisDataCad = Convert.ToDateTime(linha["especiaisDataCad"]);
                    especiais.grupoId = Convert.ToInt32(linha["idGrupo"]);
                    especiais.grupoDescricao = linha["grupoDescricao"].ToString().ToUpper();
                    especiais.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    especiaisCollections.Add(especiais); 
                }

                return especiaisCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Especial. Detalhes: " + ex.Message);
            }
        }

        public EspeciaisCollections BuscarEspeciaisGrupo(int grupo)
        {
            try
            {
                EspeciaisCollections especiaisCollections = new EspeciaisCollections();

                DataTable dataTabelEspeciais = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_EspeciaisListaNome where grupoId = " + grupo + "order by especiaisDescricao ASC ");

                foreach (DataRow linha in dataTabelEspeciais.Rows)
                {
                    Especiais especiais = new Especiais();

                    especiais.especiaisId = Convert.ToInt32(linha["especiaisId"]);
                    especiais.especiaisDescricao = linha["especiaisDescricao"].ToString().ToUpper();
                    especiais.especiaisDataCad = Convert.ToDateTime(linha["especiaisDataCad"]);
                    especiais.grupoId = Convert.ToInt32(linha["idGrupo"]);
                    especiais.grupoDescricao = linha["grupoDescricao"].ToString().ToUpper();
                    especiais.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    especiaisCollections.Add(especiais);

                }

                return especiaisCollections;
            }
            catch (Exception e)
            {
                throw new Exception("Não foi possivel Consultar o Especial. Detalhes: " + e.Message);
            }
        }
    }
}
