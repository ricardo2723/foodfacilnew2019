﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace Negocios
{
    public class EntradaProdutosNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(EntradaProdutos entradaProdutos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", entradaProdutos.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@quantidadeProdutoEntrada", entradaProdutos.quantidadeProdutoEntrada);
                acessoDadosSqlServer.AdicionarParametros("@motivoEntradaProduto", entradaProdutos.motivoEntradaProduto);
                acessoDadosSqlServer.AdicionarParametros("@nfEntradaProduto", entradaProdutos.nfEntradaProduto);
                acessoDadosSqlServer.AdicionarParametros("@dataCadEntradaProduto", entradaProdutos.dataCadEntradaProduto);
                
                acessoDadosSqlServer.AdicionarParametros("@valorCompraProduto", entradaProdutos.valorCompraProduto);
                acessoDadosSqlServer.AdicionarParametros("@valorVendaProduto", entradaProdutos.valorVendaProduto);
                acessoDadosSqlServer.AdicionarParametros("@estoqueProduto", entradaProdutos.estoqueProduto);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", entradaProdutos.usuarioId);

                string idEntradaProdutos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEntradaProdutosCrudInserir").ToString();

                return idEntradaProdutos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public EntradaProdutosCollections GetProduto(EntradaProdutos entradaProdutos)
        {
            try
            {
                EntradaProdutosCollections entradaProdutosCollections = new EntradaProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", entradaProdutos.codigoProduto);

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.StoredProcedure, "uspEntradaProdutosListaCod");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        EntradaProdutos produto = new EntradaProdutos();

                        produto.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                        produto.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                        produto.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                        produto.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);

                        entradaProdutosCollections.Add(produto);
                    }

                }

                return entradaProdutosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto: " + ex.Message);
            }
        }
        
    }
}
