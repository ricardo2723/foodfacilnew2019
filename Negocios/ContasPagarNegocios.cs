﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class ContasPagarNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(ContasPagar contasPagar)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idPlanoConta", contasPagar.idPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@nomeContaPagar", contasPagar.nomeContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@valorContaPagar", contasPagar.valorContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@vencimentoContaPagar", contasPagar.vencimentoContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@statusContaPagar", checaStatusInserir(contasPagar.statusContaPagar));
                acessoDadosSqlServer.AdicionarParametros("@dataCadContaPagar", contasPagar.dataCadContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", contasPagar.usuarioId);

                string idContaPagar = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspContasPagarCrudInserir").ToString();

                return idContaPagar;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string inserirPagamento(ContasPagar contasPagar)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idContaPagar", contasPagar.idContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@dataPagamentoContaPagas", contasPagar.pagamentoContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@valorPagoContaPagas", contasPagar.valorPagoContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@dataCadContaPagas", contasPagar.dataCadContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", contasPagar.usuarioId);

                string idContaPagar = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspContasPagarCrudInserirPagamento").ToString();

                return idContaPagar;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Alterar(ContasPagar contasPagar)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idContaPagar", contasPagar.idContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@idPlanoConta", contasPagar.idPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@nomeContaPagar", contasPagar.nomeContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@valorContaPagar", contasPagar.valorContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@vencimentoContaPagar", contasPagar.vencimentoContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@statusContaPagar", checaStatusInserir(contasPagar.statusContaPagar));
                acessoDadosSqlServer.AdicionarParametros("@dataCadContaPagar", contasPagar.dataCadContaPagar);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", contasPagar.usuarioId);

                string idContaPagar = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspContasPagarCrudAlterar").ToString();

                return idContaPagar;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(ContasPagar contasPagar)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idContaPagar", contasPagar.idContaPagar);
                string idContaPagar = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspContasPagarCrudExcluir").ToString();

                return idContaPagar;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ContasPagarCollections ConsultarPorTipo(int tipo)
        {
            try
            {
                ContasPagarCollections contasPagarCollections = new ContasPagarCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idPlanoConta", tipo);

                DataTable dataTableContasPagar =  acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspContasPagarListaTipo");

                foreach (DataRow linha in dataTableContasPagar.Rows)
                {
                    ContasPagar contasPagar = new ContasPagar();
                    
                    contasPagar.idContaPagar = Convert.ToInt32(linha["idContaPagar"]);
                    contasPagar.idPlanoConta = Convert.ToInt32(linha["idPlanoConta"]);
                    contasPagar.codPlanoConta = Convert.ToInt32(linha["codPlanoConta"]);
                    contasPagar.nomeContaPagar = linha["nomeContaPagar"].ToString().ToUpper();
                    contasPagar.valorContaPagar = Convert.ToDecimal(linha["valorContaPagar"]);
                    contasPagar.vencimentoContaPagar = Convert.ToDateTime(linha["vencimentoContaPagar"]);
                    contasPagar.statusContaPagar = checaStatusRetorno(Convert.ToBoolean(linha["statusContaPagar"]));
                    contasPagar.dataCadContaPagar = Convert.ToDateTime(linha["dataCadContaPagar"]);
                    contasPagar.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    contasPagarCollections.Add(contasPagar); 
                }

                return contasPagarCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o ContaPagar por NOME. Detalhes: " + ex.Message);
            }
        }

        public ContasPagarCollections ConsultarPorTipoCodigo(int tipo)
        {
            try
            {
                ContasPagarCollections contasPagarCollections = new ContasPagarCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codPlanoConta", tipo);

                DataTable dataTableContasPagar = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspContasPagarListaTipoCodigo");

                foreach (DataRow linha in dataTableContasPagar.Rows)
                {
                    ContasPagar contasPagar = new ContasPagar();

                    contasPagar.idContaPagar = Convert.ToInt32(linha["idContaPagar"]);
                    contasPagar.idPlanoConta = Convert.ToInt32(linha["idPlanoConta"]);
                    contasPagar.codPlanoConta = Convert.ToInt32(linha["codPlanoConta"]);
                    contasPagar.nomeContaPagar = linha["nomeContaPagar"].ToString().ToUpper();
                    contasPagar.valorContaPagar = Convert.ToDecimal(linha["valorContaPagar"]);
                    contasPagar.vencimentoContaPagar = Convert.ToDateTime(linha["vencimentoContaPagar"]);
                    contasPagar.statusContaPagar = checaStatusRetorno(Convert.ToBoolean(linha["statusContaPagar"]));
                    contasPagar.dataCadContaPagar = Convert.ToDateTime(linha["dataCadContaPagar"]);
                    contasPagar.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    contasPagarCollections.Add(contasPagar);
                }

                return contasPagarCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o ContaPagar por NOME. Detalhes: " + ex.Message);
            }
        }

        public ContasPagarCollections ConsultarPorStatus(bool status)
        {
            try
            {
                ContasPagarCollections contasPagarCollections = new ContasPagarCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@statusContaPagar", status);

                DataTable dataTableContasPagar = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspContasPagarListaStatus");

                foreach (DataRow linha in dataTableContasPagar.Rows)
                {
                    ContasPagar contasPagar = new ContasPagar();

                    contasPagar.idContaPagar = Convert.ToInt32(linha["idContaPagar"]);
                    contasPagar.idPlanoConta = Convert.ToInt32(linha["idPlanoConta"]);
                    contasPagar.codPlanoConta = Convert.ToInt32(linha["codPlanoConta"]);
                    contasPagar.nomeContaPagar = linha["nomeContaPagar"].ToString().ToUpper();
                    contasPagar.valorContaPagar = Convert.ToDecimal(linha["valorContaPagar"]);
                    contasPagar.vencimentoContaPagar = Convert.ToDateTime(linha["vencimentoContaPagar"]);
                    contasPagar.statusContaPagar = checaStatusRetorno(Convert.ToBoolean(linha["statusContaPagar"]));
                    contasPagar.dataCadContaPagar = Convert.ToDateTime(linha["dataCadContaPagar"]);
                    contasPagar.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    contasPagarCollections.Add(contasPagar);
                }

                return contasPagarCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Conta a Pagar por Status. Detalhes: " + ex.Message);
            }
        }

        public ContasPagarCollections ConsultarPorData(DateTime data)
        {
            try
            {
                ContasPagarCollections contasPagarCollections = new ContasPagarCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@vencimentoContaPagar", data);

                DataTable dataTableContasPagar = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspContasPagarListaData");

                foreach (DataRow linha in dataTableContasPagar.Rows)
                {
                    ContasPagar contasPagar = new ContasPagar();

                    contasPagar.idContaPagar = Convert.ToInt32(linha["idContaPagar"]);
                    contasPagar.idPlanoConta = Convert.ToInt32(linha["idPlanoConta"]); 
                    contasPagar.codPlanoConta = Convert.ToInt32(linha["codPlanoConta"]);
                    contasPagar.nomeContaPagar = linha["nomeContaPagar"].ToString().ToUpper();
                    contasPagar.valorContaPagar = Convert.ToDecimal(linha["valorContaPagar"]);
                    contasPagar.vencimentoContaPagar = Convert.ToDateTime(linha["vencimentoContaPagar"]);
                    contasPagar.statusContaPagar = checaStatusRetorno(Convert.ToBoolean(linha["statusContaPagar"]));
                    contasPagar.dataCadContaPagar = Convert.ToDateTime(linha["dataCadContaPagar"]);
                    contasPagar.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    contasPagarCollections.Add(contasPagar);
                }

                return contasPagarCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar Contas por Data. Detalhes: " + ex.Message);
            }
        }

        public ContasPagarCollections ConsultarContasPagas(int cod)
        {
            try
            {
                ContasPagarCollections contasPagarCollections = new ContasPagarCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idContaPagar", cod);

                DataTable dataTableContasPagar = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspContasPagarListaContasPagas");

                foreach (DataRow linha in dataTableContasPagar.Rows)
                {
                    ContasPagar contasPagar = new ContasPagar();

                    contasPagar.valorPagoContaPagar = Convert.ToDecimal(linha["valorPagoContaPagas"]);
                    contasPagar.pagamentoContaPagar = Convert.ToDateTime(linha["dataPagamentoContaPagas"]);
                    
                    contasPagarCollections.Add(contasPagar);
                }

                return contasPagarCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar Contas por Data. Detalhes: " + ex.Message);
            }
        }

        private string checaStatusRetorno(bool status)
        {
            string retorno;

            if (status)
            {
                return retorno = "PAGO";
            }
            else
            {
                return retorno = "EM ABERTO";
            }
        }

        private bool checaStatusInserir(string status)
        {
            bool retorno;

            if (status == "0")
            {
                return retorno = false;
            }
            else
            {
                return retorno = true;
            }
        }
    }
}
