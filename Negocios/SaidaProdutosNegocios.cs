﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace Negocios
{
    public class SaidaProdutosNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(SaidaProdutos saidaProdutos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", saidaProdutos.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@quantidadeProdutoSaida", saidaProdutos.quantidadeProdutoSaida);
                acessoDadosSqlServer.AdicionarParametros("@motivoSaida", saidaProdutos.motivoSaida);
                acessoDadosSqlServer.AdicionarParametros("@dataCadSaida", saidaProdutos.dataCadSaida);
                
                acessoDadosSqlServer.AdicionarParametros("@estoqueProduto", saidaProdutos.estoqueProduto);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", saidaProdutos.usuarioId);

                string idSaidaProdutos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspSaidaProdutosCrudInserir").ToString();

                return idSaidaProdutos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public SaidaProdutosCollections GetProduto(SaidaProdutos saidaProdutos)
        {
            try
            {
                SaidaProdutosCollections saidaProdutosCollections = new SaidaProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", saidaProdutos.codigoProduto);

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.StoredProcedure, "uspEntradaProdutosListaCod");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        SaidaProdutos produto = new SaidaProdutos();

                        produto.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                        produto.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);

                        saidaProdutosCollections.Add(produto);
                    }

                }

                return saidaProdutosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto: " + ex.Message);
            }
        }
        
    }
}
