﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Negocios.Validador
{
    public class ValidadorProduto : IDisposable
    {
        private static bool valida = true;

        public static bool ValidatingFieldAll(Control.ControlCollection controls, int id = 0)
        {
            foreach (Control ctrl in controls)
            {
                if (ctrl is TextBox)
                {
                    if (ctrl.Name == "txtNome")
                    {
                        if (String.IsNullOrEmpty(ctrl.Text.Trim()))
                        {
                            valida = FieldValidatedFail(ctrl, "O Nome é Obrigatório");
                            break;
                        }
                        else if (ctrl.Text.Length > 45)
                        {
                            valida = FieldValidatedFail(ctrl, "O Complemento deve ter no Maximo 45 Caracteres");
                            break;
                        }
                        else if (UniqueNameProduct(ctrl.Text, id))
                        {
                            valida = FieldValidatedFail(ctrl, "O Nome já existe no Banco de Dados");
                            break;
                        }
                        else
                        {
                            ctrl.BackColor = System.Drawing.Color.White;
                            valida = true;
                        }

                    }
                    else if (ctrl.Name == "txtCodigo")
                    {
                        int codigo;
                        if (!int.TryParse(ctrl.Text, out codigo))
                        {
                            valida = FieldValidatedFail(ctrl, "Digite apenas Valores Numéricos");
                            break;
                        }
                        else if (String.IsNullOrEmpty(ctrl.Text.Trim()))
                        {
                            valida = FieldValidatedFail(ctrl, "O Codigo é Obrigatório");
                            break;
                        }
                        else if (UniqueCodeProduct(ctrl.Text, id))
                        {
                            valida = FieldValidatedFail(ctrl, "O Codigo já existe no Banco de Dados");
                            break;
                        }
                        else
                        {
                            ctrl.BackColor = System.Drawing.Color.White;
                            valida = true;
                        }
                    }
                    else if (ctrl.Name == "txtPrecoCusto")
                    {
                        decimal precoC;

                        if (!decimal.TryParse(ctrl.Text, out precoC))
                        {
                            valida = FieldValidatedFail(ctrl, "Digite apenas Valores Numéricos");
                            break;
                        }
                        else if (String.IsNullOrEmpty(ctrl.Text.Trim()))
                        {
                            valida = FieldValidatedFail(ctrl, "O Preço de Custo é Obrigatório");
                            break;
                        }
                        else
                        {
                            ctrl.BackColor = System.Drawing.Color.White;
                            valida = true;
                        }
                    }
                    else if (ctrl.Name == "txtPrecoVenda")
                    {
                        decimal precoV;
                        if (!decimal.TryParse(ctrl.Text, out precoV))
                        {
                            valida = FieldValidatedFail(ctrl, "Digite apenas Valores Numéricos");
                            break;
                        }
                        else if (String.IsNullOrEmpty(ctrl.Text.Trim()))
                        {
                            valida = FieldValidatedFail(ctrl, "O Preço de Venda é Obrigatório");
                            break;
                        }
                        else
                        {
                            ctrl.BackColor = System.Drawing.Color.White;
                            valida = true;
                        }
                    }
                }
            }
            return valida;
        }

        public static bool ValidaITemPedido(Control.ControlCollection controls)
        {
            foreach (Control ctrl in controls)
            {
                if (ctrl is TextBox)
                {
                    if (ctrl.Name == "txtCodProduto")
                    {
                        int codigoProduto;
                        if (!int.TryParse(ctrl.Text, out codigoProduto))
                        {
                            valida = FieldValidatedFail(ctrl, "Digite apenas Valores Numéricos");
                            break;
                        }
                        else if (String.IsNullOrEmpty(ctrl.Text.Trim()))
                        {
                            valida = FieldValidatedFail(ctrl, "Digite o Código do Produto");
                            break;
                        }                        
                        else
                        {
                            ctrl.BackColor = Color.White;
                            valida = true;
                        }
                    }else if (ctrl.Name == "txtQtdProduto")
                    {
                        int qtdProduto;
                        if (!int.TryParse(ctrl.Text, out qtdProduto))
                        {
                            valida = FieldValidatedFail(ctrl, "Digite apenas Valores Numéricos");
                            break;
                        }
                        else if (String.IsNullOrEmpty(ctrl.Text.Trim()))
                        {
                            valida = FieldValidatedFail(ctrl, "Quantidade é Obrigatório");
                            break;
                        }                        
                        else
                        {
                            ctrl.BackColor = Color.White;
                            valida = true;
                        }
                    }
                }
            }
            return valida;
        }

        private static bool FieldValidatedFail(Control ctrl, string message)
        {
            ctrl.BackColor = Color.LightGoldenrodYellow;
            ctrl.Focus();
            MessageBox.Show(message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return false;
        }

        public static bool UniqueNameProduct(string texto, int id)
        {
            var pb = new ProdutosNegocios();
            if (id > 0)
            {
                //return pb.GetUniqueNameUpdate(texto, id);
            }
            else
            {
                //return pb.GetUniqueNameInsert(texto);
            }

            return true;
        }

        public static bool UniqueCodeProduct(string texto, int id)
        {
            var pb = new ProdutosNegocios();
            if (id > 0)
            {
                //return pb.GetUniqueCodeUpdate(texto, id);
            }
            else
            {
                //return pb.GetUniqueCodeInsert(texto);
            }

            return true;
        }


        public void Dispose()
        {
            Dispose();
        }
    }
}
