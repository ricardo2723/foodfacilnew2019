﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class MeuLancheNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string Alterar(MeuLanche meuLanche)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idMeuLanche", meuLanche.idMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@nomeMeuLanche", meuLanche.nomeMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@cnpjMeuLanche", meuLanche.cnpjMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@inscricaoEstadualMeuLanche", meuLanche.inscricaoEstadualMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@telefoneMeuLanche", meuLanche.telefoneMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@faxMeuLanche", meuLanche.faxMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@enderecoMeuLanche", meuLanche.enderecoMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@complementoMeuLanche", meuLanche.complementoMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@bairroMeuLanche", meuLanche.bairroMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@cepMeuLanche", meuLanche.cepMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@municipioMeuLanche", meuLanche.municipioMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@ufMeuLanche", meuLanche.ufMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@emailMeuLanche", meuLanche.emailMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@contatoMeuLanche", meuLanche.contatoMeuLanche);
                acessoDadosSqlServer.AdicionarParametros("@localLogoMeuLanche", meuLanche.localLogoMeuLanche);

                string idMeuLanche = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspMeuLancheCrudAlterar").ToString();

                return idMeuLanche;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public MeuLancheCollections ConsultarPorNome()
        {
            try
            {
                MeuLancheCollections meuLancheCollections = new MeuLancheCollections();

                DataTable dataTableMeuLanche = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspMeuLancheListaNome");

                foreach (DataRow linha in dataTableMeuLanche.Rows)
                {
                    MeuLanche meuLanche = new MeuLanche();

                    meuLanche.idMeuLanche = Convert.ToInt32(linha["idMeuLanche"]);
                    meuLanche.nomeMeuLanche = linha["nomeMeuLanche"].ToString().ToUpper();
                    meuLanche.cnpjMeuLanche = linha["cnpjMeuLanche"].ToString().ToUpper();
                    meuLanche.inscricaoEstadualMeuLanche = linha["inscricaoEstadualMeuLanche"].ToString().ToUpper();
                    meuLanche.telefoneMeuLanche = linha["telefoneMeuLanche"].ToString().ToUpper();
                    meuLanche.faxMeuLanche = linha["faxMeuLanche"].ToString().ToUpper();
                    meuLanche.enderecoMeuLanche = linha["enderecoMeuLanche"].ToString().ToUpper();
                    meuLanche.complementoMeuLanche = linha["complementoMeuLanche"].ToString().ToUpper();
                    meuLanche.bairroMeuLanche = linha["bairroMeuLanche"].ToString().ToUpper();
                    meuLanche.cepMeuLanche = linha["cepMeuLanche"].ToString().ToUpper();
                    meuLanche.municipioMeuLanche = linha["municipioMeuLanche"].ToString().ToUpper();
                    meuLanche.ufMeuLanche = linha["ufMeuLanche"].ToString().ToUpper();
                    meuLanche.emailMeuLanche = linha["emailMeuLanche"].ToString().ToUpper();
                    meuLanche.contatoMeuLanche = linha["contatoMeuLanche"].ToString().ToUpper();
                    meuLanche.localLogoMeuLanche = linha["localLogoMeuLanche"].ToString();

                    meuLancheCollections.Add(meuLanche);
                }

                return meuLancheCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o MeuLanche por NOME. Detalhes: " + ex.Message);
            }
        }

        
    }
}
