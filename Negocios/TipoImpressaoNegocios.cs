﻿using System;
using System.Data;
using AcessoBancoDados;

namespace Negocios
{
    public class TipoImpressaoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public void trocar(int tipo)
        {
            try
            {
                acessoDadosSqlServer.ExecutaManipulação(CommandType.Text, "UPDATE tipoImpressao SET tipo = " + tipo);
            }
            catch (Exception ex)
            {

            }
        }
        
        public int pegar()
        {
            int tipo = -1;

            try
            {
                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "SELECT * FROM TipoImpressao");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                     tipo = Convert.ToInt32(linha["tipo"]);
                }
                
                return tipo;
            }
            catch (Exception ex)
            {
                throw new Exception("Error ao Selecionar tipo de Impressão. Detalhes: " + ex.Message);
            }
        }
        
    }
}
