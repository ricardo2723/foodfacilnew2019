﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class CaixaNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Caixa caixa)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@valorCaixaAbre", caixa.valorCaixaAbre);
                acessoDadosSqlServer.AdicionarParametros("@valorCaixaFecha", caixa.valorCaixaFecha);
                acessoDadosSqlServer.AdicionarParametros("@dataCaixa", caixa.dataCaixa);
                acessoDadosSqlServer.AdicionarParametros("@horaCaixaAbre", caixa.horaCaixaAbre);
                acessoDadosSqlServer.AdicionarParametros("@statusCaixa", caixa.statusCaixa);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", caixa.usuarioId);
                string idCaixa = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspCaixaCrudInserir").ToString();

                return idCaixa;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Caixa caixa)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idCaixa", caixa.idCaixa);
                acessoDadosSqlServer.AdicionarParametros("@valorCaixaFecha", caixa.valorCaixaFecha);
                acessoDadosSqlServer.AdicionarParametros("@horaCaixaFecha", caixa.horaCaixaFecha);
                acessoDadosSqlServer.AdicionarParametros("@statusCaixa", caixa.statusCaixa);
                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspCaixaCrudAlterar").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string checaCaixa()
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@dataCaixa", DateTime.Now);

                string idVendCabecalho = acessoDadosSqlServer.ExecutaConsultaEscalar(CommandType.StoredProcedure, "uspCaixaChecaCaixa").ToString();

                return idVendCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public CaixaCollections Consultar()
        {
            try
            {
                CaixaCollections caixaCollections = new CaixaCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@dataCaixa", DateTime.Now);

                DataTable dataTableCaixa = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspCaixaListaCaixa");

                foreach (DataRow linha in dataTableCaixa.Rows)
                {
                    Caixa caixa = new Caixa();

                    caixa.idCaixa = Convert.ToInt32(linha["idCaixa"]);
                    caixa.valorCaixaAbre = Convert.ToDecimal(linha["valorCaixaAbre"]);
                    caixa.dataCaixa = Convert.ToDateTime(linha["dataCaixa"]);
                    caixa.horaCaixaAbre = Convert.ToDateTime(linha["horaCaixaAbre"]);
                    caixa.nomeUsuario = linha["usuarioLogin"].ToString();
                    //caixa.totalDesconto = Convert.ToDecimal(linha["totalDesconto"]);

                    caixaCollections.Add(caixa);
                }

                return caixaCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Caixa. Detalhes: " + ex.Message);
            }
        }

        public decimal Retiradas(DateTime hora, int caixa)
        {
            decimal totalRetirada = 0;

            try
            {
                RetiradasCollections retiradasCollections = new RetiradasCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@hora", hora);
                acessoDadosSqlServer.AdicionarParametros("@caixa", caixa);

                DataTable dataTableRetirada = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspRetiradasSoma");

                foreach (DataRow linha in dataTableRetirada.Rows)
                {
                    if (linha["totalRetiradas"].ToString() == "")
                    {
                        totalRetirada = 0;
                    }
                    else
                    {
                        totalRetirada = Convert.ToDecimal(linha["totalRetiradas"]);
                    }
                }

                return totalRetirada;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Caixa. Detalhes: " + ex.Message);
            }
        }

        public CaixaCollections pagamentosCaixa(DateTime hora, int idCaixa)
        {
            try
            {
                CaixaCollections caixaCollections = new CaixaCollections();

                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@tipo", "1");
                acessoDadosSqlServer.AdicionarParametros("@hora", hora);
                acessoDadosSqlServer.AdicionarParametros("@caixa", idCaixa);
                DataTable dataTableCaixa = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspCaixaPagamentosCaixa");

                foreach (DataRow linha in dataTableCaixa.Rows)
                {
                    Caixa caixa = new Caixa();

                    caixa.totalCaixa = Convert.ToDecimal(linha["total"]);
                    caixa.descricaoCaixa = linha["descricaoVCTipoPagamento"].ToString().ToUpper();
                    
                    caixaCollections.Add(caixa);
                }

                return caixaCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Caixa. Detalhes: " + ex.Message);
            }
        }

        public CaixaCollections descontosCaixa(DateTime hora, int idCaixa)
        {
            try
            {
                CaixaCollections caixaCollections = new CaixaCollections();

                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@tipo", "2");
                acessoDadosSqlServer.AdicionarParametros("@hora", hora);
                acessoDadosSqlServer.AdicionarParametros("@caixa", idCaixa);
                DataTable dataTableCaixa = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspCaixaPagamentosCaixa");

                foreach (DataRow linha in dataTableCaixa.Rows)
                {
                    Caixa caixa = new Caixa();

                    caixa.totalDesconto = Convert.ToDecimal(linha["totalDesconto"]);

                    caixaCollections.Add(caixa);
                }

                return caixaCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Caixa. Detalhes: " + ex.Message);
            }
        }

        public decimal deliveryCaixa(DateTime hora, int idCaixa)
        {
            decimal dataTableCaixa = 0;

            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@tipo", "3");
                acessoDadosSqlServer.AdicionarParametros("@hora", hora);
                acessoDadosSqlServer.AdicionarParametros("@caixa", idCaixa);
                dataTableCaixa = Convert.ToDecimal(acessoDadosSqlServer.ExecutaConsultaEscalar(CommandType.StoredProcedure, "uspCaixaPagamentosCaixa"));

                return dataTableCaixa;
            }
            catch (Exception ex)
            {
                //throw new Exception("Não foi possivel Consultar o Caixa. Detalhes: " + ex.Message);
                return dataTableCaixa = 0;
            }
        }
    }
}
