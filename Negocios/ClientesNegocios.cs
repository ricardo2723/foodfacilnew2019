﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class ClientesNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Clientes clientes)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeCliente", clientes.nomeCliente);
                acessoDadosSqlServer.AdicionarParametros("@cpfCliente", clientes.cpfCliente);
                acessoDadosSqlServer.AdicionarParametros("@cnpjCliente", clientes.cnpjCliente);
                acessoDadosSqlServer.AdicionarParametros("@identidadeCliente", clientes.identidadeCliente);
                acessoDadosSqlServer.AdicionarParametros("@telefoneCliente", clientes.telefoneCliente);
                acessoDadosSqlServer.AdicionarParametros("@celularCliente", clientes.celularCliente);
                acessoDadosSqlServer.AdicionarParametros("@cepCliente", clientes.cepCliente);
                acessoDadosSqlServer.AdicionarParametros("@enderecoCliente", clientes.enderecoCliente);
                acessoDadosSqlServer.AdicionarParametros("@complementoCliente", clientes.complementoCliente);
                acessoDadosSqlServer.AdicionarParametros("@bairroCliente", clientes.bairroCliente);
                acessoDadosSqlServer.AdicionarParametros("@municipioCliente", clientes.municipioCliente);
                acessoDadosSqlServer.AdicionarParametros("@ufCliente", clientes.ufCliente);
                acessoDadosSqlServer.AdicionarParametros("@emailCliente", clientes.emailCliente);
                acessoDadosSqlServer.AdicionarParametros("@dataNascimentoCliente", clientes.dataNascimentoCliente);
                acessoDadosSqlServer.AdicionarParametros("@pontoReferenciaCliente", clientes.pontoReferenciaCliente);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroCliente", clientes.dataCadastroCliente);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", clientes.usuarioId);

                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspClientesCrudInserir").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Clientes clientes)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idCliente",              clientes.idCliente);
                acessoDadosSqlServer.AdicionarParametros("@nomeCliente",            clientes.nomeCliente);
                acessoDadosSqlServer.AdicionarParametros("@cpfCliente",             clientes.cpfCliente);
                acessoDadosSqlServer.AdicionarParametros("@cnpjCliente",            clientes.cnpjCliente);
                acessoDadosSqlServer.AdicionarParametros("@identidadeCliente",      clientes.identidadeCliente);
                acessoDadosSqlServer.AdicionarParametros("@telefoneCliente",        clientes.telefoneCliente);
                acessoDadosSqlServer.AdicionarParametros("@celularCliente",         clientes.celularCliente);
                acessoDadosSqlServer.AdicionarParametros("@cepCliente",             clientes.cepCliente);
                acessoDadosSqlServer.AdicionarParametros("@enderecoCliente",        clientes.enderecoCliente);
                acessoDadosSqlServer.AdicionarParametros("@complementoCliente",     clientes.complementoCliente);
                acessoDadosSqlServer.AdicionarParametros("@bairroCliente",          clientes.bairroCliente);
                acessoDadosSqlServer.AdicionarParametros("@municipioCliente",       clientes.municipioCliente);
                acessoDadosSqlServer.AdicionarParametros("@ufCliente",              clientes.ufCliente);
                acessoDadosSqlServer.AdicionarParametros("@emailCliente",           clientes.emailCliente);
                acessoDadosSqlServer.AdicionarParametros("@dataNascimentoCliente",  clientes.dataNascimentoCliente);
                acessoDadosSqlServer.AdicionarParametros("@pontoReferenciaCliente", clientes.pontoReferenciaCliente);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroCliente",    clientes.dataCadastroCliente);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId",              clientes.usuarioId);
                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspClientesCrudAlterar").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Clientes clientes)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idCliente", clientes.idCliente);
                string idCliente = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspClientesCrudExcluir").ToString();

                return idCliente;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ClientesCollections ConsultarPorNome(string nome)
        {
            try
            {
                ClientesCollections clientesCollections = new ClientesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeCliente", nome);

                DataTable dataTableClientes =  acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspClientesListaNome");

                foreach (DataRow linha in dataTableClientes.Rows)
                {
                    Clientes clientes = new Clientes();
                    
                    clientes.idCliente = Convert.ToInt32(linha["idCliente"]);
                    clientes.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                    clientes.cpfCliente = linha["cpfCliente"].ToString().ToUpper();
                    clientes.cnpjCliente = linha["cnpjCliente"].ToString().ToUpper();
                    clientes.identidadeCliente = linha["identidadeCliente"].ToString().ToUpper();
                    clientes.telefoneCliente = linha["telefoneCliente"].ToString().ToUpper();
                    clientes.celularCliente = linha["celularCliente"].ToString().ToUpper();
                    clientes.cepCliente = linha["cepCliente"].ToString().ToUpper();
                    clientes.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                    clientes.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                    clientes.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                    clientes.municipioCliente = linha["municipioCliente"].ToString().ToUpper();
                    clientes.ufCliente = linha["ufCliente"].ToString().ToUpper();
                    clientes.emailCliente = linha["emailCliente"].ToString().ToUpper();
                    clientes.dataNascimentoCliente = Convert.ToDateTime(linha["dataNascimentoCliente"]);
                    clientes.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();
                    clientes.dataCadastroCliente = Convert.ToDateTime(linha["dataCadastroCliente"]);
                    clientes.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    clientesCollections.Add(clientes); 
                }

                return clientesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por NOME. Detalhes: " + ex.Message);
            }
        }

        public ClientesCollections ConsultarPorCpf(string cpf)
        {
            try
            {
                ClientesCollections clientesCollections = new ClientesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@cpfCliente", cpf);

                DataTable dataTableClientes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspClientesListaCPF");

                foreach (DataRow linha in dataTableClientes.Rows)
                {
                    Clientes clientes = new Clientes();

                    clientes.idCliente = Convert.ToInt32(linha["idCliente"]);
                    clientes.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                    clientes.cpfCliente = linha["cpfCliente"].ToString().ToUpper();
                    clientes.cnpjCliente = linha["cnpjCliente"].ToString().ToUpper();
                    clientes.identidadeCliente = linha["identidadeCliente"].ToString().ToUpper();
                    clientes.telefoneCliente = linha["telefoneCliente"].ToString().ToUpper();
                    clientes.celularCliente = linha["celularCliente"].ToString().ToUpper();
                    clientes.cepCliente = linha["cepCliente"].ToString().ToUpper();
                    clientes.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                    clientes.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                    clientes.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                    clientes.municipioCliente = linha["municipioCliente"].ToString().ToUpper();
                    clientes.ufCliente = linha["ufCliente"].ToString().ToUpper();
                    clientes.emailCliente = linha["emailCliente"].ToString().ToUpper();
                    clientes.dataNascimentoCliente = Convert.ToDateTime(linha["dataNascimentoCliente"]);
                    clientes.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();
                    clientes.dataCadastroCliente = Convert.ToDateTime(linha["dataCadastroCliente"]);
                    clientes.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    clientesCollections.Add(clientes);
                }

                return clientesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por CPF. Detalhes: " + ex.Message);
            }
        }

        public ClientesCollections ConsultarPorCnpj(string cnpj)
        {
            try
            {
                ClientesCollections clientesCollections = new ClientesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@cnpjCliente", cnpj);

                DataTable dataTableClientes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspClientesListaCnpj");

                foreach (DataRow linha in dataTableClientes.Rows)
                {
                    Clientes clientes = new Clientes();

                    clientes.idCliente = Convert.ToInt32(linha["idCliente"]);
                    clientes.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                    clientes.cpfCliente = linha["cpfCliente"].ToString().ToUpper();
                    clientes.cnpjCliente = linha["cnpjCliente"].ToString().ToUpper();
                    clientes.identidadeCliente = linha["identidadeCliente"].ToString().ToUpper();
                    clientes.telefoneCliente = linha["telefoneCliente"].ToString().ToUpper();
                    clientes.celularCliente = linha["celularCliente"].ToString().ToUpper();
                    clientes.cepCliente = linha["cepCliente"].ToString().ToUpper();
                    clientes.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                    clientes.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                    clientes.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                    clientes.municipioCliente = linha["municipioCliente"].ToString().ToUpper();
                    clientes.ufCliente = linha["ufCliente"].ToString().ToUpper();
                    clientes.emailCliente = linha["emailCliente"].ToString().ToUpper();
                    clientes.dataNascimentoCliente = Convert.ToDateTime(linha["dataNascimentoCliente"]);
                    clientes.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();
                    clientes.dataCadastroCliente = Convert.ToDateTime(linha["dataCadastroCliente"]);
                    clientes.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    clientesCollections.Add(clientes);
                }

                return clientesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por Cnpj. Detalhes: " + ex.Message);
            }
        }

        public ClientesCollections ConsultarPorTelefone(string telefone)
        {
            try
            {
                ClientesCollections clientesCollections = new ClientesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@telefoneCliente", telefone);

                DataTable dataTableClientes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspClientesListaTelefone");

                foreach (DataRow linha in dataTableClientes.Rows)
                {
                    Clientes clientes = new Clientes();

                    clientes.idCliente = Convert.ToInt32(linha["idCliente"]);
                    clientes.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                    clientes.cpfCliente = linha["cpfCliente"].ToString().ToUpper();
                    clientes.cnpjCliente = linha["cnpjCliente"].ToString().ToUpper();
                    clientes.identidadeCliente = linha["identidadeCliente"].ToString().ToUpper();
                    clientes.telefoneCliente = linha["telefoneCliente"].ToString().ToUpper();
                    clientes.celularCliente = linha["celularCliente"].ToString().ToUpper();
                    clientes.cepCliente = linha["cepCliente"].ToString().ToUpper();
                    clientes.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                    clientes.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                    clientes.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                    clientes.municipioCliente = linha["municipioCliente"].ToString().ToUpper();
                    clientes.ufCliente = linha["ufCliente"].ToString().ToUpper();
                    clientes.emailCliente = linha["emailCliente"].ToString().ToUpper();
                    clientes.dataNascimentoCliente = Convert.ToDateTime(linha["dataNascimentoCliente"]);
                    clientes.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();
                    clientes.dataCadastroCliente = Convert.ToDateTime(linha["dataCadastroCliente"]);
                    clientes.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    clientesCollections.Add(clientes);
                }

                return clientesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por Telefone. Detalhes: " + ex.Message);
            }
        }
    }
}
