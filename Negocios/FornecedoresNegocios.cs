﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class FornecedoresNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Fornecedores fornecedores)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@nomeFornecedor", fornecedores.nomeFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@cnpjFornecedor", fornecedores.cnpjFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@cpfFornecedor", fornecedores.cpfFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@inscricaoEstadualFornecedor", fornecedores.inscricaoEstadualFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@telefoneFornecedor", fornecedores.telefoneFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@faxFornecedor", fornecedores.faxFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@enderecoFornecedor", fornecedores.enderecoFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@complementoFornecedor", fornecedores.complementoFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@bairroFornecedor", fornecedores.bairroFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@cepFornecedor", fornecedores.cepFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@municipioFornecedor", fornecedores.municipioFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@ufFornecedor", fornecedores.ufFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@emailFornecedor", fornecedores.emailFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@contatoFornecedor", fornecedores.contatoFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroFornecedor", fornecedores.dataCadastroFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@pontoReferenciaFornecedor", fornecedores.pontoReferenciaFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", fornecedores.usuarioId);

                string idFornecedor = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspFornecedoresCrudInserir").ToString();

                return idFornecedor;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Alterar(Fornecedores fornecedores)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idFornecedor", fornecedores.idFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@nomeFornecedor", fornecedores.nomeFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@cnpjFornecedor", fornecedores.cnpjFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@cpfFornecedor", fornecedores.cpfFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@inscricaoEstadualFornecedor", fornecedores.inscricaoEstadualFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@telefoneFornecedor", fornecedores.telefoneFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@faxFornecedor", fornecedores.faxFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@enderecoFornecedor", fornecedores.enderecoFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@complementoFornecedor", fornecedores.complementoFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@bairroFornecedor", fornecedores.bairroFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@cepFornecedor", fornecedores.cepFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@municipioFornecedor", fornecedores.municipioFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@ufFornecedor", fornecedores.ufFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@emailFornecedor", fornecedores.emailFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@contatoFornecedor", fornecedores.contatoFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroFornecedor", fornecedores.dataCadastroFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@pontoReferenciaFornecedor", fornecedores.pontoReferenciaFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", fornecedores.usuarioId);

                string idFornecedor = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspFornecedoresCrudAlterar").ToString();

                return idFornecedor;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Fornecedores fornecedores)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idFornecedor", fornecedores.idFornecedor);
                string idFornecedor = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspFornecedoresCrudExcluir").ToString();

                return idFornecedor;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public FornecedoresCollections ConsultarPorNome(string nome)
        {
            try
            {
                FornecedoresCollections fornecedoresCollections = new FornecedoresCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeFornecedor", nome);

                DataTable dataTableFornecedores = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspFornecedoresListaNome");

                foreach (DataRow linha in dataTableFornecedores.Rows)
                {
                    Fornecedores fornecedores = new Fornecedores();

                    fornecedores.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    fornecedores.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    fornecedores.cnpjFornecedor = linha["cnpjFornecedor"].ToString().ToUpper();
                    fornecedores.cpfFornecedor = linha["cpfFornecedor"].ToString().ToUpper();
                    fornecedores.inscricaoEstadualFornecedor = linha["inscricaoEstadualFornecedor"].ToString().ToUpper();
                    fornecedores.telefoneFornecedor = linha["telefoneFornecedor"].ToString().ToUpper();
                    fornecedores.faxFornecedor = linha["faxFornecedor"].ToString().ToUpper();
                    fornecedores.enderecoFornecedor = linha["enderecoFornecedor"].ToString().ToUpper();
                    fornecedores.complementoFornecedor = linha["complementoFornecedor"].ToString().ToUpper();
                    fornecedores.bairroFornecedor = linha["bairroFornecedor"].ToString().ToUpper();
                    fornecedores.cepFornecedor = linha["cepFornecedor"].ToString().ToUpper();
                    fornecedores.municipioFornecedor = linha["municipioFornecedor"].ToString().ToUpper();
                    fornecedores.ufFornecedor = linha["ufFornecedor"].ToString().ToUpper();
                    fornecedores.emailFornecedor = linha["emailFornecedor"].ToString().ToUpper();
                    fornecedores.contatoFornecedor = (linha["contatoFornecedor"]).ToString().ToUpper();
                    fornecedores.dataCadastroFornecedor = Convert.ToDateTime(linha["dataCadastroFornecedor"]);
                    fornecedores.pontoReferenciaFornecedor = linha["pontoReferenciaFornecedor"].ToString().ToUpper();

                    fornecedores.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    fornecedoresCollections.Add(fornecedores);
                }

                return fornecedoresCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Fornecedor por NOME. Detalhes: " + ex.Message);
            }
        }

        public FornecedoresCollections ConsultarPorCpf(string cpf)
        {
            try
            {
                FornecedoresCollections fornecedoresCollections = new FornecedoresCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@cpfFornecedor", cpf);

                DataTable dataTableFornecedores = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspFornecedoresListaCPF");

                foreach (DataRow linha in dataTableFornecedores.Rows)
                {
                    Fornecedores fornecedores = new Fornecedores();

                    fornecedores.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    fornecedores.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    fornecedores.cnpjFornecedor = linha["cnpjFornecedor"].ToString().ToUpper();
                    fornecedores.cpfFornecedor = linha["cpfFornecedor"].ToString().ToUpper();
                    fornecedores.inscricaoEstadualFornecedor = linha["inscricaoEstadualFornecedor"].ToString().ToUpper();
                    fornecedores.telefoneFornecedor = linha["telefoneFornecedor"].ToString().ToUpper();
                    fornecedores.faxFornecedor = linha["faxFornecedor"].ToString().ToUpper();
                    fornecedores.enderecoFornecedor = linha["enderecoFornecedor"].ToString().ToUpper();
                    fornecedores.complementoFornecedor = linha["complementoFornecedor"].ToString().ToUpper();
                    fornecedores.bairroFornecedor = linha["bairroFornecedor"].ToString().ToUpper();
                    fornecedores.cepFornecedor = linha["cepFornecedor"].ToString().ToUpper();
                    fornecedores.municipioFornecedor = linha["municipioFornecedor"].ToString().ToUpper();
                    fornecedores.ufFornecedor = linha["ufFornecedor"].ToString().ToUpper();
                    fornecedores.emailFornecedor = linha["emailFornecedor"].ToString().ToUpper();
                    fornecedores.contatoFornecedor = (linha["contatoFornecedor"]).ToString().ToUpper();
                    fornecedores.dataCadastroFornecedor = Convert.ToDateTime(linha["dataCadastroFornecedor"]);
                    fornecedores.pontoReferenciaFornecedor = linha["pontoReferenciaFornecedor"].ToString().ToUpper();
                    fornecedores.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    fornecedoresCollections.Add(fornecedores);
                }

                return fornecedoresCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Fornecedor por CPF. Detalhes: " + ex.Message);
            }
        }

        public FornecedoresCollections ConsultarPorCnpj(string cnpj)
        {
            try
            {
                FornecedoresCollections fornecedoresCollections = new FornecedoresCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@cnpjFornecedor", cnpj);

                DataTable dataTableFornecedores = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspFornecedoresListaCnpj");

                foreach (DataRow linha in dataTableFornecedores.Rows)
                {
                    Fornecedores fornecedores = new Fornecedores();

                    fornecedores.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    fornecedores.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    fornecedores.cnpjFornecedor = linha["cnpjFornecedor"].ToString().ToUpper();
                    fornecedores.cpfFornecedor = linha["cpfFornecedor"].ToString().ToUpper();
                    fornecedores.inscricaoEstadualFornecedor = linha["inscricaoEstadualFornecedor"].ToString().ToUpper();
                    fornecedores.telefoneFornecedor = linha["telefoneFornecedor"].ToString().ToUpper();
                    fornecedores.faxFornecedor = linha["faxFornecedor"].ToString().ToUpper();
                    fornecedores.enderecoFornecedor = linha["enderecoFornecedor"].ToString().ToUpper();
                    fornecedores.complementoFornecedor = linha["complementoFornecedor"].ToString().ToUpper();
                    fornecedores.bairroFornecedor = linha["bairroFornecedor"].ToString().ToUpper();
                    fornecedores.cepFornecedor = linha["cepFornecedor"].ToString().ToUpper();
                    fornecedores.municipioFornecedor = linha["municipioFornecedor"].ToString().ToUpper();
                    fornecedores.ufFornecedor = linha["ufFornecedor"].ToString().ToUpper();
                    fornecedores.emailFornecedor = linha["emailFornecedor"].ToString().ToUpper();
                    fornecedores.contatoFornecedor = (linha["contatoFornecedor"]).ToString().ToUpper();
                    fornecedores.dataCadastroFornecedor = Convert.ToDateTime(linha["dataCadastroFornecedor"]);
                    fornecedores.pontoReferenciaFornecedor = linha["pontoReferenciaFornecedor"].ToString().ToUpper();
                    fornecedores.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    fornecedoresCollections.Add(fornecedores);
                }

                return fornecedoresCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Fornecedor por Cnpj. Detalhes: " + ex.Message);
            }
        }
    }
}
