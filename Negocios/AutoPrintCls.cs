﻿using System;
using System.IO;
using System.Text;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using Microsoft.Reporting.WinForms;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace Negocios
{
    public class AutoPrintCls : PrintDocument
    {
        private PageSettings m_pageSettings;
        private int m_currentPage;
        private List<Stream> m_pages = new List<Stream>();
        private string tipoPrint;
        public AutoPrintCls(ServerReport serverReport) : this((Report)serverReport)
        {
            RenderAllServerReportPages(serverReport);
        }

        public AutoPrintCls(LocalReport localReport) : this((Report)localReport)
        {
            RenderAllLocalReportPages(localReport);
        }

        private AutoPrintCls(Report report)
        {
            // Definir as configurações da página para o padrão definido no relatório
            ReportPageSettings reportPageSettings = report.GetDefaultPageSettings();
            // As configurações da página objeto vai usar a impressora padrão, a menos que
            // PageSettings.PrinterSettings é alterada. Isto assume que não
            // É uma impressora padrão.
            m_pageSettings = new PageSettings(); 
            m_pageSettings.PaperSize = reportPageSettings.PaperSize;
            m_pageSettings.Margins = reportPageSettings.Margins;
            
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                foreach (Stream s in m_pages)
                {
                    s.Dispose();
                }

                m_pages.Clear();
            }
        }

        protected override void OnBeginPrint(PrintEventArgs e)
        {
            base.OnBeginPrint(e);
            m_currentPage = 0;
        }

        protected override void OnPrintPage(PrintPageEventArgs e)
        {
            base.OnPrintPage(e);

            Stream pageToPrint = m_pages[m_currentPage];
            pageToPrint.Position = 0;

            // Coloque cada página em um Metafile para desenhá-lo.
            using (Metafile pageMetaFile = new Metafile(pageToPrint))
            {
                Rectangle adjustedRect = new Rectangle(
                        e.PageBounds.Left - (int)e.PageSettings.HardMarginX,
                        e.PageBounds.Top - (int)e.PageSettings.HardMarginY,
                        e.PageBounds.Width,
                        e.PageBounds.Height);

                // Desenhe um fundo branco para o relatório
                e.Graphics.FillRectangle(Brushes.White, adjustedRect);

                // Desenhe o conteúdo do relatório
                e.Graphics.DrawImage(pageMetaFile, adjustedRect);

                // Prepare-se para a página seguinte. Certifique-se de que não ter atingido o fim.
                m_currentPage++;
                e.HasMorePages = m_currentPage < m_pages.Count;
            }
        }

        protected override void OnQueryPageSettings(QueryPageSettingsEventArgs e)
        {
            e.PageSettings = (PageSettings)m_pageSettings.Clone();
        }

        private void RenderAllServerReportPages(ServerReport serverReport)
        {
            try
            {
                string deviceInfo = CreateEMFDeviceInfo();

                // Gerando páginas de representantes de imagem uma de cada vez pode ser caro. em ordem
                // Para gerar a página 2, o servidor teria que recalcular página 1 e jogá-lo
                // Distância. Usando PersistStreams faz com que o servidor para gerar todas as páginas
                // O fundo, mas voltar assim que a página 1 fique completa.
                NameValueCollection firstPageParameters = new NameValueCollection();
                firstPageParameters.Add("rs:PersistStreams", "True");

                // GetNextStream retorna a próxima página na seqüência do processo de fundo
                // Iniciado por PersistStreams.
                NameValueCollection nonFirstPageParameters = new NameValueCollection();
                nonFirstPageParameters.Add("rs:GetNextStream", "True");

                string mimeType;
                string fileExtension;


                Stream pageStream = serverReport.Render("IMAGE", deviceInfo, firstPageParameters, out mimeType, out fileExtension);

                // O servidor retorna um fluxo de vazio quando se deslocam para além da última página.
                while (pageStream.Length > 0)
                {
                    m_pages.Add(pageStream);

                    pageStream = serverReport.Render("IMAGE", deviceInfo, nonFirstPageParameters, out mimeType, out fileExtension);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("possible missing information ::  " + e);
            }
        }

        private void RenderAllLocalReportPages(LocalReport localReport)
        {
            try
            {
                string deviceInfo = CreateEMFDeviceInfo();

                Warning[] warnings;

                localReport.Render("IMAGE", deviceInfo, LocalReportCreateStreamCallback, out warnings);
            }
            catch (Exception e)
            {
                MessageBox.Show("error :: " + e);
            }
        }

        private Stream LocalReportCreateStreamCallback(
            string name,
            string extension,
            Encoding encoding,
            string mimeType,
            bool willSeek)
        {
            MemoryStream stream = new MemoryStream();
            m_pages.Add(stream);

            return stream;
        }

        private string CreateEMFDeviceInfo()
        {
            PaperSize paperSize = m_pageSettings.PaperSize;
            Margins margins = m_pageSettings.Margins;

            // A informação dispositivo string define o intervalo de páginas para imprimir, bem como o tamanho da página.
            // A página inicial e final de 0 significa gerar todas as páginas.
            return string.Format(
                CultureInfo.InvariantCulture,
                "<DeviceInfo><OutputFormat>emf</OutputFormat><StartPage>0</StartPage><EndPage>0</EndPage><MarginTop>{0}</MarginTop><MarginLeft>{1}</MarginLeft><MarginRight>{2}</MarginRight><MarginBottom>{3}</MarginBottom><PageHeight>{4}</PageHeight><PageWidth>{5}</PageWidth></DeviceInfo>",
                ToInches(margins.Top),
                ToInches(margins.Left),
                ToInches(margins.Right),
                ToInches(margins.Bottom),
                ToInches(paperSize.Height),
                ToInches(paperSize.Width));
        }

        private static string ToInches(int hundrethsOfInch)
        {
            double inches = hundrethsOfInch / 100.0;
            return inches.ToString(CultureInfo.InvariantCulture) + "in";
        }
    }
}

