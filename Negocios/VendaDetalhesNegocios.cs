﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Data;

namespace Negocios
{
    public class VendaDetalhesNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        /* NOVOS METODOS PARA O PDV */

        public VendaDetalhesCollections BuscaItensPedido(int pedido)
        {
            try
            {
                VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();
                              
                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_VendaDetalhesListaItens where idVendaCabecalhoDetalhes = " + pedido);

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaDetalhes vendaDetalhes = new VendaDetalhes();

                    vendaDetalhes.idVendaDetalhes = Convert.ToInt32(linha["idVendaDetalhes"]);
                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(linha["idVendaCabecalhoDetalhes"]);
                    vendaDetalhes.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    vendaDetalhes.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    vendaDetalhes.especialVendaDetalhes = linha["especialVendaDetalhes"].ToString().ToUpper();
                    vendaDetalhes.extraVendaDetalhes = linha["extraVendaDetalhes"].ToString().ToUpper();
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(linha["quantidadeVendaDetalhes"]);
                    vendaDetalhes.valorUnitarioVendaDetalhes = Convert.ToDecimal(linha["valorUnitarioVendaDetalhes"]);
                    vendaDetalhes.valorTotalVendaDetalhes = Convert.ToDecimal(linha["valorTotalVendaDetalhes"]);
                    vendaDetalhes.dataEnvioVendaDetalhes = Convert.ToDateTime(linha["dataEnvioVendaDetalhes"]);
                    vendaDetalhes.statusVendaDetalhes = Convert.ToInt32(linha["statusVendaDetalhes"]);
                    vendaDetalhes.atendenteVenda = linha["atendenteVendaCabecalho"].ToString();
                    vendaDetalhes.tipoProduto = Char.Parse(linha["tipoProduto"].ToString());

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items da Venda. Detalhes: " + ex.Message);
            }
        }


        /* --------------------------------------------------------------------------------------------------------------------------------- */

        public string inserir(VendaDetalhes vendaDetalhes)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalhoDetalhes", vendaDetalhes.idVendaCabecalhoDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", vendaDetalhes.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@especialVendaDetalhes", vendaDetalhes.especialVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@extraVendaDetalhes", vendaDetalhes.extraVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@quantidadeVendaDetalhes", vendaDetalhes.quantidadeVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@valorUnitarioVendaDetalhes", vendaDetalhes.valorUnitarioVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@valorTotalVendaDetalhes", vendaDetalhes.valorTotalVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@dataEnvioVendaDetalhes", vendaDetalhes.dataEnvioVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaDetalhes", vendaDetalhes.statusVendaDetalhes);

                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesCrudInserir").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public int AtualizarItemEspecial(string especial, int idVenda)
        {
            try
            {
                acessoDadosSqlServer.ExecutaManipulação(CommandType.Text, "UPDATE VendaDetalhes SET especialVendaDetalhes = '" + especial + "' WHERE idVendaDetalhes = " + idVenda);

                return 1;
            }
            catch (Exception)
            {
                //ERROR AO INSERIR ESPECIAL
                return 0;
            }
        }

        public int AtualizarItemExtra(string descricaoExtra, int idVenda)
        {
            try
            {
                acessoDadosSqlServer.ExecutaManipulação(CommandType.Text, "UPDATE VendaDetalhes SET extraVendaDetalhes = '" + descricaoExtra + "' WHERE idVendaDetalhes = " + idVenda);

                return 1;
            }
            catch (Exception)
            {
                //ERROR AO INSERIR ESPECIAL
                return 0;
            }
        }

        public string inserirItemExtra(int idItem, int idExtra, int idVenda)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendasDetalhesItem", idItem);
                acessoDadosSqlServer.AdicionarParametros("@IdVendasDetalhesExtra", idExtra);
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", idVenda);

                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesCrudInserirItemExtra").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string atualizarEstoque(VendaDetalhes vendaDetalhes)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", vendaDetalhes.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@estoqueProduto", vendaDetalhes.estoqueProduto);

                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesBaixaProduto").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string enviarVenda(VendaDetalhes vendaDetalhes, int status)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalhoDetalhes", vendaDetalhes.idVendaCabecalhoDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaDetalhes", status);

                string idVendCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesEnviarVenda").ToString();

                return idVendCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string entregarVenda(VendaDetalhes vendaDetalhes)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaDetalhes", vendaDetalhes.idVendaDetalhes);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaDetalhes", 3);

                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesEntregarVenda").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string finalizarItens(int pedido, int status)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalhoDetalhes", pedido);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaDetalhes", status);

                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesFinalizarItens").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public VendaDetalhesItemExtraCollections buscarProdItensExtraAtualizar(int idVenda)
        {
            try
            {
                VendaDetalhesItemExtraCollections vendaDetalhesCollections = new VendaDetalhesItemExtraCollections();

                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "Select * from ItemExtraDetalhes Where idVendasDetalhesItem =  (select idVendasDetalhesItem from ItemExtraDetalhes where idVendasDetalhesExtra = " + idVenda + ")  and  idVendasDetalhesExtra != " + idVenda);

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaDetalhesItemExtra vendaDetalhes = new VendaDetalhesItemExtra();

                    vendaDetalhes.idVendasDetalhesItem = Convert.ToInt32(linha["idVendasDetalhesItem"]);
                    vendaDetalhes.IdVendasDetalhesExtra = Convert.ToInt32(linha["IdVendasDetalhesExtra"]);
                    vendaDetalhes.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"]);

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items do Extra. Detalhes: " + ex.Message);
            }
        }

        public VendaDetalhesItemExtraCollections buscarItensExtra(int idVenda)
        {
            try
            {
                VendaDetalhesItemExtraCollections vendaDetalhesCollections = new VendaDetalhesItemExtraCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVenda", idVenda);

                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspItemExtraDetalhesBuscar");

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaDetalhesItemExtra vendaDetalhes = new VendaDetalhesItemExtra();

                    vendaDetalhes.idVendasDetalhesItem = Convert.ToInt32(linha["idVendasDetalhesItem"]);
                    vendaDetalhes.IdVendasDetalhesExtra = Convert.ToInt32(linha["IdVendasDetalhesExtra"]);
                    vendaDetalhes.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"]);

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items do Extra. Detalhes: " + ex.Message);
            }
        }

        public VendaDetalhesCollections buscarProdutoItensExtra(int idVenda)
        {
            try
            {
                VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();

                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "Select VendaDetalhes.codigoProduto, quantidadeVendaDetalhes, descricaoProduto, tipoProduto from VendaDetalhes join Produto on VendaDetalhes.CodigoProduto = Produto.CodigoProduto Where idVendaDetalhes =" + idVenda);

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaDetalhes vendaDetalhes = new VendaDetalhes();

                    vendaDetalhes.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToInt32(linha["quantidadeVendaDetalhes"]);
                    vendaDetalhes.descricaoProduto = linha["descricaoProduto"].ToString();
                    vendaDetalhes.tipoProduto = Convert.ToChar(linha["tipoProduto"]);

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items do Extra. Detalhes: " + ex.Message);
            }
        }

        public string ExcluirItensExtraDetalhes(int id)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.Text, "delete from ItemExtraDetalhes where idVendaCabecalho = " + id + "").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ExcluirItensPedido(int idPedido)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVenda", idPedido);
                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaDetalhesExcluirItensPedido").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public VendaDetalhesCollections consulta(string pedido)
        {
            try
            {
                VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalhoDetalhes", pedido);

                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaDetalhesListaItens");

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaDetalhes vendaDetalhes = new VendaDetalhes();

                    vendaDetalhes.idVendaDetalhes = Convert.ToInt32(linha["idVendaDetalhes"]);
                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(linha["idVendaCabecalhoDetalhes"]);
                    vendaDetalhes.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    vendaDetalhes.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    vendaDetalhes.especialVendaDetalhes = linha["especialVendaDetalhes"].ToString().ToUpper();
                    vendaDetalhes.extraVendaDetalhes = linha["extraVendaDetalhes"].ToString().ToUpper();
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(linha["quantidadeVendaDetalhes"]);
                    vendaDetalhes.valorUnitarioVendaDetalhes = Convert.ToDecimal(linha["valorUnitarioVendaDetalhes"]);
                    vendaDetalhes.valorTotalVendaDetalhes = Convert.ToDecimal(linha["valorTotalVendaDetalhes"]);
                    vendaDetalhes.dataEnvioVendaDetalhes = Convert.ToDateTime(linha["dataEnvioVendaDetalhes"]);
                    vendaDetalhes.statusVendaDetalhes = Convert.ToInt32(linha["statusVendaDetalhes"]);
                    vendaDetalhes.atendenteVenda = linha["atendenteVendaCabecalho"].ToString();

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items da Venda. Detalhes: " + ex.Message);
            }
        }

        public VendaDetalhesCollections visualizarPedidos(int grupo)
        {
            try
            {
                VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idGrupo", grupo);

                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaDetalhesVisualizarPedidos");

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaDetalhes vendaDetalhes = new VendaDetalhes();

                    vendaDetalhes.idVendaDetalhes = Convert.ToInt32(linha["idVendaDetalhes"]);
                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(linha["idVendaCabecalhoDetalhes"]);
                    vendaDetalhes.atendimentoVendaCabecalho = checaTipoAtendimento(linha["atendimentoVendaCabecalho"].ToString().ToUpper());
                    vendaDetalhes.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    vendaDetalhes.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    vendaDetalhes.extraVendaDetalhes = checaNull(linha["extraVendaDetalhes"].ToString().ToUpper());
                    vendaDetalhes.especialVendaDetalhes = checaNull(linha["especialVendaDetalhes"].ToString().ToUpper());
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(linha["quantidadeVendaDetalhes"]);
                    vendaDetalhes.atendenteVenda = linha["atendenteVendaCabecalho"].ToString();

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items da Venda. Detalhes: " + ex.Message);
            }
        }

        public VendaCabecalhoCollections tempoEsperaPedido()
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

                acessoDadosSqlServer.limparParametros();

                DataTable dataTableVendaDetalhes = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaDetalhesTempoEsperaPedido");

                foreach (DataRow linha in dataTableVendaDetalhes.Rows)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();

                    vendaCabecalho.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"]);
                    vendaCabecalho.nomeAtendimento = checaTipoAtendimento(linha["atendimentoVendaCabecalho"].ToString().ToUpper());
                    vendaCabecalho.horaVendaCabecalho = Convert.ToDateTime(linha["horaVendaCabecalho"].ToString());
                    vendaCabecalho.atendenteVendaCabecalho = linha["atendenteVendaCabecalho"].ToString();

                    vendaCabecalhoCollections.Add(vendaCabecalho);
                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items da Venda. Detalhes: " + ex.Message);
            }
        }

        public int checaItensEnviados(int idVenda)
        {
            try
            {
                VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalhoDetalhes", idVenda);

                int cont = (Int32)acessoDadosSqlServer.ExecutaConsultaEscalar(CommandType.StoredProcedure, "uspVendaDetalhesChecaItensEnviados");

                return cont;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Carregar os items da Venda. Detalhes: " + ex.Message);
            }
        }

        #region Metodo

        private string checaNull(string nulo)
        {
            if (nulo == "")
            {
                nulo = "Não Possui";
                return nulo;
            }
            else
            {
                return nulo;
            }
        }

        static public string checaTipoAtendimento(string tipo)
        {
            string retorno = "";
            string getTipo = tipo.Substring(0, 1);

            switch (getTipo)
            {
                case "D":
                    retorno = "DELIVERY";
                    break;

                case "M":
                    if (tipo.Length == 2)
                    {
                        retorno = "MESA 0" + tipo.Substring(1);
                    }
                    else
                    {
                        retorno = "MESA " + tipo.Substring(1);
                    }

                    break;

                case "B":
                    if (tipo.Length == 2)
                    {
                        retorno = "BALCÃO 0" + tipo.Substring(1);
                    }
                    else
                    {
                        retorno = "BALCÃO " + tipo.Substring(1);
                    }

                    break;

                case "C":
                    if (tipo.Length == 2)
                    {
                        retorno = "CARRO 0" + tipo.Substring(1);
                    }
                    else
                    {
                        retorno = "CARRO " + tipo.Substring(1);
                    }

                    break;
            }
            return retorno;
        }

        public string ChecaAtend(string atend)
        {
            string atende = "";

            switch (atend)
            {
                case "M1":
                    atende = "MESA 01";
                    break;
                case "M2":
                    atende = "MESA 02";
                    break;
                case "M3":
                    atende = "MESA 03";
                    break;
                case "M4":
                    atende = "MESA 04";
                    break;
                case "M5":
                    atende = "MESA 05";
                    break;
                case "M6":
                    atende = "MESA 06";
                    break;
                case "M7":
                    atende = "MESA 07";
                    break;
                case "M8":
                    atende = "MESA 08";
                    break;
                case "M9":
                    atende = "MESA 09";
                    break;
                case "M10":
                    atende = "MESA 10";
                    break;
                case "M11":
                    atende = "MESA 11";
                    break;
                case "M12":
                    atende = "MESA 12";
                    break;
                case "M13":
                    atende = "MESA 13";
                    break;
                case "M14":
                    atende = "MESA 14";
                    break;
                case "M15":
                    atende = "MESA 15";
                    break;
                case "M16":
                    atende = "MESA 16";
                    break;
                case "M17":
                    atende = "MESA 17";
                    break;
                case "M18":
                    atende = "MESA 18";
                    break;
                case "M19":
                    atende = "MESA 19";
                    break;
                case "M20":
                    atende = "MESA 20";
                    break;
                case "M21":
                    atende = "MESA 21";
                    break;
                case "M22":
                    atende = "MESA 22";
                    break;
                case "M23":
                    atende = "MESA 23";
                    break;
                case "M24":
                    atende = "MESA 24";
                    break;
                case "M25":
                    atende = "MESA 25";
                    break;
                case "M26":
                    atende = "MESA 26";
                    break;
                case "M27":
                    atende = "MESA 27";
                    break;
                case "M28":
                    atende = "MESA 28";
                    break;
                case "M29":
                    atende = "MESA 29";
                    break;
                case "M30":
                    atende = "MESA 30";
                    break;
                case "M31":
                    atende = "MESA 31";
                    break;
                case "M32":
                    atende = "MESA 32";
                    break;
                case "M33":
                    atende = "MESA 33";
                    break;
                case "M34":
                    atende = "MESA 34";
                    break;
                case "M35":
                    atende = "MESA 35";
                    break;
                case "M36":
                    atende = "MESA 36";
                    break;
                case "B1":
                    atende = "BALCÃO 01";
                    break;
                case "B2":
                    atende = "BALCÃO 02";
                    break;
                case "B3":
                    atende = "BALCÃO 03";
                    break;
                case "B4":
                    atende = "BALCÃO 04";
                    break;
                case "B5":
                    atende = "BALCÃO 05";
                    break;
                case "B6":
                    atende = "BALCÃO 06";
                    break;
                case "B7":
                    atende = "BALCÃO 07";
                    break;
                case "B8":
                    atende = "BALCÃO 08";
                    break;
                case "B9":
                    atende = "BALCÃO 09";
                    break;
                case "B10":
                    atende = "BALCÃO 10";
                    break;
                case "B11":
                    atende = "BALCÃO 11";
                    break;
                case "B12":
                    atende = "BALCÃO 12";
                    break;
                case "B13":
                    atende = "BALCÃO 13";
                    break;
                case "B14":
                    atende = "BALCÃO 14";
                    break;
                case "B15":
                    atende = "BALCÃO 15";
                    break;
                case "B16":
                    atende = "BALCÃO 16";
                    break;
                case "B17":
                    atende = "BALCÃO 17";
                    break;
                case "B18":
                    atende = "BALCÃO 18";
                    break;
                case "C1":
                    atende = "CARRO 01";
                    break;
                case "C2":
                    atende = "CARRO 02";
                    break;
                case "C3":
                    atende = "CARRO 03";
                    break;
                case "C4":
                    atende = "CARRO 04";
                    break;
                case "C5":
                    atende = "CARRO 05";
                    break;
                case "C6":
                    atende = "CARRO 06";
                    break;
                case "C7":
                    atende = "CARRO 07";
                    break;
                case "C8":
                    atende = "CARRO 08";
                    break;
                case "C9":
                    atende = "CARRO 09";
                    break;
                case "C10":
                    atende = "CARRO 10";
                    break;
                case "C11":
                    atende = "CARRO 11";
                    break;
                case "C12":
                    atende = "CARRO 12";
                    break;
                case "C13":
                    atende = "CARRO 13";
                    break;
                case "C14":
                    atende = "CARRO 14";
                    break;
                case "C15":
                    atende = "CARRO 15";
                    break;
                case "C16":
                    atende = "CARRO 16";
                    break;
                case "C17":
                    atende = "CARRO 17";
                    break;
                case "C18":
                    atende = "CARRO 18";
                    break;

            }

            return atende;
        }

        #endregion
    }
}
