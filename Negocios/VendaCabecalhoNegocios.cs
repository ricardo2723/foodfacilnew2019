﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

using System.Data.SqlClient;

namespace Negocios
{
    public class VendaCabecalhoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string checaAbreVendas(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@atendimentoVendaCabecalho", vendaCabecalho.nomeAtendimento);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaCabecalho", vendaCabecalho.statusVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", vendaCabecalho.usuarioId);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoChecaVenda").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string abreVendas(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@atendimentoVendaCabecalho", vendaCabecalho.nomeAtendimento);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", vendaCabecalho.usuarioId);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaCabecalho", vendaCabecalho.statusVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@atendenteVendaCabecalho", vendaCabecalho.atendenteVendaCabecalho);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoAbreVenda").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //NOVOS PROCEIDMENTOS
        public VendaCabecalhoCollections BuscarPedidosAbertos()
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

                DataTable dataTableVendaCabecalho = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_BuscaPedidosAbertos");

                foreach (DataRow linha in dataTableVendaCabecalho.Rows)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();

                    vendaCabecalho.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"].ToString().ToUpper());
                    vendaCabecalho.idAtendimentoTipo = Convert.ToInt32(linha["idAtendimentoTipo"].ToString());
                    vendaCabecalho.nomeAtendimento = linha["nomeAtendimento"].ToString().ToUpper();
                    vendaCabecalho.numeroAtendimento = Convert.ToInt32(linha["numeroAtendimento"].ToString());
                    vendaCabecalho.atendenteVendaCabecalho = linha["atendenteVendaCabecalho"].ToString().ToUpper();
                    vendaCabecalho.statusVendaCabecalho = Convert.ToInt32(linha["statusVendaCabecalho"].ToString());

                    vendaCabecalhoCollections.Add(vendaCabecalho);
                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Pegar os Pedidos. Detalhes: " + ex.Message);
            }
        }

        public VendaCabecalho BuscarPedidosAbertosPorId(int id)
        {
            try
            {
                    
                DataTable dataTableVendaCabecalho = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_BuscaPedidosAbertos where idVendaCabecalho = " + id);

                VendaCabecalho vendaCabecalho = new VendaCabecalho();

                foreach (DataRow linha in dataTableVendaCabecalho.Rows)
                {
                    vendaCabecalho.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"].ToString().ToUpper());
                    vendaCabecalho.idAtendimentoTipo = Convert.ToInt32(linha["idAtendimentoTipo"].ToString());
                    vendaCabecalho.numeroPessoas = Convert.ToInt32(linha["numeroPessoas"].ToString());
                    vendaCabecalho.nomeAtendimento = linha["nomeAtendimento"].ToString().ToUpper();
                    vendaCabecalho.usuarioId = Convert.ToInt32(linha["usuarioId"].ToString());
                    vendaCabecalho.numeroAtendimento = Convert.ToInt32(linha["numeroAtendimento"].ToString());
                    vendaCabecalho.atendenteVendaCabecalho = linha["atendenteVendaCabecalho"].ToString().ToUpper();
                    vendaCabecalho.statusVendaCabecalho = Convert.ToInt32(linha["statusVendaCabecalho"].ToString().ToUpper());

                }

                return vendaCabecalho;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Pegar os Pedidos. Detalhes: " + ex.Message);
            }
        }


        /* ---------------------------------------------------------------------------------------------------------------------------- */
        public VendaCabecalhoDeliveryCollections checaDelivery(string idCabecalho)
        {
            try
            {
                VendaCabecalhoDeliveryCollections vendaCabecalhoDeliveryCollections = new VendaCabecalhoDeliveryCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idCabecalho", idCabecalho);

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.StoredProcedure, "uspVendaCabecalhoChecaDelivery");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        VendaCabecalhoDelivery vendaCabecalhoDelivery = new VendaCabecalhoDelivery();

                        vendaCabecalhoDelivery.idEntrega = Convert.ToInt32(linha["idEntrega"]);
                        vendaCabecalhoDelivery.idCliente = Convert.ToInt32(linha["idCliente"]);
                        vendaCabecalhoDelivery.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                        vendaCabecalhoDelivery.telefoneCliente = linha["telefoneCliente"].ToString();
                        vendaCabecalhoDelivery.celularCliente = linha["celularCliente"].ToString();
                        vendaCabecalhoDelivery.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                        vendaCabecalhoDelivery.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                        vendaCabecalhoDelivery.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                        vendaCabecalhoDelivery.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();
                        vendaCabecalhoDelivery.valorFrete = Convert.ToDecimal(linha["valorFrete"]);
                        
                        vendaCabecalhoDeliveryCollections.Add(vendaCabecalhoDelivery);
                    }

                }

                return vendaCabecalhoDeliveryCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto: " + ex.Message);
            }
        }

        public string abreDelivery(VendaCabecalhoDelivery vendaCabecalhoDelivery)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", vendaCabecalhoDelivery.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@idCliente", vendaCabecalhoDelivery.idCliente);
                acessoDadosSqlServer.AdicionarParametros("@valorFrete", vendaCabecalhoDelivery.valorFrete);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoAbreVendaDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string atualizaDelivery(VendaCabecalhoDelivery delivery)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", delivery.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@idFormaPagamento", delivery.idFormaPagamento);
                acessoDadosSqlServer.AdicionarParametros("@trocoPara", delivery.trocoPara);
                acessoDadosSqlServer.AdicionarParametros("@troco", delivery.troco);
                acessoDadosSqlServer.AdicionarParametros("@valorFrete", delivery.valorFrete);
                acessoDadosSqlServer.AdicionarParametros("@statusEntrega", delivery.statusEntrega);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoAtualizaDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string  entregaDelivery(VendaCabecalhoDelivery delivery)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", delivery.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@idEntregador", delivery.idEntregador);
                acessoDadosSqlServer.AdicionarParametros("@horaSaida", delivery.horaSaida);
                acessoDadosSqlServer.AdicionarParametros("@statusEntrega", delivery.statusEntrega);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoEntregaDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string finalizaDelivery(VendaCabecalhoDelivery delivery)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", delivery.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@horaVolta", delivery.horaVolta);
                acessoDadosSqlServer.AdicionarParametros("@statusEntrega", delivery.statusEntrega);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoFinalizaDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string cancelarDelivery(VendaCabecalhoDelivery delivery)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", delivery.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@motivoCancelamento", delivery.motivoCancelamento);
                acessoDadosSqlServer.AdicionarParametros("@statusEntrega", delivery.statusEntrega);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoCancelarDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string checaVendas(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@atendimentoVendaCabecalho", vendaCabecalho.nomeAtendimento);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoChecaVendaImprimir").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string inserirTipoPagamento(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", vendaCabecalho.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@idCaixaPagamento", vendaCabecalho.idCaixaPagamento);
                acessoDadosSqlServer.AdicionarParametros("@descricaoVCTipoPagamento", vendaCabecalho.descricaoVCTipoPagamento);
                acessoDadosSqlServer.AdicionarParametros("@valorVCTipoPagamento", vendaCabecalho.valorVCTipoPagamento);
                
                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoTipoPagamento").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string finalizarVenda(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", vendaCabecalho.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@idCliente", vendaCabecalho.idCliente);
                acessoDadosSqlServer.AdicionarParametros("@idCartao", vendaCabecalho.idCartao);
                acessoDadosSqlServer.AdicionarParametros("@valorVendaCabecalho", vendaCabecalho.valorVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@descontoVendaCabecalho", vendaCabecalho.descontoVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@totalVendaCabecalho", vendaCabecalho.totalVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaCabecalho", vendaCabecalho.statusVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", vendaCabecalho.usuarioId);
                
                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoFinalizaVenda").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string enviarVendaDelivery(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", vendaCabecalho.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@horaVendaCabecalho", vendaCabecalho.horaVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@idCliente", vendaCabecalho.idCliente);
                acessoDadosSqlServer.AdicionarParametros("@idCartao", vendaCabecalho.idCartao);
                acessoDadosSqlServer.AdicionarParametros("@valorVendaCabecalho", vendaCabecalho.valorVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@descontoVendaCabecalho", vendaCabecalho.descontoVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@totalVendaCabecalho", vendaCabecalho.totalVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaCabecalho", vendaCabecalho.statusVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", vendaCabecalho.usuarioId);

                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoEnviarVendaDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string finalizarVendaDelivery(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", vendaCabecalho.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@statusVendaCabecalho", vendaCabecalho.statusVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", vendaCabecalho.usuarioId);


                string idVendaCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoFinalizaVendaDelivery").ToString();

                return idVendaCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string cancelarVenda(VendaCabecalho vendaCabecalho)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idVenda", vendaCabecalho.idVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@totalVenda", vendaCabecalho.totalVendaCabecalho);
                acessoDadosSqlServer.AdicionarParametros("@statusVenda", 4);

                string idVendCabecalho = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoCancelaVenda").ToString();

                return idVendCabecalho;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ProdutosCollections GetProdutoVenda(char tipo, int codProduto)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.Text, "select * from vw_VendaCabecalhoProdutosListaCod where codigoProduto = " + codProduto + " and tipoProduto = '" + tipo + "'");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        Produtos produto = new Produtos();

                        produto.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                        produto.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                        produto.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                        produto.controlaEstoqueProduto = Convert.ToChar(linha["controlaEstoqueProduto"]);
                        produto.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);
                        produto.estoqueCriticoProduto = Convert.ToDecimal(linha["estoqueCriticoProduto"]);
                        produto.manufaturadoProduto = Convert.ToChar(linha["manufaturadoProduto"]);
                        produto.tipoProduto = Char.Parse(linha["tipoProduto"].ToString());
                        produto.grupoId = Convert.ToChar(linha["grupoId"]);

                        produtosCollections.Add(produto);
                    }
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto: " + ex.Message);
            }
        }

        public VendaCabecalhoCollections reemprimeVenda()
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();


                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.StoredProcedure, "uspVendaCabecalhoReemprimeVenda");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        VendaCabecalho vendaCabecalho = new VendaCabecalho();

                        vendaCabecalho.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"]);
                        vendaCabecalho.nomeAtendimento = VendaDetalhesNegocios.checaTipoAtendimento(linha["atendimentoVendaCabecalho"].ToString()).ToUpper();
                        vendaCabecalho.dataVendaCabecalho = Convert.ToDateTime(linha["dataVendaCabecalho"]);
                        vendaCabecalho.valorVendaCabecalho = Convert.ToDecimal(linha["valorVendaCabecalho"]);
                        vendaCabecalho.descontoVendaCabecalho = Convert.ToDecimal(linha["descontoVendaCabecalho"]);
                        vendaCabecalho.totalVendaCabecalho = Convert.ToDecimal(linha["totalVendaCabecalho"]);
                        vendaCabecalho.atendenteVendaCabecalho = linha["atendenteVendaCabecalho"].ToString().ToUpper();
                        vendaCabecalhoCollections.Add(vendaCabecalho);
                    }
                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto: " + ex.Message);
            }
        }

        public VendaCabecalhoCollections buscaPedPrint()
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

                acessoDadosSqlServer.limparParametros();

                DataTable dataTableVendaCabecalho = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from PrintPedTemp where statusVenda = 0");

                foreach (DataRow linha in dataTableVendaCabecalho.Rows)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();

                    vendaCabecalho.idTipoPagamento = Convert.ToInt32(linha["idVenda"]);
                    vendaCabecalho.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"].ToString().ToUpper());
                    vendaCabecalho.nomeAtendimento = linha["tipoAtendimento"].ToString().ToUpper();
                    vendaCabecalho.atendenteVendaCabecalho = linha["atendenteVenda"].ToString().ToUpper();
                    vendaCabecalho.statusVendaCabecalho = Convert.ToInt32(linha["statusVenda"].ToString().ToUpper());
                    
                    vendaCabecalhoCollections.Add(vendaCabecalho); 
                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Pegar os Pedidos. Detalhes: " + ex.Message);
            }
        }

        public string ExcluirPedPrint(int id)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                string idVendaDetalhes = acessoDadosSqlServer.ExecutaManipulação(CommandType.Text, "delete from PrintPedTemp where idVenda = " + id + "").ToString();

                return idVendaDetalhes;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public VendaCabecalhoCollections checaAtendTipo()
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

                acessoDadosSqlServer.limparParametros();
                
                DataTable dataTableVendaCabecalho = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaCabecalhoChecaAtendTipo");

                foreach (DataRow linha in dataTableVendaCabecalho.Rows)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();

                    vendaCabecalho.atendenteVendaCabecalho = linha["nomeAtendimento"].ToString();
                    vendaCabecalho.btn = linha["buton"].ToString();
                    
                    vendaCabecalhoCollections.Add(vendaCabecalho);
                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cliente por NOME. Detalhes: " + ex.Message);
            }
        }

        public string checaAtend(string atend)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@atendimentoVendaCabecalho", atend);

                string idVenda = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoChecaAtend").ToString();

                return idVenda;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public VendaCabecalhoCollections GetAtendenteVenda()
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.Text, "select atendenteVendaCabecalho from VendasCabecalho group by atendenteVendaCabecalho");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        VendaCabecalho vendaCabecalhoAtendente = new VendaCabecalho();

                        vendaCabecalhoAtendente.atendenteVendaCabecalho = linha["atendenteVendaCabecalho"].ToString().ToUpper();
                        vendaCabecalhoCollections.Add(vendaCabecalhoAtendente);
                    }

                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Atendente: " + ex.Message);
            }
        }
        
        public VendaCabecalhoCollections getTipoPagamento(int pedido)
        {
            try
            {
                VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", pedido);

                DataTable dataTableVendaCabecalho = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaCabecalhoGetTipoPagamento");

                foreach (DataRow linha in dataTableVendaCabecalho.Rows)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();

                    vendaCabecalho.descricaoVCTipoPagamento = linha["descricaoVCTipoPagamento"].ToString();
                    vendaCabecalho.valorVCTipoPagamento = Convert.ToDecimal(linha["valorVCTipoPagamento"]);

                    vendaCabecalhoCollections.Add(vendaCabecalho);
                }

                return vendaCabecalhoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o tipo de pagamento. Detalhes: " + ex.Message);
            }
        }
    }
}
