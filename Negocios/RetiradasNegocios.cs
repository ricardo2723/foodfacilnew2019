﻿using System;
using System.Data;
using AcessoBancoDados;
using System.Data.SqlClient;

using Negocios;
using ObjetoTransferencia;

namespace Negocios
{
    public class RetiradasNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Retiradas retiradas)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idUsuario", retiradas.idUsuario);
                acessoDadosSqlServer.AdicionarParametros("@idCaixaRetirada", retiradas.idCaixaRetirada);
                acessoDadosSqlServer.AdicionarParametros("@valorRetirada", retiradas.valorRetirada);
                acessoDadosSqlServer.AdicionarParametros("@dataRetirada", retiradas.dataRetirada);
                acessoDadosSqlServer.AdicionarParametros("@administradorRetirada", retiradas.administradorRetirada);
                acessoDadosSqlServer.AdicionarParametros("@motivoRetirada", retiradas.motivoRetirada);
                string idRetiradas = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspRetiradasCrudInserir").ToString();

                return idRetiradas;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public RetiradasCollections GetUsuarioRetirada()
        {
            try
            {
                RetiradasCollections retiradasCollections = new RetiradasCollections();

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.Text, "select Retirada.idUsuario, Usuarios.usuarioLogin from Retirada inner Join Usuarios on Retirada.idUsuario = Usuarios.usuarioId group by Retirada.idUsuario, Usuarios.usuarioLogin");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        Retiradas retiradas = new Retiradas();

                        retiradas.idUsuario = Convert.ToInt32(linha["idUsuario"]);
                        retiradas.nomeLoginUsuario = linha["usuarioLogin"].ToString().ToUpper();
                        retiradasCollections.Add(retiradas);
                    }

                }

                return retiradasCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Atendente: " + ex.Message);
            }
        }

    }
}
