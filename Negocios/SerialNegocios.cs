﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class SerialNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(string install, String active)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@serialInstalacao", install);
                acessoDadosSqlServer.AdicionarParametros("@serialAtivacao", active);

                string idSerial = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspSerialCrudInserir").ToString();

                return idSerial;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Serial serial)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@serialAtivacao", serial.serialAtivacao);
                string idSerial = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspSerialCrudAlterar").ToString();

                return idSerial;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public SerialCollections chegaSerial()
        {
            try
            {
                SerialCollections serialCollections = new SerialCollections();

                acessoDadosSqlServer.limparParametros();
               
                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "SELECT * FROM Serial");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Serial serial = new Serial();

                    serial.serialInstalacao = linha["serialInstalacao"].ToString();
                    serial.serialAtivacao = linha["serialAtivacao"].ToString().ToUpper();
                                        
                    serialCollections.Add(serial); 
                }

                return serialCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cartão. Detalhes: " + ex.Message);
            }
        }       
        
    }
}
