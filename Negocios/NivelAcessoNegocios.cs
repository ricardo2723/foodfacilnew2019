﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class NivelAcessoNegocios
    {
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(NivelAcesso nivelAcesso)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoDescricao", nivelAcesso.nivelAcessoDescricao);
                acessoDadosSqlServer.AdicionarParametros("@dataCadAcessoNivel", nivelAcesso.dataCadAcessoNivel);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", nivelAcesso.usuarioId);
                string idNivelAcesso = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspNivelAcessoCrudInserir").ToString();

                return idNivelAcesso;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Alterar(NivelAcesso nivelAcesso)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoId", nivelAcesso.nivelAcessoId);
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoDescricao", nivelAcesso.nivelAcessoDescricao);
                acessoDadosSqlServer.AdicionarParametros("@dataCadAcessoNivel", nivelAcesso.dataCadAcessoNivel);
                acessoDadosSqlServer.AdicionarParametros("@idUsuario", nivelAcesso.usuarioId);
                string idNivelAcesso = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspNivelAcessoCrudAlterar").ToString();

                return idNivelAcesso;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(NivelAcesso nivelAcesso)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoId", nivelAcesso.nivelAcessoId);
                string idNivelAcesso = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspNivelAcessoCrudExcluir").ToString();

                return idNivelAcesso;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public NivelAcessoCollections ConsultarNivelAcesso(string descr)
        {
            try
            {
                NivelAcessoCollections nivelAcessoCollections = new NivelAcessoCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoDescricao", descr);

                DataTable dataTableUsuarios = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspNivelAcessoListaGeral");

                foreach (DataRow linha in dataTableUsuarios.Rows)
                {
                    NivelAcesso nivelAcesso = new NivelAcesso();
                    nivelAcesso.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    nivelAcesso.nivelAcessoDescricao = linha["nivelAcessoDescricao"].ToString().ToUpper();
                    nivelAcesso.dataCadAcessoNivel = Convert.ToDateTime(linha["dataCadAcessoNivel"]);
                    nivelAcesso.usuarioId = Convert.ToInt32(linha["idUsuario"]);

                    nivelAcessoCollections.Add(nivelAcesso); 
                }

                return nivelAcessoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Nivel de Acesso. Detalhes: " + ex.Message);
            }
        }
    }
}
