﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class BairroNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Bairro bairro)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                
                acessoDadosSqlServer.AdicionarParametros("@nomeBairro", bairro.nomeBairro);
                acessoDadosSqlServer.AdicionarParametros("@taxaBairro", bairro.taxaBairro);
                acessoDadosSqlServer.AdicionarParametros("@dataCadBairro", bairro.dataCadBairro);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", bairro.usuarioId);
                string idBairro = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspBairroCrudInserir").ToString();

                return idBairro;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Bairro bairro)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idBairro", bairro.idBairro);
                acessoDadosSqlServer.AdicionarParametros("@nomeBairro", bairro.nomeBairro);
                acessoDadosSqlServer.AdicionarParametros("@taxaBairro", bairro.taxaBairro);
                acessoDadosSqlServer.AdicionarParametros("@dataCadBairro", bairro.dataCadBairro);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", bairro.usuarioId);
                string idBairro = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspBairroCrudAlterar").ToString();

                return idBairro;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Bairro bairro)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idBairro", bairro.idBairro);
                string idBairro = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspBairroCrudExcluir").ToString();

                return idBairro;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public BairroCollections ConsultarNome(string descr)
        {
            try
            {
                BairroCollections bairroCollections = new BairroCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeBairro", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspBairroListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Bairro bairro = new Bairro();

                    bairro.idBairro = Convert.ToInt32(linha["idBairro"]);
                    bairro.nomeBairro = linha["nomeBairro"].ToString().ToUpper();
                    bairro.taxaBairro = Convert.ToDecimal(linha["taxaBairro"].ToString());
                    bairro.dataCadBairro = Convert.ToDateTime(linha["dataCadBairro"]);
                    bairro.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    bairroCollections.Add(bairro); 
                }

                return bairroCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cartão. Detalhes: " + ex.Message);
            }
        }       
        
        public decimal ConsultarTaxaBairro(string descr)
        {
            decimal taxaBairro = 0;

            try
            {
                BairroCollections bairroCollections = new BairroCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeBairro", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspBairroGetValor");

                foreach (DataRow linha in dataTableExtras.Rows)
                {                    
                    taxaBairro = Convert.ToDecimal(linha["taxaBairro"].ToString());
                }

                return taxaBairro;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Bairro. Detalhes: " + ex.Message);
            }
        }       
        

    }
}
