﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class PlanoContaNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(PlanoConta planoConta)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@codPlanoConta", planoConta.codPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@descricaoPlanoConta", planoConta.descricaoPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@dataCadPlanoConta", planoConta.dataCadPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", planoConta.usuarioId);
                string idPlanoConta = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspPlanoContaCrudInserir").ToString();

                return idPlanoConta;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(PlanoConta planoConta)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@idPlanoConta", planoConta.idPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@codPlanoConta", planoConta.codPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@descricaoPlanoConta", planoConta.descricaoPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@dataCadPlanoConta", planoConta.dataCadPlanoConta);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", planoConta.usuarioId);
                string idPlanoConta = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspPlanoContaCrudAlterar").ToString();

                return idPlanoConta;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(PlanoConta planoConta)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idPlanoConta", planoConta.idPlanoConta);
                string idPlanoConta = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspPlanoContaCrudExcluir").ToString();

                return idPlanoConta;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public PlanoContaCollections ConsultarDescricao(string descr)
        {
            try
            {
                PlanoContaCollections planoContaCollections = new PlanoContaCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@descricaoPlanoConta", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspPlanoContaListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    PlanoConta planoConta = new PlanoConta();

                    planoConta.idPlanoConta = Convert.ToInt32(linha["idPlanoConta"]);
                    planoConta.codPlanoConta = Convert.ToInt32(linha["codPlanoConta"]);
                    planoConta.descricaoPlanoConta = linha["descricaoPlanoConta"].ToString().ToUpper();
                    planoConta.dataCadPlanoConta = Convert.ToDateTime(linha["dataCadPlanoConta"]);
                    planoConta.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    planoContaCollections.Add(planoConta); 
                }

                return planoContaCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Plano de Contas. Detalhes: " + ex.Message);
            }
        }

        public PlanoContaCollections ConsultarCod(int cod)
        {
            try
            {
                PlanoContaCollections planoContaCollections = new PlanoContaCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codPlanoConta", cod);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspPlanoContaListaCodigo");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    PlanoConta planoConta = new PlanoConta();

                    planoConta.idPlanoConta = Convert.ToInt32(linha["idPlanoConta"]);
                    planoConta.codPlanoConta = Convert.ToInt32(linha["codPlanoConta"]);
                    planoConta.descricaoPlanoConta = linha["descricaoPlanoConta"].ToString().ToUpper();
                    planoConta.dataCadPlanoConta = Convert.ToDateTime(linha["dataCadPlanoConta"]);
                    planoConta.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    planoContaCollections.Add(planoConta);
                }

                return planoContaCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Plano de Contas. Detalhes: " + ex.Message);
            }
        }       
        
    }
}
