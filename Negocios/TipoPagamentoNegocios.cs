﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class TipoPagamentoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(TipoPagamento tipoPagamento)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@descricaoTipoPagamento", tipoPagamento.descricaoTipoPagamento);
                acessoDadosSqlServer.AdicionarParametros("@dataCadTipoPagamento", tipoPagamento.dataCadTipoPagamento);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", tipoPagamento.usuarioId);
                string idTipoPagamento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspTipoPagamentoCrudInserir").ToString();

                return idTipoPagamento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(TipoPagamento tipoPagamento)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idTipoPagamento", tipoPagamento.idTipoPagamento);
                acessoDadosSqlServer.AdicionarParametros("@descricaoTipoPagamento", tipoPagamento.descricaoTipoPagamento);
                acessoDadosSqlServer.AdicionarParametros("@dataCadTipoPagamento", tipoPagamento.dataCadTipoPagamento);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", tipoPagamento.usuarioId);
                string idTipoPagamento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspTipoPagamentoCrudAlterar").ToString();

                return idTipoPagamento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(TipoPagamento tipoPagamento)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idTipoPagamento", tipoPagamento.idTipoPagamento);
                string idTipoPagamento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspTipoPagamentoCrudExcluir").ToString();

                return idTipoPagamento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public TipoPagamentoCollections ConsultarNome(string descr)
        {
            try
            {
                TipoPagamentoCollections tipoPagamentoCollections = new TipoPagamentoCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@descricaoTipoPagamento", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspTipoPagamentoListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    TipoPagamento tipoPagamento = new TipoPagamento();

                    tipoPagamento.idTipoPagamento = Convert.ToInt32(linha["idTipoPagamento"]);
                    tipoPagamento.descricaoTipoPagamento = linha["descricaoTipoPagamento"].ToString().ToUpper();
                    tipoPagamento.dataCadTipoPagamento = Convert.ToDateTime(linha["dataCadTipoPagamento"]);
                    tipoPagamento.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    tipoPagamentoCollections.Add(tipoPagamento); 
                }

                return tipoPagamentoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Tipo de Pagamento. Detalhes: " + ex.Message);
            }
        }       
        
    }
}
