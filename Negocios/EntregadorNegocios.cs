﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class EntregadorNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Entregador entregador)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeEntregador", entregador.nomeEntregador);
                acessoDadosSqlServer.AdicionarParametros("@apelidoEntregador", entregador.apelidoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@cpfEntregador", entregador.cpfEntregador);
                acessoDadosSqlServer.AdicionarParametros("@statusEntregador", entregador.statusEntregador);
                acessoDadosSqlServer.AdicionarParametros("@identidadeEntregador", entregador.identidadeEntregador);
                acessoDadosSqlServer.AdicionarParametros("@telefoneEntregador", entregador.telefoneEntregador);
                acessoDadosSqlServer.AdicionarParametros("@celularEntregador", entregador.celularEntregador);
                acessoDadosSqlServer.AdicionarParametros("@cepEntregador", entregador.cepEntregador);
                acessoDadosSqlServer.AdicionarParametros("@enderecoEntregador", entregador.enderecoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@complementoEntregador", entregador.complementoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@bairroEntregador", entregador.bairroEntregador);
                acessoDadosSqlServer.AdicionarParametros("@municipioEntregador", entregador.municipioEntregador);
                acessoDadosSqlServer.AdicionarParametros("@ufEntregador", entregador.ufEntregador);
                acessoDadosSqlServer.AdicionarParametros("@emailEntregador", entregador.emailEntregador);
                acessoDadosSqlServer.AdicionarParametros("@dataNascimentoEntregador", entregador.dataNascimentoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroEntregador", entregador.dataCadastroEntregador);
                acessoDadosSqlServer.AdicionarParametros("@porcentEntregador", entregador.porcentEntregador);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", entregador.usuarioId);

                string idEntregador = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEntregadorCrudInserir").ToString();

                return idEntregador;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Entregador entregador)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idEntregador",              entregador.idEntregador);
                acessoDadosSqlServer.AdicionarParametros("@nomeEntregador",            entregador.nomeEntregador);
                acessoDadosSqlServer.AdicionarParametros("@apelidoEntregador",         entregador.apelidoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@cpfEntregador",             entregador.cpfEntregador);
                acessoDadosSqlServer.AdicionarParametros("@statusEntregador",          entregador.statusEntregador);
                acessoDadosSqlServer.AdicionarParametros("@identidadeEntregador",      entregador.identidadeEntregador);
                acessoDadosSqlServer.AdicionarParametros("@telefoneEntregador",        entregador.telefoneEntregador);
                acessoDadosSqlServer.AdicionarParametros("@celularEntregador",         entregador.celularEntregador);
                acessoDadosSqlServer.AdicionarParametros("@cepEntregador",             entregador.cepEntregador);
                acessoDadosSqlServer.AdicionarParametros("@enderecoEntregador",        entregador.enderecoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@complementoEntregador",     entregador.complementoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@bairroEntregador",          entregador.bairroEntregador);
                acessoDadosSqlServer.AdicionarParametros("@municipioEntregador",       entregador.municipioEntregador);
                acessoDadosSqlServer.AdicionarParametros("@ufEntregador",              entregador.ufEntregador);
                acessoDadosSqlServer.AdicionarParametros("@emailEntregador",           entregador.emailEntregador);
                acessoDadosSqlServer.AdicionarParametros("@dataNascimentoEntregador",  entregador.dataNascimentoEntregador);
                acessoDadosSqlServer.AdicionarParametros("@porcentEntregador",         entregador.porcentEntregador);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId",                 entregador.usuarioId);

                string idEntregador = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEntregadorCrudAlterar").ToString();

                return idEntregador;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Entregador entregador)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idEntregador", entregador.idEntregador);
                string idEntregador = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspEntregadorCrudExcluir").ToString();

                return idEntregador;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public EntregadorCollections ConsultarPorNome(string nome)
        {
            try
            {
                EntregadorCollections entregadorCollections = new EntregadorCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeEntregador", nome);

                DataTable dataTableEntregador =  acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspEntregadorListaNome");

                foreach (DataRow linha in dataTableEntregador.Rows)
                {
                    Entregador entregador = new Entregador();
                    
                    entregador.idEntregador = Convert.ToInt32(linha["idEntregador"]);
                    entregador.nomeEntregador = linha["nomeEntregador"].ToString().ToUpper();
                    entregador.apelidoEntregador = linha["apelidoEntregador"].ToString().ToUpper();
                    entregador.cpfEntregador = linha["cpfEntregador"].ToString().ToUpper();
                    entregador.identidadeEntregador = linha["identidadeEntregador"].ToString().ToUpper();
                    entregador.telefoneEntregador = linha["telefoneEntregador"].ToString().ToUpper();
                    entregador.celularEntregador = linha["celularEntregador"].ToString().ToUpper();
                    entregador.cepEntregador = linha["cepEntregador"].ToString().ToUpper();
                    entregador.enderecoEntregador = linha["enderecoEntregador"].ToString().ToUpper();
                    entregador.complementoEntregador = linha["complementoEntregador"].ToString().ToUpper();
                    entregador.bairroEntregador = linha["bairroEntregador"].ToString().ToUpper();
                    entregador.municipioEntregador = linha["municipioEntregador"].ToString().ToUpper();
                    entregador.ufEntregador = linha["ufEntregador"].ToString().ToUpper();
                    entregador.emailEntregador = linha["emailEntregador"].ToString().ToUpper();
                    entregador.dataNascimentoEntregador = Convert.ToDateTime(linha["dataNascimentoEntregador"]);
                    entregador.dataCadastroEntregador = Convert.ToDateTime(linha["dataCadastroEntregador"]);
                    entregador.porcentEntregador = Convert.ToDecimal(linha["porcentEntregador"]);
                    entregador.statusEntregador =  Convert.ToBoolean(linha["statusEntregador"]);
                    entregador.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    entregadorCollections.Add(entregador); 
                }

                return entregadorCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Entregador por NOME. Detalhes: " + ex.Message);
            }
        }

        public EntregadorCollections ConsultarPorApelido(string nome)
        {
            try
            {
                EntregadorCollections entregadorCollections = new EntregadorCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@apelidoEntregador", nome);

                DataTable dataTableEntregador = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspEntregadorListaApelido");

                foreach (DataRow linha in dataTableEntregador.Rows)
                {
                    Entregador entregador = new Entregador();

                    entregador.idEntregador = Convert.ToInt32(linha["idEntregador"]);
                    entregador.nomeEntregador = linha["nomeEntregador"].ToString().ToUpper();
                    entregador.apelidoEntregador = linha["apelidoEntregador"].ToString().ToUpper();
                    entregador.cpfEntregador = linha["cpfEntregador"].ToString().ToUpper();
                    entregador.identidadeEntregador = linha["identidadeEntregador"].ToString().ToUpper();
                    entregador.telefoneEntregador = linha["telefoneEntregador"].ToString().ToUpper();
                    entregador.celularEntregador = linha["celularEntregador"].ToString().ToUpper();
                    entregador.cepEntregador = linha["cepEntregador"].ToString().ToUpper();
                    entregador.enderecoEntregador = linha["enderecoEntregador"].ToString().ToUpper();
                    entregador.complementoEntregador = linha["complementoEntregador"].ToString().ToUpper();
                    entregador.bairroEntregador = linha["bairroEntregador"].ToString().ToUpper();
                    entregador.municipioEntregador = linha["municipioEntregador"].ToString().ToUpper();
                    entregador.ufEntregador = linha["ufEntregador"].ToString().ToUpper();
                    entregador.emailEntregador = linha["emailEntregador"].ToString().ToUpper();
                    entregador.dataNascimentoEntregador = Convert.ToDateTime(linha["dataNascimentoEntregador"]);
                    entregador.dataCadastroEntregador = Convert.ToDateTime(linha["dataCadastroEntregador"]);
                    entregador.porcentEntregador = Convert.ToDecimal(linha["porcentEntregador"]);
                    entregador.statusEntregador = Convert.ToBoolean(linha["statusEntregador"]);
                    entregador.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    entregadorCollections.Add(entregador);
                }

                return entregadorCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Entregador por NOME. Detalhes: " + ex.Message);
            }
        }

        public EntregadorCollections ConsultarPorCpf(string cpf)
        {
            try
            {
                EntregadorCollections entregadorCollections = new EntregadorCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@cpfEntregador", cpf);

                DataTable dataTableEntregador = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspEntregadorListaCPF");

                foreach (DataRow linha in dataTableEntregador.Rows)
                {
                    Entregador entregador = new Entregador();

                    entregador.idEntregador = Convert.ToInt32(linha["idEntregador"]);
                    entregador.nomeEntregador = linha["nomeEntregador"].ToString().ToUpper();
                    entregador.apelidoEntregador = linha["apelidoEntregador"].ToString().ToUpper();
                    entregador.cpfEntregador = linha["cpfEntregador"].ToString().ToUpper();
                    entregador.identidadeEntregador = linha["identidadeEntregador"].ToString().ToUpper();
                    entregador.telefoneEntregador = linha["telefoneEntregador"].ToString().ToUpper();
                    entregador.celularEntregador = linha["celularEntregador"].ToString().ToUpper();
                    entregador.cepEntregador = linha["cepEntregador"].ToString().ToUpper();
                    entregador.enderecoEntregador = linha["enderecoEntregador"].ToString().ToUpper();
                    entregador.complementoEntregador = linha["complementoEntregador"].ToString().ToUpper();
                    entregador.bairroEntregador = linha["bairroEntregador"].ToString().ToUpper();
                    entregador.municipioEntregador = linha["municipioEntregador"].ToString().ToUpper();
                    entregador.ufEntregador = linha["ufEntregador"].ToString().ToUpper();
                    entregador.emailEntregador = linha["emailEntregador"].ToString().ToUpper();
                    entregador.dataNascimentoEntregador = Convert.ToDateTime(linha["dataNascimentoEntregador"]);
                    entregador.dataCadastroEntregador = Convert.ToDateTime(linha["dataCadastroEntregador"]);
                    entregador.porcentEntregador = Convert.ToDecimal(linha["porcentEntregador"]);
                    entregador.statusEntregador = Convert.ToBoolean(linha["statusEntregador"]);
                    entregador.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    entregadorCollections.Add(entregador);
                }

                return entregadorCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Entregador por CPF. Detalhes: " + ex.Message);
            }
        }
                
    }
}
