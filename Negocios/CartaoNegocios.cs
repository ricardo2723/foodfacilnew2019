﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class CartaoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Cartao cartao)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                
                acessoDadosSqlServer.AdicionarParametros("@nomeCartao", cartao.nomeCartao);
                acessoDadosSqlServer.AdicionarParametros("@taxaCartao", cartao.taxaCartao);
                acessoDadosSqlServer.AdicionarParametros("@dataCadCartao", cartao.dataCadCartao);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", cartao.usuarioId);
                string idCartao = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspCartaoCrudInserir").ToString();

                return idCartao;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Cartao cartao)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idCartao", cartao.idCartao);
                acessoDadosSqlServer.AdicionarParametros("@nomeCartao", cartao.nomeCartao);
                acessoDadosSqlServer.AdicionarParametros("@taxaCartao", cartao.taxaCartao);
                acessoDadosSqlServer.AdicionarParametros("@dataCadCartao", cartao.dataCadCartao);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", cartao.usuarioId);
                string idCartao = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspCartaoCrudAlterar").ToString();

                return idCartao;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Cartao cartao)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idCartao", cartao.idCartao);
                string idCartao = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspCartaoCrudExcluir").ToString();

                return idCartao;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public CartaoCollections ConsultarNome(string descr)
        {
            try
            {
                CartaoCollections cartaoCollections = new CartaoCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nomeCartao", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspCartaoListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Cartao cartao = new Cartao();

                    cartao.idCartao = Convert.ToInt32(linha["idCartao"]);
                    cartao.nomeCartao = linha["nomeCartao"].ToString().ToUpper();
                    cartao.taxaCartao = Convert.ToDecimal(linha["taxaCartao"].ToString());
                    cartao.dataCadCartao = Convert.ToDateTime(linha["dataCadCartao"]);
                    cartao.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    cartaoCollections.Add(cartao); 
                }

                return cartaoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Cartão. Detalhes: " + ex.Message);
            }
        }       
        
    }
}
