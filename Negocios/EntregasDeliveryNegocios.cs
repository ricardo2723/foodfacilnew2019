﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class EntregasDeliveryNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        //public string Alterar(VendaCabecalhoDelivery vendaCabecalhoDelivery)
        //{
        //    try
        //    {
        //        acessoDadosSqlServer.limparParametros();
        //        acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalhoDelivery", vendaCabecalhoDelivery.idVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@nomeVendaCabecalhoDelivery", vendaCabecalhoDelivery.nomeVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@apelidoVendaCabecalhoDelivery", vendaCabecalhoDelivery.apelidoVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@cpfVendaCabecalhoDelivery", vendaCabecalhoDelivery.cpfVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@identidadeVendaCabecalhoDelivery", vendaCabecalhoDelivery.identidadeVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@telefoneVendaCabecalhoDelivery", vendaCabecalhoDelivery.telefoneVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@celularVendaCabecalhoDelivery", vendaCabecalhoDelivery.celularVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@cepVendaCabecalhoDelivery", vendaCabecalhoDelivery.cepVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@enderecoVendaCabecalhoDelivery", vendaCabecalhoDelivery.enderecoVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@complementoVendaCabecalhoDelivery", vendaCabecalhoDelivery.complementoVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@bairroVendaCabecalhoDelivery", vendaCabecalhoDelivery.bairroVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@municipioVendaCabecalhoDelivery", vendaCabecalhoDelivery.municipioVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@ufVendaCabecalhoDelivery", vendaCabecalhoDelivery.ufVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@emailVendaCabecalhoDelivery", vendaCabecalhoDelivery.emailVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@dataNascimentoVendaCabecalhoDelivery", vendaCabecalhoDelivery.dataNascimentoVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@porcentVendaCabecalhoDelivery", vendaCabecalhoDelivery.porcentVendaCabecalhoDelivery);
        //        acessoDadosSqlServer.AdicionarParametros("@usuarioId", vendaCabecalhoDelivery.usuarioId);

        //        string idVendaCabecalhoDelivery = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspVendaCabecalhoDeliveryCrudAlterar").ToString();

        //        return idVendaCabecalhoDelivery;
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}

        public VendaCabecalhoDeliveryCollections ConsultarPedidosPorStatus(int status)
        {
            try
            {
                VendaCabecalhoDeliveryCollections vendaCabecalhoDeliveryCollections = new VendaCabecalhoDeliveryCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@statusEntrega", status);

                DataTable dataTableVendaCabecalhoDelivery = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaCabecalhoDeliveryStatus");

                foreach (DataRow linha in dataTableVendaCabecalhoDelivery.Rows)
                {
                    VendaCabecalhoDelivery vendaCabecalhoDelivery = new VendaCabecalhoDelivery();

                    vendaCabecalhoDelivery.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"]);
                    vendaCabecalhoDelivery.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();
                    
                    vendaCabecalhoDelivery.telefoneCliente = linha["telefoneCliente"].ToString();
                    if(vendaCabecalhoDelivery.telefoneCliente != "")
                        vendaCabecalhoDelivery.telefoneCliente = linha["telefoneCliente"].ToString().Substring(2);

                    vendaCabecalhoDelivery.celularCliente = linha["celularCliente"].ToString();
                    if (vendaCabecalhoDelivery.celularCliente != "")
                        vendaCabecalhoDelivery.celularCliente = linha["celularCliente"].ToString().Substring(2);

                    vendaCabecalhoDelivery.horaPedido = Convert.ToDateTime(linha["horaVendaCabecalho"].ToString());
                    vendaCabecalhoDelivery.statusEntrega = Convert.ToInt32(linha["statusEntrega"]);
                    vendaCabecalhoDelivery.atendente = linha["atendenteVendaCabecalho"].ToString().ToUpper();
                    vendaCabecalhoDelivery.totaDelivery = linha["totalVendaCabecalho"].ToString().ToUpper();

                    vendaCabecalhoDeliveryCollections.Add(vendaCabecalhoDelivery);
                }

                return vendaCabecalhoDeliveryCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar as Entregas. Detalhes: " + ex.Message);
            }
        }

        public VendaCabecalhoDeliveryCollections ConsultarPedidosSaiuEntrega(int status)
        {
            try
            {
                VendaCabecalhoDeliveryCollections vendaCabecalhoDeliveryCollections = new VendaCabecalhoDeliveryCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@statusEntrega", status);

                DataTable dataTableVendaCabecalhoDelivery = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaCabecalhoDeliverySaiuEntrega");

                foreach (DataRow linha in dataTableVendaCabecalhoDelivery.Rows)
                {
                    VendaCabecalhoDelivery vendaCabecalhoDelivery = new VendaCabecalhoDelivery();

                    vendaCabecalhoDelivery.idVendaCabecalho = Convert.ToInt32(linha["idVendaCabecalho"]);
                    vendaCabecalhoDelivery.nomeCliente = linha["nomeCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.enderecoCliente = linha["enderecoCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.complementoCliente = linha["complementoCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.bairroCliente = linha["bairroCliente"].ToString().ToUpper();
                    vendaCabecalhoDelivery.pontoReferenciaCliente = linha["pontoReferenciaCliente"].ToString().ToUpper();

                    vendaCabecalhoDelivery.telefoneCliente = linha["telefoneCliente"].ToString();
                    if (vendaCabecalhoDelivery.telefoneCliente != "")
                        vendaCabecalhoDelivery.telefoneCliente = linha["telefoneCliente"].ToString().Substring(2);

                    vendaCabecalhoDelivery.celularCliente = linha["celularCliente"].ToString();
                    if (vendaCabecalhoDelivery.celularCliente != "")
                        vendaCabecalhoDelivery.celularCliente = linha["celularCliente"].ToString().Substring(2);
                    
                    vendaCabecalhoDelivery.horaPedido = Convert.ToDateTime(linha["dataVendaCabecalho"]);
                    vendaCabecalhoDelivery.horaSaida = Convert.ToDateTime(linha["horaSaida"]);
                    vendaCabecalhoDelivery.apelidoEntregador = linha["apelidoEntregador"].ToString().ToUpper();
                    vendaCabecalhoDelivery.statusEntrega = Convert.ToInt32(linha["statusEntrega"]);
                    vendaCabecalhoDelivery.atendente = linha["atendenteVendaCabecalho"].ToString().ToUpper();
                    vendaCabecalhoDelivery.totaDelivery = linha["totalVendaCabecalho"].ToString().ToUpper();

                    vendaCabecalhoDeliveryCollections.Add(vendaCabecalhoDelivery);
                }

                return vendaCabecalhoDeliveryCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar as Entregas. Detalhes: " + ex.Message);
            }
        }

        public VendaDetalhesCollections pegarProduto(int idPedido)
        {
            try
            {
                VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@idVendaCabecalho", idPedido);

                DataTable dataTableVendaCabecalhoDelivery = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspVendaCabecalhoDeliveryPegarProdutos");

                foreach (DataRow linha in dataTableVendaCabecalhoDelivery.Rows)
                {
                    VendaDetalhes vendaDetalhes = new VendaDetalhes();

                    vendaDetalhes.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    vendaDetalhes.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(linha["quantidadeVendaDetalhes"]);
                    vendaDetalhes.especialVendaDetalhes = checaNull(linha["especialVendaDetalhes"].ToString().ToUpper());
                    vendaDetalhes.extraVendaDetalhes = checaNull(linha["extraVendaDetalhes"].ToString().ToUpper());

                    vendaDetalhesCollections.Add(vendaDetalhes);
                }

                return vendaDetalhesCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto. Detalhes: " + ex.Message);
            }
        }

        private string checaNull(string nulo)
        {
            if (nulo == "")
            {
                nulo = "Não Possui";
                return nulo;
            }
            else
            {
                return nulo;
            }
        }
    }
}
