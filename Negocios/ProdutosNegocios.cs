﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace Negocios
{
    public class ProdutosNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Produtos produtos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", produtos.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@undMedidaId", produtos.undMedidaId);
                acessoDadosSqlServer.AdicionarParametros("@idFornecedor", produtos.idFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@grupoId", produtos.grupoId);
                acessoDadosSqlServer.AdicionarParametros("@descricaoProduto", produtos.descricaoProduto);
                acessoDadosSqlServer.AdicionarParametros("@valorCompraProduto", produtos.valorCompraProduto);
                acessoDadosSqlServer.AdicionarParametros("@valorVendaProduto", produtos.valorVendaProduto);
                acessoDadosSqlServer.AdicionarParametros("@estoqueProduto", produtos.estoqueProduto);
                acessoDadosSqlServer.AdicionarParametros("@estoqueCriticoProduto", produtos.estoqueCriticoProduto);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroProduto", produtos.dataCadastroProduto);
                acessoDadosSqlServer.AdicionarParametros("@controlaEstoqueProduto", produtos.controlaEstoqueProduto);
                acessoDadosSqlServer.AdicionarParametros("@manufaturadoProduto", produtos.manufaturadoProduto);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", produtos.usuarioId);
                acessoDadosSqlServer.AdicionarParametros("@tipoProduto", produtos.tipoProduto);

                string idProdutos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspProdutosCrudInserir").ToString();

                return idProdutos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string inserirManufaturado(Produtos produtos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codProduto", produtos.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@codProdutoManufaturado", produtos.codProdutoManufaturado);
                acessoDadosSqlServer.AdicionarParametros("@quantidadeProdutoManufaturado", produtos.quantidadeProdutoManufaturado);

                string codProdutos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspProdutosManufaturadoCrudInserir").ToString();

                return codProdutos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Alterar(Produtos produtos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", produtos.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@undMedidaId", produtos.undMedidaId);
                acessoDadosSqlServer.AdicionarParametros("@idFornecedor", produtos.idFornecedor);
                acessoDadosSqlServer.AdicionarParametros("@grupoId", produtos.grupoId);
                acessoDadosSqlServer.AdicionarParametros("@descricaoProduto", produtos.descricaoProduto);
                acessoDadosSqlServer.AdicionarParametros("@valorCompraProduto", produtos.valorCompraProduto);
                acessoDadosSqlServer.AdicionarParametros("@valorVendaProduto", produtos.valorVendaProduto);
                acessoDadosSqlServer.AdicionarParametros("@estoqueProduto", produtos.estoqueProduto);
                acessoDadosSqlServer.AdicionarParametros("@estoqueCriticoProduto", produtos.estoqueCriticoProduto);
                acessoDadosSqlServer.AdicionarParametros("@dataCadastroProduto", DateTime.Now);
                acessoDadosSqlServer.AdicionarParametros("@controlaEstoqueProduto", produtos.controlaEstoqueProduto);
                acessoDadosSqlServer.AdicionarParametros("@manufaturadoProduto", produtos.manufaturadoProduto);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", produtos.usuarioId);

                string codigoProduto = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspProdutosCrudAlterar").ToString();

                return codigoProduto;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Produtos produtos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", produtos.codigoProduto);
                string idProduto = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspProdutosCrudExcluir").ToString();

                return idProduto;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ExcluirManufaturado(Produtos produtos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codProduto", produtos.codigoProduto);
                string idProduto = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspProdutosManufaturadoCrudExcluir").ToString();

                return idProduto;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ExcluirItemManufaturado(Produtos produtos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codProduto", produtos.codigoProduto);
                acessoDadosSqlServer.AdicionarParametros("@codProdutoManufaturado", produtos.codProdutoManufaturado);

                string idProduto = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspProdutosManufaturadoCrudExcluirItem").ToString();

                return idProduto;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ProdutosCollections GetProduto(Produtos produtos)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", produtos.codProdutoManufaturado);
                acessoDadosSqlServer.AdicionarParametros("@tipoProduto", produtos.tipoProduto);

                SqlDataReader datdos = acessoDadosSqlServer.ExecutarConsultaReader(CommandType.StoredProcedure, "uspProdutosListaCod");

                if (datdos.HasRows)
                {
                    foreach (IDataRecord linha in datdos)
                    {
                        Produtos produto = new Produtos();

                        produto.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                        produto.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                        produto.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);

                        produtosCollections.Add(produto);
                    }

                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto: " + ex.Message);
            }
        }

        public ProdutosCollections ConsultarPorDrescricao(string descr, char tipoProduto)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@descricaoProduto", descr);
                acessoDadosSqlServer.AdicionarParametros("@tipoProduto", tipoProduto);

                DataTable dataTableProdutos = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspProdutosListaNome");

                foreach (DataRow linha in dataTableProdutos.Rows)
                {
                    Produtos produtos = new Produtos();

                    produtos.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    produtos.undMedidaId = Convert.ToInt32(linha["undMedidaId"]);
                    produtos.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    produtos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    produtos.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    produtos.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                    produtos.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                    produtos.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);
                    produtos.estoqueCriticoProduto = Convert.ToDecimal(linha["estoqueCriticoProduto"]);
                    produtos.dataCadastroProduto = Convert.ToDateTime(linha["dataCadastroProduto"]);
                    produtos.controlaEstoqueProduto = Convert.ToChar(linha["controlaEstoqueProduto"]);
                    produtos.manufaturadoProduto = Convert.ToChar(linha["manufaturadoProduto"]);
                    produtos.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    produtos.descricaoGrupo = linha["grupoDescricao"].ToString().ToUpper();
                    produtos.descricaoUndMedida = linha["undMedidaDescricao"].ToString().ToUpper();
                    produtos.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    produtos.tipoProduto = Convert.ToChar(linha["tipoProduto"].ToString());
                    produtosCollections.Add(produtos);
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto pela DESCRIÇÃO. Detalhes: " + ex.Message);
            }
        }

        public ProdutosCollections ConsultarPorDrescricaoExtra(string descr)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@descricaoProduto", descr);

                DataTable dataTableProdutos = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspProdutosListaNomeExtra");

                foreach (DataRow linha in dataTableProdutos.Rows)
                {
                    Produtos produtos = new Produtos();

                    produtos.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    produtos.undMedidaId = Convert.ToInt32(linha["undMedidaId"]);
                    produtos.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    produtos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    produtos.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    produtos.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                    produtos.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                    produtos.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);
                    produtos.estoqueCriticoProduto = Convert.ToDecimal(linha["estoqueCriticoProduto"]);
                    produtos.dataCadastroProduto = Convert.ToDateTime(linha["dataCadastroProduto"]);
                    produtos.controlaEstoqueProduto = Convert.ToChar(linha["controlaEstoqueProduto"]);
                    produtos.manufaturadoProduto = Convert.ToChar(linha["manufaturadoProduto"]);
                    produtos.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    produtos.descricaoGrupo = linha["grupoDescricao"].ToString().ToUpper();
                    produtos.descricaoUndMedida = linha["undMedidaDescricao"].ToString().ToUpper();
                    produtos.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    produtos.tipoProduto = Convert.ToChar(linha["tipoProduto"].ToString());
                    produtosCollections.Add(produtos);
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto pela DESCRIÇÃO. Detalhes: " + ex.Message);
            }
        }

        public ProdutosCollections ConsultarPorCodigo(string cod, char tipoProduto)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@codigoProduto", cod);
                acessoDadosSqlServer.AdicionarParametros("@tipoProduto", tipoProduto);

                DataTable dataTableProdutos = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspProdutosListaCodigo");

                foreach (DataRow linha in dataTableProdutos.Rows)
                {
                    Produtos produtos = new Produtos();

                    produtos.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    produtos.undMedidaId = Convert.ToInt32(linha["undMedidaId"]);
                    produtos.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    produtos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    produtos.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    produtos.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                    produtos.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                    produtos.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);
                    produtos.estoqueCriticoProduto = Convert.ToDecimal(linha["estoqueCriticoProduto"]);
                    produtos.dataCadastroProduto = Convert.ToDateTime(linha["dataCadastroProduto"]);
                    produtos.controlaEstoqueProduto = Convert.ToChar(linha["controlaEstoqueProduto"]);
                    produtos.manufaturadoProduto = Convert.ToChar(linha["manufaturadoProduto"]);
                    produtos.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    produtos.descricaoGrupo = linha["grupoDescricao"].ToString().ToUpper();
                    produtos.descricaoUndMedida = linha["undMedidaDescricao"].ToString().ToUpper();
                    produtos.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    produtos.tipoProduto = Convert.ToChar(linha["tipoProduto"].ToString());

                    produtosCollections.Add(produtos);
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto por CÓDIGO. Detalhes: " + ex.Message);
            }
        }

        public ProdutosCollections ConsultarPorManufaturadoCodigo(int cod, char tipoProduto)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                acessoDadosSqlServer.limparParametros();

                DataTable dataTableProdutos = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_ProdutosManufaturadoListaCod where codProduto = " + cod + "tipoProduto = " + tipoProduto);

                foreach (DataRow linha in dataTableProdutos.Rows)
                {
                    Produtos produtos = new Produtos();

                    produtos.codProdutoManufaturado = Convert.ToInt32(linha["codProdutoManufaturado"]);
                    produtos.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    produtos.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                    produtos.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                    produtos.quantidadeProdutoManufaturado = Convert.ToDecimal(linha["quantidadeProdutoManufaturado"]);
                    produtos.estoqueProduto = Convert.ToInt32(linha["estoqueProduto"]);

                    produtosCollections.Add(produtos);
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto por CÓDIGO. Detalhes: " + ex.Message);
            }
        }

        public ProdutosCollections ConsultarPorGrupo(string grupo, char tipoProduto)
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@grupoDescricao", grupo);
                acessoDadosSqlServer.AdicionarParametros("@tipoProduto", tipoProduto);

                DataTable dataTableProdutos = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspProdutosListaGrupo");

                foreach (DataRow linha in dataTableProdutos.Rows)
                {
                    Produtos produtos = new Produtos();

                    produtos.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    produtos.undMedidaId = Convert.ToInt32(linha["undMedidaId"]);
                    produtos.idFornecedor = Convert.ToInt32(linha["idFornecedor"]);
                    produtos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    produtos.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    produtos.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                    produtos.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                    produtos.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);
                    produtos.estoqueCriticoProduto = Convert.ToDecimal(linha["estoqueCriticoProduto"]);
                    produtos.dataCadastroProduto = Convert.ToDateTime(linha["dataCadastroProduto"]);
                    produtos.controlaEstoqueProduto = Convert.ToChar(linha["controlaEstoqueProduto"]);
                    produtos.manufaturadoProduto = Convert.ToChar(linha["manufaturadoProduto"]);
                    produtos.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    produtos.descricaoGrupo = linha["grupoDescricao"].ToString().ToUpper();
                    produtos.descricaoUndMedida = linha["undMedidaDescricao"].ToString().ToUpper();
                    produtos.nomeFornecedor = linha["nomeFornecedor"].ToString().ToUpper();
                    produtos.tipoProduto = Convert.ToChar(linha["tipoProduto"].ToString());

                    produtosCollections.Add(produtos);
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto pelo GRUPO. Detalhes: " + ex.Message);
            }
        }

        public ProdutosCollections ConsultarProdutoExtra()
        {
            try
            {
                ProdutosCollections produtosCollections = new ProdutosCollections();

                DataTable dataTableProdutos = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from Produto where tipoProduto = 'E'order by descricaoProduto");

                foreach (DataRow linha in dataTableProdutos.Rows)
                {
                    Produtos produtos = new Produtos();

                    produtos.codigoProduto = Convert.ToInt32(linha["codigoProduto"]);
                    produtos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    produtos.descricaoProduto = linha["descricaoProduto"].ToString().ToUpper();
                    produtos.valorCompraProduto = Convert.ToDecimal(linha["valorCompraProduto"]);
                    produtos.valorVendaProduto = Convert.ToDecimal(linha["valorVendaProduto"]);
                    produtos.estoqueProduto = Convert.ToDecimal(linha["estoqueProduto"]);
                    produtos.estoqueCriticoProduto = Convert.ToDecimal(linha["estoqueCriticoProduto"]);
                    produtos.controlaEstoqueProduto = Convert.ToChar(linha["controlaEstoqueProduto"]);
                    produtos.manufaturadoProduto = Convert.ToChar(linha["manufaturadoProduto"]);
                    produtos.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    produtos.tipoProduto = Convert.ToChar(linha["tipoProduto"].ToString());

                    produtosCollections.Add(produtos);
                }

                return produtosCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Produto pelo GRUPO. Detalhes: " + ex.Message);
            }
        }

    }
}
