﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class PermissaoAcessoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(PermissaoAcesso permissaoAcesso)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();

                acessoDadosSqlServer.AdicionarParametros("@permissaoAcessoId", permissaoAcesso.permissaoAcessoId);
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoId", permissaoAcesso.nivelAcessoId);
                acessoDadosSqlServer.AdicionarParametros("@buton", permissaoAcesso.buton);
                acessoDadosSqlServer.AdicionarParametros("@ativo", permissaoAcesso.ativo);
                acessoDadosSqlServer.AdicionarParametros("@cadastrar", permissaoAcesso.cadastrar);
                acessoDadosSqlServer.AdicionarParametros("@alterar", permissaoAcesso.alterar);
                acessoDadosSqlServer.AdicionarParametros("@excluir", permissaoAcesso.excluir);
                acessoDadosSqlServer.AdicionarParametros("@pesquisar", permissaoAcesso.pesquisar);
                acessoDadosSqlServer.AdicionarParametros("@relatorios", permissaoAcesso.relatorios);
                acessoDadosSqlServer.AdicionarParametros("@graficos", permissaoAcesso.graficos);
                acessoDadosSqlServer.AdicionarParametros("@financeiro", permissaoAcesso.financeiro);

                string idPermissaoAcesso = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspPermissaoAcessoCrudInserir").ToString();

                return idPermissaoAcesso;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public PermissaoAcessoCollections Consultar(int id, string buton)
        {
            try
            {
                PermissaoAcessoCollections permissaoAcessoCollections = new PermissaoAcessoCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@nivelAcessoId", id);
                acessoDadosSqlServer.AdicionarParametros("@buton", buton);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspPermissaoAcessoListaId");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    PermissaoAcesso permissaoAcesso = new PermissaoAcesso();

                    permissaoAcesso.permissaoAcessoId = Convert.ToInt32(linha["permissaoAcessoId"]);
                    permissaoAcesso.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    permissaoAcesso.buton = linha["buton"].ToString().ToUpper();
                    permissaoAcesso.ativo = Convert.ToBoolean(linha["ativo"]);
                    permissaoAcesso.cadastrar = Convert.ToBoolean(linha["cadastrar"]);
                    permissaoAcesso.alterar = Convert.ToBoolean(linha["alterar"]);
                    permissaoAcesso.excluir = Convert.ToBoolean(linha["excluir"]);
                    permissaoAcesso.pesquisar = Convert.ToBoolean(linha["pesquisar"]);
                    permissaoAcesso.financeiro = Convert.ToBoolean(linha["financeiro"]);
                    permissaoAcesso.relatorios = Convert.ToBoolean(linha["relatorios"]);
                    permissaoAcesso.graficos = Convert.ToBoolean(linha["graficos"]);
                    
                    permissaoAcessoCollections.Add(permissaoAcesso); 
                }

                return permissaoAcessoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Grupo. Detalhes: " + ex.Message);
            }
        }

        public PermissaoAcessoCollections ConsultarNivelId(int id)
        {
            try
            {
                PermissaoAcessoCollections permissaoAcessoCollections = new PermissaoAcessoCollections();

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "SELECT * FROM PermissaoAcesso where nivelAcessoId =" + id);

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    PermissaoAcesso permissaoAcesso = new PermissaoAcesso();

                    permissaoAcesso.permissaoAcessoId = Convert.ToInt32(linha["permissaoAcessoId"]);
                    permissaoAcesso.nivelAcessoId = Convert.ToInt32(linha["nivelAcessoId"]);
                    permissaoAcesso.buton = linha["buton"].ToString().ToUpper();
                    permissaoAcesso.ativo = Convert.ToBoolean(linha["ativo"]);
                    permissaoAcesso.cadastrar = Convert.ToBoolean(linha["cadastrar"]);
                    permissaoAcesso.alterar = Convert.ToBoolean(linha["alterar"]);
                    permissaoAcesso.excluir = Convert.ToBoolean(linha["excluir"]);
                    permissaoAcesso.pesquisar = Convert.ToBoolean(linha["pesquisar"]);
                    permissaoAcesso.financeiro = Convert.ToBoolean(linha["financeiro"]);
                    permissaoAcesso.relatorios = Convert.ToBoolean(linha["relatorios"]);
                    permissaoAcesso.graficos = Convert.ToBoolean(linha["graficos"]);

                    permissaoAcessoCollections.Add(permissaoAcesso);
                }

                return permissaoAcessoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Grupo. Detalhes: " + ex.Message);
            }
        }

    }
}
