﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class FormaPagamentoNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(FormaPagamento formaPagamento)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoDescricao", formaPagamento.formaPagamentoDescricao);
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoDataCad", formaPagamento.formaPagamentoDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", formaPagamento.usuarioId);
                string idFormaPagamento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspFormaPagamentoCrudInserir").ToString();

                return idFormaPagamento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(FormaPagamento formaPagamento)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoId", formaPagamento.formaPagamentoId);
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoDescricao", formaPagamento.formaPagamentoDescricao);
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoDataCad", formaPagamento.formaPagamentoDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", formaPagamento.usuarioId);
                string idFormaPagamento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspFormaPagamentoCrudAlterar").ToString();

                return idFormaPagamento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(FormaPagamento formaPagamento)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoId", formaPagamento.formaPagamentoId);
                string idFormaPagamento = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspFormaPagamentoCrudExcluir").ToString();

                return idFormaPagamento;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public FormaPagamentoCollections ConsultarDescricao(string descr)
        {
            try
            {
                FormaPagamentoCollections formaPagamentoCollections = new FormaPagamentoCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@formaPagamentoDescricao", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspFormaPagamentoListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    FormaPagamento formaPagamento = new FormaPagamento();

                    formaPagamento.formaPagamentoId = Convert.ToInt32(linha["formaPagamentoId"]);
                    formaPagamento.formaPagamentoDescricao = linha["formaPagamentoDescricao"].ToString().ToUpper();
                    formaPagamento.formaPagamentoDataCad = Convert.ToDateTime(linha["formaPagamentoDataCad"]);
                    formaPagamento.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    formaPagamentoCollections.Add(formaPagamento); 
                }

                return formaPagamentoCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Grupo. Detalhes: " + ex.Message);
            }
        }

    }
}
