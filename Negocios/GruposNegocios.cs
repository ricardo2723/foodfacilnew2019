﻿using System;
using System.Data;
using AcessoBancoDados;
using ObjetoTransferencia;

namespace Negocios
{
    public class GruposNegocios
    {
        //Criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        public string inserir(Grupos grupos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@grupoDescricao", grupos.grupoDescricao);
                acessoDadosSqlServer.AdicionarParametros("@grupoDataCad", grupos.grupoDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", grupos.usuarioId);
                string idGrupos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspGruposCrudInserir").ToString();

                return idGrupos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }                        
        }

        public string Alterar(Grupos grupos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@grupoId", grupos.grupoId);
                acessoDadosSqlServer.AdicionarParametros("@grupoDescricao", grupos.grupoDescricao);
                acessoDadosSqlServer.AdicionarParametros("@grupoDataCad", grupos.grupoDataCad);
                acessoDadosSqlServer.AdicionarParametros("@usuarioId", grupos.usuarioId);
                string idGrupos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspGruposCrudAlterar").ToString();

                return idGrupos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Excluir(Grupos grupos)
        {
            try
            {
                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@grupoId", grupos.grupoId);
                string idGrupos = acessoDadosSqlServer.ExecutaManipulação(CommandType.StoredProcedure, "uspGruposCrudExcluir").ToString();

                return idGrupos;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public GruposCollections ConsultarDescricao(string descr)
        {
            try
            {
                GruposCollections gruposCollections = new GruposCollections();

                acessoDadosSqlServer.limparParametros();
                acessoDadosSqlServer.AdicionarParametros("@grupoDescricao", descr);

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "uspGruposListaNome");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Grupos grupos = new Grupos();

                    grupos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    grupos.grupoDescricao = linha["grupoDescricao"].ToString().ToUpper();
                    grupos.grupoDataCad = Convert.ToDateTime(linha["grupoDataCad"]);
                    grupos.usuarioId = Convert.ToInt32(linha["usuarioId"]);
                    
                    gruposCollections.Add(grupos); 
                }

                return gruposCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Grupo. Detalhes: " + ex.Message);
            }
        }

        public GruposCollections PreencheCbGrupos()
        {
            try
            {
                GruposCollections gruposCollections = new GruposCollections();

                acessoDadosSqlServer.limparParametros();

                DataTable dataTableExtras = acessoDadosSqlServer.ExecutarConsulta(CommandType.Text, "select * from vw_GruposLista");

                foreach (DataRow linha in dataTableExtras.Rows)
                {
                    Grupos grupos = new Grupos();

                    grupos.grupoId = Convert.ToInt32(linha["grupoId"]);
                    grupos.grupoDescricao = linha["grupoDescricao"].ToString().ToUpper();
                    grupos.grupoDataCad = Convert.ToDateTime(linha["grupoDataCad"]);
                    grupos.usuarioId = Convert.ToInt32(linha["usuarioId"]);

                    gruposCollections.Add(grupos);
                }

                return gruposCollections;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel Consultar o Grupo. Detalhes: " + ex.Message);
            }
        }
        
    }
}
