USE [master]
GO
/****** Object:  Database [lancheFacil]    Script Date: 12/12/2014 20:40:20 ******/
CREATE DATABASE [lancheFacil]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'lancheFacil', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\lancheFacil.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'lancheFacil_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\lancheFacil_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [lancheFacil] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [lancheFacil].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [lancheFacil] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [lancheFacil] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [lancheFacil] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [lancheFacil] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [lancheFacil] SET ARITHABORT OFF 
GO
ALTER DATABASE [lancheFacil] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [lancheFacil] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [lancheFacil] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [lancheFacil] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [lancheFacil] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [lancheFacil] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [lancheFacil] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [lancheFacil] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [lancheFacil] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [lancheFacil] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [lancheFacil] SET  DISABLE_BROKER 
GO
ALTER DATABASE [lancheFacil] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [lancheFacil] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [lancheFacil] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [lancheFacil] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [lancheFacil] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [lancheFacil] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [lancheFacil] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [lancheFacil] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [lancheFacil] SET  MULTI_USER 
GO
ALTER DATABASE [lancheFacil] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [lancheFacil] SET DB_CHAINING OFF 
GO
ALTER DATABASE [lancheFacil] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [lancheFacil] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [lancheFacil]
GO
/****** Object:  User [ricardo]    Script Date: 12/12/2014 20:40:20 ******/
CREATE USER [ricardo] FOR LOGIN [ricardo] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [ricardo]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [ricardo]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [ricardo]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [ricardo]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [ricardo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ricardo]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [ricardo]
GO
/****** Object:  StoredProcedure [dbo].[uspBackupDb]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspBackupDb]
	@localDb varchar(250)
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
		backup database lancheFacil to disk = @localDb
				
		select 1 as retorno
				
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspBairroCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspBairroCrudAlterar]
	@idBairro	int,
	@nomeBairro	varchar(50),
	@taxaBairro	decimal(5,0),
	@dataCadBairro	datetime,
	@usuarioId	int
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			
			IF(EXISTS(SELECT idBairro FROM Bairro WHERE nomeBairro = @nomeBairro))
				RAISERROR('O Bairro ja está cadastrado no Bando de Dados.',14,1);

			UPDATE 
				Bairro
			SET		
				nomeBairro = @nomeBairro,
				taxaBairro = @taxaBairro,	
				dataCadBairro = @dataCadBairro,
				usuarioId = @usuarioId
						
			WHERE
				idBairro = @idBairro	
			
			SELECT @idBairro AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspBairroCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspBairroCrudExcluir]
	@idBairro int
AS
BEGIN
	
	DELETE FROM
		Bairro
	WHERE
		idBairro = @idBairro
	
	SELECT @idBairro AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspBairroCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspBairroCrudInserir]
	@nomeBairro	varchar(50),
	@taxaBairro	decimal(5,1),
	@dataCadBairro datetime,
	@usuarioId	int		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT idBairro FROM Bairro WHERE nomeBairro = @nomeBairro))
				RAISERROR('O Bairro ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Bairro
			(
				nomeBairro,
				taxaBairro,
				dataCadBairro,
				usuarioId
			)
			VALUES
			(
				@nomeBairro,
				@taxaBairro,
				@dataCadBairro,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspBairroGetValor]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspBairroGetValor]
	@nomeBairro	varchar(30)
AS
BEGIN
	SELECT 
		taxaBairro		
	FROM 
		Bairro 
	WHERE 
		nomeBairro = @nomeBairro 
	END



GO
/****** Object:  StoredProcedure [dbo].[uspBairroListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspBairroListaNome]
	@nomeBairro	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Bairro 
	WHERE 
		nomeBairro LIKE '%' + @nomeBairro + '%' ORDER BY nomeBairro ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspCaixaChecaCaixa]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspCaixaChecaCaixa]
	@dataCaixa date
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
		declare @date1 date;
		
		SET @date1 =(select dateadd(day,1,@dataCaixa))
		
			IF(EXISTS(SELECT idCaixa FROM Caixa WHERE dataCaixa >= @dataCaixa and dataCaixa < @date1 and statusCaixa = 1))
				RAISERROR('0',14,1);
						
			SELECT 1 AS Retorno;
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspCaixaCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspCaixaCrudAlterar]
	@idCaixa	int,
	@valorCaixaFecha	decimal(18,2),
	@horaCaixaFecha	datetime,
	@statusCaixa int
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Caixa
			SET		
				valorCaixaFecha = @valorCaixaFecha,
				horaCaixaFecha = @horaCaixaFecha,	
				statusCaixa = @statusCaixa
						
			WHERE
				idCaixa = @idCaixa	
			
			SELECT @idCaixa AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspCaixaCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspCaixaCrudInserir]
	@valorCaixaAbre	decimal(18, 2),
	@valorCaixaFecha	decimal(18, 2) = 0,
	@dataCaixa	dateTime,
	@horaCaixaAbre	time(7),
	--@horaCaixaFecha	time(7) = null,
	@statusCaixa	int,
	@usuarioId	int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO Caixa
			(
				valorCaixaAbre,
				valorCaixaFecha,
				dataCaixa,
				horaCaixaAbre,
				--horaCaixaFecha,
				statusCaixa,
				usuarioId
			)
			VALUES
			(
				@valorCaixaAbre,
				@valorCaixaFecha,
				@dataCaixa,
				@horaCaixaAbre,	
				--@horaCaixaFecha,	
				@statusCaixa,	
				@usuarioId		
				
			)
			
			SELECT @@IDENTITY AS Retorno;			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspCaixaListaCaixa]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspCaixaListaCaixa]
	@dataCaixa date
AS
BEGIN
		declare @date1 date;
		
		SET @date1 =(select dateadd(day,1,@dataCaixa))
		
	select 
		idCaixa, valorCaixaAbre, dataCaixa, horaCaixaAbre, usuarioLogin 
	from
		Caixa 
	join 
		Usuarios
	on
		Caixa.usuarioId = Usuarios.usuarioId
	where
		dataCaixa >= @dataCaixa and dataCaixa < @date1 and statusCaixa = 1
END



GO
/****** Object:  StoredProcedure [dbo].[uspCaixaPagamentosCaixa]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspCaixaPagamentosCaixa]
	@tipo varchar(1),
	--@data datetime,
	@hora date
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
		--declare @date1 date;
		
		--SET @date1 =(select dateadd(day,1,@data))
		
		IF(@tipo = 1) 
			begin
				select
					SUM(valorVCTipoPagamento) as total, descricaoVCTipoPagamento 
				from 
					VendaCabecalhoTipoPagamentos
				join
					VendasCabecalho
				on
					idVendaCabecalho = idVendaCabecalhoTipoPagamento
				where
					--dataVendaCabecalho > @hora and dataVendaCabecalho >= @data and dataVendaCabecalho < @date1 and VendasCabecalho.statusVendaCabecalho = 2
					dataVendaCabecalho = @hora and VendasCabecalho.statusVendaCabecalho = 2
				group by
					descricaoVCTipoPagamento 
			end
		else if(@tipo = 2)
			begin
				select
					SUM(descontoVendaCabecalho) as totalDesconto 
				from 
					VendasCabecalho
				where
					--dataVendaCabecalho >= @data and dataVendaCabecalho < @date1 and VendasCabecalho.statusVendaCabecalho = 2
					dataVendaCabecalho = @hora and VendasCabecalho.statusVendaCabecalho = 2
			end
			
		else if(@tipo =3)
			begin
				select
					SUM(totalVendaCabecalho) as totalDelivery 
				from 
					VendasCabecalho
				where
					--dataVendaCabecalho > @hora and dataVendaCabecalho >= @data and dataVendaCabecalho < @date1 and VendasCabecalho.statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
					dataVendaCabecalho = @hora and VendasCabecalho.statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
			end
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspCartaoCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspCartaoCrudAlterar]
	@idCartao	int,
	@nomeCartao	varchar(30),
	@taxaCartao	decimal(5,0),
	@dataCadCartao	datetime,
	@usuarioId	int
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Cartao
			SET		
				nomeCartao = @nomeCartao,
				taxaCartao = @taxaCartao,	
				dataCadCartao = @dataCadCartao,
				usuarioId = @usuarioId
						
			WHERE
				idCartao = @idCartao	
			
			SELECT @idCartao AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspCartaoCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspCartaoCrudExcluir]
	@idCartao int
AS
BEGIN
	
	DELETE FROM
		Cartao
	WHERE
		idCartao = @idCartao
	
	SELECT @idCartao AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspCartaoCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspCartaoCrudInserir]
	@nomeCartao	varchar(30),
	@taxaCartao	decimal(5,1),
	@dataCadCartao datetime,
	@usuarioId	int		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT idCartao FROM Cartao WHERE nomeCartao = @nomeCartao))
				RAISERROR('O Cartão ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Cartao
			(
				nomeCartao,
				taxaCartao,
				dataCadCartao,
				usuarioId
			)
			VALUES
			(
				@nomeCartao,
				@taxaCartao,
				@dataCadCartao,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspCartaoListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspCartaoListaNome]
	@nomeCartao	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Cartao 
	WHERE 
		nomeCartao LIKE '%' + @nomeCartao + '%' ORDER BY nomeCartao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspClientesCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClientesCrudAlterar]
	@idCliente	int,
	@nomeCliente	varchar(200),
	@cpfCliente	varchar(11),
	@cnpjCliente	varchar(15),
	@identidadeCliente	varchar(11),
	@telefoneCliente	varchar(11),
	@celularCliente	varchar(11),
	@cepCliente	varchar(11),
	@enderecoCliente	varchar(200),
	@complementoCliente	varchar(100),
	@bairroCliente	varchar(100),
	@municipioCliente	varchar(50),
	@ufCliente	varchar(2),
	@emailCliente	varchar(100),
	@dataNascimentoCliente	datetime,
	@pontoReferenciaCliente	varchar(255),
	@dataCadastroCliente	datetime,
	@usuarioId	int
	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Clientes
			SET		
				nomeCliente =  @nomeCliente,        
				cpfCliente =  @cpfCliente,
				cnpjCliente =  @cnpjCliente,
				identidadeCliente =  @identidadeCliente,
				telefoneCliente =  @telefoneCliente,
				celularCliente =  @celularCliente,
				cepCliente =  @cepCliente,
				enderecoCliente =  @enderecoCliente,
				complementoCliente =  @complementoCliente,
				bairroCliente =  @bairroCliente,
				municipioCliente =  @municipioCliente,
				ufCliente =  @ufCliente,
				emailCliente =  @emailCliente,
				dataNascimentoCliente =  	@dataNascimentoCliente,
				pontoReferenciaCliente	 =  @pontoReferenciaCliente,
				dataCadastroCliente =  @dataCadastroCliente,
				usuarioId = @usuarioId
						
			WHERE
				idCliente = @idCliente	
			
			SELECT @idCliente AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspClientesCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspClientesCrudExcluir]
	@idCliente int
AS
BEGIN
	
	DELETE FROM
		Clientes
	WHERE
		idCliente = @idCliente
	
	SELECT @idCliente AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspClientesCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClientesCrudInserir]
	@nomeCliente	varchar(200),
	@cpfCliente	varchar(11),
	@cnpjCliente	varchar(15),
	@identidadeCliente	varchar(11),
	@telefoneCliente	varchar(11),
	@celularCliente	varchar(11),
	@cepCliente	varchar(11),
	@enderecoCliente	varchar(200),
	@complementoCliente	varchar(100),
	@bairroCliente	varchar(100),
	@municipioCliente	varchar(50),
	@ufCliente	varchar(2),
	@emailCliente	varchar(100),
	@dataNascimentoCliente	datetime,
	@pontoReferenciaCliente	varchar(255),
	@dataCadastroCliente	datetime,
	@usuarioId	int	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO Clientes
			(
				nomeCliente,
				cpfCliente,
				cnpjCliente,
				identidadeCliente,
				telefoneCliente,
				celularCliente,
				cepCliente,
				enderecoCliente,
				complementoCliente,
				bairroCliente,
				municipioCliente,
				ufCliente,
				emailCliente,
				dataNascimentoCliente,
				pontoReferenciaCliente,
				dataCadastroCliente,
				usuarioId
			)
			VALUES
			(
				@nomeCliente,
				@cpfCliente,
				@cnpjCliente,
				@identidadeCliente,
				@telefoneCliente,
				@celularCliente,
				@cepCliente,
				@enderecoCliente,
				@complementoCliente,
				@bairroCliente,
				@municipioCliente,
				@ufCliente,
				@emailCliente,
				@dataNascimentoCliente,
				@pontoReferenciaCliente,
				@dataCadastroCliente,
				@usuarioId	
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspClientesListaCnpj]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspClientesListaCnpj]
	@cnpjCliente	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Clientes
	WHERE 
		cnpjCliente LIKE '%' + @cnpjCliente + '%' ORDER BY cnpjCliente ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspClientesListaCpf]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspClientesListaCpf]
	@cpfCliente	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Clientes
	WHERE 
		cpfCliente LIKE '%' + @cpfCliente + '%' ORDER BY cpfCliente ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspClientesListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspClientesListaNome]
	@nomeCliente	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Clientes
	WHERE 
		nomeCliente LIKE '%' + @nomeCliente + '%' ORDER BY nomeCliente ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspClientesListaTelefone]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClientesListaTelefone]
	@telefoneCliente	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Clientes
	WHERE 
		telefoneCliente LIKE '%' + @telefoneCliente + '%' or celularCliente LIKE '%' + @telefoneCliente + '%'
END



GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspContasPagarCrudAlterar]
	@idContaPagar int,
	@idPlanoConta	int,
	@nomeContaPagar	varchar(100),
	@valorContaPagar	decimal(18, 2),
	@vencimentoContaPagar	date,
	@statusContaPagar bigint,
	@dataCadContaPagar datetime,
	@usuarioId int
	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				ContasPagar
			SET		
				
				idPlanoConta  =  @idPlanoConta,	
				nomeContaPagar  = @nomeContaPagar,
				valorContaPagar  = @valorContaPagar,
				vencimentoContaPagar  = @vencimentoContaPagar,
				statusContaPagar  = @statusContaPagar,
				dataCadContaPagar  = @dataCadContaPagar,
				usuarioId  = @usuarioId
						
			WHERE
				idContaPagar = @idContaPagar
			
			SELECT @idContaPagar AS retorno

		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspContasPagarCrudInserir]
	@idPlanoConta	int,
	@nomeContaPagar	varchar(100),
	@valorContaPagar	decimal(18, 2),
	@vencimentoContaPagar	date,
	@statusContaPagar bigint,
	@dataCadContaPagar datetime,
	@usuarioId int
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO ContasPagar
			(
				idPlanoConta,
				nomeContaPagar,
				valorContaPagar,
				vencimentoContaPagar,
				statusContaPagar,
				dataCadContaPagar,
				usuarioId	
			)
			VALUES
			(
				@idPlanoConta,
				@nomeContaPagar,
				@valorContaPagar,
				@vencimentoContaPagar,
				@statusContaPagar,
				@dataCadContaPagar,
				@usuarioId
			)
			
			SELECT @@IDENTITY AS retorno;
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarCrudInserirPagamento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspContasPagarCrudInserirPagamento]
	@idContaPagar	int,
	@dataPagamentoContaPagas date,
	@valorPagoContaPagas decimal(18, 2),
	@dataCadContaPagas	datetime,
	@usuarioId int
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO ContasPagas
			(
				idContaPagar,
				dataPagamentoContaPagas,
				valorPagoContaPagas,
				dataCadContaPagas,
				usuarioId
			)
			VALUES
			(
				@idContaPagar,
				@dataPagamentoContaPagas,
				@valorPagoContaPagas,
				@dataCadContaPagas,
				@usuarioId
			)
			
			SELECT @@IDENTITY AS retorno;
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarListaContasPagas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspContasPagarListaContasPagas]
	@idContaPagar	int
AS
BEGIN
	SELECT 
		valorPagoContaPagas, dataPagamentoContaPagas	
	FROM 
		ContasPagas
	WHERE 
		idContaPagar = @idContaPagar
END



GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarListaData]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspContasPagarListaData]
	@vencimentoContaPagar date
AS
BEGIN

	select 
		ContasPagar.*, codPlanoConta
	from
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	where
		vencimentoContaPagar = @vencimentoContaPagar
	order by 
		nomeContaPagar
END



GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarListaStatus]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspContasPagarListaStatus]
	@statusContaPagar bigint
AS
BEGIN
	SELECT 
		ContasPagar.*, codPlanoConta		
	FROM 
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	WHERE 
		statusContaPagar = @statusContaPagar ORDER BY vencimentoContaPagar ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarListaTipo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspContasPagarListaTipo]
	@idPlanoConta	int
AS
BEGIN
	SELECT 
		ContasPagar.*, codPlanoConta	
	FROM 
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	WHERE 
		ContasPagar.idPlanoConta = @idPlanoConta ORDER BY nomeContaPagar ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspContasPagarListaTipoCodigo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspContasPagarListaTipoCodigo]
	@codPlanoConta int
AS
BEGIN
	SELECT 
		ContasPagar.*, codPlanoConta	
	FROM 
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	WHERE 
		PlanoConta.codPlanoConta = @codPlanoConta ORDER BY nomeContaPagar ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspEntradaProdutosCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntradaProdutosCrudInserir]
	@codigoProduto	int,
	@motivoEntradaProduto	varchar(100),
	@nfEntradaProduto	varchar(30),
	@dataCadEntradaProduto	datetime,
	@quantidadeProdutoEntrada	decimal(18, 2),
	@usuarioId	int,
	@valorCompraProduto decimal(18,2),
	@valorVendaProduto decimal(18,2),
	@estoqueProduto decimal(18,2)					
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO EntradaProduto
			(
				codigoProduto,
				quantidadeProdutoEntrada,
				motivoEntradaProduto,	
				nfEntradaProduto,
				dataCadEntradaProduto,	
				usuarioId	
			)
			VALUES
			(
				@codigoProduto,
				@quantidadeProdutoEntrada,
				@motivoEntradaProduto,
				@nfEntradaProduto,
				@dataCadEntradaProduto,
				@usuarioId
			)
			
			SELECT @@IDENTITY AS retorno;
			
			UPDATE 
				Produto
			SET		
				valorCompraProduto = @valorCompraProduto,
				valorVendaProduto =	@valorVendaProduto,
				estoqueProduto = @estoqueProduto,
				usuarioId = @usuarioId	
						
			WHERE
				codigoProduto = @codigoProduto	
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspEntradaProdutosListaCod]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspEntradaProdutosListaCod]
	@codigoProduto	int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				SELECT 
					descricaoProduto,
					valorCompraProduto,
					valorVendaProduto,
					estoqueProduto
				FROM 
					Produto 
				WHERE 
					codigoProduto = @codigoProduto
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntregadorCrudAlterar]
	@idEntregador	int,
	@nomeEntregador	varchar(200),
	@apelidoEntregador varchar (20),
	@cpfEntregador	varchar(11),
	@statusEntregador bigint,
	@identidadeEntregador	varchar(11),
	@telefoneEntregador	varchar(11),
	@celularEntregador	varchar(11),
	@cepEntregador	varchar(11),
	@enderecoEntregador	varchar(200),
	@complementoEntregador	varchar(100),
	@bairroEntregador	varchar(100),
	@municipioEntregador	varchar(50),
	@ufEntregador	varchar(2),
	@emailEntregador	varchar(100),
	@dataNascimentoEntregador	datetime,
	@porcentEntregador decimal(18,2),
	@usuarioId	int	
	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Entregador
			SET	
				nomeEntregador			    = @nomeEntregador,
				apelidoEntregador			= @apelidoEntregador,
				cpfEntregador				= @cpfEntregador,
				identidadeEntregador		= @identidadeEntregador,
				telefoneEntregador			= @telefoneEntregador,
				celularEntregador			= @celularEntregador,
				cepEntregador				= @cepEntregador,
				enderecoEntregador			= @enderecoEntregador,
				complementoEntregador		= @complementoEntregador,
				bairroEntregador			= @bairroEntregador,
				municipioEntregador			= @municipioEntregador,
				ufEntregador				= @ufEntregador,
				emailEntregador				= @emailEntregador,
				dataNascimentoEntregador	= @dataNascimentoEntregador,
				porcentEntregador			= @porcentEntregador,
				statusEntregador			= @statusEntregador,
				usuarioId					= @usuarioId	
						
			WHERE
				idEntregador = @idEntregador	
			
			SELECT @idEntregador AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntregadorCrudExcluir]
	@idEntregador int
AS
BEGIN
	
	DELETE FROM
		Entregador
	WHERE
		idEntregador = @idEntregador
	
	SELECT @idEntregador AS retorno
END





GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntregadorCrudInserir]
	@nomeEntregador	varchar(200),
	@apelidoEntregador varchar (20),
	@cpfEntregador	varchar(11),
	@statusEntregador bigint,
	@identidadeEntregador	varchar(11),
	@telefoneEntregador	varchar(11),
	@celularEntregador	varchar(11),
	@cepEntregador	varchar(11),
	@enderecoEntregador	varchar(200),
	@complementoEntregador	varchar(100),
	@bairroEntregador	varchar(100),
	@municipioEntregador	varchar(50),
	@ufEntregador	varchar(2),
	@emailEntregador	varchar(100),
	@dataNascimentoEntregador	datetime,
	@porcentEntregador decimal(18,2),
	@dataCadastroEntregador	datetime,
	@usuarioId	int	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO Entregador
			(
				nomeEntregador,
				apelidoEntregador,
				cpfEntregador,
				identidadeEntregador,
				telefoneEntregador,
				celularEntregador,
				cepEntregador,
				enderecoEntregador,
				complementoEntregador,
				bairroEntregador,
				municipioEntregador,
				ufEntregador,
				emailEntregador,
				dataNascimentoEntregador,
				porcentEntregador,
				statusEntregador,
				dataCadastroEntregador,
				usuarioId
			)
			VALUES
			(
				@nomeEntregador,
				@apelidoEntregador,
				@cpfEntregador,
				@identidadeEntregador,
				@telefoneEntregador,
				@celularEntregador,
				@cepEntregador,
				@enderecoEntregador,
				@complementoEntregador,
				@bairroEntregador,
				@municipioEntregador,
				@ufEntregador,
				@emailEntregador,
				@dataNascimentoEntregador,
				@porcentEntregador,
				@statusEntregador,
				@dataCadastroEntregador,
				@usuarioId	
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorListaApelido]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspEntregadorListaApelido]
	@apelidoEntregador	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Entregador
	WHERE 
		apelidoEntregador LIKE '%' + @apelidoEntregador + '%' ORDER BY apelidoEntregador ASC
END




GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorListaCodigo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntregadorListaCodigo]
	@idEntregador	int
AS
BEGIN
	SELECT 
		*		
	FROM 
		Entregador
	WHERE 
		idEntregador = @idEntregador
END




GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorListaCpf]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntregadorListaCpf]
	@cpfEntregador	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Entregador
	WHERE 
		cpfEntregador LIKE '%' + @cpfEntregador + '%' ORDER BY cpfEntregador ASC
END




GO
/****** Object:  StoredProcedure [dbo].[uspEntregadorListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEntregadorListaNome]
	@nomeEntregador	varchar(30)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Entregador
	WHERE 
		nomeEntregador LIKE '%' + @nomeEntregador + '%' ORDER BY nomeEntregador ASC
END




GO
/****** Object:  StoredProcedure [dbo].[uspEspeciaisCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspEspeciaisCrudAlterar]
	@especiaisId int,
	@especiaisDescricao	varchar(50),
	@especiaisDataCad datetime,
	@usuarioId int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Especiais
			SET		
				especiaisDescricao = @especiaisDescricao,
				especiaisDataCad = @especiaisDataCad,	
				usuarioId = @usuarioId
						
			WHERE
				especiaisId = @especiaisId	
			
			SELECT @especiaisId AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspEspeciaisCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspEspeciaisCrudExcluir]
	@especiaisId int
AS
BEGIN
	
	DELETE FROM
		Especiais
	WHERE
		especiaisId = @especiaisId
	
	SELECT @especiaisId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspEspeciaisCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspEspeciaisCrudInserir]
	@especiaisDescricao	varchar(50),
	@especiaisDataCad datetime,
	@usuarioId int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT especiaisId FROM Especiais WHERE especiaisDescricao = @especiaisDescricao))
				RAISERROR('O Especial ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Especiais
			(
				especiaisDescricao,
				especiaisDataCad,
				usuarioId
			)
			VALUES
			(
				@especiaisDescricao,
				@especiaisDataCad,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspEspeciaisListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspEspeciaisListaNome]
	@especiaisDescricao varchar(50) = null
AS
BEGIN
	SELECT 
		*		
	FROM 
		Especiais 
	WHERE 
		especiaisDescricao LIKE '%' + @especiaisDescricao + '%' ORDER BY especiaisDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspFormaPagamentoCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspFormaPagamentoCrudAlterar]
	@formaPagamentoId int,
	@formaPagamentoDescricao	varchar(50),
	@formaPagamentoDataCad datetime,
	@usuarioId int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				FormaPagamento
			SET		
				formaPagamentoDescricao = @formaPagamentoDescricao,
				formaPagamentoDataCad = @formaPagamentoDataCad,	
				usuarioId = @usuarioId
						
			WHERE
				formaPagamentoId = @formaPagamentoId	
			
			SELECT @formaPagamentoId AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspFormaPagamentoCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspFormaPagamentoCrudExcluir]
	@formaPagamentoId int
AS
BEGIN
	
	DELETE FROM
		FormaPagamento
	WHERE
		formaPagamentoId = @formaPagamentoId
	
	SELECT @formaPagamentoId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspFormaPagamentoCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspFormaPagamentoCrudInserir]
	@formaPagamentoDescricao	varchar(50),
	@formaPagamentoDataCad datetime,
	@usuarioId int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT formaPagamentoId FROM formaPagamento WHERE formaPagamentoDescricao = @formaPagamentoDescricao))
				RAISERROR('O FormaPagamento ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO FormaPagamento
			(
				formaPagamentoDescricao,
				formaPagamentoDataCad,
				usuarioId
			)
			VALUES
			(
				@formaPagamentoDescricao,
				@formaPagamentoDataCad,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspFormaPagamentoListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspFormaPagamentoListaNome]
	@formaPagamentoDescricao varchar(50) = null
AS
BEGIN
	SELECT 
		*		
	FROM 
		FormaPagamento 
	WHERE 
		formaPagamentoDescricao LIKE '%' + @formaPagamentoDescricao + '%' ORDER BY formaPagamentoDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspFornecedoresCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspFornecedoresCrudAlterar]
	@idFornecedor					int,
	@nomeFornecedor					varchar(200),
	@cnpjFornecedor					varchar(15) = null,
	@cpfFornecedor					varchar(11) = null,
	@inscricaoEstadualFornecedor	varchar(14),
	@telefoneFornecedor				varchar(11),
	@faxFornecedor					varchar(11),
	@enderecoFornecedor				varchar(200),
	@complementoFornecedor			varchar(100),
	@bairroFornecedor				varchar(100),
	@cepFornecedor					varchar(11),
	@municipioFornecedor			varchar(50),
	@ufFornecedor					varchar(2),
	@emailFornecedor				varchar(100),
	@contatoFornecedor				varchar(50),
	@dataCadastroFornecedor			datetime,
	@pontoReferenciaFornecedor      varchar(255),
	@usuarioId						int
	
	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Fornecedor
			SET		
				nomeFornecedor					=	@nomeFornecedor,				
				cnpjFornecedor					=	@cnpjFornecedor,				
				cpfFornecedor					=	@cpfFornecedor,				
				inscricaoEstadualFornecedor		=	@inscricaoEstadualFornecedor,
				telefoneFornecedor				=	@telefoneFornecedor,			
				faxFornecedor					=	@faxFornecedor,				
				enderecoFornecedor				=	@enderecoFornecedor,			
				complementoFornecedor			=	@complementoFornecedor,		
				bairroFornecedor				=	@bairroFornecedor,			
				cepFornecedor					=	@cepFornecedor,				
				municipioFornecedor				=	@municipioFornecedor,		
				ufFornecedor					=	@ufFornecedor,				
				emailFornecedor					=	@emailFornecedor,			
				contatoFornecedor				=	@contatoFornecedor,			
				dataCadastroFornecedor			=	@dataCadastroFornecedor,		
				pontoReferenciaFornecedor		=	@pontoReferenciaFornecedor,
				usuarioId						=	@usuarioId					
				
						
			WHERE
				idFornecedor = @idFornecedor	
			
			SELECT @idFornecedor AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspFornecedoresCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspFornecedoresCrudExcluir]
	@idFornecedor int
AS
BEGIN
	
	DELETE FROM
		Fornecedor
	WHERE
		idFornecedor = @idFornecedor
	
	SELECT @idFornecedor AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspFornecedoresCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspFornecedoresCrudInserir]
	@nomeFornecedor					varchar(200),
	@cnpjFornecedor					varchar(15),
	@cpfFornecedor					varchar(11),
	@inscricaoEstadualFornecedor	varchar(14),
	@telefoneFornecedor				varchar(11),
	@faxFornecedor					varchar(11),
	@enderecoFornecedor				varchar(200),
	@complementoFornecedor			varchar(100),
	@bairroFornecedor				varchar(100),
	@cepFornecedor					varchar(11),
	@municipioFornecedor			varchar(50),
	@ufFornecedor					varchar(2),
	@emailFornecedor				varchar(100),
	@contatoFornecedor				varchar(50),
	@dataCadastroFornecedor			datetime,
	@pontoReferenciaFornecedor      varchar(255),
	@usuarioId						int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT idFornecedor FROM Fornecedor WHERE cpfFornecedor = @cpfFornecedor or cnpjFornecedor = @cnpjFornecedor))
				RAISERROR('O CNPJ ou CPF do Fornecedor ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Fornecedor
			(
				nomeFornecedor,				
				cnpjFornecedor,				
				cpfFornecedor,				
				inscricaoEstadualFornecedor	,
				telefoneFornecedor,			
				faxFornecedor,				
				enderecoFornecedor,			
				complementoFornecedor,		
				bairroFornecedor,			
				cepFornecedor,				
				municipioFornecedor,			
				ufFornecedor,				
				emailFornecedor,				
				contatoFornecedor,			
				dataCadastroFornecedor,
				pontoReferenciaFornecedor,
				usuarioId					
				
			)
			VALUES
			(
				@nomeFornecedor,			
				@cnpjFornecedor,			
				@cpfFornecedor,				
				@inscricaoEstadualFornecedor,
				@telefoneFornecedor,		
				@faxFornecedor,				
				@enderecoFornecedor,		
				@complementoFornecedor,		
				@bairroFornecedor,			
				@cepFornecedor,				
				@municipioFornecedor,		
				@ufFornecedor,				
				@emailFornecedor,			
				@contatoFornecedor,			
				@dataCadastroFornecedor,	
				@pontoReferenciaFornecedor,
				@usuarioId					
				
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspFornecedoresListaCnpj]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspFornecedoresListaCnpj]
	@cnpjFornecedor					varchar(15)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Fornecedor
	WHERE 
		cnpjFornecedor LIKE '%' + @cnpjFornecedor + '%' ORDER BY cnpjFornecedor ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspFornecedoresListaCpf]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[uspFornecedoresListaCpf]
	@cpfFornecedor					varchar(11)	
AS
BEGIN
	SELECT 
		*		
	FROM Fornecedor
		
	WHERE 
		cpfFornecedor LIKE '%' + @cpfFornecedor + '%' ORDER BY cpfFornecedor ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspFornecedoresListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspFornecedoresListaNome]
	@nomeFornecedor					varchar(200)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Fornecedor
	WHERE 
		nomeFornecedor LIKE '%' + @nomeFornecedor + '%' ORDER BY nomeFornecedor ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoEntregas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoEntregas]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		* 
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	left join
		Entregador
	on
		Entregador.idEntregador = Entregas.idEntregador
	left join
		EntregaCancelada
	on
		EntregaCancelada.idVendaCabecalho = Entregas.idVendaCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoEntregasDiarias]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoEntregasDiarias]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	
	select 
		SUM(valorFrete) as valorFrete, count(idEntrega) as idEntrega, dataVendaCabecalho
	from
		VendasCabecalho
	join
		Entregas
	on
		VendasCabecalho.idVendaCabecalho = Entregas.idVendaCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by 
		dataVendaCabecalho	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoEntregasEntregador]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspGraficoEntregasEntregador]
	@dataInicial date,
	@dataFinal date
AS
BEGIN
	
	select 
		SUM(valorFrete) as valorFrete, count(idEntrega) as idEntrega, apelidoEntregador
	from
		VendasCabecalho
	join
		Entregas
	on
		VendasCabecalho.idVendaCabecalho = Entregas.idVendaCabecalho
	join
		Entregador
	on
		Entregador.idEntregador = Entregas.idEntregador
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by 
		apelidoEntregador	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoEntregasMensal]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoEntregasMensal]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		SUM(valorFrete) as valorFrete, count(idEntrega) as idEntrega, MONTH(dataVendaCabecalho) as idVendaCabecalho
	from
		VendasCabecalho
	join
		Entregas
	on
		VendasCabecalho.idVendaCabecalho = Entregas.idVendaCabecalho
	where
		MONTH(dataVendaCabecalho) BETWEEN MONTH(@dataInicial) AND MONTH(@dataFinal) AND YEAR(dataVendaCabecalho) BETWEEN YEAR(@dataInicial) AND YEAR(@dataFinal) and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by 
		MONTH(dataVendaCabecalho)
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoEntregasTempo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoEntregasTempo]
	@dataInicial date,
	@dataFinal date
AS
BEGIN
	
	select 
		apelidoEntregador, sum(ISNULL(DATEDIFF(MI, horaSaida, horaVolta),0)) as idEntrega, count(Entregas.idVendaCabecalho) as idVendaCabecalho
	FROM
		Entregas
	join
		VendasCabecalho
	on
		VendasCabecalho.idVendaCabecalho = Entregas.idVendaCabecalho
	join
		Entregador
	on
		Entregador.idEntregador = Entregas.idEntregador
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by
		apelidoEntregador
	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoInlocDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoInlocDelivery]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		REPLACE(REPLACE(REPLACE(REPLACE(SUBSTRING(atendimentoVendaCabecalho, 1, 1), 'M', 'MESAS'),
		 'D', 'DELIVERY'), 'C', 'CARRO'),'B', 'BALCÃO')  as tipo, sum(totalVendaCabecalho) as Total 
	from
		VendasCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2
	group by
		SUBSTRING(atendimentoVendaCabecalho, 1, 1)
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoVendasCanceladas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspGraficoVendasCanceladas]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		SUM(totalVendaCabecalho) as total, dataVendaCabecalho 
	from
		VendasCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 4
	group by 
		dataVendaCabecalho	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoVendasDiarias]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoVendasDiarias]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal date;
		
		SET @dateFinal =(select dateadd(day,1,@dataFinal))
		
	select 
		SUM(totalVendaCabecalho) as total, dataVendaCabecalho 
	from
		VendasCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho < @dateFinal and statusVendaCabecalho = 2
	group by 
		dataVendaCabecalho	
END



GO
/****** Object:  StoredProcedure [dbo].[uspGraficoVendasMensal]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGraficoVendasMensal]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal date;
		
		SET @dateFinal =(select dateadd(day,1,@dataFinal))
		
	select 
		SUM(totalVendaCabecalho) as total, MONTH(dataVendaCabecalho) as mes
	from
		VendasCabecalho
	where
		--MONTH(dataVendaCabecalho) >= MONTH(@dataInicial) and MONTH(dataVendaCabecalho) <= MONTH(@dateFinal) and statusVendaCabecalho = 2
		MONTH(dataVendaCabecalho) BETWEEN MONTH(@dataInicial) AND MONTH(@dateFinal) AND YEAR(dataVendaCabecalho) BETWEEN YEAR(@dataInicial) AND YEAR(@dateFinal)
	group by 
		MONTH(dataVendaCabecalho)
END



GO
/****** Object:  StoredProcedure [dbo].[uspGruposCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[uspGruposCrudAlterar]
	@grupoId int,
	@grupoDescricao	varchar(50),
	@grupoDataCad datetime,
	@usuarioId int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Grupo
			SET		
				grupoDescricao = @grupoDescricao,
				grupoDataCad = @grupoDataCad,	
				usuarioId = @usuarioId
						
			WHERE
				grupoId = @grupoId	
			
			SELECT @grupoId AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspGruposCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspGruposCrudExcluir]
	@grupoId int
AS
BEGIN
	
	DELETE FROM
		Grupo
	WHERE
		grupoId = @grupoId
	
	SELECT @grupoId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspGruposCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspGruposCrudInserir]
	@grupoDescricao	varchar(50),
	@grupoDataCad datetime,
	@usuarioId int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT grupoId FROM Grupo WHERE grupoDescricao = @grupoDescricao))
				RAISERROR('O Grupo ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Grupo
			(
				grupoDescricao,
				grupoDataCad,
				usuarioId
			)
			VALUES
			(
				@grupoDescricao,
				@grupoDataCad,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspGruposLista]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGruposLista]
	
AS
BEGIN
	SELECT 
		*		
	FROM 
		Grupo 
	WHERE 
		grupoDescricao <> 'Extras' ORDER BY grupoDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspGruposListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspGruposListaNome]
	@grupoDescricao varchar(50) = null
AS
BEGIN
	SELECT 
		*		
	FROM 
		Grupo 
	WHERE 
		grupoDescricao LIKE '%' + @grupoDescricao + '%' ORDER BY grupoDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspImprimirPedidos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspImprimirPedidos]
	@idVenda int
AS
BEGIN
	select 
		VendaDetalhes.codigoProduto, VendaDetalhes.extraVendaDetalhes, VendaDetalhes.especialVendaDetalhes, Produto.descricaoProduto, quantidadeVendaDetalhes, valorUnitarioVendaDetalhes, Produto.descricaoProduto, valorTotalVendaDetalhes, VendasCabecalho.atendenteVendaCabecalho 
	from
		VendaDetalhes	
	inner join
		Produto
	on
		Produto.codigoProduto = VendaDetalhes.codigoProduto
	inner join 
		VendasCabecalho
	on
		VendasCabecalho.idVendaCabecalho = VendaDetalhes.idVendaCabecalhoDetalhes
	WHERE
		idVendaCabecalhoDetalhes = @idVenda
END



GO
/****** Object:  StoredProcedure [dbo].[uspImprimirPedidosPapel]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspImprimirPedidosPapel]
	@idVenda int
AS
BEGIN
	select 
		VendaDetalhes.codigoProduto, VendaDetalhes.extraVendaDetalhes, VendaDetalhes.especialVendaDetalhes, Produto.descricaoProduto, quantidadeVendaDetalhes, valorUnitarioVendaDetalhes, Produto.descricaoProduto, valorTotalVendaDetalhes, VendasCabecalho.atendenteVendaCabecalho 
	from
		VendaDetalhes	
	inner join
		Produto
	on
		Produto.codigoProduto = VendaDetalhes.codigoProduto
	inner join 
		VendasCabecalho
	on
		VendasCabecalho.idVendaCabecalho = VendaDetalhes.idVendaCabecalhoDetalhes
	WHERE
		idVendaCabecalhoDetalhes = @idVenda and statusVendaDetalhes = 1
END



GO
/****** Object:  StoredProcedure [dbo].[uspMeuLancheCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspMeuLancheCrudAlterar]
	@idMeuLanche				int,
	@nomeMeuLanche				varchar(200),
	@cnpjMeuLanche				varchar(15) = null,
	@inscricaoEstadualMeuLanche	varchar(14),
	@telefoneMeuLanche			varchar(11),
	@faxMeuLanche				varchar(11),
	@enderecoMeuLanche			varchar(200),
	@complementoMeuLanche		varchar(100),
	@bairroMeuLanche			varchar(100),
	@cepMeuLanche				varchar(11),
	@municipioMeuLanche			varchar(50),
	@ufMeuLanche				varchar(2),
	@emailMeuLanche				varchar(100),
	@contatoMeuLanche			varchar(50),
	@localLogoMeuLanche			varchar(150)
	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				MeuLanche
			SET		
				nomeMeuLanche				= @nomeMeuLanche,				
				cnpjMeuLanche				= @cnpjMeuLanche,				
				inscricaoEstadualMeuLanche	= @inscricaoEstadualMeuLanche,
				telefoneMeuLanche			= @telefoneMeuLanche,			
				faxMeuLanche				= @faxMeuLanche,				
				enderecoMeuLanche			= @enderecoMeuLanche,			
				complementoMeuLanche		= @complementoMeuLanche,		
				bairroMeuLanche				= @bairroMeuLanche,			
				cepMeuLanche				= @cepMeuLanche,				
				municipioMeuLanche			= @municipioMeuLanche,		
				ufMeuLanche					= @ufMeuLanche,				
				emailMeuLanche				= @emailMeuLanche,			
				contatoMeuLanche			= @contatoMeuLanche,			
				localLogoMeuLanche			= @localLogoMeuLanche 
						
			WHERE
				idMeuLanche = @idMeuLanche	
			
			SELECT @idMeuLanche AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspMeuLancheListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspMeuLancheListaNome]
	
AS
BEGIN
	SELECT 
		*		
	FROM 
		MeuLanche
END



GO
/****** Object:  StoredProcedure [dbo].[uspNivelAcessoCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNivelAcessoCrudAlterar]
	@nivelAcessoId int,
	@nivelAcessoDescricao varchar(50),
	@dataCadAcessoNivel datetime,
	@idUsuario int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
		
			UPDATE 
				NivelAcesso
			SET		
				nivelAcessoDescricao = @nivelAcessoDescricao,
				dataCadAcessoNivel = @dataCadAcessoNivel,	
				idUsuario = @idUsuario
						
			WHERE
				nivelAcessoId = @nivelAcessoId	
			
			SELECT @nivelAcessoId AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspNivelAcessoCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspNivelAcessoCrudExcluir]
	@nivelAcessoId int
AS
BEGIN
	
	DELETE FROM
		NivelAcesso
	WHERE
		nivelAcessoId = @nivelAcessoId
	
	SELECT @nivelAcessoId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspNivelAcessoCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspNivelAcessoCrudInserir]
	@nivelAcessoDescricao varchar(50),
	@dataCadAcessoNivel datetime,
	@usuarioId int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT nivelAcessoId FROM NivelAcesso WHERE nivelAcessoDescricao = @nivelAcessoDescricao))
				RAISERROR('O Nivel de Acesso ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO NivelAcesso
			(
				nivelAcessoDescricao,
				dataCadAcessoNivel,
				idUsuario
			)
			VALUES
			(
				@nivelAcessoDescricao,
				@dataCadAcessoNivel,
				@usuarioId
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspNivelAcessoListaGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspNivelAcessoListaGeral]
	@nivelAcessoDescricao varchar(50)
AS
BEGIN
	SELECT 
		*		
	FROM 
		NivelAcesso
	where 
		nivelAcessoDescricao like '%' + @nivelAcessoDescricao + '%' ORDER BY nivelAcessoDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspPermissaoAcessoCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPermissaoAcessoCrudInserir]
	@permissaoAcessoId int,
	@nivelAcessoId int,
	@buton varchar(50),
	@ativo bigint,
	@cadastrar bigint,
	@alterar bigint,
	@excluir bigint,
	@pesquisar bigint,
	@relatorios bigint,
	@graficos bigint,
	@financeiro bigint		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			--IF(EXISTS(SELECT permissaoAcessoId FROM PermissaoAcesso WHERE permissaoAcessoId = @permissaoAcessoId))
			IF(EXISTS(SELECT permissaoAcessoId FROM PermissaoAcesso WHERE nivelAcessoId = @nivelAcessoId and buton = @buton))
				BEGIN
					UPDATE
						PermissaoAcesso
					SET
						nivelAcessoId = @nivelAcessoId,
						buton = @buton,
						ativo =  @ativo,
						cadastrar = @cadastrar,
						alterar = @alterar,
						excluir = @excluir,
						pesquisar = @pesquisar,
						relatorios = @relatorios,
						graficos = @graficos,
						financeiro = @financeiro
					WHERE
						--permissaoAcessoId = @permissaoAcessoId
						nivelAcessoId = @nivelAcessoId and buton = @buton

					SELECT @permissaoAcessoId AS retorno;
				END
			ELSE
				BEGIN
					INSERT INTO
						PermissaoAcesso
					(
						nivelAcessoId,
						buton,
						ativo,
						cadastrar,
						alterar,
						excluir,
						pesquisar,
						relatorios,
						graficos,
						financeiro
					)
					VALUES
					(
						@nivelAcessoId,
						@buton,
					    @ativo,
						@cadastrar,
						@alterar,
						@excluir,
						@pesquisar,
						@relatorios,
						@graficos,
						@financeiro
					)
						
					SELECT @@IDENTITY AS retorno;
				END
			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspPermissaoAcessoListaId]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPermissaoAcessoListaId]
	@nivelAcessoId int,
	@buton varchar(50)
AS
BEGIN
	SELECT 
		*		
	FROM 
		PermissaoAcesso 
	WHERE 
		nivelAcessoId = @nivelAcessoId and buton = @buton
END



GO
/****** Object:  StoredProcedure [dbo].[uspPlanoContaCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPlanoContaCrudAlterar]
	@idPlanoConta	int,
	@codPlanoConta	int,
	@descricaoPlanoConta	varchar(50),
	@dataCadPlanoConta	datetime,
	@usuarioId	int
	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				PlanoConta
			SET		
				codPlanoConta = @codPlanoConta,
				descricaoPlanoConta = @descricaoPlanoConta,	
				dataCadPlanoConta = @dataCadPlanoConta,
				usuarioId = @usuarioId
						
			WHERE
				idPlanoConta = @idPlanoConta	
			
			SELECT @idPlanoConta AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspPlanoContaCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspPlanoContaCrudExcluir]
	@codPlanoConta int
AS
BEGIN
	
	DELETE FROM
		PlanoConta
	WHERE
		codPlanoConta = @codPlanoConta
	
	SELECT @codPlanoConta AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspPlanoContaCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspPlanoContaCrudInserir]
	@codPlanoConta	int,
	@descricaoPlanoConta	varchar(50),
	@dataCadPlanoConta	datetime,
	@usuarioId	int		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT codPlanoConta FROM PlanoConta WHERE codPlanoConta = @codPlanoConta or descricaoPlanoConta = @descricaoPlanoConta))
				RAISERROR('O Plano Conta ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO PlanoConta
			(
				codPlanoConta,
				descricaoPlanoConta,
				dataCadPlanoConta,
				usuarioId
			)
			VALUES
			(
				@codPlanoConta,
				@descricaoPlanoConta,
				@dataCadPlanoConta,
				@usuarioId	
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspPlanoContaListaCodigo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPlanoContaListaCodigo]
	@codPlanoConta int
AS
BEGIN
	SELECT 
		*		
	FROM 
		PlanoConta 
	WHERE 
		codPlanoConta = @codPlanoConta ORDER BY codPlanoConta ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspPlanoContaListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPlanoContaListaNome]
	@descricaoPlanoConta varchar(50)
AS
BEGIN
	SELECT 
		*		
	FROM 
		PlanoConta 
	WHERE 
		descricaoPlanoConta LIKE '%' + @descricaoPlanoConta + '%' ORDER BY descricaoPlanoConta ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspProdutoListaId]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutoListaId]
	@codigoProduto	int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			if((select COUNT(codigoProduto) from Produto where codigoProduto = @codigoProduto) > 0)
			begin
				
				SELECT 
					descricaoProduto	
				FROM 
					Produto 
				WHERE 
					codigoProduto = @codigoProduto and manufaturadoProduto = 'N'
			end
			else
			begin
				RAISERROR('1',14,1);
			end
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspProdutosCrudAlterar]
	@codigoProduto	int,
	@undMedidaId	int,
	@idFornecedor	int,
	@grupoId	int,
	@descricaoProduto	varchar(100),
	@valorCompraProduto	decimal(18, 2),
	@valorVendaProduto	decimal(18, 2),
	@estoqueProduto	decimal(18, 2),
	@estoqueCriticoProduto	decimal(18, 2),
	@dataCadastroProduto	datetime,
	@controlaEstoqueProduto	char(1),
	@manufaturadoProduto	char(1),
	@usuarioId	int
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				Produto
			SET		
				codigoProduto =			@codigoProduto,
				undMedidaId =			@undMedidaId,
				idFornecedor =			@idFornecedor,
				grupoId =				@grupoId,
				descricaoProduto =		@descricaoProduto,
				valorCompraProduto =		@valorCompraProduto,
				valorVendaProduto =		@valorVendaProduto,
				estoqueProduto =			@estoqueProduto,
				estoqueCriticoProduto =	@estoqueCriticoProduto,	
				dataCadastroProduto =	@dataCadastroProduto,
				controlaEstoqueProduto =	@controlaEstoqueProduto,
				manufaturadoProduto =	@manufaturadoProduto,
				usuarioId =				@usuarioId	
						
			WHERE
				codigoProduto = @codigoProduto	
			
			SELECT @codigoProduto AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspProdutosCrudExcluir]
	@codigoProduto int
AS
BEGIN
	
	DELETE FROM
		Produto
	WHERE
		codigoProduto = @codigoProduto
	
	SELECT @codigoProduto AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspProdutosCrudInserir]
	@codigoProduto	int,
	@undMedidaId	int,
	@idFornecedor	int,
	@grupoId	int,
	@descricaoProduto	varchar(100),
	@valorCompraProduto	decimal(18, 2),
	@valorVendaProduto	decimal(18, 2),
	@estoqueProduto	decimal(18, 2),
	@estoqueCriticoProduto	decimal(18, 2),
	@dataCadastroProduto	datetime,
	@controlaEstoqueProduto	char(1),
	@manufaturadoProduto	char(1),
	@usuarioId	int
					
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT codigoProduto FROM Produto WHERE codigoProduto = @codigoProduto or descricaoProduto = @descricaoProduto))
				RAISERROR('O Código ou a Descrição do Produto ja está cadastrado no Bando de Dados.',14,1);
			
			INSERT INTO Produto
			(
				codigoProduto,
				undMedidaId,
				idFornecedor,
				grupoId,
				descricaoProduto,
				valorCompraProduto,
				valorVendaProduto,
				estoqueProduto,
				estoqueCriticoProduto,	
				dataCadastroProduto,
				controlaEstoqueProduto,
				manufaturadoProduto,
				usuarioId
			)
			VALUES
			(
				@codigoProduto,
				@undMedidaId,
				@idFornecedor,
				@grupoId,
				@descricaoProduto,
				@valorCompraProduto,
				@valorVendaProduto,
				@estoqueProduto,
				@estoqueCriticoProduto,	
				@dataCadastroProduto,
				@controlaEstoqueProduto,
				@manufaturadoProduto,
				@usuarioId	
			)
			
			SELECT @codigoProduto AS retorno;
			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaCod]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspProdutosListaCod]
	@codigoProduto	int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			--if((select COUNT(codigoProduto) from Produto where codigoProduto = @codigoProduto and manufaturadoProduto = 'N') > 0)
			--begin
				
				SELECT 
					descricaoProduto,
					valorCompraProduto,
					valorVendaProduto
				FROM 
					Produto 
				WHERE 
					codigoProduto = @codigoProduto and manufaturadoProduto = 'N'
			--end
			
			--else
				--begin
					--RAISERROR('1',14,1);
				--end
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaCodigo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutosListaCodigo]
	@codigoProduto	int
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		codigoProduto = @codigoProduto ORDER BY codigoProduto ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaGrupo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutosListaGrupo]
	@grupoDescricao	varchar(50)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		grupoDescricao LIKE '%' + @grupoDescricao + '%' ORDER BY grupoDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutosListaNome] 
	@descricaoProduto	varchar(100)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		descricaoProduto LIKE '%' + @descricaoProduto + '%' ORDER BY codigoProduto ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaNomeExtra]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutosListaNomeExtra] 
	@descricaoProduto	varchar(100)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		descricaoProduto LIKE '%' + @descricaoProduto + '%' and Grupo.grupoDescricao = 'EXTRAS' ORDER BY codigoProduto ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspProdutosManufaturadoCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspProdutosManufaturadoCrudExcluir]
	@codProduto int
AS
BEGIN
	
	DELETE FROM
		ProdutoManufaturado
	WHERE
		codProduto = @codProduto
	
	SELECT @codProduto AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosManufaturadoCrudExcluirItem]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspProdutosManufaturadoCrudExcluirItem]
	@codProduto int,
	@codProdutoManufaturado	int
AS
BEGIN
	
	DELETE FROM
		ProdutoManufaturado
	WHERE
		codProduto = @codProduto and codProdutoManufaturado = @codProdutoManufaturado
	
	SELECT @codProduto AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosManufaturadoCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutosManufaturadoCrudInserir]
	@codProduto	int,
	@codProdutoManufaturado	int,
	@quantidadeProdutoManufaturado	decimal(18, 2)
					
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT @codProduto FROM ProdutoManufaturado WHERE codProdutoManufaturado = @codProdutoManufaturado and codProduto = @codProduto))
				RAISERROR('1',14,1);
			
			INSERT INTO ProdutoManufaturado
			(
				codProduto,
				codProdutoManufaturado,
				quantidadeProdutoManufaturado			
				
			)
			VALUES
			(
				@codProduto,
				@codProdutoManufaturado,
				@quantidadeProdutoManufaturado
			)
				
			select @codProduto as retorno;
						
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspProdutosManufaturadoListaCod]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspProdutosManufaturadoListaCod]
	@codProduto	int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
				
				SELECT 
					codProduto,
					codProdutoManufaturado,
					quantidadeProdutoManufaturado,
					descricaoProduto,
					valorCompraProduto,
					valorVendaProduto,
					estoqueProduto
					
				FROM 
					ProdutoManufaturado 
				join
				Produto
				on ProdutoManufaturado.codProdutoManufaturado = Produto.codigoProduto
				
				WHERE 
					codProduto = @codProduto 
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioClientes]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioClientes]
	
AS
BEGIN
	select 
		nomeCliente, telefoneCliente, celularCliente, enderecoCliente, bairroCliente, emailCliente, cpfCliente
	from
		Clientes	
	order by 
		nomeCliente
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioContasEmAberto]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioContasEmAberto]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal date;
	
		SET @dateFinal =(select dateadd(day,1,@dataFinal))
				
	select 
		ContasPagar.*, codPlanoConta
	from
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	where
		dataCadContaPagar > @dataInicial and dataCadContaPagar < @dateFinal	and statusContaPagar = 0
	order by 
		vencimentoContaPagar asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioContasGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioContasGeral]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal date;
	
	
		SET @dateFinal =(select dateadd(day,1,@dataFinal))
		--SET @dateInicial =(select dateadd(day,-1,@dataInicial))
		
	select 
		ContasPagar.*, codPlanoConta
	from
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	where
		dataCadContaPagar > @dataInicial and dataCadContaPagar < @dateFinal	
	order by
		vencimentoContaPagar asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioContasPagas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioContasPagas]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal date;
	
		SET @dateFinal =(select dateadd(day,1,@dataFinal))
		
	select 
		ContasPagar.*, codPlanoConta, ContasPagas.dataPagamentoContaPagas
	from
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	join
		ContasPagas
	on
		ContasPagas.idContasPagas = ContasPagar.idContaPagar
	where
		dataPagamentoContaPagas >= @dataInicial and dataPagamentoContaPagas	 < @dateFinal	and statusContaPagar = 1
	order by 
		vencimentoContaPagar asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioContasVencidas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioContasVencidas]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal date;
	
		SET @dateFinal =(select dateadd(day,1,@dataFinal))
		
	select 
		ContasPagar.*, codPlanoConta
	from
		ContasPagar
	join
		PlanoConta
	on
		PlanoConta.idPlanoConta = ContasPagar.idPlanoConta
	where
		vencimentoContaPagar = @dataInicial and statusContaPagar = 0
	order by 
		vencimentoContaPagar asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioDeliveryCancelada]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioDeliveryCancelada]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		VendasCabecalho.idVendaCabecalho, totalVendaCabecalho, dataVendaCabecalho, apelidoEntregador, motivoCancelamento
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	left Join
		Entregador
	on
		Entregas.idEntregador = Entregador.idEntregador
	left Join
		EntregaCancelada
	on
		EntregaCancelada.idVendaCabecalho = Entregas.idVendaCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 4 and atendimentoVendaCabecalho = 'DV'
	order by
		dataVendaCabecalho desc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioDeliveryDiaria]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioDeliveryDiaria]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		sum(totalVendaCabecalho) as totalVendaCabecalho, COUNT(idEntrega) as idEntrega, dataVendaCabecalho, sum(valorFrete) as valorFrete
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by 
		dataVendaCabecalho
	order by
		dataVendaCabecalho desc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioDeliveryEntregador]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioDeliveryEntregador]
	@dataInicial date,
	@dataFinal date,
	@apelido varchar(50)
AS
BEGIN

	select 
		sum(totalVendaCabecalho) as totalVendaCabecalho, COUNT(idEntrega) as idEntrega, dataVendaCabecalho, sum(valorFrete) as valorFrete, apelidoEntregador, sum(ISNULL(DATEDIFF(MI, horaSaida, horaVolta),0)) as tempoEntrega
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	left join
		Entregador
	on
		Entregas.idEntregador = Entregador.idEntregador	
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV' and apelidoEntregador = @apelido
	group by 
		dataVendaCabecalho, apelidoEntregador
	order by
		dataVendaCabecalho desc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioDeliveryEntregadorGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioDeliveryEntregadorGeral]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		sum(totalVendaCabecalho) as totalVendaCabecalho, COUNT(idEntrega) as idEntrega, sum(valorFrete) as valorFrete, apelidoEntregador, sum(ISNULL(DATEDIFF(MI, horaSaida, horaVolta),0)) as tempoEntrega
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	left join
		Entregador
	on
		Entregas.idEntregador = Entregador.idEntregador	
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by 
		apelidoEntregador
	
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioDeliveryGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioDeliveryGeral]
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		*, ISNULL(DATEDIFF(MI, horaSaida, horaVolta),0) as tempoEntrega
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	left join
		Entregador
	on
		Entregador.idEntregador = Entregas.idEntregador
	left join
		EntregaCancelada
	on
		EntregaCancelada.idVendaCabecalho = Entregas.idVendaCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	order by
		dataVendaCabecalho desc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioDeliveryMensal]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioDeliveryMensal]
	@dataInicial date,
	@dataFinal date
AS
BEGIN
	select 
		sum(totalVendaCabecalho) as totalVendaCabecalho, COUNT(idEntrega) as idEntrega, MONTH(dataVendaCabecalho) as idVendaCabecalho, sum(valorFrete) as valorFrete
	from
		VendasCabecalho
	left join
		Entregas
	on
		Entregas.idVendaCabecalho = VendasCabecalho.idVendaCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho = 'DV'
	group by 
		MONTH(dataVendaCabecalho)
	order by
		MONTH(dataVendaCabecalho) desc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioEntregadoresGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioEntregadoresGeral]
	
AS
BEGIN
	select 
		nomeEntregador, apelidoEntregador, telefoneEntregador, celularEntregador, emailEntregador, statusEntregador
	from
		Entregador	
	order by 
		nomeEntregador
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioEntregadoresStatus]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioEntregadoresStatus]
	@status bigint
AS
BEGIN
	select 
		nomeEntregador, apelidoEntregador, telefoneEntregador, celularEntregador, emailEntregador, statusEntregador
	from
		Entregador	
		where statusEntregador = @status
	order by 
		nomeEntregador
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioFornecedores]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioFornecedores]
	
AS
BEGIN
	select 
		nomeFornecedor, cnpjFornecedor, cpfFornecedor, telefoneFornecedor, faxFornecedor, contatoFornecedor
	from
		Fornecedor	
	order by 
		nomeFornecedor
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoEntradaCodigo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioProdutoEntradaCodigo]
	@codigoProduto int,
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
	
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
		
	select 
		EntradaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SUM(EntradaProduto.quantidadeProdutoEntrada) as qtdEntrada, EntradaProduto.motivoEntradaProduto, EntradaProduto.nfEntradaProduto, EntradaProduto.dataCadEntradaProduto
	from
		EntradaProduto
	inner join
		Produto
	on
		Produto.codigoProduto = EntradaProduto.codigoProduto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	where
	dataCadEntradaProduto > @dataInicial and dataCadEntradaProduto < @dateFinal1 and EntradaProduto.codigoProduto = @codigoProduto
	group by
		EntradaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, EntradaProduto.motivoEntradaProduto, EntradaProduto.nfEntradaProduto, EntradaProduto.dataCadEntradaProduto
	order by 
		dataCadEntradaProduto DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoEntradaGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioProdutoEntradaGeral]
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
	
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))

	select 
		EntradaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SUM(EntradaProduto.quantidadeProdutoEntrada) as qtdEntrada, EntradaProduto.motivoEntradaProduto, EntradaProduto.nfEntradaProduto, EntradaProduto.dataCadEntradaProduto
	from
		EntradaProduto
	inner join
		Produto
	on
		Produto.codigoProduto = EntradaProduto.codigoProduto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	where
	dataCadEntradaProduto > @dataInicial and dataCadEntradaProduto < @dateFinal1	
	group by
		EntradaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, EntradaProduto.motivoEntradaProduto, EntradaProduto.nfEntradaProduto, EntradaProduto.dataCadEntradaProduto
	order by 
		dataCadEntradaProduto DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioProdutoGeral]
	
AS
BEGIN
	select 
		Produto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, Grupo.grupoDescricao, Produto.estoqueProduto, valorVendaProduto
	from
		Produto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	inner join
		Grupo
	on
		Produto.grupoId = Grupo.grupoId
	order by 
		Produto.codigoProduto asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoMaisVendido]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioProdutoMaisVendido]
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
	
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
		
	select 
		VendaDetalhes.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SUM(VendaDetalhes.quantidadeVendaDetalhes) as qtdVenda
	from
		VendaDetalhes
	inner join
		Produto
	on
		Produto.codigoProduto = VendaDetalhes.codigoProduto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	where
	dataEnvioVendaDetalhes > @dataInicial and dataEnvioVendaDetalhes < @dateFinal1	
	group by
		VendaDetalhes.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao
	order by 
		qtdVenda DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoMinimo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioProdutoMinimo]
	
AS
BEGIN
	select 
		Produto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, Produto.estoqueProduto, Produto.estoqueCriticoProduto
	from
		Produto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	WHERE
		Produto.estoqueProduto < Produto.estoqueCriticoProduto
	order by 
		Produto.codigoProduto asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoPorGrupo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioProdutoPorGrupo]
	@grupoId int
AS
BEGIN
	select 
		Produto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, Grupo.grupoDescricao, Produto.estoqueProduto, valorVendaProduto
	from
		Produto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	inner join
		Grupo
	on
		Produto.grupoId = Grupo.grupoId
	WHERE
		Grupo.grupoId = @grupoId
	order by 
		Produto.codigoProduto asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoSaidaCodigo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioProdutoSaidaCodigo]
	@codigoProduto int,
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
	
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
		
	select 
		SaidaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SUM(SaidaProduto.quantidadeProdutoSaida) as qtdSaida, SaidaProduto.motivoSaida, SaidaProduto.dataCadSaida
	from
		SaidaProduto
	inner join
		Produto
	on
		Produto.codigoProduto = SaidaProduto.codigoProduto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	where
	dataCadSaida > @dataInicial and dataCadSaida < @dateFinal1 and SaidaProduto.codigoProduto = @codigoProduto
	group by
		SaidaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SaidaProduto.motivoSaida, SaidaProduto.dataCadSaida
	order by 
		dataCadSaida DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioProdutoSaidaGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioProdutoSaidaGeral]
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
		
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
				
	select 
		SaidaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SUM(SaidaProduto.quantidadeProdutoSaida) as qtdSaida, SaidaProduto.motivoSaida, SaidaProduto.dataCadSaida
	from
		SaidaProduto
	inner join
		Produto
	on
		Produto.codigoProduto = SaidaProduto.codigoProduto
	inner join
		UndMedida
	on
		UndMedida.undMedidaId = Produto.undMedidaId
	where
	dataCadSaida > @dataFinal and dataCadSaida < @dateFinal1	
	group by
		SaidaProduto.codigoProduto, Produto.descricaoProduto, UndMedida.undMedidaDescricao, SaidaProduto.motivoSaida, SaidaProduto.dataCadSaida
	order by 
		dataCadSaida DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioRetirada]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioRetirada]
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
		
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
				
	select 
		Retirada.*, Usuarios.usuarioLogin 
	from
		Retirada
	inner join
		Usuarios
	on
		Retirada.idUsuario = Usuarios.usuarioId
	where
		Retirada.dataRetirada > @dataFinal and Retirada.dataRetirada < @dateFinal1	
	order by 
		Retirada.dataRetirada asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioRetiradaUsuario]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioRetiradaUsuario]
	@idUsuario int,
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
	
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
				
	select 
		Retirada.*, Usuarios.usuarioLogin 
	from
		Retirada
	inner join
		Usuarios
	on
		Retirada.idUsuario = Usuarios.usuarioId
	where
		Retirada.dataRetirada > @dataInicial and Retirada.dataRetirada < @dateFinal1 and @idUsuario = Retirada.idUsuario
	order by 
		Retirada.dataRetirada asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioUsuarios]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspRelatorioUsuarios]
	
AS
BEGIN
	select 
		usuarioNome, usuarioLogin, usuarioCpf, nivelAcessoDescricao
	from
		Usuarios
	join
		NivelAcesso
	on
		Usuarios.nivelAcessoId = NivelAcesso.nivelAcessoId		
	order by 
		usuarioNome
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioVendasAtendente]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioVendasAtendente]
	@atendente varchar(100),
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	select 
		idVendaCabecalho, atendimentoVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, atendenteVendaCabecalho, dataVendaCabecalho
	from
		VendasCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal	and atendenteVendaCabecalho = @atendente
	order by 
		dataVendaCabecalho DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioVendasGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioVendasGeral]
	@dataInicial date,
	@dataFinal date
AS
BEGIN
		
	select 
		idVendaCabecalho, atendimentoVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, atendenteVendaCabecalho, dataVendaCabecalho
	from
		VendasCabecalho
	where
		dataVendaCabecalho >= @dataInicial and dataVendaCabecalho <= @dataFinal and statusVendaCabecalho = 2 and atendimentoVendaCabecalho != 'DV'
	order by 
		dataVendaCabecalho DESC
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioVendasGrupo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioVendasGrupo]
	@idGrupo int,
	@dataInicial datetime,
	@dataFinal datetime
AS
BEGIN

	declare @dateFinal1 date;
	declare @dateInicial date;
	
		SET @dateInicial =(select dateadd(day,-1,@dataInicial))
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
		
		
	select
		Produto.codigoProduto, Produto.descricaoProduto, SUM(VendaDetalhes.quantidadeVendaDetalhes) as qtdVendida, SUM(VendaDetalhes.valorTotalVendaDetalhes) as valorVendido, Grupo.grupoDescricao
	from
		VendaDetalhes
	inner join 
		Produto
	on
		Produto.codigoProduto = VendaDetalhes.codigoProduto
	inner join
		Grupo
	on 
		Produto.grupoId = Grupo.grupoId
	where
		VendaDetalhes.dataEnvioVendaDetalhes > @dateInicial and VendaDetalhes.dataEnvioVendaDetalhes < @dateFinal1	and Produto.grupoId = @idGrupo
	GROUP BY
		Produto.codigoProduto, Produto.descricaoProduto, Grupo.grupoDescricao
	ORDER BY
		qtdVendida DESC
	
END



GO
/****** Object:  StoredProcedure [dbo].[uspRelatorioVendasProduto]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRelatorioVendasProduto]
	@idProduto int,
	@dataInicial date,
	@dataFinal date
AS
BEGIN

	declare @dateFinal1 date;
	
		SET @dateFinal1 =(select dateadd(day,1,@dataFinal))
				
	select
		Produto.codigoProduto, Produto.descricaoProduto, SUM(VendaDetalhes.quantidadeVendaDetalhes) as qtdVendida, SUM(VendaDetalhes.valorTotalVendaDetalhes) as valorVendido, Produto.valorVendaProduto
	from
		VendaDetalhes
	inner join 
		Produto
	on
		Produto.codigoProduto = VendaDetalhes.codigoProduto
	where
		VendaDetalhes.dataEnvioVendaDetalhes > @dataInicial and VendaDetalhes.dataEnvioVendaDetalhes < @dateFinal1	and Produto.codigoProduto = @idProduto
	GROUP BY
		Produto.codigoProduto, Produto.descricaoProduto, Produto.valorVendaProduto	
END



GO
/****** Object:  StoredProcedure [dbo].[uspRetiradasCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRetiradasCrudInserir]
	@idUsuario	int,
	@valorRetirada	decimal(18, 2) = 0,
	@dataRetirada	datetime,
	@administradorRetirada varchar (100),
	@motivoRetirada	varchar(255)
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO Retirada
			(
				idUsuario,
				valorRetirada,
				dataRetirada,
				motivoRetirada,
				administradorRetirada
			)
			VALUES
			(
				@idUsuario,
				@valorRetirada,
				@dataRetirada,
				@motivoRetirada,
				@administradorRetirada
			)
			
			SELECT @@IDENTITY AS Retorno;			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspRetiradasSoma]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRetiradasSoma]
	@hora datetime
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			--declare @date1 date;
				
			--SET @date1 =(select dateadd(day,1,@data))
				
				select 
					SUM(valorRetirada) as totalRetiradas
				from
					Retirada 
			
				where
					dataRetirada > @hora
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[uspSaidaProdutosCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSaidaProdutosCrudInserir]
	@codigoProduto	int,
	@quantidadeProdutoSaida	decimal(18, 2),
	@motivoSaida	varchar(100),
	@dataCadSaida	datetime,
	@usuarioId	int,
	@estoqueProduto decimal(18,2)					
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO SaidaProduto
			(
				codigoProduto,
				quantidadeProdutoSaida,
				motivoSaida,	
				dataCadSaida,	
				usuarioId	
			)
			VALUES
			(
				@codigoProduto,
				@quantidadeProdutoSaida,
				@motivoSaida,	
				@dataCadSaida,	
				@usuarioId	
			)
			
			SELECT @@IDENTITY AS retorno;
			
			UPDATE 
				Produto
			SET		
				estoqueProduto = @estoqueProduto,
				usuarioId = @usuarioId	
						
			WHERE
				codigoProduto = @codigoProduto	
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspSerialCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSerialCrudInserir]
	@serialInstalacao text,
	@serialAtivacao text
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
			
			INSERT INTO Serial
			(
				serialInstalacao, serialAtivacao
			)
			VALUES
			(
				@serialInstalacao, @serialAtivacao
			)
			
			SELECT @@IDENTITY AS retorno;			
			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspTipoPagamentoCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspTipoPagamentoCrudAlterar]
	@idTipoPagamento	int,
	@descricaoTipoPagamento	varchar(50),
	@dataCadTipoPagamento	datetime,
	@usuarioId int
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				TipoPagamento
			SET		
				descricaoTipoPagamento = @descricaoTipoPagamento,
				dataCadTipoPagamento = @dataCadTipoPagamento,
				usuarioId = @usuarioId
						
			WHERE
				idTipoPagamento = @idTipoPagamento	
			
			SELECT @idTipoPagamento AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspTipoPagamentoCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspTipoPagamentoCrudExcluir]
	@idTipoPagamento int
AS
BEGIN
	
	DELETE FROM
		TipoPagamento
	WHERE
		idTipoPagamento = @idTipoPagamento
	
	SELECT @idTipoPagamento AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspTipoPagamentoCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspTipoPagamentoCrudInserir]
	@descricaoTipoPagamento	varchar(50),
	@dataCadTipoPagamento	datetime,
	@usuarioId	int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT idTipoPagamento FROM TipoPagamento WHERE descricaoTipoPagamento = @descricaoTipoPagamento))
				RAISERROR('O Tipo de Pagamento ja está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO TipoPagamento
			(
				descricaoTipoPagamento,
				dataCadTipoPagamento,
				usuarioId
			)
			VALUES
			(
				@descricaoTipoPagamento,
				@dataCadTipoPagamento,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspTipoPagamentoListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspTipoPagamentoListaNome]
	@descricaoTipoPagamento varchar(50)
AS
BEGIN
	SELECT 
		*		
	FROM 
		TipoPagamento
	WHERE 
		descricaoTipoPagamento LIKE '%' + @descricaoTipoPagamento + '%' ORDER BY descricaoTipoPagamento ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspUnidadeMedidaCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspUnidadeMedidaCrudAlterar]
	@undMedidaId int,
	@undMedidaDescricao	varchar(5),
	@undMedidaDataCad datetime,
	@usuarioId int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				UndMedida
			SET		
				undMedidaDescricao = @undMedidaDescricao,	
				undMedidaDataCad = @undMedidaDataCad,
				usuarioId = @usuarioId
						
			WHERE
				undMedidaId = @undMedidaId	
			
			SELECT @undMedidaId AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspUnidadeMedidaCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspUnidadeMedidaCrudExcluir]
	@undMedidaId int
AS
BEGIN
	
	DELETE FROM
		UndMedida
	WHERE
		undMedidaId = @undMedidaId
	
	SELECT @undMedidaId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspUnidadeMedidaCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspUnidadeMedidaCrudInserir]
	@undMedidaDescricao	varchar(5),
	@undMedidaDataCad datetime,
	@usuarioId int		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT undMedidaId FROM UndMedida WHERE undMedidaDescricao = @undMedidaDescricao))
				RAISERROR('A Unidade de Medida já está cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO UndMedida
			(
				undMedidaDescricao,
				undMedidaDataCad,
				usuarioId
			)
			VALUES
			(
				@undMedidaDescricao,
				@undMedidaDataCad,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspUnidadeMedidaListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspUnidadeMedidaListaNome]
	@undMedidaDescricao varchar(5) = null
AS
BEGIN
	SELECT 
		*		
	FROM 
		UndMedida 
	WHERE 
		undMedidaDescricao LIKE '%' + @undMedidaDescricao + '%' ORDER BY undMedidaDescricao ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioChecaLogin]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspUsuarioChecaLogin]
	@usuarioLogin	varchar(20),
	@usuarioSenha	varchar(20)
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			--IF(EXISTS(SELECT * FROM Usuarios WHERE usuarioLogin = @usuarioLogin and usuarioSenha = @usuarioSenha))
				--RAISERROR('Login e Senha não Conferem.',14,1);
				
			SELECT
				*
			FROM
				Usuarios
			join 
				NivelAcesso 
			on 
			
			Usuarios.nivelAcessoId = NivelAcesso.nivelAcessoId
			
			and
			
			usuarioLogin = @usuarioLogin and usuarioSenha = @usuarioSenha
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioListaCPF]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspUsuarioListaCPF]
	@usuarioCpf varchar(20) = null
AS
BEGIN
	SELECT 
		*		
	FROM 
		Usuarios 
	join 
		NivelAcesso 
	on 
	Usuarios.nivelAcessoId = NivelAcesso.nivelAcessoId
	AND 
	usuarioCpf LIKE '%' + @usuarioCpf + '%' order by usuarioCpf ASC
END



GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioListaLogin]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspUsuarioListaLogin]
	@usuarioLogin varchar(20) = null
AS
BEGIN
	SELECT 
		usuarioId, 
		usuarioNome, 
		usuarioLogin, 
		usuarioSenha, 
		usuarioCpf, 
		nivelAcessoDescricao,
		Usuarios.nivelAcessoId
		 
	FROM 
		Usuarios 
	join 
		NivelAcesso 
	on 
	Usuarios.nivelAcessoId = NivelAcesso.nivelAcessoId
	AND 
	usuarioLogin LIKE '%' + @usuarioLogin + '%' order by usuarioLogin asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioListaNome]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspUsuarioListaNome]
	@usuarioNome varchar(100) = null
AS
BEGIN
	SELECT 
		*		
	FROM 
		Usuarios 
	join 
		NivelAcesso 
	on 
	Usuarios.nivelAcessoId = NivelAcesso.nivelAcessoId
	AND 
	usuarioNome LIKE '%' + @usuarioNome + '%' order by usuarioNome asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioListarGeral]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspUsuarioListarGeral] 
	
AS
BEGIN
	SELECT 
		*		
	FROM 
		Usuarios 
	join 
		NivelAcesso 
	on 
	Usuarios.nivelAcessoId = NivelAcesso.nivelAcessoId 
	order by 
		usuarioLogin asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspUsuariosCrudAlterar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspUsuariosCrudAlterar]
	@usuarioId int,
	@usuarioNome	varchar(100),
	@usuarioLogin	varchar(20),
	@usuarioSenha	varchar(20),
	@usuarioCpf	varchar(20),
	@nivelAcessoId	int,
	@usuarioDataCad	datetime,
	@usuarioCadUsuarioId int
AS
BEGIN
	
	UPDATE 
		Usuarios
	SET
		usuarioNome = @usuarioNome,
		usuarioLogin = @usuarioLogin,	
		usuarioSenha = @usuarioSenha,	
		usuarioCpf = @usuarioCpf,
		nivelAcessoId = @nivelAcessoId,	
		usuarioDataCad = @usuarioDataCad,
		usuarioCadUsuarioId = @usuarioCadUsuarioId
	WHERE
		usuarioId = @usuarioId	
	
	SELECT @usuarioId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspUsuariosCrudExcluir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspUsuariosCrudExcluir]
	@usuarioId int
AS
BEGIN
	
	DELETE FROM
		Usuarios
	WHERE
		usuarioId = @usuarioId
	
	SELECT @usuarioId AS retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspUsuariosCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspUsuariosCrudInserir]
	@usuarioNome	varchar(100),
	@usuarioLogin	varchar(20),
	@usuarioSenha	varchar(20),
	@usuarioCpf	varchar(20),
	@nivelAcessoId	int,
	@usuarioDataCad	datetime,
	@usuarioCadUsuarioId int	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT usuarioId FROM Usuarios WHERE usuarioLogin = @usuarioLogin))
				RAISERROR('O Login do Usuário ja está cadastrado no Bando de Dados. Escolha outro Login para Cadastrar o Usuário',14,1);
				
				
			INSERT INTO Usuarios
			(
				usuarioNome,
				usuarioLogin,
				usuarioSenha,
				usuarioCpf,
				nivelAcessoId,
				usuarioDataCad,
				usuarioCadUsuarioId
			)
			VALUES
			(
				@usuarioNome,	
				@usuarioLogin,	
				@usuarioSenha,	
				@usuarioCpf,
				@nivelAcessoId,	
				@usuarioDataCad,
				@usuarioCadUsuarioId			
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspUsuariosPreencheNivelAcesso]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspUsuariosPreencheNivelAcesso]
	
AS
BEGIN
	select 
		nivelAcessoId, 
		nivelAcessoDescricao
	
	from
		NivelAcesso
	order by
		nivelAcessoDescricao asc
END



GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoAbreVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoAbreVenda]
	@atendenteVendaCabecalho varchar(50),
	@atendimentoVendaCabecalho	varchar(4),
	@statusVendaCabecalho	int,
	@usuarioId int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				INSERT INTO VendasCabecalho
					(
						atendimentoVendaCabecalho,
						statusVendaCabecalho,
						dataVendaCabecalho,
						usuarioId,
						atendenteVendaCabecalho
				
					)
					VALUES
					(
						@atendimentoVendaCabecalho,
						@statusVendaCabecalho,
						GETDATE(),
						@usuarioId,
						@atendenteVendaCabecalho
					)
				
					select @@IDENTITY as Retorno;
				
										
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoAbreVendaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaCabecalhoAbreVendaDelivery]
	@idVendaCabecalho int,
	@idCliente	int,
	@valorFrete decimal(18,2),
	@statusEntrega int = 0		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				INSERT INTO Entregas
					(
						idVendaCabecalho,
						idCliente,
						valorFrete,
						statusEntrega
				
					)
					VALUES
					(
						@idVendaCabecalho,
						@idCliente,
						@valorFrete,
						@statusEntrega
					)
				
					select @@IDENTITY as Retorno;				
				
										
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoAtualizaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoAtualizaDelivery]
	@idVendaCabecalho int, 
	@idFormaPagamento int,
	@trocoPara decimal(18,2),
	@troco decimal(18,2),
	@valorFrete decimal (18,2),
	@statusEntrega int			
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				Entregas
			set
				idFormaPagamento = @idFormaPagamento,
				valorFrete = @valorFrete,
				trocoPara = @trocoPara,
				troco = @troco,
				statusEntrega = @statusEntrega
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoCancelarDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoCancelarDelivery]
	@idVendaCabecalho int, 
	@motivoCancelamento varchar(150),
	@statusEntrega int			
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				Entregas
			set
				statusEntrega = @statusEntrega
			where
				idVendaCabecalho = @idVendaCabecalho
			
			
			insert into EntregaCancelada
				(
					idVendaCabecalho,
					motivoCancelamento
				)
				values
				(
					@idVendaCabecalho,
					@motivoCancelamento
				)

			select @@IDENTITY AS Retorno	

		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoCancelaVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoCancelaVenda]
	@idVenda	int,
	@statusVenda	int
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			UPDATE 
				VendasCabecalho
			SET		
				statusVendaCabecalho = @statusVenda
						
			WHERE
				idVendaCabecalho = @idVenda		
			
			update
				VendaDetalhes
			set
				statusVendaDetalhes = @statusVenda
			where
				idVendaCabecalhoDetalhes = @idVenda
			
			SELECT @idVenda AS Retorno
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoChecaAtend]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaCabecalhoChecaAtend]
	@atendimentoVendaCabecalho	varchar(4),
	@statusVendaCabecalho	int	= 1
				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(select idVendaCabecalho from VendasCabecalho where atendimentoVendaCabecalho = @atendimentoVendaCabecalho and statusVendaCabecalho = @statusVendaCabecalho))
				begin
				
				    declare @idVenda int = 1;
				    
				    Select @idVenda as Retorno
				    				    
				end
			else
					begin
						declare @idVenda2 int = 0;
						
						Select @idVenda2 as Retorno
						
					end
						
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoChecaAtendTipo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaCabecalhoChecaAtendTipo]
				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			select * from AtendimentoTipo
						
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoChecaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoChecaDelivery]
	@idCabecalho int	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
				    Select 
						idEntrega, valorFrete, Clientes.nomeCliente, Clientes.idCliente, telefoneCliente, celularCliente, Clientes.enderecoCliente, Clientes.complementoCliente, Clientes.bairroCliente, Clientes.pontoReferenciaCliente
					from
						Entregas
					join
						Clientes
					on
						Entregas.idCliente = Clientes.idCliente
					where 
						Entregas.idVendaCabecalho = @idCabecalho
				
						
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoChecaVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspVendaCabecalhoChecaVenda]
	@atendimentoVendaCabecalho	varchar(4),
	@statusVendaCabecalho	int,
	@usuarioId int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(select idVendaCabecalho from VendasCabecalho where atendimentoVendaCabecalho = @atendimentoVendaCabecalho and statusVendaCabecalho = @statusVendaCabecalho))
				begin
				    declare @idVenda int;
				    
				    Select idVendaCabecalho from VendasCabecalho where atendimentoVendaCabecalho = @atendimentoVendaCabecalho and statusVendaCabecalho = @statusVendaCabecalho
				end
			else
					begin
						RAISERROR('Error ao Checar Atendimento',14,1);
					end
						
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoChecaVendaImprimir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoChecaVendaImprimir]
	@atendimentoVendaCabecalho	varchar(4),
	@statusVendaCabecalho	int	= 1			
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			Select idVendaCabecalho from VendasCabecalho where atendimentoVendaCabecalho = @atendimentoVendaCabecalho and statusVendaCabecalho = @statusVendaCabecalho
									
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoDeliveryPegarProdutos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoDeliveryPegarProdutos]
	@idVendaCabecalho int
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
				select 
					Produto.codigoProduto, Produto.descricaoProduto, quantidadeVendaDetalhes, 
					especialVendaDetalhes, extraVendaDetalhes
				from
					VendaDetalhes
				join
					Produto
				on
					VendaDetalhes.codigoProduto = Produto.codigoProduto				
				where 
					idVendaCabecalhoDetalhes = @idVendaCabecalho and statusVendaDetalhes = 3
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoDeliverySaiuEntrega]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoDeliverySaiuEntrega]
	@statusEntrega int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				select 
					VendasCabecalho.idVendaCabecalho, nomeCliente, enderecoCliente, complementoCliente,
					bairroCliente, pontoReferenciaCliente, telefoneCliente, celularCliente, dataVendaCabecalho, atendenteVendaCabecalho,
				    totalVendaCabecalho, apelidoEntregador, horaSaida, statusEntrega
				from
					Entregas
				join
					VendasCabecalho
				on
					VendasCabecalho.idVendaCabecalho = Entregas.idVendaCabecalho
				join
					Clientes
				on
					Clientes.idCliente = VendasCabecalho.idCliente
				join
					Entregador
				on
					Entregador.idEntregador = Entregas.idEntregador
				where 
					Entregas.statusEntrega = @statusEntrega
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoDeliveryStatus]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoDeliveryStatus]
	@statusEntrega int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				select 
					VendasCabecalho.idVendaCabecalho, nomeCliente, enderecoCliente, complementoCliente,
					bairroCliente, pontoReferenciaCliente, telefoneCliente, celularCliente, 
					VendasCabecalho.horaVendaCabecalho, VendasCabecalho.atendenteVendaCabecalho, VendasCabecalho.totalVendaCabecalho, statusEntrega
				from
					Entregas
				join
					VendasCabecalho
				on
					VendasCabecalho.idVendaCabecalho = Entregas.idVendaCabecalho
				join
					Clientes
				on
					Clientes.idCliente = VendasCabecalho.idCliente
				where 
					Entregas.statusEntrega = @statusEntrega
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoEntradaCancelada]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaCabecalhoEntradaCancelada]
	@idVendaCabecalho int, 
	@motivoCancelamento varchar(150)
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			insert into EntregaCancelada
				(
					idVendaCabecalho,
					motivoCancelamento
				)
				values
				(
					@idVendaCabecalho,
					@motivoCancelamento
				)

			select @@IDENTITY AS Retorno	

		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoEntregaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoEntregaDelivery]
	@idVendaCabecalho int, 
	@idEntregador int,
	@horaSaida datetime,
	@statusEntrega int			
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				Entregas
			set
				idEntregador = @idEntregador,
				horaSaida = @horaSaida,
				statusEntrega = @statusEntrega
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoEntregaItensDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoEntregaItensDelivery]
	@idVendaCabecalho int, 
	@statusEntrega int			
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
		IF(EXISTS(SELECT idVendaDetalhes FROM VendaDetalhes WHERE statusVendaDetalhes = 2 and idVendaCabecalhoDetalhes = @idVendaCabecalho))
				RAISERROR('0',14,1);

			update
				Entregas
			set
				statusEntrega = @statusEntrega
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	

		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoEnviarVendaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoEnviarVendaDelivery]
	@idVendaCabecalho int,
	@idCliente int = null,
	@horaVendaCabecalho time,
	@idCartao int = null,
	@valorVendaCabecalho decimal (18,2),
	@descontoVendaCabecalho decimal (18,2),
	@totalVendaCabecalho decimal (18,2),
	@statusVendaCabecalho int,
	@usuarioId int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				VendasCabecalho
			set
				idCliente =	@idCliente,
				idCartao = @idCartao,
				horaVendaCabecalho = @horaVendaCabecalho,
				valorVendaCabecalho = @valorVendaCabecalho,
				descontoVendaCabecalho = @descontoVendaCabecalho,
				totalVendaCabecalho = @totalVendaCabecalho,
				statusVendaCabecalho = @statusVendaCabecalho,
				usuarioId = @usuarioId
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoFinalizaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaCabecalhoFinalizaDelivery]
	@idVendaCabecalho int, 
	@horaVolta datetime,
	@statusEntrega int			
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				Entregas
			set
				horaVolta = @horaVolta,
				statusEntrega = @statusEntrega
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoFinalizaVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoFinalizaVenda]
	@idVendaCabecalho int,
	@idCliente int = null,
	@idCartao int = null,
	@valorVendaCabecalho decimal (18,2),
	@descontoVendaCabecalho decimal (18,2),
	@totalVendaCabecalho decimal (18,2),
	@statusVendaCabecalho int,
	@usuarioId int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				VendasCabecalho
			set
				idCliente =	@idCliente,
				idCartao = @idCartao,
				valorVendaCabecalho = @valorVendaCabecalho,
				descontoVendaCabecalho = @descontoVendaCabecalho,
				totalVendaCabecalho = @totalVendaCabecalho,
				statusVendaCabecalho = @statusVendaCabecalho,
				usuarioId = @usuarioId
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoFinalizaVendaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoFinalizaVendaDelivery]
	@idVendaCabecalho int, 
	@statusVendaCabecalho int,
	@usuarioId int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				VendasCabecalho
			set
				statusVendaCabecalho = @statusVendaCabecalho,
				usuarioId = @usuarioId
			where
				idVendaCabecalho = @idVendaCabecalho
			
			select @idVendaCabecalho AS Retorno	
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoGetTipoPagamento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspVendaCabecalhoGetTipoPagamento]
	@idVendaCabecalho INT
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				SELECT
					descricaoVCTipoPagamento,
					valorVCTipoPagamento
				FROM 
					VendaCabecalhoTipoPagamentos 
				WHERE 
					idVendaCabecalhoTipoPagamento = @idVendaCabecalho
				
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoProdutosListaCod]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspVendaCabecalhoProdutosListaCod]
	@codigoProduto	int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				SELECT
					codigoProduto, 
					descricaoProduto,
					valorVendaProduto,
					controlaEstoqueProduto,
					estoqueProduto,
					estoqueCriticoProduto,
					manufaturadoProduto
				FROM 
					Produto 
				WHERE 
					codigoProduto = @codigoProduto			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoReemprimeVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoReemprimeVenda]
	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
		declare @dataAtual date;
		declare @dataLimite date;
	
		SET @dataAtual =(select dateadd(day,-1,GETDATE()))
		SET @dataLimite =(select dateadd(day,1,GETDATE()))
				
				SELECT
					idVendaCabecalho,
					atendimentoVendaCabecalho,
					dataVendaCabecalho,
					valorVendaCabecalho,
					descontoVendaCabecalho,
					totalVendaCabecalho,
					atendenteVendaCabecalho
				FROM 
					VendasCabecalho 
				WHERE 
					statusVendaCabecalho = 2 and dataVendaCabecalho > @dataAtual and dataVendaCabecalho < @dataLimite
				ORDER BY 
					dataVendaCabecalho desc
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoTipoPagamento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaCabecalhoTipoPagamento]
	@idVendaCabecalho int, 
	@descricaoVCTipoPagamento varchar(50),
	@valorVCTipoPagamento decimal(18,2)	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
				
			insert into
				VendaCabecalhoTipoPagamentos
				(
					idVendaCabecalhoTipoPagamento,
					descricaoVCTipoPagamento,
					valorVCTipoPagamento
				)
				values
				(
					@idVendaCabecalho,
					@descricaoVCTipoPagamento,
					@valorVCTipoPagamento
				)

				select @idVendaCabecalho as Retorno
						
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesAtualizaItemExtra]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaDetalhesAtualizaItemExtra]
	@idVendaCabecalhoDetalhes	int,
	@extraVendaDetalhes varchar(100),
	@qtdExtraVendaDetalhes	decimal(18, 2)	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			UPDATE 
				VendaDetalhes
			SET
				extraVendaDetalhes =  @extraVendaDetalhes,
				qtdExtraVendaDetalhes = @qtdExtraVendaDetalhes
			WHERE 
				idVendaCabecalhoDetalhes =  @idVendaCabecalhoDetalhes and statusVendaDetalhes = 1 or statusVendaDetalhes = 2
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesBaixaProduto]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesBaixaProduto]
	@codigoProduto	int,
	@estoqueProduto decimal(18,2)					
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN
		
			UPDATE 
				Produto
			SET		
				estoqueProduto = @estoqueProduto
						
			WHERE
				codigoProduto = @codigoProduto	
				
				select @codigoProduto AS Retorno
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesChecaItensEnviados]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesChecaItensEnviados]
	@idVendaCabecalhoDetalhes	int	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				COUNT(idVendaCabecalhoDetalhes) as qtd 
			from
				VendaDetalhes
			where idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes and statusVendaDetalhes != 3 and statusVendaDetalhes != 6		
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesCrudInserir]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesCrudInserir]
	@idVendaCabecalhoDetalhes	int,
	@codigoProduto	int,
	@especialVendaDetalhes	varchar(50) = null,
	@extraVendaDetalhes varchar(100) = null,
	@quantidadeVendaDetalhes	decimal(18, 2),
	@valorUnitarioVendaDetalhes	decimal(18, 2),
	@valorTotalVendaDetalhes	decimal(18, 2),
	@dataEnvioVendaDetalhes datetime,
	@statusVendaDetalhes	int
					
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO VendaDetalhes
			(
				idVendaCabecalhoDetalhes,
				codigoProduto,
				especialVendaDetalhes,
				extraVendaDetalhes,
				quantidadeVendaDetalhes,	
				valorUnitarioVendaDetalhes,	
				valorTotalVendaDetalhes,
				dataEnvioVendaDetalhes,
				statusVendaDetalhes					
			)
			VALUES
			(
				@idVendaCabecalhoDetalhes,
				@codigoProduto,
				@especialVendaDetalhes,
				@extraVendaDetalhes,
				@quantidadeVendaDetalhes,	
				@valorUnitarioVendaDetalhes,	
				@valorTotalVendaDetalhes,
				@dataEnvioVendaDetalhes,
				@statusVendaDetalhes
			)
			
			SELECT @idVendaCabecalhoDetalhes AS Retorno;			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesCrudInserirExtra]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaDetalhesCrudInserirExtra]
	@idVendaCabecalhoDetalhes	int,
	@codigoProduto	int,
	@especialVendaDetalhes	varchar(50) = null,
	@extraVendaDetalhes varchar(100) = null,
	@quantidadeVendaDetalhes	decimal(18, 2),
	@valorUnitarioVendaDetalhes	decimal(18, 2),
	@valorTotalVendaDetalhes	decimal(18, 2),
	@dataEnvioVendaDetalhes datetime,
	@statusVendaDetalhes	int
					
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO VendaDetalhes
			(
				idVendaCabecalhoDetalhes,
				codigoProduto,
				especialVendaDetalhes,
				extraVendaDetalhes,
				quantidadeVendaDetalhes,	
				valorUnitarioVendaDetalhes,	
				valorTotalVendaDetalhes,
				dataEnvioVendaDetalhes,
				statusVendaDetalhes					
			)
			VALUES
			(
				@idVendaCabecalhoDetalhes,
				@codigoProduto,
				@especialVendaDetalhes,
				@extraVendaDetalhes,
				@quantidadeVendaDetalhes,	
				@valorUnitarioVendaDetalhes,	
				@valorTotalVendaDetalhes,
				@dataEnvioVendaDetalhes,
				@statusVendaDetalhes
			)
			
			SELECT @idVendaCabecalhoDetalhes AS Retorno;			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesEntregarVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspVendaDetalhesEntregarVenda]
	@idVendaDetalhes	int,
	@dataEnvioVendaDetalhes datetime,
	@statusVendaDetalhes	int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			UPDATE 
				VendaDetalhes
			SET 
				statusVendaDetalhes = @statusVendaDetalhes, dataEnvioVendaDetalhes = @dataEnvioVendaDetalhes
			WHERE 
				idVendaDetalhes = @idVendaDetalhes
            
            select @idVendaDetalhes as Retorno
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesEnviarVenda]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesEnviarVenda]
	@idVendaCabecalhoDetalhes	int,
	@statusVendaDetalhes	int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			UPDATE 
				VendaDetalhes
			SET 
				statusVendaDetalhes = @statusVendaDetalhes
			WHERE 
				idVendaCabecalhoDetalhes =  @idVendaCabecalhoDetalhes and statusVendaDetalhes = 1;
            
			UPDATE 
				VendasCabecalho
			SET 
				horaVendaCabecalho = GETDATE()
			WHERE 
				idVendaCabecalho =  @idVendaCabecalhoDetalhes

		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesExcluirItens]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaDetalhesExcluirItens]
	@idVendaCabecalhoDetalhes int
AS
BEGIN
	
	DELETE FROM
		VendaDetalhes
	WHERE
		idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes
	
	SELECT @idVendaCabecalhoDetalhes AS Retorno
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesFinalizarItens]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaDetalhesFinalizarItens]
	@idVendaCabecalhoDetalhes int, 
	@statusVendaDetalhes int
	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			update
				VendaDetalhes
			set
				statusVendaDetalhes = @statusVendaDetalhes
			where
				idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes
			
			select @idVendaCabecalhoDetalhes AS Retorno	
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesListaItens]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesListaItens]
	@idVendaCabecalhoDetalhes int
AS
BEGIN
	SELECT 
		idVendaCabecalhoDetalhes,
		Produto.codigoProduto,
		Produto.descricaoProduto,
		especialVendaDetalhes,
		extraVendaDetalhes,
		quantidadeVendaDetalhes,
		valorUnitarioVendaDetalhes,
		valorTotalVendaDetalhes,
		dataEnvioVendaDetalhes,
		statusVendaDetalhes,
		atendenteVendaCabecalho
	FROM 
		VendaDetalhes
	join
		Produto
	on
		Produto.codigoProduto = VendaDetalhes.codigoProduto	
	join
		VendasCabecalho
	on
		VendasCabecalho.idVendaCabecalho = @idVendaCabecalhoDetalhes
	WHERE 
		idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes
END



GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesTempoEsperaPedido]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesTempoEsperaPedido]
	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				horaVendaCabecalho,
				atendenteVendaCabecalho
			from
				VendasCabecalho
			inner join
				VendaDetalhes
			on
				VendasCabecalho.idVendaCabecalho = VendaDetalhes.idVendaCabecalhoDetalhes				
			where 
				statusVendaCabecalho = 1 and VendaDetalhes.statusVendaDetalhes = 2 or VendaDetalhes.statusVendaDetalhes = 3
			group by
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				atendenteVendaCabecalho,
				horaVendaCabecalho
			order by
				horaVendaCabecalho asc	
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesTempoEsperaPedidoDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesTempoEsperaPedidoDelivery]
	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				horaVendaCabecalho,
				atendenteVendaCabecalho
			from
				VendasCabecalho
			inner join
				VendaDetalhes
			on
				VendasCabecalho.idVendaCabecalho = VendaDetalhes.idVendaCabecalhoDetalhes		
			where 
				statusVendaCabecalho = 1 and VendaDetalhes.statusVendaDetalhes = 2 and atendimentoVendaCabecalho = 'DV'
			group by
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				atendenteVendaCabecalho,
				horaVendaCabecalho
			order by
				horaVendaCabecalho asc	
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesTempoEsperaPedidoGrupos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesTempoEsperaPedidoGrupos]
	@grupoId int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				horaVendaCabecalho,
				atendenteVendaCabecalho
			from
				VendasCabecalho
			inner join
				VendaDetalhes
			on
				VendasCabecalho.idVendaCabecalho = VendaDetalhes.idVendaCabecalhoDetalhes		
			inner join
				Produto
			on
				Produto.codigoProduto = VendaDetalhes.codigoProduto
			inner join
				Grupo
			on
				Grupo.grupoId = Produto.grupoId		
			where 
				statusVendaCabecalho = 1 and VendaDetalhes.statusVendaDetalhes = 2 and Grupo.grupoId = @grupoId
			group by
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				atendenteVendaCabecalho,
				horaVendaCabecalho
			order by
				horaVendaCabecalho asc	
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesTempoEsperaPedidoTodos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesTempoEsperaPedidoTodos]
	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				horaVendaCabecalho,
				atendenteVendaCabecalho
			from
				VendasCabecalho
			inner join
				VendaDetalhes
			on
				VendasCabecalho.idVendaCabecalho = VendaDetalhes.idVendaCabecalhoDetalhes		
			--inner join
			--	Produto
			--on
			--	Produto.codigoProduto = VendaDetalhes.codigoProduto
			where 
				statusVendaCabecalho = 1 and VendaDetalhes.statusVendaDetalhes = 2
			group by
				idVendaCabecalho, 
				atendimentoVendaCabecalho,
				atendenteVendaCabecalho,
				horaVendaCabecalho
			order by
				horaVendaCabecalho asc	
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesVisualizarPedidos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesVisualizarPedidos]
	@idGrupo int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaDetalhes, 
				idVendaCabecalhoDetalhes, 
				VendasCabecalho.atendimentoVendaCabecalho, 
				VendaDetalhes.codigoProduto, 
				Produto.descricaoProduto, 
				extraVendaDetalhes, 
				especialVendaDetalhes, 
				quantidadeVendaDetalhes,
				atendenteVendaCabecalho
			from
				VendaDetalhes
			join
				VendasCabecalho
			on
				idVendaCabecalho = idVendaCabecalhoDetalhes
			join
				Produto
			on
				Produto.codigoProduto  = VendaDetalhes.codigoProduto
			where 
				statusVendaDetalhes = 2 and Produto.grupoId = @idGrupo
			order by
				dataEnvioVendaDetalhes			
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesVisualizarPedidosEntregaDelivery]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaDetalhesVisualizarPedidosEntregaDelivery]
	@idVendaCabecalhoDetalhes int	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaDetalhes, 
				idVendaCabecalhoDetalhes, 
				VendasCabecalho.atendimentoVendaCabecalho, 
				VendaDetalhes.codigoProduto, 
				Produto.descricaoProduto, 
				extraVendaDetalhes, 
				especialVendaDetalhes, 
				quantidadeVendaDetalhes,
				atendenteVendaCabecalho
			from
				VendaDetalhes
			join
				VendasCabecalho
			on
				idVendaCabecalho = idVendaCabecalhoDetalhes
			join
				Produto
			on
				Produto.codigoProduto  = VendaDetalhes.codigoProduto
			where 
				statusVendaDetalhes = 2 and idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes and VendasCabecalho.atendimentoVendaCabecalho = 'DV'
			order by
				dataEnvioVendaDetalhes			
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesVisualizarPedidosEntregaGrupo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspVendaDetalhesVisualizarPedidosEntregaGrupo]
	@idVendaCabecalhoDetalhes int,
	@idGrupo int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaDetalhes, 
				idVendaCabecalhoDetalhes, 
				VendasCabecalho.atendimentoVendaCabecalho, 
				VendaDetalhes.codigoProduto, 
				Produto.descricaoProduto, 
				extraVendaDetalhes, 
				especialVendaDetalhes, 
				quantidadeVendaDetalhes,
				atendenteVendaCabecalho
			from
				VendaDetalhes
			join
				VendasCabecalho
			on
				idVendaCabecalho = idVendaCabecalhoDetalhes
			join
				Produto
			on
				Produto.codigoProduto  = VendaDetalhes.codigoProduto
			where 
				statusVendaDetalhes = 2 and Produto.grupoId = @idGrupo and idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes
			order by
				dataEnvioVendaDetalhes			
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesVisualizarPedidosEntregaTodos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspVendaDetalhesVisualizarPedidosEntregaTodos]
	@idVendaCabecalhoDetalhes int
	
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
			
			select
				idVendaDetalhes, 
				idVendaCabecalhoDetalhes, 
				VendasCabecalho.atendimentoVendaCabecalho, 
				VendaDetalhes.codigoProduto, 
				Produto.descricaoProduto, 
				extraVendaDetalhes, 
				especialVendaDetalhes, 
				quantidadeVendaDetalhes,
				atendenteVendaCabecalho
			from
				VendaDetalhes
			join
				VendasCabecalho
			on
				idVendaCabecalho = idVendaCabecalhoDetalhes
			join
				Produto
			on
				Produto.codigoProduto  = VendaDetalhes.codigoProduto
			where 
				statusVendaDetalhes = 2 and idVendaCabecalhoDetalhes = @idVendaCabecalhoDetalhes and statusVendaDetalhes !=3
			order by
				dataEnvioVendaDetalhes			
            
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END





GO
/****** Object:  Table [dbo].[AtendimentoTipo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AtendimentoTipo](
	[idAtendimento] [int] IDENTITY(1,1) NOT NULL,
	[atendimento] [varchar](3) NULL,
	[buton] [varchar](50) NULL,
 CONSTRAINT [PK_AtendimentoTipo] PRIMARY KEY CLUSTERED 
(
	[idAtendimento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bairro]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bairro](
	[idBairro] [int] IDENTITY(1,1) NOT NULL,
	[nomeBairro] [varchar](50) NULL,
	[taxaBairro] [decimal](18, 2) NULL,
	[dataCadBairro] [datetime] NULL,
	[usuarioId] [int] NULL,
 CONSTRAINT [PK_Bairro] PRIMARY KEY CLUSTERED 
(
	[idBairro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Caixa]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caixa](
	[idCaixa] [int] IDENTITY(1,1) NOT NULL,
	[valorCaixaAbre] [decimal](18, 2) NOT NULL,
	[valorCaixaFecha] [decimal](18, 2) NULL,
	[dataCaixa] [datetime] NOT NULL,
	[horaCaixaAbre] [datetime] NOT NULL,
	[horaCaixaFecha] [datetime] NULL,
	[statusCaixa] [int] NOT NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Caixa] PRIMARY KEY CLUSTERED 
(
	[idCaixa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cartao]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cartao](
	[idCartao] [int] IDENTITY(1,1) NOT NULL,
	[nomeCartao] [varchar](30) NULL,
	[taxaCartao] [decimal](5, 1) NULL,
	[dataCadCartao] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Cartao] PRIMARY KEY CLUSTERED 
(
	[idCartao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[idCliente] [int] IDENTITY(1,1) NOT NULL,
	[nomeCliente] [varchar](200) NULL,
	[cpfCliente] [varchar](11) NULL,
	[cnpjCliente] [varchar](15) NULL,
	[identidadeCliente] [varchar](11) NULL,
	[telefoneCliente] [varchar](11) NULL,
	[celularCliente] [varchar](11) NULL,
	[cepCliente] [varchar](11) NULL,
	[enderecoCliente] [varchar](200) NULL,
	[complementoCliente] [varchar](100) NULL,
	[bairroCliente] [varchar](100) NULL,
	[municipioCliente] [varchar](50) NULL,
	[ufCliente] [varchar](2) NULL,
	[emailCliente] [varchar](100) NULL,
	[dataNascimentoCliente] [datetime] NULL,
	[pontoReferenciaCliente] [varchar](255) NULL,
	[dataCadastroCliente] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContasPagar]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContasPagar](
	[idContaPagar] [int] IDENTITY(1,1) NOT NULL,
	[idPlanoConta] [int] NULL,
	[nomeContaPagar] [varchar](100) NULL,
	[valorContaPagar] [decimal](18, 2) NULL,
	[vencimentoContaPagar] [datetime] NULL,
	[statusContaPagar] [bigint] NULL,
	[dataCadContaPagar] [datetime] NULL,
	[usuarioId] [int] NULL,
 CONSTRAINT [PK_ContasPagar] PRIMARY KEY CLUSTERED 
(
	[idContaPagar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContasPagas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContasPagas](
	[idContasPagas] [int] IDENTITY(1,1) NOT NULL,
	[idContaPagar] [int] NULL,
	[dataPagamentoContaPagas] [datetime] NULL,
	[valorPagoContaPagas] [decimal](18, 2) NULL,
	[dataCadContaPagas] [datetime] NULL,
	[usuarioId] [int] NULL,
 CONSTRAINT [PK_ConstasPagas] PRIMARY KEY CLUSTERED 
(
	[idContasPagas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EntradaProduto]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EntradaProduto](
	[idEntradaProduto] [int] IDENTITY(1,1) NOT NULL,
	[codigoProduto] [int] NOT NULL,
	[quantidadeProdutoEntrada] [decimal](18, 2) NULL,
	[motivoEntradaProduto] [varchar](100) NULL,
	[nfEntradaProduto] [varchar](30) NULL,
	[dataCadEntradaProduto] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_EntradaProduto] PRIMARY KEY CLUSTERED 
(
	[idEntradaProduto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EntregaCancelada]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EntregaCancelada](
	[idEntregaCancelada] [int] IDENTITY(1,1) NOT NULL,
	[idVendaCabecalho] [int] NOT NULL,
	[motivoCancelamento] [varchar](150) NOT NULL,
 CONSTRAINT [PK_EntregaCancelada] PRIMARY KEY CLUSTERED 
(
	[idEntregaCancelada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Entregador]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Entregador](
	[idEntregador] [int] IDENTITY(1,1) NOT NULL,
	[nomeEntregador] [varchar](200) NULL,
	[apelidoEntregador] [varchar](20) NULL,
	[cpfEntregador] [varchar](11) NULL,
	[identidadeEntregador] [varchar](11) NULL,
	[telefoneEntregador] [varchar](11) NULL,
	[celularEntregador] [varchar](11) NULL,
	[cepEntregador] [varchar](11) NULL,
	[enderecoEntregador] [varchar](200) NULL,
	[complementoEntregador] [varchar](100) NULL,
	[bairroEntregador] [varchar](100) NULL,
	[municipioEntregador] [varchar](50) NULL,
	[ufEntregador] [varchar](2) NULL,
	[emailEntregador] [varchar](100) NULL,
	[dataNascimentoEntregador] [datetime] NULL,
	[porcentEntregador] [decimal](18, 2) NULL,
	[statusEntregador] [bigint] NULL,
	[dataCadastroEntregador] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Entregador] PRIMARY KEY CLUSTERED 
(
	[idEntregador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Entregas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entregas](
	[idEntrega] [int] IDENTITY(1,1) NOT NULL,
	[idVendaCabecalho] [int] NULL,
	[idCliente] [int] NULL,
	[idEntregador] [int] NULL,
	[idFormaPagamento] [int] NULL,
	[valorFrete] [decimal](18, 2) NULL,
	[trocoPara] [decimal](18, 2) NULL,
	[troco] [decimal](18, 2) NULL,
	[horaSaida] [datetime] NULL,
	[horaVolta] [datetime] NULL,
	[statusEntrega] [int] NULL,
 CONSTRAINT [PK_Entregas] PRIMARY KEY CLUSTERED 
(
	[idEntrega] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Especiais]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Especiais](
	[especiaisId] [int] IDENTITY(1,1) NOT NULL,
	[especiaisDescricao] [varchar](50) NULL,
	[especiaisDataCad] [datetime] NOT NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Especiais] PRIMARY KEY CLUSTERED 
(
	[especiaisId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormaPagamento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormaPagamento](
	[formaPagamentoId] [int] IDENTITY(1,1) NOT NULL,
	[formaPagamentoDescricao] [varchar](30) NULL,
	[formaPagamentoDataCad] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_formaPagamento] PRIMARY KEY CLUSTERED 
(
	[formaPagamentoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fornecedor]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fornecedor](
	[idFornecedor] [int] IDENTITY(1,1) NOT NULL,
	[nomeFornecedor] [varchar](200) NULL,
	[cnpjFornecedor] [varchar](15) NULL,
	[cpfFornecedor] [varchar](11) NULL,
	[inscricaoEstadualFornecedor] [varchar](14) NULL,
	[telefoneFornecedor] [varchar](11) NULL,
	[faxFornecedor] [varchar](11) NULL,
	[enderecoFornecedor] [varchar](200) NULL,
	[complementoFornecedor] [varchar](100) NULL,
	[bairroFornecedor] [varchar](100) NULL,
	[cepFornecedor] [varchar](11) NULL,
	[municipioFornecedor] [varchar](50) NULL,
	[ufFornecedor] [varchar](2) NULL,
	[emailFornecedor] [varchar](100) NULL,
	[contatoFornecedor] [varchar](50) NULL,
	[dataCadastroFornecedor] [datetime] NULL,
	[pontoReferenciaFornecedor] [varchar](255) NULL,
	[informAdicionaisFornecedor] [varchar](50) NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Fornecedor] PRIMARY KEY CLUSTERED 
(
	[idFornecedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Grupo]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Grupo](
	[grupoId] [int] IDENTITY(1,1) NOT NULL,
	[grupoDescricao] [varchar](50) NULL,
	[grupoDataCad] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Grupo] PRIMARY KEY CLUSTERED 
(
	[grupoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MeuLanche]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MeuLanche](
	[idMeuLanche] [int] IDENTITY(1,1) NOT NULL,
	[nomeMeuLanche] [varchar](200) NULL,
	[cnpjMeuLanche] [varchar](15) NULL,
	[inscricaoEstadualMeuLanche] [varchar](14) NULL,
	[telefoneMeuLanche] [varchar](11) NULL,
	[faxMeuLanche] [varchar](11) NULL,
	[enderecoMeuLanche] [varchar](200) NULL,
	[complementoMeuLanche] [varchar](100) NULL,
	[bairroMeuLanche] [varchar](100) NULL,
	[cepMeuLanche] [varchar](11) NULL,
	[municipioMeuLanche] [varchar](50) NULL,
	[ufMeuLanche] [varchar](2) NULL,
	[emailMeuLanche] [varchar](100) NULL,
	[contatoMeuLanche] [varchar](50) NULL,
	[localLogoMeuLanche] [varchar](150) NULL,
 CONSTRAINT [PK_MeuLanche] PRIMARY KEY CLUSTERED 
(
	[idMeuLanche] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NivelAcesso]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NivelAcesso](
	[nivelAcessoId] [int] IDENTITY(1,1) NOT NULL,
	[nivelAcessoDescricao] [varchar](50) NULL,
	[dataCadAcessoNivel] [datetime] NULL,
	[idUsuario] [int] NULL,
 CONSTRAINT [PK_NivelAcesso] PRIMARY KEY CLUSTERED 
(
	[nivelAcessoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pagamento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pagamento](
	[idPagamento] [int] IDENTITY(1,1) NOT NULL,
	[idCodPlanoConta] [int] NOT NULL,
	[idTipoPagamento] [int] NOT NULL,
	[idFornecedor] [int] NOT NULL,
	[numDocPagamento] [varchar](50) NULL,
	[valorTotalPagamento] [decimal](18, 2) NULL,
	[valorJuroPagamento] [decimal](18, 2) NULL,
	[valorMultaPagamento] [decimal](18, 2) NULL,
	[valorDescontoPagamento] [decimal](18, 2) NULL,
	[valorPagoPagamento] [decimal](18, 2) NULL,
	[numChequePagamento] [int] NULL,
	[chequeNominalPagamento] [varchar](50) NULL,
	[dataEmissaoPagamento] [datetime] NULL,
	[dataLancamentoPagamento] [datetime] NULL,
	[dataVencimentoPagamento] [datetime] NULL,
	[dataPagamento] [datetime] NULL,
	[dataChequePagamento] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Pagamento] PRIMARY KEY CLUSTERED 
(
	[idPagamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pedidos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pedidos](
	[pedidoId] [int] IDENTITY(1,1) NOT NULL,
	[pedidoAtendimento] [varchar](2) NOT NULL,
	[pedidoValor] [decimal](18, 2) NULL,
	[pedidoStatus] [int] NOT NULL,
	[pedidoDataCad] [datetime] NOT NULL,
	[usuarioId] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Pedido] PRIMARY KEY CLUSTERED 
(
	[pedidoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PermissaoAcesso]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PermissaoAcesso](
	[permissaoAcessoId] [int] IDENTITY(1,1) NOT NULL,
	[nivelAcessoId] [int] NULL,
	[buton] [varchar](50) NULL,
	[ativo] [bigint] NULL,
	[cadastrar] [bigint] NULL,
	[alterar] [bigint] NULL,
	[excluir] [bigint] NULL,
	[pesquisar] [bigint] NULL,
	[relatorios] [bigint] NULL,
	[graficos] [bigint] NULL,
	[financeiro] [bigint] NULL,
 CONSTRAINT [PK_PermissoesAcesso] PRIMARY KEY CLUSTERED 
(
	[permissaoAcessoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlanoConta]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlanoConta](
	[idPlanoConta] [int] IDENTITY(1,1) NOT NULL,
	[codPlanoConta] [int] NOT NULL,
	[descricaoPlanoConta] [varchar](50) NULL,
	[dataCadPlanoConta] [datetime] NULL,
	[usuarioId] [int] NULL,
 CONSTRAINT [PK_PlanoConta_1] PRIMARY KEY CLUSTERED 
(
	[idPlanoConta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Produto]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Produto](
	[codigoProduto] [int] NOT NULL,
	[undMedidaId] [int] NOT NULL,
	[idFornecedor] [int] NOT NULL,
	[grupoId] [int] NOT NULL,
	[descricaoProduto] [varchar](100) NULL,
	[valorCompraProduto] [decimal](18, 2) NULL,
	[valorVendaProduto] [decimal](18, 2) NULL,
	[estoqueProduto] [decimal](18, 2) NULL,
	[estoqueCriticoProduto] [decimal](18, 2) NULL,
	[dataCadastroProduto] [datetime] NULL,
	[controlaEstoqueProduto] [char](1) NULL,
	[manufaturadoProduto] [char](1) NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Produto_1] PRIMARY KEY CLUSTERED 
(
	[codigoProduto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProdutoManufaturado]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProdutoManufaturado](
	[codProduto] [int] NOT NULL,
	[codProdutoManufaturado] [int] NOT NULL,
	[quantidadeProdutoManufaturado] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Recebimento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recebimento](
	[idRecebimento] [int] IDENTITY(1,1) NOT NULL,
	[numDocRecebimento] [nchar](10) NULL,
	[idTipoPagamento] [int] NOT NULL,
	[idCliente] [int] NOT NULL,
	[valorTotalRecebimento] [decimal](18, 2) NULL,
	[valorJuroRecebimento] [decimal](18, 2) NULL,
	[valorMultaRecebimento] [decimal](18, 2) NULL,
	[valorDescontoPRecebimento] [decimal](18, 2) NULL,
	[valorRecebidoRecebimento] [decimal](18, 2) NULL,
	[dataEmissaoRecebimento] [datetime] NULL,
	[dataLancamentoRecebimento] [datetime] NULL,
	[dataVencimentoRecebimento] [datetime] NULL,
	[dataRecebimento] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Recebimento] PRIMARY KEY CLUSTERED 
(
	[idRecebimento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Registro]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Registro](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[razãoSocial] [varchar](100) NULL,
	[nomeFantasia] [varchar](50) NULL,
	[responsável] [varchar](50) NULL,
	[cnpj] [varchar](17) NULL,
	[telefone] [varchar](12) NULL,
	[celular] [varchar](12) NULL,
	[email] [varchar](100) NULL,
	[cidade] [varchar](50) NULL,
	[uf] [varchar](2) NULL,
	[logo] [varchar](255) NULL,
 CONSTRAINT [PK_Registro] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Retirada]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Retirada](
	[idRetirada] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[valorRetirada] [decimal](18, 2) NOT NULL,
	[dataRetirada] [datetime] NOT NULL,
	[motivoRetirada] [varchar](255) NOT NULL,
	[administradorRetirada] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Retidada] PRIMARY KEY CLUSTERED 
(
	[idRetirada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SaidaProduto]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SaidaProduto](
	[idSaidaProduto] [int] IDENTITY(1,1) NOT NULL,
	[codigoProduto] [int] NOT NULL,
	[quantidadeProdutoSaida] [decimal](18, 2) NULL,
	[motivoSaida] [varchar](100) NULL,
	[dataCadSaida] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_SaidaEntrada] PRIMARY KEY CLUSTERED 
(
	[idSaidaProduto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Serial]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Serial](
	[idSerial] [int] IDENTITY(1,1) NOT NULL,
	[serialInstalacao] [text] NULL,
	[serialAtivacao] [text] NULL,
 CONSTRAINT [PK_Serial] PRIMARY KEY CLUSTERED 
(
	[idSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TempoVendas]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempoVendas](
	[idVendaCabecalhoTempo] [int] NOT NULL,
	[dataEnvio] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tipoImpressao]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipoImpressao](
	[tipo] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoPagamento]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoPagamento](
	[idTipoPagamento] [int] IDENTITY(1,1) NOT NULL,
	[descricaoTipoPagamento] [varchar](50) NULL,
	[dataCadTipoPagamento] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_TipoPagamento] PRIMARY KEY CLUSTERED 
(
	[idTipoPagamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UndMedida]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UndMedida](
	[undMedidaId] [int] IDENTITY(1,1) NOT NULL,
	[undMedidaDescricao] [varchar](5) NULL,
	[undMedidaDataCad] [datetime] NULL,
	[usuarioId] [int] NOT NULL,
 CONSTRAINT [PK_UndMedida] PRIMARY KEY CLUSTERED 
(
	[undMedidaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[usuarioId] [int] IDENTITY(1,1) NOT NULL,
	[usuarioNome] [varchar](100) NULL,
	[usuarioLogin] [varchar](20) NULL,
	[usuarioSenha] [varchar](20) NULL,
	[usuarioCpf] [varchar](20) NULL,
	[nivelAcessoId] [int] NOT NULL,
	[usuarioDataCad] [datetime] NULL,
	[usuarioCadUsuarioId] [int] NULL,
 CONSTRAINT [PK_UsuarioId] PRIMARY KEY CLUSTERED 
(
	[usuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VendaCabecalhoTipoPagamentos]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VendaCabecalhoTipoPagamentos](
	[idVendaCabecalhoTipoPagamento] [int] NOT NULL,
	[descricaoVCTipoPagamento] [varchar](50) NOT NULL,
	[valorVCTipoPagamento] [decimal](18, 2) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VendaDetalhes]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VendaDetalhes](
	[idVendaDetalhes] [int] IDENTITY(1,1) NOT NULL,
	[idVendaCabecalhoDetalhes] [int] NOT NULL,
	[codigoProduto] [int] NOT NULL,
	[especialVendaDetalhes] [varchar](50) NULL,
	[extraVendaDetalhes] [varchar](100) NULL,
	[quantidadeVendaDetalhes] [decimal](18, 2) NOT NULL,
	[valorUnitarioVendaDetalhes] [decimal](18, 2) NOT NULL,
	[valorTotalVendaDetalhes] [decimal](18, 2) NOT NULL,
	[dataEnvioVendaDetalhes] [datetime] NULL,
	[statusVendaDetalhes] [int] NOT NULL,
 CONSTRAINT [PK_VendaDetalhes] PRIMARY KEY CLUSTERED 
(
	[idVendaDetalhes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VendasCabecalho]    Script Date: 12/12/2014 20:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VendasCabecalho](
	[idVendaCabecalho] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NULL,
	[idCartao] [int] NULL,
	[atendimentoVendaCabecalho] [varchar](4) NULL,
	[dataVendaCabecalho] [date] NULL,
	[horaVendaCabecalho] [time](7) NULL,
	[valorVendaCabecalho] [decimal](18, 2) NULL,
	[descontoVendaCabecalho] [decimal](18, 2) NULL,
	[totalVendaCabecalho] [decimal](18, 2) NULL,
	[statusVendaCabecalho] [int] NULL,
	[usuarioId] [int] NULL,
	[atendenteVendaCabecalho] [varchar](50) NULL,
 CONSTRAINT [PK_VendasCabecalho] PRIMARY KEY CLUSTERED 
(
	[idVendaCabecalho] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET IDENTITY_INSERT [dbo].[AtendimentoTipo] ON 
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (1, N'M1', N'btnPedM1')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (2, N'M2', N'btnPedM2')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (3, N'M3', N'btnPedM3')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (4, N'M4', N'btnPedM4')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (5, N'M5', N'btnPedM5')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (6, N'M6', N'btnPedM6')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (7, N'M7', N'btnPedM7')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (8, N'M8', N'btnPedM8')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (9, N'M9', N'btnPedM9')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (10, N'M10', N'btnPedM10')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (11, N'M11', N'btnPedM11')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (12, N'M12', N'btnPedM12')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (13, N'M13', N'btnPedM13')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (14, N'M14', N'btnPedM14')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (15, N'M15', N'btnPedM15')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (16, N'M16', N'btnPedM16')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (17, N'M17', N'btnPedM17')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (18, N'M18', N'btnPedM18')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (19, N'M19', N'btnPedM19')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (20, N'M20', N'btnPedM20')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (21, N'M21', N'btnPedM21')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (22, N'M22', N'btnPedM22')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (23, N'M23', N'btnPedM23')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (24, N'M24', N'btnPedM24')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (25, N'M25', N'btnPedM25')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (26, N'M26', N'btnPedM26')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (27, N'M27', N'btnPedM27')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (28, N'M28', N'btnPedM28')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (29, N'M29', N'btnPedM29')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (30, N'M30', N'btnPedM30')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (31, N'M31', N'btnPedM31')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (32, N'M32', N'btnPedM32')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (33, N'M33', N'btnPedM33')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (34, N'M34', N'btnPedM34')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (35, N'M35', N'btnPedM35')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (36, N'M36', N'btnPedM36')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (37, N'B1', N'btnPedB1')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (38, N'B2', N'btnPedB2')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (39, N'B3', N'btnPedB3')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (40, N'B4', N'btnPedB4')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (41, N'B5', N'btnPedB5')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (42, N'B6', N'btnPedB6')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (43, N'B7', N'btnPedB7')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (44, N'B8', N'btnPedB8')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (45, N'B9', N'btnPedB9')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (46, N'B10', N'btnPedB10')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (47, N'B11', N'btnPedB11')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (48, N'B12', N'btnPedB12')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (49, N'B13', N'btnPedB13')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (50, N'B14', N'btnPedB14')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (51, N'B15', N'btnPedB15')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (52, N'B16', N'btnPedB16')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (53, N'B17', N'btnPedB17')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (54, N'B18', N'btnPedB18')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (55, N'C1', N'btnPedC1')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (56, N'C2', N'btnPedC2')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (57, N'C3', N'btnPedC3')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (58, N'C4', N'btnPedC4')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (59, N'C5', N'btnPedC5')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (60, N'C6', N'btnPedC6')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (61, N'C7', N'btnPedC7')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (62, N'C8', N'btnPedC8')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (63, N'C9', N'btnPedC9')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (64, N'C10', N'btnPedC10')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (65, N'C11', N'btnPedC11')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (66, N'C12', N'btnPedC12')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (67, N'C13', N'btnPedC13')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (68, N'C14', N'btnPedC14')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (69, N'C15', N'btnPedC15')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (70, N'C16', N'btnPedC16')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (71, N'C17', N'btnPedC17')
INSERT [dbo].[AtendimentoTipo] ([idAtendimento], [atendimento], [buton]) VALUES (72, N'C18', N'btnPedC18')
SET IDENTITY_INSERT [dbo].[AtendimentoTipo] OFF

SET IDENTITY_INSERT [dbo].[Cartao] ON 
INSERT [dbo].[Cartao] ([idCartao], [nomeCartao], [taxaCartao], [dataCadCartao], [usuarioId]) VALUES (1, N'VISA', CAST(2.5 AS Decimal(5, 1)), CAST(N'2014-12-12 19:35:33.063' AS DateTime), 3)
INSERT [dbo].[Cartao] ([idCartao], [nomeCartao], [taxaCartao], [dataCadCartao], [usuarioId]) VALUES (2, N'MASTER', CAST(2.5 AS Decimal(5, 1)), CAST(N'2014-12-12 19:35:41.057' AS DateTime), 3)
INSERT [dbo].[Cartao] ([idCartao], [nomeCartao], [taxaCartao], [dataCadCartao], [usuarioId]) VALUES (3, N'ELO', CAST(2.0 AS Decimal(5, 1)), CAST(N'2014-12-12 19:35:48.480' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Cartao] OFF

SET IDENTITY_INSERT [dbo].[Clientes] ON 
INSERT [dbo].[Clientes] ([idCliente], [nomeCliente], [cpfCliente], [cnpjCliente], [identidadeCliente], [telefoneCliente], [celularCliente], [cepCliente], [enderecoCliente], [complementoCliente], [bairroCliente], [municipioCliente], [ufCliente], [emailCliente], [dataNascimentoCliente], [pontoReferenciaCliente], [dataCadastroCliente], [usuarioId]) VALUES (1, N'FRANCISCO RICARDO DE SOUZA', N'72173084291', N'', N'15794326', N'9232382349', N'92981629178', N'69042050', N'RUA VALENCIA Nº 02', N'QD 64 CJ CAMPOS ELISEOS', N'PLANALTO', N'MANAUS', N'AM', N'RICARDO.SOUZA1910@HOTMAIL.COM', CAST(N'1982-11-15 23:20:48.000' AS DateTime), N'PROXIMO A GOLFINHO RADIO TAXI', CAST(N'2014-12-03 23:22:22.587' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Clientes] OFF

SET IDENTITY_INSERT [dbo].[FormaPagamento] ON 
INSERT [dbo].[FormaPagamento] ([formaPagamentoId], [formaPagamentoDescricao], [formaPagamentoDataCad], [usuarioId]) VALUES (1, N'DINHEIRO', CAST(N'2014-12-03 23:23:20.573' AS DateTime), 3)
INSERT [dbo].[FormaPagamento] ([formaPagamentoId], [formaPagamentoDescricao], [formaPagamentoDataCad], [usuarioId]) VALUES (2, N'CARTÃO', CAST(N'2014-12-03 23:23:26.647' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[FormaPagamento] OFF

SET IDENTITY_INSERT [dbo].[Grupo] ON 
INSERT [dbo].[Grupo] ([grupoId], [grupoDescricao], [grupoDataCad], [usuarioId]) VALUES (1, N'SANDUICHES', CAST(N'2014-12-03 16:53:33.817' AS DateTime), 3)
INSERT [dbo].[Grupo] ([grupoId], [grupoDescricao], [grupoDataCad], [usuarioId]) VALUES (2, N'BEBIDAS', CAST(N'2014-12-03 16:53:43.063' AS DateTime), 3)
INSERT [dbo].[Grupo] ([grupoId], [grupoDescricao], [grupoDataCad], [usuarioId]) VALUES (3, N'DIVERSOS', CAST(N'2014-12-03 16:54:10.597' AS DateTime), 3)
INSERT [dbo].[Grupo] ([grupoId], [grupoDescricao], [grupoDataCad], [usuarioId]) VALUES (4, N'EXTRAS', CAST(N'2014-12-09 16:39:05.237' AS DateTime), 3)
INSERT [dbo].[Grupo] ([grupoId], [grupoDescricao], [grupoDataCad], [usuarioId]) VALUES (5, N'TAPIOCA', CAST(N'2014-12-09 16:58:53.487' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Grupo] OFF

SET IDENTITY_INSERT [dbo].[MeuLanche] ON 
INSERT [dbo].[MeuLanche] ([idMeuLanche], [nomeMeuLanche], [cnpjMeuLanche], [inscricaoEstadualMeuLanche], [telefoneMeuLanche], [faxMeuLanche], [enderecoMeuLanche], [complementoMeuLanche], [bairroMeuLanche], [cepMeuLanche], [municipioMeuLanche], [ufMeuLanche], [emailMeuLanche], [contatoMeuLanche], [localLogoMeuLanche]) VALUES (1, N'DR TECNOLOGIA SOLUÇÕES EM INFORMÁTICA', N'12354564845', N'15794326', N'9298162917', N'9299450917', N'RUA PROFESSORA LEA ALENCAR Nº 368', N'', N'ALVORADA 3', N'69042050', N'MANAUS', N'AM', N'RICARDO.SOUZA1910@HOTMAIL.COM', N'FRANCISCO RICARDO', N'C:\Users\RICARDO\Documents\Visual Studio 2012\Projects\DRSis_LanheFacil\Apresentação\bin\Debug\Logos\logoMeuLanche.jpg')
SET IDENTITY_INSERT [dbo].[MeuLanche] OFF

SET IDENTITY_INSERT [dbo].[NivelAcesso] ON 
INSERT [dbo].[NivelAcesso] ([nivelAcessoId], [nivelAcessoDescricao], [dataCadAcessoNivel], [idUsuario]) VALUES (1, N'ADMINISTRADOR', CAST(N'2014-12-03 00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[NivelAcesso] OFF
SET IDENTITY_INSERT [dbo].[PermissaoAcesso] ON 

INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (1, 1, N'CLIENTES', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (2, 1, N'USUARIOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (3, 1, N'FORNECEDORES', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (4, 1, N'ENTREGADORES', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (5, 1, N'PRODUTOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (6, 1, N'GRUPOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (7, 1, N'ESPECIAIS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (8, 1, N'BAIRRO', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (9, 1, N'UNIDADE', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (10, 1, N'ENTRADA', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (11, 1, N'SAIDA', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (12, 1, N'PDV', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (13, 1, N'DELIVERY', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (14, 1, N'ENTREGAS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (15, 1, N'TEMPO PEDIDO', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (16, 1, N'PEDIDOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (17, 1, N'CAIXA', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (18, 1, N'PERMISSAO USUARIOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (19, 1, N'NIVEL ACESSO', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (20, 1, N'CARTOES', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (21, 1, N'PLANO CONTA', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (22, 1, N'FORMA PAGAMENTO', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (23, 1, N'CONTAS PAGAR', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (24, 1, N'RETIRADAS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (25, 1, N'RELATORIOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (26, 1, N'GRAFICOS', 1, 1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[PermissaoAcesso] ([permissaoAcessoId], [nivelAcessoId], [buton], [ativo], [cadastrar], [alterar], [excluir], [pesquisar], [relatorios], [graficos], [financeiro]) VALUES (27, 1, N'CONFIGURAÇOES', 1, 1, 1, 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[PermissaoAcesso] OFF

SET IDENTITY_INSERT [dbo].[Registro] ON 
INSERT [dbo].[Registro] ([id], [razãoSocial], [nomeFantasia], [responsável], [cnpj], [telefone], [celular], [email], [cidade], [uf], [logo]) VALUES (1, N'FRANCISCO RICARDO DE SOUZA ME', N'DR TECNOLOGIA', N'FRANCISCO RICARDO DE SOUZA', N' 123456789875', N'92981629178', N'92994509178', N'RICARDO.SOUZA1910@HOTMAIL.COM', N'MANAUS', N'AM', NULL)
SET IDENTITY_INSERT [dbo].[Registro] OFF

INSERT [dbo].[tipoImpressao] ([tipo]) VALUES (0)
SET IDENTITY_INSERT [dbo].[UndMedida] ON 

SET IDENTITY_INSERT [dbo].[Usuarios] ON 
INSERT [dbo].[Usuarios] ([usuarioId], [usuarioNome], [usuarioLogin], [usuarioSenha], [usuarioCpf], [nivelAcessoId], [usuarioDataCad], [usuarioCadUsuarioId]) VALUES (3, N'FRANCISCO RICARDO DE SOUZA', N'FRANCISCO', N'123', N'72173084291', 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF

ALTER TABLE [dbo].[Bairro]  WITH CHECK ADD  CONSTRAINT [FK_Bairro_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Bairro] CHECK CONSTRAINT [FK_Bairro_Usuarios]
GO
ALTER TABLE [dbo].[Cartao]  WITH CHECK ADD  CONSTRAINT [FK_Cartao_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Cartao] CHECK CONSTRAINT [FK_Cartao_Usuarios]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Usuarios]
GO
ALTER TABLE [dbo].[ContasPagar]  WITH CHECK ADD  CONSTRAINT [FK_ContasPagar_PlanoConta] FOREIGN KEY([idPlanoConta])
REFERENCES [dbo].[PlanoConta] ([idPlanoConta])
GO
ALTER TABLE [dbo].[ContasPagar] CHECK CONSTRAINT [FK_ContasPagar_PlanoConta]
GO
ALTER TABLE [dbo].[ContasPagar]  WITH CHECK ADD  CONSTRAINT [FK_ContasPagar_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[ContasPagar] CHECK CONSTRAINT [FK_ContasPagar_Usuarios]
GO
ALTER TABLE [dbo].[ContasPagas]  WITH CHECK ADD  CONSTRAINT [FK_ConstasPagas_ContasPagar1] FOREIGN KEY([idContaPagar])
REFERENCES [dbo].[ContasPagar] ([idContaPagar])
GO
ALTER TABLE [dbo].[ContasPagas] CHECK CONSTRAINT [FK_ConstasPagas_ContasPagar1]
GO
ALTER TABLE [dbo].[ContasPagas]  WITH CHECK ADD  CONSTRAINT [FK_ConstasPagas_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[ContasPagas] CHECK CONSTRAINT [FK_ConstasPagas_Usuarios]
GO
ALTER TABLE [dbo].[EntradaProduto]  WITH CHECK ADD  CONSTRAINT [FK_EntradaProduto_Produto] FOREIGN KEY([codigoProduto])
REFERENCES [dbo].[Produto] ([codigoProduto])
GO
ALTER TABLE [dbo].[EntradaProduto] CHECK CONSTRAINT [FK_EntradaProduto_Produto]
GO
ALTER TABLE [dbo].[EntradaProduto]  WITH CHECK ADD  CONSTRAINT [FK_EntradaProduto_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[EntradaProduto] CHECK CONSTRAINT [FK_EntradaProduto_Usuarios]
GO
ALTER TABLE [dbo].[EntregaCancelada]  WITH CHECK ADD  CONSTRAINT [FK_EntregaCancelada_VendasCabecalho] FOREIGN KEY([idVendaCabecalho])
REFERENCES [dbo].[VendasCabecalho] ([idVendaCabecalho])
GO
ALTER TABLE [dbo].[EntregaCancelada] CHECK CONSTRAINT [FK_EntregaCancelada_VendasCabecalho]
GO
ALTER TABLE [dbo].[Entregador]  WITH CHECK ADD  CONSTRAINT [FK_Entregador_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Entregador] CHECK CONSTRAINT [FK_Entregador_Usuarios]
GO
ALTER TABLE [dbo].[Entregas]  WITH CHECK ADD  CONSTRAINT [FK_Entregas_VendasCabecalho] FOREIGN KEY([idVendaCabecalho])
REFERENCES [dbo].[VendasCabecalho] ([idVendaCabecalho])
GO
ALTER TABLE [dbo].[Entregas] CHECK CONSTRAINT [FK_Entregas_VendasCabecalho]
GO
ALTER TABLE [dbo].[Especiais]  WITH CHECK ADD  CONSTRAINT [FK_Especiais_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Especiais] CHECK CONSTRAINT [FK_Especiais_Usuarios]
GO
ALTER TABLE [dbo].[FormaPagamento]  WITH CHECK ADD  CONSTRAINT [FK_formaPagamento_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[FormaPagamento] CHECK CONSTRAINT [FK_formaPagamento_Usuarios]
GO
ALTER TABLE [dbo].[Fornecedor]  WITH CHECK ADD  CONSTRAINT [FK_Fornecedor_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Fornecedor] CHECK CONSTRAINT [FK_Fornecedor_Usuarios]
GO
ALTER TABLE [dbo].[Grupo]  WITH CHECK ADD  CONSTRAINT [FK_Grupo_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Grupo] CHECK CONSTRAINT [FK_Grupo_Usuarios]
GO
ALTER TABLE [dbo].[Pagamento]  WITH CHECK ADD  CONSTRAINT [FK_Pagamento_Fornecedor] FOREIGN KEY([idFornecedor])
REFERENCES [dbo].[Fornecedor] ([idFornecedor])
GO
ALTER TABLE [dbo].[Pagamento] CHECK CONSTRAINT [FK_Pagamento_Fornecedor]
GO
ALTER TABLE [dbo].[Pagamento]  WITH CHECK ADD  CONSTRAINT [FK_Pagamento_TipoPagamento] FOREIGN KEY([idTipoPagamento])
REFERENCES [dbo].[TipoPagamento] ([idTipoPagamento])
GO
ALTER TABLE [dbo].[Pagamento] CHECK CONSTRAINT [FK_Pagamento_TipoPagamento]
GO
ALTER TABLE [dbo].[Pagamento]  WITH CHECK ADD  CONSTRAINT [FK_Pagamento_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Pagamento] CHECK CONSTRAINT [FK_Pagamento_Usuarios]
GO
ALTER TABLE [dbo].[PlanoConta]  WITH CHECK ADD  CONSTRAINT [FK_PlanoConta_Usuarios1] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[PlanoConta] CHECK CONSTRAINT [FK_PlanoConta_Usuarios1]
GO
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_Fornecedor] FOREIGN KEY([idFornecedor])
REFERENCES [dbo].[Fornecedor] ([idFornecedor])
GO
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Fornecedor]
GO
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_Grupo] FOREIGN KEY([grupoId])
REFERENCES [dbo].[Grupo] ([grupoId])
GO
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Grupo]
GO
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_UndMedida] FOREIGN KEY([undMedidaId])
REFERENCES [dbo].[UndMedida] ([undMedidaId])
GO
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_UndMedida]
GO
ALTER TABLE [dbo].[Produto]  WITH CHECK ADD  CONSTRAINT [FK_Produto_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Produto] CHECK CONSTRAINT [FK_Produto_Usuarios]
GO
ALTER TABLE [dbo].[ProdutoManufaturado]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoManufaturado_Produto] FOREIGN KEY([codProduto])
REFERENCES [dbo].[Produto] ([codigoProduto])
GO
ALTER TABLE [dbo].[ProdutoManufaturado] CHECK CONSTRAINT [FK_ProdutoManufaturado_Produto]
GO
ALTER TABLE [dbo].[Recebimento]  WITH CHECK ADD  CONSTRAINT [FK_Recebimento_Clientes] FOREIGN KEY([idCliente])
REFERENCES [dbo].[Clientes] ([idCliente])
GO
ALTER TABLE [dbo].[Recebimento] CHECK CONSTRAINT [FK_Recebimento_Clientes]
GO
ALTER TABLE [dbo].[Recebimento]  WITH CHECK ADD  CONSTRAINT [FK_Recebimento_TipoPagamento] FOREIGN KEY([idTipoPagamento])
REFERENCES [dbo].[TipoPagamento] ([idTipoPagamento])
GO
ALTER TABLE [dbo].[Recebimento] CHECK CONSTRAINT [FK_Recebimento_TipoPagamento]
GO
ALTER TABLE [dbo].[Recebimento]  WITH CHECK ADD  CONSTRAINT [FK_Recebimento_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[Recebimento] CHECK CONSTRAINT [FK_Recebimento_Usuarios]
GO
ALTER TABLE [dbo].[SaidaProduto]  WITH CHECK ADD  CONSTRAINT [FK_SaidaEntrada_Produto] FOREIGN KEY([codigoProduto])
REFERENCES [dbo].[Produto] ([codigoProduto])
GO
ALTER TABLE [dbo].[SaidaProduto] CHECK CONSTRAINT [FK_SaidaEntrada_Produto]
GO
ALTER TABLE [dbo].[SaidaProduto]  WITH CHECK ADD  CONSTRAINT [FK_SaidaEntrada_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[SaidaProduto] CHECK CONSTRAINT [FK_SaidaEntrada_Usuarios]
GO
ALTER TABLE [dbo].[TempoVendas]  WITH CHECK ADD  CONSTRAINT [FK_TempoVendas_VendasCabecalho] FOREIGN KEY([idVendaCabecalhoTempo])
REFERENCES [dbo].[VendasCabecalho] ([idVendaCabecalho])
GO
ALTER TABLE [dbo].[TempoVendas] CHECK CONSTRAINT [FK_TempoVendas_VendasCabecalho]
GO
ALTER TABLE [dbo].[TipoPagamento]  WITH CHECK ADD  CONSTRAINT [FK_TipoPagamento_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[TipoPagamento] CHECK CONSTRAINT [FK_TipoPagamento_Usuarios]
GO
ALTER TABLE [dbo].[UndMedida]  WITH CHECK ADD  CONSTRAINT [FK_UndMedida_Usuarios] FOREIGN KEY([usuarioId])
REFERENCES [dbo].[Usuarios] ([usuarioId])
GO
ALTER TABLE [dbo].[UndMedida] CHECK CONSTRAINT [FK_UndMedida_Usuarios]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_NivelAcesso1] FOREIGN KEY([nivelAcessoId])
REFERENCES [dbo].[NivelAcesso] ([nivelAcessoId])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_NivelAcesso1]
GO
ALTER TABLE [dbo].[VendaCabecalhoTipoPagamentos]  WITH CHECK ADD  CONSTRAINT [FK_VendaCabecalhoTipoPagamentos_VendasCabecalho] FOREIGN KEY([idVendaCabecalhoTipoPagamento])
REFERENCES [dbo].[VendasCabecalho] ([idVendaCabecalho])
GO
ALTER TABLE [dbo].[VendaCabecalhoTipoPagamentos] CHECK CONSTRAINT [FK_VendaCabecalhoTipoPagamentos_VendasCabecalho]
GO
ALTER TABLE [dbo].[VendaDetalhes]  WITH CHECK ADD  CONSTRAINT [FK_VendaDetalhes_VendasCabecalho] FOREIGN KEY([idVendaCabecalhoDetalhes])
REFERENCES [dbo].[VendasCabecalho] ([idVendaCabecalho])
GO
ALTER TABLE [dbo].[VendaDetalhes] CHECK CONSTRAINT [FK_VendaDetalhes_VendasCabecalho]
GO
USE [master]
GO
ALTER DATABASE [lancheFacil] SET  READ_WRITE 
GO
