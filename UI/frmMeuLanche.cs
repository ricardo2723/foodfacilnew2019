﻿using System;
using System.Drawing;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmMeuLanche : Form
    {
        MeuLanche meuLanche = new MeuLanche();
        public string path;

        public frmMeuLanche()
        {
            InitializeComponent();       
        }

        private void frmMeuLanche_Load(object sender, EventArgs e)
        {
            MeuLancheNegocios meuLancheNegocios = new MeuLancheNegocios();
            MeuLancheCollections meuLancheCollections = meuLancheNegocios.ConsultarPorNome();

            foreach (var item in meuLancheCollections)
            {
                meuLanche.idMeuLanche = item.idMeuLanche;
                txtNome.Text = item.nomeMeuLanche;
                txtFax.Text = item.faxMeuLanche;
                txtEmail.Text = item.emailMeuLanche;
                txtContato.Text = item.contatoMeuLanche;
                txtCnpj.Text = item.cnpjMeuLanche;
                txtInscEstadual.Text = item.inscricaoEstadualMeuLanche;
                txtTelefone.Text = item.telefoneMeuLanche;
                txtCep.Text = item.cepMeuLanche;
                txtEndereco.Text = item.enderecoMeuLanche;
                txtComplemento.Text = item.complementoMeuLanche;
                txtBairro.Text = item.bairroMeuLanche;
                txtCidade.Text = item.municipioMeuLanche;
                cbUf.Text = item.ufMeuLanche;
                txtLocalLogo.Text = item.localLogoMeuLanche;
                pbMeuLanche.ImageLocation = item.localLogoMeuLanche;
            }
        }

        #region BOTÕES

        private void bntFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            atualizarMeuLanche();
        }

        #endregion

        #region AÇÕES

        private void frmCadClienteInf_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    Close();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void atualizarMeuLanche()
        {
            if (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtTelefone.Text) &&
               !String.IsNullOrEmpty(txtBairro.Text))
            {
                meuLanche.nomeMeuLanche = txtNome.Text;
                meuLanche.faxMeuLanche = txtFax.Text;
                meuLanche.emailMeuLanche = txtEmail.Text;
                meuLanche.contatoMeuLanche = txtContato.Text;
                meuLanche.cnpjMeuLanche = txtCnpj.Text;
                meuLanche.inscricaoEstadualMeuLanche = txtInscEstadual.Text;
                meuLanche.telefoneMeuLanche = txtTelefone.Text;
                meuLanche.cepMeuLanche = txtCep.Text;
                meuLanche.enderecoMeuLanche = txtEndereco.Text;
                meuLanche.complementoMeuLanche = txtComplemento.Text;
                meuLanche.bairroMeuLanche = txtBairro.Text;
                meuLanche.municipioMeuLanche = txtCidade.Text;
                meuLanche.ufMeuLanche = cbUf.Text;
                meuLanche.localLogoMeuLanche = txtLocalLogo.Text;

                MeuLancheNegocios meuLancheNegocios = new MeuLancheNegocios();
                string retorno = meuLancheNegocios.Alterar(meuLanche);

                try
                {
                    int idMeuLanche = Convert.ToInt32(retorno);
                    MessageBox.Show("MeuLanche Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    path = meuLanche.localLogoMeuLanche;
                    frmLogin.meuLanche.localLogoMeuLanche = meuLanche.localLogoMeuLanche;
                    this.DialogResult = DialogResult.Yes;
                    
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Cliente / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBairro.Focus();
                }

                else if (String.IsNullOrEmpty(txtTelefone.Text))
                {
                    MessageBox.Show("Campo Telefone é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTelefone.Focus();
                }
            }
        }

        #endregion

        private void btnLogo_Click(object sender, EventArgs e)
        {
            ofdLogo.InitialDirectory = @"Bibliotecas\Imagens";

            if (ofdLogo.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Bitmap bmp = new Bitmap(ofdLogo.FileName);

                    pbMeuLanche.Image = bmp;
                    pbMeuLanche.Image.Save(Application.StartupPath.ToString() + "\\Logos\\" + "logoMeuLanche.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não foi possivel carregar a imagem: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            pbMeuLanche.ImageLocation = ofdLogo.FileName;
            txtLocalLogo.Text = Application.StartupPath.ToString() + "\\Logos\\" + "logoMeuLanche.jpg";            
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            atualizarMeuLanche();            
        }

    }
}
