﻿namespace UI
{
    partial class frmInstallActive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInstallActive));
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtInstalacao = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtAtivacao = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnAtivarSistema = new DevComponents.DotNetBar.ButtonX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Candara", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(12, 12);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(161, 23);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX1.TabIndex = 10;
            this.labelX1.Text = "<font color=\"#FFFFFF\">Serial de Instalação</font>";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtInstalacao
            // 
            this.txtInstalacao.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtInstalacao.Border.Class = "TextBoxBorder";
            this.txtInstalacao.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtInstalacao.Font = new System.Drawing.Font("Candara", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstalacao.ForeColor = System.Drawing.Color.Black;
            this.txtInstalacao.Location = new System.Drawing.Point(13, 42);
            this.txtInstalacao.Name = "txtInstalacao";
            this.txtInstalacao.PreventEnterBeep = true;
            this.txtInstalacao.Size = new System.Drawing.Size(435, 32);
            this.txtInstalacao.TabIndex = 11;
            this.txtInstalacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInstalacao.WatermarkImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAtivacao
            // 
            this.txtAtivacao.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAtivacao.Border.Class = "TextBoxBorder";
            this.txtAtivacao.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAtivacao.Font = new System.Drawing.Font("Candara", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAtivacao.ForeColor = System.Drawing.Color.Black;
            this.txtAtivacao.Location = new System.Drawing.Point(13, 119);
            this.txtAtivacao.Name = "txtAtivacao";
            this.txtAtivacao.PreventEnterBeep = true;
            this.txtAtivacao.Size = new System.Drawing.Size(435, 32);
            this.txtAtivacao.TabIndex = 13;
            this.txtAtivacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Candara", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(12, 89);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(161, 23);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX2.TabIndex = 12;
            this.labelX2.Text = "<font color=\"#FFFFFF\">Serial de Ativação</font>";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnAtivarSistema
            // 
            this.btnAtivarSistema.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAtivarSistema.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnAtivarSistema.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAtivarSistema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAtivarSistema.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtivarSistema.Location = new System.Drawing.Point(13, 179);
            this.btnAtivarSistema.Name = "btnAtivarSistema";
            this.btnAtivarSistema.Size = new System.Drawing.Size(435, 34);
            this.btnAtivarSistema.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAtivarSistema.TabIndex = 14;
            this.btnAtivarSistema.Text = "<font color=\"#0005C5\"><b>ATIVAR SISTEMA</b></font>";
            this.btnAtivarSistema.Click += new System.EventHandler(this.btnAtivarSistema_Click);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Navy;
            this.labelX4.Location = new System.Drawing.Point(65, 244);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(340, 25);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX4.TabIndex = 15;
            this.labelX4.Text = "<font color=\"#17365D\">Copyright © - DR Tecnologia Soluções em Informática</font>";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.OldLace;
            this.labelX3.Location = new System.Drawing.Point(21, 219);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(427, 30);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "<font color=\"#17365D\">Para Ativação entre em contato com o Administrador do Siste" +
    "ma.</font>";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Navy;
            this.labelX5.Location = new System.Drawing.Point(101, 265);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(243, 26);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX5.TabIndex = 17;
            this.labelX5.Text = "<font color=\"#17365D\">Fone: (92) 8162-9178 / (92) 9450-9178</font>";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // frmInstallActive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::UI.Properties.Resources.blue_abstract_background1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(458, 292);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.btnAtivarSistema);
            this.Controls.Add(this.txtAtivacao);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.txtInstalacao);
            this.Controls.Add(this.labelX1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInstallActive";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil -Instalação e Ativação do Sistema";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtInstalacao;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAtivacao;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnAtivarSistema;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX5;
    }
}