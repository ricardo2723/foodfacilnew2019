﻿namespace UI
{
    partial class frmPdvImprimir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPdvImprimir));
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPedC18 = new System.Windows.Forms.Button();
            this.btnPedC17 = new System.Windows.Forms.Button();
            this.btnPedC16 = new System.Windows.Forms.Button();
            this.btnPedC15 = new System.Windows.Forms.Button();
            this.btnPedC14 = new System.Windows.Forms.Button();
            this.btnPedC13 = new System.Windows.Forms.Button();
            this.btnPedC12 = new System.Windows.Forms.Button();
            this.btnPedC11 = new System.Windows.Forms.Button();
            this.btnPedC10 = new System.Windows.Forms.Button();
            this.btnPedC9 = new System.Windows.Forms.Button();
            this.btnPedC8 = new System.Windows.Forms.Button();
            this.btnPedC7 = new System.Windows.Forms.Button();
            this.btnPedC6 = new System.Windows.Forms.Button();
            this.btnPedC5 = new System.Windows.Forms.Button();
            this.btnPedC4 = new System.Windows.Forms.Button();
            this.btnPedC3 = new System.Windows.Forms.Button();
            this.btnPedC2 = new System.Windows.Forms.Button();
            this.btnPedC1 = new System.Windows.Forms.Button();
            this.btnPedB18 = new System.Windows.Forms.Button();
            this.btnPedB17 = new System.Windows.Forms.Button();
            this.btnPedB16 = new System.Windows.Forms.Button();
            this.btnPedB15 = new System.Windows.Forms.Button();
            this.btnPedB14 = new System.Windows.Forms.Button();
            this.btnPedB13 = new System.Windows.Forms.Button();
            this.btnPedB12 = new System.Windows.Forms.Button();
            this.btnPedB11 = new System.Windows.Forms.Button();
            this.btnPedB10 = new System.Windows.Forms.Button();
            this.btnPedB9 = new System.Windows.Forms.Button();
            this.btnPedB8 = new System.Windows.Forms.Button();
            this.btnPedB7 = new System.Windows.Forms.Button();
            this.btnPedB6 = new System.Windows.Forms.Button();
            this.btnPedB5 = new System.Windows.Forms.Button();
            this.btnPedB4 = new System.Windows.Forms.Button();
            this.btnPedB3 = new System.Windows.Forms.Button();
            this.btnPedB2 = new System.Windows.Forms.Button();
            this.btnPedB1 = new System.Windows.Forms.Button();
            this.btnPedM4 = new System.Windows.Forms.Button();
            this.btnPedM1 = new System.Windows.Forms.Button();
            this.btnPedM2 = new System.Windows.Forms.Button();
            this.btnPedM3 = new System.Windows.Forms.Button();
            this.btnPedM5 = new System.Windows.Forms.Button();
            this.btnPedM6 = new System.Windows.Forms.Button();
            this.btnPedM7 = new System.Windows.Forms.Button();
            this.btnPedM8 = new System.Windows.Forms.Button();
            this.btnPedM9 = new System.Windows.Forms.Button();
            this.btnPedM10 = new System.Windows.Forms.Button();
            this.btnPedM11 = new System.Windows.Forms.Button();
            this.btnPedM12 = new System.Windows.Forms.Button();
            this.btnPedM13 = new System.Windows.Forms.Button();
            this.btnPedM14 = new System.Windows.Forms.Button();
            this.btnPedM15 = new System.Windows.Forms.Button();
            this.btnPedM16 = new System.Windows.Forms.Button();
            this.btnPedM17 = new System.Windows.Forms.Button();
            this.btnPedM18 = new System.Windows.Forms.Button();
            this.btnPedM19 = new System.Windows.Forms.Button();
            this.btnPedM20 = new System.Windows.Forms.Button();
            this.btnPedM21 = new System.Windows.Forms.Button();
            this.btnPedM22 = new System.Windows.Forms.Button();
            this.btnPedM23 = new System.Windows.Forms.Button();
            this.btnPedM24 = new System.Windows.Forms.Button();
            this.btnPedM25 = new System.Windows.Forms.Button();
            this.btnPedM26 = new System.Windows.Forms.Button();
            this.btnPedM27 = new System.Windows.Forms.Button();
            this.btnPedM28 = new System.Windows.Forms.Button();
            this.btnPedM29 = new System.Windows.Forms.Button();
            this.btnPedM30 = new System.Windows.Forms.Button();
            this.btnPedM31 = new System.Windows.Forms.Button();
            this.btnPedM32 = new System.Windows.Forms.Button();
            this.btnPedM33 = new System.Windows.Forms.Button();
            this.btnPedM34 = new System.Windows.Forms.Button();
            this.btnPedM35 = new System.Windows.Forms.Button();
            this.btnPedM36 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label14.Location = new System.Drawing.Point(241, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 23);
            this.label14.TabIndex = 154;
            this.label14.Text = "MESAS";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label18.Location = new System.Drawing.Point(240, 401);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 23);
            this.label18.TabIndex = 209;
            this.label18.Text = "CARRO";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label15.Location = new System.Drawing.Point(240, 257);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 23);
            this.label15.TabIndex = 190;
            this.label15.Text = "BALCÃO";
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.printer1;
            this.btnSalvar.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnSalvar.Location = new System.Drawing.Point(172, 537);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(244, 60);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 228;
            this.btnSalvar.Text = "Imprimir Todos (F12)";
            // 
            // circularProgress1
            // 
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.Location = new System.Drawing.Point(0, 0);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.Size = new System.Drawing.Size(0, 0);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPedC18);
            this.groupBox1.Controls.Add(this.btnPedC17);
            this.groupBox1.Controls.Add(this.btnPedC16);
            this.groupBox1.Controls.Add(this.btnPedC15);
            this.groupBox1.Controls.Add(this.btnPedC14);
            this.groupBox1.Controls.Add(this.btnPedC13);
            this.groupBox1.Controls.Add(this.btnPedC12);
            this.groupBox1.Controls.Add(this.btnPedC11);
            this.groupBox1.Controls.Add(this.btnPedC10);
            this.groupBox1.Controls.Add(this.btnPedC9);
            this.groupBox1.Controls.Add(this.btnPedC8);
            this.groupBox1.Controls.Add(this.btnPedC7);
            this.groupBox1.Controls.Add(this.btnPedC6);
            this.groupBox1.Controls.Add(this.btnPedC5);
            this.groupBox1.Controls.Add(this.btnPedC4);
            this.groupBox1.Controls.Add(this.btnPedC3);
            this.groupBox1.Controls.Add(this.btnPedC2);
            this.groupBox1.Controls.Add(this.btnPedC1);
            this.groupBox1.Controls.Add(this.btnPedB18);
            this.groupBox1.Controls.Add(this.btnPedB17);
            this.groupBox1.Controls.Add(this.btnPedB16);
            this.groupBox1.Controls.Add(this.btnPedB15);
            this.groupBox1.Controls.Add(this.btnPedB14);
            this.groupBox1.Controls.Add(this.btnPedB13);
            this.groupBox1.Controls.Add(this.btnPedB12);
            this.groupBox1.Controls.Add(this.btnPedB11);
            this.groupBox1.Controls.Add(this.btnPedB10);
            this.groupBox1.Controls.Add(this.btnPedB9);
            this.groupBox1.Controls.Add(this.btnPedB8);
            this.groupBox1.Controls.Add(this.btnPedB7);
            this.groupBox1.Controls.Add(this.btnPedB6);
            this.groupBox1.Controls.Add(this.btnPedB5);
            this.groupBox1.Controls.Add(this.btnPedB4);
            this.groupBox1.Controls.Add(this.btnPedB3);
            this.groupBox1.Controls.Add(this.btnPedB2);
            this.groupBox1.Controls.Add(this.btnPedB1);
            this.groupBox1.Controls.Add(this.btnPedM4);
            this.groupBox1.Controls.Add(this.btnPedM1);
            this.groupBox1.Controls.Add(this.btnPedM2);
            this.groupBox1.Controls.Add(this.btnPedM3);
            this.groupBox1.Controls.Add(this.btnPedM5);
            this.groupBox1.Controls.Add(this.btnPedM6);
            this.groupBox1.Controls.Add(this.btnPedM7);
            this.groupBox1.Controls.Add(this.btnPedM8);
            this.groupBox1.Controls.Add(this.btnPedM9);
            this.groupBox1.Controls.Add(this.btnPedM10);
            this.groupBox1.Controls.Add(this.btnPedM11);
            this.groupBox1.Controls.Add(this.btnPedM12);
            this.groupBox1.Controls.Add(this.btnPedM13);
            this.groupBox1.Controls.Add(this.btnPedM14);
            this.groupBox1.Controls.Add(this.btnPedM15);
            this.groupBox1.Controls.Add(this.btnPedM16);
            this.groupBox1.Controls.Add(this.btnPedM17);
            this.groupBox1.Controls.Add(this.btnPedM18);
            this.groupBox1.Controls.Add(this.btnPedM19);
            this.groupBox1.Controls.Add(this.btnPedM20);
            this.groupBox1.Controls.Add(this.btnPedM21);
            this.groupBox1.Controls.Add(this.btnPedM22);
            this.groupBox1.Controls.Add(this.btnPedM23);
            this.groupBox1.Controls.Add(this.btnPedM24);
            this.groupBox1.Controls.Add(this.btnPedM25);
            this.groupBox1.Controls.Add(this.btnPedM26);
            this.groupBox1.Controls.Add(this.btnPedM27);
            this.groupBox1.Controls.Add(this.btnPedM28);
            this.groupBox1.Controls.Add(this.btnPedM29);
            this.groupBox1.Controls.Add(this.btnPedM30);
            this.groupBox1.Controls.Add(this.btnPedM31);
            this.groupBox1.Controls.Add(this.btnPedM32);
            this.groupBox1.Controls.Add(this.btnPedM33);
            this.groupBox1.Controls.Add(this.btnPedM34);
            this.groupBox1.Controls.Add(this.btnPedM35);
            this.groupBox1.Controls.Add(this.btnPedM36);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Location = new System.Drawing.Point(6, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 537);
            this.groupBox1.TabIndex = 229;
            this.groupBox1.TabStop = false;
            // 
            // btnPedC18
            // 
            this.btnPedC18.BackColor = System.Drawing.Color.White;
            this.btnPedC18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC18.BackgroundImage")));
            this.btnPedC18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC18.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC18.ForeColor = System.Drawing.Color.Black;
            this.btnPedC18.Location = new System.Drawing.Point(496, 480);
            this.btnPedC18.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC18.Name = "btnPedC18";
            this.btnPedC18.Size = new System.Drawing.Size(60, 50);
            this.btnPedC18.TabIndex = 281;
            this.btnPedC18.Text = "18";
            this.btnPedC18.UseCompatibleTextRendering = true;
            this.btnPedC18.UseVisualStyleBackColor = false;
            this.btnPedC18.Click += new System.EventHandler(this.btnPedC18_Click);
            // 
            // btnPedC17
            // 
            this.btnPedC17.BackColor = System.Drawing.Color.White;
            this.btnPedC17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC17.BackgroundImage")));
            this.btnPedC17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC17.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC17.ForeColor = System.Drawing.Color.Black;
            this.btnPedC17.Location = new System.Drawing.Point(435, 480);
            this.btnPedC17.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC17.Name = "btnPedC17";
            this.btnPedC17.Size = new System.Drawing.Size(60, 50);
            this.btnPedC17.TabIndex = 280;
            this.btnPedC17.Text = "17";
            this.btnPedC17.UseCompatibleTextRendering = true;
            this.btnPedC17.UseVisualStyleBackColor = false;
            this.btnPedC17.Click += new System.EventHandler(this.btnPedC17_Click);
            // 
            // btnPedC16
            // 
            this.btnPedC16.BackColor = System.Drawing.Color.White;
            this.btnPedC16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC16.BackgroundImage")));
            this.btnPedC16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC16.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC16.ForeColor = System.Drawing.Color.Black;
            this.btnPedC16.Location = new System.Drawing.Point(374, 480);
            this.btnPedC16.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC16.Name = "btnPedC16";
            this.btnPedC16.Size = new System.Drawing.Size(60, 50);
            this.btnPedC16.TabIndex = 279;
            this.btnPedC16.Text = "16";
            this.btnPedC16.UseCompatibleTextRendering = true;
            this.btnPedC16.UseVisualStyleBackColor = false;
            this.btnPedC16.Click += new System.EventHandler(this.btnPedC16_Click);
            // 
            // btnPedC15
            // 
            this.btnPedC15.BackColor = System.Drawing.Color.White;
            this.btnPedC15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC15.BackgroundImage")));
            this.btnPedC15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC15.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC15.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC15.ForeColor = System.Drawing.Color.Black;
            this.btnPedC15.Location = new System.Drawing.Point(313, 480);
            this.btnPedC15.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC15.Name = "btnPedC15";
            this.btnPedC15.Size = new System.Drawing.Size(60, 50);
            this.btnPedC15.TabIndex = 278;
            this.btnPedC15.Text = "15";
            this.btnPedC15.UseCompatibleTextRendering = true;
            this.btnPedC15.UseVisualStyleBackColor = false;
            this.btnPedC15.Click += new System.EventHandler(this.btnPedC15_Click);
            // 
            // btnPedC14
            // 
            this.btnPedC14.BackColor = System.Drawing.Color.White;
            this.btnPedC14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC14.BackgroundImage")));
            this.btnPedC14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC14.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC14.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC14.ForeColor = System.Drawing.Color.Black;
            this.btnPedC14.Location = new System.Drawing.Point(252, 480);
            this.btnPedC14.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC14.Name = "btnPedC14";
            this.btnPedC14.Size = new System.Drawing.Size(60, 50);
            this.btnPedC14.TabIndex = 277;
            this.btnPedC14.Text = "14";
            this.btnPedC14.UseCompatibleTextRendering = true;
            this.btnPedC14.UseVisualStyleBackColor = false;
            this.btnPedC14.Click += new System.EventHandler(this.btnPedC14_Click);
            // 
            // btnPedC13
            // 
            this.btnPedC13.BackColor = System.Drawing.Color.White;
            this.btnPedC13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC13.BackgroundImage")));
            this.btnPedC13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC13.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC13.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC13.ForeColor = System.Drawing.Color.Black;
            this.btnPedC13.Location = new System.Drawing.Point(191, 480);
            this.btnPedC13.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC13.Name = "btnPedC13";
            this.btnPedC13.Size = new System.Drawing.Size(60, 50);
            this.btnPedC13.TabIndex = 276;
            this.btnPedC13.Text = "13";
            this.btnPedC13.UseCompatibleTextRendering = true;
            this.btnPedC13.UseVisualStyleBackColor = false;
            this.btnPedC13.Click += new System.EventHandler(this.btnPedC13_Click);
            // 
            // btnPedC12
            // 
            this.btnPedC12.BackColor = System.Drawing.Color.White;
            this.btnPedC12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC12.BackgroundImage")));
            this.btnPedC12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC12.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC12.ForeColor = System.Drawing.Color.Black;
            this.btnPedC12.Location = new System.Drawing.Point(130, 480);
            this.btnPedC12.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC12.Name = "btnPedC12";
            this.btnPedC12.Size = new System.Drawing.Size(60, 50);
            this.btnPedC12.TabIndex = 275;
            this.btnPedC12.Text = "12";
            this.btnPedC12.UseCompatibleTextRendering = true;
            this.btnPedC12.UseVisualStyleBackColor = false;
            this.btnPedC12.Click += new System.EventHandler(this.btnPedC12_Click);
            // 
            // btnPedC11
            // 
            this.btnPedC11.BackColor = System.Drawing.Color.White;
            this.btnPedC11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC11.BackgroundImage")));
            this.btnPedC11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC11.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC11.ForeColor = System.Drawing.Color.Black;
            this.btnPedC11.Location = new System.Drawing.Point(69, 480);
            this.btnPedC11.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC11.Name = "btnPedC11";
            this.btnPedC11.Size = new System.Drawing.Size(60, 50);
            this.btnPedC11.TabIndex = 274;
            this.btnPedC11.Text = "11";
            this.btnPedC11.UseCompatibleTextRendering = true;
            this.btnPedC11.UseVisualStyleBackColor = false;
            this.btnPedC11.Click += new System.EventHandler(this.btnPedC11_Click);
            // 
            // btnPedC10
            // 
            this.btnPedC10.BackColor = System.Drawing.Color.White;
            this.btnPedC10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC10.BackgroundImage")));
            this.btnPedC10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC10.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC10.ForeColor = System.Drawing.Color.Black;
            this.btnPedC10.Location = new System.Drawing.Point(8, 480);
            this.btnPedC10.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC10.Name = "btnPedC10";
            this.btnPedC10.Size = new System.Drawing.Size(60, 50);
            this.btnPedC10.TabIndex = 273;
            this.btnPedC10.Text = "10";
            this.btnPedC10.UseCompatibleTextRendering = true;
            this.btnPedC10.UseVisualStyleBackColor = false;
            this.btnPedC10.Click += new System.EventHandler(this.btnPedC10_Click);
            // 
            // btnPedC9
            // 
            this.btnPedC9.BackColor = System.Drawing.Color.White;
            this.btnPedC9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC9.BackgroundImage")));
            this.btnPedC9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC9.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC9.ForeColor = System.Drawing.Color.Black;
            this.btnPedC9.Location = new System.Drawing.Point(496, 426);
            this.btnPedC9.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC9.Name = "btnPedC9";
            this.btnPedC9.Size = new System.Drawing.Size(60, 50);
            this.btnPedC9.TabIndex = 272;
            this.btnPedC9.Text = "09";
            this.btnPedC9.UseCompatibleTextRendering = true;
            this.btnPedC9.UseVisualStyleBackColor = false;
            this.btnPedC9.Click += new System.EventHandler(this.btnPedC9_Click);
            // 
            // btnPedC8
            // 
            this.btnPedC8.BackColor = System.Drawing.Color.White;
            this.btnPedC8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC8.BackgroundImage")));
            this.btnPedC8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC8.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC8.ForeColor = System.Drawing.Color.Black;
            this.btnPedC8.Location = new System.Drawing.Point(435, 426);
            this.btnPedC8.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC8.Name = "btnPedC8";
            this.btnPedC8.Size = new System.Drawing.Size(60, 50);
            this.btnPedC8.TabIndex = 271;
            this.btnPedC8.Text = "08";
            this.btnPedC8.UseCompatibleTextRendering = true;
            this.btnPedC8.UseVisualStyleBackColor = false;
            this.btnPedC8.Click += new System.EventHandler(this.btnPedC8_Click);
            // 
            // btnPedC7
            // 
            this.btnPedC7.BackColor = System.Drawing.Color.White;
            this.btnPedC7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC7.BackgroundImage")));
            this.btnPedC7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC7.ForeColor = System.Drawing.Color.Black;
            this.btnPedC7.Location = new System.Drawing.Point(374, 426);
            this.btnPedC7.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC7.Name = "btnPedC7";
            this.btnPedC7.Size = new System.Drawing.Size(60, 50);
            this.btnPedC7.TabIndex = 270;
            this.btnPedC7.Text = "07";
            this.btnPedC7.UseCompatibleTextRendering = true;
            this.btnPedC7.UseVisualStyleBackColor = false;
            this.btnPedC7.Click += new System.EventHandler(this.btnPedC7_Click);
            // 
            // btnPedC6
            // 
            this.btnPedC6.BackColor = System.Drawing.Color.White;
            this.btnPedC6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC6.BackgroundImage")));
            this.btnPedC6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC6.ForeColor = System.Drawing.Color.Black;
            this.btnPedC6.Location = new System.Drawing.Point(313, 426);
            this.btnPedC6.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC6.Name = "btnPedC6";
            this.btnPedC6.Size = new System.Drawing.Size(60, 50);
            this.btnPedC6.TabIndex = 269;
            this.btnPedC6.Text = "06";
            this.btnPedC6.UseCompatibleTextRendering = true;
            this.btnPedC6.UseVisualStyleBackColor = false;
            this.btnPedC6.Click += new System.EventHandler(this.btnPedC6_Click);
            // 
            // btnPedC5
            // 
            this.btnPedC5.BackColor = System.Drawing.Color.White;
            this.btnPedC5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC5.BackgroundImage")));
            this.btnPedC5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC5.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC5.ForeColor = System.Drawing.Color.Black;
            this.btnPedC5.Location = new System.Drawing.Point(252, 426);
            this.btnPedC5.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC5.Name = "btnPedC5";
            this.btnPedC5.Size = new System.Drawing.Size(60, 50);
            this.btnPedC5.TabIndex = 268;
            this.btnPedC5.Text = "05";
            this.btnPedC5.UseCompatibleTextRendering = true;
            this.btnPedC5.UseVisualStyleBackColor = false;
            this.btnPedC5.Click += new System.EventHandler(this.btnPedC5_Click);
            // 
            // btnPedC4
            // 
            this.btnPedC4.BackColor = System.Drawing.Color.White;
            this.btnPedC4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC4.BackgroundImage")));
            this.btnPedC4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC4.ForeColor = System.Drawing.Color.Black;
            this.btnPedC4.Location = new System.Drawing.Point(191, 426);
            this.btnPedC4.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC4.Name = "btnPedC4";
            this.btnPedC4.Size = new System.Drawing.Size(60, 50);
            this.btnPedC4.TabIndex = 267;
            this.btnPedC4.Text = "04";
            this.btnPedC4.UseCompatibleTextRendering = true;
            this.btnPedC4.UseVisualStyleBackColor = false;
            this.btnPedC4.Click += new System.EventHandler(this.btnPedC4_Click);
            // 
            // btnPedC3
            // 
            this.btnPedC3.BackColor = System.Drawing.Color.White;
            this.btnPedC3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC3.BackgroundImage")));
            this.btnPedC3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC3.ForeColor = System.Drawing.Color.Black;
            this.btnPedC3.Location = new System.Drawing.Point(130, 426);
            this.btnPedC3.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC3.Name = "btnPedC3";
            this.btnPedC3.Size = new System.Drawing.Size(60, 50);
            this.btnPedC3.TabIndex = 266;
            this.btnPedC3.Text = "03";
            this.btnPedC3.UseCompatibleTextRendering = true;
            this.btnPedC3.UseVisualStyleBackColor = false;
            this.btnPedC3.Click += new System.EventHandler(this.btnPedC3_Click);
            // 
            // btnPedC2
            // 
            this.btnPedC2.BackColor = System.Drawing.Color.White;
            this.btnPedC2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC2.BackgroundImage")));
            this.btnPedC2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC2.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC2.ForeColor = System.Drawing.Color.Black;
            this.btnPedC2.Location = new System.Drawing.Point(69, 426);
            this.btnPedC2.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC2.Name = "btnPedC2";
            this.btnPedC2.Size = new System.Drawing.Size(60, 50);
            this.btnPedC2.TabIndex = 265;
            this.btnPedC2.Text = "02";
            this.btnPedC2.UseCompatibleTextRendering = true;
            this.btnPedC2.UseVisualStyleBackColor = false;
            this.btnPedC2.Click += new System.EventHandler(this.btnPedC2_Click);
            // 
            // btnPedC1
            // 
            this.btnPedC1.BackColor = System.Drawing.Color.White;
            this.btnPedC1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC1.BackgroundImage")));
            this.btnPedC1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC1.ForeColor = System.Drawing.Color.Black;
            this.btnPedC1.Location = new System.Drawing.Point(8, 426);
            this.btnPedC1.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC1.Name = "btnPedC1";
            this.btnPedC1.Size = new System.Drawing.Size(60, 50);
            this.btnPedC1.TabIndex = 264;
            this.btnPedC1.Text = "01";
            this.btnPedC1.UseCompatibleTextRendering = true;
            this.btnPedC1.UseVisualStyleBackColor = false;
            this.btnPedC1.Click += new System.EventHandler(this.btnPedC1_Click);
            // 
            // btnPedB18
            // 
            this.btnPedB18.BackColor = System.Drawing.Color.White;
            this.btnPedB18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB18.BackgroundImage")));
            this.btnPedB18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB18.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB18.ForeColor = System.Drawing.Color.Black;
            this.btnPedB18.Location = new System.Drawing.Point(496, 339);
            this.btnPedB18.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB18.Name = "btnPedB18";
            this.btnPedB18.Size = new System.Drawing.Size(60, 50);
            this.btnPedB18.TabIndex = 263;
            this.btnPedB18.Text = "18";
            this.btnPedB18.UseCompatibleTextRendering = true;
            this.btnPedB18.UseVisualStyleBackColor = false;
            this.btnPedB18.Click += new System.EventHandler(this.btnPedB18_Click);
            // 
            // btnPedB17
            // 
            this.btnPedB17.BackColor = System.Drawing.Color.White;
            this.btnPedB17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB17.BackgroundImage")));
            this.btnPedB17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB17.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB17.ForeColor = System.Drawing.Color.Black;
            this.btnPedB17.Location = new System.Drawing.Point(435, 339);
            this.btnPedB17.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB17.Name = "btnPedB17";
            this.btnPedB17.Size = new System.Drawing.Size(60, 50);
            this.btnPedB17.TabIndex = 262;
            this.btnPedB17.Text = "17";
            this.btnPedB17.UseCompatibleTextRendering = true;
            this.btnPedB17.UseVisualStyleBackColor = false;
            this.btnPedB17.Click += new System.EventHandler(this.btnPedB17_Click);
            // 
            // btnPedB16
            // 
            this.btnPedB16.BackColor = System.Drawing.Color.White;
            this.btnPedB16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB16.BackgroundImage")));
            this.btnPedB16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB16.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB16.ForeColor = System.Drawing.Color.Black;
            this.btnPedB16.Location = new System.Drawing.Point(374, 339);
            this.btnPedB16.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB16.Name = "btnPedB16";
            this.btnPedB16.Size = new System.Drawing.Size(60, 50);
            this.btnPedB16.TabIndex = 261;
            this.btnPedB16.Text = "16";
            this.btnPedB16.UseCompatibleTextRendering = true;
            this.btnPedB16.UseVisualStyleBackColor = false;
            this.btnPedB16.Click += new System.EventHandler(this.btnPedB16_Click);
            // 
            // btnPedB15
            // 
            this.btnPedB15.BackColor = System.Drawing.Color.White;
            this.btnPedB15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB15.BackgroundImage")));
            this.btnPedB15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB15.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB15.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB15.ForeColor = System.Drawing.Color.Black;
            this.btnPedB15.Location = new System.Drawing.Point(313, 339);
            this.btnPedB15.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB15.Name = "btnPedB15";
            this.btnPedB15.Size = new System.Drawing.Size(60, 50);
            this.btnPedB15.TabIndex = 260;
            this.btnPedB15.Text = "15";
            this.btnPedB15.UseCompatibleTextRendering = true;
            this.btnPedB15.UseVisualStyleBackColor = false;
            this.btnPedB15.Click += new System.EventHandler(this.btnPedB15_Click);
            // 
            // btnPedB14
            // 
            this.btnPedB14.BackColor = System.Drawing.Color.White;
            this.btnPedB14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB14.BackgroundImage")));
            this.btnPedB14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB14.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB14.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB14.ForeColor = System.Drawing.Color.Black;
            this.btnPedB14.Location = new System.Drawing.Point(252, 339);
            this.btnPedB14.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB14.Name = "btnPedB14";
            this.btnPedB14.Size = new System.Drawing.Size(60, 50);
            this.btnPedB14.TabIndex = 259;
            this.btnPedB14.Text = "14";
            this.btnPedB14.UseCompatibleTextRendering = true;
            this.btnPedB14.UseVisualStyleBackColor = false;
            this.btnPedB14.Click += new System.EventHandler(this.btnPedB14_Click);
            // 
            // btnPedB13
            // 
            this.btnPedB13.BackColor = System.Drawing.Color.White;
            this.btnPedB13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB13.BackgroundImage")));
            this.btnPedB13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB13.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB13.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB13.ForeColor = System.Drawing.Color.Black;
            this.btnPedB13.Location = new System.Drawing.Point(191, 339);
            this.btnPedB13.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB13.Name = "btnPedB13";
            this.btnPedB13.Size = new System.Drawing.Size(60, 50);
            this.btnPedB13.TabIndex = 258;
            this.btnPedB13.Text = "13";
            this.btnPedB13.UseCompatibleTextRendering = true;
            this.btnPedB13.UseVisualStyleBackColor = false;
            this.btnPedB13.Click += new System.EventHandler(this.btnPedB13_Click);
            // 
            // btnPedB12
            // 
            this.btnPedB12.BackColor = System.Drawing.Color.White;
            this.btnPedB12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB12.BackgroundImage")));
            this.btnPedB12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB12.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB12.ForeColor = System.Drawing.Color.Black;
            this.btnPedB12.Location = new System.Drawing.Point(130, 339);
            this.btnPedB12.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB12.Name = "btnPedB12";
            this.btnPedB12.Size = new System.Drawing.Size(60, 50);
            this.btnPedB12.TabIndex = 257;
            this.btnPedB12.Text = "12";
            this.btnPedB12.UseCompatibleTextRendering = true;
            this.btnPedB12.UseVisualStyleBackColor = false;
            this.btnPedB12.Click += new System.EventHandler(this.btnPedB12_Click);
            // 
            // btnPedB11
            // 
            this.btnPedB11.BackColor = System.Drawing.Color.White;
            this.btnPedB11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB11.BackgroundImage")));
            this.btnPedB11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB11.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB11.ForeColor = System.Drawing.Color.Black;
            this.btnPedB11.Location = new System.Drawing.Point(69, 339);
            this.btnPedB11.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB11.Name = "btnPedB11";
            this.btnPedB11.Size = new System.Drawing.Size(60, 50);
            this.btnPedB11.TabIndex = 256;
            this.btnPedB11.Text = "11";
            this.btnPedB11.UseCompatibleTextRendering = true;
            this.btnPedB11.UseVisualStyleBackColor = false;
            this.btnPedB11.Click += new System.EventHandler(this.btnPedB11_Click);
            // 
            // btnPedB10
            // 
            this.btnPedB10.BackColor = System.Drawing.Color.White;
            this.btnPedB10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB10.BackgroundImage")));
            this.btnPedB10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB10.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB10.ForeColor = System.Drawing.Color.Black;
            this.btnPedB10.Location = new System.Drawing.Point(8, 339);
            this.btnPedB10.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB10.Name = "btnPedB10";
            this.btnPedB10.Size = new System.Drawing.Size(60, 50);
            this.btnPedB10.TabIndex = 255;
            this.btnPedB10.Text = "10";
            this.btnPedB10.UseCompatibleTextRendering = true;
            this.btnPedB10.UseVisualStyleBackColor = false;
            this.btnPedB10.Click += new System.EventHandler(this.btnPedB10_Click);
            // 
            // btnPedB9
            // 
            this.btnPedB9.BackColor = System.Drawing.Color.White;
            this.btnPedB9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB9.BackgroundImage")));
            this.btnPedB9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB9.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB9.ForeColor = System.Drawing.Color.Black;
            this.btnPedB9.Location = new System.Drawing.Point(496, 285);
            this.btnPedB9.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB9.Name = "btnPedB9";
            this.btnPedB9.Size = new System.Drawing.Size(60, 50);
            this.btnPedB9.TabIndex = 254;
            this.btnPedB9.Text = "09";
            this.btnPedB9.UseCompatibleTextRendering = true;
            this.btnPedB9.UseVisualStyleBackColor = false;
            this.btnPedB9.Click += new System.EventHandler(this.btnPedB9_Click);
            // 
            // btnPedB8
            // 
            this.btnPedB8.BackColor = System.Drawing.Color.White;
            this.btnPedB8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB8.BackgroundImage")));
            this.btnPedB8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB8.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB8.ForeColor = System.Drawing.Color.Black;
            this.btnPedB8.Location = new System.Drawing.Point(435, 285);
            this.btnPedB8.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB8.Name = "btnPedB8";
            this.btnPedB8.Size = new System.Drawing.Size(60, 50);
            this.btnPedB8.TabIndex = 253;
            this.btnPedB8.Text = "08";
            this.btnPedB8.UseCompatibleTextRendering = true;
            this.btnPedB8.UseVisualStyleBackColor = false;
            this.btnPedB8.Click += new System.EventHandler(this.btnPedB8_Click);
            // 
            // btnPedB7
            // 
            this.btnPedB7.BackColor = System.Drawing.Color.White;
            this.btnPedB7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB7.BackgroundImage")));
            this.btnPedB7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB7.ForeColor = System.Drawing.Color.Black;
            this.btnPedB7.Location = new System.Drawing.Point(374, 285);
            this.btnPedB7.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB7.Name = "btnPedB7";
            this.btnPedB7.Size = new System.Drawing.Size(60, 50);
            this.btnPedB7.TabIndex = 252;
            this.btnPedB7.Text = "07";
            this.btnPedB7.UseCompatibleTextRendering = true;
            this.btnPedB7.UseVisualStyleBackColor = false;
            this.btnPedB7.Click += new System.EventHandler(this.btnPedB7_Click);
            // 
            // btnPedB6
            // 
            this.btnPedB6.BackColor = System.Drawing.Color.White;
            this.btnPedB6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB6.BackgroundImage")));
            this.btnPedB6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB6.ForeColor = System.Drawing.Color.Black;
            this.btnPedB6.Location = new System.Drawing.Point(313, 285);
            this.btnPedB6.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB6.Name = "btnPedB6";
            this.btnPedB6.Size = new System.Drawing.Size(60, 50);
            this.btnPedB6.TabIndex = 251;
            this.btnPedB6.Text = "06";
            this.btnPedB6.UseCompatibleTextRendering = true;
            this.btnPedB6.UseVisualStyleBackColor = false;
            this.btnPedB6.Click += new System.EventHandler(this.btnPedB6_Click);
            // 
            // btnPedB5
            // 
            this.btnPedB5.BackColor = System.Drawing.Color.White;
            this.btnPedB5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB5.BackgroundImage")));
            this.btnPedB5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB5.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB5.ForeColor = System.Drawing.Color.Black;
            this.btnPedB5.Location = new System.Drawing.Point(252, 285);
            this.btnPedB5.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB5.Name = "btnPedB5";
            this.btnPedB5.Size = new System.Drawing.Size(60, 50);
            this.btnPedB5.TabIndex = 250;
            this.btnPedB5.Text = "05";
            this.btnPedB5.UseCompatibleTextRendering = true;
            this.btnPedB5.UseVisualStyleBackColor = false;
            this.btnPedB5.Click += new System.EventHandler(this.btnPedB5_Click);
            // 
            // btnPedB4
            // 
            this.btnPedB4.BackColor = System.Drawing.Color.White;
            this.btnPedB4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB4.BackgroundImage")));
            this.btnPedB4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB4.ForeColor = System.Drawing.Color.Black;
            this.btnPedB4.Location = new System.Drawing.Point(191, 285);
            this.btnPedB4.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB4.Name = "btnPedB4";
            this.btnPedB4.Size = new System.Drawing.Size(60, 50);
            this.btnPedB4.TabIndex = 249;
            this.btnPedB4.Text = "04";
            this.btnPedB4.UseCompatibleTextRendering = true;
            this.btnPedB4.UseVisualStyleBackColor = false;
            this.btnPedB4.Click += new System.EventHandler(this.btnPedB4_Click);
            // 
            // btnPedB3
            // 
            this.btnPedB3.BackColor = System.Drawing.Color.White;
            this.btnPedB3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB3.BackgroundImage")));
            this.btnPedB3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB3.ForeColor = System.Drawing.Color.Black;
            this.btnPedB3.Location = new System.Drawing.Point(130, 285);
            this.btnPedB3.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB3.Name = "btnPedB3";
            this.btnPedB3.Size = new System.Drawing.Size(60, 50);
            this.btnPedB3.TabIndex = 248;
            this.btnPedB3.Text = "03";
            this.btnPedB3.UseCompatibleTextRendering = true;
            this.btnPedB3.UseVisualStyleBackColor = false;
            this.btnPedB3.Click += new System.EventHandler(this.btnPedB3_Click);
            // 
            // btnPedB2
            // 
            this.btnPedB2.BackColor = System.Drawing.Color.White;
            this.btnPedB2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB2.BackgroundImage")));
            this.btnPedB2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB2.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB2.ForeColor = System.Drawing.Color.Black;
            this.btnPedB2.Location = new System.Drawing.Point(69, 285);
            this.btnPedB2.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB2.Name = "btnPedB2";
            this.btnPedB2.Size = new System.Drawing.Size(60, 50);
            this.btnPedB2.TabIndex = 247;
            this.btnPedB2.Text = "02";
            this.btnPedB2.UseCompatibleTextRendering = true;
            this.btnPedB2.UseVisualStyleBackColor = false;
            this.btnPedB2.Click += new System.EventHandler(this.btnPedB2_Click);
            // 
            // btnPedB1
            // 
            this.btnPedB1.BackColor = System.Drawing.Color.White;
            this.btnPedB1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB1.BackgroundImage")));
            this.btnPedB1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB1.ForeColor = System.Drawing.Color.Black;
            this.btnPedB1.Location = new System.Drawing.Point(8, 285);
            this.btnPedB1.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB1.Name = "btnPedB1";
            this.btnPedB1.Size = new System.Drawing.Size(60, 50);
            this.btnPedB1.TabIndex = 246;
            this.btnPedB1.Text = "01";
            this.btnPedB1.UseCompatibleTextRendering = true;
            this.btnPedB1.UseVisualStyleBackColor = false;
            this.btnPedB1.Click += new System.EventHandler(this.btnPedB1_Click);
            // 
            // btnPedM4
            // 
            this.btnPedM4.BackColor = System.Drawing.Color.White;
            this.btnPedM4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM4.BackgroundImage")));
            this.btnPedM4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM4.ForeColor = System.Drawing.Color.Black;
            this.btnPedM4.Location = new System.Drawing.Point(191, 36);
            this.btnPedM4.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM4.Name = "btnPedM4";
            this.btnPedM4.Size = new System.Drawing.Size(60, 50);
            this.btnPedM4.TabIndex = 213;
            this.btnPedM4.Text = "04";
            this.btnPedM4.UseCompatibleTextRendering = true;
            this.btnPedM4.UseVisualStyleBackColor = false;
            this.btnPedM4.Click += new System.EventHandler(this.btnPedM4_Click);
            // 
            // btnPedM1
            // 
            this.btnPedM1.BackColor = System.Drawing.Color.White;
            this.btnPedM1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM1.BackgroundImage")));
            this.btnPedM1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM1.ForeColor = System.Drawing.Color.Black;
            this.btnPedM1.Location = new System.Drawing.Point(8, 36);
            this.btnPedM1.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM1.Name = "btnPedM1";
            this.btnPedM1.Size = new System.Drawing.Size(60, 50);
            this.btnPedM1.TabIndex = 210;
            this.btnPedM1.Text = "01";
            this.btnPedM1.UseCompatibleTextRendering = true;
            this.btnPedM1.UseVisualStyleBackColor = false;
            this.btnPedM1.Click += new System.EventHandler(this.btnPedM1_Click);
            // 
            // btnPedM2
            // 
            this.btnPedM2.BackColor = System.Drawing.Color.White;
            this.btnPedM2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM2.BackgroundImage")));
            this.btnPedM2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM2.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM2.ForeColor = System.Drawing.Color.Black;
            this.btnPedM2.Location = new System.Drawing.Point(69, 36);
            this.btnPedM2.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM2.Name = "btnPedM2";
            this.btnPedM2.Size = new System.Drawing.Size(60, 50);
            this.btnPedM2.TabIndex = 211;
            this.btnPedM2.Text = "02";
            this.btnPedM2.UseCompatibleTextRendering = true;
            this.btnPedM2.UseVisualStyleBackColor = false;
            this.btnPedM2.Click += new System.EventHandler(this.btnPedM2_Click);
            // 
            // btnPedM3
            // 
            this.btnPedM3.BackColor = System.Drawing.Color.White;
            this.btnPedM3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM3.BackgroundImage")));
            this.btnPedM3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM3.ForeColor = System.Drawing.Color.Black;
            this.btnPedM3.Location = new System.Drawing.Point(130, 36);
            this.btnPedM3.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM3.Name = "btnPedM3";
            this.btnPedM3.Size = new System.Drawing.Size(60, 50);
            this.btnPedM3.TabIndex = 212;
            this.btnPedM3.Text = "03";
            this.btnPedM3.UseCompatibleTextRendering = true;
            this.btnPedM3.UseVisualStyleBackColor = false;
            this.btnPedM3.Click += new System.EventHandler(this.btnPedM3_Click);
            // 
            // btnPedM5
            // 
            this.btnPedM5.BackColor = System.Drawing.Color.White;
            this.btnPedM5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM5.BackgroundImage")));
            this.btnPedM5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM5.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM5.ForeColor = System.Drawing.Color.Black;
            this.btnPedM5.Location = new System.Drawing.Point(252, 36);
            this.btnPedM5.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM5.Name = "btnPedM5";
            this.btnPedM5.Size = new System.Drawing.Size(60, 50);
            this.btnPedM5.TabIndex = 214;
            this.btnPedM5.Text = "05";
            this.btnPedM5.UseCompatibleTextRendering = true;
            this.btnPedM5.UseVisualStyleBackColor = false;
            this.btnPedM5.Click += new System.EventHandler(this.btnPedM5_Click);
            // 
            // btnPedM6
            // 
            this.btnPedM6.BackColor = System.Drawing.Color.White;
            this.btnPedM6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM6.BackgroundImage")));
            this.btnPedM6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM6.ForeColor = System.Drawing.Color.Black;
            this.btnPedM6.Location = new System.Drawing.Point(313, 36);
            this.btnPedM6.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM6.Name = "btnPedM6";
            this.btnPedM6.Size = new System.Drawing.Size(60, 50);
            this.btnPedM6.TabIndex = 215;
            this.btnPedM6.Text = "06";
            this.btnPedM6.UseCompatibleTextRendering = true;
            this.btnPedM6.UseVisualStyleBackColor = false;
            this.btnPedM6.Click += new System.EventHandler(this.btnPedM6_Click);
            // 
            // btnPedM7
            // 
            this.btnPedM7.BackColor = System.Drawing.Color.White;
            this.btnPedM7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM7.BackgroundImage")));
            this.btnPedM7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM7.ForeColor = System.Drawing.Color.Black;
            this.btnPedM7.Location = new System.Drawing.Point(374, 36);
            this.btnPedM7.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM7.Name = "btnPedM7";
            this.btnPedM7.Size = new System.Drawing.Size(60, 50);
            this.btnPedM7.TabIndex = 216;
            this.btnPedM7.Text = "07";
            this.btnPedM7.UseCompatibleTextRendering = true;
            this.btnPedM7.UseVisualStyleBackColor = false;
            this.btnPedM7.Click += new System.EventHandler(this.btnPedM7_Click);
            // 
            // btnPedM8
            // 
            this.btnPedM8.BackColor = System.Drawing.Color.White;
            this.btnPedM8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM8.BackgroundImage")));
            this.btnPedM8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM8.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM8.ForeColor = System.Drawing.Color.Black;
            this.btnPedM8.Location = new System.Drawing.Point(435, 36);
            this.btnPedM8.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM8.Name = "btnPedM8";
            this.btnPedM8.Size = new System.Drawing.Size(60, 50);
            this.btnPedM8.TabIndex = 217;
            this.btnPedM8.Text = "08";
            this.btnPedM8.UseCompatibleTextRendering = true;
            this.btnPedM8.UseVisualStyleBackColor = false;
            this.btnPedM8.Click += new System.EventHandler(this.btnPedM8_Click);
            // 
            // btnPedM9
            // 
            this.btnPedM9.BackColor = System.Drawing.Color.White;
            this.btnPedM9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM9.BackgroundImage")));
            this.btnPedM9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM9.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM9.ForeColor = System.Drawing.Color.Black;
            this.btnPedM9.Location = new System.Drawing.Point(496, 36);
            this.btnPedM9.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM9.Name = "btnPedM9";
            this.btnPedM9.Size = new System.Drawing.Size(60, 50);
            this.btnPedM9.TabIndex = 218;
            this.btnPedM9.Text = "09";
            this.btnPedM9.UseCompatibleTextRendering = true;
            this.btnPedM9.UseVisualStyleBackColor = false;
            this.btnPedM9.Click += new System.EventHandler(this.btnPedM9_Click);
            // 
            // btnPedM10
            // 
            this.btnPedM10.BackColor = System.Drawing.Color.White;
            this.btnPedM10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM10.BackgroundImage")));
            this.btnPedM10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM10.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM10.ForeColor = System.Drawing.Color.Black;
            this.btnPedM10.Location = new System.Drawing.Point(8, 90);
            this.btnPedM10.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM10.Name = "btnPedM10";
            this.btnPedM10.Size = new System.Drawing.Size(60, 50);
            this.btnPedM10.TabIndex = 219;
            this.btnPedM10.Text = "10";
            this.btnPedM10.UseCompatibleTextRendering = true;
            this.btnPedM10.UseVisualStyleBackColor = false;
            this.btnPedM10.Click += new System.EventHandler(this.btnPedM10_Click);
            // 
            // btnPedM11
            // 
            this.btnPedM11.BackColor = System.Drawing.Color.White;
            this.btnPedM11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM11.BackgroundImage")));
            this.btnPedM11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM11.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM11.ForeColor = System.Drawing.Color.Black;
            this.btnPedM11.Location = new System.Drawing.Point(69, 90);
            this.btnPedM11.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM11.Name = "btnPedM11";
            this.btnPedM11.Size = new System.Drawing.Size(60, 50);
            this.btnPedM11.TabIndex = 220;
            this.btnPedM11.Text = "11";
            this.btnPedM11.UseCompatibleTextRendering = true;
            this.btnPedM11.UseVisualStyleBackColor = false;
            this.btnPedM11.Click += new System.EventHandler(this.btnPedM11_Click);
            // 
            // btnPedM12
            // 
            this.btnPedM12.BackColor = System.Drawing.Color.White;
            this.btnPedM12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM12.BackgroundImage")));
            this.btnPedM12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM12.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM12.ForeColor = System.Drawing.Color.Black;
            this.btnPedM12.Location = new System.Drawing.Point(130, 90);
            this.btnPedM12.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM12.Name = "btnPedM12";
            this.btnPedM12.Size = new System.Drawing.Size(60, 50);
            this.btnPedM12.TabIndex = 221;
            this.btnPedM12.Text = "12";
            this.btnPedM12.UseCompatibleTextRendering = true;
            this.btnPedM12.UseVisualStyleBackColor = false;
            this.btnPedM12.Click += new System.EventHandler(this.btnPedM12_Click);
            // 
            // btnPedM13
            // 
            this.btnPedM13.BackColor = System.Drawing.Color.White;
            this.btnPedM13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM13.BackgroundImage")));
            this.btnPedM13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM13.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM13.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM13.ForeColor = System.Drawing.Color.Black;
            this.btnPedM13.Location = new System.Drawing.Point(191, 90);
            this.btnPedM13.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM13.Name = "btnPedM13";
            this.btnPedM13.Size = new System.Drawing.Size(60, 50);
            this.btnPedM13.TabIndex = 222;
            this.btnPedM13.Text = "13";
            this.btnPedM13.UseCompatibleTextRendering = true;
            this.btnPedM13.UseVisualStyleBackColor = false;
            this.btnPedM13.Click += new System.EventHandler(this.btnPedM13_Click);
            // 
            // btnPedM14
            // 
            this.btnPedM14.BackColor = System.Drawing.Color.White;
            this.btnPedM14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM14.BackgroundImage")));
            this.btnPedM14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM14.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM14.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM14.ForeColor = System.Drawing.Color.Black;
            this.btnPedM14.Location = new System.Drawing.Point(252, 90);
            this.btnPedM14.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM14.Name = "btnPedM14";
            this.btnPedM14.Size = new System.Drawing.Size(60, 50);
            this.btnPedM14.TabIndex = 223;
            this.btnPedM14.Text = "14";
            this.btnPedM14.UseCompatibleTextRendering = true;
            this.btnPedM14.UseVisualStyleBackColor = false;
            this.btnPedM14.Click += new System.EventHandler(this.btnPedM14_Click);
            // 
            // btnPedM15
            // 
            this.btnPedM15.BackColor = System.Drawing.Color.White;
            this.btnPedM15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM15.BackgroundImage")));
            this.btnPedM15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM15.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM15.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM15.ForeColor = System.Drawing.Color.Black;
            this.btnPedM15.Location = new System.Drawing.Point(313, 90);
            this.btnPedM15.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM15.Name = "btnPedM15";
            this.btnPedM15.Size = new System.Drawing.Size(60, 50);
            this.btnPedM15.TabIndex = 224;
            this.btnPedM15.Text = "15";
            this.btnPedM15.UseCompatibleTextRendering = true;
            this.btnPedM15.UseVisualStyleBackColor = false;
            this.btnPedM15.Click += new System.EventHandler(this.btnPedM15_Click);
            // 
            // btnPedM16
            // 
            this.btnPedM16.BackColor = System.Drawing.Color.White;
            this.btnPedM16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM16.BackgroundImage")));
            this.btnPedM16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM16.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM16.ForeColor = System.Drawing.Color.Black;
            this.btnPedM16.Location = new System.Drawing.Point(374, 90);
            this.btnPedM16.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM16.Name = "btnPedM16";
            this.btnPedM16.Size = new System.Drawing.Size(60, 50);
            this.btnPedM16.TabIndex = 225;
            this.btnPedM16.Text = "16";
            this.btnPedM16.UseCompatibleTextRendering = true;
            this.btnPedM16.UseVisualStyleBackColor = false;
            this.btnPedM16.Click += new System.EventHandler(this.btnPedM16_Click);
            // 
            // btnPedM17
            // 
            this.btnPedM17.BackColor = System.Drawing.Color.White;
            this.btnPedM17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM17.BackgroundImage")));
            this.btnPedM17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM17.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM17.ForeColor = System.Drawing.Color.Black;
            this.btnPedM17.Location = new System.Drawing.Point(435, 90);
            this.btnPedM17.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM17.Name = "btnPedM17";
            this.btnPedM17.Size = new System.Drawing.Size(60, 50);
            this.btnPedM17.TabIndex = 226;
            this.btnPedM17.Text = "17";
            this.btnPedM17.UseCompatibleTextRendering = true;
            this.btnPedM17.UseVisualStyleBackColor = false;
            this.btnPedM17.Click += new System.EventHandler(this.btnPedM17_Click);
            // 
            // btnPedM18
            // 
            this.btnPedM18.BackColor = System.Drawing.Color.White;
            this.btnPedM18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM18.BackgroundImage")));
            this.btnPedM18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM18.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM18.ForeColor = System.Drawing.Color.Black;
            this.btnPedM18.Location = new System.Drawing.Point(496, 90);
            this.btnPedM18.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM18.Name = "btnPedM18";
            this.btnPedM18.Size = new System.Drawing.Size(60, 50);
            this.btnPedM18.TabIndex = 227;
            this.btnPedM18.Text = "18";
            this.btnPedM18.UseCompatibleTextRendering = true;
            this.btnPedM18.UseVisualStyleBackColor = false;
            this.btnPedM18.Click += new System.EventHandler(this.btnPedM18_Click);
            // 
            // btnPedM19
            // 
            this.btnPedM19.BackColor = System.Drawing.Color.White;
            this.btnPedM19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM19.BackgroundImage")));
            this.btnPedM19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM19.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM19.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM19.ForeColor = System.Drawing.Color.Black;
            this.btnPedM19.Location = new System.Drawing.Point(8, 143);
            this.btnPedM19.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM19.Name = "btnPedM19";
            this.btnPedM19.Size = new System.Drawing.Size(60, 50);
            this.btnPedM19.TabIndex = 228;
            this.btnPedM19.Text = "19";
            this.btnPedM19.UseCompatibleTextRendering = true;
            this.btnPedM19.UseVisualStyleBackColor = false;
            this.btnPedM19.Click += new System.EventHandler(this.btnPedM19_Click);
            // 
            // btnPedM20
            // 
            this.btnPedM20.BackColor = System.Drawing.Color.White;
            this.btnPedM20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM20.BackgroundImage")));
            this.btnPedM20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM20.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM20.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM20.ForeColor = System.Drawing.Color.Black;
            this.btnPedM20.Location = new System.Drawing.Point(69, 143);
            this.btnPedM20.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM20.Name = "btnPedM20";
            this.btnPedM20.Size = new System.Drawing.Size(60, 50);
            this.btnPedM20.TabIndex = 229;
            this.btnPedM20.Text = "20";
            this.btnPedM20.UseCompatibleTextRendering = true;
            this.btnPedM20.UseVisualStyleBackColor = false;
            this.btnPedM20.Click += new System.EventHandler(this.btnPedM20_Click);
            // 
            // btnPedM21
            // 
            this.btnPedM21.BackColor = System.Drawing.Color.White;
            this.btnPedM21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM21.BackgroundImage")));
            this.btnPedM21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM21.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM21.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM21.ForeColor = System.Drawing.Color.Black;
            this.btnPedM21.Location = new System.Drawing.Point(130, 143);
            this.btnPedM21.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM21.Name = "btnPedM21";
            this.btnPedM21.Size = new System.Drawing.Size(60, 50);
            this.btnPedM21.TabIndex = 230;
            this.btnPedM21.Text = "21";
            this.btnPedM21.UseCompatibleTextRendering = true;
            this.btnPedM21.UseVisualStyleBackColor = false;
            this.btnPedM21.Click += new System.EventHandler(this.btnPedM21_Click);
            // 
            // btnPedM22
            // 
            this.btnPedM22.BackColor = System.Drawing.Color.White;
            this.btnPedM22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM22.BackgroundImage")));
            this.btnPedM22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM22.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM22.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM22.ForeColor = System.Drawing.Color.Black;
            this.btnPedM22.Location = new System.Drawing.Point(191, 143);
            this.btnPedM22.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM22.Name = "btnPedM22";
            this.btnPedM22.Size = new System.Drawing.Size(60, 50);
            this.btnPedM22.TabIndex = 231;
            this.btnPedM22.Text = "22";
            this.btnPedM22.UseCompatibleTextRendering = true;
            this.btnPedM22.UseVisualStyleBackColor = false;
            this.btnPedM22.Click += new System.EventHandler(this.btnPedM22_Click);
            // 
            // btnPedM23
            // 
            this.btnPedM23.BackColor = System.Drawing.Color.White;
            this.btnPedM23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM23.BackgroundImage")));
            this.btnPedM23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM23.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM23.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM23.ForeColor = System.Drawing.Color.Black;
            this.btnPedM23.Location = new System.Drawing.Point(252, 143);
            this.btnPedM23.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM23.Name = "btnPedM23";
            this.btnPedM23.Size = new System.Drawing.Size(60, 50);
            this.btnPedM23.TabIndex = 232;
            this.btnPedM23.Text = "23";
            this.btnPedM23.UseCompatibleTextRendering = true;
            this.btnPedM23.UseVisualStyleBackColor = false;
            this.btnPedM23.Click += new System.EventHandler(this.btnPedM23_Click);
            // 
            // btnPedM24
            // 
            this.btnPedM24.BackColor = System.Drawing.Color.White;
            this.btnPedM24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM24.BackgroundImage")));
            this.btnPedM24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM24.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM24.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM24.ForeColor = System.Drawing.Color.Black;
            this.btnPedM24.Location = new System.Drawing.Point(313, 143);
            this.btnPedM24.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM24.Name = "btnPedM24";
            this.btnPedM24.Size = new System.Drawing.Size(60, 50);
            this.btnPedM24.TabIndex = 233;
            this.btnPedM24.Text = "24";
            this.btnPedM24.UseCompatibleTextRendering = true;
            this.btnPedM24.UseVisualStyleBackColor = false;
            this.btnPedM24.Click += new System.EventHandler(this.btnPedM24_Click);
            // 
            // btnPedM25
            // 
            this.btnPedM25.BackColor = System.Drawing.Color.White;
            this.btnPedM25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM25.BackgroundImage")));
            this.btnPedM25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM25.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM25.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM25.ForeColor = System.Drawing.Color.Black;
            this.btnPedM25.Location = new System.Drawing.Point(374, 143);
            this.btnPedM25.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM25.Name = "btnPedM25";
            this.btnPedM25.Size = new System.Drawing.Size(60, 50);
            this.btnPedM25.TabIndex = 234;
            this.btnPedM25.Text = "25";
            this.btnPedM25.UseCompatibleTextRendering = true;
            this.btnPedM25.UseVisualStyleBackColor = false;
            this.btnPedM25.Click += new System.EventHandler(this.btnPedM25_Click);
            // 
            // btnPedM26
            // 
            this.btnPedM26.BackColor = System.Drawing.Color.White;
            this.btnPedM26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM26.BackgroundImage")));
            this.btnPedM26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM26.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM26.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM26.ForeColor = System.Drawing.Color.Black;
            this.btnPedM26.Location = new System.Drawing.Point(435, 143);
            this.btnPedM26.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM26.Name = "btnPedM26";
            this.btnPedM26.Size = new System.Drawing.Size(60, 50);
            this.btnPedM26.TabIndex = 235;
            this.btnPedM26.Text = "26";
            this.btnPedM26.UseCompatibleTextRendering = true;
            this.btnPedM26.UseVisualStyleBackColor = false;
            this.btnPedM26.Click += new System.EventHandler(this.btnPedM26_Click);
            // 
            // btnPedM27
            // 
            this.btnPedM27.BackColor = System.Drawing.Color.White;
            this.btnPedM27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM27.BackgroundImage")));
            this.btnPedM27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM27.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM27.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM27.ForeColor = System.Drawing.Color.Black;
            this.btnPedM27.Location = new System.Drawing.Point(496, 143);
            this.btnPedM27.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM27.Name = "btnPedM27";
            this.btnPedM27.Size = new System.Drawing.Size(60, 50);
            this.btnPedM27.TabIndex = 236;
            this.btnPedM27.Text = "27";
            this.btnPedM27.UseCompatibleTextRendering = true;
            this.btnPedM27.UseVisualStyleBackColor = false;
            this.btnPedM27.Click += new System.EventHandler(this.btnPedM27_Click);
            // 
            // btnPedM28
            // 
            this.btnPedM28.BackColor = System.Drawing.Color.White;
            this.btnPedM28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM28.BackgroundImage")));
            this.btnPedM28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM28.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM28.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM28.ForeColor = System.Drawing.Color.Black;
            this.btnPedM28.Location = new System.Drawing.Point(8, 196);
            this.btnPedM28.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM28.Name = "btnPedM28";
            this.btnPedM28.Size = new System.Drawing.Size(60, 50);
            this.btnPedM28.TabIndex = 237;
            this.btnPedM28.Text = "28";
            this.btnPedM28.UseCompatibleTextRendering = true;
            this.btnPedM28.UseVisualStyleBackColor = false;
            this.btnPedM28.Click += new System.EventHandler(this.btnPedM28_Click);
            // 
            // btnPedM29
            // 
            this.btnPedM29.BackColor = System.Drawing.Color.White;
            this.btnPedM29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM29.BackgroundImage")));
            this.btnPedM29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM29.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM29.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM29.ForeColor = System.Drawing.Color.Black;
            this.btnPedM29.Location = new System.Drawing.Point(69, 196);
            this.btnPedM29.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM29.Name = "btnPedM29";
            this.btnPedM29.Size = new System.Drawing.Size(60, 50);
            this.btnPedM29.TabIndex = 238;
            this.btnPedM29.Text = "29";
            this.btnPedM29.UseCompatibleTextRendering = true;
            this.btnPedM29.UseVisualStyleBackColor = false;
            this.btnPedM29.Click += new System.EventHandler(this.btnPedM29_Click);
            // 
            // btnPedM30
            // 
            this.btnPedM30.BackColor = System.Drawing.Color.White;
            this.btnPedM30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM30.BackgroundImage")));
            this.btnPedM30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM30.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM30.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM30.ForeColor = System.Drawing.Color.Black;
            this.btnPedM30.Location = new System.Drawing.Point(130, 196);
            this.btnPedM30.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM30.Name = "btnPedM30";
            this.btnPedM30.Size = new System.Drawing.Size(60, 50);
            this.btnPedM30.TabIndex = 239;
            this.btnPedM30.Text = "30";
            this.btnPedM30.UseCompatibleTextRendering = true;
            this.btnPedM30.UseVisualStyleBackColor = false;
            this.btnPedM30.Click += new System.EventHandler(this.btnPedM30_Click);
            // 
            // btnPedM31
            // 
            this.btnPedM31.BackColor = System.Drawing.Color.White;
            this.btnPedM31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM31.BackgroundImage")));
            this.btnPedM31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM31.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM31.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM31.ForeColor = System.Drawing.Color.Black;
            this.btnPedM31.Location = new System.Drawing.Point(191, 196);
            this.btnPedM31.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM31.Name = "btnPedM31";
            this.btnPedM31.Size = new System.Drawing.Size(60, 50);
            this.btnPedM31.TabIndex = 240;
            this.btnPedM31.Text = "31";
            this.btnPedM31.UseCompatibleTextRendering = true;
            this.btnPedM31.UseVisualStyleBackColor = false;
            this.btnPedM31.Click += new System.EventHandler(this.btnPedM31_Click);
            // 
            // btnPedM32
            // 
            this.btnPedM32.BackColor = System.Drawing.Color.White;
            this.btnPedM32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM32.BackgroundImage")));
            this.btnPedM32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM32.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM32.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM32.ForeColor = System.Drawing.Color.Black;
            this.btnPedM32.Location = new System.Drawing.Point(252, 196);
            this.btnPedM32.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM32.Name = "btnPedM32";
            this.btnPedM32.Size = new System.Drawing.Size(60, 50);
            this.btnPedM32.TabIndex = 241;
            this.btnPedM32.Text = "32";
            this.btnPedM32.UseCompatibleTextRendering = true;
            this.btnPedM32.UseVisualStyleBackColor = false;
            this.btnPedM32.Click += new System.EventHandler(this.btnPedM32_Click);
            // 
            // btnPedM33
            // 
            this.btnPedM33.BackColor = System.Drawing.Color.White;
            this.btnPedM33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM33.BackgroundImage")));
            this.btnPedM33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM33.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM33.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM33.ForeColor = System.Drawing.Color.Black;
            this.btnPedM33.Location = new System.Drawing.Point(313, 196);
            this.btnPedM33.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM33.Name = "btnPedM33";
            this.btnPedM33.Size = new System.Drawing.Size(60, 50);
            this.btnPedM33.TabIndex = 242;
            this.btnPedM33.Text = "33";
            this.btnPedM33.UseCompatibleTextRendering = true;
            this.btnPedM33.UseVisualStyleBackColor = false;
            this.btnPedM33.Click += new System.EventHandler(this.btnPedM33_Click);
            // 
            // btnPedM34
            // 
            this.btnPedM34.BackColor = System.Drawing.Color.White;
            this.btnPedM34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM34.BackgroundImage")));
            this.btnPedM34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM34.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM34.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM34.ForeColor = System.Drawing.Color.Black;
            this.btnPedM34.Location = new System.Drawing.Point(374, 196);
            this.btnPedM34.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM34.Name = "btnPedM34";
            this.btnPedM34.Size = new System.Drawing.Size(60, 50);
            this.btnPedM34.TabIndex = 243;
            this.btnPedM34.Text = "34";
            this.btnPedM34.UseCompatibleTextRendering = true;
            this.btnPedM34.UseVisualStyleBackColor = false;
            this.btnPedM34.Click += new System.EventHandler(this.btnPedM34_Click);
            // 
            // btnPedM35
            // 
            this.btnPedM35.BackColor = System.Drawing.Color.White;
            this.btnPedM35.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM35.BackgroundImage")));
            this.btnPedM35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM35.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM35.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM35.ForeColor = System.Drawing.Color.Black;
            this.btnPedM35.Location = new System.Drawing.Point(435, 196);
            this.btnPedM35.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM35.Name = "btnPedM35";
            this.btnPedM35.Size = new System.Drawing.Size(60, 50);
            this.btnPedM35.TabIndex = 244;
            this.btnPedM35.Text = "35";
            this.btnPedM35.UseCompatibleTextRendering = true;
            this.btnPedM35.UseVisualStyleBackColor = false;
            this.btnPedM35.Click += new System.EventHandler(this.btnPedM35_Click);
            // 
            // btnPedM36
            // 
            this.btnPedM36.BackColor = System.Drawing.Color.White;
            this.btnPedM36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM36.BackgroundImage")));
            this.btnPedM36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM36.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM36.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM36.ForeColor = System.Drawing.Color.Black;
            this.btnPedM36.Location = new System.Drawing.Point(496, 196);
            this.btnPedM36.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM36.Name = "btnPedM36";
            this.btnPedM36.Size = new System.Drawing.Size(60, 50);
            this.btnPedM36.TabIndex = 245;
            this.btnPedM36.Text = "36";
            this.btnPedM36.UseCompatibleTextRendering = true;
            this.btnPedM36.UseVisualStyleBackColor = false;
            this.btnPedM36.Click += new System.EventHandler(this.btnPedM36_Click);
            // 
            // frmPdvImprimir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 536);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.circularProgress1);
            this.Controls.Add(this.btnSalvar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPdvImprimir";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Imprimir Pedidos";
            this.Load += new System.EventHandler(this.frmPdvImprimir_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPedM4;
        private System.Windows.Forms.Button btnPedM1;
        private System.Windows.Forms.Button btnPedM2;
        private System.Windows.Forms.Button btnPedM3;
        private System.Windows.Forms.Button btnPedM5;
        private System.Windows.Forms.Button btnPedM6;
        private System.Windows.Forms.Button btnPedM7;
        private System.Windows.Forms.Button btnPedM8;
        private System.Windows.Forms.Button btnPedM9;
        private System.Windows.Forms.Button btnPedM10;
        private System.Windows.Forms.Button btnPedM11;
        private System.Windows.Forms.Button btnPedM12;
        private System.Windows.Forms.Button btnPedM13;
        private System.Windows.Forms.Button btnPedM14;
        private System.Windows.Forms.Button btnPedM15;
        private System.Windows.Forms.Button btnPedM16;
        private System.Windows.Forms.Button btnPedM17;
        private System.Windows.Forms.Button btnPedM18;
        private System.Windows.Forms.Button btnPedM19;
        private System.Windows.Forms.Button btnPedM20;
        private System.Windows.Forms.Button btnPedM21;
        private System.Windows.Forms.Button btnPedM22;
        private System.Windows.Forms.Button btnPedM23;
        private System.Windows.Forms.Button btnPedM24;
        private System.Windows.Forms.Button btnPedM25;
        private System.Windows.Forms.Button btnPedM26;
        private System.Windows.Forms.Button btnPedM27;
        private System.Windows.Forms.Button btnPedM28;
        private System.Windows.Forms.Button btnPedM29;
        private System.Windows.Forms.Button btnPedM30;
        private System.Windows.Forms.Button btnPedM31;
        private System.Windows.Forms.Button btnPedM32;
        private System.Windows.Forms.Button btnPedM33;
        private System.Windows.Forms.Button btnPedM34;
        private System.Windows.Forms.Button btnPedM35;
        private System.Windows.Forms.Button btnPedM36;
        private System.Windows.Forms.Button btnPedB18;
        private System.Windows.Forms.Button btnPedB17;
        private System.Windows.Forms.Button btnPedB16;
        private System.Windows.Forms.Button btnPedB15;
        private System.Windows.Forms.Button btnPedB14;
        private System.Windows.Forms.Button btnPedB13;
        private System.Windows.Forms.Button btnPedB12;
        private System.Windows.Forms.Button btnPedB11;
        private System.Windows.Forms.Button btnPedB10;
        private System.Windows.Forms.Button btnPedB9;
        private System.Windows.Forms.Button btnPedB8;
        private System.Windows.Forms.Button btnPedB7;
        private System.Windows.Forms.Button btnPedB6;
        private System.Windows.Forms.Button btnPedB5;
        private System.Windows.Forms.Button btnPedB4;
        private System.Windows.Forms.Button btnPedB3;
        private System.Windows.Forms.Button btnPedB2;
        private System.Windows.Forms.Button btnPedB1;
        private System.Windows.Forms.Button btnPedC18;
        private System.Windows.Forms.Button btnPedC17;
        private System.Windows.Forms.Button btnPedC16;
        private System.Windows.Forms.Button btnPedC15;
        private System.Windows.Forms.Button btnPedC14;
        private System.Windows.Forms.Button btnPedC13;
        private System.Windows.Forms.Button btnPedC12;
        private System.Windows.Forms.Button btnPedC11;
        private System.Windows.Forms.Button btnPedC10;
        private System.Windows.Forms.Button btnPedC9;
        private System.Windows.Forms.Button btnPedC8;
        private System.Windows.Forms.Button btnPedC7;
        private System.Windows.Forms.Button btnPedC6;
        private System.Windows.Forms.Button btnPedC5;
        private System.Windows.Forms.Button btnPedC4;
        private System.Windows.Forms.Button btnPedC3;
        private System.Windows.Forms.Button btnPedC2;
        private System.Windows.Forms.Button btnPedC1;

    }
}