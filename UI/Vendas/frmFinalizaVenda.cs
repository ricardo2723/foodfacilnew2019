﻿using System;
using System.Drawing;
using System.Windows.Forms;

using DevComponents.DotNetBar;

using Relatorios;
using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmFinalizaVenda : Form
    {
        public decimal numeroVenda;
        public string nomeAtendente;
        public string retorno;
        public string tipoAtendimento;

        VendaCabecalho vendaCabecalho = new VendaCabecalho();

        VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();

        public frmFinalizaVenda(string numeroVenda1, string total, string atendimento, string atendente)
        {
            InitializeComponent();

            numeroVenda = Convert.ToDecimal(numeroVenda1);
            nomeAtendente = atendente;
            txtTotalPedido.Text = total;
            tipoAtendimento = atendimento.ToUpper();
            lblAtendimento.Text = "FINALIZAR ATENDIMENTO - " + tipoAtendimento;
        }

        #region AÇÕES

        private void frmFinalizaVenda_Load(object sender, EventArgs e)
        {
            panelEx5.Enabled = false;
            trocaFundoPanelClick(panelEx2, txtDinheiro);
        }

        private void txtDinheiro_MouseClick(object sender, MouseEventArgs e)
        {
            trocaFundoPanelClick(panelEx2, txtDinheiro);
        }

        private void txtDinheiro_Leave(object sender, EventArgs e)
        {
            trocaFundoPanelLeave(panelEx2, txtDinheiro);
        }

        private void txtDinheiro_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnSalvar.Focus();
                    break;
            }
        }

        private void txtCartao_MouseClick(object sender, MouseEventArgs e)
        {
            trocaFundoPanelClick(panelEx3, txtCartao);
        }

        private void txtCartao_Leave(object sender, EventArgs e)
        {
            trocaFundoPanelLeave(panelEx3, txtCartao);

            if (txtCartao.Text != "0" && txtCartao.Text != "0,00" && txtCartao.Text != "")
            {
                if (lblTipoCartao.Text == "")
                {
                    selecionarTipoCartao();
                    //MessageBox.Show("E necessário informar um tipo de cartão. Para Selecionar o cartão pressione Enter", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCartao.Focus();
                }
            }
            else
            {
                vendaCabecalho.idCartao = 0;
                lblTipoCartao.Text = "";
                btnSalvar.Focus();
            }
        }

        private void txtCartao_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    selecionarTipoCartao();
                    break;
            }
        }

        private void txtTicket_MouseClick(object sender, MouseEventArgs e)
        {
            trocaFundoPanelClick(panelEx4, txtTicket);
        }

        private void txtTicket_Leave(object sender, EventArgs e)
        {
            trocaFundoPanelLeave(panelEx4, txtTicket);
        }

        private void txtTicket_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnSalvar.Focus();
                    break;
            }
        }

        private void txtDesconto_MouseClick(object sender, MouseEventArgs e)
        {
            trocaFundoPanelClick(panelEx5, txtDesconto);
        }

        private void txtDesconto_Leave(object sender, EventArgs e)
        {
            trocaFundoPanelLeave(panelEx5, txtDesconto);
        }

        private void txtDesconto_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnSalvar.Focus();
                    break;
            }
        }

        private void frmFinalizaVenda_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    trocaFundoPanelClick(panelEx2, txtDinheiro);
                    txtDinheiro.Focus();
                    break;
                case Keys.F2:
                    trocaFundoPanelClick(panelEx3, txtCartao);
                    txtCartao.Focus();
                    break;
                case Keys.F3:
                    trocaFundoPanelClick(panelEx4, txtTicket);
                    txtTicket.Focus();
                    break;
                case Keys.F4:
                    panelEx5.Enabled = true;
                    trocaFundoPanelClick(panelEx5, txtDesconto);
                    txtDesconto.Focus();
                    break;
                case Keys.F12:
                    btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void valorPago()
        {
            try
            {
                txtTotalPago.Text = (Convert.ToDecimal(txtDinheiro.Text) + Convert.ToDecimal(txtCartao.Text) + Convert.ToDecimal(txtTicket.Text)).ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("E obrigatório somente numeros", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void troco()
        {
            try
            {
                txtTroco.Text = ((Convert.ToDecimal(txtTotalPago.Text) - Convert.ToDecimal(txtTotalPedido.Text)) + Convert.ToDecimal(txtDesconto.Text)).ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("E obrigatório somente numeros", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

        }

        private void trocaFundoPanelClick(PanelEx panel, TextBox txt)
        {
            panel.Style.BackColor1.Color = Color.PaleGreen;
            panel.Style.BackColor2.Color = Color.PaleGreen;
            txt.BackColor = Color.PaleGreen;
        }

        private void trocaFundoPanelLeave(PanelEx panel, TextBox txt)
        {
            panel.Style.BackColor1.Color = Color.White;
            panel.Style.BackColor2.Color = Color.White;
            txt.BackColor = Color.White;
            txt.Text = checaRetornaDecimal(txt.Text, txt).ToString();

            if (txt.Text == "" || txt.Text == "error")
            {
                txt.Text = "0,00";
                txt.Focus();
                return;
            }

            valorPago();
            troco();
        }

        private string checaRetornaDecimal(string cod, TextBox txt)
        {
            try
            {
                decimal cod1 = Convert.ToDecimal(cod);
                return cod1.ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("O valor e Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txt.Focus();
                return "error";
            }
        }

        private void selecionarTipoCartao()
        {
            if (lblTipoCartao.Text == "")
            {
                frmPdvTipoCartao objTipoCartao = new frmPdvTipoCartao();
                objTipoCartao.ShowDialog();

                if (objTipoCartao.sair != "N")
                {
                    MessageBox.Show("Nenhum Tipo de Cartão foi Selecionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCartao.Focus();
                    return;
                }
                else
                {
                    vendaCabecalho.idCartao = objTipoCartao.idCartao;
                    lblTipoCartao.Text = objTipoCartao.nomeCartao;
                    btnSalvar.Focus();
                }
            }
            else
            {
                btnSalvar.Focus();
            }
        }

        #endregion

        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                //verificar se o total pago e igual total do pedido menos o desconto
                if (Convert.ToDecimal(txtTotalPago.Text) >= (Convert.ToDecimal(txtTotalPedido.Text) - Convert.ToDecimal(txtDesconto.Text)))
                {
                    vendaCabecalho.idCaixaPagamento = Principal.caixa;
                    vendaCabecalho.idVendaCabecalho = Convert.ToInt32(numeroVenda);
                    vendaCabecalho.idCliente = 2;
                    vendaCabecalho.valorVendaCabecalho = Convert.ToDecimal(txtTotalPedido.Text);
                    vendaCabecalho.totalVendaCabecalho = Convert.ToDecimal(txtTotalPago.Text) - Convert.ToDecimal(txtTroco.Text);
                    vendaCabecalho.descontoVendaCabecalho = Convert.ToDecimal(txtDesconto.Text);
                    vendaCabecalho.statusVendaCabecalho = 2;
                    vendaCabecalho.usuarioId = frmLogin.usuariosLogin.usuarioId;
                    vendaCabecalho.atendenteVendaCabecalho = nomeAtendente;

                    if (txtDinheiro.Text != "" && txtDinheiro.Text != "0" && txtDinheiro.Text != "0,00")
                    {
                        vendaCabecalho.valorVCTipoPagamento = Convert.ToDecimal(txtDinheiro.Text) - Convert.ToDecimal(txtTroco.Text);
                        vendaCabecalho.descricaoVCTipoPagamento = "DINHEIRO";

                        vendaCabecalhoNegocios.inserirTipoPagamento(vendaCabecalho);
                    }

                    if (txtCartao.Text != "" && txtCartao.Text != "0" && txtCartao.Text != "0,00")
                    {
                        vendaCabecalho.valorVCTipoPagamento = Convert.ToDecimal(txtCartao.Text);
                        vendaCabecalho.descricaoVCTipoPagamento = "CARTÃO";

                        vendaCabecalhoNegocios.inserirTipoPagamento(vendaCabecalho);
                    }

                    if (txtTicket.Text != "" && txtTicket.Text != "0" && txtTicket.Text != "0,00")
                    {
                        vendaCabecalho.valorVCTipoPagamento = Convert.ToDecimal(txtTicket.Text);
                        vendaCabecalho.descricaoVCTipoPagamento = "TICKET";

                        vendaCabecalhoNegocios.inserirTipoPagamento(vendaCabecalho);
                    }

                    retorno = vendaCabecalhoNegocios.finalizarVenda(vendaCabecalho);

                    Convert.ToInt32(retorno);

                    VendaDetalhesNegocios vdn = new VendaDetalhesNegocios();
                    retorno = vdn.finalizarItens(Convert.ToInt32(numeroVenda), 5);

                    Convert.ToInt32(retorno);

                    vendaCabecalho.nomeAtendimento = tipoAtendimento;
                    vendaCabecalho.troco = txtTroco.Text;
                    vendaCabecalho.dinheiro = txtDinheiro.Text;
                    vendaCabecalho.cartao = txtCartao.Text;
                    vendaCabecalho.ticket = txtTicket.Text;

                    frmRltImprimirCupom objFrmImpCupom = new frmRltImprimirCupom(vendaCabecalho, imprimiCupom.imprime, frmLogin.meuLanche);
                    objFrmImpCupom.ShowDialog();

                    vdn.ExcluirItensExtraDetalhes(Convert.ToInt32(numeroVenda));

                    MessageBox.Show("Pedido Finalizado com Sucesso", "Finalizando Venda", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.DialogResult = DialogResult.Yes;

                }
                else
                {
                    MessageBox.Show("Impossivel Finalizar Pedido. Detalhes: O Valor Pago e menor que o Valor do Pedido", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi Possivel Finalizar Venda. Detalhes: " + retorno, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private void btnAReceber_Click(object sender, EventArgs e)
        {

        }

        #endregion

    }
}
