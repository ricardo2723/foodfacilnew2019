﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmPdvEspecial : Form
    {
        frmPdv objFrmPdv = new frmPdv();

        
        public string descricaoEspecial;
        public int itemEspecial;
        public string sair;
        
        public frmPdvEspecial()
        {
            InitializeComponent();
            //txtItem.Text = item.ToString();

            carregaGrupos();
        }

        private void cbEspecial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    descricaoEspecial = cbEspecial.Text;
                    sair = "N";
                    Close();
                    break;
            }
        } 
                
        private void txtItem_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtItem.Text != "")
                    {
                        cbEspecial.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtItem_Leave(object sender, EventArgs e)
        {
            if (txtItem.Text == "" || Convert.ToDecimal(txtItem.Text) == 0)
            {
                MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtItem.Focus();
            }
        }

        private void frmPdvEspecial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    cbEspecial.Focus();
                    break;
                case Keys.F3:
                    txtItem.Focus();
                    break;
            }
        }

        private void btnNovoEspecial_Click(object sender, EventArgs e)
        {
            frmCadEspeciais objFrmExtras = new frmCadEspeciais(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmExtras.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                carregaGrupos();
            }
        }

        private void carregaGrupos()
        {
            //Carrega Combo Grupos
            EspeciaisNegocios especiaisNegocios = new EspeciaisNegocios();
            EspeciaisCollections especiaisCollections = especiaisNegocios.ConsultarDescricao("%");
            cbEspecial.DataSource = especiaisCollections;
        }

    }
}
