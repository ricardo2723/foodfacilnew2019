﻿using System.Windows.Forms;

namespace UI
{
    public partial class frmNomeAtendente : Form
    {
        public string nomeAtendente = frmLogin.usuariosLogin.usuarioLogin;

        public frmNomeAtendente()
        {
            InitializeComponent();
            txtNome.Text = nomeAtendente;
            txtNome.Focus();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtNome.Text == "")
                    {
                        MessageBox.Show("É necessário um Nome para o Atendente", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        txtNome.Focus();
                        return;
                    }
                    if (txtNome.Text.Length < 4)
                    {
                        MessageBox.Show("O nome do Atendente deve ter no MINIMO 4 Letras", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        txtNome.Focus();
                        return;
                    }

                    nomeAtendente = txtNome.Text;
                    this.DialogResult = DialogResult.Yes;

                    break;
            }
        }     
          
        
    }
}
