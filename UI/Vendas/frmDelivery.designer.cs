﻿namespace UI
{
    partial class frmDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDelivery));
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTotalGeralPedido = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtValorFrete = new System.Windows.Forms.TextBox();
            this.ckbTroco = new System.Windows.Forms.CheckBox();
            this.gbTroco = new System.Windows.Forms.GroupBox();
            this.txtValorTroco = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTrocoPara = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbTipoPagamento = new System.Windows.Forms.ComboBox();
            this.btnImprimir = new DevComponents.DotNetBar.ButtonX();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblAtendente = new System.Windows.Forms.Label();
            this.lblCodVenda = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblNomeEmpresa = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dgCupom = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.extras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.especiais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataEnvioVendaDetalhes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExtra = new DevComponents.DotNetBar.ButtonX();
            this.btnFinalizarPedido = new DevComponents.DotNetBar.ButtonX();
            this.btnEspecial = new DevComponents.DotNetBar.ButtonX();
            this.btnPesquisarProdutos = new DevComponents.DotNetBar.ButtonX();
            this.btnVisualizarPedido = new DevComponents.DotNetBar.ButtonX();
            this.btnAddQtd = new DevComponents.DotNetBar.ButtonX();
            this.btnCancelarItem = new DevComponents.DotNetBar.ButtonX();
            this.btnCancelarPedido = new DevComponents.DotNetBar.ButtonX();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtTotalGeral = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtItens = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtValorTotal = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtValorUnitario = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.PdvDataHora = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOperador = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgcDadosCliente = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn();
            this.Column8 = new DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbPontoReferencia = new System.Windows.Forms.GroupBox();
            this.lblPontoReferencia = new System.Windows.Forms.Label();
            this.gbDadosCliente = new System.Windows.Forms.GroupBox();
            this.lblEnderecoCliente = new System.Windows.Forms.Label();
            this.lblNomeCliente = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmsMenuPdv = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.retiradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbTroco.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCupom)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgcDadosCliente)).BeginInit();
            this.gbPontoReferencia.SuspendLayout();
            this.gbDadosCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.cmsMenuPdv.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.btnImprimir);
            this.panel3.Controls.Add(this.panelEx2);
            this.panel3.Controls.Add(this.btnExtra);
            this.panel3.Controls.Add(this.btnFinalizarPedido);
            this.panel3.Controls.Add(this.btnEspecial);
            this.panel3.Controls.Add(this.btnPesquisarProdutos);
            this.panel3.Controls.Add(this.btnVisualizarPedido);
            this.panel3.Controls.Add(this.btnAddQtd);
            this.panel3.Controls.Add(this.btnCancelarItem);
            this.panel3.Controls.Add(this.btnCancelarPedido);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Location = new System.Drawing.Point(20, 182);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1333, 580);
            this.panel3.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.txtTotalGeralPedido);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtValorFrete);
            this.groupBox2.Controls.Add(this.ckbTroco);
            this.groupBox2.Controls.Add(this.gbTroco);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.cbTipoPagamento);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(860, -2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(471, 419);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da Entrega";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Blue;
            this.label21.Location = new System.Drawing.Point(6, 342);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(147, 27);
            this.label21.TabIndex = 9;
            this.label21.Text = "Total Geral";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTotalGeralPedido
            // 
            this.txtTotalGeralPedido.BackColor = System.Drawing.Color.White;
            this.txtTotalGeralPedido.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalGeralPedido.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalGeralPedido.Location = new System.Drawing.Point(11, 372);
            this.txtTotalGeralPedido.Name = "txtTotalGeralPedido";
            this.txtTotalGeralPedido.ReadOnly = true;
            this.txtTotalGeralPedido.Size = new System.Drawing.Size(445, 40);
            this.txtTotalGeralPedido.TabIndex = 4;
            this.txtTotalGeralPedido.Text = "0,00";
            this.txtTotalGeralPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(5, 255);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(147, 27);
            this.label20.TabIndex = 8;
            this.label20.Text = "Valor do Frete";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtValorFrete
            // 
            this.txtValorFrete.BackColor = System.Drawing.Color.White;
            this.txtValorFrete.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorFrete.ForeColor = System.Drawing.Color.Red;
            this.txtValorFrete.Location = new System.Drawing.Point(10, 285);
            this.txtValorFrete.Name = "txtValorFrete";
            this.txtValorFrete.ReadOnly = true;
            this.txtValorFrete.Size = new System.Drawing.Size(446, 40);
            this.txtValorFrete.TabIndex = 3;
            this.txtValorFrete.Text = "0,00";
            this.txtValorFrete.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ckbTroco
            // 
            this.ckbTroco.AutoSize = true;
            this.ckbTroco.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbTroco.ForeColor = System.Drawing.Color.Blue;
            this.ckbTroco.Location = new System.Drawing.Point(10, 121);
            this.ckbTroco.Name = "ckbTroco";
            this.ckbTroco.Size = new System.Drawing.Size(83, 27);
            this.ckbTroco.TabIndex = 1;
            this.ckbTroco.Text = "Troco";
            this.ckbTroco.UseVisualStyleBackColor = true;
            this.ckbTroco.Click += new System.EventHandler(this.ckbTroco_Click);
            // 
            // gbTroco
            // 
            this.gbTroco.Controls.Add(this.txtValorTroco);
            this.gbTroco.Controls.Add(this.label19);
            this.gbTroco.Controls.Add(this.txtTrocoPara);
            this.gbTroco.Controls.Add(this.label18);
            this.gbTroco.Enabled = false;
            this.gbTroco.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.gbTroco.Location = new System.Drawing.Point(10, 139);
            this.gbTroco.Name = "gbTroco";
            this.gbTroco.Size = new System.Drawing.Size(455, 100);
            this.gbTroco.TabIndex = 2;
            this.gbTroco.TabStop = false;
            // 
            // txtValorTroco
            // 
            this.txtValorTroco.BackColor = System.Drawing.Color.White;
            this.txtValorTroco.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorTroco.Location = new System.Drawing.Point(241, 52);
            this.txtValorTroco.Name = "txtValorTroco";
            this.txtValorTroco.ReadOnly = true;
            this.txtValorTroco.Size = new System.Drawing.Size(205, 40);
            this.txtValorTroco.TabIndex = 1;
            this.txtValorTroco.Text = "0,00";
            this.txtValorTroco.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(237, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(178, 27);
            this.label19.TabIndex = 6;
            this.label19.Text = "Valor do Troco";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTrocoPara
            // 
            this.txtTrocoPara.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrocoPara.Location = new System.Drawing.Point(10, 52);
            this.txtTrocoPara.Name = "txtTrocoPara";
            this.txtTrocoPara.Size = new System.Drawing.Size(215, 40);
            this.txtTrocoPara.TabIndex = 0;
            this.txtTrocoPara.Text = "0,00";
            this.txtTrocoPara.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTrocoPara.Leave += new System.EventHandler(this.txtTrocoPara_Leave);
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(6, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(117, 27);
            this.label18.TabIndex = 0;
            this.label18.Text = "Troco Para";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(6, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(446, 27);
            this.label16.TabIndex = 1;
            this.label16.Text = "Forma Pagamento (ESC)";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbTipoPagamento
            // 
            this.cbTipoPagamento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbTipoPagamento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoPagamento.DisplayMember = "formaPagamentoDescricao";
            this.cbTipoPagamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoPagamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTipoPagamento.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoPagamento.FormattingEnabled = true;
            this.cbTipoPagamento.Location = new System.Drawing.Point(9, 63);
            this.cbTipoPagamento.Name = "cbTipoPagamento";
            this.cbTipoPagamento.Size = new System.Drawing.Size(447, 40);
            this.cbTipoPagamento.TabIndex = 0;
            this.cbTipoPagamento.ValueMember = "formaPagamentoId";
            this.cbTipoPagamento.Leave += new System.EventHandler(this.cbTipoPagamento_Leave);
            // 
            // btnImprimir
            // 
            this.btnImprimir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnImprimir.BackColor = System.Drawing.Color.LimeGreen;
            this.btnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnImprimir.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImprimir.Image = global::UI.Properties.Resources.printer;
            this.btnImprimir.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnImprimir.Location = new System.Drawing.Point(1018, 529);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(150, 44);
            this.btnImprimir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnImprimir.TabIndex = 10;
            this.btnImprimir.Text = "Imprimir - F9";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // panelEx2
            // 
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.panel16);
            this.panelEx2.Controls.Add(this.dgCupom);
            this.panelEx2.Location = new System.Drawing.Point(3, 5);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(529, 568);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.Color = System.Drawing.Color.White;
            this.panelEx2.Style.BackColor2.Color = System.Drawing.Color.White;
            this.panelEx2.Style.BorderColor.Color = System.Drawing.Color.LightGray;
            this.panelEx2.Style.BorderWidth = 2;
            this.panelEx2.Style.CornerDiameter = 0;
            this.panelEx2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 9;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.lblAtendente);
            this.panel16.Controls.Add(this.lblCodVenda);
            this.panel16.Controls.Add(this.label8);
            this.panel16.Controls.Add(this.lblNomeEmpresa);
            this.panel16.Controls.Add(this.label10);
            this.panel16.Controls.Add(this.label12);
            this.panel16.Controls.Add(this.label11);
            this.panel16.Controls.Add(this.label9);
            this.panel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel16.Location = new System.Drawing.Point(-3, 3);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(529, 133);
            this.panel16.TabIndex = 7;
            // 
            // lblAtendente
            // 
            this.lblAtendente.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendente.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblAtendente.Location = new System.Drawing.Point(6, 16);
            this.lblAtendente.Name = "lblAtendente";
            this.lblAtendente.Size = new System.Drawing.Size(464, 19);
            this.lblAtendente.TabIndex = 9;
            this.lblAtendente.Text = "Atendente:";
            this.lblAtendente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodVenda
            // 
            this.lblCodVenda.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodVenda.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCodVenda.Location = new System.Drawing.Point(6, 34);
            this.lblCodVenda.Name = "lblCodVenda";
            this.lblCodVenda.Size = new System.Drawing.Size(456, 14);
            this.lblCodVenda.TabIndex = 8;
            this.lblCodVenda.Text = "Numero Venda: ";
            this.lblCodVenda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(5, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(521, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "RECIBO DE VENDA";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNomeEmpresa
            // 
            this.lblNomeEmpresa.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeEmpresa.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNomeEmpresa.Location = new System.Drawing.Point(6, 0);
            this.lblNomeEmpresa.Name = "lblNomeEmpresa";
            this.lblNomeEmpresa.Size = new System.Drawing.Size(456, 14);
            this.lblNomeEmpresa.TabIndex = 6;
            this.lblNomeEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(2, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(524, 18);
            this.label10.TabIndex = 5;
            this.label10.Text = "-----------------------------------------------------";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(3, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(523, 18);
            this.label12.TabIndex = 4;
            this.label12.Text = "  Quant Preço Unit                            Item";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(3, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(523, 18);
            this.label11.TabIndex = 3;
            this.label11.Text = "Item   Código      Descrição                  Total";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(3, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(523, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "-----------------------------------------------------";
            // 
            // dgCupom
            // 
            this.dgCupom.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dgCupom.AllowUserToAddRows = false;
            this.dgCupom.AllowUserToDeleteRows = false;
            this.dgCupom.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgCupom.BackgroundColor = System.Drawing.Color.White;
            this.dgCupom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgCupom.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgCupom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCupom.ColumnHeadersVisible = false;
            this.dgCupom.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.Column2,
            this.Column3,
            this.Column5,
            this.extras,
            this.especiais,
            this.status,
            this.dataEnvioVendaDetalhes});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCupom.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgCupom.EnableHeadersVisualStyles = false;
            this.dgCupom.GridColor = System.Drawing.Color.White;
            this.dgCupom.Location = new System.Drawing.Point(4, 135);
            this.dgCupom.Margin = new System.Windows.Forms.Padding(0);
            this.dgCupom.MultiSelect = false;
            this.dgCupom.Name = "dgCupom";
            this.dgCupom.ReadOnly = true;
            this.dgCupom.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCupom.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgCupom.RowHeadersVisible = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HotTrack;
            this.dgCupom.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgCupom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgCupom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCupom.Size = new System.Drawing.Size(522, 428);
            this.dgCupom.TabIndex = 6;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 27;
            // 
            // Column4
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle21;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 40;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.MaxInputLength = 32;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 260;
            // 
            // Column5
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle22;
            this.Column5.HeaderText = "";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 92;
            // 
            // extras
            // 
            this.extras.HeaderText = "";
            this.extras.Name = "extras";
            this.extras.ReadOnly = true;
            this.extras.Visible = false;
            // 
            // especiais
            // 
            this.especiais.HeaderText = "";
            this.especiais.Name = "especiais";
            this.especiais.ReadOnly = true;
            this.especiais.Visible = false;
            // 
            // status
            // 
            this.status.HeaderText = "";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Visible = false;
            // 
            // dataEnvioVendaDetalhes
            // 
            this.dataEnvioVendaDetalhes.HeaderText = "";
            this.dataEnvioVendaDetalhes.Name = "dataEnvioVendaDetalhes";
            this.dataEnvioVendaDetalhes.ReadOnly = true;
            // 
            // btnExtra
            // 
            this.btnExtra.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExtra.BackColor = System.Drawing.Color.LimeGreen;
            this.btnExtra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExtra.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnExtra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExtra.Image = global::UI.Properties.Resources._7393_128x1281;
            this.btnExtra.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnExtra.Location = new System.Drawing.Point(1173, 479);
            this.btnExtra.Name = "btnExtra";
            this.btnExtra.Size = new System.Drawing.Size(157, 44);
            this.btnExtra.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExtra.TabIndex = 8;
            this.btnExtra.Text = "   Extras - F7";
            this.btnExtra.Click += new System.EventHandler(this.btnExtra_Click);
            // 
            // btnFinalizarPedido
            // 
            this.btnFinalizarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFinalizarPedido.BackColor = System.Drawing.Color.LimeGreen;
            this.btnFinalizarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFinalizarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnFinalizarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinalizarPedido.Image = global::UI.Properties.Resources.ok_fw;
            this.btnFinalizarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnFinalizarPedido.Location = new System.Drawing.Point(1173, 529);
            this.btnFinalizarPedido.Name = "btnFinalizarPedido";
            this.btnFinalizarPedido.Size = new System.Drawing.Size(157, 44);
            this.btnFinalizarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFinalizarPedido.TabIndex = 2;
            this.btnFinalizarPedido.Text = "Finalizar Pedido - F10";
            this.btnFinalizarPedido.Click += new System.EventHandler(this.btnFinalizarPedido_Click);
            // 
            // btnEspecial
            // 
            this.btnEspecial.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEspecial.BackColor = System.Drawing.Color.LimeGreen;
            this.btnEspecial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEspecial.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnEspecial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEspecial.Image = global::UI.Properties.Resources._7397_128x128;
            this.btnEspecial.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnEspecial.Location = new System.Drawing.Point(863, 529);
            this.btnEspecial.Name = "btnEspecial";
            this.btnEspecial.Size = new System.Drawing.Size(150, 44);
            this.btnEspecial.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEspecial.TabIndex = 9;
            this.btnEspecial.Text = "Especiais - F8";
            this.btnEspecial.Click += new System.EventHandler(this.btnEspecial_Click);
            // 
            // btnPesquisarProdutos
            // 
            this.btnPesquisarProdutos.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPesquisarProdutos.BackColor = System.Drawing.Color.LimeGreen;
            this.btnPesquisarProdutos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPesquisarProdutos.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnPesquisarProdutos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarProdutos.Image = global::UI.Properties.Resources._1394159835_search;
            this.btnPesquisarProdutos.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnPesquisarProdutos.Location = new System.Drawing.Point(863, 429);
            this.btnPesquisarProdutos.Name = "btnPesquisarProdutos";
            this.btnPesquisarProdutos.Size = new System.Drawing.Size(150, 44);
            this.btnPesquisarProdutos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPesquisarProdutos.TabIndex = 3;
            this.btnPesquisarProdutos.Text = "Buscar Produtos - F1  ";
            this.btnPesquisarProdutos.Click += new System.EventHandler(this.btnPesquisarProdutos_Click);
            // 
            // btnVisualizarPedido
            // 
            this.btnVisualizarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnVisualizarPedido.BackColor = System.Drawing.Color.LimeGreen;
            this.btnVisualizarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVisualizarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnVisualizarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVisualizarPedido.Image = global::UI.Properties.Resources.computer;
            this.btnVisualizarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnVisualizarPedido.Location = new System.Drawing.Point(1018, 479);
            this.btnVisualizarPedido.Name = "btnVisualizarPedido";
            this.btnVisualizarPedido.Size = new System.Drawing.Size(150, 44);
            this.btnVisualizarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnVisualizarPedido.TabIndex = 7;
            this.btnVisualizarPedido.Text = "Ver Pedidos - F6";
            this.btnVisualizarPedido.Click += new System.EventHandler(this.btnVisualizarPedido_Click);
            // 
            // btnAddQtd
            // 
            this.btnAddQtd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddQtd.BackColor = System.Drawing.Color.LimeGreen;
            this.btnAddQtd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddQtd.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddQtd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddQtd.Image = global::UI.Properties.Resources.addQtd_fw;
            this.btnAddQtd.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnAddQtd.Location = new System.Drawing.Point(1018, 429);
            this.btnAddQtd.Name = "btnAddQtd";
            this.btnAddQtd.Size = new System.Drawing.Size(150, 44);
            this.btnAddQtd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddQtd.TabIndex = 4;
            this.btnAddQtd.Text = "  Quantidade - F2 ";
            this.btnAddQtd.Click += new System.EventHandler(this.btnAddQtd_Click);
            // 
            // btnCancelarItem
            // 
            this.btnCancelarItem.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelarItem.BackColor = System.Drawing.Color.LimeGreen;
            this.btnCancelarItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarItem.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnCancelarItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarItem.Image = global::UI.Properties.Resources.excluirItem_fw;
            this.btnCancelarItem.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCancelarItem.Location = new System.Drawing.Point(1173, 429);
            this.btnCancelarItem.Name = "btnCancelarItem";
            this.btnCancelarItem.Size = new System.Drawing.Size(157, 44);
            this.btnCancelarItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancelarItem.TabIndex = 5;
            this.btnCancelarItem.Text = "Cancelar Item - F3";
            this.btnCancelarItem.Click += new System.EventHandler(this.btnCancelarItem_Click);
            // 
            // btnCancelarPedido
            // 
            this.btnCancelarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelarPedido.BackColor = System.Drawing.Color.LimeGreen;
            this.btnCancelarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnCancelarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarPedido.Image = global::UI.Properties.Resources.fecharPed1;
            this.btnCancelarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCancelarPedido.Location = new System.Drawing.Point(863, 479);
            this.btnCancelarPedido.Name = "btnCancelarPedido";
            this.btnCancelarPedido.Size = new System.Drawing.Size(150, 44);
            this.btnCancelarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancelarPedido.TabIndex = 6;
            this.btnCancelarPedido.Text = "Cancelar Pedido - F4";
            this.btnCancelarPedido.Click += new System.EventHandler(this.btnCancelarPedido_Click);
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.panel17);
            this.panel7.Controls.Add(this.panel13);
            this.panel7.Controls.Add(this.panel12);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Location = new System.Drawing.Point(538, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(319, 571);
            this.panel7.TabIndex = 0;
            // 
            // panel17
            // 
            this.panel17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel17.BackColor = System.Drawing.Color.LimeGreen;
            this.panel17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel17.Controls.Add(this.txtTotalGeral);
            this.panel17.Controls.Add(this.label13);
            this.panel17.Location = new System.Drawing.Point(5, 478);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(311, 90);
            this.panel17.TabIndex = 14;
            // 
            // txtTotalGeral
            // 
            this.txtTotalGeral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalGeral.BackColor = System.Drawing.Color.White;
            this.txtTotalGeral.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalGeral.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalGeral.Font = new System.Drawing.Font("Verdana", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalGeral.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalGeral.Location = new System.Drawing.Point(13, 32);
            this.txtTotalGeral.Name = "txtTotalGeral";
            this.txtTotalGeral.ReadOnly = true;
            this.txtTotalGeral.Size = new System.Drawing.Size(286, 52);
            this.txtTotalGeral.TabIndex = 0;
            this.txtTotalGeral.Text = "0,00";
            this.txtTotalGeral.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(13, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 16);
            this.label13.TabIndex = 1;
            this.label13.Text = "Total do Pedido";
            // 
            // panel13
            // 
            this.panel13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel13.BackColor = System.Drawing.Color.LimeGreen;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel13.Controls.Add(this.txtItens);
            this.panel13.Controls.Add(this.label7);
            this.panel13.Location = new System.Drawing.Point(5, 383);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(311, 90);
            this.panel13.TabIndex = 13;
            // 
            // txtItens
            // 
            this.txtItens.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItens.BackColor = System.Drawing.Color.White;
            this.txtItens.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItens.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtItens.Font = new System.Drawing.Font("Verdana", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItens.ForeColor = System.Drawing.Color.Black;
            this.txtItens.Location = new System.Drawing.Point(13, 28);
            this.txtItens.Name = "txtItens";
            this.txtItens.ReadOnly = true;
            this.txtItens.Size = new System.Drawing.Size(286, 53);
            this.txtItens.TabIndex = 0;
            this.txtItens.Text = "000";
            this.txtItens.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(13, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "Itens";
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel12.BackColor = System.Drawing.Color.LimeGreen;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.label6);
            this.panel12.Controls.Add(this.txtValorTotal);
            this.panel12.Location = new System.Drawing.Point(5, 288);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(311, 90);
            this.panel12.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(14, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "Valor Total";
            // 
            // txtValorTotal
            // 
            this.txtValorTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorTotal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtValorTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtValorTotal.Font = new System.Drawing.Font("Verdana", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorTotal.Location = new System.Drawing.Point(14, 24);
            this.txtValorTotal.Name = "txtValorTotal";
            this.txtValorTotal.ReadOnly = true;
            this.txtValorTotal.Size = new System.Drawing.Size(284, 60);
            this.txtValorTotal.TabIndex = 0;
            this.txtValorTotal.Text = "0,00";
            this.txtValorTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.BackColor = System.Drawing.Color.LimeGreen;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.label5);
            this.panel11.Controls.Add(this.txtValorUnitario);
            this.panel11.Location = new System.Drawing.Point(5, 193);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(311, 90);
            this.panel11.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(14, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "Valor Unitário";
            // 
            // txtValorUnitario
            // 
            this.txtValorUnitario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorUnitario.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtValorUnitario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtValorUnitario.Font = new System.Drawing.Font("Verdana", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorUnitario.Location = new System.Drawing.Point(14, 24);
            this.txtValorUnitario.Name = "txtValorUnitario";
            this.txtValorUnitario.ReadOnly = true;
            this.txtValorUnitario.Size = new System.Drawing.Size(284, 60);
            this.txtValorUnitario.TabIndex = 0;
            this.txtValorUnitario.Text = "0,00";
            this.txtValorUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.BackColor = System.Drawing.Color.LimeGreen;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.txtQuantidade);
            this.panel10.Location = new System.Drawing.Point(5, 98);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(311, 90);
            this.panel10.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(14, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Quantidade";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuantidade.BackColor = System.Drawing.SystemColors.Window;
            this.txtQuantidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtQuantidade.Font = new System.Drawing.Font("Verdana", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantidade.Location = new System.Drawing.Point(14, 24);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(284, 60);
            this.txtQuantidade.TabIndex = 1;
            this.txtQuantidade.Text = "1";
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQuantidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuantidade_KeyDown);
            this.txtQuantidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantidade_KeyPress);
            this.txtQuantidade.Leave += new System.EventHandler(this.txtQuantidade_Leave);
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.Color.LimeGreen;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.txtCodigo);
            this.panel9.Location = new System.Drawing.Point(5, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(311, 90);
            this.panel9.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(12, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Código";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigo.Font = new System.Drawing.Font("Verdana", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.ForeColor = System.Drawing.Color.Green;
            this.txtCodigo.Location = new System.Drawing.Point(14, 24);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(284, 60);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // PdvDataHora
            // 
            this.PdvDataHora.Enabled = true;
            this.PdvDataHora.Interval = 1000;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Green;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(20, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1237, 38);
            this.panel2.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.AutoSize = true;
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.pictureBox2);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.lblOperador);
            this.panel6.Location = new System.Drawing.Point(922, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(310, 33);
            this.panel6.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::UI.Properties.Resources._7834_64x64;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(3, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(39, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operador:";
            // 
            // lblOperador
            // 
            this.lblOperador.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOperador.AutoSize = true;
            this.lblOperador.BackColor = System.Drawing.Color.Transparent;
            this.lblOperador.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperador.ForeColor = System.Drawing.Color.Red;
            this.lblOperador.Location = new System.Drawing.Point(155, 4);
            this.lblOperador.Name = "lblOperador";
            this.lblOperador.Size = new System.Drawing.Size(0, 23);
            this.lblOperador.TabIndex = 6;
            this.lblOperador.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(15, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "SisLanche Delivery";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.LightGreen;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.dgcDadosCliente);
            this.panel1.Controls.Add(this.gbPontoReferencia);
            this.panel1.Controls.Add(this.gbDadosCliente);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtTelefone);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1333, 116);
            this.panel1.TabIndex = 0;
            // 
            // dgcDadosCliente
            // 
            this.dgcDadosCliente.AllowUserToAddRows = false;
            this.dgcDadosCliente.AllowUserToDeleteRows = false;
            this.dgcDadosCliente.AllowUserToOrderColumns = true;
            this.dgcDadosCliente.AllowUserToResizeColumns = false;
            this.dgcDadosCliente.AllowUserToResizeRows = false;
            this.dgcDadosCliente.BackgroundColor = System.Drawing.Color.White;
            this.dgcDadosCliente.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgcDadosCliente.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dgcDadosCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgcDadosCliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9});
            this.dgcDadosCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgcDadosCliente.DefaultCellStyle = dataGridViewCellStyle28;
            this.dgcDadosCliente.EnableHeadersVisualStyles = false;
            this.dgcDadosCliente.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.dgcDadosCliente.Location = new System.Drawing.Point(449, 4);
            this.dgcDadosCliente.MultiSelect = false;
            this.dgcDadosCliente.Name = "dgcDadosCliente";
            this.dgcDadosCliente.ReadOnly = true;
            this.dgcDadosCliente.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgcDadosCliente.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dgcDadosCliente.RowHeadersVisible = false;
            this.dgcDadosCliente.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgcDadosCliente.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Yellow;
            this.dgcDadosCliente.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgcDadosCliente.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Yellow;
            this.dgcDadosCliente.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgcDadosCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgcDadosCliente.ShowCellErrors = false;
            this.dgcDadosCliente.ShowEditingIcon = false;
            this.dgcDadosCliente.ShowRowErrors = false;
            this.dgcDadosCliente.Size = new System.Drawing.Size(881, 84);
            this.dgcDadosCliente.TabIndex = 13;
            this.dgcDadosCliente.UseCustomBackgroundColor = true;
            this.dgcDadosCliente.Visible = false;
            this.dgcDadosCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgcDadosCliente_KeyDown);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "nomeCliente";
            this.Column6.HeaderText = "Nome do Cliente";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 300;
            // 
            // Column7
            // 
            this.Column7.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.Column7.BackgroundStyle.Class = "DataGridViewBorder";
            this.Column7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Column7.Culture = new System.Globalization.CultureInfo("pt-BR");
            this.Column7.DataPropertyName = "telefoneCliente";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle27;
            this.Column7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Column7.HeaderText = "Telefone 1";
            this.Column7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Column7.Mask = "(##) ####-####";
            this.Column7.Name = "Column7";
            this.Column7.PasswordChar = '\0';
            this.Column7.ReadOnly = true;
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Column7.Text = "(  )     -";
            this.Column7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Column7.Width = 180;
            // 
            // Column8
            // 
            this.Column8.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.Column8.BackgroundStyle.Class = "DataGridViewBorder";
            this.Column8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Column8.Culture = new System.Globalization.CultureInfo("pt-BR");
            this.Column8.DataPropertyName = "celularCliente";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle18;
            this.Column8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Column8.HeaderText = "Telefone 2";
            this.Column8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Column8.Mask = "(##) ####-####";
            this.Column8.Name = "Column8";
            this.Column8.PasswordChar = '\0';
            this.Column8.ReadOnly = true;
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Column8.Text = "(  )     -";
            this.Column8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Column8.Width = 180;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "bairroCliente";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column9.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column9.HeaderText = "Bairro";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 200;
            // 
            // gbPontoReferencia
            // 
            this.gbPontoReferencia.Controls.Add(this.lblPontoReferencia);
            this.gbPontoReferencia.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPontoReferencia.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.gbPontoReferencia.Location = new System.Drawing.Point(982, 3);
            this.gbPontoReferencia.Name = "gbPontoReferencia";
            this.gbPontoReferencia.Size = new System.Drawing.Size(348, 110);
            this.gbPontoReferencia.TabIndex = 12;
            this.gbPontoReferencia.TabStop = false;
            this.gbPontoReferencia.Text = "Ponto de Referência";
            // 
            // lblPontoReferencia
            // 
            this.lblPontoReferencia.ForeColor = System.Drawing.Color.Navy;
            this.lblPontoReferencia.Location = new System.Drawing.Point(9, 31);
            this.lblPontoReferencia.Name = "lblPontoReferencia";
            this.lblPontoReferencia.Size = new System.Drawing.Size(325, 76);
            this.lblPontoReferencia.TabIndex = 0;
            // 
            // gbDadosCliente
            // 
            this.gbDadosCliente.Controls.Add(this.lblEnderecoCliente);
            this.gbDadosCliente.Controls.Add(this.lblNomeCliente);
            this.gbDadosCliente.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDadosCliente.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.gbDadosCliente.Location = new System.Drawing.Point(451, 3);
            this.gbDadosCliente.Name = "gbDadosCliente";
            this.gbDadosCliente.Size = new System.Drawing.Size(525, 110);
            this.gbDadosCliente.TabIndex = 11;
            this.gbDadosCliente.TabStop = false;
            this.gbDadosCliente.Text = "Dados do Cliente";
            // 
            // lblEnderecoCliente
            // 
            this.lblEnderecoCliente.ForeColor = System.Drawing.Color.Black;
            this.lblEnderecoCliente.Location = new System.Drawing.Point(6, 55);
            this.lblEnderecoCliente.Name = "lblEnderecoCliente";
            this.lblEnderecoCliente.Size = new System.Drawing.Size(506, 52);
            this.lblEnderecoCliente.TabIndex = 1;
            // 
            // lblNomeCliente
            // 
            this.lblNomeCliente.ForeColor = System.Drawing.Color.Red;
            this.lblNomeCliente.Location = new System.Drawing.Point(7, 31);
            this.lblNomeCliente.Name = "lblNomeCliente";
            this.lblNomeCliente.Size = new System.Drawing.Size(505, 24);
            this.lblNomeCliente.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DimGray;
            this.label14.Location = new System.Drawing.Point(121, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(324, 24);
            this.label14.TabIndex = 10;
            this.label14.Text = "F5 - Telefone | F11 - Menu";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTelefone
            // 
            this.txtTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtTelefone.Location = new System.Drawing.Point(124, 4);
            this.txtTelefone.MaxLength = 9;
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(321, 83);
            this.txtTelefone.TabIndex = 0;
            this.txtTelefone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTelefone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTelefone_KeyDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::UI.Properties.Resources.phone;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 105);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // cmsMenuPdv
            // 
            this.cmsMenuPdv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.retiradasToolStripMenuItem,
            this.cadastrarClienteToolStripMenuItem});
            this.cmsMenuPdv.Name = "cmsMenuPdv";
            this.cmsMenuPdv.Size = new System.Drawing.Size(165, 48);
            this.cmsMenuPdv.Text = "Menu PDV";
            // 
            // retiradasToolStripMenuItem
            // 
            this.retiradasToolStripMenuItem.Name = "retiradasToolStripMenuItem";
            this.retiradasToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.retiradasToolStripMenuItem.Text = "Retiradas";
            this.retiradasToolStripMenuItem.Click += new System.EventHandler(this.retiradasToolStripMenuItem_Click);
            // 
            // cadastrarClienteToolStripMenuItem
            // 
            this.cadastrarClienteToolStripMenuItem.Name = "cadastrarClienteToolStripMenuItem";
            this.cadastrarClienteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cadastrarClienteToolStripMenuItem.Text = "Cadastrar Cliente";
            this.cadastrarClienteToolStripMenuItem.Click += new System.EventHandler(this.cadastrarClienteToolStripMenuItem_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.BackgroundImage")));
            this.btnMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Location = new System.Drawing.Point(1263, 13);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(42, 39);
            this.btnMinimizar.TabIndex = 6;
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFechar.BackgroundImage")));
            this.btnFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Location = new System.Drawing.Point(1311, 13);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(42, 39);
            this.btnFechar.TabIndex = 4;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // frmDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ContextMenuStrip = this.cmsMenuPdv;
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmDelivery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRTEC - Café Regional Pupunha | Ponto de Venda";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmDelivery_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmDelivery_KeyUp);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbTroco.ResumeLayout(false);
            this.gbTroco.PerformLayout();
            this.panelEx2.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCupom)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgcDadosCliente)).EndInit();
            this.gbPontoReferencia.ResumeLayout(false);
            this.gbDadosCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.cmsMenuPdv.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtValorTotal;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtValorUnitario;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTotalGeral;
        private System.Windows.Forms.TextBox txtItens;
        private System.Windows.Forms.Timer PdvDataHora;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label lblCodVenda;
        private System.Windows.Forms.Label lblNomeEmpresa;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblOperador;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblAtendente;
        public System.Windows.Forms.DataGridView dgCupom;
        private DevComponents.DotNetBar.ButtonX btnExtra;
        private DevComponents.DotNetBar.ButtonX btnFinalizarPedido;
        private DevComponents.DotNetBar.ButtonX btnEspecial;
        private DevComponents.DotNetBar.ButtonX btnVisualizarPedido;
        private DevComponents.DotNetBar.ButtonX btnCancelarPedido;
        private DevComponents.DotNetBar.ButtonX btnCancelarItem;
        private DevComponents.DotNetBar.ButtonX btnAddQtd;
        private DevComponents.DotNetBar.ButtonX btnPesquisarProdutos;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.ButtonX btnImprimir;
        private System.Windows.Forms.ContextMenuStrip cmsMenuPdv;
        private System.Windows.Forms.ToolStripMenuItem retiradasToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbTipoPagamento;
        private System.Windows.Forms.GroupBox gbTroco;
        private System.Windows.Forms.TextBox txtValorTroco;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTrocoPara;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox ckbTroco;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtValorFrete;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtTotalGeralPedido;
        private System.Windows.Forms.ToolStripMenuItem cadastrarClienteToolStripMenuItem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox gbDadosCliente;
        private System.Windows.Forms.Label lblNomeCliente;
        private System.Windows.Forms.Label lblEnderecoCliente;
        private System.Windows.Forms.GroupBox gbPontoReferencia;
        private System.Windows.Forms.Label lblPontoReferencia;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgcDadosCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn Column7;
        private DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn extras;
        private System.Windows.Forms.DataGridViewTextBoxColumn especiais;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataEnvioVendaDetalhes;

    }
}