﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmPdvExtra : Form
    {
        public int itemExtra;
        public decimal qtdExtra;
        public int codigoProduto;
        
        public frmPdvExtra(int item)
        {
            InitializeComponent();
            txtItem.Text = item.ToString();
            txtCodigo.Focus();
            txtQuantidade.Text = "1";            
        }

        public int codDel;

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtCodigo.Text == "" || txtCodigo.Text == "0")
                    {
                        MessageBox.Show("O Código é Inválido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        txtCodigo.Focus();
                    }
                    else
                    {
                        itemExtra = Convert.ToInt32(txtItem.Text);
                        qtdExtra = Convert.ToDecimal(txtQuantidade.Text);
                        codigoProduto = Convert.ToInt32(txtCodigo.Text);

                        Close();
                    }
                    break;
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {

                e.Handled = true;

            }
        }

        private void txtQtd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtQuantidade.Text != "")
                    {
                        txtCodigo.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {

                e.Handled = true;

            }
        }

        private void txtQtd_Leave(object sender, EventArgs e)
        {
            if (txtQuantidade.Text == "" || Convert.ToDecimal(txtQuantidade.Text) == 0)
            {
                MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtQuantidade.Focus();
            }
        }

        private void txtItem_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtItem.Text != "")
                    {
                        txtCodigo.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtItem_Leave(object sender, EventArgs e)
        {
            if (txtItem.Text == "" || Convert.ToDecimal(txtItem.Text) == 0)
            {
                MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtItem.Focus();
            }
        }

        private void frmPdvExtra_KeyDown(object sender, KeyEventArgs e)
        
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    txtCodigo.Focus();
                    break;
                case Keys.F2:
                    txtQuantidade.Text = "1";
                    txtQuantidade.Focus();
                    break;
                case Keys.F3:
                    txtItem.Focus();
                    break;
                case Keys.F5:
                    btnPesqProduto.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void frmPdvExtra_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(txtCodigo.Text == "" || txtCodigo.Text == "0")
            {
                itemExtra = 0;
            }
        }

        private void btnPesqProduto_Click(object sender, EventArgs e)
        {
            pesqProduto(txtCodigo);
        }

        public void pesqProduto(TextBox txt)
        {
            frmPesqProdutosFront objPesqProduto = new frmPesqProdutosFront(1);
            DialogResult resultado = objPesqProduto.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                txt.Text = objPesqProduto.codigoProduto.ToString();
                txt.Focus();
            }
            else
            {
                txt.Text = null;
            }
        }
        
    }
}
