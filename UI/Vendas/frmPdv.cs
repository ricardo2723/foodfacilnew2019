﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

using Negocios;
using ObjetoTransferencia;
using Relatorios;


namespace UI
{
    public partial class frmPdv : Form
    {
        VendaCabecalho vendaCabecalho = new VendaCabecalho();
        VendaDetalhes vendaDetalhes = new VendaDetalhes();
        VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();

        ProdutosNegocios produtosNegocios = new ProdutosNegocios();

        public Button b;

        public frmPdv()
        {
            InitializeComponent();
            lblOperador.Text = frmLogin.usuariosLogin.usuarioLogin;
        }

        public Button but = new Button();
        public string numeroVenda;
        public string nomeAtendente;
        public string tipoAtendimento;
        public bool pararcronometro;

        public string extraDescricao;
        public decimal extraValor;
        public string especialDescricao;
        public int counter;

        private void frmPdv_Load(object sender, EventArgs e)
        {
            lblTitulo.Text = "SisLanche PDV - " + frmLogin.meuLanche.nomeMeuLanche;
            lblNomeEmpresa.Text = frmLogin.meuLanche.nomeMeuLanche;
            txtCodigo.Focus();
            checaAtendimento();
        }

        private void frmPdv_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnPesquisarProdutos.PerformClick();
                    break;
                case Keys.F2:
                    btnAddQtd.PerformClick();
                    break;
                case Keys.F3:
                    btnCancelarItem.PerformClick();
                    break;
                case Keys.F4:
                    btnCancelarPedido.PerformClick();
                    break;
                case Keys.F5:
                    btnEnviarPedido.PerformClick();
                    break;
                case Keys.F6:
                    btnVisualizarPedido.PerformClick();
                    break;
                case Keys.F7:
                    btnExtra.PerformClick();
                    break;
                case Keys.F8:
                    btnEspecial.PerformClick();
                    break;
                case Keys.F9:
                    btnImprimir.PerformClick();
                    break;
                case Keys.F10:
                    btnFinalizarPedido.PerformClick();
                    break;                      
            }
        }

        private void PdvDataHora_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = DateTime.Now.ToShortDateString() + " ÀS " + DateTime.Now.ToLongTimeString();
        }

        private void PdvPrintPed_Tick(object sender, EventArgs e)
        {
            counter = counter + 1;

            if (counter >= 30)
            {
                PdvPrintPed.Enabled = false;

                bgwImprimiPedido.RunWorkerAsync();
            }
        }

        #region BOTÕES MINI E FECHAR

        private void btnFechar_Click(object sender, EventArgs e)
        {
            bgwImprimiPedido.CancelAsync();
            Close();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        #endregion

        #region TXT_KEY AND LEAVE

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtCodigo.Text != "")
                    {
                        PedidoItemAdd();
                    }
                    else
                    {
                        MessageBox.Show("Digite o Código do Produto", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtQuantidade_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtQuantidade.Text != "")
                    {
                        txtQuantidade.ReadOnly = true;
                        txtQuantidade.BackColor = System.Drawing.SystemColors.Window;
                        txtCodigo.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {

                e.Handled = true;

            }
        }

        private void txtQuantidade_Leave(object sender, EventArgs e)
        {
            if (txtQuantidade.Text == "" || Convert.ToDecimal(txtQuantidade.Text) == 0)
            {
                MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtQuantidade.Focus();
            }
        }

        #endregion

        #region ATENDIMENTO MESAS

        private void btnPedM1_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 01", "M1", btnPedM1);
        }

        private void btnPedM2_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 02", "M2", btnPedM2);
        }

        private void btnPedM3_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 03", "M3", btnPedM3);
        }

        private void btnPedM4_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 04", "M4", btnPedM4);
        }

        private void btnPedM5_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 05", "M5", btnPedM5);
        }

        private void btnPedM6_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 06", "M6", btnPedM6);
        }

        private void btnPedM7_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 07", "M7", btnPedM7);
        }

        private void btnPedM8_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 08", "M8", btnPedM8);
        }

        private void btnPedM9_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 09", "M9", btnPedM9);
        }

        private void btnPedM10_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 10", "M10", btnPedM10);
        }

        private void btnPedM11_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 11", "M11", btnPedM11);
        }

        private void btnPedM12_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 12", "M12", btnPedM12);
        }

        private void btnPedM13_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 13", "M13", btnPedM13);
        }

        private void btnPedM14_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 14", "M14", btnPedM14);
        }

        private void btnPedM15_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 15", "M15", btnPedM15);
        }

        private void btnPedM16_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 16", "M16", btnPedM16);
        }

        private void btnPedM17_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 17", "M17", btnPedM17);
        }

        private void btnPedM18_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 18", "M18", btnPedM18);
        }

        private void btnPedM19_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 19", "M19", btnPedM19);
        }

        private void btnPedM20_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 20", "M20", btnPedM20);
        }

        private void btnPedM21_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 21", "M21", btnPedM21);
        }

        private void btnPedM22_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 22", "M22", btnPedM22);
        }

        private void btnPedM23_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 23", "M23", btnPedM23);
        }

        private void btnPedM24_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 24", "M24", btnPedM24);
        }

        private void btnPedM25_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 25", "M25", btnPedM25);
        }

        private void btnPedM26_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 26", "M26", btnPedM26);
        }

        private void btnPedM27_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 27", "M27", btnPedM27);
        }

        private void btnPedM28_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 28", "M28", btnPedM28);
        }

        private void btnPedM29_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 29", "M29", btnPedM29);
        }

        private void btnPedM30_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 30", "M30", btnPedM30);
        }

        private void btnPedM31_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 31", "M31", btnPedM31);
        }

        private void btnPedM32_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 32", "M32", btnPedM32);
        }

        private void btnPedM33_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 33", "M33", btnPedM33);
        }

        private void btnPedM34_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 34", "M34", btnPedM34);
        }

        private void btnPedM35_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 35", "M35", btnPedM35);
        }

        private void btnPedM36_Click(object sender, EventArgs e)
        {
            carregaAtendimento("MESA 36", "M36", btnPedM36);
        }

        #endregion

        #region ATENDIMENTO BALCÃO

        private void btnPedB1_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 01", "B1", btnPedB1);
        }

        private void btnPedB2_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 02", "B2", btnPedB2);
        }

        private void btnPedB3_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 03", "B3", btnPedB3);
        }

        private void btnPedB4_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 04", "B4", btnPedB4);
        }

        private void btnPedB5_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 05", "B5", btnPedB5);
        }

        private void btnPedB6_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 06", "B6", btnPedB6);
        }

        private void btnPedB7_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 07", "B7", btnPedB7);
        }

        private void btnPedB8_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 08", "B8", btnPedB8);
        }

        private void btnPedB9_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 09", "B9", btnPedB9);
        }

        private void btnPedB10_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 10", "B10", btnPedB10);
        }

        private void btnPedB11_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 11", "B11", btnPedB11);
        }

        private void btnPedB12_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 12", "B12", btnPedB12);
        }

        private void btnPedB13_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 13", "B13", btnPedB13);
        }

        private void btnPedB14_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 14", "B14", btnPedB14);
        }

        private void btnPedB15_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 15", "B15", btnPedB15);
        }

        private void btnPedB16_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 16", "B16", btnPedB16);
        }

        private void btnPedB17_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 17", "B17", btnPedB17);
        }

        private void btnPedB18_Click(object sender, EventArgs e)
        {
            carregaAtendimento("BALCÃO 18", "B18", btnPedB18);
        }

        #endregion

        #region ATENDIMENTO CARRO

        private void btnPedC1_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 01", "C1", btnPedC1);
        }

        private void btnPedC2_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 02", "C2", btnPedC2);
        }

        private void btnPedC3_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 03", "C3", btnPedC3);
        }

        private void btnPedC4_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 04", "C4", btnPedC4);
        }

        private void btnPedC5_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 05", "C5", btnPedC5);
        }

        private void btnPedC6_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 06", "C6", btnPedC6);
        }

        private void btnPedC7_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 07", "C7", btnPedC7);
        }

        private void btnPedC8_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 08", "C8", btnPedC8);
        }

        private void btnPedC9_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 09", "C9", btnPedC9);
        }

        private void btnPedC10_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 10", "C10", btnPedC10);
        }

        private void btnPedC11_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 11", "C11", btnPedC11);
        }

        private void btnPedC12_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 12", "C12", btnPedC12);
        }

        private void btnPedC13_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 13", "C13", btnPedC13);
        }

        private void btnPedC14_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 14", "C14", btnPedC14);
        }

        private void btnPedC15_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 15", "C15", btnPedC15);
        }

        private void btnPedC16_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 16", "C16", btnPedC16);
        }

        private void btnPedC17_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 17", "C17", btnPedC17);
        }

        private void btnPedC18_Click(object sender, EventArgs e)
        {
            carregaAtendimento("CARR0 18", "C18", btnPedC18);
        }

        #endregion

        #region METODOS

        private void checarPedidos(Button buton, string atendimento)
        {
            VendaCabecalho vendaCabecalho = new VendaCabecalho();
            vendaCabecalho.nomeAtendimento = atendimento;
            vendaCabecalho.usuarioId = frmLogin.usuariosLogin.usuarioId;
            vendaCabecalho.statusVendaCabecalho = 1;

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            numeroVenda = vendaCabecalhoNegocios.checaAbreVendas(vendaCabecalho);

            try
            {
                Convert.ToInt32(numeroVenda);

                if (Convert.ToInt32(numeroVenda) > 0)
                {
                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);

                    //Busca todos os itens referente a essa venda na tabela VendaDetalhes
                    CarregarDataGrid();
                    buton.BackColor = System.Drawing.Color.Red;
                    txtItens.Text = dgCupom.Rows.Count.ToString();
                    txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
                    lblCodVenda.Text = "Código Venda: " + Generic.formatCodigo(numeroVenda);
                    lblAtendente.Text = "Atendente: " + nomeAtendente;
                }
            }
            catch (Exception)
            {
                abrePedidos(buton, atendimento);
            }
        }

        private void abrePedidos(Button buton, string atendimento)
        {
            DialogResult dr = MessageBox.Show("Não existe pedido para esse Atendimento. Deseja abrir um novo pedido para este Atendimento?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                frmNomeAtendente objAtendente = new frmNomeAtendente();
                DialogResult atend = objAtendente.ShowDialog();


                if (atend == DialogResult.Yes)
                {
                    nomeAtendente = objAtendente.nomeAtendente;

                    VendaCabecalho vendaCabecalho = new VendaCabecalho();
                    vendaCabecalho.nomeAtendimento = atendimento;
                    vendaCabecalho.usuarioId = frmLogin.usuariosLogin.usuarioId;
                    vendaCabecalho.atendenteVendaCabecalho = objAtendente.nomeAtendente;
                    nomeAtendente = vendaCabecalho.atendenteVendaCabecalho;
                    vendaCabecalho.statusVendaCabecalho = 1;

                    VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                    numeroVenda = vendaCabecalhoNegocios.abreVendas(vendaCabecalho);

                    try
                    {
                        int ckVenda = Convert.ToInt32(numeroVenda);

                        if (Convert.ToInt32(numeroVenda) > 0)
                        {
                            dgCupom.Rows.Clear();
                            buton.BackColor = System.Drawing.Color.Red;
                            txtItens.Text = dgCupom.Rows.Count.ToString();
                            txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
                            lblCodVenda.Text = "Código Venda: " + Generic.formatCodigo(numeroVenda);
                            lblAtendente.Text = "Atendente: " + nomeAtendente;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Impossivel abrir Pedido: " + numeroVenda);
                    }
                }
                else
                {
                    MessageBox.Show("Para Abertura de um PEDIDO e Obrigatório o nome de um ATENDENTE", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    numeroVenda = "0";
                    return;
                }
            }
        }

        public void PedidoItemAdd()
        {
            if (Convert.ToInt32(numeroVenda) != 0)
            {
                int i, z;

                Produtos produtos = new Produtos();
                produtos.codigoProduto = Convert.ToInt32(txtCodigo.Text);

                VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                ProdutosCollections produtosCollections = vendaCabecalhoNegocios.GetProdutoVenda('P', produtos.codigoProduto);

                if (produtosCollections.Count > 0)
                {
                    foreach (var item in produtosCollections)
                    {
                        txtValorUnitario.Text = item.valorVendaProduto.ToString();
                        lblDescricao.Text = item.descricaoProduto;
                        vendaDetalhes.codigoProduto = item.codigoProduto;
                        vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                        vendaDetalhes.estoqueProduto = item.estoqueProduto;
                        vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                        vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                    }

                    //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                    if (vendaDetalhes.controlaEstoqueProduto == 'S')
                    {
                        //VERIFICA SE EXISTE O PRODUTO EM ESTOQUE PARA A VENDA E SE E MAIOR QUE O PEDIDO
                        if (vendaDetalhes.estoqueProduto < Convert.ToDecimal(txtQuantidade.Text))
                        {
                            MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            txtCodigo.Text = "";
                            return;
                        }
                        else
                        {
                            //SE EXISTIR ESTOQUE SUFICIENTE VERIFICA SE O ESTOQUE ESTA CRITICO
                            if ((vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQuantidade.Text)) <= vendaDetalhes.estoqueCriticoProduto)
                            {
                                MessageBox.Show("O estoque está abaixo do Permitido. Providenciar o Produto. Estoque: " + Convert.ToInt32(vendaDetalhes.estoqueProduto) + " Unidades", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                            if (vendaDetalhes.manufaturadoProduto == 'N')
                            {
                                try
                                {
                                    vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQuantidade.Text);

                                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                                    int checaretorno = Convert.ToInt32(retorno);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }

                            }
                            else if (vendaDetalhes.manufaturadoProduto == 'S')
                            {
                                try
                                {
                                    //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                                    vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQuantidade.Text);

                                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                                    int checaretorno = Convert.ToInt32(retorno);

                                    //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                                    produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                                    VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                                    foreach (var item in produtosCollections)
                                    {
                                        vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                                        vendaDetalhesManufaturado.estoqueProduto = vendaDetalhes.estoqueProduto;

                                        retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                                        checaretorno = Convert.ToInt32(retorno);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }
                            }
                        }
                    }
                    else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                    {
                        if (vendaDetalhes.manufaturadoProduto == 'S')
                        {
                            try
                            {
                                //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                                produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                                VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                                foreach (var item in produtosCollections)
                                {
                                    decimal qtd1 = item.quantidadeProdutoManufaturado;
                                    decimal totalEst = qtd1 * Convert.ToDecimal(txtQuantidade.Text);

                                    if (item.estoqueProduto < totalEst)
                                    {
                                        MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                }

                                foreach (var item in produtosCollections)
                                {
                                    vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                    vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                    vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                    if (vendaDetalhesManufaturado.estoqueProduto < Convert.ToDecimal(txtQuantidade.Text))
                                    {
                                        MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }

                                    vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto - (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * Convert.ToDecimal(txtQuantidade.Text));

                                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                                    Convert.ToInt32(retorno);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                        }
                    }
                    txtValorTotal.Text = Convert.ToString(Convert.ToDecimal(txtQuantidade.Text) * Convert.ToDecimal(txtValorUnitario.Text));

                    txtTotalGeral.Text = Convert.ToString(Convert.ToDecimal(txtTotalGeral.Text) + Convert.ToDecimal(txtValorTotal.Text));

                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(txtQuantidade.Text);
                    vendaDetalhes.valorUnitarioVendaDetalhes = Convert.ToDecimal(txtValorUnitario.Text);
                    vendaDetalhes.valorTotalVendaDetalhes = Convert.ToDecimal(txtValorTotal.Text);
                    vendaDetalhes.statusVendaDetalhes = 1;
                    vendaDetalhes.dataEnvioVendaDetalhes = DateTime.Now;

                    VendaDetalhesNegocios vendaDetalhesNegociosIns = new VendaDetalhesNegocios();
                    string retorno1 = vendaDetalhesNegociosIns.inserir(vendaDetalhes);

                    try
                    {
                        vendaDetalhes.idVendaDetalhes = Convert.ToInt32(retorno1);

                        i = dgCupom.Rows.Count;
                        z = i + 1;

                        dgCupom.Rows.Add(Generic.formatItens(z) + z, "    " + txtQuantidade.Text + " x", Generic.formatCodigo(txtCodigo.Text) + "        R$ " + (Convert.ToDecimal(txtValorUnitario.Text) + extraValor), lblDescricao.Text + extraDescricao, "R$ " + (Convert.ToDecimal(txtValorTotal.Text) + extraValor * Convert.ToDecimal(txtQuantidade.Text)), extraDescricao, especialDescricao, vendaDetalhes.statusVendaDetalhes, DateTime.Now, vendaDetalhes.idVendaDetalhes);

                        txtItens.Text = dgCupom.Rows.Count.ToString();

                        dgCupom.FirstDisplayedScrollingRowIndex = dgCupom.Rows.Count - 1;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno1, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }

                    extraValor = 0;
                    extraDescricao = "";
                    especialDescricao = "";

                    txtCodigo.Text = "";
                    txtQuantidade.Text = "1";
                    txtCodigo.Focus();

                }
                else
                {
                    MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    txtCodigo.Text = txtCodigo.Text;
                }
            }
            else
            {
                MessageBox.Show("E necessário escolher um tipo de Atendimento", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = txtCodigo.Text;
            }
        }

        private void PedidoExtraAdd()
        {
            string manufaturado = "N";

            if (dgCupom.Rows.Count > 0)
            {
                frmPdvExtra objExtra = new frmPdvExtra(dgCupom.RowCount);
                objExtra.ShowDialog();

                if (objExtra.itemExtra != 0)
                {
                    if (Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[7].Value) == 1 || Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[7].Value) == 2)
                    {
                        if (Convert.ToInt32(numeroVenda) != 0)
                        {
                            Produtos produtos = new Produtos();
                            produtos.codigoProduto = Convert.ToInt32(objExtra.codigoProduto);

                            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                            ProdutosCollections produtosCollections = vendaCabecalhoNegocios.GetProdutoVenda('P', produtos.codigoProduto);

                            if (produtosCollections.Count > 0)
                            {
                                foreach (var item in produtosCollections)
                                {
                                    vendaDetalhes.valorUnitarioVendaDetalhes = item.valorVendaProduto;
                                    vendaDetalhes.descricaoProduto = item.descricaoProduto;
                                    vendaDetalhes.codigoProduto = item.codigoProduto;
                                    vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                                    vendaDetalhes.estoqueProduto = item.estoqueProduto;
                                    vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                                    vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                                }

                                //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                                if (vendaDetalhes.controlaEstoqueProduto == 'S')
                                {
                                    //VERIFICA SE EXISTE O PRODUTO EM ESTOQUE PARA A VENDA E SE E MAIOR QUE O PEDIDO
                                    if (vendaDetalhes.estoqueProduto < Convert.ToDecimal(objExtra.qtdExtra))
                                    {
                                        MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        manufaturado = "S";
                                        return;
                                    }
                                    else
                                    {
                                        //SE EXISTIR ESTOQUE SUFICIENTE VERIFICA SE O ESTOQUE ESTA CRITICO
                                        if ((vendaDetalhes.estoqueProduto - Convert.ToDecimal(objExtra.qtdExtra)) <= vendaDetalhes.estoqueCriticoProduto)
                                        {
                                            MessageBox.Show("O estoque está abaixo do Permitido. Providenciar o Produto. Estoque: " + Convert.ToInt32(vendaDetalhes.estoqueProduto) + " Unidades", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }

                                        //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                                        if (vendaDetalhes.manufaturadoProduto == 'N')
                                        {
                                            try
                                            {
                                                vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(objExtra.qtdExtra);

                                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                manufaturado = "S";
                                                return;
                                            }

                                        }
                                        else if (vendaDetalhes.manufaturadoProduto == 'S')
                                        {
                                            MessageBox.Show("Não e possível adicionar Produtos Manufaturado a Extra", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            manufaturado = "S";
                                            return;
                                        }
                                    }
                                }
                                else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                                {
                                    if (vendaDetalhes.manufaturadoProduto == 'N')
                                    {
                                        try
                                        {
                                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                                            foreach (var item in produtosCollections)
                                            {
                                                decimal qtd1 = item.quantidadeProdutoManufaturado;
                                                decimal totalEst = qtd1 * Convert.ToDecimal(objExtra.qtdExtra);

                                                if (item.estoqueProduto < totalEst)
                                                {
                                                    MessageBox.Show("Estoque de" + item.descricaoProduto + " é insuficiente! Providencie Estoque: ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                    manufaturado = "S";
                                                    return;
                                                }
                                            }

                                            foreach (var item in produtosCollections)
                                            {
                                                vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                                vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                                if (vendaDetalhesManufaturado.estoqueProduto < Convert.ToDecimal(objExtra.qtdExtra))
                                                {
                                                    MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                    manufaturado = "S";
                                                    return;
                                                }

                                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto - (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * Convert.ToDecimal(objExtra.qtdExtra));

                                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            manufaturado = "S";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Não e possível adicionar Produtos Manufaturado a Extra", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        manufaturado = "S";
                                        return;
                                    }
                                }

                                vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                                vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(objExtra.qtdExtra);
                                vendaDetalhes.valorTotalVendaDetalhes = vendaDetalhes.valorUnitarioVendaDetalhes * vendaDetalhes.quantidadeVendaDetalhes;
                                vendaDetalhes.statusVendaDetalhes = 6;
                                vendaDetalhes.dataEnvioVendaDetalhes = DateTime.Now;

                                VendaDetalhesNegocios vendaDetalhesNegociosIns = new VendaDetalhesNegocios();
                                string retorno1 = vendaDetalhesNegociosIns.inserir(vendaDetalhes);

                                try
                                {
                                    vendaDetalhes.idVendaDetalhes = Convert.ToInt32(retorno1);

                                    vendaDetalhesNegociosIns.inserirItemExtra(Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[9].Value), vendaDetalhes.idVendaDetalhes, Convert.ToInt32(numeroVenda));
                                    //************************ATE AQUI DEU BAIXA NO ESTOQUE NO EXTRA E ADICIONOU O PEDIDO A LISTA ****************************
                                    //ATUALIZAR ITEM COM EXTRA
                                    string descricao = Convert.ToString(dgCupom.Rows[objExtra.itemExtra - 1].Cells[5].Value += vendaDetalhes.descricaoProduto + " = " + objExtra.qtdExtra + " / ");

                                    vendaDetalhesNegocios.AtualizarItemExtra(descricao, Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[9].Value));

                                    if (manufaturado == "S")
                                    {
                                        MessageBox.Show("Não foi possivel Adicionar o Extra.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                    else
                                    {
                                        //aqui esta o error tenho que acrescenta na mesma linha

                                        try
                                        {
                                            dgCupom.Rows.Clear();
                                            CarregarDataGrid();
                                            vendaDetalhes.extraVendaDetalhes = string.Empty;
                                            txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
                                        }
                                        catch (Exception)
                                        {
                                            MessageBox.Show("Não foi possivel Adicionar o Extra.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        }
                                    }

                                    MessageBox.Show("Extra inserido com Sucesso", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno1, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    manufaturado = "S";
                                    return;
                                }

                            }
                            else
                            {
                                MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                                txtCodigo.Text = txtCodigo.Text;
                            }
                        }
                        else
                        {
                            MessageBox.Show("E necessário escolher um tipo de Atendimento", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            txtCodigo.Text = txtCodigo.Text;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Não e possivel atribuir Extra para esse Produto. Pedido Entregue", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    return;
                }
            }

            else
            {
                MessageBox.Show("Não existem itens para atribuir a esse Extra", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
            //**************************************FIM**************************************
        }

        public void excluirItemUpEstoque(int codProd, decimal qtd, int idVenda)
        {
            //DEVOLVE ITEM EXCLUIDO AO ESTOQUE

            Produtos produtos = new Produtos();
            produtos.codigoProduto = Convert.ToInt32(codProd);

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            ProdutosCollections produtosCollections = vendaCabecalhoNegocios.GetProdutoVenda('P', produtos.codigoProduto);

            if (produtosCollections.Count > 0)
            {
                foreach (var item in produtosCollections)
                {
                    txtValorUnitario.Text = item.valorVendaProduto.ToString();
                    lblDescricao.Text = item.descricaoProduto;
                    vendaDetalhes.codigoProduto = item.codigoProduto;
                    vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                    vendaDetalhes.estoqueProduto = item.estoqueProduto;
                    vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                    vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                }

                //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                if (vendaDetalhes.controlaEstoqueProduto == 'S')
                {
                    //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                    if (vendaDetalhes.manufaturadoProduto == 'N')
                    {
                        try
                        {
                            vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                            Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                    }
                    else if (vendaDetalhes.manufaturadoProduto == 'S')
                    {
                        try
                        {
                            //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                            vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                            Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));

                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                            foreach (var item in produtosCollections)
                            {
                                vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                {
                    if (vendaDetalhes.manufaturadoProduto == 'S')
                    {
                        try
                        {
                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                            foreach (var item in produtosCollections)
                            {
                                decimal qtd1 = item.quantidadeProdutoManufaturado;
                                decimal totalEst = qtd1 * qtd;

                                if (item.estoqueProduto < totalEst)
                                {
                                    MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                            }

                            foreach (var item in produtosCollections)
                            {
                                vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                if (vendaDetalhesManufaturado.estoqueProduto < qtd)
                                {
                                    MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }

                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto + (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * qtd);

                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));

                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                //APOS VERIFICAR ESTOQUE E ATUALIZAR SE FOR O CASO, O PRODUTO E EXCLUIDO
                Convert.ToInt32(vendaDetalhesNegocios.ExcluirItensPedido(idVenda));

            }
            else
            {
                MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                txtCodigo.Text = txtCodigo.Text;
            }
        }

        private void PedidoItemExc()
        {
            if (dgCupom.Rows.Count > 0)
            {
                frmPdvDelete objDelete = new frmPdvDelete();
                objDelete.ShowDialog();

                if (objDelete.codDel != 0 && objDelete.codDel <= dgCupom.Rows.Count)
                {
                    dgCupom.Rows.Clear();
                    CarregarDataGrid();

                    if (Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[7].Value) == 6)
                    {
                        string descricao = "";
                        VendaDetalhesNegocios itemExt = new VendaDetalhesNegocios();

                        int idVendaExtra = Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[9].Value);

                        VendaDetalhesItemExtraCollections item1 = itemExt.buscarProdItensExtraAtualizar(idVendaExtra);

                        foreach (var itemA in item1)
                        {
                            VendaDetalhesCollections item2 = itemExt.buscarProdutoItensExtra(itemA.IdVendasDetalhesExtra);
                            //VERIFICA SE E O ULTIMO EXTRA A EXCLUIR E ENTAO LIMPA A DESCRIÇÃO DO EXTRA
                            if (item2.Count == 0)
                            {
                                descricao = "";
                                itemExt.AtualizarItemExtra(descricao, itemA.idVendasDetalhesItem);
                            }
                            else
                            {
                                foreach (var itemExtra in item2)
                                {
                                    descricao += itemExtra.descricaoProduto + " = " + itemExtra.quantidadeVendaDetalhes + " / ";
                                    itemExt.AtualizarItemExtra(descricao, itemA.idVendasDetalhesItem);
                                }
                            }
                        }
                    }
                    if (Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[7].Value) != 3 && Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[7].Value) != 4)
                    {
                        excluirItem(objDelete.codDel);
                    }
                    else
                    {
                        frmPermissaoAdm objPermissao = new frmPermissaoAdm();
                        DialogResult permissao = objPermissao.ShowDialog();

                        if (permissao == DialogResult.Yes)
                        {
                            excluirItem(objDelete.codDel);
                        }
                        else
                        {
                            MessageBox.Show("E necessário Autorização de um Administrador para efetuar Retiradas.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        return;
                    }

                }
                else
                {
                    MessageBox.Show("Item Inválido. Digite um item valido para Excluir", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCodigo.Focus();
                }
            }
            else
            {
                MessageBox.Show("Não existem itens para Excluir", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void excluirItem(int codDel)
        {
            int idVenda = Convert.ToInt32(dgCupom.Rows[codDel - 1].Cells[9].Value);
            excluirItemUpEstoque(Convert.ToInt32(dgCupom.Rows[codDel - 1].Cells[2].Value.ToString().Substring(0, 6)), Convert.ToDecimal(dgCupom.Rows[codDel - 1].Cells[1].Value.ToString().Trim(new char[] { ' ', 'x' })), idVenda);

            VendaDetalhesItemExtraCollections itemExtra = vendaDetalhesNegocios.buscarItensExtra(idVenda);

            foreach (var itemEx in itemExtra)
            {
                VendaDetalhesCollections itemProd = vendaDetalhesNegocios.buscarProdutoItensExtra(itemEx.IdVendasDetalhesExtra);
                foreach (var itemP in itemProd)
                {
                    excluirItemUpEstoque(itemP.codigoProduto, itemP.quantidadeVendaDetalhes, itemEx.IdVendasDetalhesExtra);
                }
            }

            dgCupom.Rows.RemoveAt(codDel - 1);

            dgCupom.Rows.Clear();
            CarregarDataGrid();

            txtItens.Text = dgCupom.Rows.Count.ToString();

            txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
        }

        private void CarregarDataGrid()
        {
            int i = 1;

            VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();
            VendaDetalhesCollections vendaDetalhesCollections = vendaDetalhesNegocios.consulta(numeroVenda);

            foreach (var item in vendaDetalhesCollections)
            {
                dgCupom.Rows.Add(Generic.formatItens(i) + i, "    " + Convert.ToInt32(item.quantidadeVendaDetalhes) + " x", Generic.formatCodigo(item.codigoProduto.ToString()) + "        R$ " + item.valorUnitarioVendaDetalhes, item.descricaoProduto, "R$ " + item.valorTotalVendaDetalhes, item.extraVendaDetalhes, item.especialVendaDetalhes, item.statusVendaDetalhes, item.dataEnvioVendaDetalhes, item.idVendaDetalhes);
                dgCupom.FirstDisplayedScrollingRowIndex = dgCupom.Rows.Count - 1;

                nomeAtendente = item.atendenteVenda;

                i++;
            }
        }

        private void carregaAtendimento(string tipoAtend, string siglaAtendimento, Button but1)
        {
            this.Cursor = Cursors.WaitCursor;

            b = but1;
            dgCupom.Rows.Clear();
            txtCodigo.Focus();
            tipoAtendimento = tipoAtend;
            lblAtendimento.Text = "Atendimento: " + "<font color=\"#0A11FF\">" + tipoAtend + "</font>";
            lblAtendimentoCupom.Text = "Atendimento " + tipoAtend;
            but = but1;
            checarPedidos(but1, siglaAtendimento);

            this.Cursor = Cursors.Default;
        }

        public void checaAtendimento()
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            VendaCabecalhoCollections vendaCabecalhoCollections = vendaCabecalhoNegocios.checaAtendTipo();

            try
            {
                foreach (var item in vendaCabecalhoCollections)
                {
                    Button btn = new Button();

                    int ret = Convert.ToInt32(vendaCabecalhoNegocios.checaAtend(item.atendenteVendaCabecalho));

                    if (ret == 1)
                    {
                        butonn(item.btn);
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        public void butonn(string but)
        {
            switch (but)
            {
                case "btnPedM1":
                    btnPedM1.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM2":
                    btnPedM2.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM3":
                    btnPedM3.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM4":
                    btnPedM4.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM5":
                    btnPedM5.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM6":
                    btnPedM6.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM7":
                    btnPedM7.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM8":
                    btnPedM8.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM9":
                    btnPedM9.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM10":
                    btnPedM10.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM11":
                    btnPedM11.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM12":
                    btnPedM12.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM13":
                    btnPedM13.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM14":
                    btnPedM14.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM15":
                    btnPedM15.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM16":
                    btnPedM16.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM17":
                    btnPedM17.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM18":
                    btnPedM18.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM19":
                    btnPedM19.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM20":
                    btnPedM20.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM21":
                    btnPedM21.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM22":
                    btnPedM22.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM23":
                    btnPedM23.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM24":
                    btnPedM24.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM25":
                    btnPedM25.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM26":
                    btnPedM26.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM27":
                    btnPedM27.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM28":
                    btnPedM28.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM29":
                    btnPedM29.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM30":
                    btnPedM30.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM31":
                    btnPedM31.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM32":
                    btnPedM32.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM33":
                    btnPedM33.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM34":
                    btnPedM34.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM35":
                    btnPedM35.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM36":
                    btnPedM36.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB1":
                    btnPedB1.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB2":
                    btnPedB2.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB3":
                    btnPedB3.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB4":
                    btnPedB4.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB5":
                    btnPedB5.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB6":
                    btnPedB6.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB7":
                    btnPedB7.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB8":
                    btnPedB8.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB9":
                    btnPedB9.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB10":
                    btnPedB10.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB11":
                    btnPedB11.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB12":
                    btnPedB12.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB13":
                    btnPedB13.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB14":
                    btnPedB14.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB15":
                    btnPedB15.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB16":
                    btnPedB16.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB17":
                    btnPedB17.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB18":
                    btnPedB18.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC1":
                    btnPedC1.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC2":
                    btnPedC2.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC3":
                    btnPedC3.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC4":
                    btnPedC4.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC5":
                    btnPedC5.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC6":
                    btnPedC6.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC7":
                    btnPedC7.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC8":
                    btnPedC8.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC9":
                    btnPedC9.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC10":
                    btnPedC10.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC11":
                    btnPedC11.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC12":
                    btnPedC12.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC13":
                    btnPedC13.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC14":
                    btnPedC14.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC15":
                    btnPedC15.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC16":
                    btnPedC16.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC17":
                    btnPedC17.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC18":
                    btnPedC18.BackColor = System.Drawing.Color.Red;
                    break;
            }

        }

        public void limpaCamposPedido()
        {
            txtCodigo.Focus();
            dgCupom.Rows.Clear();
            but.BackColor = System.Drawing.Color.White;
            txtItens.Text = "";
            txtQuantidade.Text = "1";
            txtTotalGeral.Text = "0,00";
            txtValorTotal.Text = "0,00";
            txtValorUnitario.Text = "0,00";
            lblAtendimentoCupom.Text = "Atendimento: ";
            lblAtendimento.Text = "Atendimento: ";
            lblCodVenda.Text = "Numero Venda: ";
            lblAtendente.Text = "Atendente: ";
        }

        #endregion

        #region BOTÕES

        private void btnPesquisarProdutos_Click(object sender, EventArgs e)
        {
            frmCadProdutos pesqProd = new frmCadProdutos();
            pesqProd.pesqProduto(txtCodigo);
        }

        private void btnCancelarItem_Click(object sender, EventArgs e)
        {
            PedidoItemExc();
            txtCodigo.Focus();
        }

        private void btnAddQtd_Click(object sender, EventArgs e)
        {
            txtQuantidade.ReadOnly = false;
            txtQuantidade.Text = "";
            txtQuantidade.Focus();
        }

        private void btnEspecial_Click(object sender, EventArgs e)
        {
            if (dgCupom.Rows.Count > 0)
            {
                frmPdvEspecial objFrmEspecial = new frmPdvEspecial();
                objFrmEspecial.ShowDialog();

                if (objFrmEspecial.sair != "N")
                {
                    MessageBox.Show("Especial Cancelado.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    if (Convert.ToInt32(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[7].Value) == 1 || Convert.ToInt32(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[7].Value) == 2)
                    {
                        try
                        {
                            string descricao = Convert.ToString(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[6].Value += objFrmEspecial.descricaoEspecial + " / ");

                            // ATUALIZAR ITENS COM ESPECIAL

                            vendaDetalhesNegocios.AtualizarItemEspecial(descricao, Convert.ToInt32(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[9].Value));
                            dgCupom.Rows.Clear();
                            vendaDetalhes.especialVendaDetalhes = string.Empty;
                            CarregarDataGrid();

                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Não foi possivel Adicionar o Especial.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Não e possivel atribuir Especial para esse Produto. Pedido Entregue", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }

            }
            else
            {
                MessageBox.Show("Não existem itens para atribuir a esse Extra", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void btnCancelarPedido_Click(object sender, EventArgs e)
        {
            if (dgCupom.Rows.Count > 0)
            {
                DialogResult cancelPedido = MessageBox.Show("Deseja Cancelar esse Pedido?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (cancelPedido == DialogResult.Yes)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();
                    VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();

                    try
                    {
                        vendaCabecalho.idVendaCabecalho = Convert.ToInt32(numeroVenda);
                        vendaCabecalho.totalVendaCabecalho = Convert.ToDecimal(txtTotalGeral.Text);

                        Convert.ToInt32(vendaCabecalhoNegocios.cancelarVenda(vendaCabecalho));

                        foreach (DataGridViewRow rows in dgCupom.Rows)
                        {
                            vendaDetalhes.codigoProduto = Convert.ToInt32(rows.Cells[2].Value.ToString().Substring(0, 6));
                            vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(rows.Cells[1].Value.ToString().Trim(new char[] { ' ', 'x' }));

                            excluirItemUpEstoque(vendaDetalhes.codigoProduto, vendaDetalhes.quantidadeVendaDetalhes, Convert.ToInt32(numeroVenda));
                        }

                        MessageBox.Show("Pedido Cancelado com Sucesso", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        limpaCamposPedido();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Não existem Pedido para Cancelar", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void btnEnviarPedido_Click(object sender, EventArgs e)
        {
            if (dgCupom.Rows.Count > 0)
            {
                int c = 0;

                for (int i = 0; i < dgCupom.RowCount; i++)
                {
                    if (Convert.ToInt32(dgCupom.Rows[i].Cells[7].Value) == 1)
                        c++;
                }

                if (c > 0)
                {
                    try
                    {
                        if (Generic.checaImpressao() == 3)
                        {
                            vendaCabecalho.idVendaCabecalho = vendaDetalhes.idVendaCabecalhoDetalhes;
                            vendaCabecalho.nomeAtendimento = tipoAtendimento;
                            vendaCabecalho.atendenteVendaCabecalho = nomeAtendente;

                            DialogResult Imprimir = MessageBox.Show("Deseja Imprimir o Pedido?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                            if (Imprimir == DialogResult.Yes)
                            {
                                frmRltImprimirCupom objFrmImpCupom = new frmRltImprimirCupom(vendaCabecalho, imprimiCupom.imprimePedido, "");
                                objFrmImpCupom.ShowDialog();
                            }
                        }
                        else
                            MessageBox.Show("Pedido Enviado com Sucesso", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);


                        vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                        vendaDetalhesNegocios.enviarVenda(vendaDetalhes, Generic.checaImpressao());

                        dgCupom.Rows.Clear();
                        CarregarDataGrid();


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Erro ao enviar o pedido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    MessageBox.Show("Todos os Pedidos ja foram entregues", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Não existem Itens para Enviar", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void btnVisualizarPedido_Click(object sender, EventArgs e)
        {
            frmTempoPedido objTempoPedido = new frmTempoPedido();
            objTempoPedido.ShowDialog();
        }

        private void btnExtra_Click(object sender, EventArgs e)
        {
            PedidoExtraAdd();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            frmPdvImprimir pdvImprimir = new frmPdvImprimir();
            pdvImprimir.ShowDialog();
        }

        private void btnFinalizarPedido_Click(object sender, EventArgs e)
        {
            if (dgCupom.RowCount > 0)
            {

                if (vendaDetalhesNegocios.checaItensEnviados(Convert.ToInt32(numeroVenda)) == 0)
                {
                    frmFinalizaVenda objFrmFinalizaVenda = new frmFinalizaVenda(numeroVenda, txtTotalGeral.Text, tipoAtendimento, nomeAtendente);
                    DialogResult finalizado = objFrmFinalizaVenda.ShowDialog();

                    if (finalizado == DialogResult.Yes)
                    {
                        limpaCamposPedido();
                    }
                }
                else
                {
                    MessageBox.Show("Não e possivel Finalizar. Ainda Existem Itens a serem entregues", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCodigo.Focus();
                }
            }
            else
            {
                MessageBox.Show("Não e possivel Finalizar. Não existem itens para esse Atendimento", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        #endregion

        private void retiradasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadRetiradas objRetiradas = new frmCadRetiradas();
            objRetiradas.ShowDialog();
        }

        private void calculadoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("calc.exe");
        }
        //------------------teste----------------------------
        private void bgwImprimiPedido_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!bgwImprimiPedido.CancellationPending)
            {
                bgwImprimiPedido.ReportProgress(0, "Imprimindo Cupom....");
                Thread.Sleep(500);
                VendaCabecalho vc = new VendaCabecalho();
                VendaCabecalhoNegocios vcn = new VendaCabecalhoNegocios();

                VendaCabecalhoCollections vcc = vcn.buscaPedPrint();

                if (vcc.Count > 0)
                {
                    foreach (var printe in vcc)
                    {
                        vc.idTipoPagamento = printe.idTipoPagamento;
                        vc.idVendaCabecalho = printe.idVendaCabecalho;
                        vc.nomeAtendimento = printe.nomeAtendimento;
                        vc.atendenteVendaCabecalho = printe.atendenteVendaCabecalho;
                        vc.statusVendaCabecalho = printe.statusVendaCabecalho;

                        frmRltImprimirCupom print = new frmRltImprimirCupom(vc);
                        print.imprimirPedido();

                        vcn.ExcluirPedPrint(vc.idTipoPagamento);

                        vendaDetalhes.idVendaCabecalhoDetalhes = vc.idVendaCabecalho;
                        vendaDetalhesNegocios.enviarVenda(vendaDetalhes, 3);
                    }
                }
                counter = 0;
                bgwImprimiPedido.CancelAsync();
            }
        }

        private void bgwImprimiPedido_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void bgwImprimiPedido_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            PdvPrintPed.Enabled = true;
        }

    }
}
