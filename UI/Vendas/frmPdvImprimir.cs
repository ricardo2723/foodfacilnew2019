﻿using System;
using System.Windows.Forms;

using Relatorios;
using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmPdvImprimir : Form
    {
        VendaDetalhes vendaDetalhes = new VendaDetalhes();

        public string numeroVenda;

        public frmPdvImprimir()
        {
            InitializeComponent();
        }

        private void frmPdvImprimir_Load(object sender, EventArgs e)
        {
            checaAtendimento(groupBox1);
        }

        private void checarPedidos(string siglaAtendimento, string atendimento)
        {
            this.Cursor = Cursors.WaitCursor;

            VendaCabecalho vendaCabecalho = new VendaCabecalho();
            vendaCabecalho.nomeAtendimento = siglaAtendimento;

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            numeroVenda = vendaCabecalhoNegocios.checaVendas(vendaCabecalho);

            try
            {
                vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                vendaDetalhes.tipoAtendimento = atendimento;

                frmRltImprimirPedidos rltImprimir = new frmRltImprimirPedidos(vendaDetalhes, frmLogin.meuLanche);
                rltImprimir.ShowDialog();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não e Possivel Imprimir. Não existe Pedido para esse Atendimento." + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public void checaAtendimento(GroupBox crtl)
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            VendaCabecalhoCollections vendaCabecalhoCollections = vendaCabecalhoNegocios.checaAtendTipo();
            
            try
            {
                foreach (var item in vendaCabecalhoCollections)
                {
                    Button btn = new Button();

                    int ret = Convert.ToInt32(vendaCabecalhoNegocios.checaAtend(item.atendenteVendaCabecalho));

                    if (ret == 1)
                    {
                        butonn(item.btn);
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        public void butonn(string but)
        {
            switch (but)
            {
                case "btnPedM1":
                    btnPedM1.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM2":
                    btnPedM2.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM3":
                    btnPedM3.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM4":
                    btnPedM4.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM5":
                    btnPedM5.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM6":
                    btnPedM6.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM7":
                    btnPedM7.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM8":
                    btnPedM8.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM9":
                    btnPedM9.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM10":
                    btnPedM10.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM11":
                    btnPedM11.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM12":
                    btnPedM12.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM13":
                    btnPedM13.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM14":
                    btnPedM14.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM15":
                    btnPedM15.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM16":
                    btnPedM16.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM17":
                    btnPedM17.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM18":
                    btnPedM18.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM19":
                    btnPedM19.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM20":
                    btnPedM20.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM21":
                    btnPedM21.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM22":
                    btnPedM22.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM23":
                    btnPedM23.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM24":
                    btnPedM24.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM25":
                    btnPedM25.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM26":
                    btnPedM26.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM27":
                    btnPedM27.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM28":
                    btnPedM28.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM29":
                    btnPedM29.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM30":
                    btnPedM30.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM31":
                    btnPedM31.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM32":
                    btnPedM32.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM33":
                    btnPedM33.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM34":
                    btnPedM34.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM35":
                    btnPedM35.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM36":
                    btnPedM36.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB1":
                    btnPedB1.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB2":
                    btnPedB2.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB3":
                    btnPedB3.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB4":
                    btnPedB4.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB5":
                    btnPedB5.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB6":
                    btnPedB6.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB7":
                    btnPedB7.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB8":
                    btnPedB8.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB9":
                    btnPedB9.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB10":
                    btnPedB10.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB11":
                    btnPedB11.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB12":
                    btnPedB12.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB13":
                    btnPedB13.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB14":
                    btnPedB14.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB15":
                    btnPedB15.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB16":
                    btnPedB16.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB17":
                    btnPedB17.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB18":
                    btnPedB18.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC1":
                    btnPedC1.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC2":
                    btnPedC2.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC3":
                    btnPedC3.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC4":
                    btnPedC4.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC5":
                    btnPedC5.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC6":
                    btnPedC6.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC7":
                    btnPedC7.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC8":
                    btnPedC8.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC9":
                    btnPedC9.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC10":
                    btnPedC10.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC11":
                    btnPedC11.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC12":
                    btnPedC12.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC13":
                    btnPedC13.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC14":
                    btnPedC14.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC15":
                    btnPedC15.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC16":
                    btnPedC16.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC17":
                    btnPedC17.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC18":
                    btnPedC18.BackColor = System.Drawing.Color.Red;
                    break;
            }

        }

        #region BOTÕES MESAS

        private void btnPedM1_Click(object sender, EventArgs e)
        {
            checarPedidos("M1", "MESA 01");
        }

        private void btnPedM2_Click(object sender, EventArgs e)
        {
            checarPedidos("M2", "MESA 02");
        }

        private void btnPedM3_Click(object sender, EventArgs e)
        {
            checarPedidos("M3", "MESA 03");
        }

        private void btnPedM4_Click(object sender, EventArgs e)
        {
            checarPedidos("M4", "MESA 04");
        }

        private void btnPedM5_Click(object sender, EventArgs e)
        {
            checarPedidos("M5", "MESA 05");
        }

        private void btnPedM6_Click(object sender, EventArgs e)
        {
            checarPedidos("M6", "MESA 06");
        }

        private void btnPedM7_Click(object sender, EventArgs e)
        {
            checarPedidos("M7", "MESA 07");
        }

        private void btnPedM8_Click(object sender, EventArgs e)
        {
            checarPedidos("M8", "MESA 08");
        }

        private void btnPedM9_Click(object sender, EventArgs e)
        {
            checarPedidos("M9", "MESA 09");
        }

        private void btnPedM10_Click(object sender, EventArgs e)
        {
            checarPedidos("M10", "MESA 10");
        }

        private void btnPedM11_Click(object sender, EventArgs e)
        {
            checarPedidos("M11", "MESA 11");
        }

        private void btnPedM12_Click(object sender, EventArgs e)
        {
            checarPedidos("M12", "MESA 12");
        }

        private void btnPedM13_Click(object sender, EventArgs e)
        {
            checarPedidos("M13", "MESA 13");
        }

        private void btnPedM14_Click(object sender, EventArgs e)
        {
            checarPedidos("M14", "MESA 14");
        }

        private void btnPedM15_Click(object sender, EventArgs e)
        {
            checarPedidos("M15", "MESA 15");
        }

        private void btnPedM16_Click(object sender, EventArgs e)
        {
            checarPedidos("M16", "MESA 16");
        }

        private void btnPedM17_Click(object sender, EventArgs e)
        {
            checarPedidos("M17", "MESA 17");
        }

        private void btnPedM18_Click(object sender, EventArgs e)
        {
            checarPedidos("M18", "MESA 18");
        }

        private void btnPedM19_Click(object sender, EventArgs e)
        {
            checarPedidos("M19", "MESA 19");
        }

        private void btnPedM20_Click(object sender, EventArgs e)
        {
            checarPedidos("M20", "MESA 20");
        }

        private void btnPedM21_Click(object sender, EventArgs e)
        {
            checarPedidos("M21", "MESA 21");
        }

        private void btnPedM22_Click(object sender, EventArgs e)
        {
            checarPedidos("M22", "MESA 22");
        }

        private void btnPedM23_Click(object sender, EventArgs e)
        {
            checarPedidos("M23", "MESA 23");
        }

        private void btnPedM24_Click(object sender, EventArgs e)
        {
            checarPedidos("M24", "MESA 24");
        }

        private void btnPedM25_Click(object sender, EventArgs e)
        {
            checarPedidos("M25", "MESA 25");
        }

        private void btnPedM26_Click(object sender, EventArgs e)
        {
            checarPedidos("M26", "MESA 26");
        }

        private void btnPedM27_Click(object sender, EventArgs e)
        {
            checarPedidos("M27", "MESA 27");
        }

        private void btnPedM28_Click(object sender, EventArgs e)
        {
            checarPedidos("M28", "MESA 28");
        }

        private void btnPedM29_Click(object sender, EventArgs e)
        {
            checarPedidos("M29", "MESA 29");
        }

        private void btnPedM30_Click(object sender, EventArgs e)
        {
            checarPedidos("M30", "MESA 30");
        }

        private void btnPedM31_Click(object sender, EventArgs e)
        {
            checarPedidos("M31", "MESA 31");
        }

        private void btnPedM32_Click(object sender, EventArgs e)
        {
            checarPedidos("M32", "MESA 32");
        }

        private void btnPedM33_Click(object sender, EventArgs e)
        {
            checarPedidos("M33", "MESA 33");
        }

        private void btnPedM34_Click(object sender, EventArgs e)
        {
            checarPedidos("M34", "MESA 34");
        }

        private void btnPedM35_Click(object sender, EventArgs e)
        {
            checarPedidos("M35", "MESA 35");
        }

        private void btnPedM36_Click(object sender, EventArgs e)
        {
            checarPedidos("M36", "MESA 36");
        }

        #endregion

        #region BOTÕES BALCÃO

        private void btnPedB1_Click(object sender, EventArgs e)
        {
            checarPedidos("B1", "BALCÃO 01");
        }

        private void btnPedB2_Click(object sender, EventArgs e)
        {
            checarPedidos("B2", "BALCÃO 02");
        }

        private void btnPedB3_Click(object sender, EventArgs e)
        {
            checarPedidos("B3", "BALCÃO 03");
        }

        private void btnPedB4_Click(object sender, EventArgs e)
        {
            checarPedidos("B4", "BALCÃO 04");
        }

        private void btnPedB5_Click(object sender, EventArgs e)
        {
            checarPedidos("B5", "BALCÃO 05");
        }

        private void btnPedB6_Click(object sender, EventArgs e)
        {
            checarPedidos("B6", "BALCÃO 06");
        }

        private void btnPedB7_Click(object sender, EventArgs e)
        {
            checarPedidos("B7", "BALCÃO 07");
        }

        private void btnPedB8_Click(object sender, EventArgs e)
        {
            checarPedidos("B8", "BALCÃO 08");
        }

        private void btnPedB9_Click(object sender, EventArgs e)
        {
            checarPedidos("B9", "BALCÃO 09");
        }

        private void btnPedB10_Click(object sender, EventArgs e)
        {
            checarPedidos("B10", "BALCÃO 10");
        }

        private void btnPedB11_Click(object sender, EventArgs e)
        {
            checarPedidos("B11", "BALCÃO 11");
        }

        private void btnPedB12_Click(object sender, EventArgs e)
        {
            checarPedidos("B12", "BALCÃO 12");
        }

        private void btnPedB13_Click(object sender, EventArgs e)
        {
            checarPedidos("B13", "BALCÃO 13");
        }

        private void btnPedB14_Click(object sender, EventArgs e)
        {
            checarPedidos("B14", "BALCÃO 14");
        }

        private void btnPedB15_Click(object sender, EventArgs e)
        {
            checarPedidos("B15", "BALCÃO 15");
        }

        private void btnPedB16_Click(object sender, EventArgs e)
        {
            checarPedidos("B16", "BALCÃO 16");
        }

        private void btnPedB17_Click(object sender, EventArgs e)
        {
            checarPedidos("B17", "BALCÃO 17");
        }

        private void btnPedB18_Click(object sender, EventArgs e)
        {
            checarPedidos("B18", "BALCÃO 18");
        }

        #endregion

        #region BOTÕES CARRO

        private void btnPedC1_Click(object sender, EventArgs e)
        {
            checarPedidos("C", "CARRO 01");
        }

        private void btnPedC2_Click(object sender, EventArgs e)
        {
            checarPedidos("C2", "CARRO 02");
        }

        private void btnPedC3_Click(object sender, EventArgs e)
        {
            checarPedidos("C3", "CARRO 03");
        }

        private void btnPedC4_Click(object sender, EventArgs e)
        {
            checarPedidos("C4", "CARRO 04");
        }

        private void btnPedC5_Click(object sender, EventArgs e)
        {
            checarPedidos("C5", "CARRO 05");
        }

        private void btnPedC6_Click(object sender, EventArgs e)
        {
            checarPedidos("C6", "CARRO 06");
        }

        private void btnPedC7_Click(object sender, EventArgs e)
        {
            checarPedidos("C7", "CARRO 07");
        }

        private void btnPedC8_Click(object sender, EventArgs e)
        {
            checarPedidos("C8", "CARRO 08");
        }

        private void btnPedC9_Click(object sender, EventArgs e)
        {
            checarPedidos("C9", "CARRO 09");
        }

        private void btnPedC10_Click(object sender, EventArgs e)
        {
            checarPedidos("C10", "CARRO 10");
        }

        private void btnPedC11_Click(object sender, EventArgs e)
        {
            checarPedidos("C11", "CARRO 11");
        }

        private void btnPedC12_Click(object sender, EventArgs e)
        {
            checarPedidos("C12", "CARRO 12");
        }

        private void btnPedC13_Click(object sender, EventArgs e)
        {
            checarPedidos("C13", "CARRO 13");
        }

        private void btnPedC14_Click(object sender, EventArgs e)
        {
            checarPedidos("C14", "CARRO 14");
        }

        private void btnPedC15_Click(object sender, EventArgs e)
        {
            checarPedidos("C15", "CARRO 15");
        }

        private void btnPedC16_Click(object sender, EventArgs e)
        {
            checarPedidos("C16", "CARRO 16");
        }

        private void btnPedC17_Click(object sender, EventArgs e)
        {
            checarPedidos("C17", "CARRO 17");
        }

        private void btnPedC18_Click(object sender, EventArgs e)
        {
            checarPedidos("C18", "CARRO 18");
        }

        #endregion

    }
}
