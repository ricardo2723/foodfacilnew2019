﻿namespace UI
{
    partial class frmCarregaPedidosPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCarregaPedidosPrint));
            this.bwPrintCupom = new System.ComponentModel.BackgroundWorker();
            this.lblMenssagem = new DevComponents.DotNetBar.LabelX();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.SuspendLayout();
            // 
            // bwPrintCupom
            // 
            this.bwPrintCupom.WorkerReportsProgress = true;
            this.bwPrintCupom.WorkerSupportsCancellation = true;
            this.bwPrintCupom.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPrintCupom_DoWork);
            this.bwPrintCupom.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwPrintCupom_ProgressChanged);
            this.bwPrintCupom.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwPrintCupom_RunWorkerCompleted);
            // 
            // lblMenssagem
            // 
            // 
            // 
            // 
            this.lblMenssagem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMenssagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenssagem.ForeColor = System.Drawing.Color.Blue;
            this.lblMenssagem.Location = new System.Drawing.Point(98, 12);
            this.lblMenssagem.Name = "lblMenssagem";
            this.lblMenssagem.SingleLineColor = System.Drawing.Color.Blue;
            this.lblMenssagem.Size = new System.Drawing.Size(268, 84);
            this.lblMenssagem.TabIndex = 5;
            this.lblMenssagem.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // circularProgress1
            // 
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.Location = new System.Drawing.Point(10, 12);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot;
            this.circularProgress1.ProgressColor = System.Drawing.Color.DodgerBlue;
            this.circularProgress1.Size = new System.Drawing.Size(93, 84);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 4;
            this.circularProgress1.UseWaitCursor = true;
            // 
            // frmCarregaPedidosPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 113);
            this.Controls.Add(this.lblMenssagem);
            this.Controls.Add(this.circularProgress1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCarregaPedidosPrint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmCarregaPedidosPrint_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker bwPrintCupom;
        private DevComponents.DotNetBar.LabelX lblMenssagem;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
    }
}