﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmVerPedidos : Form
    {
        VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();

        public frmVerPedidos()
        {
            InitializeComponent();
            dgvPedidos1.AutoGenerateColumns = false;
            
            //Carrega Combo Grupos
            GruposNegocios gruposNegocios = new GruposNegocios();
            GruposCollections gruposCollections = gruposNegocios.PreencheCbGrupos();
            cbGrupo1.DataSource = null;
            cbGrupo1.DataSource = gruposCollections;
          
        }

        private void cbGrupo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            carregaPedidos(cbGrupo1);
        }

        private void dgvPedidos1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPedidos1.Columns[e.ColumnIndex].Name == "btnEntregar")
            {
                entregarPedido();
            }
        }

        private void dgvPedidos1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    entregarPedido();
                    break;
            }
        }

        #region METODOS

        private void carregaPedidos(ComboBox cb)
        {
            VendaDetalhesCollections vendaDetalhesCollection = vendaDetalhesNegocios.visualizarPedidos(Convert.ToInt32(cb.SelectedValue));

            dgvPedidos1.DataSource = null;
            dgvPedidos1.DataSource = vendaDetalhesCollection;

            lblNumPedidos1.Text = "Pedidos Pendentes: " + dgvPedidos1.Rows.Count;
            dgvPedidos1.Focus();
        }

        private void entregarPedido()
        {
            string retorno = "";
            
            try
            {
                VendaDetalhes vd = dgvPedidos1.SelectedRows[0].DataBoundItem as VendaDetalhes;
 
                retorno = vendaDetalhesNegocios.entregarVenda(vd);

                int checaRetorno = Convert.ToInt32(retorno);                

                MessageBox.Show("Pedido Entregue", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                
                dgvPedidos1.Refresh();

                carregaPedidos(cbGrupo1);
               
            }
            catch (Exception)
            {
                MessageBox.Show("Error ao entregar Pedido: não possui Pedidos a entregar", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion        

        
    }
}
