﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;
using Relatorios;


namespace UI
{
    public partial class frmDelivery : Form
    {
        string bairro;

        VendaCabecalho vendaCabecalhoFinaliza = new VendaCabecalho();
        VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
        VendaDetalhes vendaDetalhes = new VendaDetalhes();
        VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();
        VendaCabecalhoDelivery delivery = new VendaCabecalhoDelivery();
        VendaCabecalhoDeliveryCollections deliveryCollections = new VendaCabecalhoDeliveryCollections();
        ClientesNegocios clientesNegocios = new ClientesNegocios();
        Clientes cliente = new Clientes();
        Clientes clienteSelecionado = new Clientes();

        ProdutosNegocios produtosNegocios = new ProdutosNegocios();

        public frmDelivery()
        {
            InitializeComponent();
            lblOperador.Text = frmLogin.usuariosLogin.usuarioLogin;
            lblNomeEmpresa.Text = frmLogin.meuLanche.nomeMeuLanche;
        }

        public string numeroVenda = "0";
        public string nomeAtendente = frmLogin.usuariosLogin.usuarioLogin;
        public string tipoAtendimento = "DV";

        public bool pararcronometro;

        public string extraDescricao;
        public decimal extraValor;
        public string especialDescricao;

        private void frmDelivery_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
            carregaPedido(tipoAtendimento);
            carregaFormaPagamento();
            txtTelefone.Focus();
        }

        private void frmDelivery_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnPesquisarProdutos.PerformClick();
                    break;
                case Keys.F2:
                    btnAddQtd.PerformClick();
                    break;
                case Keys.F3:
                    btnCancelarItem.PerformClick();
                    break;
                case Keys.F4:
                    btnCancelarPedido.PerformClick();
                    break;
                case Keys.F5:
                    txtTelefone.Focus();
                    break;
                case Keys.F6:
                    btnVisualizarPedido.PerformClick();
                    break;
                case Keys.F7:
                    btnExtra.PerformClick();
                    break;
                case Keys.F8:
                    btnEspecial.PerformClick();
                    break;
                case Keys.F9:
                    btnImprimir.PerformClick();
                    break;
                case Keys.F10:
                    btnFinalizarPedido.PerformClick();
                    break;
                case Keys.F11:
                    cmsMenuPdv.Show();
                    break;
                case Keys.Escape:
                    cbTipoPagamento.Focus();
                    break;
            }
        }

        //private void PdvDataHora_Tick(object sender, EventArgs e)
        //{
        //lblDataHora.Text = DateTime.Now.ToShortDateString() + " ÀS " + DateTime.Now.ToLongTimeString();
        //}

        #region BOTÕES MINI E FECHAR

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        #endregion

        #region TXT_KEY AND LEAVE

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtCodigo.Text != "")
                    {
                        PedidoItemAdd();
                    }
                    else
                    {
                        MessageBox.Show("Digite o Código do Produto", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtQuantidade_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtQuantidade.Text != "")
                    {
                        txtQuantidade.ReadOnly = true;
                        txtQuantidade.BackColor = System.Drawing.SystemColors.Window;
                        txtCodigo.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {

                e.Handled = true;

            }
        }

        private void txtQuantidade_Leave(object sender, EventArgs e)
        {
            if (txtQuantidade.Text == "" || Convert.ToDecimal(txtQuantidade.Text) == 0)
            {
                MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtQuantidade.Focus();
            }
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtTelefone.Text != "")
                    {
                        pesquisaClienteTelefone(txtTelefone.Text);
                    }
                    else
                    {
                        MessageBox.Show("Digite o Código do Produto", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
            }
        }

        private void dgcDadosCliente_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    getCliente();
                    abrePedidos(tipoAtendimento);
                    break;
            }
        }

        private void txtTrocoPara_Leave(object sender, EventArgs e)
        {
            try
            {
                txtTrocoPara.Text = Convert.ToDecimal(txtTrocoPara.Text).ToString("N");
                txtValorTroco.Text = (Convert.ToDecimal(txtTrocoPara.Text) - Convert.ToDecimal(txtTotalGeralPedido.Text)).ToString("N");
            }
            catch (Exception ex)
            {
                MessageBox.Show("É obrigatório somente numeros: " + ex, "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void cbTipoPagamento_Leave(object sender, EventArgs e)
        {
            if (cbTipoPagamento.Text == "CARTÃO" || cbTipoPagamento.Text == "CARTAO")
            {
                ckbTroco.Checked = false;
                ckbTroco.Enabled = false;
                btnFinalizarPedido.Focus();
            }
            else
            {
                ckbTroco.Enabled = true;
                ckbTroco.Focus();
            }
        }

        private void ckbTroco_Click(object sender, EventArgs e)
        {
            if (ckbTroco.Checked)
            {
                gbTroco.Enabled = true;
                txtTrocoPara.Focus();
            }
            else
            {
                gbTroco.Enabled = false;
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "DELIVERY")
                {

                }
            }
        }

        private void carregaPedido(string siglaAtendimento)
        {
            this.Cursor = Cursors.WaitCursor;

            txtTelefone.Focus();
            checarPedidos(siglaAtendimento);

            this.Cursor = Cursors.Default;
        }

        private void checarPedidos(string atendimento)
        {
            VendaCabecalho vendaCabecalho = new VendaCabecalho();
            vendaCabecalho.nomeAtendimento = atendimento;
            vendaCabecalho.usuarioId = frmLogin.usuariosLogin.usuarioId;
            vendaCabecalho.statusVendaCabecalho = 0;

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            numeroVenda = vendaCabecalhoNegocios.checaAbreVendas(vendaCabecalho);

            try
            {
                int ckVenda = Convert.ToInt32(numeroVenda);

                if (Convert.ToInt32(numeroVenda) > 0)
                {
                    deliveryCollections = vendaCabecalhoNegocios.checaDelivery(numeroVenda);

                    foreach (var item in deliveryCollections)
                    {
                        delivery.idEntrega = item.idEntrega;
                        delivery.idCliente = item.idCliente;
                        delivery.nomeCliente = item.nomeCliente;
                        delivery.telefoneCliente = item.telefoneCliente;
                        delivery.celularCliente = item.celularCliente;
                        delivery.enderecoCliente = item.enderecoCliente;
                        delivery.complementoCliente = item.complementoCliente;
                        delivery.bairroCliente = item.bairroCliente;
                        delivery.pontoReferenciaCliente = item.pontoReferenciaCliente;
                        delivery.valorFrete = item.valorFrete;
                    }

                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);

                    //Busca todos os itens referente a essa venda na tabela VendaDetalhes
                    CarregarDataGrid();
                    txtItens.Text = dgCupom.Rows.Count.ToString();
                    txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
                    lblCodVenda.Text = "Código Venda: " + Generic.formatCodigo(numeroVenda);
                    lblAtendente.Text = "Atendente: " + nomeAtendente;

                    lblNomeCliente.Text = delivery.nomeCliente;
                    lblEnderecoCliente.Text = delivery.enderecoCliente + " " + delivery.complementoCliente + " - " + delivery.bairroCliente;
                    lblPontoReferencia.Text = delivery.pontoReferenciaCliente;
                    txtValorFrete.Text = delivery.valorFrete.ToString("N");
                    
                    if(delivery.telefoneCliente != "")
                        txtTelefone.Text = delivery.telefoneCliente.Substring(2);

                    if (delivery.celularCliente != "")
                        txtTelefone.Text = delivery.celularCliente.Substring(2);

                    txtTotalGeralPedido.Text = (Convert.ToDecimal(txtTotalGeral.Text) + Convert.ToDecimal(txtValorFrete.Text)).ToString("N");

                }
            }
            catch (Exception)
            {
                numeroVenda = "0";
                txtTelefone.Focus();
            }
        }

        private void abrePedidos(string atendimento)
        {
            DialogResult yes = MessageBox.Show("Deseja Abrir uma Venda para esse Cliente? ", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (yes == DialogResult.Yes)
            {
                VendaCabecalho vendaCabecalho = new VendaCabecalho();
                vendaCabecalho.nomeAtendimento = atendimento;
                vendaCabecalho.usuarioId = frmLogin.usuariosLogin.usuarioId;
                vendaCabecalho.atendenteVendaCabecalho = frmLogin.usuariosLogin.usuarioLogin;
                nomeAtendente = vendaCabecalho.atendenteVendaCabecalho;
                vendaCabecalho.statusVendaCabecalho = 0;

                VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                numeroVenda = vendaCabecalhoNegocios.abreVendas(vendaCabecalho);

                try
                {
                    int ckVenda = Convert.ToInt32(numeroVenda);

                    delivery.idVendaCabecalho = ckVenda;
                    delivery.valorFrete = Convert.ToDecimal(txtValorFrete.Text);

                    string numeroEntrega = vendaCabecalhoNegocios.abreDelivery(delivery);
                    ckVenda = Convert.ToInt32(numeroEntrega);

                    if (Convert.ToInt32(numeroVenda) > 0)
                    {
                        dgCupom.Rows.Clear();
                        txtItens.Text = dgCupom.Rows.Count.ToString();
                        txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
                        txtTotalGeralPedido.Text = (Convert.ToDecimal(txtTotalGeralPedido.Text) + Convert.ToDecimal(txtValorFrete.Text)).ToString("N");
                        lblCodVenda.Text = "Código Venda: " + Generic.formatCodigo(numeroVenda);
                        lblAtendente.Text = "Atendente: " + nomeAtendente;

                        txtCodigo.Focus();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Error ao abrir Pedido: " + numeroVenda);
                }
            }
            else
            {
                lblNomeCliente.Text = string.Empty;
                lblEnderecoCliente.Text = string.Empty;
                lblPontoReferencia.Text = string.Empty;
                txtValorFrete.Text = string.Empty;
                txtTelefone.Text = String.Empty;

                txtTelefone.Focus();
            }

        }

        public void PedidoItemAdd()
        {
            if (Convert.ToInt32(numeroVenda) != 0)
            {
                int i, z;

                Produtos produtos = new Produtos();
                produtos.codigoProduto = Convert.ToInt32(txtCodigo.Text);

                VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                ProdutosCollections produtosCollections = null; //vendaCabecalhoNegocios.GetProdutoVenda(produtos);

                if (produtosCollections.Count > 0)
                {
                    foreach (var item in produtosCollections)
                    {
                        txtValorUnitario.Text = item.valorVendaProduto.ToString();
                        vendaDetalhes.codigoProduto = item.codigoProduto;
                        vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                        vendaDetalhes.estoqueProduto = item.estoqueProduto;
                        vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                        vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                        vendaDetalhes.descricaoProduto = item.descricaoProduto;
                    }

                    //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                    if (vendaDetalhes.controlaEstoqueProduto == 'S')
                    {
                        //VERIFICA SE EXISTE O PRODUTO EM ESTOQUE PARA A VENDA E SE E MAIOR QUE O PEDIDO
                        if (vendaDetalhes.estoqueProduto < Convert.ToDecimal(txtQuantidade.Text))
                        {
                            MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            txtCodigo.Text = "";
                            return;
                        }
                        else
                        {
                            //SE EXISTIR ESTOQUE SUFICIENTE VERIFICA SE O ESTOQUE ESTA CRITICO
                            if ((vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQuantidade.Text)) <= vendaDetalhes.estoqueCriticoProduto)
                            {
                                MessageBox.Show("O estoque está abaixo do Permitido. Providenciar o Produto. Estoque: " + Convert.ToInt32(vendaDetalhes.estoqueProduto) + " Unidades", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                            if (vendaDetalhes.manufaturadoProduto == 'N')
                            {
                                try
                                {
                                    vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQuantidade.Text);

                                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                                    int checaretorno = Convert.ToInt32(retorno);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }

                            }
                            else if (vendaDetalhes.manufaturadoProduto == 'S')
                            {
                                try
                                {
                                    //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                                    vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQuantidade.Text);

                                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                                    int checaretorno = Convert.ToInt32(retorno);

                                    //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                                    produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                                    VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                                    foreach (var item in produtosCollections)
                                    {
                                        vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                                        vendaDetalhesManufaturado.estoqueProduto = vendaDetalhes.estoqueProduto;

                                        retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                                        checaretorno = Convert.ToInt32(retorno);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }
                            }
                        }
                    }
                    else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                    {
                        if (vendaDetalhes.manufaturadoProduto == 'S')
                        {
                            try
                            {
                                //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                                produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                                VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                                foreach (var item in produtosCollections)
                                {
                                    decimal qtd1 = item.quantidadeProdutoManufaturado;
                                    decimal totalEst = qtd1 * Convert.ToDecimal(txtQuantidade.Text);

                                    if (item.estoqueProduto < totalEst)
                                    {
                                        MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                }

                                foreach (var item in produtosCollections)
                                {
                                    vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                    vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                    vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                    if (vendaDetalhesManufaturado.estoqueProduto < Convert.ToDecimal(txtQuantidade.Text))
                                    {
                                        MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }

                                    vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto - (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * Convert.ToDecimal(txtQuantidade.Text));

                                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                                    int checaretorno = Convert.ToInt32(retorno);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                        }
                    }
                    txtValorTotal.Text = Convert.ToString(Convert.ToDecimal(txtQuantidade.Text) * Convert.ToDecimal(txtValorUnitario.Text));

                    txtTotalGeral.Text = Convert.ToString(Convert.ToDecimal(txtTotalGeral.Text) + Convert.ToDecimal(txtValorTotal.Text));

                    txtTotalGeralPedido.Text = (Convert.ToDecimal(txtValorFrete.Text) + Convert.ToDecimal(txtTotalGeral.Text)).ToString("N");

                    vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                    vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(txtQuantidade.Text);
                    vendaDetalhes.valorUnitarioVendaDetalhes = Convert.ToDecimal(txtValorUnitario.Text);
                    vendaDetalhes.valorTotalVendaDetalhes = Convert.ToDecimal(txtValorTotal.Text);
                    vendaDetalhes.statusVendaDetalhes = 1;
                    vendaDetalhes.dataEnvioVendaDetalhes = DateTime.Now;


                    VendaDetalhesNegocios vendaDetalhesNegociosIns = new VendaDetalhesNegocios();
                    string retorno1 = vendaDetalhesNegociosIns.inserir(vendaDetalhes);

                    try
                    {
                        int checaRetorno = Convert.ToInt32(retorno1);

                        i = dgCupom.Rows.Count;
                        z = i + 1;

                        dgCupom.Rows.Add(Generic.formatItens(z) + z, "    " + txtQuantidade.Text + " x", Generic.formatCodigo(txtCodigo.Text) + "        R$ " + (Convert.ToDecimal(txtValorUnitario.Text) + extraValor), vendaDetalhes.descricaoProduto + extraDescricao, "R$ " + (Convert.ToDecimal(txtValorTotal.Text) + extraValor * Convert.ToDecimal(txtQuantidade.Text)), extraDescricao, especialDescricao, vendaDetalhes.statusVendaDetalhes, DateTime.Now);

                        txtItens.Text = dgCupom.Rows.Count.ToString();

                        dgCupom.FirstDisplayedScrollingRowIndex = dgCupom.Rows.Count - 1;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno1, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }

                    extraValor = 0;
                    extraDescricao = "";
                    especialDescricao = "";

                    txtCodigo.Text = "";
                    txtQuantidade.Text = "1";
                    txtCodigo.Focus();

                }
                else
                {
                    MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    txtCodigo.Text = txtCodigo.Text;
                }
            }
            else
            {
                MessageBox.Show("E necessário escolher um tipo de Atendimento", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = txtCodigo.Text;
            }

        }

        private void PedidoExtraAdd()
        {
            string manufaturado = "N";

            if (dgCupom.Rows.Count > 0)
            {
                frmPdvExtra objExtra = new frmPdvExtra(dgCupom.RowCount);
                objExtra.ShowDialog();

                if (objExtra.itemExtra != 0)
                {
                    if (Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[7].Value) == 1 || Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[7].Value) == 2)
                    {
                        if (Convert.ToInt32(numeroVenda) != 0)
                        {
                            Produtos produtos = new Produtos();
                            produtos.codigoProduto = Convert.ToInt32(objExtra.codigoProduto);

                            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                            ProdutosCollections produtosCollections = vendaCabecalhoNegocios.GetProdutoVenda('P', produtos.codigoProduto);

                            if (produtosCollections.Count > 0)
                            {
                                foreach (var item in produtosCollections)
                                {
                                    vendaDetalhes.valorUnitarioVendaDetalhes = item.valorVendaProduto;
                                    vendaDetalhes.descricaoProduto = item.descricaoProduto;
                                    vendaDetalhes.codigoProduto = item.codigoProduto;
                                    vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                                    vendaDetalhes.estoqueProduto = item.estoqueProduto;
                                    vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                                    vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                                }

                                //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                                if (vendaDetalhes.controlaEstoqueProduto == 'S')
                                {
                                    //VERIFICA SE EXISTE O PRODUTO EM ESTOQUE PARA A VENDA E SE E MAIOR QUE O PEDIDO
                                    if (vendaDetalhes.estoqueProduto < Convert.ToDecimal(objExtra.qtdExtra))
                                    {
                                        MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        manufaturado = "S";
                                        return;
                                    }
                                    else
                                    {
                                        //SE EXISTIR ESTOQUE SUFICIENTE VERIFICA SE O ESTOQUE ESTA CRITICO
                                        if ((vendaDetalhes.estoqueProduto - Convert.ToDecimal(objExtra.qtdExtra)) <= vendaDetalhes.estoqueCriticoProduto)
                                        {
                                            MessageBox.Show("O estoque está abaixo do Permitido. Providenciar o Produto. Estoque: " + Convert.ToInt32(vendaDetalhes.estoqueProduto) + " Unidades", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }

                                        //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                                        if (vendaDetalhes.manufaturadoProduto == 'N')
                                        {
                                            try
                                            {
                                                vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(objExtra.qtdExtra);

                                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                manufaturado = "S";
                                                return;
                                            }

                                        }
                                        else if (vendaDetalhes.manufaturadoProduto == 'S')
                                        {
                                            MessageBox.Show("Não e possível adicionar Produtos Manufaturado a Extra", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            manufaturado = "S";
                                            return;
                                        }
                                    }
                                }
                                else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                                {
                                    if (vendaDetalhes.manufaturadoProduto == 'N')
                                    {
                                        try
                                        {
                                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                                            foreach (var item in produtosCollections)
                                            {
                                                decimal qtd1 = item.quantidadeProdutoManufaturado;
                                                decimal totalEst = qtd1 * Convert.ToDecimal(objExtra.qtdExtra);

                                                if (item.estoqueProduto < totalEst)
                                                {
                                                    MessageBox.Show("Estoque de" + item.descricaoProduto + " é insuficiente! Providencie Estoque: ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                    manufaturado = "S";
                                                    return;
                                                }
                                            }

                                            foreach (var item in produtosCollections)
                                            {
                                                vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                                vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                                if (vendaDetalhesManufaturado.estoqueProduto < Convert.ToDecimal(objExtra.qtdExtra))
                                                {
                                                    MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                    manufaturado = "S";
                                                    return;
                                                }

                                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto - (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * Convert.ToDecimal(objExtra.qtdExtra));

                                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            manufaturado = "S";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Não e possível adicionar Produtos Manufaturado a Extra", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        manufaturado = "S";
                                        return;
                                    }
                                }

                                vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                                vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(objExtra.qtdExtra);
                                vendaDetalhes.valorTotalVendaDetalhes = vendaDetalhes.valorUnitarioVendaDetalhes * vendaDetalhes.quantidadeVendaDetalhes;
                                vendaDetalhes.statusVendaDetalhes = 6;
                                vendaDetalhes.dataEnvioVendaDetalhes = DateTime.Now;

                                VendaDetalhesNegocios vendaDetalhesNegociosIns = new VendaDetalhesNegocios();
                                string retorno1 = vendaDetalhesNegociosIns.inserir(vendaDetalhes);

                                try
                                {
                                    vendaDetalhes.idVendaDetalhes = Convert.ToInt32(retorno1);

                                    vendaDetalhesNegociosIns.inserirItemExtra(Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[9].Value), vendaDetalhes.idVendaDetalhes, Convert.ToInt32(numeroVenda));
                                    //************************ATE AQUI DEU BAIXA NO ESTOQUE NO EXTRA E ADICIONOU O PEDIDO A LISTA ****************************
                                    //ATUALIZAR ITEM COM EXTRA
                                    string descricao = Convert.ToString(dgCupom.Rows[objExtra.itemExtra - 1].Cells[5].Value += vendaDetalhes.descricaoProduto + " = " + objExtra.qtdExtra + " / ");

                                    vendaDetalhesNegocios.AtualizarItemExtra(descricao, Convert.ToInt32(dgCupom.Rows[objExtra.itemExtra - 1].Cells[9].Value));

                                    if (manufaturado == "S")
                                    {
                                        MessageBox.Show("Não foi possivel Adicionar o Extra.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }
                                    else
                                    {
                                        //aqui esta o error tenho que acrescenta na mesma linha

                                        try
                                        {
                                            dgCupom.Rows.Clear();
                                            CarregarDataGrid();
                                            vendaDetalhes.extraVendaDetalhes = string.Empty;
                                            txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
                                        }
                                        catch (Exception)
                                        {
                                            MessageBox.Show("Não foi possivel Adicionar o Extra.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        }
                                    }

                                    MessageBox.Show("Extra inserido com Sucesso", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno1, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    manufaturado = "S";
                                    return;
                                }

                            }
                            else
                            {
                                MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                                txtCodigo.Text = txtCodigo.Text;
                            }
                        }
                        else
                        {
                            MessageBox.Show("E necessário escolher um tipo de Atendimento", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            txtCodigo.Text = txtCodigo.Text;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Não e possivel atribuir Extra para esse Produto. Pedido Entregue", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    return;
                }
            }

            else
            {
                MessageBox.Show("Não existem itens para atribuir a esse Extra", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
            //**************************************FIM**************************************
        }

        public void excluirItemUpEstoque(int codProd, decimal qtd, int idVenda)
        {
            //DEVOLVE ITEM EXCLUIDO AO ESTOQUE

            Produtos produtos = new Produtos();
            produtos.codigoProduto = Convert.ToInt32(codProd);

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            ProdutosCollections produtosCollections = null; // vendaCabecalhoNegocios.GetProdutoVenda(produtos);

            if (produtosCollections.Count > 0)
            {
                foreach (var item in produtosCollections)
                {
                    txtValorUnitario.Text = item.valorVendaProduto.ToString();
                    vendaDetalhes.codigoProduto = item.codigoProduto;
                    vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                    vendaDetalhes.estoqueProduto = item.estoqueProduto;
                    vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                    vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                }

                //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                if (vendaDetalhes.controlaEstoqueProduto == 'S')
                {
                    //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                    if (vendaDetalhes.manufaturadoProduto == 'N')
                    {
                        try
                        {
                            vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                            Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                    }
                    else if (vendaDetalhes.manufaturadoProduto == 'S')
                    {
                        try
                        {
                            //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                            vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                            Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));

                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                            foreach (var item in produtosCollections)
                            {
                                vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                {
                    if (vendaDetalhes.manufaturadoProduto == 'S')
                    {
                        try
                        {
                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                            foreach (var item in produtosCollections)
                            {
                                decimal qtd1 = item.quantidadeProdutoManufaturado;
                                decimal totalEst = qtd1 * qtd;

                                if (item.estoqueProduto < totalEst)
                                {
                                    MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                            }

                            foreach (var item in produtosCollections)
                            {
                                vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                if (vendaDetalhesManufaturado.estoqueProduto < qtd)
                                {
                                    MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }

                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto + (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * qtd);

                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                //APOS VERIFICAR ESTOQUE E ATUALIZAR SE FOR O CASO, O PRODUTO E EXCLUIDO
                Convert.ToInt32(vendaDetalhesNegocios.ExcluirItensPedido(idVenda));
            }
            else
            {
                MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                txtCodigo.Text = txtCodigo.Text;
            }
        }

        private void PedidoItemExc()
        {
            if (dgCupom.Rows.Count > 0)
            {
                frmPdvDelete objDelete = new frmPdvDelete();
                objDelete.ShowDialog();

                if (objDelete.codDel != 0 && objDelete.codDel <= dgCupom.Rows.Count)
                {
                    dgCupom.Rows.Clear();
                    CarregarDataGrid();

                    if (Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[7].Value) == 6)
                    {
                        string descricao = "";
                        VendaDetalhesNegocios itemExt = new VendaDetalhesNegocios();

                        int idVendaExtra = Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[9].Value);

                        VendaDetalhesItemExtraCollections item1 = itemExt.buscarProdItensExtraAtualizar(idVendaExtra);

                        foreach (var itemA in item1)
                        {
                            VendaDetalhesCollections item2 = itemExt.buscarProdutoItensExtra(itemA.IdVendasDetalhesExtra);
                            //VERIFICA SE E O ULTIMO EXTRA A EXCLUIR E ENTAO LIMPA A DESCRIÇÃO DO EXTRA
                            if (item2.Count == 0)
                            {
                                descricao = "";
                                itemExt.AtualizarItemExtra(descricao, itemA.idVendasDetalhesItem);
                            }
                            else
                            {
                                foreach (var itemExtra in item2)
                                {
                                    descricao += itemExtra.descricaoProduto + " = " + itemExtra.quantidadeVendaDetalhes + " / ";
                                    itemExt.AtualizarItemExtra(descricao, itemA.idVendasDetalhesItem);
                                }
                            }
                        }
                    }
                    if (Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[7].Value) != 3 && Convert.ToInt32(dgCupom.Rows[objDelete.codDel - 1].Cells[7].Value) != 4)
                    {
                        excluirItem(objDelete.codDel);
                    }
                    else
                    {
                        frmPermissaoAdm objPermissao = new frmPermissaoAdm();
                        DialogResult permissao = objPermissao.ShowDialog();

                        if (permissao == DialogResult.Yes)
                        {
                            excluirItem(objDelete.codDel);
                        }
                        else
                        {
                            MessageBox.Show("E necessário Autorização de um Administrador para efetuar Retiradas.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Item Inválido. Digite um item valido para Excluir", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCodigo.Focus();
                }
            }
            else
            {
                MessageBox.Show("Não existem itens para Excluir", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void excluirItem(int codDel)
        {
            int idVenda = Convert.ToInt32(dgCupom.Rows[codDel - 1].Cells[9].Value);
            excluirItemUpEstoque(Convert.ToInt32(dgCupom.Rows[codDel - 1].Cells[2].Value.ToString().Substring(0, 6)), Convert.ToDecimal(dgCupom.Rows[codDel - 1].Cells[1].Value.ToString().Trim(new char[] { ' ', 'x' })), idVenda);

            VendaDetalhesItemExtraCollections itemExtra = vendaDetalhesNegocios.buscarItensExtra(idVenda);

            foreach (var itemEx in itemExtra)
            {
                VendaDetalhesCollections itemProd = vendaDetalhesNegocios.buscarProdutoItensExtra(itemEx.IdVendasDetalhesExtra);
                foreach (var itemP in itemProd)
                {
                    excluirItemUpEstoque(itemP.codigoProduto, itemP.quantidadeVendaDetalhes, itemEx.IdVendasDetalhesExtra);
                }
            }

            dgCupom.Rows.RemoveAt(codDel - 1);

            dgCupom.Rows.Clear();
            CarregarDataGrid();

            txtItens.Text = dgCupom.Rows.Count.ToString();

            txtTotalGeral.Text = Generic.totalGeralDgv(dgCupom).ToString();
        }

        private void CarregarDataGrid()
        {
            int i = 1;

            VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();
            VendaDetalhesCollections vendaDetalhesCollections = vendaDetalhesNegocios.consulta(numeroVenda);

            foreach (var item in vendaDetalhesCollections)
            {
                dgCupom.Rows.Add(Generic.formatItens(i) + i, "    " + Convert.ToInt32(item.quantidadeVendaDetalhes) + " x", Generic.formatCodigo(item.codigoProduto.ToString()) + "        R$ " + item.valorUnitarioVendaDetalhes, item.descricaoProduto, "R$ " + item.valorTotalVendaDetalhes, item.extraVendaDetalhes, item.especialVendaDetalhes, item.statusVendaDetalhes, item.dataEnvioVendaDetalhes);
                dgCupom.FirstDisplayedScrollingRowIndex = dgCupom.Rows.Count - 1;

                nomeAtendente = item.atendenteVenda;

                i++;
            }
        }

        private void checaBairro()
        {
            BairroNegocios bairroNegocios = new BairroNegocios();
            txtValorFrete.Text = bairroNegocios.ConsultarTaxaBairro(bairro).ToString("N");
        }

        private void pesquisaClienteTelefone(string nome)
        {
            if (numeroVenda == "0")
            {
                dgcDadosCliente.AutoGenerateColumns = false;
                dgcDadosCliente.DataSource = null;

                ClientesCollections clientesCollections = clientesNegocios.ConsultarPorTelefone(txtTelefone.Text);

                if (clientesCollections.Count > 0)
                {
                    if (clientesCollections.Count > 1)
                    {
                        gbDadosCliente.Visible = false;
                        gbPontoReferencia.Visible = false;

                        dgcDadosCliente.Visible = true;
                        dgcDadosCliente.DataSource = clientesCollections;
                        dgcDadosCliente.Focus();
                    }
                    else
                    {
                        foreach (var item in clientesCollections)
                        {
                            cliente.idCliente = item.idCliente;
                            cliente.nomeCliente = item.nomeCliente;
                            cliente.telefoneCliente = item.telefoneCliente;
                            cliente.celularCliente = item.celularCliente;
                            cliente.enderecoCliente = item.enderecoCliente;
                            cliente.complementoCliente = item.complementoCliente;
                            cliente.bairroCliente = item.bairroCliente;
                            cliente.pontoReferenciaCliente = item.pontoReferenciaCliente;
                        }

                        delivery.idCliente = cliente.idCliente;

                        lblNomeCliente.Text = cliente.nomeCliente;
                        lblEnderecoCliente.Text = cliente.enderecoCliente + " " + cliente.complementoCliente + " - " + cliente.bairroCliente;
                        lblPontoReferencia.Text = cliente.pontoReferenciaCliente;

                        bairro = cliente.bairroCliente;
                        checaBairro();

                        abrePedidos(tipoAtendimento);
                    }
                }
                else
                {
                    MessageBox.Show("Cliente não encontrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    frmCadClientes objFrmCadClientes = new frmCadClientes(AcaoCRUD.Inserir, null);
                    DialogResult resultado = objFrmCadClientes.ShowDialog();
                    if (resultado == DialogResult.Yes)
                    {
                        txtTelefone.Focus();
                        return;
                    }

                    txtTelefone.Text = string.Empty;
                    txtTelefone.Focus();
                }
            }
            else
            {
                MessageBox.Show("E necessário fechar ou cancelar o Pedido em Aberto para Abrir um novo Pedido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void getCliente()
        {
            clienteSelecionado = dgcDadosCliente.SelectedRows[0].DataBoundItem as Clientes;

            lblNomeCliente.Text = clienteSelecionado.nomeCliente;
            lblEnderecoCliente.Text = clienteSelecionado.enderecoCliente + " " + clienteSelecionado.complementoCliente + " - " + clienteSelecionado.bairroCliente;
            lblPontoReferencia.Text = clienteSelecionado.pontoReferenciaCliente;
            bairro = clienteSelecionado.bairroCliente;
            checaBairro();
            delivery.idCliente = clienteSelecionado.idCliente;
            dgcDadosCliente.Visible = false;
            gbDadosCliente.Visible = true;
            gbPontoReferencia.Visible = true;
        }

        private void carregaFormaPagamento()
        {
            FormaPagamentoNegocios formaPagamentoNegocios = new FormaPagamentoNegocios();
            FormaPagamentoCollections formaPagamentoCollections = formaPagamentoNegocios.ConsultarDescricao("");

            cbTipoPagamento.DataSource = null;
            cbTipoPagamento.DataSource = formaPagamentoCollections;
        }

        private void selecionarTipoCartao()
        {
            if (cbTipoPagamento.Text == "CARTÃO" || cbTipoPagamento.Text == "CARTAO")
            {
                frmPdvTipoCartao objTipoCartao = new frmPdvTipoCartao();
                objTipoCartao.ShowDialog();

                if (objTipoCartao.sair != "N")
                {
                    MessageBox.Show("Nenhum Tipo de Cartão foi Selecionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cbTipoPagamento.Focus();
                    return;
                }
                else
                {
                    DialogResult yes = MessageBox.Show("O Cartão seleciona e o " + objTipoCartao.nomeCartao + ". Deseja continuar?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    if (yes == DialogResult.Yes)
                    {
                        vendaCabecalhoFinaliza.idCartao = objTipoCartao.idCartao;
                    }
                    else
                    {
                        cbTipoPagamento.Focus();
                    }
                }
            }
            else
            {
                return;
            }
        }

        private void limparCampos()
        {
            dgCupom.Rows.Clear();
            txtItens.Text = "";
            txtQuantidade.Text = "1";
            txtTotalGeral.Text = "0,00";
            txtValorTotal.Text = "0,00";
            txtValorUnitario.Text = "0,00";
            lblNomeCliente.Text = string.Empty;
            lblEnderecoCliente.Text = string.Empty;
            lblPontoReferencia.Text = string.Empty;
            txtValorFrete.Text = "0,00";
            txtTelefone.Text = String.Empty;
            txtTotalGeralPedido.Text = "0,00";
            cbTipoPagamento.Refresh();
            txtTrocoPara.Text = "0,00";
            txtValorTroco.Text = "0,00";
            ckbTroco.Checked = false;
            gbTroco.Enabled = false;

            numeroVenda = "0";
            txtTelefone.Focus();
        }

        private void enviarPedido()
        {
            DialogResult yes = MessageBox.Show("Deseja Realmente Finalizar o Pedido ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (yes == DialogResult.Yes)
            {
                selecionarTipoCartao();

                vendaCabecalhoFinaliza.idVendaCabecalho = Convert.ToInt32(numeroVenda);
                vendaCabecalhoFinaliza.idCliente = delivery.idCliente;
                vendaCabecalhoFinaliza.valorVendaCabecalho = Convert.ToDecimal(txtTotalGeral.Text);
                vendaCabecalhoFinaliza.totalVendaCabecalho = Convert.ToDecimal(txtTotalGeralPedido.Text);
                vendaCabecalhoFinaliza.descontoVendaCabecalho = 0;
                vendaCabecalhoFinaliza.statusVendaCabecalho = 1;
                vendaCabecalhoFinaliza.usuarioId = frmLogin.usuariosLogin.usuarioId;
                vendaCabecalhoFinaliza.atendenteVendaCabecalho = nomeAtendente;
                vendaCabecalhoFinaliza.horaVendaCabecalho = DateTime.Now;

                vendaCabecalhoFinaliza.valorVCTipoPagamento = Convert.ToDecimal(txtTotalGeralPedido.Text);
                vendaCabecalhoFinaliza.descricaoVCTipoPagamento = cbTipoPagamento.Text;

                //INSERIR O TIPO DE PAGAMENTO
                int ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.inserirTipoPagamento(vendaCabecalhoFinaliza));

                //ENVIAR PEDIDO PARA PRODUÇÃO
                ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.enviarVendaDelivery(vendaCabecalhoFinaliza));

                //IMPRIMIR PEDIDO
                if (Generic.checaImpressao() == 3)
                {
                    vendaCabecalhoFinaliza.nomeAtendimento = "DELIVERY";
                    vendaCabecalhoFinaliza.atendenteVendaCabecalho = nomeAtendente;

                    frmRltImprimirCupom objFrmImpCupom = new frmRltImprimirCupom(vendaCabecalhoFinaliza, imprimiCupom.imprimePedido, lblNomeCliente.Text);
                    objFrmImpCupom.ShowDialog();

                    delivery.statusEntrega = 2;
                }
                else
                    delivery.statusEntrega = 1;
                
                //MUDAR O STATUS DE CADA ITEM
                VendaDetalhesNegocios vdn = new VendaDetalhesNegocios();
                ckretorno = Convert.ToInt32(vdn.finalizarItens(Convert.ToInt32(numeroVenda), Generic.checaImpressao()));

                delivery.idVendaCabecalho = Convert.ToInt32(numeroVenda);
                delivery.idFormaPagamento = Convert.ToInt32(cbTipoPagamento.SelectedValue);
                delivery.trocoPara = Convert.ToDecimal(txtTrocoPara.Text);
                delivery.troco = Convert.ToDecimal(txtValorTroco.Text);
                delivery.valorFrete = Convert.ToDecimal(txtValorFrete.Text);
                

                ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.atualizaDelivery(delivery));

                MessageBox.Show("Pedido Finalizado com Sucesso", "Finalizando Venda", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                limparCampos();
            }
            else
            {
                return;
            }
        }

        #endregion

        #region BOTÕES

        private void btnPesquisarProdutos_Click(object sender, EventArgs e)
        {
            frmCadProdutos pesqProd = new frmCadProdutos();
            pesqProd.pesqProduto(txtCodigo);
        }

        private void btnCancelarItem_Click(object sender, EventArgs e)
        {
            PedidoItemExc();
            txtCodigo.Focus();
        }

        private void btnAddQtd_Click(object sender, EventArgs e)
        {
            txtQuantidade.ReadOnly = false;
            txtQuantidade.Text = "";
            txtQuantidade.Focus();
        }

        private void btnEspecial_Click(object sender, EventArgs e)
        {
            if (dgCupom.Rows.Count > 0)
            {
                frmPdvEspecial objFrmEspecial = new frmPdvEspecial();
                objFrmEspecial.ShowDialog();

                if (objFrmEspecial.sair != "N")
                {
                    MessageBox.Show("Especial Cancelado.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    if (Convert.ToInt32(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[7].Value) == 1 || Convert.ToInt32(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[7].Value) == 2)
                    {
                        try
                        {
                            string descricao = Convert.ToString(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[6].Value += objFrmEspecial.descricaoEspecial + " / ");

                            // ATUALIZAR ITENS COM ESPECIAL

                            vendaDetalhesNegocios.AtualizarItemEspecial(descricao, Convert.ToInt32(dgCupom.Rows[objFrmEspecial.itemEspecial - 1].Cells[9].Value));
                            dgCupom.Rows.Clear();
                            vendaDetalhes.especialVendaDetalhes = string.Empty;
                            CarregarDataGrid();

                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Não foi possivel Adicionar o Especial.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Não e possivel atribuir Especial para esse Produto. Pedido Entregue", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            else
            {
                MessageBox.Show("Não existem itens para atribuir a esse Extra", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void btnCancelarPedido_Click(object sender, EventArgs e)
        {
            if (dgCupom.Rows.Count > 0)
            {
                DialogResult cancelPedido = MessageBox.Show("Deseja Cancelar esse Pedido?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (cancelPedido == DialogResult.Yes)
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();
                    VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();

                    try
                    {
                        vendaCabecalho.idVendaCabecalho = Convert.ToInt32(numeroVenda);
                        Convert.ToInt32(vendaCabecalhoNegocios.cancelarVenda(vendaCabecalho));

                        foreach (DataGridViewRow rows in dgCupom.Rows)
                        {
                            vendaDetalhes.codigoProduto = Convert.ToInt32(rows.Cells[2].Value.ToString().Substring(0, 6));
                            vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(rows.Cells[1].Value.ToString().Trim(new char[] { ' ', 'x' }));

                            excluirItemUpEstoque(vendaDetalhes.codigoProduto, vendaDetalhes.quantidadeVendaDetalhes, Convert.ToInt32(numeroVenda));
                        }

                        MessageBox.Show("Pedido Cancelado com Sucesso", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        limparCampos();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Não existem Pedido para Cancelar", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }
        }

        private void btnVisualizarPedido_Click(object sender, EventArgs e)
        {
            frmVerPedidos objVerPedidos = new frmVerPedidos();
            objVerPedidos.ShowDialog();
        }

        private void btnExtra_Click(object sender, EventArgs e)
        {
            PedidoExtraAdd();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            //frmDeliveryImprimir pdvImprimir = new frmDeliveryImprimir();
            //pdvImprimir.ShowDialog();
        }

        private void btnFinalizarPedido_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgCupom.RowCount > 0)
                {
                    if (Convert.ToDecimal(txtTrocoPara.Text) >= Convert.ToDecimal(txtTotalGeralPedido.Text))
                    {
                        enviarPedido();
                    }
                    else
                    {
                        if (cbTipoPagamento.Text == "CARTÃO" || cbTipoPagamento.Text == "CARTAO")
                        {
                            enviarPedido();
                        }
                        else
                        {
                            MessageBox.Show("Não e possivel Enviar Pedido. O Valor do Troco deve ser Maior ou igual ao valor do Pedido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            txtTrocoPara.Focus();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Não e possivel Enviar Pedido. Não existem itens para esse Pedido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtTelefone.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não e possivel Enviar Pedido" + ex, "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        #endregion

        #region MENU STRIP

        private void retiradasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadRetiradas objRetiradas = new frmCadRetiradas();
            objRetiradas.ShowDialog();
        }

        private void cadastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadClienteFront objCliente = new frmCadClienteFront();
            objCliente.ShowDialog();
        }

        #endregion
    }
}
