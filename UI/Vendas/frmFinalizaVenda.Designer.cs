﻿namespace UI
{
    partial class frmFinalizaVenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFinalizaVenda));
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.txtTotalPedido = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.txtDinheiro = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.lblTipoCartao = new DevComponents.DotNetBar.LabelX();
            this.txtCartao = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.panelEx4 = new DevComponents.DotNetBar.PanelEx();
            this.txtTicket = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.panelEx5 = new DevComponents.DotNetBar.PanelEx();
            this.txtDesconto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.line1 = new DevComponents.DotNetBar.Controls.Line();
            this.panelEx6 = new DevComponents.DotNetBar.PanelEx();
            this.txtTotalPago = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.panelEx7 = new DevComponents.DotNetBar.PanelEx();
            this.txtTroco = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.lblAtendimento = new DevComponents.DotNetBar.LabelX();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.btnAReceber = new DevComponents.DotNetBar.ButtonX();
            this.panelEx1.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.panelEx4.SuspendLayout();
            this.panelEx5.SuspendLayout();
            this.panelEx6.SuspendLayout();
            this.panelEx7.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX1.Location = new System.Drawing.Point(10, 54);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(134, 20);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX1.TabIndex = 9;
            this.labelX1.Text = "TOTAL A PAGAR";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.txtTotalPedido);
            this.panelEx1.Controls.Add(this.labelX2);
            this.panelEx1.Location = new System.Drawing.Point(10, 78);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(478, 58);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.Color = System.Drawing.Color.DeepSkyBlue;
            this.panelEx1.Style.BackColor2.Color = System.Drawing.Color.Blue;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 8;
            // 
            // txtTotalPedido
            // 
            this.txtTotalPedido.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTotalPedido.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtTotalPedido.Border.Class = "TextBoxBorder";
            this.txtTotalPedido.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalPedido.Font = new System.Drawing.Font("Arial", 23.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPedido.ForeColor = System.Drawing.Color.Green;
            this.txtTotalPedido.Location = new System.Drawing.Point(203, 9);
            this.txtTotalPedido.Multiline = true;
            this.txtTotalPedido.Name = "txtTotalPedido";
            this.txtTotalPedido.PreventEnterBeep = true;
            this.txtTotalPedido.ReadOnly = true;
            this.txtTotalPedido.Size = new System.Drawing.Size(265, 40);
            this.txtTotalPedido.TabIndex = 4;
            this.txtTotalPedido.Text = "0,00";
            this.txtTotalPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX2.Location = new System.Drawing.Point(12, 17);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(161, 29);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "TOTAL DO PEDIDO";
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.txtDinheiro);
            this.panelEx2.Controls.Add(this.labelX3);
            this.panelEx2.Location = new System.Drawing.Point(10, 167);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(478, 58);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.Color = System.Drawing.Color.White;
            this.panelEx2.Style.BackColor2.Color = System.Drawing.Color.White;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.StyleMouseOver.BackColor1.Color = System.Drawing.Color.PaleGreen;
            this.panelEx2.StyleMouseOver.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panelEx2.TabIndex = 0;
            // 
            // txtDinheiro
            // 
            this.txtDinheiro.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.txtDinheiro.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtDinheiro.Border.Class = "TextBoxBorder";
            this.txtDinheiro.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDinheiro.Font = new System.Drawing.Font("Arial", 23F, System.Drawing.FontStyle.Bold);
            this.txtDinheiro.ForeColor = System.Drawing.Color.Blue;
            this.txtDinheiro.Location = new System.Drawing.Point(210, 9);
            this.txtDinheiro.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.txtDinheiro.Multiline = true;
            this.txtDinheiro.Name = "txtDinheiro";
            this.txtDinheiro.PreventEnterBeep = true;
            this.txtDinheiro.Size = new System.Drawing.Size(258, 40);
            this.txtDinheiro.TabIndex = 1;
            this.txtDinheiro.Text = "0,00";
            this.txtDinheiro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDinheiro.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtDinheiro_MouseClick);
            this.txtDinheiro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDinheiro_KeyUp);
            this.txtDinheiro.Leave += new System.EventHandler(this.txtDinheiro_Leave);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX3.Location = new System.Drawing.Point(12, 17);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(122, 29);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "DINHEIRO - F1";
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX4.Location = new System.Drawing.Point(10, 142);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(285, 20);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX4.TabIndex = 10;
            this.labelX4.Text = "INFORME OS VALORES RECEBIDOS";
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.lblTipoCartao);
            this.panelEx3.Controls.Add(this.txtCartao);
            this.panelEx3.Controls.Add(this.labelX5);
            this.panelEx3.Location = new System.Drawing.Point(10, 222);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(478, 86);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.Color = System.Drawing.Color.White;
            this.panelEx3.Style.BackColor2.Color = System.Drawing.Color.White;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.StyleMouseDown.BackColor1.Color = System.Drawing.Color.PaleGreen;
            this.panelEx3.StyleMouseDown.BackColor2.Color = System.Drawing.Color.PaleGreen;
            this.panelEx3.TabIndex = 1;
            // 
            // lblTipoCartao
            // 
            // 
            // 
            // 
            this.lblTipoCartao.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTipoCartao.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCartao.ForeColor = System.Drawing.Color.Blue;
            this.lblTipoCartao.Location = new System.Drawing.Point(13, 53);
            this.lblTipoCartao.Name = "lblTipoCartao";
            this.lblTipoCartao.Size = new System.Drawing.Size(424, 29);
            this.lblTipoCartao.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.lblTipoCartao.TabIndex = 3;
            this.lblTipoCartao.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtCartao
            // 
            this.txtCartao.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.txtCartao.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtCartao.Border.Class = "TextBoxBorder";
            this.txtCartao.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCartao.Font = new System.Drawing.Font("Arial", 23F, System.Drawing.FontStyle.Bold);
            this.txtCartao.ForeColor = System.Drawing.Color.Blue;
            this.txtCartao.Location = new System.Drawing.Point(210, 12);
            this.txtCartao.Multiline = true;
            this.txtCartao.Name = "txtCartao";
            this.txtCartao.PreventEnterBeep = true;
            this.txtCartao.Size = new System.Drawing.Size(258, 40);
            this.txtCartao.TabIndex = 1;
            this.txtCartao.Text = "0,00";
            this.txtCartao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCartao.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtCartao_MouseClick);
            this.txtCartao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCartao_KeyDown);
            this.txtCartao.Leave += new System.EventHandler(this.txtCartao_Leave);
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX5.Location = new System.Drawing.Point(12, 12);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(112, 29);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX5.TabIndex = 2;
            this.labelX5.Text = "CARTÃO - F2";
            // 
            // panelEx4
            // 
            this.panelEx4.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx4.Controls.Add(this.txtTicket);
            this.panelEx4.Controls.Add(this.labelX6);
            this.panelEx4.Location = new System.Drawing.Point(10, 307);
            this.panelEx4.Name = "panelEx4";
            this.panelEx4.Size = new System.Drawing.Size(478, 58);
            this.panelEx4.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx4.Style.BackColor1.Color = System.Drawing.Color.White;
            this.panelEx4.Style.BackColor2.Color = System.Drawing.Color.White;
            this.panelEx4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx4.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx4.Style.GradientAngle = 90;
            this.panelEx4.StyleMouseOver.BackColor1.Color = System.Drawing.Color.PaleGreen;
            this.panelEx4.StyleMouseOver.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panelEx4.TabIndex = 2;
            // 
            // txtTicket
            // 
            this.txtTicket.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.txtTicket.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtTicket.Border.Class = "TextBoxBorder";
            this.txtTicket.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTicket.Font = new System.Drawing.Font("Arial", 23F, System.Drawing.FontStyle.Bold);
            this.txtTicket.ForeColor = System.Drawing.Color.Blue;
            this.txtTicket.Location = new System.Drawing.Point(210, 9);
            this.txtTicket.Multiline = true;
            this.txtTicket.Name = "txtTicket";
            this.txtTicket.PreventEnterBeep = true;
            this.txtTicket.Size = new System.Drawing.Size(258, 40);
            this.txtTicket.TabIndex = 1;
            this.txtTicket.Text = "0,00";
            this.txtTicket.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTicket.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtTicket_MouseClick);
            this.txtTicket.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTicket_KeyUp);
            this.txtTicket.Leave += new System.EventHandler(this.txtTicket_Leave);
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX6.Location = new System.Drawing.Point(12, 17);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(100, 29);
            this.labelX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX6.TabIndex = 2;
            this.labelX6.Text = "TICKET - F3";
            // 
            // panelEx5
            // 
            this.panelEx5.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx5.Controls.Add(this.txtDesconto);
            this.panelEx5.Controls.Add(this.labelX7);
            this.panelEx5.Location = new System.Drawing.Point(10, 362);
            this.panelEx5.Name = "panelEx5";
            this.panelEx5.Size = new System.Drawing.Size(478, 58);
            this.panelEx5.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx5.Style.BackColor1.Color = System.Drawing.Color.White;
            this.panelEx5.Style.BackColor2.Color = System.Drawing.Color.White;
            this.panelEx5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx5.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx5.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx5.Style.GradientAngle = 90;
            this.panelEx5.StyleMouseOver.BackColor1.Color = System.Drawing.Color.PaleGreen;
            this.panelEx5.StyleMouseOver.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panelEx5.TabIndex = 3;
            // 
            // txtDesconto
            // 
            this.txtDesconto.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.txtDesconto.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtDesconto.Border.Class = "TextBoxBorder";
            this.txtDesconto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDesconto.Font = new System.Drawing.Font("Arial", 23F, System.Drawing.FontStyle.Bold);
            this.txtDesconto.ForeColor = System.Drawing.Color.Blue;
            this.txtDesconto.Location = new System.Drawing.Point(210, 9);
            this.txtDesconto.Multiline = true;
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.PreventEnterBeep = true;
            this.txtDesconto.Size = new System.Drawing.Size(258, 40);
            this.txtDesconto.TabIndex = 1;
            this.txtDesconto.Text = "0,00";
            this.txtDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDesconto.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtDesconto_MouseClick);
            this.txtDesconto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDesconto_KeyUp);
            this.txtDesconto.Leave += new System.EventHandler(this.txtDesconto_Leave);
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX7.Location = new System.Drawing.Point(12, 17);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(135, 29);
            this.labelX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX7.TabIndex = 2;
            this.labelX7.Text = "DESCONTO - F4";
            // 
            // line1
            // 
            this.line1.DashOffset = 1F;
            this.line1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.line1.Location = new System.Drawing.Point(10, 423);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(478, 20);
            this.line1.StartLineCap = DevComponents.DotNetBar.Controls.eLineEndType.Circle;
            this.line1.TabIndex = 11;
            // 
            // panelEx6
            // 
            this.panelEx6.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx6.Controls.Add(this.txtTotalPago);
            this.panelEx6.Controls.Add(this.labelX8);
            this.panelEx6.Location = new System.Drawing.Point(10, 445);
            this.panelEx6.Name = "panelEx6";
            this.panelEx6.Size = new System.Drawing.Size(478, 58);
            this.panelEx6.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx6.Style.BackColor1.Color = System.Drawing.Color.PowderBlue;
            this.panelEx6.Style.BackColor2.Color = System.Drawing.Color.DodgerBlue;
            this.panelEx6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx6.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx6.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx6.Style.GradientAngle = 90;
            this.panelEx6.TabIndex = 6;
            // 
            // txtTotalPago
            // 
            this.txtTotalPago.BackColor = System.Drawing.Color.PowderBlue;
            // 
            // 
            // 
            this.txtTotalPago.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtTotalPago.Border.Class = "TextBoxBorder";
            this.txtTotalPago.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTotalPago.Font = new System.Drawing.Font("Arial", 23.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPago.ForeColor = System.Drawing.Color.Green;
            this.txtTotalPago.Location = new System.Drawing.Point(210, 9);
            this.txtTotalPago.Multiline = true;
            this.txtTotalPago.Name = "txtTotalPago";
            this.txtTotalPago.PreventEnterBeep = true;
            this.txtTotalPago.ReadOnly = true;
            this.txtTotalPago.Size = new System.Drawing.Size(258, 40);
            this.txtTotalPago.TabIndex = 4;
            this.txtTotalPago.Text = "0,00";
            this.txtTotalPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX8.Location = new System.Drawing.Point(12, 17);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(115, 29);
            this.labelX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX8.TabIndex = 2;
            this.labelX8.Text = "TOTAL PAGO";
            // 
            // panelEx7
            // 
            this.panelEx7.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx7.Controls.Add(this.txtTroco);
            this.panelEx7.Controls.Add(this.labelX9);
            this.panelEx7.Location = new System.Drawing.Point(10, 505);
            this.panelEx7.Name = "panelEx7";
            this.panelEx7.Size = new System.Drawing.Size(478, 58);
            this.panelEx7.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panelEx7.Style.BackColor2.Color = System.Drawing.Color.Red;
            this.panelEx7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx7.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx7.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx7.Style.GradientAngle = 90;
            this.panelEx7.TabIndex = 7;
            // 
            // txtTroco
            // 
            this.txtTroco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // 
            // 
            this.txtTroco.Border.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtTroco.Border.Class = "TextBoxBorder";
            this.txtTroco.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTroco.Font = new System.Drawing.Font("Arial", 23.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTroco.ForeColor = System.Drawing.Color.Red;
            this.txtTroco.Location = new System.Drawing.Point(210, 9);
            this.txtTroco.Multiline = true;
            this.txtTroco.Name = "txtTroco";
            this.txtTroco.PreventEnterBeep = true;
            this.txtTroco.ReadOnly = true;
            this.txtTroco.Size = new System.Drawing.Size(258, 40);
            this.txtTroco.TabIndex = 4;
            this.txtTroco.Text = "0,00";
            this.txtTroco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX9
            // 
            this.labelX9.AutoSize = true;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX9.Location = new System.Drawing.Point(12, 17);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(65, 29);
            this.labelX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.labelX9.TabIndex = 2;
            this.labelX9.Text = "TROCO";
            // 
            // lblAtendimento
            // 
            this.lblAtendimento.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.lblAtendimento.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAtendimento.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendimento.ForeColor = System.Drawing.Color.Blue;
            this.lblAtendimento.Location = new System.Drawing.Point(10, 9);
            this.lblAtendimento.Name = "lblAtendimento";
            this.lblAtendimento.Size = new System.Drawing.Size(478, 39);
            this.lblAtendimento.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.lblAtendimento.TabIndex = 12;
            this.lblAtendimento.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnFechar.Location = new System.Drawing.Point(10, 569);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(104, 43);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 5;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>Sair (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.shopping_cart_accept;
            this.btnSalvar.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnSalvar.Location = new System.Drawing.Point(313, 569);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(175, 43);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 4;
            this.btnSalvar.Text = "Finalizar Venda (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnAReceber
            // 
            this.btnAReceber.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAReceber.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAReceber.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAReceber.Image = global::UI.Properties.Resources.formaPagamento;
            this.btnAReceber.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnAReceber.Location = new System.Drawing.Point(120, 569);
            this.btnAReceber.Name = "btnAReceber";
            this.btnAReceber.Size = new System.Drawing.Size(187, 43);
            this.btnAReceber.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAReceber.TabIndex = 13;
            this.btnAReceber.Text = "A Receber (F10)";
            this.btnAReceber.Click += new System.EventHandler(this.btnAReceber_Click);
            // 
            // frmFinalizaVenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 615);
            this.Controls.Add(this.btnAReceber);
            this.Controls.Add(this.lblAtendimento);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.panelEx7);
            this.Controls.Add(this.panelEx6);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.panelEx5);
            this.Controls.Add(this.panelEx4);
            this.Controls.Add(this.panelEx3);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.panelEx2);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.labelX1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFinalizaVenda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Finalizar Pedido";
            this.Load += new System.EventHandler(this.frmFinalizaVenda_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmFinalizaVenda_KeyUp);
            this.panelEx1.ResumeLayout(false);
            this.panelEx1.PerformLayout();
            this.panelEx2.ResumeLayout(false);
            this.panelEx2.PerformLayout();
            this.panelEx3.ResumeLayout(false);
            this.panelEx3.PerformLayout();
            this.panelEx4.ResumeLayout(false);
            this.panelEx4.PerformLayout();
            this.panelEx5.ResumeLayout(false);
            this.panelEx5.PerformLayout();
            this.panelEx6.ResumeLayout(false);
            this.panelEx6.PerformLayout();
            this.panelEx7.ResumeLayout(false);
            this.panelEx7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.PanelEx panelEx4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTicket;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.PanelEx panelEx5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDesconto;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalPedido;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTotalPago;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTroco;
        private DevComponents.DotNetBar.Controls.Line line1;
        private DevComponents.DotNetBar.PanelEx panelEx6;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.PanelEx panelEx7;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCartao;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDinheiro;
        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private DevComponents.DotNetBar.LabelX lblAtendimento;
        private DevComponents.DotNetBar.LabelX lblTipoCartao;
        private DevComponents.DotNetBar.ButtonX btnAReceber;
    }
}