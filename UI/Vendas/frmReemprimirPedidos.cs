﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;
using Relatorios;

namespace UI
{
    public partial class frmReemprimirPedidos : Form
    {
        VendaCabecalho vendaCabecalho = new VendaCabecalho();
        VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();

        public frmReemprimirPedidos()
        {
            InitializeComponent();

            dgvReemprimirPedidos.AutoGenerateColumns = false;
        }

        private void frmReemprimirPedidos_Load(object sender, EventArgs e)
        {
            frmCarregaPedidosPrint frm1 = new frmCarregaPedidosPrint();
            frm1.ShowDialog();

            dgvReemprimirPedidos.DataSource = frm1.vendaCabecalhoCollections;
        }

        private void dgvReemprimirPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvReemprimirPedidos.Columns[e.ColumnIndex].Name == "btnImprimir")
            {
                imprimirPedido();
            }
        }

        private void dgvReemprimirPedidos_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    imprimirPedido();
                    break;
            }
        }

        private void imprimirPedido()
        {
            try
            {
                dgvReemprimirPedidos.Refresh();

                VendaCabecalho vendas = dgvReemprimirPedidos.SelectedRows[0].DataBoundItem as VendaCabecalho;

                if (vendas.nomeAtendimento == "DELIVERY")
                {
                    frmRltImprimirCupom objImprimeCupom = new frmRltImprimirCupom(vendas, imprimiCupom.reimprimeDelivery, frmLogin.meuLanche);
                    objImprimeCupom.ShowDialog();    
                }
                else
                {
                    frmRltImprimirCupom objImprimeCupom = new frmRltImprimirCupom(vendas, imprimiCupom.reimprime, frmLogin.meuLanche);
                    objImprimeCupom.ShowDialog();   
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ao Imprimir Pedido: "+ ex, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
