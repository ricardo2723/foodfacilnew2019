﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmCarregaPedidosPrint : Form
    {
        VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
        public VendaCabecalhoCollections vendaCabecalhoCollections = new VendaCabecalhoCollections();

        public frmCarregaPedidosPrint()
        {
            InitializeComponent();
        }

        private void frmCarregaPedidosPrint_Load(object sender, EventArgs e)
        {
            circularProgress1.IsRunning = true;
            bwPrintCupom.RunWorkerAsync();  
        }

        private void bwPrintCupom_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!bwPrintCupom.CancellationPending)
            {
                bwPrintCupom.ReportProgress(0, "Carregando Pedidos...");
                Thread.Sleep(500);
                carregaDgv();
                bwPrintCupom.CancelAsync();
            }
        }

        private void bwPrintCupom_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMenssagem.Text = e.UserState.ToString();
        }

        private void bwPrintCupom_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Close();
        }

        private void carregaDgv()
        {
            vendaCabecalhoCollections = vendaCabecalhoNegocios.reemprimeVenda();           
        }
    }
}
