﻿namespace UI
{
    partial class frmEntregaDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEntregaDelivery));
            this.dgvDelivery = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn();
            this.Column8 = new DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn();
            this.hora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabItem2 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tempoPedidos = new System.Windows.Forms.Timer(this.components);
            this.tabControl2 = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblValorPedido = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblAtendente = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnEnviarPedido = new DevComponents.DotNetBar.ButtonX();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMotivoCancelamento = new System.Windows.Forms.TextBox();
            this.lblMotivoCancelamento = new System.Windows.Forms.Label();
            this.lblBairro = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEntregador = new System.Windows.Forms.ComboBox();
            this.lblNumPedido = new System.Windows.Forms.Label();
            this.lblTempoEspera = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNomeCliente = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.lblEndercoCliente = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.lblPontoReferenciaCliente = new System.Windows.Forms.Label();
            this.dgvProdutos = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiProntoEntrega = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.lblValorPedidoEntrega = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblAtendenteEntrega = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblTempoTotalPedido = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblTempoEnvio = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblHoraPedido = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblNomeEntegador = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnFinalizarPedido = new DevComponents.DotNetBar.ButtonX();
            this.txtMotivoCancelamentoEspera = new System.Windows.Forms.TextBox();
            this.lblMotivoCancelamentoEspera = new System.Windows.Forms.Label();
            this.lblBairroEntrega = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblNumPedidoEntrega = new System.Windows.Forms.Label();
            this.lblTempoPedidoEspera = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblNomeClienteEntrega = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblEnderecoEntrega = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cbStatuEntrega = new System.Windows.Forms.ComboBox();
            this.lblPontoReferenciaEntrega = new System.Windows.Forms.Label();
            this.dgvDeliverySaiuEntrega = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaSaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempoEntr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiSaiuEntrega = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.tempoEntrega = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl2)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdutos)).BeginInit();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliverySaiuEntrega)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDelivery
            // 
            this.dgvDelivery.AllowUserToAddRows = false;
            this.dgvDelivery.AllowUserToDeleteRows = false;
            this.dgvDelivery.AllowUserToResizeColumns = false;
            this.dgvDelivery.AllowUserToResizeRows = false;
            this.dgvDelivery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDelivery.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDelivery.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.balloonTip1.SetBalloonCaption(this.dgvDelivery, "Atualizar Pedidos");
            this.balloonTip1.SetBalloonText(this.dgvDelivery, "Pressione F5 para Atualizar os Pedidos");
            this.dgvDelivery.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDelivery.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDelivery.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDelivery.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column7,
            this.Column8,
            this.hora,
            this.tempo});
            this.dgvDelivery.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvDelivery.Location = new System.Drawing.Point(-1, 0);
            this.dgvDelivery.MultiSelect = false;
            this.dgvDelivery.Name = "dgvDelivery";
            this.dgvDelivery.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvDelivery.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvDelivery.RowHeadersVisible = false;
            this.dgvDelivery.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvDelivery.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Yellow;
            this.dgvDelivery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvDelivery.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDelivery.Size = new System.Drawing.Size(1358, 247);
            this.dgvDelivery.StandardTab = true;
            this.dgvDelivery.TabIndex = 1;
            this.dgvDelivery.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDelivery_CellClick);
            this.dgvDelivery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvDelivery_KeyDown);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "idVendaCabecalho";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "NUM PED";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 130;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "nomeCliente";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Column2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column2.HeaderText = "NOME CLIENTE";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 400;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "bairroCliente";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Column5.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column5.HeaderText = "BAIRRO";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 250;
            // 
            // Column7
            // 
            this.Column7.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.Column7.BackgroundStyle.Class = "DataGridViewBorder";
            this.Column7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Column7.Culture = new System.Globalization.CultureInfo("pt-BR");
            this.Column7.DataPropertyName = "telefoneCliente";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Column7.HeaderText = "TELEFONE";
            this.Column7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Column7.Mask = "####-####";
            this.Column7.Name = "Column7";
            this.Column7.PasswordChar = '\0';
            this.Column7.ReadOnly = true;
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Column7.Text = "    -";
            this.Column7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Column7.Width = 150;
            // 
            // Column8
            // 
            this.Column8.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.Column8.BackgroundStyle.Class = "DataGridViewBorder";
            this.Column8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Column8.Culture = new System.Globalization.CultureInfo("pt-BR");
            this.Column8.DataPropertyName = "celularCliente";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Column8.HeaderText = "CELULAR";
            this.Column8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Column8.Mask = "#####-####";
            this.Column8.Name = "Column8";
            this.Column8.PasswordChar = '\0';
            this.Column8.ReadOnly = true;
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Column8.Text = "     -";
            this.Column8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Column8.Width = 150;
            // 
            // hora
            // 
            this.hora.DataPropertyName = "horaPedido";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.Format = "T";
            dataGridViewCellStyle7.NullValue = null;
            this.hora.DefaultCellStyle = dataGridViewCellStyle7;
            this.hora.HeaderText = "HORA";
            this.hora.Name = "hora";
            this.hora.ReadOnly = true;
            this.hora.Width = 130;
            // 
            // tempo
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Red;
            this.tempo.DefaultCellStyle = dataGridViewCellStyle8;
            this.tempo.HeaderText = "TEMPO";
            this.tempo.Name = "tempo";
            this.tempo.ReadOnly = true;
            this.tempo.Width = 130;
            // 
            // tabItem2
            // 
            this.tabItem2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.tabItem2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.tabItem2.Name = "tabItem2";
            this.tabItem2.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue;
            this.tabItem2.Text = "Pedidos Saiu Para Entrega";
            this.tabItem2.TextColor = System.Drawing.Color.Black;
            // 
            // tempoPedidos
            // 
            this.tempoPedidos.Enabled = true;
            this.tempoPedidos.Interval = 1000;
            this.tempoPedidos.Tick += new System.EventHandler(this.tempoPedidos_Tick);
            // 
            // tabControl2
            // 
            this.tabControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControl2.CanReorderTabs = true;
            this.tabControl2.ColorScheme.TabItemBackground = System.Drawing.Color.LightBlue;
            this.tabControl2.ColorScheme.TabItemBackground2 = System.Drawing.Color.LightBlue;
            this.tabControl2.ColorScheme.TabItemSelectedBackground = System.Drawing.Color.LightBlue;
            this.tabControl2.ColorScheme.TabItemSelectedBackground2 = System.Drawing.Color.LightBlue;
            this.tabControl2.ColorScheme.TabPanelBackground2 = System.Drawing.Color.LightBlue;
            this.tabControl2.ColorScheme.TabPanelBorder = System.Drawing.Color.LightBlue;
            this.tabControl2.Controls.Add(this.tabControlPanel3);
            this.tabControl2.Controls.Add(this.tabControlPanel1);
            this.tabControl2.ForeColor = System.Drawing.Color.Black;
            this.tabControl2.Location = new System.Drawing.Point(1, 1);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedTabFont = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.tabControl2.SelectedTabIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1368, 768);
            this.tabControl2.TabIndex = 0;
            this.tabControl2.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl2.Tabs.Add(this.tiProntoEntrega);
            this.tabControl2.Tabs.Add(this.tiSaiuEntrega);
            this.tabControl2.Text = "tabControl2";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.lblValorPedido);
            this.tabControlPanel3.Controls.Add(this.label25);
            this.tabControlPanel3.Controls.Add(this.lblAtendente);
            this.tabControlPanel3.Controls.Add(this.label15);
            this.tabControlPanel3.Controls.Add(this.label8);
            this.tabControlPanel3.Controls.Add(this.btnEnviarPedido);
            this.tabControlPanel3.Controls.Add(this.label6);
            this.tabControlPanel3.Controls.Add(this.txtMotivoCancelamento);
            this.tabControlPanel3.Controls.Add(this.lblMotivoCancelamento);
            this.tabControlPanel3.Controls.Add(this.lblBairro);
            this.tabControlPanel3.Controls.Add(this.label2);
            this.tabControlPanel3.Controls.Add(this.label1);
            this.tabControlPanel3.Controls.Add(this.cbEntregador);
            this.tabControlPanel3.Controls.Add(this.lblNumPedido);
            this.tabControlPanel3.Controls.Add(this.lblTempoEspera);
            this.tabControlPanel3.Controls.Add(this.label4);
            this.tabControlPanel3.Controls.Add(this.label3);
            this.tabControlPanel3.Controls.Add(this.lblNomeCliente);
            this.tabControlPanel3.Controls.Add(this.label5);
            this.tabControlPanel3.Controls.Add(this.label);
            this.tabControlPanel3.Controls.Add(this.lblEndercoCliente);
            this.tabControlPanel3.Controls.Add(this.label10);
            this.tabControlPanel3.Controls.Add(this.label7);
            this.tabControlPanel3.Controls.Add(this.cbStatus);
            this.tabControlPanel3.Controls.Add(this.lblPontoReferenciaCliente);
            this.tabControlPanel3.Controls.Add(this.dgvDelivery);
            this.tabControlPanel3.Controls.Add(this.dgvProdutos);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 30);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1368, 738);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.LightBlue;
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.LightBlue;
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.LightBlue;
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 1;
            this.tabControlPanel3.TabItem = this.tiProntoEntrega;
            // 
            // lblValorPedido
            // 
            this.lblValorPedido.BackColor = System.Drawing.Color.Transparent;
            this.lblValorPedido.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorPedido.ForeColor = System.Drawing.Color.Red;
            this.lblValorPedido.Location = new System.Drawing.Point(1134, 545);
            this.lblValorPedido.Name = "lblValorPedido";
            this.lblValorPedido.Size = new System.Drawing.Size(215, 23);
            this.lblValorPedido.TabIndex = 28;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(988, 545);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(140, 23);
            this.label25.TabIndex = 27;
            this.label25.Text = "Valor Pedido:";
            // 
            // lblAtendente
            // 
            this.lblAtendente.BackColor = System.Drawing.Color.Transparent;
            this.lblAtendente.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendente.ForeColor = System.Drawing.Color.Red;
            this.lblAtendente.Location = new System.Drawing.Point(1100, 505);
            this.lblAtendente.Name = "lblAtendente";
            this.lblAtendente.Size = new System.Drawing.Size(249, 23);
            this.lblAtendente.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(988, 505);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 23);
            this.label15.TabIndex = 25;
            this.label15.Text = "Atendente:";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.label8.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(0, 468);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(1357, 31);
            this.label8.TabIndex = 24;
            this.label8.Text = "DADOS DO CLIENTE";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEnviarPedido
            // 
            this.btnEnviarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.Graphic;
            this.btnEnviarPedido.BackColor = System.Drawing.Color.MediumBlue;
            this.btnEnviarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEnviarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnEnviarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnviarPedido.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviarPedido.Image = global::UI.Properties.Resources.ok_fw;
            this.btnEnviarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnEnviarPedido.Location = new System.Drawing.Point(1146, 657);
            this.btnEnviarPedido.Name = "btnEnviarPedido";
            this.btnEnviarPedido.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnEnviarPedido.Size = new System.Drawing.Size(211, 41);
            this.btnEnviarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEnviarPedido.TabIndex = 5;
            this.btnEnviarPedido.Text = "ENVIAR PEDIDO - F10";
            this.btnEnviarPedido.TextColor = System.Drawing.Color.White;
            this.btnEnviarPedido.Click += new System.EventHandler(this.btnEnviarPedido_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.label6.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(0, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1357, 31);
            this.label6.TabIndex = 23;
            this.label6.Text = "ITENS DO PEDIDO";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMotivoCancelamento
            // 
            this.txtMotivoCancelamento.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoCancelamento.Location = new System.Drawing.Point(589, 662);
            this.txtMotivoCancelamento.MaxLength = 149;
            this.txtMotivoCancelamento.Name = "txtMotivoCancelamento";
            this.txtMotivoCancelamento.Size = new System.Drawing.Size(494, 31);
            this.txtMotivoCancelamento.TabIndex = 4;
            this.txtMotivoCancelamento.Visible = false;
            // 
            // lblMotivoCancelamento
            // 
            this.lblMotivoCancelamento.AutoSize = true;
            this.lblMotivoCancelamento.BackColor = System.Drawing.Color.Transparent;
            this.lblMotivoCancelamento.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotivoCancelamento.ForeColor = System.Drawing.Color.Navy;
            this.lblMotivoCancelamento.Location = new System.Drawing.Point(585, 636);
            this.lblMotivoCancelamento.Name = "lblMotivoCancelamento";
            this.lblMotivoCancelamento.Size = new System.Drawing.Size(255, 23);
            this.lblMotivoCancelamento.TabIndex = 21;
            this.lblMotivoCancelamento.Text = "Motivo do Cancelamento:";
            this.lblMotivoCancelamento.Visible = false;
            // 
            // lblBairro
            // 
            this.lblBairro.BackColor = System.Drawing.Color.Transparent;
            this.lblBairro.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.ForeColor = System.Drawing.Color.Red;
            this.lblBairro.Location = new System.Drawing.Point(736, 545);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(242, 23);
            this.lblBairro.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(300, 636);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "Nome do Entregador:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 505);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Numero do Pedido:";
            // 
            // cbEntregador
            // 
            this.cbEntregador.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbEntregador.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbEntregador.DisplayMember = "apelidoEntregador";
            this.cbEntregador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEntregador.Enabled = false;
            this.cbEntregador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEntregador.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEntregador.ForeColor = System.Drawing.Color.Blue;
            this.cbEntregador.FormattingEnabled = true;
            this.cbEntregador.Items.AddRange(new object[] {
            "PRONTO PARA ENTREGA",
            "SAIU PARA ENTREGA",
            "PEDIDO ENTREGUE",
            "PEDIDO CANCELADO"});
            this.cbEntregador.Location = new System.Drawing.Point(304, 662);
            this.cbEntregador.Name = "cbEntregador";
            this.cbEntregador.Size = new System.Drawing.Size(268, 31);
            this.cbEntregador.TabIndex = 3;
            this.cbEntregador.ValueMember = "idEntregador";
            // 
            // lblNumPedido
            // 
            this.lblNumPedido.BackColor = System.Drawing.Color.Transparent;
            this.lblNumPedido.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPedido.ForeColor = System.Drawing.Color.Red;
            this.lblNumPedido.Location = new System.Drawing.Point(202, 505);
            this.lblNumPedido.Name = "lblNumPedido";
            this.lblNumPedido.Size = new System.Drawing.Size(115, 23);
            this.lblNumPedido.TabIndex = 3;
            this.lblNumPedido.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTempoEspera
            // 
            this.lblTempoEspera.BackColor = System.Drawing.Color.Transparent;
            this.lblTempoEspera.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoEspera.ForeColor = System.Drawing.Color.Red;
            this.lblTempoEspera.Location = new System.Drawing.Point(1179, 586);
            this.lblTempoEspera.Name = "lblTempoEspera";
            this.lblTempoEspera.Size = new System.Drawing.Size(159, 23);
            this.lblTempoEspera.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(331, 504);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Cliente:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(988, 586);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(185, 23);
            this.label3.TabIndex = 16;
            this.label3.Text = "Tempo de Espera:";
            // 
            // lblNomeCliente
            // 
            this.lblNomeCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblNomeCliente.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeCliente.ForeColor = System.Drawing.Color.Red;
            this.lblNomeCliente.Location = new System.Drawing.Point(422, 505);
            this.lblNomeCliente.Name = "lblNomeCliente";
            this.lblNomeCliente.Size = new System.Drawing.Size(517, 23);
            this.lblNomeCliente.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 545);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 23);
            this.label5.TabIndex = 6;
            this.label5.Text = "Endereço:";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(666, 545);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(77, 23);
            this.label.TabIndex = 14;
            this.label.Text = "Bairro:";
            // 
            // lblEndercoCliente
            // 
            this.lblEndercoCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblEndercoCliente.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndercoCliente.ForeColor = System.Drawing.Color.Red;
            this.lblEndercoCliente.Location = new System.Drawing.Point(124, 545);
            this.lblEndercoCliente.Name = "lblEndercoCliente";
            this.lblEndercoCliente.Size = new System.Drawing.Size(536, 23);
            this.lblEndercoCliente.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(11, 636);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(197, 23);
            this.label10.TabIndex = 13;
            this.label10.Text = "Selecione o Status:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 586);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 23);
            this.label7.TabIndex = 8;
            this.label7.Text = "Ponto de Referência:";
            // 
            // cbStatus
            // 
            this.cbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStatus.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStatus.ForeColor = System.Drawing.Color.Blue;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Items.AddRange(new object[] {
            "PRONTO PARA ENTREGA",
            "SAIU PARA ENTREGA",
            "PEDIDO CANCELADO"});
            this.cbStatus.Location = new System.Drawing.Point(15, 662);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(268, 31);
            this.cbStatus.TabIndex = 2;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.cbStatus_SelectedIndexChanged);
            // 
            // lblPontoReferenciaCliente
            // 
            this.lblPontoReferenciaCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblPontoReferenciaCliente.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPontoReferenciaCliente.ForeColor = System.Drawing.Color.Red;
            this.lblPontoReferenciaCliente.Location = new System.Drawing.Point(227, 586);
            this.lblPontoReferenciaCliente.Name = "lblPontoReferenciaCliente";
            this.lblPontoReferenciaCliente.Size = new System.Drawing.Size(683, 23);
            this.lblPontoReferenciaCliente.TabIndex = 9;
            // 
            // dgvProdutos
            // 
            this.dgvProdutos.AllowUserToAddRows = false;
            this.dgvProdutos.AllowUserToDeleteRows = false;
            this.dgvProdutos.AllowUserToResizeColumns = false;
            this.dgvProdutos.AllowUserToResizeRows = false;
            this.dgvProdutos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvProdutos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.dgvProdutos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgvProdutos.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvProdutos.Location = new System.Drawing.Point(1, 278);
            this.dgvProdutos.MultiSelect = false;
            this.dgvProdutos.Name = "dgvProdutos";
            this.dgvProdutos.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvProdutos.RowHeadersVisible = false;
            this.dgvProdutos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvProdutos.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Lime;
            this.dgvProdutos.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Lime;
            this.dgvProdutos.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvProdutos.RowTemplate.ReadOnly = true;
            this.dgvProdutos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProdutos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProdutos.Size = new System.Drawing.Size(1356, 189);
            this.dgvProdutos.TabIndex = 20;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "codigoProduto";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn1.HeaderText = "COD PROD";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 130;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "descricaoProduto";
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn2.HeaderText = "DESCRIÇÃO PRODUTO";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 500;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "extraVendaDetalhes";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn3.HeaderText = "EXTRA";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "especialVendaDetalhes";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.Format = "T";
            dataGridViewCellStyle14.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn4.HeaderText = "ESPECIAL";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 300;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "quantidadeVendaDetalhes";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn5.HeaderText = "QTD";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // tiProntoEntrega
            // 
            this.tiProntoEntrega.AttachedControl = this.tabControlPanel3;
            this.tiProntoEntrega.Name = "tiProntoEntrega";
            this.tiProntoEntrega.Text = "Pedidos Para Entrega";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.lblValorPedidoEntrega);
            this.tabControlPanel1.Controls.Add(this.label26);
            this.tabControlPanel1.Controls.Add(this.lblAtendenteEntrega);
            this.tabControlPanel1.Controls.Add(this.label22);
            this.tabControlPanel1.Controls.Add(this.lblTempoTotalPedido);
            this.tabControlPanel1.Controls.Add(this.label19);
            this.tabControlPanel1.Controls.Add(this.lblTempoEnvio);
            this.tabControlPanel1.Controls.Add(this.label16);
            this.tabControlPanel1.Controls.Add(this.lblHoraPedido);
            this.tabControlPanel1.Controls.Add(this.label12);
            this.tabControlPanel1.Controls.Add(this.lblNomeEntegador);
            this.tabControlPanel1.Controls.Add(this.label9);
            this.tabControlPanel1.Controls.Add(this.btnFinalizarPedido);
            this.tabControlPanel1.Controls.Add(this.txtMotivoCancelamentoEspera);
            this.tabControlPanel1.Controls.Add(this.lblMotivoCancelamentoEspera);
            this.tabControlPanel1.Controls.Add(this.lblBairroEntrega);
            this.tabControlPanel1.Controls.Add(this.label13);
            this.tabControlPanel1.Controls.Add(this.label14);
            this.tabControlPanel1.Controls.Add(this.lblNumPedidoEntrega);
            this.tabControlPanel1.Controls.Add(this.lblTempoPedidoEspera);
            this.tabControlPanel1.Controls.Add(this.label17);
            this.tabControlPanel1.Controls.Add(this.label18);
            this.tabControlPanel1.Controls.Add(this.lblNomeClienteEntrega);
            this.tabControlPanel1.Controls.Add(this.label20);
            this.tabControlPanel1.Controls.Add(this.label21);
            this.tabControlPanel1.Controls.Add(this.lblEnderecoEntrega);
            this.tabControlPanel1.Controls.Add(this.label23);
            this.tabControlPanel1.Controls.Add(this.label24);
            this.tabControlPanel1.Controls.Add(this.cbStatuEntrega);
            this.tabControlPanel1.Controls.Add(this.lblPontoReferenciaEntrega);
            this.tabControlPanel1.Controls.Add(this.dgvDeliverySaiuEntrega);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 30);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1368, 738);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.LightBlue;
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.LightBlue;
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.LightBlue;
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 2;
            this.tabControlPanel1.TabItem = this.tiSaiuEntrega;
            // 
            // lblValorPedidoEntrega
            // 
            this.lblValorPedidoEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblValorPedidoEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorPedidoEntrega.ForeColor = System.Drawing.Color.Red;
            this.lblValorPedidoEntrega.Location = new System.Drawing.Point(1145, 369);
            this.lblValorPedidoEntrega.Name = "lblValorPedidoEntrega";
            this.lblValorPedidoEntrega.Size = new System.Drawing.Size(210, 23);
            this.lblValorPedidoEntrega.TabIndex = 55;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(999, 369);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 23);
            this.label26.TabIndex = 54;
            this.label26.Text = "Valor Pedido:";
            // 
            // lblAtendenteEntrega
            // 
            this.lblAtendenteEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblAtendenteEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendenteEntrega.ForeColor = System.Drawing.Color.Red;
            this.lblAtendenteEntrega.Location = new System.Drawing.Point(1111, 329);
            this.lblAtendenteEntrega.Name = "lblAtendenteEntrega";
            this.lblAtendenteEntrega.Size = new System.Drawing.Size(244, 23);
            this.lblAtendenteEntrega.TabIndex = 53;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(999, 329);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(115, 23);
            this.label22.TabIndex = 52;
            this.label22.Text = "Atendente:";
            // 
            // lblTempoTotalPedido
            // 
            this.lblTempoTotalPedido.BackColor = System.Drawing.Color.Transparent;
            this.lblTempoTotalPedido.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoTotalPedido.ForeColor = System.Drawing.Color.Navy;
            this.lblTempoTotalPedido.Location = new System.Drawing.Point(873, 550);
            this.lblTempoTotalPedido.Name = "lblTempoTotalPedido";
            this.lblTempoTotalPedido.Size = new System.Drawing.Size(165, 23);
            this.lblTempoTotalPedido.TabIndex = 51;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(635, 550);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(232, 23);
            this.label19.TabIndex = 50;
            this.label19.Text = "Tempo Total do Pedido";
            // 
            // lblTempoEnvio
            // 
            this.lblTempoEnvio.BackColor = System.Drawing.Color.Transparent;
            this.lblTempoEnvio.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoEnvio.ForeColor = System.Drawing.Color.Navy;
            this.lblTempoEnvio.Location = new System.Drawing.Point(826, 503);
            this.lblTempoEnvio.Name = "lblTempoEnvio";
            this.lblTempoEnvio.Size = new System.Drawing.Size(165, 23);
            this.lblTempoEnvio.TabIndex = 49;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(635, 503);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(184, 23);
            this.label16.TabIndex = 48;
            this.label16.Text = "Tempo para Envio";
            // 
            // lblHoraPedido
            // 
            this.lblHoraPedido.BackColor = System.Drawing.Color.Transparent;
            this.lblHoraPedido.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraPedido.ForeColor = System.Drawing.Color.Navy;
            this.lblHoraPedido.Location = new System.Drawing.Point(821, 460);
            this.lblHoraPedido.Name = "lblHoraPedido";
            this.lblHoraPedido.Size = new System.Drawing.Size(165, 23);
            this.lblHoraPedido.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(635, 460);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 23);
            this.label12.TabIndex = 46;
            this.label12.Text = "Hora do Pedido:";
            // 
            // lblNomeEntegador
            // 
            this.lblNomeEntegador.BackColor = System.Drawing.Color.Transparent;
            this.lblNomeEntegador.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeEntegador.ForeColor = System.Drawing.Color.Navy;
            this.lblNomeEntegador.Location = new System.Drawing.Point(300, 486);
            this.lblNomeEntegador.Name = "lblNomeEntegador";
            this.lblNomeEntegador.Size = new System.Drawing.Size(272, 31);
            this.lblNomeEntegador.TabIndex = 45;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.label9.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(0, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(1368, 31);
            this.label9.TabIndex = 44;
            this.label9.Text = "DADOS DO CLIENTE";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFinalizarPedido
            // 
            this.btnFinalizarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.Graphic;
            this.btnFinalizarPedido.BackColor = System.Drawing.Color.MediumBlue;
            this.btnFinalizarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFinalizarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnFinalizarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinalizarPedido.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizarPedido.Image = global::UI.Properties.Resources.ok_fw;
            this.btnFinalizarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnFinalizarPedido.Location = new System.Drawing.Point(1119, 646);
            this.btnFinalizarPedido.Name = "btnFinalizarPedido";
            this.btnFinalizarPedido.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnFinalizarPedido.Size = new System.Drawing.Size(245, 41);
            this.btnFinalizarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFinalizarPedido.TabIndex = 31;
            this.btnFinalizarPedido.Text = "FINALIZAR PEDIDO - F12";
            this.btnFinalizarPedido.TextColor = System.Drawing.Color.White;
            this.btnFinalizarPedido.Click += new System.EventHandler(this.btnFinalizarPedido_Click);
            // 
            // txtMotivoCancelamentoEspera
            // 
            this.txtMotivoCancelamentoEspera.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoCancelamentoEspera.Location = new System.Drawing.Point(15, 564);
            this.txtMotivoCancelamentoEspera.MaxLength = 149;
            this.txtMotivoCancelamentoEspera.Multiline = true;
            this.txtMotivoCancelamentoEspera.Name = "txtMotivoCancelamentoEspera";
            this.txtMotivoCancelamentoEspera.Size = new System.Drawing.Size(557, 123);
            this.txtMotivoCancelamentoEspera.TabIndex = 29;
            this.txtMotivoCancelamentoEspera.Visible = false;
            // 
            // lblMotivoCancelamentoEspera
            // 
            this.lblMotivoCancelamentoEspera.AutoSize = true;
            this.lblMotivoCancelamentoEspera.BackColor = System.Drawing.Color.Transparent;
            this.lblMotivoCancelamentoEspera.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotivoCancelamentoEspera.ForeColor = System.Drawing.Color.Black;
            this.lblMotivoCancelamentoEspera.Location = new System.Drawing.Point(11, 538);
            this.lblMotivoCancelamentoEspera.Name = "lblMotivoCancelamentoEspera";
            this.lblMotivoCancelamentoEspera.Size = new System.Drawing.Size(255, 23);
            this.lblMotivoCancelamentoEspera.TabIndex = 43;
            this.lblMotivoCancelamentoEspera.Text = "Motivo do Cancelamento:";
            this.lblMotivoCancelamentoEspera.Visible = false;
            // 
            // lblBairroEntrega
            // 
            this.lblBairroEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblBairroEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairroEntrega.ForeColor = System.Drawing.Color.Navy;
            this.lblBairroEntrega.Location = new System.Drawing.Point(771, 369);
            this.lblBairroEntrega.Name = "lblBairroEntrega";
            this.lblBairroEntrega.Size = new System.Drawing.Size(215, 23);
            this.lblBairroEntrega.TabIndex = 39;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(300, 460);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(217, 23);
            this.label13.TabIndex = 42;
            this.label13.Text = "Nome do Entregador:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 329);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(196, 23);
            this.label14.TabIndex = 25;
            this.label14.Text = "Numero do Pedido:";
            // 
            // lblNumPedidoEntrega
            // 
            this.lblNumPedidoEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblNumPedidoEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPedidoEntrega.ForeColor = System.Drawing.Color.Navy;
            this.lblNumPedidoEntrega.Location = new System.Drawing.Point(203, 329);
            this.lblNumPedidoEntrega.Name = "lblNumPedidoEntrega";
            this.lblNumPedidoEntrega.Size = new System.Drawing.Size(122, 23);
            this.lblNumPedidoEntrega.TabIndex = 28;
            this.lblNumPedidoEntrega.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTempoPedidoEspera
            // 
            this.lblTempoPedidoEspera.BackColor = System.Drawing.Color.Transparent;
            this.lblTempoPedidoEspera.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempoPedidoEspera.ForeColor = System.Drawing.Color.Navy;
            this.lblTempoPedidoEspera.Location = new System.Drawing.Point(1190, 410);
            this.lblTempoPedidoEspera.Name = "lblTempoPedidoEspera";
            this.lblTempoPedidoEspera.Size = new System.Drawing.Size(165, 23);
            this.lblTempoPedidoEspera.TabIndex = 41;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(331, 328);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 23);
            this.label17.TabIndex = 30;
            this.label17.Text = "Cliente:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(999, 410);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(185, 23);
            this.label18.TabIndex = 40;
            this.label18.Text = "Tempo de Espera:";
            // 
            // lblNomeClienteEntrega
            // 
            this.lblNomeClienteEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblNomeClienteEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeClienteEntrega.ForeColor = System.Drawing.Color.Navy;
            this.lblNomeClienteEntrega.Location = new System.Drawing.Point(422, 329);
            this.lblNomeClienteEntrega.Name = "lblNomeClienteEntrega";
            this.lblNomeClienteEntrega.Size = new System.Drawing.Size(564, 23);
            this.lblNomeClienteEntrega.TabIndex = 32;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(11, 369);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(107, 23);
            this.label20.TabIndex = 33;
            this.label20.Text = "Endereço:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(701, 369);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 23);
            this.label21.TabIndex = 38;
            this.label21.Text = "Bairro:";
            // 
            // lblEnderecoEntrega
            // 
            this.lblEnderecoEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblEnderecoEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoEntrega.ForeColor = System.Drawing.Color.Navy;
            this.lblEnderecoEntrega.Location = new System.Drawing.Point(124, 369);
            this.lblEnderecoEntrega.Name = "lblEnderecoEntrega";
            this.lblEnderecoEntrega.Size = new System.Drawing.Size(571, 23);
            this.lblEnderecoEntrega.TabIndex = 34;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(11, 460);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(197, 23);
            this.label23.TabIndex = 37;
            this.label23.Text = "Selecione o Status:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(11, 410);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(210, 23);
            this.label24.TabIndex = 35;
            this.label24.Text = "Ponto de Referência:";
            // 
            // cbStatuEntrega
            // 
            this.cbStatuEntrega.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbStatuEntrega.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStatuEntrega.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatuEntrega.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStatuEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStatuEntrega.ForeColor = System.Drawing.Color.Blue;
            this.cbStatuEntrega.FormattingEnabled = true;
            this.cbStatuEntrega.Items.AddRange(new object[] {
            "PEDIDO ENTREGUE",
            "PEDIDO CANCELADO"});
            this.cbStatuEntrega.Location = new System.Drawing.Point(15, 486);
            this.cbStatuEntrega.Name = "cbStatuEntrega";
            this.cbStatuEntrega.Size = new System.Drawing.Size(268, 31);
            this.cbStatuEntrega.TabIndex = 26;
            this.cbStatuEntrega.SelectedIndexChanged += new System.EventHandler(this.cbStatuEntrega_SelectedIndexChanged);
            // 
            // lblPontoReferenciaEntrega
            // 
            this.lblPontoReferenciaEntrega.BackColor = System.Drawing.Color.Transparent;
            this.lblPontoReferenciaEntrega.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPontoReferenciaEntrega.ForeColor = System.Drawing.Color.Navy;
            this.lblPontoReferenciaEntrega.Location = new System.Drawing.Point(227, 410);
            this.lblPontoReferenciaEntrega.Name = "lblPontoReferenciaEntrega";
            this.lblPontoReferenciaEntrega.Size = new System.Drawing.Size(683, 23);
            this.lblPontoReferenciaEntrega.TabIndex = 36;
            // 
            // dgvDeliverySaiuEntrega
            // 
            this.dgvDeliverySaiuEntrega.AllowUserToAddRows = false;
            this.dgvDeliverySaiuEntrega.AllowUserToDeleteRows = false;
            this.dgvDeliverySaiuEntrega.AllowUserToResizeColumns = false;
            this.dgvDeliverySaiuEntrega.AllowUserToResizeRows = false;
            this.dgvDeliverySaiuEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDeliverySaiuEntrega.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDeliverySaiuEntrega.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.balloonTip1.SetBalloonCaption(this.dgvDeliverySaiuEntrega, "Atualizar Pedidos");
            this.balloonTip1.SetBalloonText(this.dgvDeliverySaiuEntrega, "Pressione F5 para Atualizar os Pedidos");
            this.dgvDeliverySaiuEntrega.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDeliverySaiuEntrega.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvDeliverySaiuEntrega.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeliverySaiuEntrega.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.horaSaid,
            this.tempoEntr});
            this.dgvDeliverySaiuEntrega.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvDeliverySaiuEntrega.Location = new System.Drawing.Point(-1, 0);
            this.dgvDeliverySaiuEntrega.MultiSelect = false;
            this.dgvDeliverySaiuEntrega.Name = "dgvDeliverySaiuEntrega";
            this.dgvDeliverySaiuEntrega.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvDeliverySaiuEntrega.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvDeliverySaiuEntrega.RowHeadersVisible = false;
            this.dgvDeliverySaiuEntrega.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvDeliverySaiuEntrega.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.LightSkyBlue;
            this.dgvDeliverySaiuEntrega.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgvDeliverySaiuEntrega.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Blue;
            this.dgvDeliverySaiuEntrega.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvDeliverySaiuEntrega.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDeliverySaiuEntrega.Size = new System.Drawing.Size(1365, 289);
            this.dgvDeliverySaiuEntrega.StandardTab = true;
            this.dgvDeliverySaiuEntrega.TabIndex = 2;
            this.dgvDeliverySaiuEntrega.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeliverySaiuEntrega_CellClick);
            this.dgvDeliverySaiuEntrega.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvDeliverySaiuEntrega_KeyDown);
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "idVendaCabecalho";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn6.HeaderText = "NUM PED";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "nomeCliente";
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn7.HeaderText = "NOME CLIENTE";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 400;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "bairroCliente";
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn8.HeaderText = "BAIRRO";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 280;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "apelidoEntregador";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.Format = "T";
            dataGridViewCellStyle21.NullValue = null;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn9.HeaderText = "ENTREGADOR";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 200;
            // 
            // horaSaid
            // 
            this.horaSaid.DataPropertyName = "horaSaida";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle22.Format = "T";
            dataGridViewCellStyle22.NullValue = null;
            this.horaSaid.DefaultCellStyle = dataGridViewCellStyle22;
            this.horaSaid.HeaderText = "HORA SAIDA";
            this.horaSaid.Name = "horaSaid";
            this.horaSaid.ReadOnly = true;
            this.horaSaid.Width = 150;
            // 
            // tempoEntr
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle23.Format = "T";
            dataGridViewCellStyle23.NullValue = null;
            this.tempoEntr.DefaultCellStyle = dataGridViewCellStyle23;
            this.tempoEntr.HeaderText = "TEMPO ENTREGA";
            this.tempoEntr.Name = "tempoEntr";
            this.tempoEntr.ReadOnly = true;
            this.tempoEntr.Width = 150;
            // 
            // tiSaiuEntrega
            // 
            this.tiSaiuEntrega.AttachedControl = this.tabControlPanel1;
            this.tiSaiuEntrega.Name = "tiSaiuEntrega";
            this.tiSaiuEntrega.Text = "Pedidos Sairam para Entrega";
            this.tiSaiuEntrega.Click += new System.EventHandler(this.tiSaiuEntrega_Click);
            // 
            // tabItem1
            // 
            this.tabItem1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.tabItem1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(168)))), ((int)(((byte)(228)))));
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue;
            this.tabItem1.Text = "Pedidos Para Entrega";
            this.tabItem1.TextColor = System.Drawing.Color.Black;
            // 
            // balloonTip1
            // 
            this.balloonTip1.Style = DevComponents.DotNetBar.eBallonStyle.Office2007Alert;
            // 
            // tempoEntrega
            // 
            this.tempoEntrega.Enabled = true;
            this.tempoEntrega.Interval = 1000;
            this.tempoEntrega.Tick += new System.EventHandler(this.tempoEntrega_Tick);
            // 
            // frmEntregaDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 730);
            this.Controls.Add(this.tabControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEntregaDelivery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Entregas - Delivery";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEntregaDelivery_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEntregaDelivery_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl2)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdutos)).EndInit();
            this.tabControlPanel1.ResumeLayout(false);
            this.tabControlPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliverySaiuEntrega)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDelivery;
        private DevComponents.DotNetBar.TabItem tabItem2;
        private System.Windows.Forms.Timer tempoPedidos;
        private DevComponents.DotNetBar.TabControl tabControl2;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem tiProntoEntrega;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbEntregador;
        private System.Windows.Forms.Label lblTempoEspera;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Label lblPontoReferenciaCliente;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblEndercoCliente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNomeCliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNumPedido;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private System.Windows.Forms.TextBox txtMotivoCancelamento;
        private System.Windows.Forms.Label lblMotivoCancelamento;
        private System.Windows.Forms.Label label6;
        private DevComponents.DotNetBar.ButtonX btnEnviarPedido;
        private DevComponents.DotNetBar.BalloonTip balloonTip1;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private System.Windows.Forms.DataGridView dgvDeliverySaiuEntrega;
        private DevComponents.DotNetBar.TabItem tiSaiuEntrega;
        private System.Windows.Forms.Timer tempoEntrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaSaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn tempoEntr;
        private System.Windows.Forms.DataGridView dgvProdutos;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.ButtonX btnFinalizarPedido;
        private System.Windows.Forms.TextBox txtMotivoCancelamentoEspera;
        private System.Windows.Forms.Label lblMotivoCancelamentoEspera;
        private System.Windows.Forms.Label lblBairroEntrega;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblNumPedidoEntrega;
        private System.Windows.Forms.Label lblTempoPedidoEspera;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblNomeClienteEntrega;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblEnderecoEntrega;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbStatuEntrega;
        private System.Windows.Forms.Label lblPontoReferenciaEntrega;
        private System.Windows.Forms.Label lblNomeEntegador;
        private System.Windows.Forms.Label lblTempoEnvio;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblHoraPedido;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblTempoTotalPedido;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblAtendente;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblAtendenteEntrega;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblValorPedido;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblValorPedidoEntrega;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn Column7;
        private DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn hora;
        private System.Windows.Forms.DataGridViewTextBoxColumn tempo;
    }
}