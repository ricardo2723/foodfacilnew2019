﻿namespace UI
{
    partial class frmPdv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPdv));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnPedC18 = new System.Windows.Forms.Button();
            this.btnPedM4 = new System.Windows.Forms.Button();
            this.btnPedC17 = new System.Windows.Forms.Button();
            this.btnPedM1 = new System.Windows.Forms.Button();
            this.btnPedC16 = new System.Windows.Forms.Button();
            this.btnPedM2 = new System.Windows.Forms.Button();
            this.btnPedC15 = new System.Windows.Forms.Button();
            this.btnPedM3 = new System.Windows.Forms.Button();
            this.btnPedC14 = new System.Windows.Forms.Button();
            this.btnPedM5 = new System.Windows.Forms.Button();
            this.btnPedC13 = new System.Windows.Forms.Button();
            this.btnPedM6 = new System.Windows.Forms.Button();
            this.btnPedC12 = new System.Windows.Forms.Button();
            this.btnPedM7 = new System.Windows.Forms.Button();
            this.btnPedC11 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.btnPedC10 = new System.Windows.Forms.Button();
            this.btnPedM8 = new System.Windows.Forms.Button();
            this.btnPedC9 = new System.Windows.Forms.Button();
            this.btnPedM9 = new System.Windows.Forms.Button();
            this.btnPedC8 = new System.Windows.Forms.Button();
            this.btnPedM10 = new System.Windows.Forms.Button();
            this.btnPedC7 = new System.Windows.Forms.Button();
            this.btnPedM11 = new System.Windows.Forms.Button();
            this.btnPedC6 = new System.Windows.Forms.Button();
            this.btnPedM12 = new System.Windows.Forms.Button();
            this.btnPedC5 = new System.Windows.Forms.Button();
            this.btnPedM13 = new System.Windows.Forms.Button();
            this.btnPedC4 = new System.Windows.Forms.Button();
            this.btnPedM14 = new System.Windows.Forms.Button();
            this.btnPedC3 = new System.Windows.Forms.Button();
            this.btnPedM15 = new System.Windows.Forms.Button();
            this.btnPedC2 = new System.Windows.Forms.Button();
            this.btnPedM16 = new System.Windows.Forms.Button();
            this.btnPedC1 = new System.Windows.Forms.Button();
            this.btnPedM17 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.btnPedM18 = new System.Windows.Forms.Button();
            this.btnPedB18 = new System.Windows.Forms.Button();
            this.btnPedM19 = new System.Windows.Forms.Button();
            this.btnPedB17 = new System.Windows.Forms.Button();
            this.btnPedM20 = new System.Windows.Forms.Button();
            this.btnPedB16 = new System.Windows.Forms.Button();
            this.btnPedM21 = new System.Windows.Forms.Button();
            this.btnPedB15 = new System.Windows.Forms.Button();
            this.btnPedM22 = new System.Windows.Forms.Button();
            this.btnPedB14 = new System.Windows.Forms.Button();
            this.btnPedM23 = new System.Windows.Forms.Button();
            this.btnPedB13 = new System.Windows.Forms.Button();
            this.btnPedM24 = new System.Windows.Forms.Button();
            this.btnPedB12 = new System.Windows.Forms.Button();
            this.btnPedM25 = new System.Windows.Forms.Button();
            this.btnPedB11 = new System.Windows.Forms.Button();
            this.btnPedM26 = new System.Windows.Forms.Button();
            this.btnPedB10 = new System.Windows.Forms.Button();
            this.btnPedM27 = new System.Windows.Forms.Button();
            this.btnPedB9 = new System.Windows.Forms.Button();
            this.btnPedM28 = new System.Windows.Forms.Button();
            this.btnPedB8 = new System.Windows.Forms.Button();
            this.btnPedM29 = new System.Windows.Forms.Button();
            this.btnPedB7 = new System.Windows.Forms.Button();
            this.btnPedM30 = new System.Windows.Forms.Button();
            this.btnPedB6 = new System.Windows.Forms.Button();
            this.btnPedM31 = new System.Windows.Forms.Button();
            this.btnPedB5 = new System.Windows.Forms.Button();
            this.btnPedM32 = new System.Windows.Forms.Button();
            this.btnPedB4 = new System.Windows.Forms.Button();
            this.btnPedM33 = new System.Windows.Forms.Button();
            this.btnPedB3 = new System.Windows.Forms.Button();
            this.btnPedM34 = new System.Windows.Forms.Button();
            this.btnPedB2 = new System.Windows.Forms.Button();
            this.btnPedM35 = new System.Windows.Forms.Button();
            this.btnPedB1 = new System.Windows.Forms.Button();
            this.btnPedM36 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.lblAtendimento = new DevComponents.DotNetBar.LabelX();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblAtendente = new System.Windows.Forms.Label();
            this.lblCodVenda = new System.Windows.Forms.Label();
            this.lblAtendimentoCupom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblNomeEmpresa = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dgCupom = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.extras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.especiais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataEnvioVendaDetalhes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idVendaDetalhes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDataHora = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtTotalGeral = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtItens = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtValorTotal = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtValorUnitario = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnImprimir = new DevComponents.DotNetBar.ButtonX();
            this.btnExtra = new DevComponents.DotNetBar.ButtonX();
            this.btnFinalizarPedido = new DevComponents.DotNetBar.ButtonX();
            this.btnEspecial = new DevComponents.DotNetBar.ButtonX();
            this.btnVisualizarPedido = new DevComponents.DotNetBar.ButtonX();
            this.btnEnviarPedido = new DevComponents.DotNetBar.ButtonX();
            this.btnCancelarPedido = new DevComponents.DotNetBar.ButtonX();
            this.btnCancelarItem = new DevComponents.DotNetBar.ButtonX();
            this.btnAddQtd = new DevComponents.DotNetBar.ButtonX();
            this.btnPesquisarProdutos = new DevComponents.DotNetBar.ButtonX();
            this.PdvDataHora = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOperador = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.cmsMenuPdv = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.retiradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculadoraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PdvPrintPed = new System.Windows.Forms.Timer(this.components);
            this.bgwImprimiPedido = new System.ComponentModel.BackgroundWorker();
            this.panel3.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCupom)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.cmsMenuPdv.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.btnPedC18);
            this.panel3.Controls.Add(this.btnPedM4);
            this.panel3.Controls.Add(this.btnPedC17);
            this.panel3.Controls.Add(this.btnPedM1);
            this.panel3.Controls.Add(this.btnPedC16);
            this.panel3.Controls.Add(this.btnPedM2);
            this.panel3.Controls.Add(this.btnPedC15);
            this.panel3.Controls.Add(this.btnPedM3);
            this.panel3.Controls.Add(this.btnPedC14);
            this.panel3.Controls.Add(this.btnPedM5);
            this.panel3.Controls.Add(this.btnPedC13);
            this.panel3.Controls.Add(this.btnPedM6);
            this.panel3.Controls.Add(this.btnPedC12);
            this.panel3.Controls.Add(this.btnPedM7);
            this.panel3.Controls.Add(this.btnPedC11);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.btnPedC10);
            this.panel3.Controls.Add(this.btnPedM8);
            this.panel3.Controls.Add(this.btnPedC9);
            this.panel3.Controls.Add(this.btnPedM9);
            this.panel3.Controls.Add(this.btnPedC8);
            this.panel3.Controls.Add(this.btnPedM10);
            this.panel3.Controls.Add(this.btnPedC7);
            this.panel3.Controls.Add(this.btnPedM11);
            this.panel3.Controls.Add(this.btnPedC6);
            this.panel3.Controls.Add(this.btnPedM12);
            this.panel3.Controls.Add(this.btnPedC5);
            this.panel3.Controls.Add(this.btnPedM13);
            this.panel3.Controls.Add(this.btnPedC4);
            this.panel3.Controls.Add(this.btnPedM14);
            this.panel3.Controls.Add(this.btnPedC3);
            this.panel3.Controls.Add(this.btnPedM15);
            this.panel3.Controls.Add(this.btnPedC2);
            this.panel3.Controls.Add(this.btnPedM16);
            this.panel3.Controls.Add(this.btnPedC1);
            this.panel3.Controls.Add(this.btnPedM17);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.btnPedM18);
            this.panel3.Controls.Add(this.btnPedB18);
            this.panel3.Controls.Add(this.btnPedM19);
            this.panel3.Controls.Add(this.btnPedB17);
            this.panel3.Controls.Add(this.btnPedM20);
            this.panel3.Controls.Add(this.btnPedB16);
            this.panel3.Controls.Add(this.btnPedM21);
            this.panel3.Controls.Add(this.btnPedB15);
            this.panel3.Controls.Add(this.btnPedM22);
            this.panel3.Controls.Add(this.btnPedB14);
            this.panel3.Controls.Add(this.btnPedM23);
            this.panel3.Controls.Add(this.btnPedB13);
            this.panel3.Controls.Add(this.btnPedM24);
            this.panel3.Controls.Add(this.btnPedB12);
            this.panel3.Controls.Add(this.btnPedM25);
            this.panel3.Controls.Add(this.btnPedB11);
            this.panel3.Controls.Add(this.btnPedM26);
            this.panel3.Controls.Add(this.btnPedB10);
            this.panel3.Controls.Add(this.btnPedM27);
            this.panel3.Controls.Add(this.btnPedB9);
            this.panel3.Controls.Add(this.btnPedM28);
            this.panel3.Controls.Add(this.btnPedB8);
            this.panel3.Controls.Add(this.btnPedM29);
            this.panel3.Controls.Add(this.btnPedB7);
            this.panel3.Controls.Add(this.btnPedM30);
            this.panel3.Controls.Add(this.btnPedB6);
            this.panel3.Controls.Add(this.btnPedM31);
            this.panel3.Controls.Add(this.btnPedB5);
            this.panel3.Controls.Add(this.btnPedM32);
            this.panel3.Controls.Add(this.btnPedB4);
            this.panel3.Controls.Add(this.btnPedM33);
            this.panel3.Controls.Add(this.btnPedB3);
            this.panel3.Controls.Add(this.btnPedM34);
            this.panel3.Controls.Add(this.btnPedB2);
            this.panel3.Controls.Add(this.btnPedM35);
            this.panel3.Controls.Add(this.btnPedB1);
            this.panel3.Controls.Add(this.btnPedM36);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.lblAtendimento);
            this.panel3.Controls.Add(this.panelEx2);
            this.panel3.Controls.Add(this.lblDataHora);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Location = new System.Drawing.Point(12, 182);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1350, 580);
            this.panel3.TabIndex = 5;
            // 
            // btnPedC18
            // 
            this.btnPedC18.BackColor = System.Drawing.Color.White;
            this.btnPedC18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC18.BackgroundImage")));
            this.btnPedC18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC18.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC18.ForeColor = System.Drawing.Color.Black;
            this.btnPedC18.Location = new System.Drawing.Point(496, 526);
            this.btnPedC18.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC18.Name = "btnPedC18";
            this.btnPedC18.Size = new System.Drawing.Size(60, 50);
            this.btnPedC18.TabIndex = 227;
            this.btnPedC18.Text = "18";
            this.btnPedC18.UseCompatibleTextRendering = true;
            this.btnPedC18.UseVisualStyleBackColor = false;
            this.btnPedC18.Click += new System.EventHandler(this.btnPedC18_Click);
            // 
            // btnPedM4
            // 
            this.btnPedM4.BackColor = System.Drawing.Color.White;
            this.btnPedM4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM4.BackgroundImage")));
            this.btnPedM4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM4.ForeColor = System.Drawing.Color.Black;
            this.btnPedM4.Location = new System.Drawing.Point(191, 70);
            this.btnPedM4.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM4.Name = "btnPedM4";
            this.btnPedM4.Size = new System.Drawing.Size(60, 50);
            this.btnPedM4.TabIndex = 157;
            this.btnPedM4.Text = "04";
            this.btnPedM4.UseCompatibleTextRendering = true;
            this.btnPedM4.UseVisualStyleBackColor = false;
            this.btnPedM4.Click += new System.EventHandler(this.btnPedM4_Click);
            // 
            // btnPedC17
            // 
            this.btnPedC17.BackColor = System.Drawing.Color.White;
            this.btnPedC17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC17.BackgroundImage")));
            this.btnPedC17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC17.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC17.ForeColor = System.Drawing.Color.Black;
            this.btnPedC17.Location = new System.Drawing.Point(435, 526);
            this.btnPedC17.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC17.Name = "btnPedC17";
            this.btnPedC17.Size = new System.Drawing.Size(60, 50);
            this.btnPedC17.TabIndex = 226;
            this.btnPedC17.Text = "17";
            this.btnPedC17.UseCompatibleTextRendering = true;
            this.btnPedC17.UseVisualStyleBackColor = false;
            this.btnPedC17.Click += new System.EventHandler(this.btnPedC17_Click);
            // 
            // btnPedM1
            // 
            this.btnPedM1.BackColor = System.Drawing.Color.White;
            this.btnPedM1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM1.BackgroundImage")));
            this.btnPedM1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM1.ForeColor = System.Drawing.Color.Black;
            this.btnPedM1.Location = new System.Drawing.Point(8, 70);
            this.btnPedM1.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM1.Name = "btnPedM1";
            this.btnPedM1.Size = new System.Drawing.Size(60, 50);
            this.btnPedM1.TabIndex = 153;
            this.btnPedM1.Text = "01";
            this.btnPedM1.UseCompatibleTextRendering = true;
            this.btnPedM1.UseVisualStyleBackColor = false;
            this.btnPedM1.Click += new System.EventHandler(this.btnPedM1_Click);
            // 
            // btnPedC16
            // 
            this.btnPedC16.BackColor = System.Drawing.Color.White;
            this.btnPedC16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC16.BackgroundImage")));
            this.btnPedC16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC16.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC16.ForeColor = System.Drawing.Color.Black;
            this.btnPedC16.Location = new System.Drawing.Point(374, 526);
            this.btnPedC16.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC16.Name = "btnPedC16";
            this.btnPedC16.Size = new System.Drawing.Size(60, 50);
            this.btnPedC16.TabIndex = 225;
            this.btnPedC16.Text = "16";
            this.btnPedC16.UseCompatibleTextRendering = true;
            this.btnPedC16.UseVisualStyleBackColor = false;
            this.btnPedC16.Click += new System.EventHandler(this.btnPedC16_Click);
            // 
            // btnPedM2
            // 
            this.btnPedM2.BackColor = System.Drawing.Color.White;
            this.btnPedM2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM2.BackgroundImage")));
            this.btnPedM2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM2.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM2.ForeColor = System.Drawing.Color.Black;
            this.btnPedM2.Location = new System.Drawing.Point(69, 70);
            this.btnPedM2.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM2.Name = "btnPedM2";
            this.btnPedM2.Size = new System.Drawing.Size(60, 50);
            this.btnPedM2.TabIndex = 155;
            this.btnPedM2.Text = "02";
            this.btnPedM2.UseCompatibleTextRendering = true;
            this.btnPedM2.UseVisualStyleBackColor = false;
            this.btnPedM2.Click += new System.EventHandler(this.btnPedM2_Click);
            // 
            // btnPedC15
            // 
            this.btnPedC15.BackColor = System.Drawing.Color.White;
            this.btnPedC15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC15.BackgroundImage")));
            this.btnPedC15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC15.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC15.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC15.ForeColor = System.Drawing.Color.Black;
            this.btnPedC15.Location = new System.Drawing.Point(313, 526);
            this.btnPedC15.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC15.Name = "btnPedC15";
            this.btnPedC15.Size = new System.Drawing.Size(60, 50);
            this.btnPedC15.TabIndex = 224;
            this.btnPedC15.Text = "15";
            this.btnPedC15.UseCompatibleTextRendering = true;
            this.btnPedC15.UseVisualStyleBackColor = false;
            this.btnPedC15.Click += new System.EventHandler(this.btnPedC15_Click);
            // 
            // btnPedM3
            // 
            this.btnPedM3.BackColor = System.Drawing.Color.White;
            this.btnPedM3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM3.BackgroundImage")));
            this.btnPedM3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM3.ForeColor = System.Drawing.Color.Black;
            this.btnPedM3.Location = new System.Drawing.Point(130, 70);
            this.btnPedM3.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM3.Name = "btnPedM3";
            this.btnPedM3.Size = new System.Drawing.Size(60, 50);
            this.btnPedM3.TabIndex = 156;
            this.btnPedM3.Text = "03";
            this.btnPedM3.UseCompatibleTextRendering = true;
            this.btnPedM3.UseVisualStyleBackColor = false;
            this.btnPedM3.Click += new System.EventHandler(this.btnPedM3_Click);
            // 
            // btnPedC14
            // 
            this.btnPedC14.BackColor = System.Drawing.Color.White;
            this.btnPedC14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC14.BackgroundImage")));
            this.btnPedC14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC14.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC14.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC14.ForeColor = System.Drawing.Color.Black;
            this.btnPedC14.Location = new System.Drawing.Point(252, 526);
            this.btnPedC14.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC14.Name = "btnPedC14";
            this.btnPedC14.Size = new System.Drawing.Size(60, 50);
            this.btnPedC14.TabIndex = 223;
            this.btnPedC14.Text = "14";
            this.btnPedC14.UseCompatibleTextRendering = true;
            this.btnPedC14.UseVisualStyleBackColor = false;
            this.btnPedC14.Click += new System.EventHandler(this.btnPedC14_Click);
            // 
            // btnPedM5
            // 
            this.btnPedM5.BackColor = System.Drawing.Color.White;
            this.btnPedM5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM5.BackgroundImage")));
            this.btnPedM5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM5.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM5.ForeColor = System.Drawing.Color.Black;
            this.btnPedM5.Location = new System.Drawing.Point(252, 70);
            this.btnPedM5.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM5.Name = "btnPedM5";
            this.btnPedM5.Size = new System.Drawing.Size(60, 50);
            this.btnPedM5.TabIndex = 158;
            this.btnPedM5.Text = "05";
            this.btnPedM5.UseCompatibleTextRendering = true;
            this.btnPedM5.UseVisualStyleBackColor = false;
            this.btnPedM5.Click += new System.EventHandler(this.btnPedM5_Click);
            // 
            // btnPedC13
            // 
            this.btnPedC13.BackColor = System.Drawing.Color.White;
            this.btnPedC13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC13.BackgroundImage")));
            this.btnPedC13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC13.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC13.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC13.ForeColor = System.Drawing.Color.Black;
            this.btnPedC13.Location = new System.Drawing.Point(191, 526);
            this.btnPedC13.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC13.Name = "btnPedC13";
            this.btnPedC13.Size = new System.Drawing.Size(60, 50);
            this.btnPedC13.TabIndex = 222;
            this.btnPedC13.Text = "13";
            this.btnPedC13.UseCompatibleTextRendering = true;
            this.btnPedC13.UseVisualStyleBackColor = false;
            this.btnPedC13.Click += new System.EventHandler(this.btnPedC13_Click);
            // 
            // btnPedM6
            // 
            this.btnPedM6.BackColor = System.Drawing.Color.White;
            this.btnPedM6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM6.BackgroundImage")));
            this.btnPedM6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM6.ForeColor = System.Drawing.Color.Black;
            this.btnPedM6.Location = new System.Drawing.Point(313, 70);
            this.btnPedM6.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM6.Name = "btnPedM6";
            this.btnPedM6.Size = new System.Drawing.Size(60, 50);
            this.btnPedM6.TabIndex = 159;
            this.btnPedM6.Text = "06";
            this.btnPedM6.UseCompatibleTextRendering = true;
            this.btnPedM6.UseVisualStyleBackColor = false;
            this.btnPedM6.Click += new System.EventHandler(this.btnPedM6_Click);
            // 
            // btnPedC12
            // 
            this.btnPedC12.BackColor = System.Drawing.Color.White;
            this.btnPedC12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC12.BackgroundImage")));
            this.btnPedC12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC12.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC12.ForeColor = System.Drawing.Color.Black;
            this.btnPedC12.Location = new System.Drawing.Point(130, 526);
            this.btnPedC12.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC12.Name = "btnPedC12";
            this.btnPedC12.Size = new System.Drawing.Size(60, 50);
            this.btnPedC12.TabIndex = 221;
            this.btnPedC12.Text = "12";
            this.btnPedC12.UseCompatibleTextRendering = true;
            this.btnPedC12.UseVisualStyleBackColor = false;
            this.btnPedC12.Click += new System.EventHandler(this.btnPedC12_Click);
            // 
            // btnPedM7
            // 
            this.btnPedM7.BackColor = System.Drawing.Color.White;
            this.btnPedM7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM7.BackgroundImage")));
            this.btnPedM7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM7.ForeColor = System.Drawing.Color.Black;
            this.btnPedM7.Location = new System.Drawing.Point(374, 70);
            this.btnPedM7.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM7.Name = "btnPedM7";
            this.btnPedM7.Size = new System.Drawing.Size(60, 50);
            this.btnPedM7.TabIndex = 160;
            this.btnPedM7.Text = "07";
            this.btnPedM7.UseCompatibleTextRendering = true;
            this.btnPedM7.UseVisualStyleBackColor = false;
            this.btnPedM7.Click += new System.EventHandler(this.btnPedM7_Click);
            // 
            // btnPedC11
            // 
            this.btnPedC11.BackColor = System.Drawing.Color.White;
            this.btnPedC11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC11.BackgroundImage")));
            this.btnPedC11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC11.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC11.ForeColor = System.Drawing.Color.Black;
            this.btnPedC11.Location = new System.Drawing.Point(69, 526);
            this.btnPedC11.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC11.Name = "btnPedC11";
            this.btnPedC11.Size = new System.Drawing.Size(60, 50);
            this.btnPedC11.TabIndex = 220;
            this.btnPedC11.Text = "11";
            this.btnPedC11.UseCompatibleTextRendering = true;
            this.btnPedC11.UseVisualStyleBackColor = false;
            this.btnPedC11.Click += new System.EventHandler(this.btnPedC11_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(241, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 23);
            this.label14.TabIndex = 154;
            this.label14.Text = "MESAS";
            // 
            // btnPedC10
            // 
            this.btnPedC10.BackColor = System.Drawing.Color.White;
            this.btnPedC10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC10.BackgroundImage")));
            this.btnPedC10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC10.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC10.ForeColor = System.Drawing.Color.Black;
            this.btnPedC10.Location = new System.Drawing.Point(8, 526);
            this.btnPedC10.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC10.Name = "btnPedC10";
            this.btnPedC10.Size = new System.Drawing.Size(60, 50);
            this.btnPedC10.TabIndex = 219;
            this.btnPedC10.Text = "10";
            this.btnPedC10.UseCompatibleTextRendering = true;
            this.btnPedC10.UseVisualStyleBackColor = false;
            this.btnPedC10.Click += new System.EventHandler(this.btnPedC10_Click);
            // 
            // btnPedM8
            // 
            this.btnPedM8.BackColor = System.Drawing.Color.White;
            this.btnPedM8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM8.BackgroundImage")));
            this.btnPedM8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM8.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM8.ForeColor = System.Drawing.Color.Black;
            this.btnPedM8.Location = new System.Drawing.Point(435, 70);
            this.btnPedM8.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM8.Name = "btnPedM8";
            this.btnPedM8.Size = new System.Drawing.Size(60, 50);
            this.btnPedM8.TabIndex = 161;
            this.btnPedM8.Text = "08";
            this.btnPedM8.UseCompatibleTextRendering = true;
            this.btnPedM8.UseVisualStyleBackColor = false;
            this.btnPedM8.Click += new System.EventHandler(this.btnPedM8_Click);
            // 
            // btnPedC9
            // 
            this.btnPedC9.BackColor = System.Drawing.Color.White;
            this.btnPedC9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC9.BackgroundImage")));
            this.btnPedC9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC9.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC9.ForeColor = System.Drawing.Color.Black;
            this.btnPedC9.Location = new System.Drawing.Point(496, 472);
            this.btnPedC9.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC9.Name = "btnPedC9";
            this.btnPedC9.Size = new System.Drawing.Size(60, 50);
            this.btnPedC9.TabIndex = 218;
            this.btnPedC9.Text = "09";
            this.btnPedC9.UseCompatibleTextRendering = true;
            this.btnPedC9.UseVisualStyleBackColor = false;
            this.btnPedC9.Click += new System.EventHandler(this.btnPedC9_Click);
            // 
            // btnPedM9
            // 
            this.btnPedM9.BackColor = System.Drawing.Color.White;
            this.btnPedM9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM9.BackgroundImage")));
            this.btnPedM9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM9.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM9.ForeColor = System.Drawing.Color.Black;
            this.btnPedM9.Location = new System.Drawing.Point(496, 70);
            this.btnPedM9.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM9.Name = "btnPedM9";
            this.btnPedM9.Size = new System.Drawing.Size(60, 50);
            this.btnPedM9.TabIndex = 162;
            this.btnPedM9.Text = "09";
            this.btnPedM9.UseCompatibleTextRendering = true;
            this.btnPedM9.UseVisualStyleBackColor = false;
            this.btnPedM9.Click += new System.EventHandler(this.btnPedM9_Click);
            // 
            // btnPedC8
            // 
            this.btnPedC8.BackColor = System.Drawing.Color.White;
            this.btnPedC8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC8.BackgroundImage")));
            this.btnPedC8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC8.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC8.ForeColor = System.Drawing.Color.Black;
            this.btnPedC8.Location = new System.Drawing.Point(435, 472);
            this.btnPedC8.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC8.Name = "btnPedC8";
            this.btnPedC8.Size = new System.Drawing.Size(60, 50);
            this.btnPedC8.TabIndex = 217;
            this.btnPedC8.Text = "08";
            this.btnPedC8.UseCompatibleTextRendering = true;
            this.btnPedC8.UseVisualStyleBackColor = false;
            this.btnPedC8.Click += new System.EventHandler(this.btnPedC8_Click);
            // 
            // btnPedM10
            // 
            this.btnPedM10.BackColor = System.Drawing.Color.White;
            this.btnPedM10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM10.BackgroundImage")));
            this.btnPedM10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM10.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM10.ForeColor = System.Drawing.Color.Black;
            this.btnPedM10.Location = new System.Drawing.Point(8, 124);
            this.btnPedM10.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM10.Name = "btnPedM10";
            this.btnPedM10.Size = new System.Drawing.Size(60, 50);
            this.btnPedM10.TabIndex = 163;
            this.btnPedM10.Text = "10";
            this.btnPedM10.UseCompatibleTextRendering = true;
            this.btnPedM10.UseVisualStyleBackColor = false;
            this.btnPedM10.Click += new System.EventHandler(this.btnPedM10_Click);
            // 
            // btnPedC7
            // 
            this.btnPedC7.BackColor = System.Drawing.Color.White;
            this.btnPedC7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC7.BackgroundImage")));
            this.btnPedC7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC7.ForeColor = System.Drawing.Color.Black;
            this.btnPedC7.Location = new System.Drawing.Point(374, 472);
            this.btnPedC7.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC7.Name = "btnPedC7";
            this.btnPedC7.Size = new System.Drawing.Size(60, 50);
            this.btnPedC7.TabIndex = 216;
            this.btnPedC7.Text = "07";
            this.btnPedC7.UseCompatibleTextRendering = true;
            this.btnPedC7.UseVisualStyleBackColor = false;
            this.btnPedC7.Click += new System.EventHandler(this.btnPedC7_Click);
            // 
            // btnPedM11
            // 
            this.btnPedM11.BackColor = System.Drawing.Color.White;
            this.btnPedM11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM11.BackgroundImage")));
            this.btnPedM11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM11.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM11.ForeColor = System.Drawing.Color.Black;
            this.btnPedM11.Location = new System.Drawing.Point(69, 124);
            this.btnPedM11.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM11.Name = "btnPedM11";
            this.btnPedM11.Size = new System.Drawing.Size(60, 50);
            this.btnPedM11.TabIndex = 164;
            this.btnPedM11.Text = "11";
            this.btnPedM11.UseCompatibleTextRendering = true;
            this.btnPedM11.UseVisualStyleBackColor = false;
            this.btnPedM11.Click += new System.EventHandler(this.btnPedM11_Click);
            // 
            // btnPedC6
            // 
            this.btnPedC6.BackColor = System.Drawing.Color.White;
            this.btnPedC6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC6.BackgroundImage")));
            this.btnPedC6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC6.ForeColor = System.Drawing.Color.Black;
            this.btnPedC6.Location = new System.Drawing.Point(313, 472);
            this.btnPedC6.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC6.Name = "btnPedC6";
            this.btnPedC6.Size = new System.Drawing.Size(60, 50);
            this.btnPedC6.TabIndex = 215;
            this.btnPedC6.Text = "06";
            this.btnPedC6.UseCompatibleTextRendering = true;
            this.btnPedC6.UseVisualStyleBackColor = false;
            this.btnPedC6.Click += new System.EventHandler(this.btnPedC6_Click);
            // 
            // btnPedM12
            // 
            this.btnPedM12.BackColor = System.Drawing.Color.White;
            this.btnPedM12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM12.BackgroundImage")));
            this.btnPedM12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM12.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM12.ForeColor = System.Drawing.Color.Black;
            this.btnPedM12.Location = new System.Drawing.Point(130, 124);
            this.btnPedM12.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM12.Name = "btnPedM12";
            this.btnPedM12.Size = new System.Drawing.Size(60, 50);
            this.btnPedM12.TabIndex = 165;
            this.btnPedM12.Text = "12";
            this.btnPedM12.UseCompatibleTextRendering = true;
            this.btnPedM12.UseVisualStyleBackColor = false;
            this.btnPedM12.Click += new System.EventHandler(this.btnPedM12_Click);
            // 
            // btnPedC5
            // 
            this.btnPedC5.BackColor = System.Drawing.Color.White;
            this.btnPedC5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC5.BackgroundImage")));
            this.btnPedC5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC5.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC5.ForeColor = System.Drawing.Color.Black;
            this.btnPedC5.Location = new System.Drawing.Point(252, 472);
            this.btnPedC5.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC5.Name = "btnPedC5";
            this.btnPedC5.Size = new System.Drawing.Size(60, 50);
            this.btnPedC5.TabIndex = 214;
            this.btnPedC5.Text = "05";
            this.btnPedC5.UseCompatibleTextRendering = true;
            this.btnPedC5.UseVisualStyleBackColor = false;
            this.btnPedC5.Click += new System.EventHandler(this.btnPedC5_Click);
            // 
            // btnPedM13
            // 
            this.btnPedM13.BackColor = System.Drawing.Color.White;
            this.btnPedM13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM13.BackgroundImage")));
            this.btnPedM13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM13.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM13.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM13.ForeColor = System.Drawing.Color.Black;
            this.btnPedM13.Location = new System.Drawing.Point(191, 124);
            this.btnPedM13.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM13.Name = "btnPedM13";
            this.btnPedM13.Size = new System.Drawing.Size(60, 50);
            this.btnPedM13.TabIndex = 166;
            this.btnPedM13.Text = "13";
            this.btnPedM13.UseCompatibleTextRendering = true;
            this.btnPedM13.UseVisualStyleBackColor = false;
            this.btnPedM13.Click += new System.EventHandler(this.btnPedM13_Click);
            // 
            // btnPedC4
            // 
            this.btnPedC4.BackColor = System.Drawing.Color.White;
            this.btnPedC4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC4.BackgroundImage")));
            this.btnPedC4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC4.ForeColor = System.Drawing.Color.Black;
            this.btnPedC4.Location = new System.Drawing.Point(191, 472);
            this.btnPedC4.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC4.Name = "btnPedC4";
            this.btnPedC4.Size = new System.Drawing.Size(60, 50);
            this.btnPedC4.TabIndex = 213;
            this.btnPedC4.Text = "04";
            this.btnPedC4.UseCompatibleTextRendering = true;
            this.btnPedC4.UseVisualStyleBackColor = false;
            this.btnPedC4.Click += new System.EventHandler(this.btnPedC4_Click);
            // 
            // btnPedM14
            // 
            this.btnPedM14.BackColor = System.Drawing.Color.White;
            this.btnPedM14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM14.BackgroundImage")));
            this.btnPedM14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM14.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM14.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM14.ForeColor = System.Drawing.Color.Black;
            this.btnPedM14.Location = new System.Drawing.Point(252, 124);
            this.btnPedM14.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM14.Name = "btnPedM14";
            this.btnPedM14.Size = new System.Drawing.Size(60, 50);
            this.btnPedM14.TabIndex = 167;
            this.btnPedM14.Text = "14";
            this.btnPedM14.UseCompatibleTextRendering = true;
            this.btnPedM14.UseVisualStyleBackColor = false;
            this.btnPedM14.Click += new System.EventHandler(this.btnPedM14_Click);
            // 
            // btnPedC3
            // 
            this.btnPedC3.BackColor = System.Drawing.Color.White;
            this.btnPedC3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC3.BackgroundImage")));
            this.btnPedC3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC3.ForeColor = System.Drawing.Color.Black;
            this.btnPedC3.Location = new System.Drawing.Point(130, 472);
            this.btnPedC3.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC3.Name = "btnPedC3";
            this.btnPedC3.Size = new System.Drawing.Size(60, 50);
            this.btnPedC3.TabIndex = 212;
            this.btnPedC3.Text = "03";
            this.btnPedC3.UseCompatibleTextRendering = true;
            this.btnPedC3.UseVisualStyleBackColor = false;
            this.btnPedC3.Click += new System.EventHandler(this.btnPedC3_Click);
            // 
            // btnPedM15
            // 
            this.btnPedM15.BackColor = System.Drawing.Color.White;
            this.btnPedM15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM15.BackgroundImage")));
            this.btnPedM15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM15.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM15.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM15.ForeColor = System.Drawing.Color.Black;
            this.btnPedM15.Location = new System.Drawing.Point(313, 124);
            this.btnPedM15.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM15.Name = "btnPedM15";
            this.btnPedM15.Size = new System.Drawing.Size(60, 50);
            this.btnPedM15.TabIndex = 168;
            this.btnPedM15.Text = "15";
            this.btnPedM15.UseCompatibleTextRendering = true;
            this.btnPedM15.UseVisualStyleBackColor = false;
            this.btnPedM15.Click += new System.EventHandler(this.btnPedM15_Click);
            // 
            // btnPedC2
            // 
            this.btnPedC2.BackColor = System.Drawing.Color.White;
            this.btnPedC2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC2.BackgroundImage")));
            this.btnPedC2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC2.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC2.ForeColor = System.Drawing.Color.Black;
            this.btnPedC2.Location = new System.Drawing.Point(69, 472);
            this.btnPedC2.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC2.Name = "btnPedC2";
            this.btnPedC2.Size = new System.Drawing.Size(60, 50);
            this.btnPedC2.TabIndex = 211;
            this.btnPedC2.Text = "02";
            this.btnPedC2.UseCompatibleTextRendering = true;
            this.btnPedC2.UseVisualStyleBackColor = false;
            this.btnPedC2.Click += new System.EventHandler(this.btnPedC2_Click);
            // 
            // btnPedM16
            // 
            this.btnPedM16.BackColor = System.Drawing.Color.White;
            this.btnPedM16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM16.BackgroundImage")));
            this.btnPedM16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM16.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM16.ForeColor = System.Drawing.Color.Black;
            this.btnPedM16.Location = new System.Drawing.Point(374, 124);
            this.btnPedM16.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM16.Name = "btnPedM16";
            this.btnPedM16.Size = new System.Drawing.Size(60, 50);
            this.btnPedM16.TabIndex = 169;
            this.btnPedM16.Text = "16";
            this.btnPedM16.UseCompatibleTextRendering = true;
            this.btnPedM16.UseVisualStyleBackColor = false;
            this.btnPedM16.Click += new System.EventHandler(this.btnPedM16_Click);
            // 
            // btnPedC1
            // 
            this.btnPedC1.BackColor = System.Drawing.Color.White;
            this.btnPedC1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedC1.BackgroundImage")));
            this.btnPedC1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedC1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedC1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedC1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedC1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedC1.ForeColor = System.Drawing.Color.Black;
            this.btnPedC1.Location = new System.Drawing.Point(8, 472);
            this.btnPedC1.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedC1.Name = "btnPedC1";
            this.btnPedC1.Size = new System.Drawing.Size(60, 50);
            this.btnPedC1.TabIndex = 210;
            this.btnPedC1.Text = "01";
            this.btnPedC1.UseCompatibleTextRendering = true;
            this.btnPedC1.UseVisualStyleBackColor = false;
            this.btnPedC1.Click += new System.EventHandler(this.btnPedC1_Click);
            // 
            // btnPedM17
            // 
            this.btnPedM17.BackColor = System.Drawing.Color.White;
            this.btnPedM17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM17.BackgroundImage")));
            this.btnPedM17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM17.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM17.ForeColor = System.Drawing.Color.Black;
            this.btnPedM17.Location = new System.Drawing.Point(435, 124);
            this.btnPedM17.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM17.Name = "btnPedM17";
            this.btnPedM17.Size = new System.Drawing.Size(60, 50);
            this.btnPedM17.TabIndex = 170;
            this.btnPedM17.Text = "17";
            this.btnPedM17.UseCompatibleTextRendering = true;
            this.btnPedM17.UseVisualStyleBackColor = false;
            this.btnPedM17.Click += new System.EventHandler(this.btnPedM17_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(240, 446);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 23);
            this.label18.TabIndex = 209;
            this.label18.Text = "CARRO";
            // 
            // btnPedM18
            // 
            this.btnPedM18.BackColor = System.Drawing.Color.White;
            this.btnPedM18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM18.BackgroundImage")));
            this.btnPedM18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM18.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM18.ForeColor = System.Drawing.Color.Black;
            this.btnPedM18.Location = new System.Drawing.Point(496, 124);
            this.btnPedM18.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM18.Name = "btnPedM18";
            this.btnPedM18.Size = new System.Drawing.Size(60, 50);
            this.btnPedM18.TabIndex = 171;
            this.btnPedM18.Text = "18";
            this.btnPedM18.UseCompatibleTextRendering = true;
            this.btnPedM18.UseVisualStyleBackColor = false;
            this.btnPedM18.Click += new System.EventHandler(this.btnPedM18_Click);
            // 
            // btnPedB18
            // 
            this.btnPedB18.BackColor = System.Drawing.Color.White;
            this.btnPedB18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB18.BackgroundImage")));
            this.btnPedB18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB18.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB18.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB18.ForeColor = System.Drawing.Color.Black;
            this.btnPedB18.Location = new System.Drawing.Point(496, 376);
            this.btnPedB18.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB18.Name = "btnPedB18";
            this.btnPedB18.Size = new System.Drawing.Size(60, 50);
            this.btnPedB18.TabIndex = 208;
            this.btnPedB18.Text = "18";
            this.btnPedB18.UseCompatibleTextRendering = true;
            this.btnPedB18.UseVisualStyleBackColor = false;
            this.btnPedB18.Click += new System.EventHandler(this.btnPedB18_Click);
            // 
            // btnPedM19
            // 
            this.btnPedM19.BackColor = System.Drawing.Color.White;
            this.btnPedM19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM19.BackgroundImage")));
            this.btnPedM19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM19.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM19.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM19.ForeColor = System.Drawing.Color.Black;
            this.btnPedM19.Location = new System.Drawing.Point(8, 177);
            this.btnPedM19.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM19.Name = "btnPedM19";
            this.btnPedM19.Size = new System.Drawing.Size(60, 50);
            this.btnPedM19.TabIndex = 172;
            this.btnPedM19.Text = "19";
            this.btnPedM19.UseCompatibleTextRendering = true;
            this.btnPedM19.UseVisualStyleBackColor = false;
            this.btnPedM19.Click += new System.EventHandler(this.btnPedM19_Click);
            // 
            // btnPedB17
            // 
            this.btnPedB17.BackColor = System.Drawing.Color.White;
            this.btnPedB17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB17.BackgroundImage")));
            this.btnPedB17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB17.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB17.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB17.ForeColor = System.Drawing.Color.Black;
            this.btnPedB17.Location = new System.Drawing.Point(435, 376);
            this.btnPedB17.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB17.Name = "btnPedB17";
            this.btnPedB17.Size = new System.Drawing.Size(60, 50);
            this.btnPedB17.TabIndex = 207;
            this.btnPedB17.Text = "17";
            this.btnPedB17.UseCompatibleTextRendering = true;
            this.btnPedB17.UseVisualStyleBackColor = false;
            this.btnPedB17.Click += new System.EventHandler(this.btnPedB17_Click);
            // 
            // btnPedM20
            // 
            this.btnPedM20.BackColor = System.Drawing.Color.White;
            this.btnPedM20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM20.BackgroundImage")));
            this.btnPedM20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM20.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM20.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM20.ForeColor = System.Drawing.Color.Black;
            this.btnPedM20.Location = new System.Drawing.Point(69, 177);
            this.btnPedM20.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM20.Name = "btnPedM20";
            this.btnPedM20.Size = new System.Drawing.Size(60, 50);
            this.btnPedM20.TabIndex = 173;
            this.btnPedM20.Text = "20";
            this.btnPedM20.UseCompatibleTextRendering = true;
            this.btnPedM20.UseVisualStyleBackColor = false;
            this.btnPedM20.Click += new System.EventHandler(this.btnPedM20_Click);
            // 
            // btnPedB16
            // 
            this.btnPedB16.BackColor = System.Drawing.Color.White;
            this.btnPedB16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB16.BackgroundImage")));
            this.btnPedB16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB16.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB16.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB16.ForeColor = System.Drawing.Color.Black;
            this.btnPedB16.Location = new System.Drawing.Point(374, 376);
            this.btnPedB16.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB16.Name = "btnPedB16";
            this.btnPedB16.Size = new System.Drawing.Size(60, 50);
            this.btnPedB16.TabIndex = 206;
            this.btnPedB16.Text = "16";
            this.btnPedB16.UseCompatibleTextRendering = true;
            this.btnPedB16.UseVisualStyleBackColor = false;
            this.btnPedB16.Click += new System.EventHandler(this.btnPedB16_Click);
            // 
            // btnPedM21
            // 
            this.btnPedM21.BackColor = System.Drawing.Color.White;
            this.btnPedM21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM21.BackgroundImage")));
            this.btnPedM21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM21.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM21.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM21.ForeColor = System.Drawing.Color.Black;
            this.btnPedM21.Location = new System.Drawing.Point(130, 177);
            this.btnPedM21.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM21.Name = "btnPedM21";
            this.btnPedM21.Size = new System.Drawing.Size(60, 50);
            this.btnPedM21.TabIndex = 174;
            this.btnPedM21.Text = "21";
            this.btnPedM21.UseCompatibleTextRendering = true;
            this.btnPedM21.UseVisualStyleBackColor = false;
            this.btnPedM21.Click += new System.EventHandler(this.btnPedM21_Click);
            // 
            // btnPedB15
            // 
            this.btnPedB15.BackColor = System.Drawing.Color.White;
            this.btnPedB15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB15.BackgroundImage")));
            this.btnPedB15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB15.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB15.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB15.ForeColor = System.Drawing.Color.Black;
            this.btnPedB15.Location = new System.Drawing.Point(313, 376);
            this.btnPedB15.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB15.Name = "btnPedB15";
            this.btnPedB15.Size = new System.Drawing.Size(60, 50);
            this.btnPedB15.TabIndex = 205;
            this.btnPedB15.Text = "15";
            this.btnPedB15.UseCompatibleTextRendering = true;
            this.btnPedB15.UseVisualStyleBackColor = false;
            this.btnPedB15.Click += new System.EventHandler(this.btnPedB15_Click);
            // 
            // btnPedM22
            // 
            this.btnPedM22.BackColor = System.Drawing.Color.White;
            this.btnPedM22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM22.BackgroundImage")));
            this.btnPedM22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM22.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM22.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM22.ForeColor = System.Drawing.Color.Black;
            this.btnPedM22.Location = new System.Drawing.Point(191, 177);
            this.btnPedM22.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM22.Name = "btnPedM22";
            this.btnPedM22.Size = new System.Drawing.Size(60, 50);
            this.btnPedM22.TabIndex = 175;
            this.btnPedM22.Text = "22";
            this.btnPedM22.UseCompatibleTextRendering = true;
            this.btnPedM22.UseVisualStyleBackColor = false;
            this.btnPedM22.Click += new System.EventHandler(this.btnPedM22_Click);
            // 
            // btnPedB14
            // 
            this.btnPedB14.BackColor = System.Drawing.Color.White;
            this.btnPedB14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB14.BackgroundImage")));
            this.btnPedB14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB14.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB14.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB14.ForeColor = System.Drawing.Color.Black;
            this.btnPedB14.Location = new System.Drawing.Point(252, 376);
            this.btnPedB14.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB14.Name = "btnPedB14";
            this.btnPedB14.Size = new System.Drawing.Size(60, 50);
            this.btnPedB14.TabIndex = 204;
            this.btnPedB14.Text = "14";
            this.btnPedB14.UseCompatibleTextRendering = true;
            this.btnPedB14.UseVisualStyleBackColor = false;
            this.btnPedB14.Click += new System.EventHandler(this.btnPedB14_Click);
            // 
            // btnPedM23
            // 
            this.btnPedM23.BackColor = System.Drawing.Color.White;
            this.btnPedM23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM23.BackgroundImage")));
            this.btnPedM23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM23.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM23.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM23.ForeColor = System.Drawing.Color.Black;
            this.btnPedM23.Location = new System.Drawing.Point(252, 177);
            this.btnPedM23.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM23.Name = "btnPedM23";
            this.btnPedM23.Size = new System.Drawing.Size(60, 50);
            this.btnPedM23.TabIndex = 176;
            this.btnPedM23.Text = "23";
            this.btnPedM23.UseCompatibleTextRendering = true;
            this.btnPedM23.UseVisualStyleBackColor = false;
            this.btnPedM23.Click += new System.EventHandler(this.btnPedM23_Click);
            // 
            // btnPedB13
            // 
            this.btnPedB13.BackColor = System.Drawing.Color.White;
            this.btnPedB13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB13.BackgroundImage")));
            this.btnPedB13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB13.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB13.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB13.ForeColor = System.Drawing.Color.Black;
            this.btnPedB13.Location = new System.Drawing.Point(191, 376);
            this.btnPedB13.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB13.Name = "btnPedB13";
            this.btnPedB13.Size = new System.Drawing.Size(60, 50);
            this.btnPedB13.TabIndex = 203;
            this.btnPedB13.Text = "13";
            this.btnPedB13.UseCompatibleTextRendering = true;
            this.btnPedB13.UseVisualStyleBackColor = false;
            this.btnPedB13.Click += new System.EventHandler(this.btnPedB13_Click);
            // 
            // btnPedM24
            // 
            this.btnPedM24.BackColor = System.Drawing.Color.White;
            this.btnPedM24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM24.BackgroundImage")));
            this.btnPedM24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM24.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM24.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM24.ForeColor = System.Drawing.Color.Black;
            this.btnPedM24.Location = new System.Drawing.Point(313, 177);
            this.btnPedM24.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM24.Name = "btnPedM24";
            this.btnPedM24.Size = new System.Drawing.Size(60, 50);
            this.btnPedM24.TabIndex = 177;
            this.btnPedM24.Text = "24";
            this.btnPedM24.UseCompatibleTextRendering = true;
            this.btnPedM24.UseVisualStyleBackColor = false;
            this.btnPedM24.Click += new System.EventHandler(this.btnPedM24_Click);
            // 
            // btnPedB12
            // 
            this.btnPedB12.BackColor = System.Drawing.Color.White;
            this.btnPedB12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB12.BackgroundImage")));
            this.btnPedB12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB12.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB12.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB12.ForeColor = System.Drawing.Color.Black;
            this.btnPedB12.Location = new System.Drawing.Point(130, 376);
            this.btnPedB12.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB12.Name = "btnPedB12";
            this.btnPedB12.Size = new System.Drawing.Size(60, 50);
            this.btnPedB12.TabIndex = 202;
            this.btnPedB12.Text = "12";
            this.btnPedB12.UseCompatibleTextRendering = true;
            this.btnPedB12.UseVisualStyleBackColor = false;
            this.btnPedB12.Click += new System.EventHandler(this.btnPedB12_Click);
            // 
            // btnPedM25
            // 
            this.btnPedM25.BackColor = System.Drawing.Color.White;
            this.btnPedM25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM25.BackgroundImage")));
            this.btnPedM25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM25.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM25.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM25.ForeColor = System.Drawing.Color.Black;
            this.btnPedM25.Location = new System.Drawing.Point(374, 177);
            this.btnPedM25.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM25.Name = "btnPedM25";
            this.btnPedM25.Size = new System.Drawing.Size(60, 50);
            this.btnPedM25.TabIndex = 178;
            this.btnPedM25.Text = "25";
            this.btnPedM25.UseCompatibleTextRendering = true;
            this.btnPedM25.UseVisualStyleBackColor = false;
            this.btnPedM25.Click += new System.EventHandler(this.btnPedM25_Click);
            // 
            // btnPedB11
            // 
            this.btnPedB11.BackColor = System.Drawing.Color.White;
            this.btnPedB11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB11.BackgroundImage")));
            this.btnPedB11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB11.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB11.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB11.ForeColor = System.Drawing.Color.Black;
            this.btnPedB11.Location = new System.Drawing.Point(69, 376);
            this.btnPedB11.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB11.Name = "btnPedB11";
            this.btnPedB11.Size = new System.Drawing.Size(60, 50);
            this.btnPedB11.TabIndex = 201;
            this.btnPedB11.Text = "11";
            this.btnPedB11.UseCompatibleTextRendering = true;
            this.btnPedB11.UseVisualStyleBackColor = false;
            this.btnPedB11.Click += new System.EventHandler(this.btnPedB11_Click);
            // 
            // btnPedM26
            // 
            this.btnPedM26.BackColor = System.Drawing.Color.White;
            this.btnPedM26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM26.BackgroundImage")));
            this.btnPedM26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM26.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM26.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM26.ForeColor = System.Drawing.Color.Black;
            this.btnPedM26.Location = new System.Drawing.Point(435, 177);
            this.btnPedM26.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM26.Name = "btnPedM26";
            this.btnPedM26.Size = new System.Drawing.Size(60, 50);
            this.btnPedM26.TabIndex = 179;
            this.btnPedM26.Text = "26";
            this.btnPedM26.UseCompatibleTextRendering = true;
            this.btnPedM26.UseVisualStyleBackColor = false;
            this.btnPedM26.Click += new System.EventHandler(this.btnPedM26_Click);
            // 
            // btnPedB10
            // 
            this.btnPedB10.BackColor = System.Drawing.Color.White;
            this.btnPedB10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB10.BackgroundImage")));
            this.btnPedB10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB10.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB10.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB10.ForeColor = System.Drawing.Color.Black;
            this.btnPedB10.Location = new System.Drawing.Point(8, 376);
            this.btnPedB10.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB10.Name = "btnPedB10";
            this.btnPedB10.Size = new System.Drawing.Size(60, 50);
            this.btnPedB10.TabIndex = 200;
            this.btnPedB10.Text = "10";
            this.btnPedB10.UseCompatibleTextRendering = true;
            this.btnPedB10.UseVisualStyleBackColor = false;
            this.btnPedB10.Click += new System.EventHandler(this.btnPedB10_Click);
            // 
            // btnPedM27
            // 
            this.btnPedM27.BackColor = System.Drawing.Color.White;
            this.btnPedM27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM27.BackgroundImage")));
            this.btnPedM27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM27.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM27.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM27.ForeColor = System.Drawing.Color.Black;
            this.btnPedM27.Location = new System.Drawing.Point(496, 177);
            this.btnPedM27.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM27.Name = "btnPedM27";
            this.btnPedM27.Size = new System.Drawing.Size(60, 50);
            this.btnPedM27.TabIndex = 180;
            this.btnPedM27.Text = "27";
            this.btnPedM27.UseCompatibleTextRendering = true;
            this.btnPedM27.UseVisualStyleBackColor = false;
            this.btnPedM27.Click += new System.EventHandler(this.btnPedM27_Click);
            // 
            // btnPedB9
            // 
            this.btnPedB9.BackColor = System.Drawing.Color.White;
            this.btnPedB9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB9.BackgroundImage")));
            this.btnPedB9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB9.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB9.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB9.ForeColor = System.Drawing.Color.Black;
            this.btnPedB9.Location = new System.Drawing.Point(496, 322);
            this.btnPedB9.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB9.Name = "btnPedB9";
            this.btnPedB9.Size = new System.Drawing.Size(60, 50);
            this.btnPedB9.TabIndex = 199;
            this.btnPedB9.Text = "09";
            this.btnPedB9.UseCompatibleTextRendering = true;
            this.btnPedB9.UseVisualStyleBackColor = false;
            this.btnPedB9.Click += new System.EventHandler(this.btnPedB9_Click);
            // 
            // btnPedM28
            // 
            this.btnPedM28.BackColor = System.Drawing.Color.White;
            this.btnPedM28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM28.BackgroundImage")));
            this.btnPedM28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM28.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM28.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM28.ForeColor = System.Drawing.Color.Black;
            this.btnPedM28.Location = new System.Drawing.Point(8, 230);
            this.btnPedM28.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM28.Name = "btnPedM28";
            this.btnPedM28.Size = new System.Drawing.Size(60, 50);
            this.btnPedM28.TabIndex = 181;
            this.btnPedM28.Text = "28";
            this.btnPedM28.UseCompatibleTextRendering = true;
            this.btnPedM28.UseVisualStyleBackColor = false;
            this.btnPedM28.Click += new System.EventHandler(this.btnPedM28_Click);
            // 
            // btnPedB8
            // 
            this.btnPedB8.BackColor = System.Drawing.Color.White;
            this.btnPedB8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB8.BackgroundImage")));
            this.btnPedB8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB8.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB8.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB8.ForeColor = System.Drawing.Color.Black;
            this.btnPedB8.Location = new System.Drawing.Point(435, 322);
            this.btnPedB8.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB8.Name = "btnPedB8";
            this.btnPedB8.Size = new System.Drawing.Size(60, 50);
            this.btnPedB8.TabIndex = 198;
            this.btnPedB8.Text = "08";
            this.btnPedB8.UseCompatibleTextRendering = true;
            this.btnPedB8.UseVisualStyleBackColor = false;
            this.btnPedB8.Click += new System.EventHandler(this.btnPedB8_Click);
            // 
            // btnPedM29
            // 
            this.btnPedM29.BackColor = System.Drawing.Color.White;
            this.btnPedM29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM29.BackgroundImage")));
            this.btnPedM29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM29.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM29.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM29.ForeColor = System.Drawing.Color.Black;
            this.btnPedM29.Location = new System.Drawing.Point(69, 230);
            this.btnPedM29.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM29.Name = "btnPedM29";
            this.btnPedM29.Size = new System.Drawing.Size(60, 50);
            this.btnPedM29.TabIndex = 182;
            this.btnPedM29.Text = "29";
            this.btnPedM29.UseCompatibleTextRendering = true;
            this.btnPedM29.UseVisualStyleBackColor = false;
            this.btnPedM29.Click += new System.EventHandler(this.btnPedM29_Click);
            // 
            // btnPedB7
            // 
            this.btnPedB7.BackColor = System.Drawing.Color.White;
            this.btnPedB7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB7.BackgroundImage")));
            this.btnPedB7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB7.ForeColor = System.Drawing.Color.Black;
            this.btnPedB7.Location = new System.Drawing.Point(374, 322);
            this.btnPedB7.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB7.Name = "btnPedB7";
            this.btnPedB7.Size = new System.Drawing.Size(60, 50);
            this.btnPedB7.TabIndex = 197;
            this.btnPedB7.Text = "07";
            this.btnPedB7.UseCompatibleTextRendering = true;
            this.btnPedB7.UseVisualStyleBackColor = false;
            this.btnPedB7.Click += new System.EventHandler(this.btnPedB7_Click);
            // 
            // btnPedM30
            // 
            this.btnPedM30.BackColor = System.Drawing.Color.White;
            this.btnPedM30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM30.BackgroundImage")));
            this.btnPedM30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM30.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM30.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM30.ForeColor = System.Drawing.Color.Black;
            this.btnPedM30.Location = new System.Drawing.Point(130, 230);
            this.btnPedM30.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM30.Name = "btnPedM30";
            this.btnPedM30.Size = new System.Drawing.Size(60, 50);
            this.btnPedM30.TabIndex = 183;
            this.btnPedM30.Text = "30";
            this.btnPedM30.UseCompatibleTextRendering = true;
            this.btnPedM30.UseVisualStyleBackColor = false;
            this.btnPedM30.Click += new System.EventHandler(this.btnPedM30_Click);
            // 
            // btnPedB6
            // 
            this.btnPedB6.BackColor = System.Drawing.Color.White;
            this.btnPedB6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB6.BackgroundImage")));
            this.btnPedB6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB6.ForeColor = System.Drawing.Color.Black;
            this.btnPedB6.Location = new System.Drawing.Point(313, 322);
            this.btnPedB6.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB6.Name = "btnPedB6";
            this.btnPedB6.Size = new System.Drawing.Size(60, 50);
            this.btnPedB6.TabIndex = 196;
            this.btnPedB6.Text = "06";
            this.btnPedB6.UseCompatibleTextRendering = true;
            this.btnPedB6.UseVisualStyleBackColor = false;
            this.btnPedB6.Click += new System.EventHandler(this.btnPedB6_Click);
            // 
            // btnPedM31
            // 
            this.btnPedM31.BackColor = System.Drawing.Color.White;
            this.btnPedM31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM31.BackgroundImage")));
            this.btnPedM31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM31.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM31.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM31.ForeColor = System.Drawing.Color.Black;
            this.btnPedM31.Location = new System.Drawing.Point(191, 230);
            this.btnPedM31.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM31.Name = "btnPedM31";
            this.btnPedM31.Size = new System.Drawing.Size(60, 50);
            this.btnPedM31.TabIndex = 184;
            this.btnPedM31.Text = "31";
            this.btnPedM31.UseCompatibleTextRendering = true;
            this.btnPedM31.UseVisualStyleBackColor = false;
            this.btnPedM31.Click += new System.EventHandler(this.btnPedM31_Click);
            // 
            // btnPedB5
            // 
            this.btnPedB5.BackColor = System.Drawing.Color.White;
            this.btnPedB5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB5.BackgroundImage")));
            this.btnPedB5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB5.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB5.ForeColor = System.Drawing.Color.Black;
            this.btnPedB5.Location = new System.Drawing.Point(252, 322);
            this.btnPedB5.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB5.Name = "btnPedB5";
            this.btnPedB5.Size = new System.Drawing.Size(60, 50);
            this.btnPedB5.TabIndex = 195;
            this.btnPedB5.Text = "05";
            this.btnPedB5.UseCompatibleTextRendering = true;
            this.btnPedB5.UseVisualStyleBackColor = false;
            this.btnPedB5.Click += new System.EventHandler(this.btnPedB5_Click);
            // 
            // btnPedM32
            // 
            this.btnPedM32.BackColor = System.Drawing.Color.White;
            this.btnPedM32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM32.BackgroundImage")));
            this.btnPedM32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM32.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM32.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM32.ForeColor = System.Drawing.Color.Black;
            this.btnPedM32.Location = new System.Drawing.Point(252, 230);
            this.btnPedM32.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM32.Name = "btnPedM32";
            this.btnPedM32.Size = new System.Drawing.Size(60, 50);
            this.btnPedM32.TabIndex = 185;
            this.btnPedM32.Text = "32";
            this.btnPedM32.UseCompatibleTextRendering = true;
            this.btnPedM32.UseVisualStyleBackColor = false;
            this.btnPedM32.Click += new System.EventHandler(this.btnPedM32_Click);
            // 
            // btnPedB4
            // 
            this.btnPedB4.BackColor = System.Drawing.Color.White;
            this.btnPedB4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB4.BackgroundImage")));
            this.btnPedB4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB4.ForeColor = System.Drawing.Color.Black;
            this.btnPedB4.Location = new System.Drawing.Point(191, 322);
            this.btnPedB4.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB4.Name = "btnPedB4";
            this.btnPedB4.Size = new System.Drawing.Size(60, 50);
            this.btnPedB4.TabIndex = 194;
            this.btnPedB4.Text = "04";
            this.btnPedB4.UseCompatibleTextRendering = true;
            this.btnPedB4.UseVisualStyleBackColor = false;
            this.btnPedB4.Click += new System.EventHandler(this.btnPedB4_Click);
            // 
            // btnPedM33
            // 
            this.btnPedM33.BackColor = System.Drawing.Color.White;
            this.btnPedM33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM33.BackgroundImage")));
            this.btnPedM33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM33.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM33.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM33.ForeColor = System.Drawing.Color.Black;
            this.btnPedM33.Location = new System.Drawing.Point(313, 230);
            this.btnPedM33.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM33.Name = "btnPedM33";
            this.btnPedM33.Size = new System.Drawing.Size(60, 50);
            this.btnPedM33.TabIndex = 186;
            this.btnPedM33.Text = "33";
            this.btnPedM33.UseCompatibleTextRendering = true;
            this.btnPedM33.UseVisualStyleBackColor = false;
            this.btnPedM33.Click += new System.EventHandler(this.btnPedM33_Click);
            // 
            // btnPedB3
            // 
            this.btnPedB3.BackColor = System.Drawing.Color.White;
            this.btnPedB3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB3.BackgroundImage")));
            this.btnPedB3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB3.ForeColor = System.Drawing.Color.Black;
            this.btnPedB3.Location = new System.Drawing.Point(130, 322);
            this.btnPedB3.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB3.Name = "btnPedB3";
            this.btnPedB3.Size = new System.Drawing.Size(60, 50);
            this.btnPedB3.TabIndex = 193;
            this.btnPedB3.Text = "03";
            this.btnPedB3.UseCompatibleTextRendering = true;
            this.btnPedB3.UseVisualStyleBackColor = false;
            this.btnPedB3.Click += new System.EventHandler(this.btnPedB3_Click);
            // 
            // btnPedM34
            // 
            this.btnPedM34.BackColor = System.Drawing.Color.White;
            this.btnPedM34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM34.BackgroundImage")));
            this.btnPedM34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM34.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM34.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM34.ForeColor = System.Drawing.Color.Black;
            this.btnPedM34.Location = new System.Drawing.Point(374, 230);
            this.btnPedM34.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM34.Name = "btnPedM34";
            this.btnPedM34.Size = new System.Drawing.Size(60, 50);
            this.btnPedM34.TabIndex = 187;
            this.btnPedM34.Text = "34";
            this.btnPedM34.UseCompatibleTextRendering = true;
            this.btnPedM34.UseVisualStyleBackColor = false;
            this.btnPedM34.Click += new System.EventHandler(this.btnPedM34_Click);
            // 
            // btnPedB2
            // 
            this.btnPedB2.BackColor = System.Drawing.Color.White;
            this.btnPedB2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB2.BackgroundImage")));
            this.btnPedB2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB2.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB2.ForeColor = System.Drawing.Color.Black;
            this.btnPedB2.Location = new System.Drawing.Point(69, 322);
            this.btnPedB2.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB2.Name = "btnPedB2";
            this.btnPedB2.Size = new System.Drawing.Size(60, 50);
            this.btnPedB2.TabIndex = 192;
            this.btnPedB2.Text = "02";
            this.btnPedB2.UseCompatibleTextRendering = true;
            this.btnPedB2.UseVisualStyleBackColor = false;
            this.btnPedB2.Click += new System.EventHandler(this.btnPedB2_Click);
            // 
            // btnPedM35
            // 
            this.btnPedM35.BackColor = System.Drawing.Color.White;
            this.btnPedM35.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM35.BackgroundImage")));
            this.btnPedM35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM35.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM35.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM35.ForeColor = System.Drawing.Color.Black;
            this.btnPedM35.Location = new System.Drawing.Point(435, 230);
            this.btnPedM35.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM35.Name = "btnPedM35";
            this.btnPedM35.Size = new System.Drawing.Size(60, 50);
            this.btnPedM35.TabIndex = 188;
            this.btnPedM35.Text = "35";
            this.btnPedM35.UseCompatibleTextRendering = true;
            this.btnPedM35.UseVisualStyleBackColor = false;
            this.btnPedM35.Click += new System.EventHandler(this.btnPedM35_Click);
            // 
            // btnPedB1
            // 
            this.btnPedB1.BackColor = System.Drawing.Color.White;
            this.btnPedB1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedB1.BackgroundImage")));
            this.btnPedB1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedB1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedB1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedB1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedB1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedB1.ForeColor = System.Drawing.Color.Black;
            this.btnPedB1.Location = new System.Drawing.Point(8, 322);
            this.btnPedB1.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedB1.Name = "btnPedB1";
            this.btnPedB1.Size = new System.Drawing.Size(60, 50);
            this.btnPedB1.TabIndex = 191;
            this.btnPedB1.Text = "01";
            this.btnPedB1.UseCompatibleTextRendering = true;
            this.btnPedB1.UseVisualStyleBackColor = false;
            this.btnPedB1.Click += new System.EventHandler(this.btnPedB1_Click);
            // 
            // btnPedM36
            // 
            this.btnPedM36.BackColor = System.Drawing.Color.White;
            this.btnPedM36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPedM36.BackgroundImage")));
            this.btnPedM36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPedM36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedM36.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnPedM36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedM36.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedM36.ForeColor = System.Drawing.Color.Black;
            this.btnPedM36.Location = new System.Drawing.Point(496, 230);
            this.btnPedM36.Margin = new System.Windows.Forms.Padding(0);
            this.btnPedM36.Name = "btnPedM36";
            this.btnPedM36.Size = new System.Drawing.Size(60, 50);
            this.btnPedM36.TabIndex = 189;
            this.btnPedM36.Text = "36";
            this.btnPedM36.UseCompatibleTextRendering = true;
            this.btnPedM36.UseVisualStyleBackColor = false;
            this.btnPedM36.Click += new System.EventHandler(this.btnPedM36_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label15.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(240, 296);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 23);
            this.label15.TabIndex = 190;
            this.label15.Text = "BALCÃO";
            // 
            // lblAtendimento
            // 
            // 
            // 
            // 
            this.lblAtendimento.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAtendimento.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendimento.Location = new System.Drawing.Point(7, 8);
            this.lblAtendimento.Name = "lblAtendimento";
            this.lblAtendimento.Size = new System.Drawing.Size(287, 23);
            this.lblAtendimento.TabIndex = 17;
            this.lblAtendimento.Text = "Atendimento:";
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.panel16);
            this.panelEx2.Controls.Add(this.dgCupom);
            this.panelEx2.Location = new System.Drawing.Point(569, 0);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(502, 470);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.Color = System.Drawing.Color.White;
            this.panelEx2.Style.BackColor2.Color = System.Drawing.Color.White;
            this.panelEx2.Style.BorderColor.Color = System.Drawing.Color.LightGray;
            this.panelEx2.Style.BorderWidth = 2;
            this.panelEx2.Style.CornerDiameter = 0;
            this.panelEx2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 9;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.White;
            this.panel16.Controls.Add(this.lblAtendente);
            this.panel16.Controls.Add(this.lblCodVenda);
            this.panel16.Controls.Add(this.lblAtendimentoCupom);
            this.panel16.Controls.Add(this.label8);
            this.panel16.Controls.Add(this.lblNomeEmpresa);
            this.panel16.Controls.Add(this.label10);
            this.panel16.Controls.Add(this.label12);
            this.panel16.Controls.Add(this.label11);
            this.panel16.Controls.Add(this.label9);
            this.panel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel16.Location = new System.Drawing.Point(3, 3);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(496, 133);
            this.panel16.TabIndex = 7;
            // 
            // lblAtendente
            // 
            this.lblAtendente.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendente.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblAtendente.Location = new System.Drawing.Point(246, 17);
            this.lblAtendente.Name = "lblAtendente";
            this.lblAtendente.Size = new System.Drawing.Size(244, 19);
            this.lblAtendente.TabIndex = 9;
            this.lblAtendente.Text = "Atendente:";
            this.lblAtendente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCodVenda
            // 
            this.lblCodVenda.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodVenda.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCodVenda.Location = new System.Drawing.Point(3, 38);
            this.lblCodVenda.Name = "lblCodVenda";
            this.lblCodVenda.Size = new System.Drawing.Size(456, 14);
            this.lblCodVenda.TabIndex = 8;
            this.lblCodVenda.Text = "Numero Venda: ";
            this.lblCodVenda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAtendimentoCupom
            // 
            this.lblAtendimentoCupom.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendimentoCupom.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblAtendimentoCupom.Location = new System.Drawing.Point(3, 19);
            this.lblAtendimentoCupom.Name = "lblAtendimentoCupom";
            this.lblAtendimentoCupom.Size = new System.Drawing.Size(230, 14);
            this.lblAtendimentoCupom.TabIndex = 7;
            this.lblAtendimentoCupom.Text = "Atendimento:  ";
            this.lblAtendimentoCupom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(5, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(485, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "CUPOM NÃO FISCAL";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNomeEmpresa
            // 
            this.lblNomeEmpresa.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeEmpresa.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNomeEmpresa.Location = new System.Drawing.Point(3, 0);
            this.lblNomeEmpresa.Name = "lblNomeEmpresa";
            this.lblNomeEmpresa.Size = new System.Drawing.Size(487, 18);
            this.lblNomeEmpresa.TabIndex = 6;
            this.lblNomeEmpresa.Text = "Nome da Empresa";
            this.lblNomeEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(2, 117);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(488, 18);
            this.label10.TabIndex = 5;
            this.label10.Text = "------------------------------------------------";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(3, 102);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(448, 18);
            this.label12.TabIndex = 4;
            this.label12.Text = "  Quant Preço Unit                      Item";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(3, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(458, 18);
            this.label11.TabIndex = 3;
            this.label11.Text = "Item   Código      Descrição            Total";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(3, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(488, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "------------------------------------------------";
            // 
            // dgCupom
            // 
            this.dgCupom.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dgCupom.AllowUserToAddRows = false;
            this.dgCupom.AllowUserToDeleteRows = false;
            this.dgCupom.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgCupom.BackgroundColor = System.Drawing.Color.White;
            this.dgCupom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgCupom.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgCupom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCupom.ColumnHeadersVisible = false;
            this.dgCupom.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.Column2,
            this.Column3,
            this.Column5,
            this.extras,
            this.especiais,
            this.status,
            this.dataEnvioVendaDetalhes,
            this.idVendaDetalhes});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCupom.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgCupom.EnableHeadersVisualStyles = false;
            this.dgCupom.GridColor = System.Drawing.Color.White;
            this.dgCupom.Location = new System.Drawing.Point(4, 135);
            this.dgCupom.Margin = new System.Windows.Forms.Padding(0);
            this.dgCupom.MultiSelect = false;
            this.dgCupom.Name = "dgCupom";
            this.dgCupom.ReadOnly = true;
            this.dgCupom.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCupom.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgCupom.RowHeadersVisible = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HotTrack;
            this.dgCupom.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgCupom.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgCupom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCupom.Size = new System.Drawing.Size(495, 323);
            this.dgCupom.TabIndex = 6;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 27;
            // 
            // Column4
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 40;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.MaxInputLength = 32;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 225;
            // 
            // Column5
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column5.HeaderText = "";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 92;
            // 
            // extras
            // 
            this.extras.HeaderText = "";
            this.extras.Name = "extras";
            this.extras.ReadOnly = true;
            this.extras.Visible = false;
            // 
            // especiais
            // 
            this.especiais.HeaderText = "";
            this.especiais.Name = "especiais";
            this.especiais.ReadOnly = true;
            this.especiais.Visible = false;
            // 
            // status
            // 
            this.status.HeaderText = "";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Visible = false;
            // 
            // dataEnvioVendaDetalhes
            // 
            this.dataEnvioVendaDetalhes.HeaderText = "";
            this.dataEnvioVendaDetalhes.Name = "dataEnvioVendaDetalhes";
            this.dataEnvioVendaDetalhes.ReadOnly = true;
            this.dataEnvioVendaDetalhes.Visible = false;
            // 
            // idVendaDetalhes
            // 
            this.idVendaDetalhes.HeaderText = "";
            this.idVendaDetalhes.Name = "idVendaDetalhes";
            this.idVendaDetalhes.ReadOnly = true;
            this.idVendaDetalhes.Visible = false;
            // 
            // lblDataHora
            // 
            this.lblDataHora.BackColor = System.Drawing.Color.Transparent;
            this.lblDataHora.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataHora.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblDataHora.Location = new System.Drawing.Point(300, 10);
            this.lblDataHora.Name = "lblDataHora";
            this.lblDataHora.Size = new System.Drawing.Size(263, 18);
            this.lblDataHora.TabIndex = 16;
            this.lblDataHora.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.panel17);
            this.panel7.Controls.Add(this.panel13);
            this.panel7.Controls.Add(this.panel12);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Location = new System.Drawing.Point(1081, 1);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(266, 471);
            this.panel7.TabIndex = 11;
            // 
            // panel17
            // 
            this.panel17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel17.BackColor = System.Drawing.Color.Blue;
            this.panel17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel17.Controls.Add(this.txtTotalGeral);
            this.panel17.Controls.Add(this.label13);
            this.panel17.Location = new System.Drawing.Point(5, 400);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(255, 68);
            this.panel17.TabIndex = 14;
            // 
            // txtTotalGeral
            // 
            this.txtTotalGeral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalGeral.BackColor = System.Drawing.Color.Blue;
            this.txtTotalGeral.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalGeral.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalGeral.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalGeral.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtTotalGeral.Location = new System.Drawing.Point(12, 17);
            this.txtTotalGeral.Name = "txtTotalGeral";
            this.txtTotalGeral.ReadOnly = true;
            this.txtTotalGeral.Size = new System.Drawing.Size(232, 39);
            this.txtTotalGeral.TabIndex = 3;
            this.txtTotalGeral.Text = "0,00";
            this.txtTotalGeral.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(12, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 16);
            this.label13.TabIndex = 1;
            this.label13.Text = "Total Geral";
            // 
            // panel13
            // 
            this.panel13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel13.BackColor = System.Drawing.Color.Blue;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel13.Controls.Add(this.txtItens);
            this.panel13.Controls.Add(this.label7);
            this.panel13.Location = new System.Drawing.Point(5, 315);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(255, 68);
            this.panel13.TabIndex = 13;
            // 
            // txtItens
            // 
            this.txtItens.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItens.BackColor = System.Drawing.Color.Blue;
            this.txtItens.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItens.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtItens.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItens.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtItens.Location = new System.Drawing.Point(12, 20);
            this.txtItens.Name = "txtItens";
            this.txtItens.ReadOnly = true;
            this.txtItens.Size = new System.Drawing.Size(232, 39);
            this.txtItens.TabIndex = 2;
            this.txtItens.Text = "0";
            this.txtItens.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(12, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "Itens";
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel12.BackColor = System.Drawing.Color.Blue;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.label6);
            this.panel12.Controls.Add(this.txtValorTotal);
            this.panel12.Location = new System.Drawing.Point(5, 237);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(255, 68);
            this.panel12.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(12, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "Valor Total";
            // 
            // txtValorTotal
            // 
            this.txtValorTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorTotal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtValorTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtValorTotal.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorTotal.Location = new System.Drawing.Point(12, 18);
            this.txtValorTotal.Name = "txtValorTotal";
            this.txtValorTotal.ReadOnly = true;
            this.txtValorTotal.Size = new System.Drawing.Size(232, 43);
            this.txtValorTotal.TabIndex = 0;
            this.txtValorTotal.Text = "0,00";
            this.txtValorTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.BackColor = System.Drawing.Color.Blue;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.label5);
            this.panel11.Controls.Add(this.txtValorUnitario);
            this.panel11.Location = new System.Drawing.Point(5, 159);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(255, 68);
            this.panel11.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(12, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "Valor Unitário";
            // 
            // txtValorUnitario
            // 
            this.txtValorUnitario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorUnitario.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtValorUnitario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtValorUnitario.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorUnitario.Location = new System.Drawing.Point(12, 18);
            this.txtValorUnitario.Name = "txtValorUnitario";
            this.txtValorUnitario.ReadOnly = true;
            this.txtValorUnitario.Size = new System.Drawing.Size(232, 43);
            this.txtValorUnitario.TabIndex = 0;
            this.txtValorUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.BackColor = System.Drawing.Color.Blue;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.txtQuantidade);
            this.panel10.Location = new System.Drawing.Point(5, 81);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(255, 68);
            this.panel10.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(12, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Quantidade";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuantidade.BackColor = System.Drawing.SystemColors.Window;
            this.txtQuantidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtQuantidade.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantidade.Location = new System.Drawing.Point(12, 18);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(232, 43);
            this.txtQuantidade.TabIndex = 0;
            this.txtQuantidade.Text = "1";
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQuantidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuantidade_KeyDown);
            this.txtQuantidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantidade_KeyPress);
            this.txtQuantidade.Leave += new System.EventHandler(this.txtQuantidade_Leave);
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.Color.Blue;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.txtCodigo);
            this.panel9.Location = new System.Drawing.Point(5, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(255, 68);
            this.panel9.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(12, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Código";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigo.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(12, 18);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(232, 43);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.btnImprimir);
            this.panel8.Controls.Add(this.btnExtra);
            this.panel8.Controls.Add(this.btnFinalizarPedido);
            this.panel8.Controls.Add(this.btnEspecial);
            this.panel8.Controls.Add(this.btnVisualizarPedido);
            this.panel8.Controls.Add(this.btnEnviarPedido);
            this.panel8.Controls.Add(this.btnCancelarPedido);
            this.panel8.Controls.Add(this.btnCancelarItem);
            this.panel8.Controls.Add(this.btnAddQtd);
            this.panel8.Controls.Add(this.btnPesquisarProdutos);
            this.panel8.Location = new System.Drawing.Point(566, 468);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(784, 109);
            this.panel8.TabIndex = 14;
            // 
            // btnImprimir
            // 
            this.btnImprimir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnImprimir.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnImprimir.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImprimir.Image = global::UI.Properties.Resources.printer;
            this.btnImprimir.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnImprimir.Location = new System.Drawing.Point(470, 63);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(150, 44);
            this.btnImprimir.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnImprimir.TabIndex = 17;
            this.btnImprimir.Text = "Imprimir - F9";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnExtra
            // 
            this.btnExtra.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExtra.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnExtra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExtra.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnExtra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExtra.Image = global::UI.Properties.Resources._7393_128x1281;
            this.btnExtra.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnExtra.Location = new System.Drawing.Point(159, 63);
            this.btnExtra.Name = "btnExtra";
            this.btnExtra.Size = new System.Drawing.Size(150, 44);
            this.btnExtra.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnExtra.TabIndex = 16;
            this.btnExtra.Text = "   Extras - F7";
            this.btnExtra.Click += new System.EventHandler(this.btnExtra_Click);
            // 
            // btnFinalizarPedido
            // 
            this.btnFinalizarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFinalizarPedido.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnFinalizarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFinalizarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnFinalizarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinalizarPedido.Image = global::UI.Properties.Resources.ok_fw;
            this.btnFinalizarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnFinalizarPedido.Location = new System.Drawing.Point(625, 63);
            this.btnFinalizarPedido.Name = "btnFinalizarPedido";
            this.btnFinalizarPedido.Size = new System.Drawing.Size(150, 44);
            this.btnFinalizarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnFinalizarPedido.TabIndex = 15;
            this.btnFinalizarPedido.Text = "Finalizar Pedido - F10";
            this.btnFinalizarPedido.Click += new System.EventHandler(this.btnFinalizarPedido_Click);
            // 
            // btnEspecial
            // 
            this.btnEspecial.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEspecial.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnEspecial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEspecial.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnEspecial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEspecial.Image = global::UI.Properties.Resources._7397_128x128;
            this.btnEspecial.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnEspecial.Location = new System.Drawing.Point(315, 63);
            this.btnEspecial.Name = "btnEspecial";
            this.btnEspecial.Size = new System.Drawing.Size(150, 44);
            this.btnEspecial.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnEspecial.TabIndex = 14;
            this.btnEspecial.Text = "Especiais - F8";
            this.btnEspecial.Click += new System.EventHandler(this.btnEspecial_Click);
            // 
            // btnVisualizarPedido
            // 
            this.btnVisualizarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnVisualizarPedido.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnVisualizarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVisualizarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnVisualizarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVisualizarPedido.Image = global::UI.Properties.Resources.computer;
            this.btnVisualizarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnVisualizarPedido.Location = new System.Drawing.Point(3, 63);
            this.btnVisualizarPedido.Name = "btnVisualizarPedido";
            this.btnVisualizarPedido.Size = new System.Drawing.Size(150, 44);
            this.btnVisualizarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnVisualizarPedido.TabIndex = 13;
            this.btnVisualizarPedido.Text = "Ver Pedidos - F6";
            this.btnVisualizarPedido.Click += new System.EventHandler(this.btnVisualizarPedido_Click);
            // 
            // btnEnviarPedido
            // 
            this.btnEnviarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEnviarPedido.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnEnviarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEnviarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnEnviarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnviarPedido.Image = global::UI.Properties.Resources.wifi;
            this.btnEnviarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnEnviarPedido.Location = new System.Drawing.Point(625, 10);
            this.btnEnviarPedido.Name = "btnEnviarPedido";
            this.btnEnviarPedido.Size = new System.Drawing.Size(150, 44);
            this.btnEnviarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnEnviarPedido.TabIndex = 12;
            this.btnEnviarPedido.Text = "   Enviar Pedido - F5";
            this.btnEnviarPedido.Click += new System.EventHandler(this.btnEnviarPedido_Click);
            // 
            // btnCancelarPedido
            // 
            this.btnCancelarPedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelarPedido.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnCancelarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarPedido.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnCancelarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarPedido.Image = global::UI.Properties.Resources.fecharPed1;
            this.btnCancelarPedido.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCancelarPedido.Location = new System.Drawing.Point(470, 10);
            this.btnCancelarPedido.Name = "btnCancelarPedido";
            this.btnCancelarPedido.Size = new System.Drawing.Size(150, 44);
            this.btnCancelarPedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnCancelarPedido.TabIndex = 11;
            this.btnCancelarPedido.Text = "Cancelar Pedido - F4";
            this.btnCancelarPedido.Click += new System.EventHandler(this.btnCancelarPedido_Click);
            // 
            // btnCancelarItem
            // 
            this.btnCancelarItem.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelarItem.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnCancelarItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarItem.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnCancelarItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelarItem.Image = global::UI.Properties.Resources.excluirItem_fw;
            this.btnCancelarItem.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCancelarItem.Location = new System.Drawing.Point(315, 10);
            this.btnCancelarItem.Name = "btnCancelarItem";
            this.btnCancelarItem.Size = new System.Drawing.Size(150, 44);
            this.btnCancelarItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnCancelarItem.TabIndex = 10;
            this.btnCancelarItem.Text = "Cancelar Item - F3";
            this.btnCancelarItem.Click += new System.EventHandler(this.btnCancelarItem_Click);
            // 
            // btnAddQtd
            // 
            this.btnAddQtd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddQtd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnAddQtd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddQtd.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddQtd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddQtd.Image = global::UI.Properties.Resources.addQtd_fw;
            this.btnAddQtd.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnAddQtd.Location = new System.Drawing.Point(159, 10);
            this.btnAddQtd.Name = "btnAddQtd";
            this.btnAddQtd.Size = new System.Drawing.Size(150, 44);
            this.btnAddQtd.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnAddQtd.TabIndex = 9;
            this.btnAddQtd.Text = "  Quantidade - F2 ";
            this.btnAddQtd.Click += new System.EventHandler(this.btnAddQtd_Click);
            // 
            // btnPesquisarProdutos
            // 
            this.btnPesquisarProdutos.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPesquisarProdutos.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnPesquisarProdutos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPesquisarProdutos.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnPesquisarProdutos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarProdutos.Image = global::UI.Properties.Resources._1394159835_search;
            this.btnPesquisarProdutos.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnPesquisarProdutos.Location = new System.Drawing.Point(3, 10);
            this.btnPesquisarProdutos.Name = "btnPesquisarProdutos";
            this.btnPesquisarProdutos.Size = new System.Drawing.Size(150, 44);
            this.btnPesquisarProdutos.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.btnPesquisarProdutos.TabIndex = 8;
            this.btnPesquisarProdutos.Text = "Buscar Produtos - F1  ";
            this.btnPesquisarProdutos.Click += new System.EventHandler(this.btnPesquisarProdutos_Click);
            // 
            // PdvDataHora
            // 
            this.PdvDataHora.Enabled = true;
            this.PdvDataHora.Interval = 1000;
            this.PdvDataHora.Tick += new System.EventHandler(this.PdvDataHora_Tick);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Blue;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.lblTitulo);
            this.panel2.Location = new System.Drawing.Point(20, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1237, 38);
            this.panel2.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.AutoSize = true;
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.pictureBox2);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.lblOperador);
            this.panel6.Location = new System.Drawing.Point(922, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(310, 33);
            this.panel6.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::UI.Properties.Resources._7834_64x64;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(3, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operador:";
            // 
            // lblOperador
            // 
            this.lblOperador.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOperador.AutoSize = true;
            this.lblOperador.BackColor = System.Drawing.Color.Transparent;
            this.lblOperador.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperador.ForeColor = System.Drawing.Color.Red;
            this.lblOperador.Location = new System.Drawing.Point(155, 4);
            this.lblOperador.Name = "lblOperador";
            this.lblOperador.Size = new System.Drawing.Size(0, 23);
            this.lblOperador.TabIndex = 6;
            this.lblOperador.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTitulo.Location = new System.Drawing.Point(15, 7);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(156, 25);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "SisLanche PDV - ";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Blue;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblDescricao);
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1333, 116);
            this.panel1.TabIndex = 2;
            // 
            // lblDescricao
            // 
            this.lblDescricao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDescricao.BackColor = System.Drawing.Color.Blue;
            this.lblDescricao.Font = new System.Drawing.Font("Arial Narrow", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDescricao.Location = new System.Drawing.Point(3, 21);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(1327, 75);
            this.lblDescricao.TabIndex = 1;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.BackgroundImage")));
            this.btnMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Location = new System.Drawing.Point(1263, 13);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(42, 39);
            this.btnMinimizar.TabIndex = 6;
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFechar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFechar.BackgroundImage")));
            this.btnFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Location = new System.Drawing.Point(1311, 13);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(42, 39);
            this.btnFechar.TabIndex = 4;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // cmsMenuPdv
            // 
            this.cmsMenuPdv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.retiradasToolStripMenuItem,
            this.calculadoraToolStripMenuItem});
            this.cmsMenuPdv.Name = "cmsMenuPdv";
            this.cmsMenuPdv.Size = new System.Drawing.Size(138, 48);
            this.cmsMenuPdv.Text = "Menu PDV";
            // 
            // retiradasToolStripMenuItem
            // 
            this.retiradasToolStripMenuItem.Name = "retiradasToolStripMenuItem";
            this.retiradasToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.retiradasToolStripMenuItem.Text = "Retiradas";
            this.retiradasToolStripMenuItem.Click += new System.EventHandler(this.retiradasToolStripMenuItem_Click);
            // 
            // calculadoraToolStripMenuItem
            // 
            this.calculadoraToolStripMenuItem.Name = "calculadoraToolStripMenuItem";
            this.calculadoraToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.calculadoraToolStripMenuItem.Text = "Calculadora";
            this.calculadoraToolStripMenuItem.Click += new System.EventHandler(this.calculadoraToolStripMenuItem_Click);
            // 
            // PdvPrintPed
            // 
            this.PdvPrintPed.Enabled = true;
            this.PdvPrintPed.Interval = 1000;
            this.PdvPrintPed.Tick += new System.EventHandler(this.PdvPrintPed_Tick);
            // 
            // bgwImprimiPedido
            // 
            this.bgwImprimiPedido.WorkerReportsProgress = true;
            this.bgwImprimiPedido.WorkerSupportsCancellation = true;
            this.bgwImprimiPedido.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwImprimiPedido_DoWork);
            this.bgwImprimiPedido.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwImprimiPedido_ProgressChanged);
            this.bgwImprimiPedido.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwImprimiPedido_RunWorkerCompleted);
            // 
            // frmPdv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.ContextMenuStrip = this.cmsMenuPdv;
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmPdv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRTEC - Café Regional Pupunha | Ponto de Venda";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPdv_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmPdv_KeyUp);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelEx2.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCupom)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.cmsMenuPdv.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtValorTotal;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtValorUnitario;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTotalGeral;
        private System.Windows.Forms.TextBox txtItens;
        private System.Windows.Forms.Timer PdvDataHora;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label lblCodVenda;
        private System.Windows.Forms.Label lblAtendimentoCupom;
        private System.Windows.Forms.Label lblNomeEmpresa;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblOperador;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblAtendente;
        public System.Windows.Forms.DataGridView dgCupom;
        private System.Windows.Forms.Panel panel8;
        private DevComponents.DotNetBar.ButtonX btnExtra;
        private DevComponents.DotNetBar.ButtonX btnFinalizarPedido;
        private DevComponents.DotNetBar.ButtonX btnEspecial;
        private DevComponents.DotNetBar.ButtonX btnVisualizarPedido;
        private DevComponents.DotNetBar.ButtonX btnEnviarPedido;
        private DevComponents.DotNetBar.ButtonX btnCancelarPedido;
        private DevComponents.DotNetBar.ButtonX btnCancelarItem;
        private DevComponents.DotNetBar.ButtonX btnAddQtd;
        private DevComponents.DotNetBar.ButtonX btnPesquisarProdutos;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.ButtonX btnImprimir;
        private System.Windows.Forms.ContextMenuStrip cmsMenuPdv;
        private System.Windows.Forms.ToolStripMenuItem retiradasToolStripMenuItem;
        private System.Windows.Forms.Label lblDataHora;
        private DevComponents.DotNetBar.LabelX lblAtendimento;
        private System.Windows.Forms.Button btnPedC18;
        private System.Windows.Forms.Button btnPedM4;
        private System.Windows.Forms.Button btnPedC17;
        private System.Windows.Forms.Button btnPedM1;
        private System.Windows.Forms.Button btnPedC16;
        private System.Windows.Forms.Button btnPedM2;
        private System.Windows.Forms.Button btnPedC15;
        private System.Windows.Forms.Button btnPedM3;
        private System.Windows.Forms.Button btnPedC14;
        private System.Windows.Forms.Button btnPedM5;
        private System.Windows.Forms.Button btnPedC13;
        private System.Windows.Forms.Button btnPedM6;
        private System.Windows.Forms.Button btnPedC12;
        private System.Windows.Forms.Button btnPedM7;
        private System.Windows.Forms.Button btnPedC11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnPedC10;
        private System.Windows.Forms.Button btnPedM8;
        private System.Windows.Forms.Button btnPedC9;
        private System.Windows.Forms.Button btnPedM9;
        private System.Windows.Forms.Button btnPedC8;
        private System.Windows.Forms.Button btnPedM10;
        private System.Windows.Forms.Button btnPedC7;
        private System.Windows.Forms.Button btnPedM11;
        private System.Windows.Forms.Button btnPedC6;
        private System.Windows.Forms.Button btnPedM12;
        private System.Windows.Forms.Button btnPedC5;
        private System.Windows.Forms.Button btnPedM13;
        private System.Windows.Forms.Button btnPedC4;
        private System.Windows.Forms.Button btnPedM14;
        private System.Windows.Forms.Button btnPedC3;
        private System.Windows.Forms.Button btnPedM15;
        private System.Windows.Forms.Button btnPedC2;
        private System.Windows.Forms.Button btnPedM16;
        private System.Windows.Forms.Button btnPedC1;
        private System.Windows.Forms.Button btnPedM17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnPedM18;
        private System.Windows.Forms.Button btnPedB18;
        private System.Windows.Forms.Button btnPedM19;
        private System.Windows.Forms.Button btnPedB17;
        private System.Windows.Forms.Button btnPedM20;
        private System.Windows.Forms.Button btnPedB16;
        private System.Windows.Forms.Button btnPedM21;
        private System.Windows.Forms.Button btnPedB15;
        private System.Windows.Forms.Button btnPedM22;
        private System.Windows.Forms.Button btnPedB14;
        private System.Windows.Forms.Button btnPedM23;
        private System.Windows.Forms.Button btnPedB13;
        private System.Windows.Forms.Button btnPedM24;
        private System.Windows.Forms.Button btnPedB12;
        private System.Windows.Forms.Button btnPedM25;
        private System.Windows.Forms.Button btnPedB11;
        private System.Windows.Forms.Button btnPedM26;
        private System.Windows.Forms.Button btnPedB10;
        private System.Windows.Forms.Button btnPedM27;
        private System.Windows.Forms.Button btnPedB9;
        private System.Windows.Forms.Button btnPedM28;
        private System.Windows.Forms.Button btnPedB8;
        private System.Windows.Forms.Button btnPedM29;
        private System.Windows.Forms.Button btnPedB7;
        private System.Windows.Forms.Button btnPedM30;
        private System.Windows.Forms.Button btnPedB6;
        private System.Windows.Forms.Button btnPedM31;
        private System.Windows.Forms.Button btnPedB5;
        private System.Windows.Forms.Button btnPedM32;
        private System.Windows.Forms.Button btnPedB4;
        private System.Windows.Forms.Button btnPedM33;
        private System.Windows.Forms.Button btnPedB3;
        private System.Windows.Forms.Button btnPedM34;
        private System.Windows.Forms.Button btnPedB2;
        private System.Windows.Forms.Button btnPedM35;
        private System.Windows.Forms.Button btnPedB1;
        private System.Windows.Forms.Button btnPedM36;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolStripMenuItem calculadoraToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn extras;
        private System.Windows.Forms.DataGridViewTextBoxColumn especiais;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataEnvioVendaDetalhes;
        private System.Windows.Forms.DataGridViewTextBoxColumn idVendaDetalhes;
        private System.Windows.Forms.Timer PdvPrintPed;
        private System.ComponentModel.BackgroundWorker bgwImprimiPedido;

    }
}