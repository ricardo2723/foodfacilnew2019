﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmTempoPedido : Form
    {
        VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();
        DateTime horaChegada;
        DateTime horaSaida;
        TimeSpan tempoPercorrido;

        public frmTempoPedido()
        {
            InitializeComponent();
            dgvTempoPedidos.AutoGenerateColumns = false;

            carregaPedidos();
        }
        
        #region METODOS

        private void carregaPedidos()
        {
            VendaCabecalhoCollections vendaCabecalhoCollection = vendaDetalhesNegocios.tempoEsperaPedido();

            dgvTempoPedidos.DataSource = null;
            dgvTempoPedidos.DataSource = vendaCabecalhoCollection;
        }
                
        #endregion        

        private void tempoPedidos_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvTempoPedidos.RowCount; i++)
            {
                horaChegada = Convert.ToDateTime(dgvTempoPedidos.Rows[i].Cells[3].Value);
                horaSaida = DateTime.Now;
                tempoPercorrido = horaSaida.Subtract(horaChegada);
                dgvTempoPedidos.Rows[i].Cells[4].Value = tempoPercorrido.ToString("hh") + ":" + tempoPercorrido.ToString("mm") + ":" + tempoPercorrido.ToString("ss");  
            }                 
        }

        private void frmTempoPedido_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    carregaPedidos();
                    break;                
            }
        }        
    }
}
