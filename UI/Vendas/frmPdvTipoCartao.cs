﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmPdvTipoCartao : Form
    {
        
        public string nomeCartao;
        public int idCartao;
        public string sair = "S";
        
        public frmPdvTipoCartao()
        {
            InitializeComponent();
            
            //Carrega List Box
            CartaoNegocios cartaoNegocios = new CartaoNegocios();
            CartaoCollections cartaoCollections = cartaoNegocios.ConsultarNome("%");
            ltbCartao.DataSource = null;
            ltbCartao.DataSource = cartaoCollections;
        }

        private void ltbCartao_MouseClick(object sender, MouseEventArgs e)
        {
            carregaCartao();
        }

        private void ltbCartao_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {                
                case Keys.Enter:
                    carregaCartao();
                    break;
                default:
                    break;
            }
        }

        private void carregaCartao()
        {
            nomeCartao = ltbCartao.Text;
            idCartao = Convert.ToInt32(ltbCartao.SelectedValue);
            sair = "N";
            Close();
        }
    }
}
