﻿using System;
using System.Windows.Forms;

namespace UI
{
    public partial class frmPdvDelete : Form
    {
        public frmPdvDelete()
        {
            InitializeComponent();
        }

        public int codDel;

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtCodigo_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtDelete.Text != "")
                    {
                        codDel = Convert.ToInt32(txtDelete.Text);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Digite o numero Item", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        txtDelete.Focus();
                    }
                    break;
            }                        
        }
    }
}
