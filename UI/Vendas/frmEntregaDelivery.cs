﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;
using Relatorios;

namespace UI
{
    public partial class frmEntregaDelivery : Form
    {
        VendaCabecalho vendaCabecalho = new VendaCabecalho();

        VendaCabecalhoDelivery delivery = new VendaCabecalhoDelivery();
        VendaCabecalhoDelivery deliveryEntrega = new VendaCabecalhoDelivery();
        VendaCabecalhoDelivery deliverySelecionado = new VendaCabecalhoDelivery();
        VendaCabecalhoDelivery deliverySelecionadoEntrega = new VendaCabecalhoDelivery();
        EntregasDeliveryNegocios entregaDelivery = new EntregasDeliveryNegocios();

        VendaDetalhesCollections listaProdutos = new VendaDetalhesCollections();
        VendaDetalhesCollections listaProdutosEntrega = new VendaDetalhesCollections();

        DateTime horaChegada;
        DateTime horaSaida;
        TimeSpan tempoPercorrido;
        TimeSpan tempoParaEnvio;
        TimeSpan tempoTotalPedido;

        public frmEntregaDelivery()
        {
            InitializeComponent();
        }

        private void tempoPedidos_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvDelivery.RowCount; i++)
            {
                horaChegada = Convert.ToDateTime(dgvDelivery.Rows[i].Cells["hora"].Value);
                horaSaida = DateTime.Now;
                tempoPercorrido = horaSaida.Subtract(horaChegada);
                dgvDelivery.Rows[i].Cells["tempo"].Value = tempoPercorrido.ToString("hh") + ":" + tempoPercorrido.ToString("mm") + ":" + tempoPercorrido.ToString("ss");
            }
        }

        private void tempoEntrega_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvDeliverySaiuEntrega.RowCount; i++)
            {
                horaChegada = Convert.ToDateTime(dgvDeliverySaiuEntrega.Rows[i].Cells["horaSaid"].Value);
                horaSaida = DateTime.Now;
                tempoPercorrido = horaSaida.Subtract(horaChegada);
                dgvDeliverySaiuEntrega.Rows[i].Cells["tempoEntr"].Value = tempoPercorrido.ToString("hh") + ":" + tempoPercorrido.ToString("mm") + ":" + tempoPercorrido.ToString("ss");
            }
        }

        private void frmEntregaDelivery_Load(object sender, EventArgs e)
        {
            carregaPedidosParaEntrega();
            carregaEntregador();
        }

        private void frmEntregaDelivery_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    dgvDelivery.Focus();
                    break;
                case Keys.F5:
                    carregaPedidosParaEntrega();
                    break;
                case Keys.F10:
                    btnEnviarPedido.PerformClick();
                    break;
            }
        }

        private void dgvDelivery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                selecionaPedido();
            }
        }

        private void dgvDelivery_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selecionaPedido();
        }

        private void dgvDeliverySaiuEntrega_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                selecionaPedidoEntrega();
            }
        }

        private void dgvDeliverySaiuEntrega_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selecionaPedidoEntrega();
        }

        private void tiSaiuEntrega_Click(object sender, EventArgs e)
        {
            carregaPedidosSaiuEntrega();
        }

        private void selecionaPedido()
        {
            try
            {
                deliverySelecionado = dgvDelivery.SelectedRows[0].DataBoundItem as VendaCabecalhoDelivery;

                lblNumPedido.Text = deliverySelecionado.idVendaCabecalho.ToString("00000000");
                lblNomeCliente.Text = deliverySelecionado.nomeCliente;
                lblAtendente.Text = deliverySelecionado.atendente;
                lblEndercoCliente.Text = deliverySelecionado.enderecoCliente + " " + deliverySelecionado.complementoCliente;
                lblBairro.Text = deliverySelecionado.bairroCliente;
                lblPontoReferenciaCliente.Text = deliverySelecionado.pontoReferenciaCliente;
                lblValorPedido.Text = "R$ " + deliverySelecionado.totaDelivery;
                lblTempoEspera.Text = dgvDelivery.CurrentRow.Cells["tempo"].Value.ToString();
                cbStatus.SelectedIndex = deliverySelecionado.statusEntrega - 2;

                pegarProduto(deliverySelecionado.idVendaCabecalho);
            }
            catch (Exception)
            {
                dgvDelivery.Focus();
            }

        }

        private void selecionaPedidoEntrega()
        {
            try
            {
                deliverySelecionadoEntrega = dgvDeliverySaiuEntrega.SelectedRows[0].DataBoundItem as VendaCabecalhoDelivery;

                lblNumPedidoEntrega.Text = deliverySelecionadoEntrega.idVendaCabecalho.ToString("00000000");
                lblNomeClienteEntrega.Text = deliverySelecionadoEntrega.nomeCliente;
                lblAtendenteEntrega.Text = deliverySelecionadoEntrega.atendente;
                lblEnderecoEntrega.Text = deliverySelecionadoEntrega.enderecoCliente + " " + deliverySelecionado.complementoCliente;
                lblBairroEntrega.Text = deliverySelecionadoEntrega.bairroCliente;
                lblPontoReferenciaEntrega.Text = deliverySelecionadoEntrega.pontoReferenciaCliente;
                lblValorPedidoEntrega.Text = "R$ " + deliverySelecionadoEntrega.totaDelivery;
                lblTempoPedidoEspera.Text = dgvDeliverySaiuEntrega.CurrentRow.Cells["tempoEntr"].Value.ToString();
                lblNomeEntegador.Text = deliverySelecionadoEntrega.apelidoEntregador;
                lblHoraPedido.Text = deliverySelecionadoEntrega.horaPedido.ToString("HH:mm:ss");

                tempoParaEnvio = deliverySelecionadoEntrega.horaSaida.Subtract(Convert.ToDateTime(lblHoraPedido.Text));
                lblTempoEnvio.Text = tempoParaEnvio.ToString("hh") + ":" + tempoParaEnvio.ToString("mm") + ":" + tempoParaEnvio.ToString("ss");
                tempoTotalPedido = DateTime.Now.Subtract(Convert.ToDateTime(lblHoraPedido.Text));
                lblTempoTotalPedido.Text = tempoTotalPedido.ToString("hh") + ":" + tempoTotalPedido.ToString("mm") + ":" + tempoTotalPedido.ToString("ss");

                pegarProdutoEntrega(deliverySelecionadoEntrega.idVendaCabecalho);
            }
            catch (Exception)
            {
                dgvDeliverySaiuEntrega.Focus();
            }

        }

        private void carregaPedidosParaEntrega()
        {
            dgvDelivery.AutoGenerateColumns = false;
            VendaCabecalhoDeliveryCollections deliveryCollectionsParaEntrega = entregaDelivery.ConsultarPedidosPorStatus(2);
            dgvDelivery.DataSource = deliveryCollectionsParaEntrega;
            dgvDelivery.Focus();
        }

        private void carregaPedidosSaiuEntrega()
        {
            dgvDeliverySaiuEntrega.AutoGenerateColumns = false;
            VendaCabecalhoDeliveryCollections deliveryCollections = entregaDelivery.ConsultarPedidosSaiuEntrega(3);
            dgvDeliverySaiuEntrega.DataSource = deliveryCollections;
            dgvDeliverySaiuEntrega.Focus();
        }

        private void carregaEntregador()
        {
            EntregadorNegocios entregadorNegocios = new EntregadorNegocios();
            EntregadorCollections entregadorCollections = entregadorNegocios.ConsultarPorApelido("");

            cbEntregador.DataSource = null;
            cbEntregador.DataSource = entregadorCollections;
        }

        private void pegarProduto(int idPedido)
        {
            dgvProdutos.AutoGenerateColumns = false;

            listaProdutos = entregaDelivery.pegarProduto(idPedido);

            dgvProdutos.DataSource = null;
            dgvProdutos.DataSource = listaProdutos;
        }

        private void pegarProdutoEntrega(int idPedido)
        {
            dgvProdutos.AutoGenerateColumns = false;

            listaProdutosEntrega = entregaDelivery.pegarProduto(idPedido);
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStatus.SelectedIndex == 1)
            {
                cbEntregador.Enabled = true;
                txtMotivoCancelamento.Visible = false;
                lblMotivoCancelamento.Visible = false;
            }
            else if (cbStatus.SelectedIndex == 2)
            {
                cbEntregador.Enabled = false;
                txtMotivoCancelamento.Visible = true;
                lblMotivoCancelamento.Visible = true;
                txtMotivoCancelamento.Focus();
            }
            else
            {
                cbEntregador.Enabled = false;
                txtMotivoCancelamento.Visible = false;
                lblMotivoCancelamento.Visible = false;
            }
        }

        private void cbStatuEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStatuEntrega.SelectedIndex == 0)
            {
                txtMotivoCancelamentoEspera.Visible = false;
                lblMotivoCancelamentoEspera.Visible = false;
            }
            else if (cbStatuEntrega.SelectedIndex == 1)
            {
                txtMotivoCancelamentoEspera.Visible = true;
                lblMotivoCancelamentoEspera.Visible = true;
                txtMotivoCancelamentoEspera.Focus();
            }
        }

        private void btnEnviarPedido_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvDelivery.Rows)
            {
                if (deliverySelecionado.idVendaCabecalho > Convert.ToInt32(item.Cells[0].Value))
                {                   
                    MessageBox.Show("É necessário obedecer a ordem dos Pedidos", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    dgvDelivery.Focus();
                    return;
                }
            }

            if (lblNumPedido.Text != string.Empty && cbStatus.Text != "" && cbStatus.Text != "PRONTO PARA ENTREGA")
            {
                try
                {
                    VendaCabecalho vendaCabecalho = new VendaCabecalho();
                    VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                    int ckretorno;

                    delivery.idVendaCabecalho = Convert.ToInt32(lblNumPedido.Text);
                    delivery.idEntregador = Convert.ToInt32(cbEntregador.SelectedValue);

                    vendaCabecalho.idVendaCabecalho = deliverySelecionado.idVendaCabecalho;
                    vendaCabecalho.atendenteVendaCabecalho = deliverySelecionado.atendente;

                    switch (cbStatus.SelectedIndex)
                    {
                        case 1:
                            delivery.statusEntrega = 3;
                            delivery.horaSaida = DateTime.Now;
                            ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.entregaDelivery(delivery));

                            frmRltImprimirCupom imprimirDelivery = new frmRltImprimirCupom(vendaCabecalho, imprimiCupom.imprimeDelivery, frmLogin.meuLanche);
                            imprimirDelivery.ShowDialog();

                            MessageBox.Show("O Pedido Esta liberado para Entrega", "Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case 2:
                            if (txtMotivoCancelamento.Text != string.Empty)
                            {
                                delivery.statusEntrega = 5;
                                delivery.motivoCancelamento = txtMotivoCancelamento.Text;
                                ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.cancelarDelivery(delivery));
                                cancelarItensStatus(Convert.ToInt32(lblNumPedido.Text), 1);
                                MessageBox.Show("Pedido Cancelado Sucesso", "Cancelamento", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                txtMotivoCancelamentoEspera.Text = string.Empty;
                                txtMotivoCancelamentoEspera.Visible = false;
                                lblMotivoCancelamentoEspera.Visible = false;
                            }
                            else
                            {
                                MessageBox.Show("Digite no Campo Motivo do Cancelamento a Causa do Cancelamento do Pedido", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                txtMotivoCancelamento.Focus();
                            }
                            break;
                    }

                    carregaPedidosParaEntrega();
                    limparLabel(1);
                    dgvDelivery.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error ao Atualizar pedidos verifique o acesso ao banco de dados" + ex, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("É Necessário selecionar um Pedido ou mudar o Status para Entrega", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                dgvDelivery.Focus();
            }
        }

        private void btnFinalizarPedido_Click(object sender, EventArgs e)
        {
            if (lblNumPedidoEntrega.Text != string.Empty && cbStatuEntrega.Text != string.Empty)
            {
                try
                {
                    VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
                    deliveryEntrega.idVendaCabecalho = Convert.ToInt32(lblNumPedidoEntrega.Text);
                    int ckretorno;

                    switch (cbStatuEntrega.SelectedIndex)
                    {
                        case 0:
                            deliveryEntrega.statusEntrega = 4;
                            deliveryEntrega.horaVolta = DateTime.Now;
                            ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.finalizaDelivery(deliveryEntrega));
                            finalizarVenda();
                            MessageBox.Show("Pedido finalizado com Sucesso", "Finalizar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case 1:
                            if (txtMotivoCancelamentoEspera.Text != string.Empty)
                            {
                                deliveryEntrega.statusEntrega = 5;
                                deliveryEntrega.motivoCancelamento = txtMotivoCancelamentoEspera.Text;
                                ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.cancelarDelivery(deliveryEntrega));
                                cancelarItensStatus(deliveryEntrega.idVendaCabecalho, 2);
                                MessageBox.Show("Pedido Cancelado com Sucesso", "Cancelamento", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                txtMotivoCancelamentoEspera.Text = string.Empty;
                                txtMotivoCancelamentoEspera.Visible = false;
                                lblMotivoCancelamentoEspera.Visible = false;
                            }
                            else
                            {
                                MessageBox.Show("Digite no Campo Motivo do Cancelamento a Causa do Cancelamento do Pedido", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                txtMotivoCancelamento.Focus();
                            }
                            break;
                    }

                    carregaPedidosSaiuEntrega();
                    limparLabel(2);
                    dgvDelivery.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error ao Atualizar pedidos verifique o acesso ao banco de dados" + ex, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("É Necessário selecionar um Pedido ou mudar o Status para Entrega", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                dgvDelivery.Focus();
            }
        }

        private void limparLabel(int opc)
        {
            if (opc == 1)
            {
                lblNumPedido.Text = string.Empty;
                lblNomeCliente.Text = string.Empty;
                lblEndercoCliente.Text = string.Empty;
                lblPontoReferenciaCliente.Text = string.Empty;
                lblBairro.Text = string.Empty;
                lblTempoEspera.Text = string.Empty;
                lblAtendente.Text = string.Empty;
                lblValorPedido.Text = string.Empty;
                cbStatus.Text = string.Empty;
                cbEntregador.Text = string.Empty;
                cbEntregador.Enabled = false;
                txtMotivoCancelamento.Text = string.Empty;
                dgvProdutos.DataSource = null;
            }
            else
            {
                lblNumPedidoEntrega.Text = string.Empty;
                lblNomeClienteEntrega.Text = string.Empty;
                lblEnderecoEntrega.Text = string.Empty;
                lblPontoReferenciaEntrega.Text = string.Empty;
                lblBairroEntrega.Text = string.Empty;
                lblAtendenteEntrega.Text = string.Empty;
                lblValorPedidoEntrega.Text = string.Empty;
                lblTempoPedidoEspera.Text = string.Empty;
                cbStatuEntrega.Text = string.Empty;
                txtMotivoCancelamentoEspera.Text = string.Empty;
                lblHoraPedido.Text = string.Empty;
                lblTempoEnvio.Text = string.Empty;
                lblTempoTotalPedido.Text = string.Empty;
            }
        }

        public void excluirItemUpEstoque(int codProd, decimal qtd)
        {
            //DEVOLVE ITEM EXCLUIDO AO ESTOQUE

            Produtos produtos = new Produtos();
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            VendaDetalhes vendaDetalhes = new VendaDetalhes();
            VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();

            produtos.codigoProduto = Convert.ToInt32(codProd);

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            ProdutosCollections produtosCollections = null; // vendaCabecalhoNegocios.GetProdutoVenda('P', produtos);


            foreach (var item in produtosCollections)
            {
                vendaDetalhes.valorVendaProduto = item.valorVendaProduto;
                vendaDetalhes.codigoProduto = item.codigoProduto;
                vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                vendaDetalhes.estoqueProduto = item.estoqueProduto;
                vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
            }

            //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
            if (vendaDetalhes.controlaEstoqueProduto == 'S')
            {
                //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                if (vendaDetalhes.manufaturadoProduto == 'N')
                {
                    try
                    {
                        vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                        string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                        int checaretorno = Convert.ToInt32(retorno);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }

                }
                else if (vendaDetalhes.manufaturadoProduto == 'S')
                {
                    try
                    {
                        //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                        vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                        string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                        int checaretorno = Convert.ToInt32(retorno);

                        //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                        produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                        VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                        foreach (var item in produtosCollections)
                        {
                            vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                            vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                            retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                            checaretorno = Convert.ToInt32(retorno);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }

            }
            else if (vendaDetalhes.controlaEstoqueProduto == 'N')
            {
                if (vendaDetalhes.manufaturadoProduto == 'S')
                {
                    try
                    {
                        //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                        produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                        VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                        foreach (var item in produtosCollections)
                        {
                            decimal qtd1 = item.quantidadeProdutoManufaturado;
                            decimal totalEst = qtd1 * qtd;

                            if (item.estoqueProduto < totalEst)
                            {
                                MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }

                        foreach (var item in produtosCollections)
                        {
                            vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                            vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                            vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                            if (vendaDetalhesManufaturado.estoqueProduto < qtd)
                            {
                                MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }

                            vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto + (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * qtd);

                            string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                            int checaretorno = Convert.ToInt32(retorno);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
        }

        private void cancelarItensStatus(int idPedido, int opc)
        {
            VendaCabecalho vendaCabecalho = new VendaCabecalho();
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            VendaDetalhes vendaDetalhes = new VendaDetalhes();

            try
            {
                vendaCabecalho.idVendaCabecalho = Convert.ToInt32(idPedido);
                string retornaVenda = vendaCabecalhoNegocios.cancelarVenda(vendaCabecalho);

                int checaRetorno = Convert.ToInt32(retornaVenda);

                if (opc == 1)
                {
                    foreach (DataGridViewRow rows in dgvProdutos.Rows)
                    {
                        vendaDetalhes.codigoProduto = Convert.ToInt32(rows.Cells[0].Value);
                        vendaDetalhes.quantidadeVendaDetalhes = Convert.ToDecimal(rows.Cells[4].Value);

                        excluirItemUpEstoque(vendaDetalhes.codigoProduto, vendaDetalhes.quantidadeVendaDetalhes);
                    }
                }

                else
                {
                    foreach (var item in listaProdutosEntrega)
                    {
                        vendaDetalhes.codigoProduto = item.codigoProduto;
                        vendaDetalhes.quantidadeVendaDetalhes = item.quantidadeVendaDetalhes;

                        excluirItemUpEstoque(vendaDetalhes.codigoProduto, vendaDetalhes.quantidadeVendaDetalhes);
                    }
                }
                MessageBox.Show("Pedido Cancelado com Sucesso", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void finalizarVenda()
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            try
            {
                vendaCabecalho.idVendaCabecalho = Convert.ToInt32(deliverySelecionadoEntrega.idVendaCabecalho);
                vendaCabecalho.idCliente = deliverySelecionadoEntrega.idCliente;
                vendaCabecalho.statusVendaCabecalho = 2;
                vendaCabecalho.usuarioId = frmLogin.usuariosLogin.usuarioId;

                int ckretorno = Convert.ToInt32(vendaCabecalhoNegocios.finalizarVendaDelivery(vendaCabecalho));

                VendaDetalhesNegocios vdn = new VendaDetalhesNegocios();

                ckretorno = Convert.ToInt32(vdn.finalizarItens(Convert.ToInt32(deliverySelecionadoEntrega.idVendaCabecalho), 5));

                MessageBox.Show("Pedido Finalizado com Sucesso", "Finalizando Venda", MessageBoxButtons.OK, MessageBoxIcon.Information);                                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi Possivel Finalizar Venda. Detalhes: " + ex, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}