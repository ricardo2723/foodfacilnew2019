﻿namespace UI
{
    partial class frmBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBackup));
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.bwBackup = new System.ComponentModel.BackgroundWorker();
            this.lblMenssagem = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // circularProgress1
            // 
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.Location = new System.Drawing.Point(3, 3);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressColor = System.Drawing.Color.Blue;
            this.circularProgress1.Size = new System.Drawing.Size(66, 52);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 0;
            // 
            // bwBackup
            // 
            this.bwBackup.WorkerReportsProgress = true;
            this.bwBackup.WorkerSupportsCancellation = true;
            this.bwBackup.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwBackup_DoWork);
            this.bwBackup.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwBackup_ProgressChanged);
            this.bwBackup.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwBackup_RunWorkerCompleted);
            // 
            // lblMenssagem
            // 
            // 
            // 
            // 
            this.lblMenssagem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMenssagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenssagem.ForeColor = System.Drawing.Color.Blue;
            this.lblMenssagem.Location = new System.Drawing.Point(75, 18);
            this.lblMenssagem.Name = "lblMenssagem";
            this.lblMenssagem.SingleLineColor = System.Drawing.Color.Blue;
            this.lblMenssagem.Size = new System.Drawing.Size(268, 23);
            this.lblMenssagem.TabIndex = 1;
            // 
            // frmBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(361, 61);
            this.Controls.Add(this.lblMenssagem);
            this.Controls.Add(this.circularProgress1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBackup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Preparando Backup";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBackup_FormClosed);
            this.Load += new System.EventHandler(this.frmBackup_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private System.ComponentModel.BackgroundWorker bwBackup;
        private DevComponents.DotNetBar.LabelX lblMenssagem;
    }
}