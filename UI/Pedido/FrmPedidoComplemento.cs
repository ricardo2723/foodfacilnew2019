﻿using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using UI.GenericFunctions;
using UI.StylesForms;

namespace UI.Pedido
{
    public partial class FrmPedidoComplemento : Form
    {
        private VendaDetalhes vendaDetalhesItens;
        EspeciaisNegocios especiaisNegocios = new EspeciaisNegocios();
        ProdutosNegocios produtosNegociosExtras = new ProdutosNegocios();
        VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();

        public int coditem;
        public decimal qtdExtra;
        public int codigoProdutoExtra;

        public FrmPedidoComplemento(VendaDetalhes vendaDetalhes)
        {
            InitializeComponent();

            vendaDetalhesItens = vendaDetalhes;

            ConfigSkin.ThemeForm(this, lbTitulo, "Complementos do Pedido Nº " + vendaDetalhesItens.idVendaCabecalhoDetalhes.ToString("000000"));

            lblCodigo.Text = vendaDetalhesItens.codigoProduto.ToString();
            lblNomeProduto.Text = vendaDetalhesItens.descricaoProduto;
            coditem = vendaDetalhesItens.codigoProduto;

        }

        private void pTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            MoveForm.MouseMoveForm(this);
        }

        private void pbCloser_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmPedidoComplemento_Load(object sender, EventArgs e)
        {

        }


        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            frmPesqProdutosFront objPesqProduto = new frmPesqProdutosFront(1);
            DialogResult resultado = objPesqProduto.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                txtCodComplemento.Text = objPesqProduto.codigoProduto.ToString();
                txtCodComplemento.Focus();
            }
            else
            {
                txtCodComplemento.Text = null;
            }
        }

        
        private void txtCodComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {                    
                    case Keys.Enter:

                        Convert.ToInt32(txtCodComplemento.Text);

                        if (txtCodComplemento.Text == "" || txtCodComplemento.Text == "0")
                        {
                            MessageBox.Show("O Código é Inválido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            txtCodComplemento.Focus();
                        }
                        else
                        {
                            qtdExtra = Convert.ToDecimal(nudQtdExtra.Value);
                            codigoProdutoExtra = Convert.ToInt32(txtCodComplemento.Text);

                            this.Close();
                        }
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("O Código é Inválido digite apenas Numeros", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodComplemento.Focus();
            }
            
        }

        private void nudQtdExtra_Leave(object sender, EventArgs e)
        {
            if (nudQtdExtra.Value == 0)
            {
                MessageBox.Show("Digite um valor maior que 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                nudQtdExtra.Focus();
            }
        }

        private void FrmPedidoComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    txtCodComplemento.Focus();
                    break;
                case Keys.F2:
                    nudQtdExtra.Value = 1;
                    nudQtdExtra.Focus();
                    break;
                case Keys.F5:
                    btnPesquisarProduto.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
    }
}
