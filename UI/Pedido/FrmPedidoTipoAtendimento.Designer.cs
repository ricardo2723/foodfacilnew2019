﻿namespace UI.Pedido
{
    partial class FrmPedidoTipoAtendimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPedidoTipoAtendimento));
            this.bindTipoAtendimento = new System.Windows.Forms.BindingSource(this.components);
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lbTitulo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgTipoAtendimento = new MetroFramework.Controls.MetroGrid();
            this.idAtendimentoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.atendimentoSiglaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeAtendimentoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.butonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bindTipoAtendimento)).BeginInit();
            this.panelTitulo.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTipoAtendimento)).BeginInit();
            this.SuspendLayout();
            // 
            // bindTipoAtendimento
            // 
            this.bindTipoAtendimento.DataSource = typeof(ObjetoTransferencia.AtendimentoTipo);
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTitulo.Controls.Add(this.lbTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Margin = new System.Windows.Forms.Padding(2);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(226, 30);
            this.panelTitulo.TabIndex = 28;
            // 
            // lbTitulo
            // 
            this.lbTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Location = new System.Drawing.Point(9, 6);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(207, 17);
            this.lbTitulo.TabIndex = 1;
            this.lbTitulo.Text = "Selecione o Tipo de Atendimento";
            this.lbTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgTipoAtendimento);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(226, 76);
            this.panel1.TabIndex = 29;
            // 
            // dgTipoAtendimento
            // 
            this.dgTipoAtendimento.AllowUserToAddRows = false;
            this.dgTipoAtendimento.AllowUserToDeleteRows = false;
            this.dgTipoAtendimento.AllowUserToResizeRows = false;
            this.dgTipoAtendimento.AutoGenerateColumns = false;
            this.dgTipoAtendimento.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTipoAtendimento.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgTipoAtendimento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgTipoAtendimento.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgTipoAtendimento.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkSlateBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Purple;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTipoAtendimento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTipoAtendimento.ColumnHeadersHeight = 35;
            this.dgTipoAtendimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgTipoAtendimento.ColumnHeadersVisible = false;
            this.dgTipoAtendimento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idAtendimentoDataGridViewTextBoxColumn,
            this.atendimentoSiglaDataGridViewTextBoxColumn,
            this.nomeAtendimentoDataGridViewTextBoxColumn,
            this.butonDataGridViewTextBoxColumn});
            this.dgTipoAtendimento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgTipoAtendimento.DataSource = this.bindTipoAtendimento;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTipoAtendimento.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgTipoAtendimento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTipoAtendimento.EnableHeadersVisualStyles = false;
            this.dgTipoAtendimento.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgTipoAtendimento.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgTipoAtendimento.Location = new System.Drawing.Point(0, 0);
            this.dgTipoAtendimento.Margin = new System.Windows.Forms.Padding(2);
            this.dgTipoAtendimento.MultiSelect = false;
            this.dgTipoAtendimento.Name = "dgTipoAtendimento";
            this.dgTipoAtendimento.ReadOnly = true;
            this.dgTipoAtendimento.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTipoAtendimento.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgTipoAtendimento.RowHeadersVisible = false;
            this.dgTipoAtendimento.RowHeadersWidth = 54;
            this.dgTipoAtendimento.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgTipoAtendimento.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgTipoAtendimento.RowTemplate.DividerHeight = 1;
            this.dgTipoAtendimento.RowTemplate.Height = 35;
            this.dgTipoAtendimento.RowTemplate.ReadOnly = true;
            this.dgTipoAtendimento.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTipoAtendimento.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgTipoAtendimento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTipoAtendimento.Size = new System.Drawing.Size(226, 76);
            this.dgTipoAtendimento.TabIndex = 28;
            // 
            // idAtendimentoDataGridViewTextBoxColumn
            // 
            this.idAtendimentoDataGridViewTextBoxColumn.DataPropertyName = "idAtendimento";
            this.idAtendimentoDataGridViewTextBoxColumn.HeaderText = "idAtendimento";
            this.idAtendimentoDataGridViewTextBoxColumn.Name = "idAtendimentoDataGridViewTextBoxColumn";
            this.idAtendimentoDataGridViewTextBoxColumn.ReadOnly = true;
            this.idAtendimentoDataGridViewTextBoxColumn.Visible = false;
            // 
            // atendimentoSiglaDataGridViewTextBoxColumn
            // 
            this.atendimentoSiglaDataGridViewTextBoxColumn.DataPropertyName = "atendimentoSigla";
            this.atendimentoSiglaDataGridViewTextBoxColumn.HeaderText = "atendimentoSigla";
            this.atendimentoSiglaDataGridViewTextBoxColumn.Name = "atendimentoSiglaDataGridViewTextBoxColumn";
            this.atendimentoSiglaDataGridViewTextBoxColumn.ReadOnly = true;
            this.atendimentoSiglaDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomeAtendimentoDataGridViewTextBoxColumn
            // 
            this.nomeAtendimentoDataGridViewTextBoxColumn.DataPropertyName = "nomeAtendimento";
            this.nomeAtendimentoDataGridViewTextBoxColumn.HeaderText = "nomeAtendimento";
            this.nomeAtendimentoDataGridViewTextBoxColumn.Name = "nomeAtendimentoDataGridViewTextBoxColumn";
            this.nomeAtendimentoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // butonDataGridViewTextBoxColumn
            // 
            this.butonDataGridViewTextBoxColumn.DataPropertyName = "buton";
            this.butonDataGridViewTextBoxColumn.HeaderText = "buton";
            this.butonDataGridViewTextBoxColumn.Name = "butonDataGridViewTextBoxColumn";
            this.butonDataGridViewTextBoxColumn.ReadOnly = true;
            this.butonDataGridViewTextBoxColumn.Visible = false;
            // 
            // FrmPedidoTipoAtendimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(226, 106);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmPedidoTipoAtendimento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Food Fácil - Tipo do Atendimento";
            this.Load += new System.EventHandler(this.FrmPedidoTipoAtendimento_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPedidoTipoAtendimento_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bindTipoAtendimento)).EndInit();
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTipoAtendimento)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource bindTipoAtendimento;
        private System.Windows.Forms.Panel panelTitulo;
        public System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroGrid dgTipoAtendimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abreviaturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idAtendimentoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn atendimentoSiglaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeAtendimentoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn butonDataGridViewTextBoxColumn;
    }
}