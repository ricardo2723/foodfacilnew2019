﻿namespace UI.Pedidos
{
    partial class FrmPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPedido));
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lbTitulo = new System.Windows.Forms.Label();
            this.pbCloser = new System.Windows.Forms.PictureBox();
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblPedido = new System.Windows.Forms.Label();
            this.panelCorpo = new System.Windows.Forms.Panel();
            this.flpPedidoMesa = new System.Windows.Forms.FlowLayoutPanel();
            this.panelTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloser)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelCorpo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTitulo.Controls.Add(this.lbTitulo);
            this.panelTitulo.Controls.Add(this.pbCloser);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Margin = new System.Windows.Forms.Padding(2);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(1072, 30);
            this.panelTitulo.TabIndex = 19;
            this.panelTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pTitulo_MouseMove);
            // 
            // lbTitulo
            // 
            this.lbTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Location = new System.Drawing.Point(2, 6);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(41, 17);
            this.lbTitulo.TabIndex = 1;
            this.lbTitulo.Text = "Titulo";
            this.lbTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbCloser
            // 
            this.pbCloser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCloser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCloser.Image = ((System.Drawing.Image)(resources.GetObject("pbCloser.Image")));
            this.pbCloser.Location = new System.Drawing.Point(1046, 6);
            this.pbCloser.Margin = new System.Windows.Forms.Padding(2);
            this.pbCloser.Name = "pbCloser";
            this.pbCloser.Size = new System.Drawing.Size(15, 16);
            this.pbCloser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCloser.TabIndex = 0;
            this.pbCloser.TabStop = false;
            this.pbCloser.Click += new System.EventHandler(this.pbCloser_Click);
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.GhostWhite;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.lblPedido);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 30);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1072, 49);
            this.panelTop.TabIndex = 20;
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblPedido.Location = new System.Drawing.Point(11, 18);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(121, 13);
            this.lblPedido.TabIndex = 0;
            this.lblPedido.Text = "[ NOVO PEDIDO - F12 ]";
            this.lblPedido.Click += new System.EventHandler(this.lblPedido_Click);
            // 
            // panelCorpo
            // 
            this.panelCorpo.BackColor = System.Drawing.Color.Silver;
            this.panelCorpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelCorpo.Controls.Add(this.flpPedidoMesa);
            this.panelCorpo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCorpo.Location = new System.Drawing.Point(0, 79);
            this.panelCorpo.Name = "panelCorpo";
            this.panelCorpo.Size = new System.Drawing.Size(1072, 627);
            this.panelCorpo.TabIndex = 21;
            // 
            // flpPedidoMesa
            // 
            this.flpPedidoMesa.AutoScroll = true;
            this.flpPedidoMesa.AutoScrollMargin = new System.Drawing.Size(30, 0);
            this.flpPedidoMesa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flpPedidoMesa.Location = new System.Drawing.Point(12, 10);
            this.flpPedidoMesa.Name = "flpPedidoMesa";
            this.flpPedidoMesa.Size = new System.Drawing.Size(1047, 570);
            this.flpPedidoMesa.TabIndex = 0;
            // 
            // FrmPedido
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1072, 706);
            this.Controls.Add(this.panelCorpo);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelTitulo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPedido";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPedido_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPedido_KeyDown);
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloser)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelCorpo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitulo;
        public System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.PictureBox pbCloser;
        private System.Windows.Forms.Panel panelCorpo;
        private System.Windows.Forms.Label lblPedido;
        public System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.FlowLayoutPanel flpPedidoMesa;
    }
}