﻿using Negocios;
using ObjetoTransferencia;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace UI.Pedido
{
    public partial class FrmPedidoTipoAtendimento : Form
    {
        AtendimentoTipoNegocios atendimentoTipoNegocios = new AtendimentoTipoNegocios();
        public VendaCabecalho pedido;

        private int yDg;

        public FrmPedidoTipoAtendimento()
        {
            InitializeComponent();
            yDg = (atendimentoTipoNegocios.GetTipoAtendimento(null).Count() * 35) + 30;
            this.ClientSize = new Size(226, yDg);
        }

        private void FrmPedidoTipoAtendimento_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Close();
                    this.DialogResult = DialogResult.No;
                    break;
                case Keys.Enter:
                    GetTypeAtendment();
                    break;
                default:
                    break;
            }
        }

        private void FrmPedidoTipoAtendimento_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void LoadDataGrid(string nome = null)
        {
            try
            {
                dgTipoAtendimento.DataSource = atendimentoTipoNegocios.GetTipoAtendimento(nome);
                dgTipoAtendimento.Focus();
            }
            catch (Exception)
            {
                MessageBox.Show("Nenhum Registro Encotrado para sua Pesquisa", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private AtendimentoTipo GetRowSelectDataGrid()
        {
            return dgTipoAtendimento.SelectedRows[0].DataBoundItem as AtendimentoTipo;
        }

        private void GetTypeAtendment()
        {
            /*FrmPedidoNumeroAtendimento fpna = new FrmPedidoNumeroAtendimento(GetRowSelectDataGrid());
            DialogResult retorno = fpna.ShowDialog();

            if (retorno == DialogResult.Yes)
            {
                request = fpna.request;
                this.DialogResult = DialogResult.Yes;
                Close();
            }
            */
            
        }
    }
}
