﻿namespace UI.Pedido
{
    partial class FrmPedidoComplemento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPedidoComplemento));
            this.pTitulo = new System.Windows.Forms.Panel();
            this.lbTitulo = new System.Windows.Forms.Label();
            this.pbCloser = new System.Windows.Forms.PictureBox();
            this.panelCorpo = new System.Windows.Forms.Panel();
            this.txtCodComplemento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.nudQtdExtra = new System.Windows.Forms.NumericUpDown();
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblNomeProduto = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.btnPesquisarProduto = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblObservacao = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.bindExtras = new System.Windows.Forms.BindingSource(this.components);
            this.bindExtraItens = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloser)).BeginInit();
            this.panelCorpo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtdExtra)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindExtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindExtraItens)).BeginInit();
            this.SuspendLayout();
            // 
            // pTitulo
            // 
            this.pTitulo.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTitulo.Controls.Add(this.lbTitulo);
            this.pTitulo.Controls.Add(this.pbCloser);
            this.pTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pTitulo.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pTitulo.Location = new System.Drawing.Point(0, 0);
            this.pTitulo.Margin = new System.Windows.Forms.Padding(2);
            this.pTitulo.Name = "pTitulo";
            this.pTitulo.Size = new System.Drawing.Size(472, 30);
            this.pTitulo.TabIndex = 21;
            this.pTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pTitulo_MouseMove);
            // 
            // lbTitulo
            // 
            this.lbTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Location = new System.Drawing.Point(2, 5);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(51, 18);
            this.lbTitulo.TabIndex = 1;
            this.lbTitulo.Text = "Titulo";
            this.lbTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbCloser
            // 
            this.pbCloser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCloser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCloser.Image = ((System.Drawing.Image)(resources.GetObject("pbCloser.Image")));
            this.pbCloser.Location = new System.Drawing.Point(446, 6);
            this.pbCloser.Margin = new System.Windows.Forms.Padding(2);
            this.pbCloser.Name = "pbCloser";
            this.pbCloser.Size = new System.Drawing.Size(15, 16);
            this.pbCloser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCloser.TabIndex = 0;
            this.pbCloser.TabStop = false;
            this.pbCloser.Click += new System.EventHandler(this.pbCloser_Click);
            // 
            // panelCorpo
            // 
            this.panelCorpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelCorpo.Controls.Add(this.txtCodComplemento);
            this.panelCorpo.Controls.Add(this.nudQtdExtra);
            this.panelCorpo.Controls.Add(this.panelTop);
            this.panelCorpo.Controls.Add(this.btnPesquisarProduto);
            this.panelCorpo.Controls.Add(this.label2);
            this.panelCorpo.Controls.Add(this.label1);
            this.panelCorpo.Controls.Add(this.lblObservacao);
            this.panelCorpo.Controls.Add(this.materialDivider1);
            this.panelCorpo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCorpo.Location = new System.Drawing.Point(0, 30);
            this.panelCorpo.Name = "panelCorpo";
            this.panelCorpo.Size = new System.Drawing.Size(472, 213);
            this.panelCorpo.TabIndex = 0;
            // 
            // txtCodComplemento
            // 
            this.txtCodComplemento.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtCodComplemento.Border.Class = "TextBoxBorder";
            this.txtCodComplemento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodComplemento.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtCodComplemento.Location = new System.Drawing.Point(15, 133);
            this.txtCodComplemento.Name = "txtCodComplemento";
            this.txtCodComplemento.PreventEnterBeep = true;
            this.txtCodComplemento.Size = new System.Drawing.Size(160, 29);
            this.txtCodComplemento.TabIndex = 0;
            this.txtCodComplemento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodComplemento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodComplemento_KeyDown);
            // 
            // nudQtdExtra
            // 
            this.nudQtdExtra.Font = new System.Drawing.Font("Arial", 13F);
            this.nudQtdExtra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nudQtdExtra.Location = new System.Drawing.Point(183, 134);
            this.nudQtdExtra.Margin = new System.Windows.Forms.Padding(5);
            this.nudQtdExtra.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudQtdExtra.Name = "nudQtdExtra";
            this.nudQtdExtra.Size = new System.Drawing.Size(61, 27);
            this.nudQtdExtra.TabIndex = 16;
            this.nudQtdExtra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudQtdExtra.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudQtdExtra.Leave += new System.EventHandler(this.nudQtdExtra_Leave);
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelTop.Controls.Add(this.lblNomeProduto);
            this.panelTop.Controls.Add(this.lblCodigo);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(470, 62);
            this.panelTop.TabIndex = 4;
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProduto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblNomeProduto.Location = new System.Drawing.Point(86, 18);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(383, 27);
            this.lblNomeProduto.TabIndex = 3;
            this.lblNomeProduto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodigo
            // 
            this.lblCodigo.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.ForeColor = System.Drawing.Color.White;
            this.lblCodigo.Location = new System.Drawing.Point(7, 17);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(70, 29);
            this.lblCodigo.TabIndex = 4;
            this.lblCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPesquisarProduto
            // 
            this.btnPesquisarProduto.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPesquisarProduto.AutoSize = true;
            this.btnPesquisarProduto.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnPesquisarProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPesquisarProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisarProduto.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPesquisarProduto.FlatAppearance.BorderSize = 0;
            this.btnPesquisarProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisarProduto.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisarProduto.ForeColor = System.Drawing.Color.White;
            this.btnPesquisarProduto.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarProduto.Image")));
            this.btnPesquisarProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisarProduto.Location = new System.Drawing.Point(313, 173);
            this.btnPesquisarProduto.Margin = new System.Windows.Forms.Padding(2);
            this.btnPesquisarProduto.Name = "btnPesquisarProduto";
            this.btnPesquisarProduto.Size = new System.Drawing.Size(149, 30);
            this.btnPesquisarProduto.TabIndex = 3;
            this.btnPesquisarProduto.Text = "Produto - F5";
            this.btnPesquisarProduto.UseVisualStyleBackColor = false;
            this.btnPesquisarProduto.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label2.Location = new System.Drawing.Point(180, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Qtd - F2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label1.Location = new System.Drawing.Point(12, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Codigo - F1";
            // 
            // lblObservacao
            // 
            this.lblObservacao.AutoSize = true;
            this.lblObservacao.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservacao.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.lblObservacao.Location = new System.Drawing.Point(12, 74);
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.Size = new System.Drawing.Size(102, 16);
            this.lblObservacao.TabIndex = 1;
            this.lblObservacao.Text = "Complementos";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.Purple;
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(12, 91);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(450, 2);
            this.materialDivider1.TabIndex = 0;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // bindExtras
            // 
            this.bindExtras.DataSource = typeof(ObjetoTransferencia.Produtos);
            // 
            // bindExtraItens
            // 
            this.bindExtraItens.DataSource = typeof(ObjetoTransferencia.VendaDetalhes);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "RequestId";
            this.dataGridViewTextBoxColumn2.HeaderText = "RequestId";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn3.HeaderText = "ProductCode";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Complemento";
            this.dataGridViewTextBoxColumn4.HeaderText = "Complemento";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Qtd";
            this.dataGridViewTextBoxColumn5.HeaderText = "Qtd";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Observacao";
            this.dataGridViewTextBoxColumn6.HeaderText = "Observacao";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "DataAbertura";
            this.dataGridViewTextBoxColumn7.HeaderText = "DataAbertura";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Status";
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ProductNome";
            this.dataGridViewTextBoxColumn9.HeaderText = "ProductNome";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "ProductPrecoVenda";
            this.dataGridViewTextBoxColumn10.HeaderText = "ProductPrecoVenda";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ProductMetricUnit";
            this.dataGridViewTextBoxColumn11.HeaderText = "ProductMetricUnit";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "SubTotal";
            this.dataGridViewTextBoxColumn12.HeaderText = "SubTotal";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // FrmPedidoComplemento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 243);
            this.Controls.Add(this.panelCorpo);
            this.Controls.Add(this.pTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPedidoComplemento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adicionar Complemento Extra";
            this.Load += new System.EventHandler(this.FrmPedidoComplemento_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPedidoComplemento_KeyDown);
            this.pTitulo.ResumeLayout(false);
            this.pTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloser)).EndInit();
            this.panelCorpo.ResumeLayout(false);
            this.panelCorpo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtdExtra)).EndInit();
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindExtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindExtraItens)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pTitulo;
        public System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.PictureBox pbCloser;
        private System.Windows.Forms.Panel panelCorpo;
        private System.Windows.Forms.Label lblObservacao;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Label lblNomeProduto;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.BindingSource bindExtraItens;
        private System.Windows.Forms.BindingSource bindExtras;
        private System.Windows.Forms.NumericUpDown nudQtdExtra;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodComplemento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPesquisarProduto;
        private System.Windows.Forms.Label label2;
    }
}