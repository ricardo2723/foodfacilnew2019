﻿using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using UI.GenericFunctions;
using UI.StylesForms;

namespace UI.Pedido
{
    public partial class FrmPedidoComplemento : Form
    {
        ViewComplementsCategoriesGenerate ComplementCategoryGenerente = new ViewComplementsCategoriesGenerate();
        public VendaDetalhesCollections vendaDetalhesCollections = new VendaDetalhesCollections();
        
        private VendaDetalhes vendaDetalhesComplementos;

        public FrmPedidoComplemento(VendaDetalhes vendaDetalhes)
        {
            InitializeComponent();

            vendaDetalhesComplementos = vendaDetalhes;

            ConfigSkin.ThemeForm(this, lbTitulo, "Complementos do Pedido Nº " + vendaDetalhesComplementos.idVendaCabecalhoDetalhes.ToString("000000"));

            CarregaEspeciais();
            ComplementCategoryGenerente.LoadComplements(vendaDetalhesComplementos, flpComplementos, checkBox_Click);
            lblCodigo.Text = vendaDetalhesComplementos.codigoProduto.ToString();
            lblNomeProduto.Text = vendaDetalhesComplementos.descricaoProduto;
        }

        private void pTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            MoveForm.MouseMoveForm(this);
        }

        private void pbCloser_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmPedidoComplemento_Load(object sender, EventArgs e)
        {

        }

        private void CarregaEspeciais()
        {
            EspeciaisNegocios especiaisNegocios = new EspeciaisNegocios();
            EspeciaisCollections especiaisCollections = new EspeciaisCollections();
            especiaisCollections = especiaisNegocios.BuscarEspeciaisGrupo(vendaDetalhesComplementos.grupoId);

            int i = 1;

            foreach (var itemCat in especiaisCollections)
            {
                CheckBox checkBox = new CheckBox();

                checkBox.AutoSize = true;
                checkBox.Font = new Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                checkBox.ForeColor = Color.Black;
                checkBox.Name = "cbEspecial" + i;
                checkBox.TabIndex = 3;
                checkBox.Text = itemCat.especiaisDescricao;
                checkBox.UseVisualStyleBackColor = true;
                
                /*
                foreach (string obs in observacaoSplit)
                {
                    if (checkBox.Text == obs.Trim())
                    {
                        checkBox.Checked = true;
                    }
                }*/

                flpObservacoes.Controls.Add(checkBox);
                i++;
            }
        }

        private void checkBox_Click(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            string nomeControls = cb.Name;

            if (cb.Checked) ComplementChecked(true, nomeControls);
            else ComplementChecked(false, nomeControls);
        }

        private void ComplementChecked(bool check, string nomeControls)
        {
            foreach (Control c in flpComplementos.Controls)
            {
                if (c is FlowLayoutPanel)
                {
                    if (c.Name == nomeControls)
                    {
                        foreach (Control flp in c.Controls)
                        {
                            if (flp is NumericUpDown)
                            {
                                if (flp.Name == nomeControls)
                                {
                                    flp.Visible = check;
                                    flp.Text = "1";
                                }
                            }

                        }
                    }
                }
            }
        }

        // ADICIONA OS COMPLEMENTOS DO ITEM AO BANCO E FAZ O RELACIONAMENTO COM ELE
        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            /*RequestItemsBusiness rib = new RequestItemsBusiness();
            string complemento = "";
            string observacao = "";

            rib.DeleteItemComplemento(vendaDetalhesComplementos);

            SetItemsComments();
            SetItemsComplements();

            if (listComments.Count > 0)
            {
                foreach (var itemLista in listComments)
                {
                    if (observacao == "") observacao = itemLista.nome;
                    else observacao += " | " + itemLista.nome;
                }
            }

            foreach (var itemLista in vendaDetalhesCollections)
            {
                RequestsItems requestItems = new RequestsItems();

                requestItems.RequestId = itemLista.RequestId;
                requestItems.ProductCode = itemLista.ProductCode;
                requestItems.Qtd = itemLista.Qtd;
                requestItems.ItemId = vendaDetalhesComplementos.Id;
                requestItems.Status = 0;

                rib.InsertItem(requestItems);

                if (complemento == "") complemento = itemLista.ProductNome + " * " + itemLista.Qtd;
                else complemento += " | " + itemLista.ProductNome + " * " + itemLista.Qtd;
            }

            rib.UpdateItem(vendaDetalhesComplementos.Id, complemento, observacao);

            this.DialogResult = DialogResult.Yes;*/
        }

        // PEGA TODOS AS OBSERVAÇÕES SELECIONADAS E SALVA EM UMA LISTA
        private void SetItemsComments()
        {
            /*foreach (Control flpO in flpObservacoes.Controls)
            {
                if (flpO is CheckBox && (flpO.AccessibilityObject.State & AccessibleStates.Checked) != 0)
                {
                    Comments itemComment = new Comments();
                    itemComment.nome = flpO.Text;

                    listComments.Add(itemComment);
                }
            }*/
        }

        // PEGA TODOS OS COMPLEMENTOS SELECIONADOS E SALVA EM UMA LISTA
        private void SetItemsComplements()
        {
            /*foreach (Control flpC in flpComplementos.Controls)
            {
                if (flpC is FlowLayoutPanel)
                {
                    foreach (Control flpCI in flpC.Controls)
                    {
                        if (flpCI is CheckBox && (flpCI.AccessibilityObject.State & AccessibleStates.Checked) != 0)
                        {
                            if (!ExistInList(flpCI.Text, flpC))
                            {
                                RequestsItems requestItem = new RequestsItems();

                                foreach (Control flpCiDados in flpC.Controls)
                                {
                                    if (flpCiDados.Name == "lblProdutoCode")
                                    {
                                        requestItem.ProductCode = int.Parse(flpCiDados.Text);
                                    }

                                    if (flpCiDados is NumericUpDown)
                                    {
                                        requestItem.Qtd = decimal.Parse(flpCiDados.Text);
                                    }
                                }

                                requestItem.RequestId = vendaDetalhesComplementos.RequestId;
                                requestItem.ProductNome = flpCI.Text;
                                vendaDetalhesCollections.Add(requestItem);
                            }
                        }
                        else
                        {
                            DeleteItemList(flpCI.Text);
                        }
                    }
                }
            }*/
        }

        //EVITA O CADASTRO DUPLICADO DE UM ITEM DEPOIS QUE ELE FOI CHECADO E DESCHECADO
        private bool ExistInList(string control, Control flpC)
        {
            /*foreach (var itemCheck in vendaDetalhesCollections)
            {
                if (itemCheck.ProductNome == control)
                {
                    foreach (Control itemCheckControl in flpC.Controls)
                    {
                        if (itemCheckControl is NumericUpDown)
                        {
                            if (itemCheckControl.Text == itemCheck.Qtd.ToString())
                            {
                                return true;
                            }
                            else
                            {
                                itemCheck.Qtd = decimal.Parse(itemCheckControl.Text);
                                return true;
                            }

                        }
                    }
                }
            }*/
            return false;
        }

        //EXCLUI UM ITEM DA LISTA
        private void DeleteItemList(string item)
        {
            /*for (int i = 0; i < vendaDetalhesCollections.Count; i++)
            {
                if (item == vendaDetalhesCollections[i].ProductNome)
                {
                    vendaDetalhesCollections.RemoveAt(i);
                }
            }*/
        }
    }
}
