﻿using Negocios;
using Negocios.Validador;
using ObjetoTransferencia;
using Relatorios;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using UI.GenericFunctions;

namespace UI.Pedido
{
    public partial class FrmPedidoAberto : Form
    {
        VendaCabecalho _pedido;
        VendaCabecalhoNegocios pedidoNegocios = new VendaCabecalhoNegocios();
        AcaoGrud _checaExistePedido;
        AcaoGrud _checaProdutoEstoque;
        VendaDetalhesCollections listaItensPedido = new VendaDetalhesCollections();
        VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();
        ProdutosNegocios produtosNegocios = new ProdutosNegocios();

        public FrmPedidoAberto(AcaoGrud checaExistePedido, VendaCabecalho pedido)
        {
            InitializeComponent();
            StylesForms.ConfigSkin.ThemeForm(this, lbTitulo, "Abertura de Pedido");
            this._checaExistePedido = checaExistePedido;
            this._pedido = pedido;
        }

        #region AÇÕES

        private void pTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            MoveForm.MouseMoveForm(this);
        }

        private void pbCloser_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmPedidoAberto_Load(object sender, EventArgs e)
        {
            if (_checaExistePedido == AcaoGrud.NoExistPedido)
            {
                FrmPedidoTipoAtendimento fpta = new FrmPedidoTipoAtendimento();
                DialogResult retorno = fpta.ShowDialog();
                if (retorno == DialogResult.Yes)
                {
                    //this.getRequest = fpta.request;                    
                    //SetControlsInformation(getRequest);
                }
                else
                    this.Close();
            }
            else
            {
                SetControlsInformation(_pedido);
                LoadComboBox();
                cbAtendente.SelectedValue = _pedido.usuarioId;
                nudNumClientes.Value = _pedido.numeroPessoas;
                CarregaDataGrid(CarregarItensPedidos());
            }
        }

        private void FrmPedidoAberto_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F5:
                    GetProductSearch();
                    break;
                case Keys.F12:
                    btnEnviarPedido.PerformClick();
                    break;
                case Keys.F1:
                    btnComplentos.PerformClick();
                    break;
                default:
                    break;
            }
        }

        private void txtCodProduto_KeyDown(object sender, KeyEventArgs e)
        {

            if (txtCodProduto.Text != null && txtQtdProduto.Value > 0)
            {
                switch (e.KeyCode)
                {
                    case Keys.Enter:
                        AddItem(Convert.ToInt32(txtCodProduto.Text), 'P');
                        break;
                    default:
                        break;
                }
            }
        }

        private void txtCodProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtQtdProduto_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (txtCodProduto.Text != "" && txtQtdProduto.Value > 0)
                        AddItem(Convert.ToInt32(txtCodProduto.Text), 'P');
                    txtCodProduto.Focus();
                    break;
                default:
                    break;
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            GetProductSearch();
        }

        private void btnComplentos_Click(object sender, EventArgs e)
        {
            if (bindItensPedidos.Count > 0)
            {
                if ((bindItensPedidos.Current as VendaDetalhes).statusVendaDetalhes > 1)
                {
                    MessageBox.Show("Não e Possivel Adicionar o complemento, o item ja foi enviado para a cozinha.",
                        "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if ((bindItensPedidos.Current as VendaDetalhes).tipoProduto == 'E')
                {
                    MessageBox.Show("Não e Possivel Adicionar o complemento ao um item Extra.",
                        "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    AddComplements();
                }
            }
            else
            {
                MessageBox.Show("Não existem itens para atribuir esse Complemento", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodProduto.Focus();
            }
        }

        private void nudNumClientes_ValueChanged(object sender, EventArgs e)
        {
            /*pedidoNegocios.ChangerQtdPeople(nudNumClientes.Value, lblNumPedido.Text);
            SumDataGridSubTotal();*/
        }

        private void dgPedidoItems_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    DeleteItemRequest();
                    break;
                default:
                    break;
            }
        }

        private void FrmPedidoAberto_FormClosed(object sender, FormClosedEventArgs e)
        {
            CheckRequestClose();
        }

        private void btnEnviarPedido_Click(object sender, EventArgs e)
        {/*
            if (bindItensPedidos.Count > 0)
            {
                int c = 0;

                List<VendaDetalhes> _listaItensPedidos = new List<VendaDetalhes>();
                List<VendaDetalhes> _tempListaItensPedidos = new List<VendaDetalhes>();

                _listaItensPedidos = bindItensPedidos.DataSource as List<VendaDetalhes>;

                _tempListaItensPedidos = _listaItensPedidos.Where(x => x.statusVendaDetalhes == 1).ToList();

                /*for (int i = 0; i < bindItensPedidos.Count; i++)
                {
                    if (Convert.ToInt32(dgCupom.Rows[i].Cells[7].Value) == 1)
                        c++;
                }*/

               /* if (_tempListaItensPedidos.Count > 0)
                {
                    try
                    {
                        if (Generic.checaImpressao() == 3)
                        {
                            VendaCabecalho vendaCabecalho = new VendaCabecalho();

                            vendaCabecalho.idVendaCabecalho = (bindItensPedidos.Current as VendaDetalhes).idVendaCabecalhoDetalhes;
                            vendaCabecalho.nomeAtendimento = (bindItensPedidos.Current as VendaDetalhes).tipoAtendimento;
                            vendaCabecalho.atendenteVendaCabecalho = (bindItensPedidos.Current as VendaDetalhes).atendenteVenda;

                            DialogResult Imprimir = MessageBox.Show("Deseja Imprimir o Pedido?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                            if (Imprimir == DialogResult.Yes)
                            {
                                frmRltImprimirCupom objFrmImpCupom = new frmRltImprimirCupom(vendaCabecalho, imprimiCupom.imprimePedido, "");
                                objFrmImpCupom.ShowDialog();
                            }
                        }
                        else
                            MessageBox.Show("Pedido Enviado com Sucesso", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);


                        vendaDetalhes.idVendaCabecalhoDetalhes = Convert.ToInt32(numeroVenda);
                        vendaDetalhesNegocios.enviarVenda(vendaDetalhes, Generic.checaImpressao());

                        dgCupom.Rows.Clear();
                        CarregarDataGrid();


                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Erro ao enviar o pedido", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    MessageBox.Show("Todos os Pedidos ja foram entregues", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Não existem Itens para Enviar", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Focus();
            }*/
        }

        private void btnPagamento_Click(object sender, EventArgs e)
        {
            /*CashBoxesBusiness cbb = new CashBoxesBusiness();
            List<CashBoxes> caixa = new List<CashBoxes>();
            caixa = cbb.GetCashBoxes();

            if (caixa.Count > 0)
            {
                
            }
            else
            {
                MessageBox.Show("O Caixa encontra-se Fechado, E necessário abrir o caixa para poder finalizar o Pedido",
                    "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                FrmAbreFechaCaixa afc = new FrmAbreFechaCaixa();
                afc.ShowDialog();
            }*/
        }

        #endregion

        #region METODOS

        private void LoadComboBox()
        {
            UsuariosNegocios usuariosNegocios = new UsuariosNegocios();
            try
            {
                bindAtendente.DataSource = usuariosNegocios.ConsultarPorNome("");
            }
            catch (Exception)
            {
                MessageBox.Show("Nenhum Registro Encotrado para sua Pesquisa", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void CarregaDataGrid(VendaDetalhesCollections itemsPedidos)
        {
            bindItensPedidos.DataSource = null;
            bindItensPedidos.DataSource = itemsPedidos;
            ImageRowDisplay();
            SumDataGridSubTotal();
            //dgPedidoItems.FirstDisplayedScrollingRowIndex = dgPedidoItems.Rows.Count - 1;
        }

        private void GetProductSearch()
        {
            /*FrmListaProdutos produtos = new FrmListaProdutos(AcaoGrud.GetProduct);
            DialogResult resultado = produtos.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                txtCodProduto.Text = produtos.GetRowSelectDataGrid().Codigo.ToString();
                AddItem();
                txtCodProduto.Focus();
            }
            else
            {
                txtCodProduto.Text = null;
                txtCodProduto.Focus();
            }*/
        }

        private ProdutosCollections GetProduto(int codigoProduto, char tipoProduto)
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            ProdutosCollections produtosCollections = vendaCabecalhoNegocios.GetProdutoVenda(tipoProduto, codigoProduto);

            return produtosCollections;
        }

        public void AtualizaEstoqueProdutoNaoManufaturado(VendaDetalhes vendaDetalhes)
        {
            try
            {
                vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQtdProduto.Text);

                string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                int checaretorno = Convert.ToInt32(retorno);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public void AtualizaEstoqueProdutoManufaturado(VendaDetalhes vendaDetalhes)
        {
            try
            {
                //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQtdProduto.Text);

                string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes);

                int checaretorno = Convert.ToInt32(retorno);

                //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                ProdutosCollections produtosCollectionsManufaturado = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                foreach (var item in produtosCollectionsManufaturado)
                {
                    vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                    vendaDetalhesManufaturado.estoqueProduto = vendaDetalhes.estoqueProduto;

                    retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                    checaretorno = Convert.ToInt32(retorno);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public void AtualizaProdutoManufaturadoNaoControlaEstoque(VendaDetalhes vendaDetalhes)
        {
            try
            {
                //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                ProdutosCollections produtosCollectionsManufaturados = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                foreach (var item in produtosCollectionsManufaturados)
                {
                    decimal qtd1 = item.quantidadeProdutoManufaturado;
                    decimal totalEst = qtd1 * Convert.ToDecimal(txtQtdProduto.Text);

                    if (item.estoqueProduto < totalEst)
                    {
                        MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }

                foreach (var item in produtosCollectionsManufaturados)
                {
                    vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                    vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                    vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                    if (vendaDetalhesManufaturado.estoqueProduto < Convert.ToDecimal(txtQtdProduto.Text))
                    {
                        MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto - (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * Convert.ToDecimal(txtQtdProduto.Text));

                    string retorno = vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado);

                    Convert.ToInt32(retorno);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void ChecaEstoque(VendaDetalhes vendaDetalhes)
        {
            //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
            if (vendaDetalhes.controlaEstoqueProduto == 'S')
            {
                //SE EXISTIR ESTOQUE SUFICIENTE VERIFICA SE O ESTOQUE ESTA CRITICO
                if ((vendaDetalhes.estoqueProduto - Convert.ToDecimal(txtQtdProduto.Text)) <= vendaDetalhes.estoqueCriticoProduto)
                {
                    MessageBox.Show("O estoque está abaixo do Permitido. Providenciar o Produto. Estoque: " + Convert.ToInt32(vendaDetalhes.estoqueProduto) + " Unidades", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                if (vendaDetalhes.manufaturadoProduto == 'N')
                {
                    _checaProdutoEstoque = AcaoGrud.ProdutoNaoManufaturadoAtualizaEstoque;
                }
                else if (vendaDetalhes.manufaturadoProduto == 'S')
                {
                    _checaProdutoEstoque = AcaoGrud.ProdutoManufaturadoAtualizaEstoque;
                }
            }
            else if (vendaDetalhes.controlaEstoqueProduto == 'N')
            {
                if (vendaDetalhes.manufaturadoProduto == 'S')
                {
                    //ATUALIZA ESTOQUE DO PRODUTOS MANUFATURADOS
                    _checaProdutoEstoque = AcaoGrud.ProdutoManufaturadoAtualizaNaoControlaEstoque;
                }
            }

        }

        private void AddItem(int codigoProduto, char tipoProduto)
        {
            Produtos produtos = new Produtos();
            try
            {
                VendaDetalhes vendaDetalhes = new VendaDetalhes();

                produtos = GetProduto(codigoProduto, tipoProduto).FirstOrDefault();

                if (produtos != null)
                {
                    vendaDetalhes.idVendaCabecalhoDetalhes = int.Parse(lblNumPedido.Text);
                    vendaDetalhes.codigoProduto = produtos.codigoProduto;
                    vendaDetalhes.quantidadeVendaDetalhes = int.Parse(txtQtdProduto.Text);
                    vendaDetalhes.controlaEstoqueProduto = produtos.controlaEstoqueProduto;
                    vendaDetalhes.estoqueProduto = produtos.estoqueProduto;
                    vendaDetalhes.estoqueCriticoProduto = produtos.estoqueCriticoProduto;
                    vendaDetalhes.manufaturadoProduto = produtos.manufaturadoProduto;
                    vendaDetalhes.descricaoProduto = produtos.descricaoProduto;
                    vendaDetalhes.tipoProduto = produtos.tipoProduto;
                    vendaDetalhes.grupoId = produtos.grupoId;

                    lblNomeProduto.Text = produtos.descricaoProduto;

                    vendaDetalhes.valorUnitarioVendaDetalhes = produtos.valorVendaProduto;
                    vendaDetalhes.valorTotalVendaDetalhes = vendaDetalhes.quantidadeVendaDetalhes * produtos.valorVendaProduto;

                    vendaDetalhes.statusVendaDetalhes = 1;

                    vendaDetalhes.dataEnvioVendaDetalhes = DateTime.Now;

                    //VERIFICA SE EXISTE O PRODUTO EM ESTOQUE PARA A VENDA E SE E MAIOR QUE O PEDIDO
                    if (vendaDetalhes.controlaEstoqueProduto == 'S' && vendaDetalhes.estoqueProduto < Convert.ToDecimal(txtQtdProduto.Text))
                    {
                        MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        txtCodProduto.Text = "";
                        return;
                    }

                    ChecaEstoque(vendaDetalhes);

                    if (_checaProdutoEstoque == AcaoGrud.ProdutoNaoManufaturadoAtualizaEstoque)
                    {
                        //ATUALIZA O ESTOQUE DO PRODUTO NAO  MANUFATURADO
                        AtualizaEstoqueProdutoNaoManufaturado(vendaDetalhes);
                    }

                    if (_checaProdutoEstoque == AcaoGrud.ProdutoManufaturadoAtualizaEstoque)
                    {
                        //ATUALIZA O ESTOQUE DO PRODUTO MANUFATURADO
                        AtualizaEstoqueProdutoManufaturado(vendaDetalhes);
                    }

                    if (_checaProdutoEstoque == AcaoGrud.ProdutoManufaturadoAtualizaNaoControlaEstoque)
                    {
                        AtualizaProdutoManufaturadoNaoControlaEstoque(vendaDetalhes);
                    }

                    string retorno1 = vendaDetalhesNegocios.inserir(vendaDetalhes);

                    try
                    {
                        vendaDetalhes.idVendaDetalhes = Convert.ToInt32(retorno1);

                        VendaDetalhes vendaDetalhesCurrent = new VendaDetalhes();
                        vendaDetalhesCurrent = (bindItensPedidos.Current as VendaDetalhes);

                        if (vendaDetalhes.tipoProduto == 'E')
                        {
                            vendaDetalhesNegocios.inserirItemExtra(vendaDetalhesCurrent.idVendaDetalhes, vendaDetalhes.idVendaDetalhes, vendaDetalhes.idVendaCabecalhoDetalhes);
                            vendaDetalhesNegocios.AtualizarItemExtra(" " + vendaDetalhesCurrent.extraVendaDetalhes + vendaDetalhes.descricaoProduto + " = " + vendaDetalhes.quantidadeVendaDetalhes + " | ", vendaDetalhesCurrent.idVendaDetalhes);

                            MessageBox.Show("Complemento Extra adicionado com Sucesso", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        CarregaDataGrid(CarregarItensPedidos());

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno1, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
                }

                SumDataGridSubTotal();
                txtCodProduto.Text = null;
                txtQtdProduto.Text = "1";
                txtCodProduto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Não foi Possivel Inserir o Item. " + ex, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void AddComplements()
        {
            FrmPedidoComplemento fpc = new FrmPedidoComplemento((bindItensPedidos.Current as VendaDetalhes));
            fpc.ShowDialog();

            if (fpc.codigoProdutoExtra != 0)
            {
                AddItem(fpc.codigoProdutoExtra, 'E');
            }
            else
            {
                MessageBox.Show("Ocorreu um error ao adicionar o item extra", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            txtCodProduto.Focus();
        }

        private void DeleteItemRequest()
        {
            VendaDetalhes vendaDetalhes = (bindItensPedidos.Current as VendaDetalhes);

            if (vendaDetalhes.statusVendaDetalhes == 1)
            {
                if (vendaDetalhes.tipoProduto == 'P')
                {
                    SetRowItemComplements();
                }
                else
                {
                    /*RequestItemsBusiness rib = new RequestItemsBusiness();
                    CheckResultQuery.ResultQuery(rib.DeleteItem(vendaDetalhes), null, 1);
                    CarregaDataGrid(LoadItems());
                    txtCodProduto.Focus();*/
                }
            }
            else
            {
                /// SOLICTAR SENHA DO ADMINISTRADOR PARA ITEMS QUE JA FORAM ENVIADOS PARA A COZINHA
                /// SE AUTORIZADO IMPRIMIR CUPOM EXCLUINDO ITEM

                /*RequestItemsBusiness rib = new RequestItemsBusiness();
                if (MessageBox.Show("O item " + ri.ProductNome + " que deseja Excluir já foi enviado para o Preparo. Somente o Gerente poderá Excluir o Item do Pedido", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    CheckResultQuery.ResultQuery(rib.DeleteItem(ri), null, 1);
                    CarregaDataGrid(LoadItems());
                }*/
                MessageBox.Show(
                    "O item " + vendaDetalhes.descricaoProduto +
                    " que deseja Excluir já foi enviado para o Preparo. Somente o Gerente poderá Excluir o Item do Pedido",
                    "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SetRowItemComplements()
        {
            foreach (DataGridViewRow row in dgPedidoItems.Rows)
            {
                if (int.Parse(row.Cells["idVendaDetalhesDataGridViewTextBoxColumn"].Value.ToString()) == (bindItensPedidos.Current as VendaDetalhes).idVendaDetalhes)
                {
                    dgPedidoItems.Rows[row.Index].Selected = true;
                    break;
                }
            }
            AddComplements();
        }

        private VendaDetalhesCollections CarregarItensPedidos()
        {
            VendaDetalhesNegocios itensPedidos = new VendaDetalhesNegocios();
            return itensPedidos.BuscaItensPedido(int.Parse(lblNumPedido.Text));
        }

        private void SumDataGridSubTotal()
        {
            decimal subTotal = 0;
            foreach (DataGridViewRow item in dgPedidoItems.Rows)
            {
                subTotal += decimal.Parse(item.Cells[7].Value.ToString());
            }
            lblSubTotal.Text = "Total: " + subTotal.ToString("C");
            lblPessoa.Text = "Por Pessoa: " + (subTotal / decimal.Parse(nudNumClientes.Value.ToString())).ToString("C");
            lblTotalItems.Text = "Total Itens: " + dgPedidoItems.Rows.Count.ToString("00");
        }

        private int CheckIdDataGridNotSend()
        {
            int cont = 0;
            foreach (DataGridViewRow item in dgPedidoItems.Rows)
            {
                if (int.Parse(item.Cells[11].Value.ToString()) > 0)
                {
                    cont += 1;
                }
            }
            return cont;
        }

        private void DelItemAndRequestIfNotSend()
        {
            VendaCabecalhoNegocios pedidoNegocios = new VendaCabecalhoNegocios();
            VendaCabecalho pedido = new VendaCabecalho();
            pedido.idVendaCabecalho = int.Parse(lblNumPedido.Text);
            //if (int.Parse(pedidoNegocios.Excluir(request)) == 1) this.DialogResult = DialogResult.Yes;
            //else this.DialogResult = DialogResult.No;
        }

        private void DelOnlyItemIfNotSend()
        {
            /*listaItemsPedido = null;
            listaItemsPedido = dgPedidoItems.DataSource as List<RequestsItems>;
            RequestItemsBusiness rib = new RequestItemsBusiness();

            foreach (var item in listaItemsPedido)
            {
                if (item.Status == 0 && item.ItemId == 0)
                {
                    rib.DeleteItem(item);
                }
            }*/
        }

        private void CheckRequestClose()
        {
            if (dgPedidoItems.Rows.Count == 0 || CheckIdDataGridNotSend() == 0)
            {
                DelItemAndRequestIfNotSend();
            }
            else if (CheckIdDataGridNotSend() != dgPedidoItems.Rows.Count)
            {
                DelOnlyItemIfNotSend();
            }
        }

        private void SetControlsInformation(VendaCabecalho setPedido)
        {
            lblNumPedido.Text = setPedido.idVendaCabecalho.ToString("000000");
            lblNumAtendimento.Text = setPedido.nomeAtendimento + " " + setPedido.numeroAtendimento.ToString("000");
            //lblTempo.Text = DateTime.Now.Subtract(setPedido.DataAbertura).Hours.ToString("00") + "h" + DateTime.Now.Subtract(setPedido.DataAbertura).Minutes.ToString("00") + "m";
            //nudNumClientes.Value = setPedido.QtdPessoas;
            cbAtendente.SelectedValue = setPedido.usuarioId;
        }

        public void ImageRowDisplay()
        {
            foreach (DataGridViewRow itemGrid in dgPedidoItems.Rows)
            {
                if (Char.Parse(itemGrid.Cells[32].Value.ToString()) == 'E')
                {
                    ((TextAndImageCell)itemGrid.Cells[1]).Image = (Image)imageList1.Images[1];
                }
            }
        }
        #endregion

        private void btnEcluirItem_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Deseja Excluir o Item " + (bindItensPedidos.Current as VendaDetalhes).descricaoProduto + " ", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if ((bindItensPedidos.Current as VendaDetalhes).statusVendaDetalhes > 1)
                {
                    //VERIFICAR SE QUEM ESTA EXCLUINDO E ATENDENTE OU GERENTE - SOMENTE GERENTE PODE EXCLUIR ITEM JA ENVIADO
                    MessageBox.Show("Usuário sem permissão para excluir um item que ja foi enviado para Cozinha. Entre com a Senha do Administrador", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    frmPermissaoAdm objPermissao = new frmPermissaoAdm();
                    DialogResult permissao = objPermissao.ShowDialog();

                    if (permissao == DialogResult.Yes)
                    {
                        PedidoItemExcluir();
                    }
                    else
                    {
                        MessageBox.Show("E necessário Autorização de um Administrador para efetuar Retiradas.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    PedidoItemExcluir();
                }
            }
        }

        private void PedidoItemExcluir()
        {
            if ((bindItensPedidos.Current as VendaDetalhes).tipoProduto == 'E')
            {
                string descricao = "";
                VendaDetalhesNegocios itemExt = new VendaDetalhesNegocios();

                VendaDetalhesItemExtraCollections itemExtras = itemExt.buscarProdItensExtraAtualizar((bindItensPedidos.Current as VendaDetalhes).idVendaDetalhes);

                foreach (var itemA in itemExtras)
                {
                    VendaDetalhesCollections item2 = itemExt.buscarProdutoItensExtra(itemA.IdVendasDetalhesExtra);
                    //VERIFICA SE E O ULTIMO EXTRA A EXCLUIR E ENTAO LIMPA A DESCRIÇÃO DO EXTRA
                    if (item2.Count == 0)
                    {
                        descricao = "";
                        itemExt.AtualizarItemExtra(descricao, itemA.idVendasDetalhesItem);
                    }
                    else
                    {
                        foreach (var itemExtra in item2)
                        {
                            descricao += itemExtra.descricaoProduto + " = " + itemExtra.quantidadeVendaDetalhes + " | ";
                            itemExt.AtualizarItemExtra(descricao, itemA.idVendasDetalhesItem);
                        }
                    }
                }
            }

            if(ExcluirItem() == 1)
            {
                CarregaDataGrid(CarregarItensPedidos());
                MessageBox.Show("Item Excluido com Sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
                
        }

        private int ExcluirItem()
        {
            excluirItemUpEstoque((bindItensPedidos.Current as VendaDetalhes).codigoProduto, (bindItensPedidos.Current as VendaDetalhes).quantidadeVendaDetalhes, (bindItensPedidos.Current as VendaDetalhes).idVendaDetalhes, (bindItensPedidos.Current as VendaDetalhes).tipoProduto);

            VendaDetalhesItemExtraCollections itemExtra = vendaDetalhesNegocios.buscarItensExtra((bindItensPedidos.Current as VendaDetalhes).idVendaDetalhes);

            foreach (var itemEx in itemExtra)
            {
                VendaDetalhesCollections itemProd = vendaDetalhesNegocios.buscarProdutoItensExtra(itemEx.IdVendasDetalhesExtra);
                foreach (var itemP in itemProd)
                {
                    excluirItemUpEstoque(itemP.codigoProduto, itemP.quantidadeVendaDetalhes, itemEx.IdVendasDetalhesExtra, itemP.tipoProduto);
                }
            }

            return 1;
        }

        private void excluirItemUpEstoque(int codProd, decimal qtd, int idVenda, char tipoProduto)
        {
            //DEVOLVE ITEM EXCLUIDO AO ESTOQUE

            Produtos produtos = new Produtos();
            produtos.codigoProduto = Convert.ToInt32(codProd);
            VendaDetalhes vendaDetalhes = new VendaDetalhes();

            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            ProdutosCollections produtosCollections = vendaCabecalhoNegocios.GetProdutoVenda(tipoProduto, produtos.codigoProduto);

            if (produtosCollections.Count > 0)
            {
                foreach (var item in produtosCollections)
                {
                    vendaDetalhes.codigoProduto = item.codigoProduto;
                    vendaDetalhes.controlaEstoqueProduto = item.controlaEstoqueProduto;
                    vendaDetalhes.estoqueProduto = item.estoqueProduto;
                    vendaDetalhes.estoqueCriticoProduto = item.estoqueCriticoProduto;
                    vendaDetalhes.manufaturadoProduto = item.manufaturadoProduto;
                }

                //VERIFICA SE O PRODUTO E CONTROLADO NO ESTOQUE
                if (vendaDetalhes.controlaEstoqueProduto == 'S')
                {
                    //VERIFICA SE O PRODUTO E MANUFATURADO. SE NAO É MANUFATURADO APENAS ATUALIZA O ESTOQUE
                    if (vendaDetalhes.manufaturadoProduto == 'N')
                    {
                        try
                        {
                            vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                            Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }

                    }
                    else if (vendaDetalhes.manufaturadoProduto == 'S')
                    {
                        try
                        {
                            //ATUALIZA O ESTOQUE POIS O PRODUTO E MANUFATURADO MAIS O MESMO E CONTROLADO NO ESTOQUE
                            vendaDetalhes.estoqueProduto = vendaDetalhes.estoqueProduto + qtd;

                            Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhes));

                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                            foreach (var item in produtosCollections)
                            {
                                vendaDetalhesManufaturado.codigoProduto = item.codigoProduto;
                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                else if (vendaDetalhes.controlaEstoqueProduto == 'N')
                {
                    if (vendaDetalhes.manufaturadoProduto == 'S')
                    {
                        try
                        {
                            //ATUALIZA O ESTOQUE DE TODOS OS PRODUTOS QUE FAZEM PARTE DESSE PRODUTO
                            produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(vendaDetalhes.codigoProduto, 'P');

                            VendaDetalhes vendaDetalhesManufaturado = new VendaDetalhes();

                            foreach (var item in produtosCollections)
                            {
                                decimal qtd1 = item.quantidadeProdutoManufaturado;
                                decimal totalEst = qtd1 * qtd;

                                if (item.estoqueProduto < totalEst)
                                {
                                    MessageBox.Show("Estoque Insuficiente! Produto Finalizado: " + item.descricaoProduto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                            }

                            foreach (var item in produtosCollections)
                            {
                                vendaDetalhesManufaturado.codigoProduto = item.codProdutoManufaturado;
                                vendaDetalhesManufaturado.quantidadeProdutoManufaturado = item.quantidadeProdutoManufaturado;
                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto;

                                if (vendaDetalhesManufaturado.estoqueProduto < qtd)
                                {
                                    MessageBox.Show("Estoque Insuficiente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }

                                vendaDetalhesManufaturado.estoqueProduto = item.estoqueProduto + (vendaDetalhesManufaturado.quantidadeProdutoManufaturado * qtd);

                                Convert.ToInt32(vendaDetalhesNegocios.atualizarEstoque(vendaDetalhesManufaturado));

                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Não foi possivel atualizar Estoque. Detalhes: " + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                //APOS VERIFICAR ESTOQUE E ATUALIZAR SE FOR O CASO, O PRODUTO E EXCLUIDO
                Convert.ToInt32(vendaDetalhesNegocios.ExcluirItensPedido(idVenda));

            }
            else
            {
                MessageBox.Show("Produto não Existe", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
        }

        private void btnEspeciais_Click(object sender, EventArgs e)
        {
            if (bindItensPedidos.Count > 0)
            {
                if ((bindItensPedidos.Current as VendaDetalhes).statusVendaDetalhes > 1)
                {
                    MessageBox.Show("Não e Possivel Adicionar observações, o item ja foi enviado para a cozinha.",
                        "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if ((bindItensPedidos.Current as VendaDetalhes).tipoProduto == 'E')
                {
                    MessageBox.Show("Não e Possivel Adicionar observações ao um item Extra.",
                        "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                frmPdvEspecial objFrmEspecial = new frmPdvEspecial();
                objFrmEspecial.ShowDialog();

                if (objFrmEspecial.sair != "N")
                {
                    MessageBox.Show("Especial Cancelado.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    if ((bindItensPedidos.Current as VendaDetalhes).statusVendaDetalhes <= 1)
                    {
                        try
                        {
                            string descricao = Convert.ToString((bindItensPedidos.Current as VendaDetalhes).especialVendaDetalhes += objFrmEspecial.descricaoEspecial + " | ");

                            // ATUALIZAR ITENS COM ESPECIAL

                            vendaDetalhesNegocios.AtualizarItemEspecial(descricao, (bindItensPedidos.Current as VendaDetalhes).idVendaDetalhes);
                            CarregaDataGrid(CarregarItensPedidos());
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Não foi possivel Adicionar o Especial.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Não e possivel atribuir Especial para esse Produto. Pedido Entregue", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Não existem itens para atribuir Observações", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodProduto.Focus();
            }
        }
    }
}