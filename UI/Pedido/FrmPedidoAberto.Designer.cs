﻿using UI.GenericFunctions;

namespace UI.Pedido
{
    partial class FrmPedidoAberto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPedidoAberto));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pTitulo = new System.Windows.Forms.Panel();
            this.lbTitulo = new System.Windows.Forms.Label();
            this.pbCloser = new System.Windows.Forms.PictureBox();
            this.pInformações = new System.Windows.Forms.Panel();
            this.btnEspeciais = new System.Windows.Forms.Button();
            this.btnEcluirItem = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNumAtendimento = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.nudNumClientes = new System.Windows.Forms.NumericUpDown();
            this.btnComplentos = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.cbAtendente = new System.Windows.Forms.ComboBox();
            this.bindAtendente = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNumPedido = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTempo = new System.Windows.Forms.Label();
            this.pCorpo = new System.Windows.Forms.Panel();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtQtdProduto = new System.Windows.Forms.NumericUpDown();
            this.txtCodProduto = new System.Windows.Forms.TextBox();
            this.lblNomeProduto = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnPagamento = new System.Windows.Forms.Button();
            this.btnEnviarPedido = new System.Windows.Forms.Button();
            this.dgPedidoItems = new MetroFramework.Controls.MetroGrid();
            this.codigoProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoProdutoDataGridViewTextBoxColumn = new UI.GenericFunctions.TextAndImageColumn();
            this.idVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.especialVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.extraVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusVendaDetalhesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemPedidoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.atendenteVendaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoAtendimentoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.undMedidaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idFornecedorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grupoIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorCompraProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorVendaProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estoqueProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataCadastroProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.manufaturadoProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usuarioIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codProdutoManufaturadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoUndMedidaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoGrupoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeFornecedorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoProdutoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindItensPedidos = new System.Windows.Forms.BindingSource(this.components);
            this.lblPessoa = new System.Windows.Forms.Label();
            this.lblTotalItems = new System.Windows.Forms.Label();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.productCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productMetricUnitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productPrecoVendaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subTotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.complementoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.observacaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataAberturaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloser)).BeginInit();
            this.pInformações.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindAtendente)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.pCorpo.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQtdProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidoItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindItensPedidos)).BeginInit();
            this.SuspendLayout();
            // 
            // pTitulo
            // 
            this.pTitulo.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTitulo.Controls.Add(this.lbTitulo);
            this.pTitulo.Controls.Add(this.pbCloser);
            this.pTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pTitulo.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pTitulo.Location = new System.Drawing.Point(0, 0);
            this.pTitulo.Margin = new System.Windows.Forms.Padding(2);
            this.pTitulo.Name = "pTitulo";
            this.pTitulo.Size = new System.Drawing.Size(862, 30);
            this.pTitulo.TabIndex = 20;
            this.pTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pTitulo_MouseMove);
            // 
            // lbTitulo
            // 
            this.lbTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Location = new System.Drawing.Point(2, 5);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(51, 18);
            this.lbTitulo.TabIndex = 1;
            this.lbTitulo.Text = "Titulo";
            this.lbTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbCloser
            // 
            this.pbCloser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCloser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCloser.Image = ((System.Drawing.Image)(resources.GetObject("pbCloser.Image")));
            this.pbCloser.Location = new System.Drawing.Point(836, 6);
            this.pbCloser.Margin = new System.Windows.Forms.Padding(2);
            this.pbCloser.Name = "pbCloser";
            this.pbCloser.Size = new System.Drawing.Size(15, 16);
            this.pbCloser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCloser.TabIndex = 0;
            this.pbCloser.TabStop = false;
            this.pbCloser.Click += new System.EventHandler(this.pbCloser_Click);
            // 
            // pInformações
            // 
            this.pInformações.BackColor = System.Drawing.Color.Chocolate;
            this.pInformações.Controls.Add(this.btnEspeciais);
            this.pInformações.Controls.Add(this.btnEcluirItem);
            this.pInformações.Controls.Add(this.tableLayoutPanel2);
            this.pInformações.Controls.Add(this.materialDivider2);
            this.pInformações.Controls.Add(this.materialDivider1);
            this.pInformações.Controls.Add(this.nudNumClientes);
            this.pInformações.Controls.Add(this.btnComplentos);
            this.pInformações.Controls.Add(this.textBox1);
            this.pInformações.Controls.Add(this.btnPesquisar);
            this.pInformações.Controls.Add(this.cbAtendente);
            this.pInformações.Controls.Add(this.label6);
            this.pInformações.Controls.Add(this.label8);
            this.pInformações.Controls.Add(this.label3);
            this.pInformações.Controls.Add(this.tableLayoutPanel1);
            this.pInformações.Dock = System.Windows.Forms.DockStyle.Left;
            this.pInformações.Location = new System.Drawing.Point(0, 30);
            this.pInformações.Name = "pInformações";
            this.pInformações.Size = new System.Drawing.Size(258, 584);
            this.pInformações.TabIndex = 1;
            // 
            // btnEspeciais
            // 
            this.btnEspeciais.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEspeciais.AutoSize = true;
            this.btnEspeciais.BackColor = System.Drawing.Color.Transparent;
            this.btnEspeciais.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEspeciais.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEspeciais.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEspeciais.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnEspeciais.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnEspeciais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEspeciais.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEspeciais.ForeColor = System.Drawing.Color.White;
            this.btnEspeciais.Image = ((System.Drawing.Image)(resources.GetObject("btnEspeciais.Image")));
            this.btnEspeciais.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEspeciais.Location = new System.Drawing.Point(7, 405);
            this.btnEspeciais.Margin = new System.Windows.Forms.Padding(2);
            this.btnEspeciais.Name = "btnEspeciais";
            this.btnEspeciais.Size = new System.Drawing.Size(244, 29);
            this.btnEspeciais.TabIndex = 8;
            this.btnEspeciais.Text = "Observações - F2";
            this.btnEspeciais.UseVisualStyleBackColor = false;
            this.btnEspeciais.Click += new System.EventHandler(this.btnEspeciais_Click);
            // 
            // btnEcluirItem
            // 
            this.btnEcluirItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEcluirItem.AutoSize = true;
            this.btnEcluirItem.BackColor = System.Drawing.Color.Transparent;
            this.btnEcluirItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEcluirItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEcluirItem.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEcluirItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnEcluirItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnEcluirItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEcluirItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEcluirItem.ForeColor = System.Drawing.Color.White;
            this.btnEcluirItem.Image = ((System.Drawing.Image)(resources.GetObject("btnEcluirItem.Image")));
            this.btnEcluirItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEcluirItem.Location = new System.Drawing.Point(7, 471);
            this.btnEcluirItem.Margin = new System.Windows.Forms.Padding(2);
            this.btnEcluirItem.Name = "btnEcluirItem";
            this.btnEcluirItem.Size = new System.Drawing.Size(244, 29);
            this.btnEcluirItem.TabIndex = 7;
            this.btnEcluirItem.Text = "Excluir Item - DEL";
            this.btnEcluirItem.UseVisualStyleBackColor = false;
            this.btnEcluirItem.Click += new System.EventHandler(this.btnEcluirItem_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.34413F));
            this.tableLayoutPanel2.Controls.Add(this.lblNumAtendimento, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(247, 60);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // lblNumAtendimento
            // 
            this.lblNumAtendimento.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNumAtendimento.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumAtendimento.ForeColor = System.Drawing.Color.White;
            this.lblNumAtendimento.Location = new System.Drawing.Point(3, 18);
            this.lblNumAtendimento.Name = "lblNumAtendimento";
            this.lblNumAtendimento.Size = new System.Drawing.Size(241, 25);
            this.lblNumAtendimento.TabIndex = 0;
            this.lblNumAtendimento.Text = "000000";
            this.lblNumAtendimento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.White;
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(3, 355);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(247, 1);
            this.materialDivider2.TabIndex = 5;
            this.materialDivider2.Text = "materialDivider1";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.White;
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(5, 148);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(247, 1);
            this.materialDivider1.TabIndex = 5;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // nudNumClientes
            // 
            this.nudNumClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudNumClientes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudNumClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudNumClientes.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudNumClientes.Location = new System.Drawing.Point(4, 316);
            this.nudNumClientes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNumClientes.Name = "nudNumClientes";
            this.nudNumClientes.Size = new System.Drawing.Size(68, 23);
            this.nudNumClientes.TabIndex = 2;
            this.nudNumClientes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudNumClientes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNumClientes.ValueChanged += new System.EventHandler(this.nudNumClientes_ValueChanged);
            // 
            // btnComplentos
            // 
            this.btnComplentos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnComplentos.AutoSize = true;
            this.btnComplentos.BackColor = System.Drawing.Color.Transparent;
            this.btnComplentos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnComplentos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnComplentos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnComplentos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnComplentos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnComplentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComplentos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComplentos.ForeColor = System.Drawing.Color.White;
            this.btnComplentos.Image = ((System.Drawing.Image)(resources.GetObject("btnComplentos.Image")));
            this.btnComplentos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnComplentos.Location = new System.Drawing.Point(7, 372);
            this.btnComplentos.Margin = new System.Windows.Forms.Padding(2);
            this.btnComplentos.Name = "btnComplentos";
            this.btnComplentos.Size = new System.Drawing.Size(244, 29);
            this.btnComplentos.TabIndex = 4;
            this.btnComplentos.Text = "Complementos - F1";
            this.btnComplentos.UseVisualStyleBackColor = false;
            this.btnComplentos.Click += new System.EventHandler(this.btnComplentos_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(4, 255);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(246, 23);
            this.textBox1.TabIndex = 1;
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesquisar.AutoSize = true;
            this.btnPesquisar.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesquisar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPesquisar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPesquisar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.ForeColor = System.Drawing.Color.White;
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(7, 438);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(2);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(244, 29);
            this.btnPesquisar.TabIndex = 3;
            this.btnPesquisar.Text = "Pesquisar Produto - F5";
            this.btnPesquisar.UseVisualStyleBackColor = false;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // cbAtendente
            // 
            this.cbAtendente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbAtendente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindAtendente, "usuarioNome", true));
            this.cbAtendente.DataSource = this.bindAtendente;
            this.cbAtendente.DisplayMember = "usuarioNome";
            this.cbAtendente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAtendente.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAtendente.FormattingEnabled = true;
            this.cbAtendente.Location = new System.Drawing.Point(4, 193);
            this.cbAtendente.Name = "cbAtendente";
            this.cbAtendente.Size = new System.Drawing.Size(246, 24);
            this.cbAtendente.TabIndex = 0;
            this.cbAtendente.ValueMember = "usuarioId";
            // 
            // bindAtendente
            // 
            this.bindAtendente.DataSource = typeof(ObjetoTransferencia.Usuarios);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 295);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 16);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nº Clientes:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(3, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "Atendente:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Cliente:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.4413F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.5587F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNumPedido, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTempo, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 72);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(247, 67);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(54, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pedido Nº:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNumPedido
            // 
            this.lblNumPedido.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNumPedido.AutoSize = true;
            this.lblNumPedido.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPedido.ForeColor = System.Drawing.Color.White;
            this.lblNumPedido.Location = new System.Drawing.Point(135, 7);
            this.lblNumPedido.Name = "lblNumPedido";
            this.lblNumPedido.Size = new System.Drawing.Size(38, 18);
            this.lblNumPedido.TabIndex = 0;
            this.lblNumPedido.Text = "000";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(7, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tempo do Pedido:";
            // 
            // lblTempo
            // 
            this.lblTempo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTempo.AutoSize = true;
            this.lblTempo.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempo.ForeColor = System.Drawing.Color.White;
            this.lblTempo.Location = new System.Drawing.Point(135, 41);
            this.lblTempo.Name = "lblTempo";
            this.lblTempo.Size = new System.Drawing.Size(70, 18);
            this.lblTempo.TabIndex = 0;
            this.lblTempo.Text = "00h00m";
            // 
            // pCorpo
            // 
            this.pCorpo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pCorpo.Controls.Add(this.panelTop);
            this.pCorpo.Controls.Add(this.button1);
            this.pCorpo.Controls.Add(this.btnImprimir);
            this.pCorpo.Controls.Add(this.btnPagamento);
            this.pCorpo.Controls.Add(this.btnEnviarPedido);
            this.pCorpo.Controls.Add(this.dgPedidoItems);
            this.pCorpo.Controls.Add(this.lblPessoa);
            this.pCorpo.Controls.Add(this.lblTotalItems);
            this.pCorpo.Controls.Add(this.lblSubTotal);
            this.pCorpo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pCorpo.Location = new System.Drawing.Point(258, 30);
            this.pCorpo.Name = "pCorpo";
            this.pCorpo.Size = new System.Drawing.Size(604, 584);
            this.pCorpo.TabIndex = 0;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelTop.Controls.Add(this.txtQtdProduto);
            this.panelTop.Controls.Add(this.txtCodProduto);
            this.panelTop.Controls.Add(this.lblNomeProduto);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Controls.Add(this.label7);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(604, 62);
            this.panelTop.TabIndex = 0;
            // 
            // txtQtdProduto
            // 
            this.txtQtdProduto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtQtdProduto.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtdProduto.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.txtQtdProduto.Location = new System.Drawing.Point(519, 23);
            this.txtQtdProduto.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQtdProduto.Name = "txtQtdProduto";
            this.txtQtdProduto.Size = new System.Drawing.Size(67, 30);
            this.txtQtdProduto.TabIndex = 1;
            this.txtQtdProduto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQtdProduto.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQtdProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQtdProduto_KeyDown);
            // 
            // txtCodProduto
            // 
            this.txtCodProduto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtCodProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodProduto.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodProduto.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.txtCodProduto.HideSelection = false;
            this.txtCodProduto.Location = new System.Drawing.Point(9, 23);
            this.txtCodProduto.MaxLength = 5;
            this.txtCodProduto.Name = "txtCodProduto";
            this.txtCodProduto.Size = new System.Drawing.Size(57, 30);
            this.txtCodProduto.TabIndex = 0;
            this.txtCodProduto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodProduto_KeyDown);
            this.txtCodProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodProduto_KeyPress);
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProduto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblNomeProduto.Location = new System.Drawing.Point(69, 23);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(442, 27);
            this.lblNomeProduto.TabIndex = 3;
            this.lblNomeProduto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(9, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Código";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(517, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "Qtd";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Purple;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(19, 527);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 46);
            this.button1.TabIndex = 4;
            this.button1.Text = "Imprimir - F2";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImprimir.AutoSize = true;
            this.btnImprimir.BackColor = System.Drawing.Color.Purple;
            this.btnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImprimir.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnImprimir.FlatAppearance.BorderSize = 0;
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.ForeColor = System.Drawing.Color.White;
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(17, 527);
            this.btnImprimir.Margin = new System.Windows.Forms.Padding(2);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(161, 46);
            this.btnImprimir.TabIndex = 4;
            this.btnImprimir.Text = "Imprimir - F2";
            this.btnImprimir.UseVisualStyleBackColor = false;
            // 
            // btnPagamento
            // 
            this.btnPagamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPagamento.AutoSize = true;
            this.btnPagamento.BackColor = System.Drawing.Color.Chocolate;
            this.btnPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPagamento.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPagamento.FlatAppearance.BorderSize = 0;
            this.btnPagamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPagamento.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagamento.ForeColor = System.Drawing.Color.White;
            this.btnPagamento.Image = ((System.Drawing.Image)(resources.GetObject("btnPagamento.Image")));
            this.btnPagamento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPagamento.Location = new System.Drawing.Point(191, 527);
            this.btnPagamento.Margin = new System.Windows.Forms.Padding(2);
            this.btnPagamento.Name = "btnPagamento";
            this.btnPagamento.Size = new System.Drawing.Size(181, 46);
            this.btnPagamento.TabIndex = 3;
            this.btnPagamento.Text = "Pagamento - F3";
            this.btnPagamento.UseVisualStyleBackColor = false;
            this.btnPagamento.Click += new System.EventHandler(this.btnPagamento_Click);
            // 
            // btnEnviarPedido
            // 
            this.btnEnviarPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnviarPedido.AutoSize = true;
            this.btnEnviarPedido.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnEnviarPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEnviarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnviarPedido.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEnviarPedido.FlatAppearance.BorderSize = 0;
            this.btnEnviarPedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnviarPedido.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviarPedido.ForeColor = System.Drawing.Color.White;
            this.btnEnviarPedido.Image = ((System.Drawing.Image)(resources.GetObject("btnEnviarPedido.Image")));
            this.btnEnviarPedido.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnviarPedido.Location = new System.Drawing.Point(390, 527);
            this.btnEnviarPedido.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnviarPedido.Name = "btnEnviarPedido";
            this.btnEnviarPedido.Size = new System.Drawing.Size(205, 46);
            this.btnEnviarPedido.TabIndex = 2;
            this.btnEnviarPedido.Text = "Enviar Pedido - F12";
            this.btnEnviarPedido.UseVisualStyleBackColor = false;
            this.btnEnviarPedido.Click += new System.EventHandler(this.btnEnviarPedido_Click);
            // 
            // dgPedidoItems
            // 
            this.dgPedidoItems.AllowUserToAddRows = false;
            this.dgPedidoItems.AllowUserToDeleteRows = false;
            this.dgPedidoItems.AllowUserToResizeColumns = false;
            this.dgPedidoItems.AllowUserToResizeRows = false;
            this.dgPedidoItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPedidoItems.AutoGenerateColumns = false;
            this.dgPedidoItems.BackgroundColor = System.Drawing.Color.White;
            this.dgPedidoItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPedidoItems.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgPedidoItems.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPedidoItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgPedidoItems.ColumnHeadersHeight = 25;
            this.dgPedidoItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPedidoItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigoProdutoDataGridViewTextBoxColumn,
            this.descricaoProdutoDataGridViewTextBoxColumn,
            this.idVendaDetalhesDataGridViewTextBoxColumn,
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn,
            this.especialVendaDetalhesDataGridViewTextBoxColumn,
            this.extraVendaDetalhesDataGridViewTextBoxColumn,
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn,
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn,
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn,
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn,
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn,
            this.statusVendaDetalhesDataGridViewTextBoxColumn,
            this.itemPedidoDataGridViewTextBoxColumn,
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn,
            this.atendenteVendaDataGridViewTextBoxColumn,
            this.tipoAtendimentoDataGridViewTextBoxColumn,
            this.undMedidaIdDataGridViewTextBoxColumn,
            this.idFornecedorDataGridViewTextBoxColumn,
            this.grupoIdDataGridViewTextBoxColumn,
            this.valorCompraProdutoDataGridViewTextBoxColumn,
            this.valorVendaProdutoDataGridViewTextBoxColumn,
            this.estoqueProdutoDataGridViewTextBoxColumn,
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn,
            this.dataCadastroProdutoDataGridViewTextBoxColumn,
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn,
            this.manufaturadoProdutoDataGridViewTextBoxColumn,
            this.usuarioIdDataGridViewTextBoxColumn,
            this.codProdutoManufaturadoDataGridViewTextBoxColumn,
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn,
            this.descricaoUndMedidaDataGridViewTextBoxColumn,
            this.descricaoGrupoDataGridViewTextBoxColumn,
            this.nomeFornecedorDataGridViewTextBoxColumn,
            this.tipoProdutoDataGridViewTextBoxColumn});
            this.dgPedidoItems.DataSource = this.bindItensPedidos;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPedidoItems.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgPedidoItems.EnableHeadersVisualStyles = false;
            this.dgPedidoItems.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgPedidoItems.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgPedidoItems.Location = new System.Drawing.Point(9, 68);
            this.dgPedidoItems.MultiSelect = false;
            this.dgPedidoItems.Name = "dgPedidoItems";
            this.dgPedidoItems.ReadOnly = true;
            this.dgPedidoItems.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPedidoItems.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgPedidoItems.RowHeadersVisible = false;
            this.dgPedidoItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPedidoItems.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgPedidoItems.RowTemplate.DividerHeight = 1;
            this.dgPedidoItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPedidoItems.Size = new System.Drawing.Size(586, 391);
            this.dgPedidoItems.TabIndex = 1;
            this.dgPedidoItems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPedidoItems_KeyDown);
            // 
            // codigoProdutoDataGridViewTextBoxColumn
            // 
            this.codigoProdutoDataGridViewTextBoxColumn.DataPropertyName = "codigoProduto";
            this.codigoProdutoDataGridViewTextBoxColumn.HeaderText = "Codigo";
            this.codigoProdutoDataGridViewTextBoxColumn.Name = "codigoProdutoDataGridViewTextBoxColumn";
            this.codigoProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.codigoProdutoDataGridViewTextBoxColumn.Width = 70;
            // 
            // descricaoProdutoDataGridViewTextBoxColumn
            // 
            this.descricaoProdutoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.descricaoProdutoDataGridViewTextBoxColumn.DataPropertyName = "descricaoProduto";
            this.descricaoProdutoDataGridViewTextBoxColumn.HeaderText = "descricaoProduto";
            this.descricaoProdutoDataGridViewTextBoxColumn.Image = null;
            this.descricaoProdutoDataGridViewTextBoxColumn.Name = "descricaoProdutoDataGridViewTextBoxColumn";
            this.descricaoProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.descricaoProdutoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // idVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.idVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "idVendaDetalhes";
            this.idVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "idVendaDetalhes";
            this.idVendaDetalhesDataGridViewTextBoxColumn.Name = "idVendaDetalhesDataGridViewTextBoxColumn";
            this.idVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.idVendaDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // idVendaCabecalhoDetalhesDataGridViewTextBoxColumn
            // 
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn.DataPropertyName = "idVendaCabecalhoDetalhes";
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn.HeaderText = "idVendaCabecalhoDetalhes";
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn.Name = "idVendaCabecalhoDetalhesDataGridViewTextBoxColumn";
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.idVendaCabecalhoDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // especialVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.especialVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "especialVendaDetalhes";
            this.especialVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "especialVendaDetalhes";
            this.especialVendaDetalhesDataGridViewTextBoxColumn.Name = "especialVendaDetalhesDataGridViewTextBoxColumn";
            this.especialVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.especialVendaDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // extraVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.extraVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "extraVendaDetalhes";
            this.extraVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "extraVendaDetalhes";
            this.extraVendaDetalhesDataGridViewTextBoxColumn.Name = "extraVendaDetalhesDataGridViewTextBoxColumn";
            this.extraVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.extraVendaDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // qtdExtraVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "qtdExtraVendaDetalhes";
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "qtdExtraVendaDetalhes";
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn.Name = "qtdExtraVendaDetalhesDataGridViewTextBoxColumn";
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.qtdExtraVendaDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // quantidadeVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "quantidadeVendaDetalhes";
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "Qtd";
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn.Name = "quantidadeVendaDetalhesDataGridViewTextBoxColumn";
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.quantidadeVendaDetalhesDataGridViewTextBoxColumn.Width = 50;
            // 
            // valorUnitarioVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "valorUnitarioVendaDetalhes";
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "Valor Unit";
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn.Name = "valorUnitarioVendaDetalhesDataGridViewTextBoxColumn";
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.valorUnitarioVendaDetalhesDataGridViewTextBoxColumn.Width = 70;
            // 
            // valorTotalVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "valorTotalVendaDetalhes";
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "Valor T.";
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn.Name = "valorTotalVendaDetalhesDataGridViewTextBoxColumn";
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.valorTotalVendaDetalhesDataGridViewTextBoxColumn.Width = 70;
            // 
            // dataEnvioVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "dataEnvioVendaDetalhes";
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "dataEnvioVendaDetalhes";
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn.Name = "dataEnvioVendaDetalhesDataGridViewTextBoxColumn";
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.dataEnvioVendaDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // statusVendaDetalhesDataGridViewTextBoxColumn
            // 
            this.statusVendaDetalhesDataGridViewTextBoxColumn.DataPropertyName = "statusVendaDetalhes";
            this.statusVendaDetalhesDataGridViewTextBoxColumn.HeaderText = "statusVendaDetalhes";
            this.statusVendaDetalhesDataGridViewTextBoxColumn.Name = "statusVendaDetalhesDataGridViewTextBoxColumn";
            this.statusVendaDetalhesDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusVendaDetalhesDataGridViewTextBoxColumn.Visible = false;
            // 
            // itemPedidoDataGridViewTextBoxColumn
            // 
            this.itemPedidoDataGridViewTextBoxColumn.DataPropertyName = "itemPedido";
            this.itemPedidoDataGridViewTextBoxColumn.HeaderText = "itemPedido";
            this.itemPedidoDataGridViewTextBoxColumn.Name = "itemPedidoDataGridViewTextBoxColumn";
            this.itemPedidoDataGridViewTextBoxColumn.ReadOnly = true;
            this.itemPedidoDataGridViewTextBoxColumn.Visible = false;
            // 
            // atendimentoVendaCabecalhoDataGridViewTextBoxColumn
            // 
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn.DataPropertyName = "atendimentoVendaCabecalho";
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn.HeaderText = "atendimentoVendaCabecalho";
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn.Name = "atendimentoVendaCabecalhoDataGridViewTextBoxColumn";
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn.ReadOnly = true;
            this.atendimentoVendaCabecalhoDataGridViewTextBoxColumn.Visible = false;
            // 
            // atendenteVendaDataGridViewTextBoxColumn
            // 
            this.atendenteVendaDataGridViewTextBoxColumn.DataPropertyName = "atendenteVenda";
            this.atendenteVendaDataGridViewTextBoxColumn.HeaderText = "atendenteVenda";
            this.atendenteVendaDataGridViewTextBoxColumn.Name = "atendenteVendaDataGridViewTextBoxColumn";
            this.atendenteVendaDataGridViewTextBoxColumn.ReadOnly = true;
            this.atendenteVendaDataGridViewTextBoxColumn.Visible = false;
            // 
            // tipoAtendimentoDataGridViewTextBoxColumn
            // 
            this.tipoAtendimentoDataGridViewTextBoxColumn.DataPropertyName = "tipoAtendimento";
            this.tipoAtendimentoDataGridViewTextBoxColumn.HeaderText = "tipoAtendimento";
            this.tipoAtendimentoDataGridViewTextBoxColumn.Name = "tipoAtendimentoDataGridViewTextBoxColumn";
            this.tipoAtendimentoDataGridViewTextBoxColumn.ReadOnly = true;
            this.tipoAtendimentoDataGridViewTextBoxColumn.Visible = false;
            // 
            // undMedidaIdDataGridViewTextBoxColumn
            // 
            this.undMedidaIdDataGridViewTextBoxColumn.DataPropertyName = "undMedidaId";
            this.undMedidaIdDataGridViewTextBoxColumn.HeaderText = "undMedidaId";
            this.undMedidaIdDataGridViewTextBoxColumn.Name = "undMedidaIdDataGridViewTextBoxColumn";
            this.undMedidaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.undMedidaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // idFornecedorDataGridViewTextBoxColumn
            // 
            this.idFornecedorDataGridViewTextBoxColumn.DataPropertyName = "idFornecedor";
            this.idFornecedorDataGridViewTextBoxColumn.HeaderText = "idFornecedor";
            this.idFornecedorDataGridViewTextBoxColumn.Name = "idFornecedorDataGridViewTextBoxColumn";
            this.idFornecedorDataGridViewTextBoxColumn.ReadOnly = true;
            this.idFornecedorDataGridViewTextBoxColumn.Visible = false;
            // 
            // grupoIdDataGridViewTextBoxColumn
            // 
            this.grupoIdDataGridViewTextBoxColumn.DataPropertyName = "grupoId";
            this.grupoIdDataGridViewTextBoxColumn.HeaderText = "grupoId";
            this.grupoIdDataGridViewTextBoxColumn.Name = "grupoIdDataGridViewTextBoxColumn";
            this.grupoIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.grupoIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // valorCompraProdutoDataGridViewTextBoxColumn
            // 
            this.valorCompraProdutoDataGridViewTextBoxColumn.DataPropertyName = "valorCompraProduto";
            this.valorCompraProdutoDataGridViewTextBoxColumn.HeaderText = "valorCompraProduto";
            this.valorCompraProdutoDataGridViewTextBoxColumn.Name = "valorCompraProdutoDataGridViewTextBoxColumn";
            this.valorCompraProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.valorCompraProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // valorVendaProdutoDataGridViewTextBoxColumn
            // 
            this.valorVendaProdutoDataGridViewTextBoxColumn.DataPropertyName = "valorVendaProduto";
            this.valorVendaProdutoDataGridViewTextBoxColumn.HeaderText = "valorVendaProduto";
            this.valorVendaProdutoDataGridViewTextBoxColumn.Name = "valorVendaProdutoDataGridViewTextBoxColumn";
            this.valorVendaProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.valorVendaProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // estoqueProdutoDataGridViewTextBoxColumn
            // 
            this.estoqueProdutoDataGridViewTextBoxColumn.DataPropertyName = "estoqueProduto";
            this.estoqueProdutoDataGridViewTextBoxColumn.HeaderText = "estoqueProduto";
            this.estoqueProdutoDataGridViewTextBoxColumn.Name = "estoqueProdutoDataGridViewTextBoxColumn";
            this.estoqueProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.estoqueProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // estoqueCriticoProdutoDataGridViewTextBoxColumn
            // 
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn.DataPropertyName = "estoqueCriticoProduto";
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn.HeaderText = "estoqueCriticoProduto";
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn.Name = "estoqueCriticoProdutoDataGridViewTextBoxColumn";
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.estoqueCriticoProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataCadastroProdutoDataGridViewTextBoxColumn
            // 
            this.dataCadastroProdutoDataGridViewTextBoxColumn.DataPropertyName = "dataCadastroProduto";
            this.dataCadastroProdutoDataGridViewTextBoxColumn.HeaderText = "dataCadastroProduto";
            this.dataCadastroProdutoDataGridViewTextBoxColumn.Name = "dataCadastroProdutoDataGridViewTextBoxColumn";
            this.dataCadastroProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.dataCadastroProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // controlaEstoqueProdutoDataGridViewTextBoxColumn
            // 
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn.DataPropertyName = "controlaEstoqueProduto";
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn.HeaderText = "controlaEstoqueProduto";
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn.Name = "controlaEstoqueProdutoDataGridViewTextBoxColumn";
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.controlaEstoqueProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // manufaturadoProdutoDataGridViewTextBoxColumn
            // 
            this.manufaturadoProdutoDataGridViewTextBoxColumn.DataPropertyName = "manufaturadoProduto";
            this.manufaturadoProdutoDataGridViewTextBoxColumn.HeaderText = "manufaturadoProduto";
            this.manufaturadoProdutoDataGridViewTextBoxColumn.Name = "manufaturadoProdutoDataGridViewTextBoxColumn";
            this.manufaturadoProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.manufaturadoProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // usuarioIdDataGridViewTextBoxColumn
            // 
            this.usuarioIdDataGridViewTextBoxColumn.DataPropertyName = "usuarioId";
            this.usuarioIdDataGridViewTextBoxColumn.HeaderText = "usuarioId";
            this.usuarioIdDataGridViewTextBoxColumn.Name = "usuarioIdDataGridViewTextBoxColumn";
            this.usuarioIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.usuarioIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // codProdutoManufaturadoDataGridViewTextBoxColumn
            // 
            this.codProdutoManufaturadoDataGridViewTextBoxColumn.DataPropertyName = "codProdutoManufaturado";
            this.codProdutoManufaturadoDataGridViewTextBoxColumn.HeaderText = "codProdutoManufaturado";
            this.codProdutoManufaturadoDataGridViewTextBoxColumn.Name = "codProdutoManufaturadoDataGridViewTextBoxColumn";
            this.codProdutoManufaturadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.codProdutoManufaturadoDataGridViewTextBoxColumn.Visible = false;
            // 
            // quantidadeProdutoManufaturadoDataGridViewTextBoxColumn
            // 
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn.DataPropertyName = "quantidadeProdutoManufaturado";
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn.HeaderText = "quantidadeProdutoManufaturado";
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn.Name = "quantidadeProdutoManufaturadoDataGridViewTextBoxColumn";
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.quantidadeProdutoManufaturadoDataGridViewTextBoxColumn.Visible = false;
            // 
            // descricaoUndMedidaDataGridViewTextBoxColumn
            // 
            this.descricaoUndMedidaDataGridViewTextBoxColumn.DataPropertyName = "descricaoUndMedida";
            this.descricaoUndMedidaDataGridViewTextBoxColumn.HeaderText = "descricaoUndMedida";
            this.descricaoUndMedidaDataGridViewTextBoxColumn.Name = "descricaoUndMedidaDataGridViewTextBoxColumn";
            this.descricaoUndMedidaDataGridViewTextBoxColumn.ReadOnly = true;
            this.descricaoUndMedidaDataGridViewTextBoxColumn.Visible = false;
            // 
            // descricaoGrupoDataGridViewTextBoxColumn
            // 
            this.descricaoGrupoDataGridViewTextBoxColumn.DataPropertyName = "descricaoGrupo";
            this.descricaoGrupoDataGridViewTextBoxColumn.HeaderText = "descricaoGrupo";
            this.descricaoGrupoDataGridViewTextBoxColumn.Name = "descricaoGrupoDataGridViewTextBoxColumn";
            this.descricaoGrupoDataGridViewTextBoxColumn.ReadOnly = true;
            this.descricaoGrupoDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomeFornecedorDataGridViewTextBoxColumn
            // 
            this.nomeFornecedorDataGridViewTextBoxColumn.DataPropertyName = "nomeFornecedor";
            this.nomeFornecedorDataGridViewTextBoxColumn.HeaderText = "nomeFornecedor";
            this.nomeFornecedorDataGridViewTextBoxColumn.Name = "nomeFornecedorDataGridViewTextBoxColumn";
            this.nomeFornecedorDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomeFornecedorDataGridViewTextBoxColumn.Visible = false;
            // 
            // tipoProdutoDataGridViewTextBoxColumn
            // 
            this.tipoProdutoDataGridViewTextBoxColumn.DataPropertyName = "tipoProduto";
            this.tipoProdutoDataGridViewTextBoxColumn.HeaderText = "tipoProduto";
            this.tipoProdutoDataGridViewTextBoxColumn.Name = "tipoProdutoDataGridViewTextBoxColumn";
            this.tipoProdutoDataGridViewTextBoxColumn.ReadOnly = true;
            this.tipoProdutoDataGridViewTextBoxColumn.Visible = false;
            // 
            // bindItensPedidos
            // 
            this.bindItensPedidos.DataSource = typeof(ObjetoTransferencia.VendaDetalhes);
            // 
            // lblPessoa
            // 
            this.lblPessoa.AutoSize = true;
            this.lblPessoa.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPessoa.ForeColor = System.Drawing.Color.Black;
            this.lblPessoa.Location = new System.Drawing.Point(9, 468);
            this.lblPessoa.Name = "lblPessoa";
            this.lblPessoa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblPessoa.Size = new System.Drawing.Size(135, 18);
            this.lblPessoa.TabIndex = 3;
            this.lblPessoa.Text = "Por Pessoa: 0,00";
            this.lblPessoa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalItems
            // 
            this.lblTotalItems.AutoSize = true;
            this.lblTotalItems.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalItems.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTotalItems.Location = new System.Drawing.Point(9, 495);
            this.lblTotalItems.Name = "lblTotalItems";
            this.lblTotalItems.Size = new System.Drawing.Size(108, 18);
            this.lblTotalItems.TabIndex = 3;
            this.lblTotalItems.Text = "Total Itens: 0";
            this.lblTotalItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.ForeColor = System.Drawing.Color.Black;
            this.lblSubTotal.Location = new System.Drawing.Point(442, 468);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(122, 18);
            this.lblSubTotal.TabIndex = 3;
            this.lblSubTotal.Text = "Sub Total: 0,00";
            this.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // productCodeDataGridViewTextBoxColumn
            // 
            this.productCodeDataGridViewTextBoxColumn.DataPropertyName = "ProductCode";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.productCodeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this.productCodeDataGridViewTextBoxColumn.HeaderText = "Codigo";
            this.productCodeDataGridViewTextBoxColumn.Name = "productCodeDataGridViewTextBoxColumn";
            this.productCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.productCodeDataGridViewTextBoxColumn.Width = 50;
            // 
            // productMetricUnitDataGridViewTextBoxColumn
            // 
            this.productMetricUnitDataGridViewTextBoxColumn.DataPropertyName = "ProductMetricUnit";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.productMetricUnitDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this.productMetricUnitDataGridViewTextBoxColumn.HeaderText = "Und";
            this.productMetricUnitDataGridViewTextBoxColumn.Name = "productMetricUnitDataGridViewTextBoxColumn";
            this.productMetricUnitDataGridViewTextBoxColumn.ReadOnly = true;
            this.productMetricUnitDataGridViewTextBoxColumn.Width = 50;
            // 
            // qtdDataGridViewTextBoxColumn
            // 
            this.qtdDataGridViewTextBoxColumn.DataPropertyName = "Qtd";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.Format = "N0";
            dataGridViewCellStyle19.NullValue = null;
            this.qtdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this.qtdDataGridViewTextBoxColumn.HeaderText = "Qtd";
            this.qtdDataGridViewTextBoxColumn.Name = "qtdDataGridViewTextBoxColumn";
            this.qtdDataGridViewTextBoxColumn.ReadOnly = true;
            this.qtdDataGridViewTextBoxColumn.Width = 60;
            // 
            // productPrecoVendaDataGridViewTextBoxColumn
            // 
            this.productPrecoVendaDataGridViewTextBoxColumn.DataPropertyName = "ProductPrecoVenda";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = null;
            this.productPrecoVendaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle20;
            this.productPrecoVendaDataGridViewTextBoxColumn.HeaderText = "Valor Unit";
            this.productPrecoVendaDataGridViewTextBoxColumn.Name = "productPrecoVendaDataGridViewTextBoxColumn";
            this.productPrecoVendaDataGridViewTextBoxColumn.ReadOnly = true;
            this.productPrecoVendaDataGridViewTextBoxColumn.Width = 70;
            // 
            // subTotalDataGridViewTextBoxColumn
            // 
            this.subTotalDataGridViewTextBoxColumn.DataPropertyName = "SubTotal";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = null;
            this.subTotalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle21;
            this.subTotalDataGridViewTextBoxColumn.HeaderText = "Sub Total";
            this.subTotalDataGridViewTextBoxColumn.Name = "subTotalDataGridViewTextBoxColumn";
            this.subTotalDataGridViewTextBoxColumn.ReadOnly = true;
            this.subTotalDataGridViewTextBoxColumn.Width = 80;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // requestIdDataGridViewTextBoxColumn
            // 
            this.requestIdDataGridViewTextBoxColumn.DataPropertyName = "RequestId";
            this.requestIdDataGridViewTextBoxColumn.HeaderText = "RequestId";
            this.requestIdDataGridViewTextBoxColumn.Name = "requestIdDataGridViewTextBoxColumn";
            this.requestIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.requestIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // complementoDataGridViewTextBoxColumn
            // 
            this.complementoDataGridViewTextBoxColumn.DataPropertyName = "Complemento";
            this.complementoDataGridViewTextBoxColumn.HeaderText = "Complemento";
            this.complementoDataGridViewTextBoxColumn.Name = "complementoDataGridViewTextBoxColumn";
            this.complementoDataGridViewTextBoxColumn.ReadOnly = true;
            this.complementoDataGridViewTextBoxColumn.Visible = false;
            // 
            // observacaoDataGridViewTextBoxColumn
            // 
            this.observacaoDataGridViewTextBoxColumn.DataPropertyName = "Observacao";
            this.observacaoDataGridViewTextBoxColumn.HeaderText = "Observacao";
            this.observacaoDataGridViewTextBoxColumn.Name = "observacaoDataGridViewTextBoxColumn";
            this.observacaoDataGridViewTextBoxColumn.ReadOnly = true;
            this.observacaoDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataAberturaDataGridViewTextBoxColumn
            // 
            this.dataAberturaDataGridViewTextBoxColumn.DataPropertyName = "DataAbertura";
            this.dataAberturaDataGridViewTextBoxColumn.HeaderText = "DataAbertura";
            this.dataAberturaDataGridViewTextBoxColumn.Name = "dataAberturaDataGridViewTextBoxColumn";
            this.dataAberturaDataGridViewTextBoxColumn.ReadOnly = true;
            this.dataAberturaDataGridViewTextBoxColumn.Visible = false;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "addComplent.png");
            this.imageList1.Images.SetKeyName(1, "addComplent.png");
            this.imageList1.Images.SetKeyName(2, "addComplent.png");
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ProductNome";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn1.HeaderText = "Produto";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ProductMetricUnit";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn2.HeaderText = "Und";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Qtd";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.Format = "N0";
            dataGridViewCellStyle24.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn3.HeaderText = "Qtd";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ProductPrecoVenda";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.Format = "N2";
            dataGridViewCellStyle25.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn4.HeaderText = "Valor Unit";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 70;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "SubTotal";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.Format = "N2";
            dataGridViewCellStyle26.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn5.HeaderText = "Sub Total";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 80;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn6.HeaderText = "Id";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "RequestId";
            this.dataGridViewTextBoxColumn7.HeaderText = "RequestId";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Complemento";
            this.dataGridViewTextBoxColumn8.HeaderText = "Complemento";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Observacao";
            this.dataGridViewTextBoxColumn9.HeaderText = "Observacao";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "DataAbertura";
            this.dataGridViewTextBoxColumn10.HeaderText = "DataAbertura";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Status";
            this.dataGridViewTextBoxColumn11.HeaderText = "Status";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // FrmPedidoAberto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(862, 614);
            this.Controls.Add(this.pCorpo);
            this.Controls.Add(this.pInformações);
            this.Controls.Add(this.pTitulo);
            this.KeyPreview = true;
            this.Name = "FrmPedidoAberto";
            this.Text = "Food Fácil - Pedido Aberto";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPedidoAberto_FormClosed);
            this.Load += new System.EventHandler(this.FrmPedidoAberto_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPedidoAberto_KeyDown);
            this.pTitulo.ResumeLayout(false);
            this.pTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCloser)).EndInit();
            this.pInformações.ResumeLayout(false);
            this.pInformações.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudNumClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindAtendente)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.pCorpo.ResumeLayout(false);
            this.pCorpo.PerformLayout();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQtdProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidoItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindItensPedidos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pTitulo;
        public System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.PictureBox pbCloser;
        private System.Windows.Forms.Panel pInformações;
        private System.Windows.Forms.Panel pCorpo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNumPedido;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cbAtendente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTempo;
        private MetroFramework.Controls.MetroGrid dgPedidoItems;
        private System.Windows.Forms.NumericUpDown nudNumClientes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnEnviarPedido;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnPagamento;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.BindingSource bindAtendente;
        private System.Windows.Forms.Label lblPessoa;
        private System.Windows.Forms.Label lblSubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn productCodigoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label lblTotalItems;
        private System.Windows.Forms.ImageList imageList1;
        private TextAndImageColumn textAndImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn productCodeDataGridViewTextBoxColumn;
        private TextAndImageColumn productNomeTextAndImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productMetricUnitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productPrecoVendaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subTotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn requestIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn complementoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn observacaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataAberturaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnComplentos;
        private System.Windows.Forms.Panel panelTop;
        public System.Windows.Forms.TextBox txtCodProduto;
        private System.Windows.Forms.Label lblNomeProduto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productMetricUnitDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource bindItensPedidos;
        private System.Windows.Forms.Button button1;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.NumericUpDown txtQtdProduto;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn requestIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productCodeDataGridViewTextBoxColumn1;
        private TextAndImageColumn productNomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productUnidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productPrecoVendaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn subTotalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn complementoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn observacaoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataAberturaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn metricUnitsIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productPrecoCustoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeProductDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productCategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblNumAtendimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoProdutoDataGridViewTextBoxColumn;
        private TextAndImageColumn descricaoProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idVendaCabecalhoDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn especialVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn extraVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdExtraVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidadeVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorUnitarioVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorTotalVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataEnvioVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusVendaDetalhesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemPedidoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn atendimentoVendaCabecalhoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn atendenteVendaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoAtendimentoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn undMedidaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idFornecedorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn grupoIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorCompraProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorVendaProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estoqueProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estoqueCriticoProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataCadastroProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn controlaEstoqueProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn manufaturadoProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuarioIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codProdutoManufaturadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidadeProdutoManufaturadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoUndMedidaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoGrupoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeFornecedorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoProdutoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnEcluirItem;
        private System.Windows.Forms.Button btnEspeciais;
    }
}