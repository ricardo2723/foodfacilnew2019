﻿using System;
using System.Windows.Forms;
using Negocios;
using UI.GenericFunctions;
using UI.Pedido;
using UI.StylesForms;

namespace UI.Pedidos
{
    public partial class FrmPedido : Form
    {
        ViewRequestsGenerate vrg = new ViewRequestsGenerate();

        public FrmPedido()
        {
            InitializeComponent();
            ConfigSkin.ThemeForm(this, lbTitulo, "Pedidos Abertos");
        }

        private void FrmPedido_Load(object sender, EventArgs e)
        {
            CarregarPedidosAbertos();
        }

        private void pbCloser_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmPedido_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    AbrirPedido();
                    break;
                case Keys.Escape:
                    Close();
                    break;
                case Keys.F5:
                    CarregarPedidosAbertos();
                    break;
                default:
                    break;
            }
        }

        private void pTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            MoveForm.MouseMoveForm(this);
        }

        public void CarregarPedidosAbertos()
        {
            flpPedidoMesa.Controls.Clear();
            vrg.ControlGenerate(flpPedidoMesa, pPedidoCorpo1_Click);
        }

        private void AbrirPedido()
        {
            if (Forms.OpenFormResult(new FrmPedidoAberto(AcaoGrud.NoExistPedido, null)) == DialogResult.Yes)
            {
                CarregarPedidosAbertos();
            }
        }

        private void pPedidoCorpo1_Click(object sender, EventArgs e)
        {
            Panel p = sender as Panel;
            VendaCabecalhoNegocios pedidos = new VendaCabecalhoNegocios();

            foreach (Control ctrl in p.Controls)
            {
                if (ctrl is Label)
                {
                    for (int i = 1; i <= flpPedidoMesa.Controls.Count; i++)
                    {
                        if (ctrl.Name == "lblNumPedido" + i)
                        {
                            FrmPedidoAberto frmP = new FrmPedidoAberto(AcaoGrud.ExistPedido, pedidos.BuscarPedidosAbertosPorId(int.Parse(ctrl.Text)));
                            DialogResult resultado = frmP.ShowDialog();

                            if (resultado == DialogResult.Yes)
                            {
                                CarregarPedidosAbertos();
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void lblPedido_Click(object sender, EventArgs e)
        {
            AbrirPedido();
        }
    }
}
