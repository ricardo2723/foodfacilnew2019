namespace UI
{
    partial class frmSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSplash));
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbSplash = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.lblPorcentagem = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.White;
            this.labelX4.Location = new System.Drawing.Point(194, 296);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(281, 23);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "<font color=\"#FFFFFF\">Copyright - DR Tecnologia Solu��es em Inform�tica</font>";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::UI.Properties.Resources.LogoDrSis1;
            this.pictureBox1.Location = new System.Drawing.Point(52, 95);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(197, 161);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // pbSplash
            // 
            // 
            // 
            // 
            this.pbSplash.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(198)))));
            this.pbSplash.BackgroundStyle.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbSplash.BackgroundStyle.BorderColorLight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.pbSplash.BackgroundStyle.BorderColorLight2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.pbSplash.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.pbSplash.BackgroundStyle.Description = "Teste";
            this.pbSplash.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.pbSplash.BackgroundStyle.TextTrimming = DevComponents.DotNetBar.eStyleTextTrimming.Word;
            this.pbSplash.Location = new System.Drawing.Point(279, 166);
            this.pbSplash.Maximum = 30;
            this.pbSplash.Name = "pbSplash";
            this.pbSplash.Size = new System.Drawing.Size(305, 23);
            this.pbSplash.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pbSplash.TabIndex = 8;
            this.pbSplash.Text = "progressBarX1";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Candara", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(279, 137);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(281, 23);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX1.TabIndex = 9;
            this.labelX1.Text = "<font color=\"#FFFFFF\">Iniciando o Sistema ...</font>";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblPorcentagem
            // 
            this.lblPorcentagem.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblPorcentagem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPorcentagem.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorcentagem.ForeColor = System.Drawing.Color.White;
            this.lblPorcentagem.Location = new System.Drawing.Point(450, 195);
            this.lblPorcentagem.Name = "lblPorcentagem";
            this.lblPorcentagem.SingleLineColor = System.Drawing.Color.White;
            this.lblPorcentagem.Size = new System.Drawing.Size(38, 23);
            this.lblPorcentagem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.lblPorcentagem.TabIndex = 10;
            this.lblPorcentagem.Text = "<font color=\"#FFFFFF\"></font>";
            this.lblPorcentagem.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Candara", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(365, 195);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(84, 23);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX2.TabIndex = 11;
            this.labelX2.Text = "<font color=\"#FFFFFF\">Processando </font>";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // frmSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(657, 356);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.lblPorcentagem);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.pbSplash);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelX4);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSplash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil -Splash";
            this.Load += new System.EventHandler(this.frmSplash_Load);
            this.Shown += new System.EventHandler(this.frmSplash_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.Controls.ProgressBarX pbSplash;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX lblPorcentagem;
        private DevComponents.DotNetBar.LabelX labelX2;
    }
}