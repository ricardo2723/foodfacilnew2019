﻿namespace UI
{
    partial class frmCadEntradaProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadEntradaProdutos));
            this.panelProduto = new DevComponents.DotNetBar.PanelEx();
            this.btnPesquisaProduto = new DevComponents.DotNetBar.ButtonX();
            this.txtQuantidade = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.txtNf = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCodigo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtMotivoEntrada = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txtValorVenda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtValorCusto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.panelProduto.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelProduto
            // 
            this.panelProduto.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelProduto.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelProduto.Controls.Add(this.btnPesquisaProduto);
            this.panelProduto.Controls.Add(this.txtQuantidade);
            this.panelProduto.Controls.Add(this.labelX15);
            this.panelProduto.Controls.Add(this.txtNf);
            this.panelProduto.Controls.Add(this.txtCodigo);
            this.panelProduto.Controls.Add(this.labelX14);
            this.panelProduto.Controls.Add(this.labelX8);
            this.panelProduto.Controls.Add(this.txtMotivoEntrada);
            this.panelProduto.Controls.Add(this.labelX9);
            this.panelProduto.Controls.Add(this.txtValorVenda);
            this.panelProduto.Controls.Add(this.txtProduto);
            this.panelProduto.Controls.Add(this.labelX7);
            this.panelProduto.Controls.Add(this.labelX1);
            this.panelProduto.Controls.Add(this.txtValorCusto);
            this.panelProduto.Controls.Add(this.labelX5);
            this.panelProduto.Location = new System.Drawing.Point(3, 3);
            this.panelProduto.Name = "panelProduto";
            this.panelProduto.Size = new System.Drawing.Size(384, 188);
            this.panelProduto.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelProduto.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelProduto.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelProduto.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelProduto.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelProduto.Style.GradientAngle = 90;
            this.panelProduto.TabIndex = 0;
            // 
            // btnPesquisaProduto
            // 
            this.btnPesquisaProduto.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPesquisaProduto.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisaProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.balloonTip1.SetBalloonCaption(this.btnPesquisaProduto, "Pesquisar (F5)");
            this.balloonTip1.SetBalloonText(this.btnPesquisaProduto, "Pesquisar Codigo do Produto");
            this.btnPesquisaProduto.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnPesquisaProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisaProduto.HoverImage = global::UI.Properties.Resources._1394159835_search;
            this.btnPesquisaProduto.Image = global::UI.Properties.Resources._1394159835_search;
            this.btnPesquisaProduto.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnPesquisaProduto.Location = new System.Drawing.Point(91, 23);
            this.btnPesquisaProduto.Name = "btnPesquisaProduto";
            this.btnPesquisaProduto.Size = new System.Drawing.Size(25, 25);
            this.btnPesquisaProduto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPesquisaProduto.TabIndex = 1;
            this.btnPesquisaProduto.Click += new System.EventHandler(this.btnPesquisaProduto_Click);
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtQuantidade.Border.Class = "TextBoxBorder";
            this.txtQuantidade.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtQuantidade.ForeColor = System.Drawing.Color.Black;
            this.txtQuantidade.Location = new System.Drawing.Point(290, 73);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.PreventEnterBeep = true;
            this.txtQuantidade.Size = new System.Drawing.Size(81, 20);
            this.txtQuantidade.TabIndex = 6;
            this.txtQuantidade.Text = "0";
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Location = new System.Drawing.Point(290, 53);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(62, 23);
            this.labelX15.TabIndex = 98;
            this.labelX15.Text = "Quantidade";
            // 
            // txtNf
            // 
            this.txtNf.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtNf.Border.Class = "TextBoxBorder";
            this.txtNf.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNf.ForeColor = System.Drawing.Color.Black;
            this.txtNf.Location = new System.Drawing.Point(9, 73);
            this.txtNf.MaxLength = 30;
            this.txtNf.Name = "txtNf";
            this.txtNf.PreventEnterBeep = true;
            this.txtNf.Size = new System.Drawing.Size(87, 20);
            this.txtNf.TabIndex = 3;
            this.txtNf.Text = "0";
            this.txtNf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            this.balloonTip1.SetBalloonCaption(this.txtCodigo, null);
            this.balloonTip1.SetBalloonText(this.txtCodigo, "Após digitar o Código pressione Enter");
            // 
            // 
            // 
            this.txtCodigo.Border.Class = "TextBoxBorder";
            this.txtCodigo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigo.ForeColor = System.Drawing.Color.Black;
            this.txtCodigo.Location = new System.Drawing.Point(9, 25);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PreventEnterBeep = true;
            this.txtCodigo.Size = new System.Drawing.Size(76, 20);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            this.txtCodigo.Leave += new System.EventHandler(this.txtCodigo_Leave);
            // 
            // labelX14
            // 
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Location = new System.Drawing.Point(9, 53);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(80, 23);
            this.labelX14.TabIndex = 97;
            this.labelX14.Text = "Doc Entrada";
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(9, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(76, 23);
            this.labelX8.TabIndex = 80;
            this.labelX8.Text = "Código";
            // 
            // txtMotivoEntrada
            // 
            this.txtMotivoEntrada.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMotivoEntrada.Border.Class = "TextBoxBorder";
            this.txtMotivoEntrada.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMotivoEntrada.ForeColor = System.Drawing.Color.Black;
            this.txtMotivoEntrada.Location = new System.Drawing.Point(9, 119);
            this.txtMotivoEntrada.MaxLength = 100;
            this.txtMotivoEntrada.Multiline = true;
            this.txtMotivoEntrada.Name = "txtMotivoEntrada";
            this.txtMotivoEntrada.PreventEnterBeep = true;
            this.txtMotivoEntrada.Size = new System.Drawing.Size(362, 52);
            this.txtMotivoEntrada.TabIndex = 7;
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(9, 99);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(117, 23);
            this.labelX9.TabIndex = 96;
            this.labelX9.Text = "Motivo Entrada";
            // 
            // txtValorVenda
            // 
            this.txtValorVenda.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtValorVenda.Border.Class = "TextBoxBorder";
            this.txtValorVenda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorVenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorVenda.ForeColor = System.Drawing.Color.Blue;
            this.txtValorVenda.Location = new System.Drawing.Point(196, 73);
            this.txtValorVenda.Name = "txtValorVenda";
            this.txtValorVenda.PreventEnterBeep = true;
            this.txtValorVenda.Size = new System.Drawing.Size(88, 20);
            this.txtValorVenda.TabIndex = 5;
            this.txtValorVenda.Text = "0,00";
            this.txtValorVenda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorVenda.Leave += new System.EventHandler(this.txtValorVenda_Leave);
            // 
            // txtProduto
            // 
            this.txtProduto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtProduto.Border.Class = "TextBoxBorder";
            this.txtProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProduto.ForeColor = System.Drawing.Color.Black;
            this.txtProduto.Location = new System.Drawing.Point(122, 25);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.PreventEnterBeep = true;
            this.txtProduto.ReadOnly = true;
            this.txtProduto.Size = new System.Drawing.Size(249, 20);
            this.txtProduto.TabIndex = 2;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(196, 53);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(62, 23);
            this.labelX7.TabIndex = 94;
            this.labelX7.Text = "Valor Venda";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(122, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(135, 23);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "Descrição do Produto";
            // 
            // txtValorCusto
            // 
            this.txtValorCusto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtValorCusto.Border.Class = "TextBoxBorder";
            this.txtValorCusto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorCusto.ForeColor = System.Drawing.Color.Red;
            this.txtValorCusto.Location = new System.Drawing.Point(102, 73);
            this.txtValorCusto.Name = "txtValorCusto";
            this.txtValorCusto.PreventEnterBeep = true;
            this.txtValorCusto.Size = new System.Drawing.Size(88, 20);
            this.txtValorCusto.TabIndex = 4;
            this.txtValorCusto.Text = "0,00";
            this.txtValorCusto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorCusto.Leave += new System.EventHandler(this.txtValorCusto_Leave);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(102, 53);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(83, 23);
            this.labelX5.TabIndex = 92;
            this.labelX5.Text = "Valor Custo";
            // 
            // balloonTip1
            // 
            this.balloonTip1.Style = DevComponents.DotNetBar.eBallonStyle.Office2007Alert;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnFechar.Location = new System.Drawing.Point(273, 195);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(101, 34);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>  Fechar (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.salva;
            this.btnSalvar.Location = new System.Drawing.Point(12, 197);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(101, 34);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 1;
            this.btnSalvar.Text = "  Salvar (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.bntSalvar_Click);
            // 
            // frmCadEntradaProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(391, 235);
            this.Controls.Add(this.panelProduto);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadEntradaProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Entrada de Produtos";
            this.Load += new System.EventHandler(this.frmCadEntradaProdutos_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadEntradaProdutos_KeyUp);
            this.panelProduto.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private DevComponents.DotNetBar.PanelEx panelProduto;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigo;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProduto;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.BalloonTip balloonTip1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtQuantidade;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMotivoEntrada;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorVenda;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorCusto;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.ButtonX btnPesquisaProduto;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNf;
        private DevComponents.DotNetBar.LabelX labelX14;
    }
}