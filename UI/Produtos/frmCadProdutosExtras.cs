﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadProdutosExtras : Form
    {
        AcaoCRUD acaoSelecionada;
        public char controlaProduto = 'S';
        public char manufaturaProduto = 'N';
        public decimal custoProdutoManufatura;
        public decimal vendaProdutoManufatura;

        Produtos produtoManufatura = new Produtos();

        public frmCadProdutosExtras()
        {
            InitializeComponent();
        }

        public frmCadProdutosExtras(AcaoCRUD acao, Produtos produtos)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            //Carrega Combo Unidade de Medida
            UnidadeMedidaNegocios unidadeMedidaNegocios = new UnidadeMedidaNegocios();
            UnidadeMedidaCollections unidadeMedidaCollections = unidadeMedidaNegocios.ConsultarDescricao("%");
            cbUnd.DataSource = null;
            cbUnd.DataSource = unidadeMedidaCollections;

            //Carrega Combo Grupos
            GruposNegocios gruposNegocios = new GruposNegocios();
            GruposCollections gruposCollections = gruposNegocios.ConsultarDescricao("%");

            //Carrega Combo Grupos
            FornecedoresNegocios fornecedoresNegocios = new FornecedoresNegocios();
            FornecedoresCollections fornecedoresCollections = fornecedoresNegocios.ConsultarPorNome("%");
            cbFornecedor.DataSource = null;
            cbFornecedor.DataSource = fornecedoresCollections;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Complementos de Produtos";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                txtCodigo.ReadOnly = true;

                this.Text = "DRSis - Food Facil - Alterar Complementos de Produtos";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                if (produtos.controlaEstoqueProduto == 'S')
                {
                    rbEstoqueSim.Checked = true;
                }
                else if (produtos.controlaEstoqueProduto == 'N')
                {
                    rbEstoqueNao.Checked = true;
                }

                txtLucroPorcent.Enabled = true;
                txtLucroReal.Enabled = true;

                txtCodigo.Text = produtos.codigoProduto.ToString();
                cbUnd.SelectedValue = produtos.undMedidaId;
                cbFornecedor.SelectedValue = produtos.idFornecedor;
                txtProduto.Text = produtos.descricaoProduto;
                txtValorCusto.Text = produtos.valorCompraProduto.ToString();
                txtValorVenda.Text = produtos.valorVendaProduto.ToString();
                txtQuantidade.Text = produtos.estoqueProduto.ToString();
                txtEstoqueCritico.Text = produtos.estoqueCriticoProduto.ToString();
                controlaProduto = produtos.controlaEstoqueProduto;
            }
        }

        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirProduto();
            }
            else if (acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarProduto();
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadProdutos_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void rbEstoqueSim_Click(object sender, EventArgs e)
        {
            controlaProduto = 'S';
        }

        private void rbEstoqueNao_Click(object sender, EventArgs e)
        {
            controlaProduto = 'N';
        }

        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(txtCodigo.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        private void txtValorCusto_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal cust = Convert.ToDecimal(txtValorCusto.Text);
                txtValorCusto.Text = cust.ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Custo Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorCusto.Text = "";
                txtValorCusto.Focus();
            }
        }

        private void txtValorVenda_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDecimal(txtValorVenda.Text);
                decimal vend = Convert.ToDecimal(txtValorVenda.Text);
                txtValorVenda.Text = vend.ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Venda Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorVenda.Text = "";
                txtValorVenda.Focus();
            }
        }

        private void txtEstoqueCritico_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDecimal(txtEstoqueCritico.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Estoque Crítico Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtEstoqueCritico.Text = "";
                txtEstoqueCritico.Focus();
            }
        }

        private void txtLucroPorcent_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtLucroPorcent.Text == "")
                    return;

                txtValorVenda.Text = (Convert.ToDecimal(txtValorCusto.Text) * (Convert.ToDecimal(txtLucroPorcent.Text) / 100) + Convert.ToDecimal(txtValorCusto.Text)).ToString("N");

            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Lucro Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorCusto.Text = "";
                txtValorCusto.Focus();
            }

        }

        private void txtLucroReal_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtLucroReal.Text == "")
                    return;

                txtValorVenda.Text = (Convert.ToDecimal(txtValorCusto.Text) + Convert.ToDecimal(txtLucroReal.Text)).ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Lucro Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorVenda.Text = "";
                txtValorVenda.Focus();
            }
        }

        #endregion

        #region METODOS

        private void inserirProduto()
        {
            if
               (!String.IsNullOrEmpty(txtCodigo.Text) &&
               !String.IsNullOrEmpty(txtProduto.Text) &&
               !String.IsNullOrEmpty(cbUnd.Text) &&
               !String.IsNullOrEmpty(txtValorCusto.Text) &&
               !String.IsNullOrEmpty(txtValorVenda.Text) &&
               !String.IsNullOrEmpty(txtEstoqueCritico.Text))
            {
                Produtos produtos = new Produtos();

                produtos.codigoProduto = Generic.checaInteiro(txtCodigo.Text);
                produtos.undMedidaId = Convert.ToInt32(cbUnd.SelectedValue);
                produtos.idFornecedor = Convert.ToInt32(cbFornecedor.SelectedValue);
                produtos.grupoId = 5;
                produtos.descricaoProduto = txtProduto.Text;
                produtos.valorCompraProduto = Convert.ToDecimal(txtValorCusto.Text);
                produtos.valorVendaProduto = Convert.ToDecimal(txtValorVenda.Text);
                produtos.estoqueProduto = Convert.ToDecimal(txtQuantidade.Text);
                produtos.estoqueCriticoProduto = Convert.ToDecimal(txtEstoqueCritico.Text);
                produtos.manufaturadoProduto = manufaturaProduto;
                produtos.controlaEstoqueProduto = controlaProduto;
                produtos.dataCadastroProduto = DateTime.Now;
                produtos.usuarioId = frmLogin.usuariosLogin.usuarioId;
                produtos.tipoProduto = 'E';

                ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                string retorno = produtosNegocios.inserir(produtos);

                try
                {
                    Convert.ToInt32(retorno);

                    MessageBox.Show("Complemento Cadastrado com Sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(retorno, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

                else if (String.IsNullOrEmpty(txtProduto.Text))
                {
                    MessageBox.Show("Campo Descrição do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProduto.Focus();
                }

                else if (String.IsNullOrEmpty(cbUnd.Text))
                {
                    MessageBox.Show("Campo Und é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbUnd.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorCusto.Text))
                {
                    MessageBox.Show("Campo Valor Custo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorCusto.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorVenda.Text))
                {
                    MessageBox.Show("Campo Valor Venda é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorVenda.Focus();
                }

                else if (String.IsNullOrEmpty(txtEstoqueCritico.Text))
                {
                    MessageBox.Show("Campo Estoque Crítico é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEstoqueCritico.Focus();
                }
            }
        }

        private void atualizarProduto()
        {
            if
               (!String.IsNullOrEmpty(txtCodigo.Text) &&
               !String.IsNullOrEmpty(txtProduto.Text) &&
               !String.IsNullOrEmpty(cbUnd.Text) &&
               !String.IsNullOrEmpty(txtValorCusto.Text) &&
               !String.IsNullOrEmpty(txtValorVenda.Text) &&
               !String.IsNullOrEmpty(txtEstoqueCritico.Text))
            {
                Produtos produtos = new Produtos();

                produtos.codigoProduto = Generic.checaInteiro(txtCodigo.Text);
                produtos.undMedidaId = Convert.ToInt32(cbUnd.SelectedValue);
                produtos.idFornecedor = Convert.ToInt32(cbFornecedor.SelectedValue);
                produtos.grupoId = 5;
                produtos.descricaoProduto = txtProduto.Text;
                produtos.valorCompraProduto = Convert.ToDecimal(txtValorCusto.Text);
                produtos.valorVendaProduto = Convert.ToDecimal(txtValorVenda.Text);
                produtos.estoqueProduto = Convert.ToDecimal(txtQuantidade.Text);
                produtos.estoqueCriticoProduto = Convert.ToDecimal(txtEstoqueCritico.Text);
                produtos.manufaturadoProduto = manufaturaProduto;
                produtos.controlaEstoqueProduto = controlaProduto;
                produtos.dataCadastroProduto = DateTime.Now;
                produtos.usuarioId = frmLogin.usuariosLogin.usuarioId;

                ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                string retorno = produtosNegocios.Alterar(produtos);

                try
                {
                    int checaRetorno = Convert.ToInt32(retorno);

                    MessageBox.Show("Produto Atualizado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;

                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

                else if (String.IsNullOrEmpty(txtProduto.Text))
                {
                    MessageBox.Show("Campo Descrição do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProduto.Focus();
                }

                else if (String.IsNullOrEmpty(cbUnd.Text))
                {
                    MessageBox.Show("Campo Und é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbUnd.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorCusto.Text))
                {
                    MessageBox.Show("Campo Valor Custo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorCusto.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorVenda.Text))
                {
                    MessageBox.Show("Campo Valor Venda é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorVenda.Focus();
                }

                else if (String.IsNullOrEmpty(txtEstoqueCritico.Text))
                {
                    MessageBox.Show("Campo Estoque Crítico é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEstoqueCritico.Focus();
                }
            }
        }

        #endregion

    }
}
