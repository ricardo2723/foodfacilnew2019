﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadGrupos : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadGrupos(AcaoCRUD acao, Grupos grupos)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Grupos";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Grupos";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = grupos.grupoId.ToString();
                txtDescricao.Text = grupos.grupoDescricao;               
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirGrupos();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarGrupos();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirGrupos()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                Grupos grupos = new Grupos();

                grupos.usuarioId = frmLogin.usuariosLogin.usuarioId;
                grupos.grupoDescricao = txtDescricao.Text;
                grupos.grupoDataCad = DateTime.Now;

                GruposNegocios grupoNegocios = new GruposNegocios();
                string retorno = grupoNegocios.inserir(grupos);

                try
                {
                    int idGrupos = Convert.ToInt32(retorno);
                    MessageBox.Show("Grupo Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }

            }
        }

        private void atualizarGrupos()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                Grupos grupos = new Grupos();

                grupos.grupoId = Convert.ToInt32(txtCodigo.Text);
                grupos.usuarioId = frmLogin.usuariosLogin.usuarioId;
                grupos.grupoDescricao = txtDescricao.Text;
                grupos.grupoDataCad = DateTime.Now;

                GruposNegocios gruposNegocios = new GruposNegocios();
                string retorno = gruposNegocios.Alterar(grupos);

                try
                {
                    int idGrupos = Convert.ToInt32(retorno);
                    MessageBox.Show("Grupo Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }
                                
            }
        }

        #endregion       
        
    }
}
