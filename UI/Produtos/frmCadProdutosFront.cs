﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadProdutosFront : Form
    {
        public frmCadProdutosFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvProdutos.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            frmCadProdutos objFrmCadProdutos = new frmCadProdutos(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmCadProdutos.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaProdutoNome("%");
            }

            this.Cursor = Cursors.Default;
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarProdutos();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (rbDescricao.Checked)
            {   
                pesquisaProdutoNome(txtPesquisa.Text);                
            }
            else if (rbCodigo.Checked)
            {
                pesquisaProdutoCodigo(txtPesquisa.Text);
            }
            else if (rbGrupo.Checked)
            {
                pesquisaProdutoGrupo(txtPesquisa.Text);
            }

            txtPesquisa.Text = "";
                                    
            this.Cursor = Cursors.Default;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (dgvProdutos.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Produto? Todos as informações desse Produto serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;
                }

                //Pegar o usuario Selecionado no Grid
                Produtos produtosSelecionado = dgvProdutos.SelectedRows[0].DataBoundItem as Produtos;

                //instanciar a regra de negocios
                ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                string retorno = produtosNegocios.ExcluirManufaturado(produtosSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idProduto = Convert.ToInt32(retorno);

                    if (idProduto > 0)
                    {
                        retorno = produtosNegocios.Excluir(produtosSelecionado);

                        idProduto = Convert.ToInt32(retorno);
                    }

                    MessageBox.Show("Produto Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaProdutoNome("%" + txtPesquisa.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Produto para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            this.Cursor = Cursors.Default;
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "PRODUTOS")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaProdutoNome(string nome)
        {
            
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.ConsultarPorDrescricao(nome, 'P');

            dgvProdutos.DataSource = null;
            dgvProdutos.DataSource = produtosCollections;

            dgvProdutos.Update();
            dgvProdutos.Refresh();

            labelX2.Text = "Produtos Listados: " + dgvProdutos.RowCount;

            Generic.msgPesquisa("Produto não encontrado", dgvProdutos.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaProdutoCodigo(string cod)
        {
            try
            {
                int cod1 = Convert.ToInt32(cod);
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.ConsultarPorCodigo(cod, 'P');

            dgvProdutos.DataSource = null;
            dgvProdutos.DataSource = produtosCollections;

            dgvProdutos.Update();
            dgvProdutos.Refresh();

            labelX2.Text = "Produtos Listados: " + dgvProdutos.RowCount;

            //parar_cronometro = true;

            Generic.msgPesquisa("Produto não encontrado", dgvProdutos.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaProdutoGrupo(string grupo)
        {
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.ConsultarPorGrupo(grupo, 'P');

            dgvProdutos.DataSource = null;
            dgvProdutos.DataSource = produtosCollections;

            dgvProdutos.Update();
            dgvProdutos.Refresh();

            labelX2.Text = "Produtos Listados: " + dgvProdutos.RowCount;

            Generic.msgPesquisa("Produto não encontrado", dgvProdutos.RowCount);

            txtPesquisa.Text = null;
        }

        private void alterarProdutos()
        {
            if (dgvProdutos.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Produtos produtosSelecionado = dgvProdutos.SelectedRows[0].DataBoundItem as Produtos;

                frmCadProdutos objFrmCadProdutos = new frmCadProdutos(AcaoCRUD.Alterar, produtosSelecionado);
                DialogResult resultado = objFrmCadProdutos.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaProdutoNome("%");
                }

            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Produto para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadProdutosFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadProdutosFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
                case Keys.F6:
                    txtPesquisa.Focus();
                    break;
            }
        }

        private void txtPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvProdutos_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        #endregion
     }
}
