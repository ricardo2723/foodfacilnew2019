﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadEntradaProdutos : Form
    {
        public decimal quantidadeEstoque;
        
        EntradaProdutos entradaProduto = new EntradaProdutos();

        public frmCadEntradaProdutos()
        {
            InitializeComponent();          
        }

        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            inserirProduto();            
        }

        private void btnPesquisaProduto_Click(object sender, EventArgs e)
        {
            pesqProduto(txtCodigo);
        }

        #endregion

        #region AÇÕES

        private void frmCadEntradaProdutos_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadEntradaProdutos_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    this.btnPesquisaProduto.PerformClick();
                    break;
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    getProdutoCod();
                    break;
            }
        }

        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtCodigo.Text == "" || txtCodigo.Text == "0")
                    return;

                Convert.ToInt32(txtCodigo.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        private void txtValorCusto_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDecimal(txtValorCusto.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Custo Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorCusto.Text = "";
                txtValorCusto.Focus();
            }
        }

        private void txtValorVenda_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDecimal(txtValorVenda.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Venda Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorVenda.Text = "";
                txtValorVenda.Focus();
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "ENTRADA")
                {
                    btnSalvar.Enabled = item.cadastrar;
                }
            }
        }

        private void inserirProduto()
        {
            if (Convert.ToDecimal(txtQuantidade.Text) == 0)
            {
                MessageBox.Show("É necessário um valor maior que zero no campo Quantidade", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtQuantidade.Focus();
                return;
            }

            if
               (!String.IsNullOrEmpty(txtCodigo.Text) &&
               !String.IsNullOrEmpty(txtProduto.Text) &&
               !String.IsNullOrEmpty(txtValorCusto.Text) &&
               !String.IsNullOrEmpty(txtValorVenda.Text) &&
               !String.IsNullOrEmpty(txtQuantidade.Text) &&
               !String.IsNullOrEmpty(txtNf.Text))
            {
                entradaProduto.codigoProduto = Generic.checaInteiro(txtCodigo.Text);
                entradaProduto.descricaoProduto = txtProduto.Text;
                entradaProduto.valorCompraProduto = Convert.ToDecimal(txtValorCusto.Text);
                entradaProduto.valorVendaProduto = Convert.ToDecimal(txtValorVenda.Text);
                entradaProduto.estoqueProduto = Convert.ToDecimal(txtQuantidade.Text) + quantidadeEstoque;
                entradaProduto.nfEntradaProduto = txtNf.Text.ToUpper();
                entradaProduto.motivoEntradaProduto = txtMotivoEntrada.Text.ToUpper();
                entradaProduto.quantidadeProdutoEntrada = Convert.ToDecimal(txtQuantidade.Text);               
                entradaProduto.dataCadEntradaProduto = DateTime.Now.Date;
                entradaProduto.usuarioId = frmLogin.usuariosLogin.usuarioId;

                EntradaProdutosNegocios entradaProdutosNegocios = new EntradaProdutosNegocios();
                string retorno = entradaProdutosNegocios.inserir(entradaProduto);

                try
                {
                    int checaRetorno = Convert.ToInt32(retorno);

                    MessageBox.Show("Entrada do Produto salvo com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Generic.limparCampos(panelProduto);
                    txtCodigo.Focus();
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

                else if (String.IsNullOrEmpty(txtProduto.Text))
                {
                    MessageBox.Show("Campo Descrição do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProduto.Focus();
                }
                
                else if (String.IsNullOrEmpty(txtValorCusto.Text))
                {
                    MessageBox.Show("Campo Valor Custo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorCusto.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorVenda.Text))
                {
                    MessageBox.Show("Campo Valor Venda é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorVenda.Focus();
                }

                else if (String.IsNullOrEmpty(txtNf.Text))
                {
                    MessageBox.Show("Campo Doc Entrada é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNf.Focus();
                }

                else if (String.IsNullOrEmpty(txtQuantidade.Text))
                {
                    MessageBox.Show("Campo Quantidade é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQuantidade.Focus();
                }
            }
        }

        private void getProdutoCod()
        {
            entradaProduto.codigoProduto = Generic.checaInteiro(txtCodigo.Text);

            if (entradaProduto.codigoProduto == 0)
                return;

            EntradaProdutosNegocios entradaProdutosNegocios = new EntradaProdutosNegocios();
            EntradaProdutosCollections entradaProdutosCollections = entradaProdutosNegocios.GetProduto(entradaProduto);

            if (entradaProdutosCollections.Count > 0)
            {
                foreach (var itemLista in entradaProdutosCollections)
                {
                    entradaProduto.descricaoProduto = itemLista.descricaoProduto;
                    entradaProduto.valorCompraProduto = itemLista.valorCompraProduto;
                    entradaProduto.valorVendaProduto = itemLista.valorVendaProduto;
                    entradaProduto.estoqueProduto = itemLista.estoqueProduto;

                    txtProduto.Text = entradaProduto.descricaoProduto.ToUpper();
                    txtValorCusto.Text = entradaProduto.valorCompraProduto.ToString("N");
                    txtValorVenda.Text = entradaProduto.valorVendaProduto.ToString("N");
                    quantidadeEstoque = entradaProduto.estoqueProduto;

                    txtNf.Focus();
                }
            }
            else
            {
                MessageBox.Show("Código do Produto não cadastro. Digite um Código Valido", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        public void pesqProduto(TextBox txt)
        {
            frmPesqProdutosFront objPesqProduto = new frmPesqProdutosFront(0);
            DialogResult resultado = objPesqProduto.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                txt.Text = objPesqProduto.codigoProduto.ToString();
                txt.Focus();
            }
            else
            {
                txt.Text = null;
            }
        }

        #endregion

    }
}
