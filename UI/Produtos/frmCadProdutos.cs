﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadProdutos : Form
    {
        AcaoCRUD acaoSelecionada;
        public char controlaProduto = 'S';
        public char manufaturaProduto = 'N';
        public decimal custoProdutoManufatura;
        public decimal vendaProdutoManufatura;

        Produtos produtoManufatura = new Produtos();

        public frmCadProdutos()
        {
            InitializeComponent();
        }

        public frmCadProdutos(AcaoCRUD acao, Produtos produtos)
        {
            InitializeComponent();

            dgvItensManufatura.AutoGenerateColumns = false;

            acaoSelecionada = acao;

            //Carrega Combo Unidade de Medida
            UnidadeMedidaNegocios unidadeMedidaNegocios = new UnidadeMedidaNegocios();
            UnidadeMedidaCollections unidadeMedidaCollections = unidadeMedidaNegocios.ConsultarDescricao("%");
            cbUnd.DataSource = null;
            cbUnd.DataSource = unidadeMedidaCollections;

            //Carrega Combo Grupos
            GruposNegocios gruposNegocios = new GruposNegocios();
            GruposCollections gruposCollections = gruposNegocios.ConsultarDescricao("%");
            cbGrupo.DataSource = null;
            cbGrupo.DataSource = gruposCollections;

            //Carrega Combo Grupos
            FornecedoresNegocios fornecedoresNegocios = new FornecedoresNegocios();
            FornecedoresCollections fornecedoresCollections = fornecedoresNegocios.ConsultarPorNome("%");
            cbFornecedor.DataSource = null;
            cbFornecedor.DataSource = fornecedoresCollections;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Produtos";
                btnSalvar.Text = "  Salvar Novo (F12)";
                panelManufatura.Enabled = false;
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                txtCodigo.ReadOnly = true;

                this.Text = "DRSis - Food Facil - Alterar Produtos";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                if (produtos.controlaEstoqueProduto == 'S')
                {
                    rbEstoqueSim.Checked = true;
                }
                else if (produtos.controlaEstoqueProduto == 'N')
                {
                    rbEstoqueNao.Checked = true;
                }

                if (produtos.manufaturadoProduto == 'S')
                {
                    rbManufaturaSim.Checked = true;
                    panelManufatura.Enabled = true;
                    txtLucroPorcent.Enabled = false;
                    txtLucroReal.Enabled = false;
                    carregaGrid(produtos);
                }
                else if (produtos.manufaturadoProduto == 'N')
                {
                    rbManufaturaNao.Checked = true;
                    panelManufatura.Enabled = false;
                    txtLucroPorcent.Enabled = true;
                    txtLucroReal.Enabled = true;
                }

                txtCodigo.Text = produtos.codigoProduto.ToString();
                cbUnd.SelectedValue = produtos.undMedidaId;
                cbFornecedor.SelectedValue = produtos.idFornecedor;
                cbGrupo.SelectedValue = produtos.grupoId;
                txtProduto.Text = produtos.descricaoProduto;
                txtValorCusto.Text = produtos.valorCompraProduto.ToString();
                txtValorVenda.Text = produtos.valorVendaProduto.ToString();
                txtQuantidade.Text = produtos.estoqueProduto.ToString();
                txtEstoqueCritico.Text = produtos.estoqueCriticoProduto.ToString();
                manufaturaProduto = produtos.manufaturadoProduto;
                controlaProduto = produtos.controlaEstoqueProduto;                
            }
        }

        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirProduto();
            }
            else if (acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarProduto();
            }
        }

        private void btnAddItemProduto_Click(object sender, EventArgs e)
        {
            PedidoItemAdd();
        }

        private void btnPesqProduto_Click(object sender, EventArgs e)
        {
            pesqProduto(txtCodManufatura);
        }

        #endregion

        #region AÇÕES

        private void frmCadProdutos_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtCodManufatura_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    getProdutoCod();
                    break;
            }
        }

        private void txtQuantidadeManufatura_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnAddItemProduto.PerformClick();
                    break;
            }
        }

        private void btnExcluirItemProduto_Click(object sender, EventArgs e)
        {
            PedidoItemExcluir();
        }

        private void rbManufaturaNao_Click(object sender, EventArgs e)
        {
            panelManufatura.Enabled = false;
            txtValorCusto.ReadOnly = false;
            rbEstoqueSim.Checked = true;
            txtLucroReal.Enabled = true;
            txtLucroPorcent.Enabled = true;
            controlaProduto = 'S';
            manufaturaProduto = 'N';
        }

        private void rbManufaturaSim_Click(object sender, EventArgs e)
        {
            panelManufatura.Enabled = true;
            txtValorCusto.ReadOnly = true;
            rbEstoqueNao.Checked = true;
            txtLucroReal.Enabled = false;
            txtLucroPorcent.Enabled = false;
            manufaturaProduto = 'S';
            controlaProduto = 'N';
        }

        private void rbEstoqueSim_Click(object sender, EventArgs e)
        {
            controlaProduto = 'S';
        }

        private void rbEstoqueNao_Click(object sender, EventArgs e)
        {
            controlaProduto = 'N';
        }

        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(txtCodigo.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        private void txtValorCusto_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal cust = Convert.ToDecimal(txtValorCusto.Text);
                txtValorCusto.Text = cust.ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Custo Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorCusto.Text = "";
                txtValorCusto.Focus();
            }
        }

        private void txtValorVenda_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDecimal(txtValorVenda.Text);
                decimal vend = Convert.ToDecimal(txtValorVenda.Text);
                txtValorVenda.Text = vend.ToString("N");
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Venda Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorVenda.Text = "";
                txtValorVenda.Focus();
            }
        }

        private void txtEstoqueCritico_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDecimal(txtEstoqueCritico.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Estoque Crítico Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtEstoqueCritico.Text = "";
                txtEstoqueCritico.Focus();
            }
        }

        private void txtLucroPorcent_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtLucroPorcent.Text == "")
                    return;

                txtValorVenda.Text = (Convert.ToDecimal(txtValorCusto.Text) * (Convert.ToDecimal(txtLucroPorcent.Text) / 100) + Convert.ToDecimal(txtValorCusto.Text)).ToString("N");

            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Lucro Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorCusto.Text = "";
                txtValorCusto.Focus();
            }

        }

        private void txtLucroReal_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtLucroReal.Text == "")
                    return;

                txtValorVenda.Text = (Convert.ToDecimal(txtValorCusto.Text) + Convert.ToDecimal(txtLucroReal.Text)).ToString("N");                
            }
            catch (Exception)
            {
                MessageBox.Show("Valor de Lucro Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorVenda.Text = "";
                txtValorVenda.Focus();
            }
        }

        #endregion

        #region METODOS

        private void inserirProduto()
        {
            if
               (!String.IsNullOrEmpty(txtCodigo.Text) &&
               !String.IsNullOrEmpty(txtProduto.Text) &&
               !String.IsNullOrEmpty(cbUnd.Text) &&
               !String.IsNullOrEmpty(cbGrupo.Text) &&
               !String.IsNullOrEmpty(txtValorCusto.Text) &&
               !String.IsNullOrEmpty(txtValorVenda.Text) &&
               !String.IsNullOrEmpty(txtEstoqueCritico.Text))
            {
                Produtos produtos = new Produtos();

                produtos.codigoProduto = Generic.checaInteiro(txtCodigo.Text);
                produtos.undMedidaId = Convert.ToInt32(cbUnd.SelectedValue);
                produtos.idFornecedor = Convert.ToInt32(cbFornecedor.SelectedValue);
                produtos.grupoId = Convert.ToInt32(cbGrupo.SelectedValue);
                produtos.descricaoProduto = txtProduto.Text;
                produtos.valorCompraProduto = Convert.ToDecimal(txtValorCusto.Text);
                produtos.valorVendaProduto = Convert.ToDecimal(txtValorVenda.Text);
                produtos.estoqueProduto = Convert.ToDecimal(txtQuantidade.Text);
                produtos.estoqueCriticoProduto = Convert.ToDecimal(txtEstoqueCritico.Text);
                produtos.manufaturadoProduto = manufaturaProduto;
                produtos.controlaEstoqueProduto = controlaProduto;
                produtos.dataCadastroProduto = DateTime.Now;
                produtos.usuarioId = frmLogin.usuariosLogin.usuarioId;
                produtos.tipoProduto = 'P';

                ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                string retorno = produtosNegocios.inserir(produtos);

                try
                {
                    int checaRetorno = Convert.ToInt32(retorno);

                    if (rbManufaturaSim.Checked)
                    {
                        for (int i = 0; i < dgvItensManufatura.Rows.Count; i++)
                        {
                            produtos.codigoProduto = checaRetorno;
                            produtos.codProdutoManufaturado = Convert.ToInt32(dgvItensManufatura.Rows[i].Cells["idProdutoManifaturado"].Value);
                            produtos.quantidadeProdutoManufaturado = Convert.ToDecimal(dgvItensManufatura.Rows[i].Cells["qtdProdutoManufaturado"].Value);

                            retorno = produtosNegocios.inserirManufaturado(produtos);

                            checaRetorno = Convert.ToInt32(retorno);
                        }
                    }

                    MessageBox.Show("Produto Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                    

                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

                else if (String.IsNullOrEmpty(txtProduto.Text))
                {
                    MessageBox.Show("Campo Descrição do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProduto.Focus();
                }

                else if (String.IsNullOrEmpty(cbUnd.Text))
                {
                    MessageBox.Show("Campo Und é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbUnd.Focus();
                }

                else if (String.IsNullOrEmpty(cbGrupo.Text))
                {
                    MessageBox.Show("Campo Grupo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbGrupo.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorCusto.Text))
                {
                    MessageBox.Show("Campo Valor Custo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorCusto.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorVenda.Text))
                {
                    MessageBox.Show("Campo Valor Venda é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbGrupo.Focus();
                }

                else if (String.IsNullOrEmpty(txtEstoqueCritico.Text))
                {
                    MessageBox.Show("Campo Estoque Crítico é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEstoqueCritico.Focus();
                }
            }
        }

        private void atualizarProduto()
        {
            if
               (!String.IsNullOrEmpty(txtCodigo.Text) &&
               !String.IsNullOrEmpty(txtProduto.Text) &&
               !String.IsNullOrEmpty(cbUnd.Text) &&
               !String.IsNullOrEmpty(cbGrupo.Text) &&
               !String.IsNullOrEmpty(txtValorCusto.Text) &&
               !String.IsNullOrEmpty(txtValorVenda.Text) &&
               !String.IsNullOrEmpty(txtEstoqueCritico.Text))
            {
                Produtos produtos = new Produtos();

                produtos.codigoProduto = Generic.checaInteiro(txtCodigo.Text);
                produtos.undMedidaId = Convert.ToInt32(cbUnd.SelectedValue);
                produtos.idFornecedor = Convert.ToInt32(cbFornecedor.SelectedValue);
                produtos.grupoId = Convert.ToInt32(cbGrupo.SelectedValue);
                produtos.descricaoProduto = txtProduto.Text;
                produtos.valorCompraProduto = Convert.ToDecimal(txtValorCusto.Text);
                produtos.valorVendaProduto = Convert.ToDecimal(txtValorVenda.Text);
                produtos.estoqueProduto = Convert.ToDecimal(txtQuantidade.Text);
                produtos.estoqueCriticoProduto = Convert.ToDecimal(txtEstoqueCritico.Text);
                produtos.manufaturadoProduto = manufaturaProduto;
                produtos.controlaEstoqueProduto = controlaProduto;
                produtos.dataCadastroProduto = DateTime.Now;
                produtos.usuarioId = frmLogin.usuariosLogin.usuarioId;

                ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                string retorno = produtosNegocios.Alterar(produtos);

                try
                {
                    int checaRetorno = Convert.ToInt32(retorno);

                    MessageBox.Show("Produto Atualizado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                    

                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

                else if (String.IsNullOrEmpty(txtProduto.Text))
                {
                    MessageBox.Show("Campo Descrição do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProduto.Focus();
                }

                else if (String.IsNullOrEmpty(cbUnd.Text))
                {
                    MessageBox.Show("Campo Und é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbUnd.Focus();
                }

                else if (String.IsNullOrEmpty(cbGrupo.Text))
                {
                    MessageBox.Show("Campo Grupo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbGrupo.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorCusto.Text))
                {
                    MessageBox.Show("Campo Valor Custo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtValorCusto.Focus();
                }

                else if (String.IsNullOrEmpty(txtValorVenda.Text))
                {
                    MessageBox.Show("Campo Valor Venda é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbGrupo.Focus();
                }

                else if (String.IsNullOrEmpty(txtEstoqueCritico.Text))
                {
                    MessageBox.Show("Campo Estoque Crítico é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEstoqueCritico.Focus();
                }
            }
        }

        private void getProdutoCod()
        {
            produtoManufatura.codProdutoManufaturado = Generic.checaInteiro(txtCodManufatura.Text);

            if (produtoManufatura.codProdutoManufaturado == 0)
                return;

            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.GetProduto(produtoManufatura);

            if (produtosCollections.Count > 0)
            {
                foreach (var itemLista in produtosCollections)
                {
                    produtoManufatura.descricaoProduto = itemLista.descricaoProduto;
                    produtoManufatura.valorCompraProduto = itemLista.valorCompraProduto;
                    produtoManufatura.valorVendaProduto = itemLista.valorVendaProduto;

                    txtProdutoManufatura.Text = produtoManufatura.descricaoProduto;
                    txtQuantidadeManufatura.Focus();
                }
            }
            else
            {
                MessageBox.Show("Código do Produto não cadastro para Itens. Digite um Código Valido", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodManufatura.Text = "";
                txtCodManufatura.Focus();
            }

        }
        
        private void PedidoItemAdd()
        {
            if
               (!String.IsNullOrEmpty(txtCodManufatura.Text) &&
               !String.IsNullOrEmpty(txtProdutoManufatura.Text) &&
               !String.IsNullOrEmpty(txtQuantidadeManufatura.Text))
            {
                if (acaoSelecionada == AcaoCRUD.Alterar)
                {
                    string retorno = "";
                    try
                    {
                        produtoManufatura.codigoProduto = Convert.ToInt32(txtCodigo.Text);
                        produtoManufatura.codProdutoManufaturado = Convert.ToInt32(txtCodManufatura.Text);
                        produtoManufatura.quantidadeProdutoManufaturado = Convert.ToDecimal(txtQuantidadeManufatura.Text);

                        ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                        retorno = produtosNegocios.inserirManufaturado(produtoManufatura);

                        int checaRetorno = Convert.ToInt32(retorno);

                        carregaGrid(produtoManufatura);

                        somaGrid();

                        Generic.limparCampos(panelManufatura);
                        txtCodManufatura.Focus();

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;    
                    }
                    
                }
                else if (acaoSelecionada == AcaoCRUD.Inserir)
                {
                    produtoManufatura.quantidadeProdutoManufaturado = Convert.ToDecimal(txtQuantidadeManufatura.Text);

                    custoProdutoManufatura += produtoManufatura.valorCompraProduto * produtoManufatura.quantidadeProdutoManufaturado;
                    txtValorCusto.Text = custoProdutoManufatura.ToString();

                    vendaProdutoManufatura += produtoManufatura.valorVendaProduto * produtoManufatura.quantidadeProdutoManufaturado;
                    txtValorVenda.Text = vendaProdutoManufatura.ToString();

                    dgvItensManufatura.Rows.Add(produtoManufatura.codProdutoManufaturado, produtoManufatura.descricaoProduto, produtoManufatura.quantidadeProdutoManufaturado, produtoManufatura.valorCompraProduto, produtoManufatura.valorVendaProduto);
                    Generic.limparCampos(panelManufatura);
                    txtCodManufatura.Focus();
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtCodManufatura.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodManufatura.Focus();
                }

                else if (String.IsNullOrEmpty(txtProdutoManufatura.Text))
                {
                    MessageBox.Show("Campo Item do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProdutoManufatura.Focus();
                }

                else if (String.IsNullOrEmpty(txtQuantidadeManufatura.Text))
                {
                    MessageBox.Show("Campo Quantidade é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQuantidadeManufatura.Focus();
                }
            }
        }

        private void PedidoItemExcluir()
        {
            if (acaoSelecionada == AcaoCRUD.Inserir)
            {
                if (dgvItensManufatura.Rows.Count > 0)
                {
                    custoProdutoManufatura = (Convert.ToDecimal(txtValorCusto.Text) - (Convert.ToDecimal(dgvItensManufatura.SelectedRows[0].Cells[3].Value) * Convert.ToDecimal(dgvItensManufatura.SelectedRows[0].Cells[2].Value)));
                    txtValorCusto.Text = custoProdutoManufatura.ToString("N");

                    vendaProdutoManufatura = (Convert.ToDecimal(txtValorVenda.Text) - (Convert.ToDecimal(dgvItensManufatura.SelectedRows[0].Cells[4].Value) * Convert.ToDecimal(dgvItensManufatura.SelectedRows[0].Cells[2].Value)));
                    txtValorVenda.Text = vendaProdutoManufatura.ToString("N");

                    dgvItensManufatura.Rows.RemoveAt(dgvItensManufatura.CurrentRow.Index);
                }
            }
            else if (acaoSelecionada == AcaoCRUD.Alterar)
            {
                Produtos produtosSelecionado = dgvItensManufatura.SelectedRows[0].DataBoundItem as Produtos;

                produtosSelecionado.codigoProduto = Convert.ToInt32(txtCodigo.Text);
                //instanciar a regra de negocios
                ProdutosNegocios produtosNegocios = new ProdutosNegocios();
                string retorno = produtosNegocios.ExcluirItemManufaturado(produtosSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idProduto = Convert.ToInt32(retorno);
                    MessageBox.Show("Item Excluido com Sucesso", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    carregaGrid(produtosSelecionado);

                    somaGrid();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno + ex, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void carregaGrid(Produtos produtos)
        {
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.ConsultarPorManufaturadoCodigo(produtos.codigoProduto, 'P');

            dgvItensManufatura.DataSource = null;
            dgvItensManufatura.DataSource = produtosCollections;

            dgvItensManufatura.Update();
            dgvItensManufatura.Refresh();
        }

        private void somaGrid()
        {
            decimal soma = 0;
            foreach (DataGridViewRow dr in dgvItensManufatura.Rows)
                soma += Convert.ToDecimal(dr.Cells[3].Value) * Convert.ToDecimal(dr.Cells[2].Value);
            txtValorCusto.Text = soma.ToString("N");

            soma = 0;
            foreach (DataGridViewRow dri in dgvItensManufatura.Rows)
                soma += Convert.ToDecimal(dri.Cells[4].Value) * Convert.ToDecimal(dri.Cells[2].Value);
            txtValorVenda.Text = soma.ToString("N");
        }

        public void pesqProduto(TextBox txt)
        {
            frmPesqProdutosFront objPesqProduto = new frmPesqProdutosFront(0);
            DialogResult resultado = objPesqProduto.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                txt.Text = objPesqProduto.codigoProduto.ToString();
                txt.Focus();
            }
            else
            {
                txt.Text = null;
            }
        }

        #endregion

        

    }
}
