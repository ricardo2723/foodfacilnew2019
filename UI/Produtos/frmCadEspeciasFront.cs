﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadEspeciasFront : Form
    {
        public frmCadEspeciasFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvEspeciais.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadEspeciais objFrmExtras = new frmCadEspeciais(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmExtras.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaEspeciais("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarEspecial();
            pesquisaEspeciais("%");            
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisaEspeciais("%" + txtPesquisar.Text + "%");
            txtPesquisar.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvEspeciais.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Especial? Todas as informações desse Especial serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Especiais especialSelecionado = dgvEspeciais.SelectedRows[0].DataBoundItem as Especiais; 

                //instanciar a regra de negocios
                EspeciaisNegocios especialNegocios = new EspeciaisNegocios();
                string retorno = especialNegocios.Excluir(especialSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idExtra = Convert.ToInt32(retorno);
                    MessageBox.Show("Especial Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaEspeciais("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Especial para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "ESPECIAIS")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaEspeciais(string descri)
        {

            EspeciaisNegocios especiaisNegocios = new EspeciaisNegocios();
            bindEspeciais.DataSource = null;
            bindEspeciais.DataSource = especiaisNegocios.ConsultarDescricao(descri);

            labelX2.Text = "Especiais Listados: " + dgvEspeciais.RowCount;

            Generic.msgPesquisa("Especiais não encontrado", dgvEspeciais.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarEspecial()
        {
            
            if (dgvEspeciais.RowCount > 0)
            {
                frmCadEspeciais objFrmCadEspeciais = new frmCadEspeciais(AcaoCRUD.Alterar, (bindEspeciais.Current as Especiais));
                DialogResult resultado = objFrmCadEspeciais.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaEspeciais("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Especial para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadExtrasFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
            pesquisaEspeciais("%");

        }

        private void frmCadExtrasFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvExtras_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        #endregion
       
    }
}
