﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadGruposFront : Form
    {
        public frmCadGruposFront()
        {
            InitializeComponent();
            //Não gerar Linhas Automaticas
            dgvGrupos.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadGrupos objFrmExtras = new frmCadGrupos(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmExtras.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaGrupos("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarGrupo();
            pesquisaGrupos("%");            
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisaGrupos("%" + txtPesquisar.Text + "%");
            txtPesquisar.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvGrupos.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Grupo? Todas as informações desse Grupo serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Grupos grupoSelecionado = dgvGrupos.SelectedRows[0].DataBoundItem as Grupos; 

                //instanciar a regra de negocios
                GruposNegocios grupoNegocios = new GruposNegocios();
                string retorno = grupoNegocios.Excluir(grupoSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idExtra = Convert.ToInt32(retorno);
                    MessageBox.Show("Grupo Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaGrupos("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Especial para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "GRUPOS")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaGrupos(string descri)
        {

            GruposNegocios gruposNegocios = new GruposNegocios();
            GruposCollections gruposCollections = gruposNegocios.ConsultarDescricao(descri);

            dgvGrupos.DataSource = null;
            dgvGrupos.DataSource = gruposCollections;
            
            dgvGrupos.Update();
            dgvGrupos.Refresh();
            
            labelX2.Text = "Grupos Listados: " + dgvGrupos.RowCount;

            Generic.msgPesquisa("Grupo não encontrado", dgvGrupos.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarGrupo()
        {
            if (dgvGrupos.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Grupos grupoSelecionado = dgvGrupos.SelectedRows[0].DataBoundItem as Grupos;

                frmCadGrupos objFrmCadGrupos = new frmCadGrupos(AcaoCRUD.Alterar, grupoSelecionado);
                DialogResult resultado = objFrmCadGrupos.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaGrupos("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Grupo para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadExtrasFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadExtrasFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvExtras_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        #endregion
       
    }
}
