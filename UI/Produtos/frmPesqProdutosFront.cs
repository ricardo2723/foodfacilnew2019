﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmPesqProdutosFront : Form
    {
        public int codigoProduto;
        int extra;

        public frmPesqProdutosFront(int num)
        {
            InitializeComponent();
            dgvProdutos.AutoGenerateColumns = false;
            extra = num;
        }

        #region BOTÕES

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (rbDescricao.Checked)
            {
                pesquisaProdutoNome(txtPesquisa.Text);
            }
            else if (rbCodigo.Checked)
            {
                pesquisaProdutoCodigo(txtPesquisa.Text);
            }
            else if (rbGrupo.Checked)
            {
                pesquisaProdutoGrupo(txtPesquisa.Text);
            }

            txtPesquisa.Text = "";

            this.Cursor = Cursors.Default;
        }

        #endregion

        #region METODOS

        private void pesquisaProdutoNome(string nome)
        {
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = new ProdutosCollections();
            dgvProdutos.DataSource = null;

            if (extra == 1)
            {
                produtosCollections = produtosNegocios.ConsultarPorDrescricaoExtra(nome);
            }
            else
            {                
                produtosCollections = produtosNegocios.ConsultarPorDrescricao(nome, 'P');
            }            
            
            dgvProdutos.DataSource = produtosCollections;
            dgvProdutos.Update();

            Generic.msgPesquisa("Produto não encontrado", dgvProdutos.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaProdutoCodigo(string cod)
        {
            try
            {
                int cod1 = Convert.ToInt32(cod);
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.ConsultarPorCodigo(cod, 'P');

            dgvProdutos.DataSource = null;
            dgvProdutos.DataSource = produtosCollections;

            dgvProdutos.Update();
            dgvProdutos.Refresh();

            Generic.msgPesquisa("Produto não encontrado", dgvProdutos.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaProdutoGrupo(string grupo)
        {
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            ProdutosCollections produtosCollections = produtosNegocios.ConsultarPorGrupo(grupo, 'P');

            dgvProdutos.DataSource = null;
            dgvProdutos.DataSource = produtosCollections;

            dgvProdutos.Update();
            dgvProdutos.Refresh();

            Generic.msgPesquisa("Produto não encontrado", dgvProdutos.RowCount);

            txtPesquisa.Text = null;
        }

        #endregion

        #region AÇÕES

        private void frmPesqProdutosFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void rbDescricao_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
        }

        private void rbCodigo_CheckedChanged(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
        }

        private void rbGrupo_CheckedChanged(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
        }

        #endregion

        private void dgvProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    codigoProduto = Convert.ToInt32(dgvProdutos.SelectedRows[0].Cells[0].Value);
                    this.DialogResult = DialogResult.Yes;
                    break;
            }
        }
    }
}
