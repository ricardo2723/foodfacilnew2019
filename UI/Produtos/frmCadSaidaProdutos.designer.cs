﻿namespace UI
{
    partial class frmCadSaidaProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadSaidaProdutos));
            this.panelProduto = new DevComponents.DotNetBar.PanelEx();
            this.btnPesquisaProduto = new DevComponents.DotNetBar.ButtonX();
            this.txtQuantidade = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.txtCodigo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtMotivoSaida = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txtProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.panelProduto.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelProduto
            // 
            this.panelProduto.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelProduto.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelProduto.Controls.Add(this.btnPesquisaProduto);
            this.panelProduto.Controls.Add(this.txtQuantidade);
            this.panelProduto.Controls.Add(this.labelX15);
            this.panelProduto.Controls.Add(this.txtCodigo);
            this.panelProduto.Controls.Add(this.labelX8);
            this.panelProduto.Controls.Add(this.txtMotivoSaida);
            this.panelProduto.Controls.Add(this.labelX9);
            this.panelProduto.Controls.Add(this.txtProduto);
            this.panelProduto.Controls.Add(this.labelX1);
            this.panelProduto.Location = new System.Drawing.Point(3, 3);
            this.panelProduto.Name = "panelProduto";
            this.panelProduto.Size = new System.Drawing.Size(384, 188);
            this.panelProduto.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelProduto.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelProduto.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelProduto.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelProduto.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelProduto.Style.GradientAngle = 90;
            this.panelProduto.TabIndex = 0;
            // 
            // btnPesquisaProduto
            // 
            this.btnPesquisaProduto.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPesquisaProduto.BackColor = System.Drawing.Color.Transparent;
            this.btnPesquisaProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.balloonTip1.SetBalloonCaption(this.btnPesquisaProduto, "Pesquisar (F5)");
            this.balloonTip1.SetBalloonText(this.btnPesquisaProduto, "Pesquisar Codigo do Produto");
            this.btnPesquisaProduto.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnPesquisaProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisaProduto.HoverImage = global::UI.Properties.Resources._1394159835_search;
            this.btnPesquisaProduto.Image = global::UI.Properties.Resources._1394159835_search;
            this.btnPesquisaProduto.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnPesquisaProduto.Location = new System.Drawing.Point(91, 23);
            this.btnPesquisaProduto.Name = "btnPesquisaProduto";
            this.btnPesquisaProduto.Size = new System.Drawing.Size(25, 25);
            this.btnPesquisaProduto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPesquisaProduto.TabIndex = 1;
            this.btnPesquisaProduto.Click += new System.EventHandler(this.btnPesquisaProduto_Click);
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtQuantidade.Border.Class = "TextBoxBorder";
            this.txtQuantidade.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtQuantidade.ForeColor = System.Drawing.Color.Black;
            this.txtQuantidade.Location = new System.Drawing.Point(9, 73);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.PreventEnterBeep = true;
            this.txtQuantidade.Size = new System.Drawing.Size(81, 20);
            this.txtQuantidade.TabIndex = 6;
            this.txtQuantidade.Text = "0";
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Location = new System.Drawing.Point(9, 53);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(62, 23);
            this.labelX15.TabIndex = 98;
            this.labelX15.Text = "Quantidade";
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            this.balloonTip1.SetBalloonCaption(this.txtCodigo, null);
            this.balloonTip1.SetBalloonText(this.txtCodigo, "Após digitar o Código pressione Enter");
            // 
            // 
            // 
            this.txtCodigo.Border.Class = "TextBoxBorder";
            this.txtCodigo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigo.ForeColor = System.Drawing.Color.Black;
            this.txtCodigo.Location = new System.Drawing.Point(9, 25);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PreventEnterBeep = true;
            this.txtCodigo.Size = new System.Drawing.Size(76, 20);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            this.txtCodigo.Leave += new System.EventHandler(this.txtCodigo_Leave);
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(9, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(76, 23);
            this.labelX8.TabIndex = 80;
            this.labelX8.Text = "Código";
            // 
            // txtMotivoSaida
            // 
            this.txtMotivoSaida.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMotivoSaida.Border.Class = "TextBoxBorder";
            this.txtMotivoSaida.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMotivoSaida.ForeColor = System.Drawing.Color.Black;
            this.txtMotivoSaida.Location = new System.Drawing.Point(9, 119);
            this.txtMotivoSaida.MaxLength = 100;
            this.txtMotivoSaida.Multiline = true;
            this.txtMotivoSaida.Name = "txtMotivoSaida";
            this.txtMotivoSaida.PreventEnterBeep = true;
            this.txtMotivoSaida.Size = new System.Drawing.Size(362, 52);
            this.txtMotivoSaida.TabIndex = 7;
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(9, 99);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(76, 23);
            this.labelX9.TabIndex = 96;
            this.labelX9.Text = "Motivo Saida";
            // 
            // txtProduto
            // 
            this.txtProduto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtProduto.Border.Class = "TextBoxBorder";
            this.txtProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProduto.ForeColor = System.Drawing.Color.Black;
            this.txtProduto.Location = new System.Drawing.Point(122, 25);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.PreventEnterBeep = true;
            this.txtProduto.ReadOnly = true;
            this.txtProduto.Size = new System.Drawing.Size(249, 20);
            this.txtProduto.TabIndex = 2;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(122, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(135, 23);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "Descrição do Produto";
            // 
            // balloonTip1
            // 
            this.balloonTip1.Style = DevComponents.DotNetBar.eBallonStyle.Office2007Alert;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnFechar.Location = new System.Drawing.Point(273, 195);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(101, 34);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 2;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>  Fechar (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.salva;
            this.btnSalvar.Location = new System.Drawing.Point(12, 197);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(101, 34);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 1;
            this.btnSalvar.Text = "  Salvar (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.bntSalvar_Click);
            // 
            // frmCadSaidaProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(391, 235);
            this.Controls.Add(this.panelProduto);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadSaidaProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Saida de Produtos";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadSaidaProdutos_KeyUp);
            this.panelProduto.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private DevComponents.DotNetBar.PanelEx panelProduto;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigo;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProduto;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.BalloonTip balloonTip1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtQuantidade;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMotivoSaida;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.ButtonX btnPesquisaProduto;
    }
}