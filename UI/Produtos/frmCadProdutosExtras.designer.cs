﻿namespace UI
{
    partial class frmCadProdutosExtras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadProdutosExtras));
            this.panelProduto = new DevComponents.DotNetBar.PanelEx();
            this.txtQuantidade = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.cbUnd = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtEstoqueCritico = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCodigo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtInformAdicional = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cbFornecedor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtLucroReal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtValorVenda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtLucroPorcent = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtValorCusto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rbEstoqueNao = new System.Windows.Forms.RadioButton();
            this.rbEstoqueSim = new System.Windows.Forms.RadioButton();
            this.panelProduto.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelProduto
            // 
            this.panelProduto.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelProduto.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelProduto.Controls.Add(this.txtQuantidade);
            this.panelProduto.Controls.Add(this.labelX15);
            this.panelProduto.Controls.Add(this.cbUnd);
            this.panelProduto.Controls.Add(this.txtEstoqueCritico);
            this.panelProduto.Controls.Add(this.txtCodigo);
            this.panelProduto.Controls.Add(this.labelX14);
            this.panelProduto.Controls.Add(this.labelX8);
            this.panelProduto.Controls.Add(this.txtInformAdicional);
            this.panelProduto.Controls.Add(this.cbFornecedor);
            this.panelProduto.Controls.Add(this.labelX9);
            this.panelProduto.Controls.Add(this.labelX4);
            this.panelProduto.Controls.Add(this.txtLucroReal);
            this.panelProduto.Controls.Add(this.labelX10);
            this.panelProduto.Controls.Add(this.labelX2);
            this.panelProduto.Controls.Add(this.txtValorVenda);
            this.panelProduto.Controls.Add(this.txtProduto);
            this.panelProduto.Controls.Add(this.labelX7);
            this.panelProduto.Controls.Add(this.labelX1);
            this.panelProduto.Controls.Add(this.txtLucroPorcent);
            this.panelProduto.Controls.Add(this.labelX6);
            this.panelProduto.Controls.Add(this.txtValorCusto);
            this.panelProduto.Controls.Add(this.labelX5);
            this.panelProduto.Location = new System.Drawing.Point(3, 3);
            this.panelProduto.Name = "panelProduto";
            this.panelProduto.Size = new System.Drawing.Size(592, 242);
            this.panelProduto.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelProduto.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelProduto.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelProduto.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelProduto.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelProduto.Style.GradientAngle = 90;
            this.panelProduto.TabIndex = 0;
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtQuantidade.Border.Class = "TextBoxBorder";
            this.txtQuantidade.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtQuantidade.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantidade.ForeColor = System.Drawing.Color.Black;
            this.txtQuantidade.Location = new System.Drawing.Point(503, 127);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.PreventEnterBeep = true;
            this.txtQuantidade.Size = new System.Drawing.Size(81, 25);
            this.txtQuantidade.TabIndex = 12;
            this.txtQuantidade.Text = "0";
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Location = new System.Drawing.Point(503, 107);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(62, 23);
            this.labelX15.TabIndex = 98;
            this.labelX15.Text = "Qtd Estoque";
            // 
            // cbUnd
            // 
            this.cbUnd.DisplayMember = "undMedidaDescricao";
            this.cbUnd.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbUnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUnd.FormattingEnabled = true;
            this.cbUnd.ItemHeight = 14;
            this.cbUnd.Location = new System.Drawing.Point(9, 72);
            this.cbUnd.Name = "cbUnd";
            this.cbUnd.Size = new System.Drawing.Size(62, 20);
            this.cbUnd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbUnd.TabIndex = 2;
            this.cbUnd.ValueMember = "undMedidaId";
            // 
            // txtEstoqueCritico
            // 
            this.txtEstoqueCritico.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtEstoqueCritico.Border.Class = "TextBoxBorder";
            this.txtEstoqueCritico.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtEstoqueCritico.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstoqueCritico.ForeColor = System.Drawing.Color.Black;
            this.txtEstoqueCritico.Location = new System.Drawing.Point(405, 127);
            this.txtEstoqueCritico.Name = "txtEstoqueCritico";
            this.txtEstoqueCritico.PreventEnterBeep = true;
            this.txtEstoqueCritico.Size = new System.Drawing.Size(87, 25);
            this.txtEstoqueCritico.TabIndex = 11;
            this.txtEstoqueCritico.Text = "0";
            this.txtEstoqueCritico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEstoqueCritico.Leave += new System.EventHandler(this.txtEstoqueCritico_Leave);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtCodigo.Border.Class = "TextBoxBorder";
            this.txtCodigo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigo.ForeColor = System.Drawing.Color.Black;
            this.txtCodigo.Location = new System.Drawing.Point(9, 25);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PreventEnterBeep = true;
            this.txtCodigo.Size = new System.Drawing.Size(76, 20);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.Leave += new System.EventHandler(this.txtCodigo_Leave);
            // 
            // labelX14
            // 
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Location = new System.Drawing.Point(405, 107);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(80, 23);
            this.labelX14.TabIndex = 97;
            this.labelX14.Text = "Estoque Crítico";
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(9, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(76, 23);
            this.labelX8.TabIndex = 80;
            this.labelX8.Text = "Código";
            // 
            // txtInformAdicional
            // 
            this.txtInformAdicional.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtInformAdicional.Border.Class = "TextBoxBorder";
            this.txtInformAdicional.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtInformAdicional.ForeColor = System.Drawing.Color.Black;
            this.txtInformAdicional.Location = new System.Drawing.Point(9, 173);
            this.txtInformAdicional.Multiline = true;
            this.txtInformAdicional.Name = "txtInformAdicional";
            this.txtInformAdicional.PreventEnterBeep = true;
            this.txtInformAdicional.Size = new System.Drawing.Size(575, 52);
            this.txtInformAdicional.TabIndex = 13;
            // 
            // cbFornecedor
            // 
            this.cbFornecedor.DisplayMember = "nomeFornecedor";
            this.cbFornecedor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFornecedor.FormattingEnabled = true;
            this.cbFornecedor.ItemHeight = 14;
            this.cbFornecedor.Location = new System.Drawing.Point(78, 72);
            this.cbFornecedor.Name = "cbFornecedor";
            this.cbFornecedor.Size = new System.Drawing.Size(506, 20);
            this.cbFornecedor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbFornecedor.TabIndex = 4;
            this.cbFornecedor.ValueMember = "idFornecedor";
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(9, 153);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(117, 23);
            this.labelX9.TabIndex = 96;
            this.labelX9.Text = "Informações Adicionais";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(78, 51);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(62, 23);
            this.labelX4.TabIndex = 76;
            this.labelX4.Text = "Fornecedor";
            // 
            // txtLucroReal
            // 
            this.txtLucroReal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtLucroReal.Border.Class = "TextBoxBorder";
            this.txtLucroReal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtLucroReal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLucroReal.ForeColor = System.Drawing.Color.Black;
            this.txtLucroReal.Location = new System.Drawing.Point(207, 127);
            this.txtLucroReal.Name = "txtLucroReal";
            this.txtLucroReal.PreventEnterBeep = true;
            this.txtLucroReal.Size = new System.Drawing.Size(88, 25);
            this.txtLucroReal.TabIndex = 9;
            this.txtLucroReal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLucroReal.Leave += new System.EventHandler(this.txtLucroReal_Leave);
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Location = new System.Drawing.Point(207, 107);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(62, 23);
            this.labelX10.TabIndex = 95;
            this.labelX10.Text = "Lucro R$";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(9, 51);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(62, 23);
            this.labelX2.TabIndex = 74;
            this.labelX2.Text = "Und";
            // 
            // txtValorVenda
            // 
            this.txtValorVenda.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtValorVenda.Border.Class = "TextBoxBorder";
            this.txtValorVenda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorVenda.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorVenda.ForeColor = System.Drawing.Color.Black;
            this.txtValorVenda.Location = new System.Drawing.Point(306, 127);
            this.txtValorVenda.Name = "txtValorVenda";
            this.txtValorVenda.PreventEnterBeep = true;
            this.txtValorVenda.Size = new System.Drawing.Size(88, 25);
            this.txtValorVenda.TabIndex = 10;
            this.txtValorVenda.Text = "0,00";
            this.txtValorVenda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorVenda.Leave += new System.EventHandler(this.txtValorVenda_Leave);
            // 
            // txtProduto
            // 
            this.txtProduto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtProduto.Border.Class = "TextBoxBorder";
            this.txtProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProduto.ForeColor = System.Drawing.Color.Black;
            this.txtProduto.Location = new System.Drawing.Point(91, 25);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.PreventEnterBeep = true;
            this.txtProduto.Size = new System.Drawing.Size(493, 20);
            this.txtProduto.TabIndex = 1;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(306, 107);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(62, 23);
            this.labelX7.TabIndex = 94;
            this.labelX7.Text = "Valor Venda";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(91, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(135, 23);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "Descrição do Produto";
            // 
            // txtLucroPorcent
            // 
            this.txtLucroPorcent.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtLucroPorcent.Border.Class = "TextBoxBorder";
            this.txtLucroPorcent.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtLucroPorcent.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLucroPorcent.ForeColor = System.Drawing.Color.Black;
            this.txtLucroPorcent.Location = new System.Drawing.Point(108, 127);
            this.txtLucroPorcent.Name = "txtLucroPorcent";
            this.txtLucroPorcent.PreventEnterBeep = true;
            this.txtLucroPorcent.Size = new System.Drawing.Size(88, 25);
            this.txtLucroPorcent.TabIndex = 8;
            this.txtLucroPorcent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLucroPorcent.Leave += new System.EventHandler(this.txtLucroPorcent_Leave);
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(108, 107);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(62, 23);
            this.labelX6.TabIndex = 93;
            this.labelX6.Text = "Lucro %";
            // 
            // txtValorCusto
            // 
            this.txtValorCusto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtValorCusto.Border.Class = "TextBoxBorder";
            this.txtValorCusto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorCusto.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorCusto.ForeColor = System.Drawing.Color.Black;
            this.txtValorCusto.Location = new System.Drawing.Point(9, 127);
            this.txtValorCusto.Name = "txtValorCusto";
            this.txtValorCusto.PreventEnterBeep = true;
            this.txtValorCusto.Size = new System.Drawing.Size(88, 24);
            this.txtValorCusto.TabIndex = 7;
            this.txtValorCusto.Text = "0,00";
            this.txtValorCusto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorCusto.Leave += new System.EventHandler(this.txtValorCusto_Leave);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(9, 107);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(83, 23);
            this.labelX5.TabIndex = 92;
            this.labelX5.Text = "Valor Custo";
            // 
            // balloonTip1
            // 
            this.balloonTip1.Style = DevComponents.DotNetBar.eBallonStyle.Office2007Alert;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnFechar.Location = new System.Drawing.Point(486, 278);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(101, 34);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>  Fechar (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.salva;
            this.btnSalvar.Location = new System.Drawing.Point(325, 278);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(137, 34);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 2;
            this.btnSalvar.Text = "  Salvar (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.bntSalvar_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.rbEstoqueNao);
            this.groupPanel2.Controls.Add(this.rbEstoqueSim);
            this.groupPanel2.Location = new System.Drawing.Point(3, 261);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(140, 51);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "Controla Estoque ?";
            // 
            // rbEstoqueNao
            // 
            this.rbEstoqueNao.AutoSize = true;
            this.rbEstoqueNao.BackColor = System.Drawing.Color.Transparent;
            this.rbEstoqueNao.Checked = true;
            this.rbEstoqueNao.Location = new System.Drawing.Point(75, 7);
            this.rbEstoqueNao.Name = "rbEstoqueNao";
            this.rbEstoqueNao.Size = new System.Drawing.Size(45, 17);
            this.rbEstoqueNao.TabIndex = 1;
            this.rbEstoqueNao.TabStop = true;
            this.rbEstoqueNao.Text = "Não";
            this.rbEstoqueNao.UseVisualStyleBackColor = false;
            this.rbEstoqueNao.Click += new System.EventHandler(this.rbEstoqueNao_Click);
            // 
            // rbEstoqueSim
            // 
            this.rbEstoqueSim.AutoSize = true;
            this.rbEstoqueSim.BackColor = System.Drawing.Color.Transparent;
            this.rbEstoqueSim.Location = new System.Drawing.Point(15, 7);
            this.rbEstoqueSim.Name = "rbEstoqueSim";
            this.rbEstoqueSim.Size = new System.Drawing.Size(42, 17);
            this.rbEstoqueSim.TabIndex = 0;
            this.rbEstoqueSim.Text = "Sim";
            this.rbEstoqueSim.UseVisualStyleBackColor = false;
            this.rbEstoqueSim.Click += new System.EventHandler(this.rbEstoqueSim_Click);
            // 
            // frmCadProdutosExtras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(598, 320);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.panelProduto);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadProdutosExtras";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Complementos";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadProdutos_KeyUp);
            this.panelProduto.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private DevComponents.DotNetBar.PanelEx panelProduto;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbUnd;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigo;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbFornecedor;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProduto;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.BalloonTip balloonTip1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtQuantidade;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX txtEstoqueCritico;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX txtInformAdicional;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtLucroReal;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorVenda;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtLucroPorcent;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorCusto;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.RadioButton rbEstoqueNao;
        private System.Windows.Forms.RadioButton rbEstoqueSim;
    }
}