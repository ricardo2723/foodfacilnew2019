﻿namespace UI
{
    partial class frmCadProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadProdutos));
            this.panelManufatura = new DevComponents.DotNetBar.PanelEx();
            this.btnAddItemProduto = new DevComponents.DotNetBar.ButtonX();
            this.btnPesqProduto = new DevComponents.DotNetBar.ButtonX();
            this.btnExcluirItemProduto = new DevComponents.DotNetBar.ButtonX();
            this.txtProdutoManufatura = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCodManufatura = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.dgvItensManufatura = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idProdutoManifaturado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemProduto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdProdutoManufaturado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtQuantidadeManufatura = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.panelProduto = new DevComponents.DotNetBar.PanelEx();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rbManufaturaNao = new System.Windows.Forms.RadioButton();
            this.rbManufaturaSim = new System.Windows.Forms.RadioButton();
            this.txtQuantidade = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cbGrupo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.cbUnd = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtEstoqueCritico = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCodigo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtInformAdicional = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cbFornecedor = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtLucroReal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtValorVenda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtLucroPorcent = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtValorCusto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rbEstoqueNao = new System.Windows.Forms.RadioButton();
            this.rbEstoqueSim = new System.Windows.Forms.RadioButton();
            this.panelManufatura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItensManufatura)).BeginInit();
            this.panelProduto.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelManufatura
            // 
            this.panelManufatura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelManufatura.CanvasColor = System.Drawing.Color.Transparent;
            this.panelManufatura.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelManufatura.Controls.Add(this.btnAddItemProduto);
            this.panelManufatura.Controls.Add(this.btnPesqProduto);
            this.panelManufatura.Controls.Add(this.btnExcluirItemProduto);
            this.panelManufatura.Controls.Add(this.txtProdutoManufatura);
            this.panelManufatura.Controls.Add(this.txtCodManufatura);
            this.panelManufatura.Controls.Add(this.labelX16);
            this.panelManufatura.Controls.Add(this.dgvItensManufatura);
            this.panelManufatura.Controls.Add(this.txtQuantidadeManufatura);
            this.panelManufatura.Controls.Add(this.labelX13);
            this.panelManufatura.Controls.Add(this.labelX12);
            this.panelManufatura.Location = new System.Drawing.Point(9, 167);
            this.panelManufatura.Name = "panelManufatura";
            this.panelManufatura.Size = new System.Drawing.Size(575, 201);
            this.panelManufatura.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelManufatura.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelManufatura.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelManufatura.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelManufatura.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelManufatura.Style.GradientAngle = 90;
            this.panelManufatura.TabIndex = 6;
            // 
            // btnAddItemProduto
            // 
            this.btnAddItemProduto.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddItemProduto.BackColor = System.Drawing.Color.Transparent;
            this.btnAddItemProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.balloonTip1.SetBalloonCaption(this.btnAddItemProduto, "Adicionar");
            this.balloonTip1.SetBalloonText(this.btnAddItemProduto, "Adicionar Itens do Produto");
            this.btnAddItemProduto.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddItemProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItemProduto.HoverImage = global::UI.Properties.Resources._1394153974_add;
            this.btnAddItemProduto.Image = global::UI.Properties.Resources._1394153974_add;
            this.btnAddItemProduto.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnAddItemProduto.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAddItemProduto.Location = new System.Drawing.Point(541, 25);
            this.btnAddItemProduto.Name = "btnAddItemProduto";
            this.btnAddItemProduto.Size = new System.Drawing.Size(31, 31);
            this.btnAddItemProduto.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.btnAddItemProduto.TabIndex = 4;
            this.btnAddItemProduto.Click += new System.EventHandler(this.btnAddItemProduto_Click);
            // 
            // btnPesqProduto
            // 
            this.btnPesqProduto.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPesqProduto.BackColor = System.Drawing.Color.Transparent;
            this.btnPesqProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.balloonTip1.SetBalloonCaption(this.btnPesqProduto, "Pesquisar");
            this.balloonTip1.SetBalloonText(this.btnPesqProduto, "Pesquisar Codigo do Produto");
            this.btnPesqProduto.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnPesqProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesqProduto.HoverImage = global::UI.Properties.Resources._1394159835_search;
            this.btnPesqProduto.Image = global::UI.Properties.Resources._1394159835_search;
            this.btnPesqProduto.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnPesqProduto.Location = new System.Drawing.Point(68, 30);
            this.btnPesqProduto.Name = "btnPesqProduto";
            this.btnPesqProduto.Size = new System.Drawing.Size(25, 25);
            this.btnPesqProduto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPesqProduto.TabIndex = 1;
            this.btnPesqProduto.Click += new System.EventHandler(this.btnPesqProduto_Click);
            // 
            // btnExcluirItemProduto
            // 
            this.btnExcluirItemProduto.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExcluirItemProduto.BackColor = System.Drawing.Color.Transparent;
            this.btnExcluirItemProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.balloonTip1.SetBalloonCaption(this.btnExcluirItemProduto, "Excluir");
            this.balloonTip1.SetBalloonText(this.btnExcluirItemProduto, "Excluir os itens do Produto");
            this.btnExcluirItemProduto.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnExcluirItemProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluirItemProduto.HoverImage = global::UI.Properties.Resources.fecharPed_fw;
            this.btnExcluirItemProduto.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnExcluirItemProduto.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnExcluirItemProduto.Location = new System.Drawing.Point(272, 167);
            this.btnExcluirItemProduto.Name = "btnExcluirItemProduto";
            this.btnExcluirItemProduto.Size = new System.Drawing.Size(31, 31);
            this.btnExcluirItemProduto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExcluirItemProduto.TabIndex = 6;
            this.btnExcluirItemProduto.Click += new System.EventHandler(this.btnExcluirItemProduto_Click);
            // 
            // txtProdutoManufatura
            // 
            this.txtProdutoManufatura.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.txtProdutoManufatura.Border.Class = "TextBoxBorder";
            this.txtProdutoManufatura.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProdutoManufatura.ForeColor = System.Drawing.Color.Black;
            this.txtProdutoManufatura.Location = new System.Drawing.Point(104, 30);
            this.txtProdutoManufatura.Name = "txtProdutoManufatura";
            this.txtProdutoManufatura.PreventEnterBeep = true;
            this.txtProdutoManufatura.ReadOnly = true;
            this.txtProdutoManufatura.Size = new System.Drawing.Size(346, 20);
            this.txtProdutoManufatura.TabIndex = 2;
            // 
            // txtCodManufatura
            // 
            this.txtCodManufatura.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtCodManufatura.Border.Class = "TextBoxBorder";
            this.txtCodManufatura.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodManufatura.ForeColor = System.Drawing.Color.Black;
            this.txtCodManufatura.Location = new System.Drawing.Point(4, 30);
            this.txtCodManufatura.Name = "txtCodManufatura";
            this.txtCodManufatura.PreventEnterBeep = true;
            this.txtCodManufatura.Size = new System.Drawing.Size(63, 20);
            this.txtCodManufatura.TabIndex = 0;
            this.txtCodManufatura.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodManufatura.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodManufatura_KeyDown);
            // 
            // labelX16
            // 
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Location = new System.Drawing.Point(4, 9);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(49, 23);
            this.labelX16.TabIndex = 50;
            this.labelX16.Text = "Cod.";
            // 
            // dgvItensManufatura
            // 
            this.dgvItensManufatura.AllowUserToAddRows = false;
            this.dgvItensManufatura.AllowUserToDeleteRows = false;
            this.dgvItensManufatura.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Blue;
            this.dgvItensManufatura.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItensManufatura.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItensManufatura.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItensManufatura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvItensManufatura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idProdutoManifaturado,
            this.itemProduto,
            this.qtdProdutoManufaturado,
            this.Column1,
            this.Column2});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItensManufatura.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvItensManufatura.EnableHeadersVisualStyles = false;
            this.dgvItensManufatura.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.dgvItensManufatura.Location = new System.Drawing.Point(4, 56);
            this.dgvItensManufatura.MultiSelect = false;
            this.dgvItensManufatura.Name = "dgvItensManufatura";
            this.dgvItensManufatura.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItensManufatura.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvItensManufatura.RowHeadersVisible = false;
            this.dgvItensManufatura.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvItensManufatura.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItensManufatura.Size = new System.Drawing.Size(568, 106);
            this.dgvItensManufatura.TabIndex = 5;
            // 
            // idProdutoManifaturado
            // 
            this.idProdutoManifaturado.DataPropertyName = "codProdutoManufaturado";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.idProdutoManifaturado.DefaultCellStyle = dataGridViewCellStyle3;
            this.idProdutoManifaturado.HeaderText = "Código";
            this.idProdutoManifaturado.Name = "idProdutoManifaturado";
            this.idProdutoManifaturado.ReadOnly = true;
            this.idProdutoManifaturado.Width = 60;
            // 
            // itemProduto
            // 
            this.itemProduto.DataPropertyName = "descricaoProduto";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.itemProduto.DefaultCellStyle = dataGridViewCellStyle4;
            this.itemProduto.HeaderText = "Produto";
            this.itemProduto.Name = "itemProduto";
            this.itemProduto.ReadOnly = true;
            this.itemProduto.Width = 260;
            // 
            // qtdProdutoManufaturado
            // 
            this.qtdProdutoManufaturado.DataPropertyName = "quantidadeProdutoManufaturado";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.qtdProdutoManufaturado.DefaultCellStyle = dataGridViewCellStyle5;
            this.qtdProdutoManufaturado.HeaderText = "Quantidade";
            this.qtdProdutoManufaturado.Name = "qtdProdutoManufaturado";
            this.qtdProdutoManufaturado.ReadOnly = true;
            this.qtdProdutoManufaturado.Width = 80;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "valorCompraProduto";
            this.Column1.HeaderText = "Valor Custo";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 70;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "valorVendaProduto";
            this.Column2.HeaderText = "Valor Venda";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 80;
            // 
            // txtQuantidadeManufatura
            // 
            this.txtQuantidadeManufatura.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtQuantidadeManufatura.Border.Class = "TextBoxBorder";
            this.txtQuantidadeManufatura.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtQuantidadeManufatura.ForeColor = System.Drawing.Color.Black;
            this.txtQuantidadeManufatura.Location = new System.Drawing.Point(456, 30);
            this.txtQuantidadeManufatura.Name = "txtQuantidadeManufatura";
            this.txtQuantidadeManufatura.PreventEnterBeep = true;
            this.txtQuantidadeManufatura.Size = new System.Drawing.Size(79, 20);
            this.txtQuantidadeManufatura.TabIndex = 3;
            this.txtQuantidadeManufatura.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQuantidadeManufatura.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuantidadeManufatura_KeyDown);
            // 
            // labelX13
            // 
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Location = new System.Drawing.Point(456, 9);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(71, 23);
            this.labelX13.TabIndex = 47;
            this.labelX13.Text = "Quantidade";
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Location = new System.Drawing.Point(104, 9);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(94, 23);
            this.labelX12.TabIndex = 39;
            this.labelX12.Text = "Itens do Produto";
            // 
            // panelProduto
            // 
            this.panelProduto.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelProduto.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelProduto.Controls.Add(this.groupPanel1);
            this.panelProduto.Controls.Add(this.txtQuantidade);
            this.panelProduto.Controls.Add(this.cbGrupo);
            this.panelProduto.Controls.Add(this.labelX15);
            this.panelProduto.Controls.Add(this.cbUnd);
            this.panelProduto.Controls.Add(this.txtEstoqueCritico);
            this.panelProduto.Controls.Add(this.txtCodigo);
            this.panelProduto.Controls.Add(this.labelX14);
            this.panelProduto.Controls.Add(this.labelX8);
            this.panelProduto.Controls.Add(this.txtInformAdicional);
            this.panelProduto.Controls.Add(this.cbFornecedor);
            this.panelProduto.Controls.Add(this.labelX9);
            this.panelProduto.Controls.Add(this.labelX4);
            this.panelProduto.Controls.Add(this.txtLucroReal);
            this.panelProduto.Controls.Add(this.labelX3);
            this.panelProduto.Controls.Add(this.labelX10);
            this.panelProduto.Controls.Add(this.labelX2);
            this.panelProduto.Controls.Add(this.txtValorVenda);
            this.panelProduto.Controls.Add(this.txtProduto);
            this.panelProduto.Controls.Add(this.labelX7);
            this.panelProduto.Controls.Add(this.labelX1);
            this.panelProduto.Controls.Add(this.txtLucroPorcent);
            this.panelProduto.Controls.Add(this.panelManufatura);
            this.panelProduto.Controls.Add(this.labelX6);
            this.panelProduto.Controls.Add(this.txtValorCusto);
            this.panelProduto.Controls.Add(this.labelX5);
            this.panelProduto.Location = new System.Drawing.Point(3, 3);
            this.panelProduto.Name = "panelProduto";
            this.panelProduto.Size = new System.Drawing.Size(592, 517);
            this.panelProduto.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelProduto.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelProduto.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelProduto.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelProduto.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelProduto.Style.GradientAngle = 90;
            this.panelProduto.TabIndex = 0;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.rbManufaturaNao);
            this.groupPanel1.Controls.Add(this.rbManufaturaSim);
            this.groupPanel1.Location = new System.Drawing.Point(9, 101);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(575, 51);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 5;
            this.groupPanel1.Text = "Esse Produto e Formado Por outros Produtos ?";
            // 
            // rbManufaturaNao
            // 
            this.rbManufaturaNao.AutoSize = true;
            this.rbManufaturaNao.BackColor = System.Drawing.Color.Transparent;
            this.rbManufaturaNao.Checked = true;
            this.rbManufaturaNao.Location = new System.Drawing.Point(292, 7);
            this.rbManufaturaNao.Name = "rbManufaturaNao";
            this.rbManufaturaNao.Size = new System.Drawing.Size(45, 17);
            this.rbManufaturaNao.TabIndex = 1;
            this.rbManufaturaNao.TabStop = true;
            this.rbManufaturaNao.Text = "Não";
            this.rbManufaturaNao.UseVisualStyleBackColor = false;
            this.rbManufaturaNao.Click += new System.EventHandler(this.rbManufaturaNao_Click);
            // 
            // rbManufaturaSim
            // 
            this.rbManufaturaSim.AutoSize = true;
            this.rbManufaturaSim.BackColor = System.Drawing.Color.Transparent;
            this.rbManufaturaSim.Location = new System.Drawing.Point(232, 7);
            this.rbManufaturaSim.Name = "rbManufaturaSim";
            this.rbManufaturaSim.Size = new System.Drawing.Size(42, 17);
            this.rbManufaturaSim.TabIndex = 0;
            this.rbManufaturaSim.Text = "Sim";
            this.rbManufaturaSim.UseVisualStyleBackColor = false;
            this.rbManufaturaSim.Click += new System.EventHandler(this.rbManufaturaSim_Click);
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtQuantidade.Border.Class = "TextBoxBorder";
            this.txtQuantidade.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtQuantidade.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantidade.ForeColor = System.Drawing.Color.Black;
            this.txtQuantidade.Location = new System.Drawing.Point(503, 406);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.PreventEnterBeep = true;
            this.txtQuantidade.Size = new System.Drawing.Size(81, 25);
            this.txtQuantidade.TabIndex = 12;
            this.txtQuantidade.Text = "0";
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbGrupo
            // 
            this.cbGrupo.DisplayMember = "grupoDescricao";
            this.cbGrupo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGrupo.FormattingEnabled = true;
            this.cbGrupo.ItemHeight = 14;
            this.cbGrupo.Location = new System.Drawing.Point(77, 72);
            this.cbGrupo.Name = "cbGrupo";
            this.cbGrupo.Size = new System.Drawing.Size(230, 20);
            this.cbGrupo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbGrupo.TabIndex = 3;
            this.cbGrupo.ValueMember = "grupoId";
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Location = new System.Drawing.Point(503, 386);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(62, 23);
            this.labelX15.TabIndex = 98;
            this.labelX15.Text = "Qtd Estoque";
            // 
            // cbUnd
            // 
            this.cbUnd.DisplayMember = "undMedidaDescricao";
            this.cbUnd.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbUnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUnd.FormattingEnabled = true;
            this.cbUnd.ItemHeight = 14;
            this.cbUnd.Location = new System.Drawing.Point(9, 72);
            this.cbUnd.Name = "cbUnd";
            this.cbUnd.Size = new System.Drawing.Size(62, 20);
            this.cbUnd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbUnd.TabIndex = 2;
            this.cbUnd.ValueMember = "undMedidaId";
            // 
            // txtEstoqueCritico
            // 
            this.txtEstoqueCritico.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtEstoqueCritico.Border.Class = "TextBoxBorder";
            this.txtEstoqueCritico.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtEstoqueCritico.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstoqueCritico.ForeColor = System.Drawing.Color.Black;
            this.txtEstoqueCritico.Location = new System.Drawing.Point(405, 406);
            this.txtEstoqueCritico.Name = "txtEstoqueCritico";
            this.txtEstoqueCritico.PreventEnterBeep = true;
            this.txtEstoqueCritico.Size = new System.Drawing.Size(87, 25);
            this.txtEstoqueCritico.TabIndex = 11;
            this.txtEstoqueCritico.Text = "0";
            this.txtEstoqueCritico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEstoqueCritico.Leave += new System.EventHandler(this.txtEstoqueCritico_Leave);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtCodigo.Border.Class = "TextBoxBorder";
            this.txtCodigo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigo.ForeColor = System.Drawing.Color.Black;
            this.txtCodigo.Location = new System.Drawing.Point(9, 25);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PreventEnterBeep = true;
            this.txtCodigo.Size = new System.Drawing.Size(76, 20);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.Leave += new System.EventHandler(this.txtCodigo_Leave);
            // 
            // labelX14
            // 
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Location = new System.Drawing.Point(405, 386);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(80, 23);
            this.labelX14.TabIndex = 97;
            this.labelX14.Text = "Estoque Crítico";
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(9, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(76, 23);
            this.labelX8.TabIndex = 80;
            this.labelX8.Text = "Código";
            // 
            // txtInformAdicional
            // 
            this.txtInformAdicional.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtInformAdicional.Border.Class = "TextBoxBorder";
            this.txtInformAdicional.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtInformAdicional.ForeColor = System.Drawing.Color.Black;
            this.txtInformAdicional.Location = new System.Drawing.Point(9, 452);
            this.txtInformAdicional.Multiline = true;
            this.txtInformAdicional.Name = "txtInformAdicional";
            this.txtInformAdicional.PreventEnterBeep = true;
            this.txtInformAdicional.Size = new System.Drawing.Size(575, 52);
            this.txtInformAdicional.TabIndex = 13;
            // 
            // cbFornecedor
            // 
            this.cbFornecedor.DisplayMember = "nomeFornecedor";
            this.cbFornecedor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFornecedor.FormattingEnabled = true;
            this.cbFornecedor.ItemHeight = 14;
            this.cbFornecedor.Location = new System.Drawing.Point(313, 72);
            this.cbFornecedor.Name = "cbFornecedor";
            this.cbFornecedor.Size = new System.Drawing.Size(271, 20);
            this.cbFornecedor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbFornecedor.TabIndex = 4;
            this.cbFornecedor.ValueMember = "idFornecedor";
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(9, 432);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(117, 23);
            this.labelX9.TabIndex = 96;
            this.labelX9.Text = "Informações Adicionais";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(313, 51);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(62, 23);
            this.labelX4.TabIndex = 76;
            this.labelX4.Text = "Fornecedor";
            // 
            // txtLucroReal
            // 
            this.txtLucroReal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtLucroReal.Border.Class = "TextBoxBorder";
            this.txtLucroReal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtLucroReal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLucroReal.ForeColor = System.Drawing.Color.Black;
            this.txtLucroReal.Location = new System.Drawing.Point(207, 406);
            this.txtLucroReal.Name = "txtLucroReal";
            this.txtLucroReal.PreventEnterBeep = true;
            this.txtLucroReal.Size = new System.Drawing.Size(88, 25);
            this.txtLucroReal.TabIndex = 9;
            this.txtLucroReal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLucroReal.Leave += new System.EventHandler(this.txtLucroReal_Leave);
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(78, 51);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(62, 23);
            this.labelX3.TabIndex = 75;
            this.labelX3.Text = "Grupo";
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Location = new System.Drawing.Point(207, 386);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(62, 23);
            this.labelX10.TabIndex = 95;
            this.labelX10.Text = "Lucro R$";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(9, 51);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(62, 23);
            this.labelX2.TabIndex = 74;
            this.labelX2.Text = "Und";
            // 
            // txtValorVenda
            // 
            this.txtValorVenda.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtValorVenda.Border.Class = "TextBoxBorder";
            this.txtValorVenda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorVenda.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorVenda.ForeColor = System.Drawing.Color.Blue;
            this.txtValorVenda.Location = new System.Drawing.Point(306, 406);
            this.txtValorVenda.Name = "txtValorVenda";
            this.txtValorVenda.PreventEnterBeep = true;
            this.txtValorVenda.Size = new System.Drawing.Size(88, 25);
            this.txtValorVenda.TabIndex = 10;
            this.txtValorVenda.Text = "0,00";
            this.txtValorVenda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorVenda.Leave += new System.EventHandler(this.txtValorVenda_Leave);
            // 
            // txtProduto
            // 
            this.txtProduto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtProduto.Border.Class = "TextBoxBorder";
            this.txtProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProduto.ForeColor = System.Drawing.Color.Black;
            this.txtProduto.Location = new System.Drawing.Point(91, 25);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.PreventEnterBeep = true;
            this.txtProduto.Size = new System.Drawing.Size(493, 20);
            this.txtProduto.TabIndex = 1;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(306, 386);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(62, 23);
            this.labelX7.TabIndex = 94;
            this.labelX7.Text = "Valor Venda";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(91, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(135, 23);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "Descrição do Produto";
            // 
            // txtLucroPorcent
            // 
            this.txtLucroPorcent.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtLucroPorcent.Border.Class = "TextBoxBorder";
            this.txtLucroPorcent.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtLucroPorcent.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLucroPorcent.ForeColor = System.Drawing.Color.Black;
            this.txtLucroPorcent.Location = new System.Drawing.Point(108, 406);
            this.txtLucroPorcent.Name = "txtLucroPorcent";
            this.txtLucroPorcent.PreventEnterBeep = true;
            this.txtLucroPorcent.Size = new System.Drawing.Size(88, 25);
            this.txtLucroPorcent.TabIndex = 8;
            this.txtLucroPorcent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLucroPorcent.Leave += new System.EventHandler(this.txtLucroPorcent_Leave);
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(108, 386);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(62, 23);
            this.labelX6.TabIndex = 93;
            this.labelX6.Text = "Lucro %";
            // 
            // txtValorCusto
            // 
            this.txtValorCusto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtValorCusto.Border.Class = "TextBoxBorder";
            this.txtValorCusto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorCusto.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorCusto.ForeColor = System.Drawing.Color.Red;
            this.txtValorCusto.Location = new System.Drawing.Point(9, 406);
            this.txtValorCusto.Name = "txtValorCusto";
            this.txtValorCusto.PreventEnterBeep = true;
            this.txtValorCusto.Size = new System.Drawing.Size(88, 24);
            this.txtValorCusto.TabIndex = 7;
            this.txtValorCusto.Text = "0,00";
            this.txtValorCusto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorCusto.Leave += new System.EventHandler(this.txtValorCusto_Leave);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(9, 386);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(83, 23);
            this.labelX5.TabIndex = 92;
            this.labelX5.Text = "Valor Custo";
            // 
            // balloonTip1
            // 
            this.balloonTip1.Style = DevComponents.DotNetBar.eBallonStyle.Office2007Alert;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnFechar.Location = new System.Drawing.Point(490, 543);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(101, 34);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>  Fechar (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.salva;
            this.btnSalvar.Location = new System.Drawing.Point(329, 543);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(137, 34);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 2;
            this.btnSalvar.Text = "  Salvar (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.bntSalvar_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.rbEstoqueNao);
            this.groupPanel2.Controls.Add(this.rbEstoqueSim);
            this.groupPanel2.Location = new System.Drawing.Point(7, 526);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(140, 51);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "Controla Estoque ?";
            // 
            // rbEstoqueNao
            // 
            this.rbEstoqueNao.AutoSize = true;
            this.rbEstoqueNao.BackColor = System.Drawing.Color.Transparent;
            this.rbEstoqueNao.Location = new System.Drawing.Point(75, 7);
            this.rbEstoqueNao.Name = "rbEstoqueNao";
            this.rbEstoqueNao.Size = new System.Drawing.Size(45, 17);
            this.rbEstoqueNao.TabIndex = 1;
            this.rbEstoqueNao.Text = "Não";
            this.rbEstoqueNao.UseVisualStyleBackColor = false;
            this.rbEstoqueNao.Click += new System.EventHandler(this.rbEstoqueNao_Click);
            // 
            // rbEstoqueSim
            // 
            this.rbEstoqueSim.AutoSize = true;
            this.rbEstoqueSim.BackColor = System.Drawing.Color.Transparent;
            this.rbEstoqueSim.Checked = true;
            this.rbEstoqueSim.Location = new System.Drawing.Point(15, 7);
            this.rbEstoqueSim.Name = "rbEstoqueSim";
            this.rbEstoqueSim.Size = new System.Drawing.Size(42, 17);
            this.rbEstoqueSim.TabIndex = 0;
            this.rbEstoqueSim.TabStop = true;
            this.rbEstoqueSim.Text = "Sim";
            this.rbEstoqueSim.UseVisualStyleBackColor = false;
            this.rbEstoqueSim.Click += new System.EventHandler(this.rbEstoqueSim_Click);
            // 
            // frmCadProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(598, 584);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.panelProduto);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Produtos";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadProdutos_KeyUp);
            this.panelManufatura.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItensManufatura)).EndInit();
            this.panelProduto.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private DevComponents.DotNetBar.PanelEx panelManufatura;
        private DevComponents.DotNetBar.ButtonX btnExcluirItemProduto;
        private DevComponents.DotNetBar.PanelEx panelProduto;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbGrupo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbUnd;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigo;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbFornecedor;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProduto;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.BalloonTip balloonTip1;
        private DevComponents.DotNetBar.ButtonX btnAddItemProduto;
        private DevComponents.DotNetBar.ButtonX btnPesqProduto;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProdutoManufatura;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodManufatura;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvItensManufatura;
        private DevComponents.DotNetBar.Controls.TextBoxX txtQuantidadeManufatura;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.RadioButton rbManufaturaNao;
        private System.Windows.Forms.RadioButton rbManufaturaSim;
        private DevComponents.DotNetBar.Controls.TextBoxX txtQuantidade;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX txtEstoqueCritico;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX txtInformAdicional;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtLucroReal;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorVenda;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtLucroPorcent;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorCusto;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.RadioButton rbEstoqueNao;
        private System.Windows.Forms.RadioButton rbEstoqueSim;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProdutoManifaturado;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemProduto;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdProdutoManufaturado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}