﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadSaidaProdutos : Form
    {
        public decimal quantidadeEstoque;
        
        SaidaProdutos saidaProduto = new SaidaProdutos();

        public frmCadSaidaProdutos()
        {
            InitializeComponent();          
        }

        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            inserirProduto();            
        }

        private void btnPesquisaProduto_Click(object sender, EventArgs e)
        {
            pesqProduto(txtCodigo);
        }

        #endregion

        #region AÇÕES

        private void frmCadSaidaProdutos_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    this.btnPesquisaProduto.PerformClick();
                    break;
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    getProdutoCod();
                    break;
            }
        }

        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtCodigo.Text == "" || txtCodigo.Text == "0")
                    return;

                Convert.ToInt32(txtCodigo.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "SAIDA")
                {
                    btnSalvar.Enabled = item.cadastrar;                    
                }
            }
        }

        private void inserirProduto()
        {
            if (Convert.ToDecimal(txtQuantidade.Text) == 0)
            {
                MessageBox.Show("É necessário um valor maior que zero no campo Quantidade", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtQuantidade.Focus();
                return;
            }

            if
               (!String.IsNullOrEmpty(txtCodigo.Text) &&
               !String.IsNullOrEmpty(txtProduto.Text) &&
               !String.IsNullOrEmpty(txtMotivoSaida.Text) &&
               !String.IsNullOrEmpty(txtQuantidade.Text))
            {
                saidaProduto.codigoProduto = Generic.checaInteiro(txtCodigo.Text);
                saidaProduto.descricaoProduto = txtProduto.Text;

                if(quantidadeEstoque < Convert.ToDecimal(txtQuantidade.Text))
                {
                    MessageBox.Show("O valor de Saida e maior que o valor em Estoque", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    saidaProduto.estoqueProduto = quantidadeEstoque - Convert.ToDecimal(txtQuantidade.Text);
                }
                
                saidaProduto.motivoSaida = txtMotivoSaida.Text.ToUpper();
                saidaProduto.quantidadeProdutoSaida = Convert.ToDecimal(txtQuantidade.Text);               
                saidaProduto.dataCadSaida = DateTime.Now.Date;
                saidaProduto.usuarioId = frmLogin.usuariosLogin.usuarioId;

                SaidaProdutosNegocios saidaProdutosNegocios = new SaidaProdutosNegocios();
                string retorno = saidaProdutosNegocios.inserir(saidaProduto);

                try
                {
                    int checaRetorno = Convert.ToInt32(retorno);

                    MessageBox.Show("Saida do Produto executada com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Generic.limparCampos(panelProduto);
                    txtCodigo.Focus();
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                if (String.IsNullOrEmpty(txtCodigo.Text))
                {
                    MessageBox.Show("Campo Código é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

                else if (String.IsNullOrEmpty(txtProduto.Text))
                {
                    MessageBox.Show("Campo Descrição do Produto é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProduto.Focus();
                }
                
                else if (String.IsNullOrEmpty(txtQuantidade.Text))
                {
                    MessageBox.Show("Campo Quantidade é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQuantidade.Focus();
                }

                else if (String.IsNullOrEmpty(txtMotivoSaida.Text))
                {
                    MessageBox.Show("Campo Motivo Saida é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMotivoSaida.Focus();
                }
            }
        }

        private void getProdutoCod()
        {
            saidaProduto.codigoProduto = Generic.checaInteiro(txtCodigo.Text);

            if (saidaProduto.codigoProduto == 0)
                return;

            SaidaProdutosNegocios saidaProdutosNegocios = new SaidaProdutosNegocios();
            SaidaProdutosCollections saidaProdutosCollections = saidaProdutosNegocios.GetProduto(saidaProduto);

            if (saidaProdutosCollections.Count > 0)
            {
                foreach (var itemLista in saidaProdutosCollections)
                {
                    saidaProduto.descricaoProduto = itemLista.descricaoProduto;
                    saidaProduto.estoqueProduto = itemLista.estoqueProduto;

                    txtProduto.Text = saidaProduto.descricaoProduto.ToUpper();
                    quantidadeEstoque = saidaProduto.estoqueProduto;

                    txtQuantidade.Focus();
                }
            }
            else
            {
                MessageBox.Show("Código do Produto não cadastro. Digite um Código Valido", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        public void pesqProduto(TextBox txt)
        {
            frmPesqProdutosFront objPesqProduto = new frmPesqProdutosFront(0);
            DialogResult resultado = objPesqProduto.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                txt.Text = objPesqProduto.codigoProduto.ToString();
                txt.Focus();
            }
            else
            {
                txt.Text = null;
            }
        }

        #endregion

       

    }
}
