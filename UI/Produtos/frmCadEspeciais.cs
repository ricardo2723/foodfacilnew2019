﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadEspeciais : Form
    {
        AcaoCRUD acaoSelecionada;
        GruposNegocios gruposNegocios = new GruposNegocios();
        Especiais especiais = new Especiais();

        public frmCadEspeciais(AcaoCRUD acao, Especiais especiais)
        {
            InitializeComponent();

            acaoSelecionada = acao;
            this.especiais = especiais;
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirEspeciais();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarEspeciais();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadEspeciais_Load(object sender, EventArgs e)
        {
            bindGrupo.DataSource = gruposNegocios.PreencheCbGrupos();

            if (acaoSelecionada == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Especiais";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acaoSelecionada == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Especiais";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = especiais.especiaisId.ToString();
                txtDescricao.Text = especiais.especiaisDescricao;
                cbGrupo.SelectedValue = especiais.grupoId;
            }
        }

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirEspeciais()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                Especiais especiais = new Especiais();

                especiais.usuarioId = frmLogin.usuariosLogin.usuarioId;
                especiais.grupoId = (bindGrupo.Current as Grupos).grupoId;
                especiais.especiaisDescricao = txtDescricao.Text;
                especiais.especiaisDataCad = DateTime.Now;

                EspeciaisNegocios especiaisNegocios = new EspeciaisNegocios();
                string retorno = especiaisNegocios.inserir(especiais);

                try
                {
                    int idEspeciais = Convert.ToInt32(retorno);
                    MessageBox.Show("Especial Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }

            }
        }

        private void atualizarEspeciais()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                Especiais especiais = new Especiais();

                especiais.especiaisId = Convert.ToInt32(txtCodigo.Text);
                especiais.grupoId = (bindGrupo.Current as Grupos).grupoId;
                especiais.usuarioId = frmLogin.usuariosLogin.usuarioId;
                especiais.especiaisDescricao = txtDescricao.Text;
                especiais.especiaisDataCad = DateTime.Now;

                EspeciaisNegocios especiaisNegocios = new EspeciaisNegocios();
                string retorno = especiaisNegocios.Alterar(especiais);

                try
                {
                    int idEspeciais = Convert.ToInt32(retorno);
                    MessageBox.Show("Especial Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }
                                
            }
        }

        #endregion

        
    }
}
