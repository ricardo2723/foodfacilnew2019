﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI;
using Relatorios;
using System.Diagnostics;

namespace UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Process aProcess = Process.GetCurrentProcess();
            string aProcName = aProcess.ProcessName;

            if (Process.GetProcessesByName(aProcName).Length > 1)
            {
                MessageBox.Show("O programa já está em execução!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            frmSplash objSplash = new frmSplash();
            objSplash.ShowDialog();

            frmLogin objLogin = new frmLogin();

            if (objLogin.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new Principal());
                return;
            }
            else
            {
                Application.Exit();
                return;
            }

            //Application.Run(new frmCadPermisaoUsuarios());  
              
        }
    }
}
