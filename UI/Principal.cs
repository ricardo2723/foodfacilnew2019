﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Diagnostics;
using System.IO;

using Negocios;
using ObjetoTransferencia;
using Relatorios;
using UI.Pedidos;

namespace UI
{
    public partial class Principal : DevComponents.DotNetBar.RibbonForm
    {
        bool checaDemostrativos = false;
        TipoImpressaoNegocios trocaTipoImpressao = new TipoImpressaoNegocios();
        public static int caixa = 0;
        public static string tipoAutoPrint;

        CaixaNegocios caixaNegocios = new CaixaNegocios();
        
        public Principal()
        {
            InitializeComponent();

            tsslUsuarioNome.Text = " Usuário: " + frmLogin.usuariosLogin.usuarioLogin;

            pegaIp();

            tsslDataAtual.Text = " Data: " + DateTime.Now.ToShortDateString();

            CaixaCollections caixaCollections = caixaNegocios.Consultar();

            foreach (var item in caixaCollections)
            {
                caixa = item.idCaixa;
            }
        }

        #region AÇÕES

        private void Principal_Load(object sender, EventArgs e)
        {
            pbLogoPrincipal.ImageLocation = frmLogin.meuLanche.localLogoMeuLanche;

            checaPermissaoUsuario();
            checaImpressao();
        }

        private void Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmBackup objBackup = new frmBackup();
            objBackup.ShowDialog();
        }

        private void ckbImpressaoPapel_Click(object sender, EventArgs e)
        {
            if (ckbImpressaoPapel.Checked)
            {
                ckbImpressaoTela.Checked = false;
            }

            ckbImpressaoPapel.Checked = true;
            
            trocaTipoImpressao.trocar(0);

            frmSplash.tipoImpressao = 0;

        }

        private void ckbImpressaoTela_Click(object sender, EventArgs e)
        {
            if (ckbImpressaoTela.Checked)
            {
                ckbImpressaoPapel.Checked = false;
            }

            ckbImpressaoTela.Checked = true;

            trocaTipoImpressao.trocar(1);

            frmSplash.tipoImpressao = 1;
        }

        private void checaImpressao()
        {
            if (frmSplash.tipoImpressao == 0)
            {
                ckbImpressaoPapel.Checked = true;
                ckbImpressaoTela.Checked = false;
            }
            else if (frmSplash.tipoImpressao == 1)
            {
                ckbImpressaoTela.Checked = true;
                ckbImpressaoPapel.Checked = false;
            }
            else
            {
                MessageBox.Show("Error ao Selecionar Tipo de Impressão", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.Exit();
            }
        }
        #endregion

        #region BOTÕES

        #region CADASTROS

        private void btnPermissaoUsuarios_Click(object sender, EventArgs e)
        {
            frmCadPermisaoUsuarios objPermissaoUsuarios = new frmCadPermisaoUsuarios();
            objPermissaoUsuarios.ShowDialog();
        }

        private void btnNivelAcesso_Click(object sender, EventArgs e)
        {
            frmCadNivelAcessoFront objNivelAcesso = new frmCadNivelAcessoFront();
            objNivelAcesso.ShowDialog();
        }

        private void btnCadClientes_Click(object sender, EventArgs e)
        {
            frmCadClienteFront objCadCliente = new frmCadClienteFront();
            objCadCliente.ShowDialog();
        }

        private void btnCadUsuarios_Click(object sender, EventArgs e)
        {
            frmCadUsuariosFront objCadUsuarios = new frmCadUsuariosFront();
            objCadUsuarios.ShowDialog();
        }

        private void BtnCadFornecedores_Click(object sender, EventArgs e)
        {
            frmCadFornecedoresFront objCadFornecedores = new frmCadFornecedoresFront();
            objCadFornecedores.ShowDialog();
        }

        private void btnEntragador_Click(object sender, EventArgs e)
        {
            frmCadEntregadorFront objEntregador = new frmCadEntregadorFront();
            objEntregador.ShowDialog();
        }

        private void btnCadProdutos_Click(object sender, EventArgs e)
        {
            frmCadProdutosFront objCadProdutos = new frmCadProdutosFront();
            objCadProdutos.ShowDialog();
        }

        private void btnComplementoProdutos_Click(object sender, EventArgs e)
        {
            frmCadProdutosFrontExtras objCadProdutosExtras = new frmCadProdutosFrontExtras();
            objCadProdutosExtras.ShowDialog();
        }

        private void btnCadGrupos_Click(object sender, EventArgs e)
        {
            frmCadGruposFront objCadGrupo = new frmCadGruposFront();
            objCadGrupo.ShowDialog();
        }

        private void btnCadUnidadeMedida_Click(object sender, EventArgs e)
        {
            frmCadUnidadeMedidaFront objCadUnidadeMedida = new frmCadUnidadeMedidaFront();
            objCadUnidadeMedida.ShowDialog();
        }

        private void btnEspecial_Click(object sender, EventArgs e)
        {
            frmCadEspeciasFront objCadEspeciais = new frmCadEspeciasFront();
            objCadEspeciais.ShowDialog();
        }

        private void btnEntradaProduto_Click(object sender, EventArgs e)
        {
            frmCadEntradaProdutos objEntrada = new frmCadEntradaProdutos();
            objEntrada.ShowDialog();
        }

        private void btnBairro_Click(object sender, EventArgs e)
        {
            frmCadBairroFront objBairro = new frmCadBairroFront();
            objBairro.ShowDialog();
        }

        private void btnCalculadora_Click(object sender, EventArgs e)
        {
            Process.Start("calc.exe");
        }

        #endregion

        #region FINANCEIRO

        private void btnPlanoConta_Click(object sender, EventArgs e)
        {
            frmCadPlanoContaFront objCadPlanoConta = new frmCadPlanoContaFront();
            objCadPlanoConta.ShowDialog();
        }

        private void btnCadCartoes_Click(object sender, EventArgs e)
        {
            frmCadCartaoFront objCadCartao = new frmCadCartaoFront();
            objCadCartao.ShowDialog();
        }

        private void btnFormaPagamento_Click(object sender, EventArgs e)
        {
            frmCadFormaPagamentoFront objFormaPagamento = new frmCadFormaPagamentoFront();
            objFormaPagamento.ShowDialog();
        }

        private void btnContasPagar_Click(object sender, EventArgs e)
        {
            frmCadContasPagarFront objContaPagar = new frmCadContasPagarFront();
            objContaPagar.ShowDialog();
        }

        private void btnRetiradas_Click(object sender, EventArgs e)
        {
            frmCadRetiradas objRetiradas = new frmCadRetiradas();
            objRetiradas.ShowDialog();
        }

        #endregion

        #region MOVIMENTOS

        private void btnSaidaProduto_Click(object sender, EventArgs e)
        {
            frmCadSaidaProdutos objSaida = new frmCadSaidaProdutos();
            objSaida.ShowDialog();
        }

        private void btnPdv_Click(object sender, EventArgs e)
        {
            FrmPedido pedido = new FrmPedido();
            pedido.ShowDialog();

            //frmPdv pdv = new frmPdv();
            //pdv.ShowDialog();
            //checaPDV();
        }

        private void btnCaixa_Click(object sender, EventArgs e)
        {
            checaCaixa();
        }

        private void btnImprimirPedidos_Click(object sender, EventArgs e)
        {
            frmReemprimirPedidos objReimprimePedidos = new frmReemprimirPedidos();
            objReimprimePedidos.ShowDialog();
        }

        private void btnTempoPedidos_Click(object sender, EventArgs e)
        {
            frmTempoPedido objFrmTempoPedido = new frmTempoPedido();
            objFrmTempoPedido.ShowDialog();
        }

        private void btnDelivery_Click(object sender, EventArgs e)
        {
            checaDelivery();
        }

        private void btnStatusDelivery_Click(object sender, EventArgs e)
        {
            frmEntregaDelivery objEntregaDelivery = new frmEntregaDelivery();
            objEntregaDelivery.ShowDialog();
        }

        #endregion

        #region BOTÕES MENU ITEM

        private void btnISair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMenuBackup_Click(object sender, EventArgs e)
        {
            frmBackup objBackup = new frmBackup();
            objBackup.ShowDialog();
        }

        private void btnCadMeuLanche_Click(object sender, EventArgs e)
        {
            frmMeuLanche ObjMeuLanche = new frmMeuLanche();
            if (ObjMeuLanche.ShowDialog() == DialogResult.Yes)
            {
                pbLogoPrincipal.ImageLocation = ObjMeuLanche.path;
            }
        }

        private void btnCadImpressao_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region BTN RELATÓRIOS

        private void btnRltClientes_Click(object sender, EventArgs e)
        {
            frmRltPessoas objRltPessoas = new frmRltPessoas(relatorioPessoas.Clientes, frmLogin.meuLanche);
            objRltPessoas.ShowDialog();
        }

        private void btnRltUsuarios_Click(object sender, EventArgs e)
        {
            frmRltPessoas objRltPessoas = new frmRltPessoas(relatorioPessoas.Usuarios, frmLogin.meuLanche);
            objRltPessoas.ShowDialog();
        }

        private void btnRltFornecedores_Click(object sender, EventArgs e)
        {
            frmRltPessoas objRltPessoas = new frmRltPessoas(relatorioPessoas.Fornecedores, frmLogin.meuLanche);
            objRltPessoas.ShowDialog();
        }

        private void btnRltEntregadores_Click(object sender, EventArgs e)
        {
            frmRltPessoas objRltPessoas = new frmRltPessoas(relatorioPessoas.EntregadoresGeral, frmLogin.meuLanche);
            objRltPessoas.ShowDialog();
        }

        private void btnRltEntregadorAtivo_Click(object sender, EventArgs e)
        {
            frmRltPessoas objRltPessoas = new frmRltPessoas(relatorioPessoas.EntregadoresAtivos, frmLogin.meuLanche, 1);
            objRltPessoas.ShowDialog();
        }

        private void btnRltEntregadoresDesligados_Click(object sender, EventArgs e)
        {
            frmRltPessoas objRltPessoas = new frmRltPessoas(relatorioPessoas.EntregadoresDesligados, frmLogin.meuLanche, 0);
            objRltPessoas.ShowDialog();
        }

        private void btnRltProdutosGeral_Click(object sender, EventArgs e)
        {
            frmRltProdutos objRltProdutosGeral = new frmRltProdutos(relatorioProdutos.ProdutoGeral, frmLogin.meuLanche);
            objRltProdutosGeral.ShowDialog();
        }

        private void btnRltProdutoEstoMinimo_Click(object sender, EventArgs e)
        {
            frmRltProdutos objRltProdutosGeral = new frmRltProdutos(relatorioProdutos.ProdutoMinimo, frmLogin.meuLanche);
            objRltProdutosGeral.ShowDialog();
        }

        private void btnRltProdutoEstoMinGrupo_Click(object sender, EventArgs e)
        {
            frmRltProdutoPorGrupo objRltProdMinGrupo = new frmRltProdutoPorGrupo(frmLogin.meuLanche, 1);
            objRltProdMinGrupo.ShowDialog();
        }

        private void btnRltProdutosMaisVendido_Click(object sender, EventArgs e)
        {
            frmRltProdutosMais objProdutosMais = new frmRltProdutosMais(frmLogin.meuLanche);
            objProdutosMais.ShowDialog();
        }

        private void btnRltProdutosGrupos_Click(object sender, EventArgs e)
        {
            frmRltProdutoPorGrupo objProdutoGrupo = new frmRltProdutoPorGrupo(frmLogin.meuLanche);
            objProdutoGrupo.ShowDialog();
        }

        private void btnRltProdutosEntrada_Click(object sender, EventArgs e)
        {
            frmRltProdutosEntradaSaida objEntradas = new frmRltProdutosEntradaSaida(1, frmLogin.meuLanche);
            objEntradas.ShowDialog();
        }

        private void btnRltProdutosSaida_Click(object sender, EventArgs e)
        {
            frmRltProdutosEntradaSaida objEntradas = new frmRltProdutosEntradaSaida(2, frmLogin.meuLanche);
            objEntradas.ShowDialog();
        }

        private void btnRltVendas_Click(object sender, EventArgs e)
        {
            frmRltVendaPrincipal objVendas = new frmRltVendaPrincipal(frmLogin.meuLanche);
            objVendas.ShowDialog();
        }

        private void btnRltDelivery_Click(object sender, EventArgs e)
        {
            frmRltDeliveryPrincipal objDelivery = new frmRltDeliveryPrincipal(frmLogin.meuLanche);
            objDelivery.ShowDialog();
        }

        private void btnRltRetiradas_Click(object sender, EventArgs e)
        {
            frmRltRetiradasPrincipal objRetiradas = new frmRltRetiradasPrincipal(frmLogin.meuLanche);
            objRetiradas.ShowDialog();
        }

        private void btnRltContas_Click(object sender, EventArgs e)
        {
            frmRltContasPrincipal objRltContas = new frmRltContasPrincipal(frmLogin.meuLanche);
            objRltContas.ShowDialog();
        }

        #endregion

        #region GRÁFICOS

        private void btnGrafVendasDiarias_Click(object sender, EventArgs e)
        {
            frmGrafVendasPrincipal objGrafVenda = new frmGrafVendasPrincipal(GraficosVendas.Diaria);
            objGrafVenda.ShowDialog();
        }

        private void btnGrafVendasMensal_Click(object sender, EventArgs e)
        {
            frmGrafVendasPrincipal objGrafVenda = new frmGrafVendasPrincipal(GraficosVendas.Mensal);
            objGrafVenda.ShowDialog();
        }

        private void btnGrafInlocDelivery_Click(object sender, EventArgs e)
        {
            frmGrafVendasPrincipal objGrafVenda = new frmGrafVendasPrincipal(GraficosVendas.InLocDelivery);
            objGrafVenda.ShowDialog();
        }

        private void btnGrafCanceladas_Click(object sender, EventArgs e)
        {
            frmGrafVendasPrincipal objGrafVenda = new frmGrafVendasPrincipal(GraficosVendas.Canceladas);
            objGrafVenda.ShowDialog();
        }

        private void btnGrafEntregasDiarias_Click(object sender, EventArgs e)
        {
            frmGrafEntregasPrincipal objGrafEntregas = new frmGrafEntregasPrincipal(GraficosEntregas.Diaria);
            objGrafEntregas.ShowDialog();
        }

        private void btnGrafEntregasMensal_Click(object sender, EventArgs e)
        {
            frmGrafEntregasPrincipal objGrafEntregas = new frmGrafEntregasPrincipal(GraficosEntregas.Mensal);
            objGrafEntregas.ShowDialog();
        }

        private void btnGrafEntregasEntregador_Click(object sender, EventArgs e)
        {
            frmGrafEntregasPrincipal objGrafEntregas = new frmGrafEntregasPrincipal(GraficosEntregas.Entregador);
            objGrafEntregas.ShowDialog();
        }

        private void btnGrafEntregasTempo_Click(object sender, EventArgs e)
        {
            frmGrafEntregasPrincipal objGrafEntregas = new frmGrafEntregasPrincipal(GraficosEntregas.Tempo);
            objGrafEntregas.ShowDialog();
        }

        #endregion

        #region BOTÕES APARENCIA

        private void btnMetro_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
        }

        private void btnOffice2010Black_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Black;
        }

        private void btnOffice2010Blue_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
        }

        private void btnOffice2010Silver_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Silver;
        }

        private void btnOffice2007Black_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Black;
        }

        private void btnOffice2007Blue_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Blue;
        }

        private void btnOffice2007Silver_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Silver;
        }

        private void btnVistaClass_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007VistaGlass;
        }

        private void btnVisualStudio2010_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2010Blue;
        }

        private void btnVisualStudio2012Ligth_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2012Light;
        }

        private void btnVisualStudio2012Black_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2012Dark;
        }

        private void btnWindowsBlue_Click(object sender, EventArgs e)
        {
            stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue;
        }

        #endregion

        #region BOTÕES METRO STYLE

        private void btnBlackClouds_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.BlackClouds;
        }

        private void btnBlackLilac_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.BlackLilac;
        }

        private void btnBlackSky_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.BlackSky;
        }

        private void btnBlue_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Blue;
        }

        private void btnBlueBrown_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.BlueishBrown;
        }

        private void btnBorderAux_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Bordeaux;
        }

        private void btnChery_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Cherry;
        }

        private void btnDarkBrown_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.DarkBrown;
        }

        private void btnDarkPurple_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.DarkPurple;
        }

        private void btnDarkRed_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.DarkRed;
        }

        private void btnEarlyMaroon_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.EarlyMaroon;
        }

        private void btnEarlyRed_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.EarlyRed;
        }

        private void btnExpresso_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Espresso;
        }

        private void btnFlorestGreen_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.ForestGreen;
        }

        private void btnGrayOrange_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.GrayOrange;
        }

        private void btnGreen_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Green;
        }

        private void btnLatter_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Latte;
        }

        private void btnMaroonSilver_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.MaroonSilver;
        }

        private void btnOrange_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Orange;
        }

        private void btnPowerRed_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.PowderRed;
        }

        private void btnPurple_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Purple;
        }

        private void btnRed_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Red;
        }

        private void btnRedAmplifield_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.RedAmplified;
        }

        private void btnRust_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.Rust;
        }

        private void btnSilverBlues_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.SilverBlues;
        }

        private void btnSilverGreen_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.SilverGreen;
        }

        private void btnSkyGreen_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.SkyGreen;
        }

        private void btnViStudioDark_Click(object sender, EventArgs e)
        {
            stiloPrincipal.MetroColorParameters = DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters.VisualStudio2012Dark;
        }

        #endregion

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                switch (item.buton)
                {
                    case "CLIENTES":
                        btnCadClientes.Enabled = item.ativo;
                        break;
                    case "USUARIOS":
                        btnCadUsuarios.Enabled = item.ativo;
                        break;
                    case "FORNECEDORES":
                        BtnCadFornecedores.Enabled = item.ativo;
                        break;
                    case "ENTREGADORES":
                        btnEntragador.Enabled = item.ativo;
                        break;
                    case "PRODUTOS":
                        btnCadProdutos.Enabled = item.ativo;
                        break;
                    case "GRUPOS":
                        btnCadGrupos.Enabled = item.ativo;
                        break;
                    case "ESPECIAIS":
                        btnEspecial.Enabled = item.ativo;
                        break;
                    case "BAIRRO":
                        btnBairro.Enabled = item.ativo;
                        break;
                    case "UNIDADE":
                        btnCadUnidadeMedida.Enabled = item.ativo;
                        break;
                    case "ENTRADA":
                        btnEntradaProduto.Enabled = item.ativo;
                        break;
                    case "SAIDA":
                        btnSaidaProduto.Enabled = item.ativo;
                        break;
                    case "PDV":
                        btnPdv.Enabled = item.ativo;
                        break;
                    case "DELIVERY":
                        btnDelivery.Enabled = item.ativo;
                        break;
                    case "ENTREGAS":
                        btnStatusDelivery.Enabled = item.ativo;
                        break;
                    case "PEDIDOS":
                        btnImprimirPedidos.Enabled = item.ativo;
                        break;
                    case "TEMPO PEDIDO":
                        btnTempoPedidos.Enabled = item.ativo;
                        break;
                    case "CAIXA":
                        btnCaixa.Enabled = item.ativo;
                        checaDemostrativos = item.financeiro;
                        break;
                    case "PERMISSAO USUARIOS":
                        btnPermissaoUsuarios.Enabled = item.ativo;
                        break;
                    case "NIVEL ACESSO":
                        btnNivelAcesso.Enabled = item.ativo;
                        break;
                    case "CARTOES":
                        btnCadCartoes.Enabled = item.ativo;
                        break;
                    case "PLANO CONTA":
                        btnPlanoConta.Enabled = item.ativo;
                        break;
                    case "FORMA PAGAMENTO":
                        btnFormaPagamento.Enabled = item.ativo;
                        break;
                    case "CONTAS PAGAR":
                        btnContasPagar.Enabled = item.ativo;
                        break;
                    case "RETIRADAS":
                        btnRetiradas.Enabled = item.ativo;
                        break;
                    case "RELATORIOS":
                        rtRelatorios.Enabled = item.ativo;
                        break;
                    case "GRAFICOS":
                        rtGraficos.Enabled = item.ativo;
                        break;
                    case "CONFIGURAÇOES":
                        btnCadLancheFacil.Enabled = item.ativo;
                        break;
                }
            }
        }

        public void checaCaixa()
        {
            CaixaNegocios caixaNegocios = new CaixaNegocios();

            string retorno = caixaNegocios.checaCaixa();

            if (retorno == "1")
            {
                frmCaixa objCaixa = new frmCaixa();
                objCaixa.ShowDialog();
            }
            else if (retorno == "0")
            {
                if (checaDemostrativos)
                {
                    frmCaixaDemostrativo objCaixaDemostrativo = new frmCaixaDemostrativo();
                    objCaixaDemostrativo.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Usuário não possui Permissão de Acesso", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                
            }
        }

        public void checaPDV()
        {
            CaixaNegocios caixaNegocios = new CaixaNegocios();

            string retorno = caixaNegocios.checaCaixa();

            if (retorno == "1")
            {
                MessageBox.Show("O Caixa ainda não foi Aberto Click em ( OK ) para abrir o Caixa", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                frmCaixa objCaixa = new frmCaixa();

                DialogResult resultado = objCaixa.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    frmPdv objPdv = new frmPdv();
                    objPdv.ShowDialog();
                }
            }
            else if (retorno == "0")
            {
                frmPdv objPdv = new frmPdv();
                objPdv.ShowDialog();
            }
        }

        public void checaDelivery()
        {
            CaixaNegocios caixaNegocios = new CaixaNegocios();

            string retorno = caixaNegocios.checaCaixa();

            if (retorno == "1")
            {
                MessageBox.Show("O Caixa ainda não foi Aberto Click em ( OK ) para abrir o Caixa", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                frmCaixa objCaixa = new frmCaixa();

                DialogResult resultado = objCaixa.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    frmDelivery objDelivery = new frmDelivery();
                    objDelivery.ShowDialog();
                }
            }
            else if (retorno == "0")
            {
                frmDelivery objDelivery = new frmDelivery();
                objDelivery.ShowDialog();
            }
        }

        private void pegaIp()
        {
            try
            {
                //string nomePc = Dns.GetHostName();
                //IPAddress[] ip = Dns.GetHostAddressesAsync(nomePc);
                //tsslIp.Text = "| IP Máquina: " + ip[2].ToString();

                //Buscar o endereço dwe IP na Máquina Local.
                //Primeiramente vamos obter o host local.
                String strHostName = Dns.GetHostName();
                //Em seguida, usando o nome do host, obter o endereço IP lista.
                IPHostEntry ipEntry = Dns.GetHostByName(strHostName);
                IPAddress[] addr = ipEntry.AddressList;
                String strIP = String.Empty;
                for (int i = 0; i < 1; i++)
                {
                    strIP = strIP + addr[i].ToString();
                }

                tsslIp.Text = " IP Máquina: " + strIP;
            }
            catch (Exception)
            {
                MessageBox.Show("O programa não foi connectado ao Servidor", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tsslIp.Text = "| IP Máquina: Não Connectado";
            }

        }

        #endregion

        
    }
}