﻿namespace UI
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribarMovimentoVisualizar = new DevComponents.DotNetBar.RibbonBar();
            this.btnImprimirPedidos = new DevComponents.DotNetBar.ButtonItem();
            this.btnTempoPedidos = new DevComponents.DotNetBar.ButtonItem();
            this.btnCaixa = new DevComponents.DotNetBar.ButtonItem();
            this.ribarMovimentoVendas = new DevComponents.DotNetBar.RibbonBar();
            this.btnPdv = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelivery = new DevComponents.DotNetBar.ButtonItem();
            this.btnStatusDelivery = new DevComponents.DotNetBar.ButtonItem();
            this.ribarMovimentoProdutos = new DevComponents.DotNetBar.RibbonBar();
            this.btnEntradaProduto = new DevComponents.DotNetBar.ButtonItem();
            this.btnSaidaProduto = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribarCadastroDiversos = new DevComponents.DotNetBar.RibbonBar();
            this.btnBairro = new DevComponents.DotNetBar.ButtonItem();
            this.btnCalculadora = new DevComponents.DotNetBar.ButtonItem();
            this.btnCadUnidadeMedida = new DevComponents.DotNetBar.ButtonItem();
            this.ribarCadastroProdutos = new DevComponents.DotNetBar.RibbonBar();
            this.btnCadProdutos = new DevComponents.DotNetBar.ButtonItem();
            this.btnCadGrupos = new DevComponents.DotNetBar.ButtonItem();
            this.btnEspecial = new DevComponents.DotNetBar.ButtonItem();
            this.btnComplementoProdutos = new DevComponents.DotNetBar.ButtonItem();
            this.ribarCadastroPessoas = new DevComponents.DotNetBar.RibbonBar();
            this.btnCadClientes = new DevComponents.DotNetBar.ButtonItem();
            this.btnCadUsuarios = new DevComponents.DotNetBar.ButtonItem();
            this.BtnCadFornecedores = new DevComponents.DotNetBar.ButtonItem();
            this.btnEntragador = new DevComponents.DotNetBar.ButtonItem();
            this.ribarCadastroSegurança = new DevComponents.DotNetBar.RibbonBar();
            this.btnPermissaoUsuarios = new DevComponents.DotNetBar.ButtonItem();
            this.btnNivelAcesso = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribarRelatoriosProdutos = new DevComponents.DotNetBar.RibbonBar();
            this.btnRltProdutosGeral = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltProdutosGrupos = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltProdutoEstoMinimo = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltProdutoEstoMinGrupo = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltProdutosMaisVendido = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltProdutosEntrada = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltProdutosSaida = new DevComponents.DotNetBar.ButtonItem();
            this.ribarRelatoriosPessoas = new DevComponents.DotNetBar.RibbonBar();
            this.btnRltClientes = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltUsuarios = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltFornecedores = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltEntregadores = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltEntregadorAtivo = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltEntregadoresDesligados = new DevComponents.DotNetBar.ButtonItem();
            this.ribarRelatoriosFinanceiro = new DevComponents.DotNetBar.RibbonBar();
            this.btnRltRetiradas = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltContas = new DevComponents.DotNetBar.ButtonItem();
            this.ribarRelatoriosMovimentos = new DevComponents.DotNetBar.RibbonBar();
            this.btnRltVendas = new DevComponents.DotNetBar.ButtonItem();
            this.btnRltDelivery = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel5 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribarFinanceiroFinaceira = new DevComponents.DotNetBar.RibbonBar();
            this.btnCadCartoes = new DevComponents.DotNetBar.ButtonItem();
            this.btnPlanoConta = new DevComponents.DotNetBar.ButtonItem();
            this.btnFormaPagamento = new DevComponents.DotNetBar.ButtonItem();
            this.btnContasPagar = new DevComponents.DotNetBar.ButtonItem();
            this.btnRetiradas = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel6 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribarGraficosEntregas = new DevComponents.DotNetBar.RibbonBar();
            this.btnGrafEntregasDiarias = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrafEntregasMensal = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrafEntregasEntregador = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrafEntregasTempo = new DevComponents.DotNetBar.ButtonItem();
            this.ribarGraficosVendas = new DevComponents.DotNetBar.RibbonBar();
            this.btnGrafVendasDiarias = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrafVendasMensal = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrafInlocDelivery = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrafCanceladas = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribarAperencia = new DevComponents.DotNetBar.RibbonBar();
            this.btnMetro = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.btnBlackClouds = new DevComponents.DotNetBar.ButtonItem();
            this.btnBlackLilac = new DevComponents.DotNetBar.ButtonItem();
            this.btnBlackSky = new DevComponents.DotNetBar.ButtonItem();
            this.btnBlue = new DevComponents.DotNetBar.ButtonItem();
            this.btnBlueBrown = new DevComponents.DotNetBar.ButtonItem();
            this.btnBorderAux = new DevComponents.DotNetBar.ButtonItem();
            this.btnBrown = new DevComponents.DotNetBar.ButtonItem();
            this.btnChery = new DevComponents.DotNetBar.ButtonItem();
            this.btnDarkBrown = new DevComponents.DotNetBar.ButtonItem();
            this.btnDarkPurple = new DevComponents.DotNetBar.ButtonItem();
            this.btnDarkRed = new DevComponents.DotNetBar.ButtonItem();
            this.btnDefault = new DevComponents.DotNetBar.ButtonItem();
            this.btnEarlyMaroon = new DevComponents.DotNetBar.ButtonItem();
            this.btnEarlyRed = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpresso = new DevComponents.DotNetBar.ButtonItem();
            this.btnFlorestGreen = new DevComponents.DotNetBar.ButtonItem();
            this.btnGrayOrange = new DevComponents.DotNetBar.ButtonItem();
            this.btnGreen = new DevComponents.DotNetBar.ButtonItem();
            this.btnLatter = new DevComponents.DotNetBar.ButtonItem();
            this.btnMaroonSilver = new DevComponents.DotNetBar.ButtonItem();
            this.btnOrange = new DevComponents.DotNetBar.ButtonItem();
            this.btnPowerRed = new DevComponents.DotNetBar.ButtonItem();
            this.btnPurple = new DevComponents.DotNetBar.ButtonItem();
            this.btnRed = new DevComponents.DotNetBar.ButtonItem();
            this.btnRedAmplifield = new DevComponents.DotNetBar.ButtonItem();
            this.btnRust = new DevComponents.DotNetBar.ButtonItem();
            this.btnSilverBlues = new DevComponents.DotNetBar.ButtonItem();
            this.btnSilverGreen = new DevComponents.DotNetBar.ButtonItem();
            this.btnSkyGreen = new DevComponents.DotNetBar.ButtonItem();
            this.btnViStudioDark = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffice2010Black = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffice2010Blue = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffice2010Silver = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffice2007Black = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffice2007Blue = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffice2007Silver = new DevComponents.DotNetBar.ButtonItem();
            this.btnVistaClass = new DevComponents.DotNetBar.ButtonItem();
            this.btnVisualStudio2010 = new DevComponents.DotNetBar.ButtonItem();
            this.btnVisualStudio2012Ligth = new DevComponents.DotNetBar.ButtonItem();
            this.btnVisualStudio2012Black = new DevComponents.DotNetBar.ButtonItem();
            this.btnWindowsBlue = new DevComponents.DotNetBar.ButtonItem();
            this.applicationButton1 = new DevComponents.DotNetBar.ApplicationButton();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.btnCadLancheFacil = new DevComponents.DotNetBar.ButtonItem();
            this.btnCadImpressao = new DevComponents.DotNetBar.ButtonItem();
            this.ckbImpressaoPapel = new DevComponents.DotNetBar.CheckBoxItem();
            this.ckbImpressaoTela = new DevComponents.DotNetBar.CheckBoxItem();
            this.btnMenuBackup = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.btnISair = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem2 = new DevComponents.DotNetBar.RibbonTabItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.Financeiro = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtRelatorios = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtGraficos = new DevComponents.DotNetBar.RibbonTabItem();
            this.Stilo = new DevComponents.DotNetBar.RibbonTabItem();
            this.stiloPrincipal = new DevComponents.DotNetBar.StyleManager(this.components);
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslUsuarioNome = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslIp = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslDataAtual = new System.Windows.Forms.ToolStripStatusLabel();
            this.microChartItem1 = new DevComponents.DotNetBar.MicroChartItem();
            this.ofDialog = new System.Windows.Forms.OpenFileDialog();
            this.microChartItem2 = new DevComponents.DotNetBar.MicroChartItem();
            this.pbLogoPrincipal = new System.Windows.Forms.PictureBox();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel5.SuspendLayout();
            this.ribbonPanel6.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.ribbonPanel5);
            this.ribbonControl1.Controls.Add(this.ribbonPanel6);
            this.ribbonControl1.Controls.Add(this.ribbonPanel4);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.ForeColor = System.Drawing.Color.Black;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.applicationButton1,
            this.ribbonTabItem1,
            this.ribbonTabItem2,
            this.Financeiro,
            this.rtRelatorios,
            this.rtGraficos,
            this.Stilo});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.RibbonStripFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.Size = new System.Drawing.Size(1314, 154);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 0;
            this.ribbonControl1.Text = "DR Sistemas - Food Facil";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel2.Controls.Add(this.ribarMovimentoVisualizar);
            this.ribbonPanel2.Controls.Add(this.ribarMovimentoVendas);
            this.ribbonPanel2.Controls.Add(this.ribarMovimentoProdutos);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 54);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel2.Size = new System.Drawing.Size(1314, 100);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 2;
            // 
            // ribarMovimentoVisualizar
            // 
            this.ribarMovimentoVisualizar.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarMovimentoVisualizar.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarMovimentoVisualizar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarMovimentoVisualizar.ContainerControlProcessDialogKey = true;
            this.ribarMovimentoVisualizar.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarMovimentoVisualizar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnImprimirPedidos,
            this.btnTempoPedidos,
            this.btnCaixa});
            this.ribarMovimentoVisualizar.Location = new System.Drawing.Point(322, 0);
            this.ribarMovimentoVisualizar.Name = "ribarMovimentoVisualizar";
            this.ribarMovimentoVisualizar.Size = new System.Drawing.Size(182, 98);
            this.ribarMovimentoVisualizar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarMovimentoVisualizar.TabIndex = 2;
            this.ribarMovimentoVisualizar.Text = "Visualizar / Imprimir";
            // 
            // 
            // 
            this.ribarMovimentoVisualizar.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarMovimentoVisualizar.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnImprimirPedidos
            // 
            this.btnImprimirPedidos.EnableImageAnimation = true;
            this.btnImprimirPedidos.Image = global::UI.Properties.Resources.printer2;
            this.btnImprimirPedidos.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnImprimirPedidos.ImagePaddingHorizontal = 20;
            this.btnImprimirPedidos.ImagePaddingVertical = 0;
            this.btnImprimirPedidos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnImprimirPedidos.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnImprimirPedidos.Name = "btnImprimirPedidos";
            this.btnImprimirPedidos.SubItemsExpandWidth = 14;
            this.btnImprimirPedidos.Text = "Pedidos";
            this.btnImprimirPedidos.Tooltip = "Reemprimir Pedidos";
            this.btnImprimirPedidos.Click += new System.EventHandler(this.btnImprimirPedidos_Click);
            // 
            // btnTempoPedidos
            // 
            this.btnTempoPedidos.EnableImageAnimation = true;
            this.btnTempoPedidos.Image = global::UI.Properties.Resources.Clock4;
            this.btnTempoPedidos.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnTempoPedidos.ImagePaddingHorizontal = 20;
            this.btnTempoPedidos.ImagePaddingVertical = 0;
            this.btnTempoPedidos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnTempoPedidos.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnTempoPedidos.Name = "btnTempoPedidos";
            this.btnTempoPedidos.SubItemsExpandWidth = 14;
            this.btnTempoPedidos.Text = "Tempo Pedido";
            this.btnTempoPedidos.Tooltip = "Tempo de Espera do Cliente ";
            this.btnTempoPedidos.Click += new System.EventHandler(this.btnTempoPedidos_Click);
            // 
            // btnCaixa
            // 
            this.btnCaixa.EnableImageAnimation = true;
            this.btnCaixa.Image = global::UI.Properties.Resources._1392677106_cash_register;
            this.btnCaixa.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCaixa.ImagePaddingHorizontal = 10;
            this.btnCaixa.ImagePaddingVertical = 0;
            this.btnCaixa.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCaixa.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCaixa.Name = "btnCaixa";
            this.btnCaixa.SubItemsExpandWidth = 14;
            this.btnCaixa.Text = "Caixa";
            this.btnCaixa.Tooltip = "Gerencia de Caixa";
            this.btnCaixa.Click += new System.EventHandler(this.btnCaixa_Click);
            // 
            // ribarMovimentoVendas
            // 
            this.ribarMovimentoVendas.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarMovimentoVendas.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarMovimentoVendas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarMovimentoVendas.ContainerControlProcessDialogKey = true;
            this.ribarMovimentoVendas.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarMovimentoVendas.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnPdv,
            this.btnDelivery,
            this.btnStatusDelivery});
            this.ribarMovimentoVendas.Location = new System.Drawing.Point(111, 0);
            this.ribarMovimentoVendas.Name = "ribarMovimentoVendas";
            this.ribarMovimentoVendas.Size = new System.Drawing.Size(211, 98);
            this.ribarMovimentoVendas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarMovimentoVendas.TabIndex = 1;
            this.ribarMovimentoVendas.Text = "Vendas";
            // 
            // 
            // 
            this.ribarMovimentoVendas.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarMovimentoVendas.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarMovimentoVendas.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnPdv
            // 
            this.btnPdv.EnableImageAnimation = true;
            this.btnPdv.Image = global::UI.Properties.Resources.icoPdv;
            this.btnPdv.ImageFixedSize = new System.Drawing.Size(60, 40);
            this.btnPdv.ImagePaddingHorizontal = 10;
            this.btnPdv.ImagePaddingVertical = 0;
            this.btnPdv.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPdv.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPdv.Name = "btnPdv";
            this.btnPdv.SubItemsExpandWidth = 14;
            this.btnPdv.Text = "PDV";
            this.btnPdv.Tooltip = "Efetuar Vendas";
            this.btnPdv.Click += new System.EventHandler(this.btnPdv_Click);
            // 
            // btnDelivery
            // 
            this.btnDelivery.EnableImageAnimation = true;
            this.btnDelivery.Image = global::UI.Properties.Resources.entrega;
            this.btnDelivery.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.btnDelivery.ImagePaddingHorizontal = 10;
            this.btnDelivery.ImagePaddingVertical = 2;
            this.btnDelivery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDelivery.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDelivery.Name = "btnDelivery";
            this.btnDelivery.SubItemsExpandWidth = 14;
            this.btnDelivery.Text = "Delivery";
            this.btnDelivery.Tooltip = "Atendimento Delivery";
            this.btnDelivery.Click += new System.EventHandler(this.btnDelivery_Click);
            // 
            // btnStatusDelivery
            // 
            this.btnStatusDelivery.EnableImageAnimation = true;
            this.btnStatusDelivery.Image = global::UI.Properties.Resources.iconDelivery1;
            this.btnStatusDelivery.ImageFixedSize = new System.Drawing.Size(60, 40);
            this.btnStatusDelivery.ImagePaddingHorizontal = 10;
            this.btnStatusDelivery.ImagePaddingVertical = 0;
            this.btnStatusDelivery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnStatusDelivery.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnStatusDelivery.Name = "btnStatusDelivery";
            this.btnStatusDelivery.SubItemsExpandWidth = 14;
            this.btnStatusDelivery.Text = "Entregas";
            this.btnStatusDelivery.Tooltip = "Status Delivery";
            this.btnStatusDelivery.Click += new System.EventHandler(this.btnStatusDelivery_Click);
            // 
            // ribarMovimentoProdutos
            // 
            this.ribarMovimentoProdutos.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarMovimentoProdutos.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarMovimentoProdutos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarMovimentoProdutos.ContainerControlProcessDialogKey = true;
            this.ribarMovimentoProdutos.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarMovimentoProdutos.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnEntradaProduto,
            this.btnSaidaProduto});
            this.ribarMovimentoProdutos.Location = new System.Drawing.Point(3, 0);
            this.ribarMovimentoProdutos.Name = "ribarMovimentoProdutos";
            this.ribarMovimentoProdutos.Size = new System.Drawing.Size(108, 98);
            this.ribarMovimentoProdutos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarMovimentoProdutos.TabIndex = 0;
            this.ribarMovimentoProdutos.Text = "Produtos";
            // 
            // 
            // 
            this.ribarMovimentoProdutos.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarMovimentoProdutos.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarMovimentoProdutos.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnEntradaProduto
            // 
            this.btnEntradaProduto.EnableImageAnimation = true;
            this.btnEntradaProduto.Image = global::UI.Properties.Resources.shopping_cart_add;
            this.btnEntradaProduto.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnEntradaProduto.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEntradaProduto.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnEntradaProduto.Name = "btnEntradaProduto";
            this.btnEntradaProduto.SubItemsExpandWidth = 14;
            this.btnEntradaProduto.Text = "Entrada";
            this.btnEntradaProduto.Tooltip = "Entrada de Produtos";
            this.btnEntradaProduto.Click += new System.EventHandler(this.btnEntradaProduto_Click);
            // 
            // btnSaidaProduto
            // 
            this.btnSaidaProduto.EnableImageAnimation = true;
            this.btnSaidaProduto.Image = global::UI.Properties.Resources.shopping_cart_delete;
            this.btnSaidaProduto.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnSaidaProduto.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSaidaProduto.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSaidaProduto.Name = "btnSaidaProduto";
            this.btnSaidaProduto.SubItemsExpandWidth = 14;
            this.btnSaidaProduto.Text = "Saida";
            this.btnSaidaProduto.Tooltip = "Saida de Produtos";
            this.btnSaidaProduto.Click += new System.EventHandler(this.btnSaidaProduto_Click);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.ribarCadastroDiversos);
            this.ribbonPanel1.Controls.Add(this.ribarCadastroProdutos);
            this.ribbonPanel1.Controls.Add(this.ribarCadastroPessoas);
            this.ribbonPanel1.Controls.Add(this.ribarCadastroSegurança);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 54);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel1.Size = new System.Drawing.Size(1314, 100);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            this.ribbonPanel1.Visible = false;
            // 
            // ribarCadastroDiversos
            // 
            this.ribarCadastroDiversos.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarCadastroDiversos.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroDiversos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroDiversos.ContainerControlProcessDialogKey = true;
            this.ribarCadastroDiversos.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarCadastroDiversos.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBairro,
            this.btnCalculadora,
            this.btnCadUnidadeMedida});
            this.ribarCadastroDiversos.ItemSpacing = 10;
            this.ribarCadastroDiversos.Location = new System.Drawing.Point(760, 0);
            this.ribarCadastroDiversos.Name = "ribarCadastroDiversos";
            this.ribarCadastroDiversos.Size = new System.Drawing.Size(201, 98);
            this.ribarCadastroDiversos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarCadastroDiversos.TabIndex = 2;
            this.ribarCadastroDiversos.Text = "Diversos";
            // 
            // 
            // 
            this.ribarCadastroDiversos.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroDiversos.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroDiversos.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnBairro
            // 
            this.btnBairro.EnableImageAnimation = true;
            this.btnBairro.Image = global::UI.Properties.Resources.bairro;
            this.btnBairro.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnBairro.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBairro.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBairro.Name = "btnBairro";
            this.btnBairro.SubItemsExpandWidth = 14;
            this.btnBairro.Text = "Bairro";
            this.btnBairro.Tooltip = "Casdastrar Bairros";
            this.btnBairro.Click += new System.EventHandler(this.btnBairro_Click);
            // 
            // btnCalculadora
            // 
            this.btnCalculadora.EnableImageAnimation = true;
            this.btnCalculadora.Image = global::UI.Properties.Resources.calculadora;
            this.btnCalculadora.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCalculadora.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCalculadora.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCalculadora.Name = "btnCalculadora";
            this.btnCalculadora.SubItemsExpandWidth = 14;
            this.btnCalculadora.Text = "Calculadora";
            this.btnCalculadora.Tooltip = "Auxilio da Calculadora";
            this.btnCalculadora.Click += new System.EventHandler(this.btnCalculadora_Click);
            // 
            // btnCadUnidadeMedida
            // 
            this.btnCadUnidadeMedida.EnableImageAnimation = true;
            this.btnCadUnidadeMedida.Image = global::UI.Properties.Resources.unidadeMedida_fw;
            this.btnCadUnidadeMedida.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCadUnidadeMedida.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCadUnidadeMedida.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCadUnidadeMedida.Name = "btnCadUnidadeMedida";
            this.btnCadUnidadeMedida.SubItemsExpandWidth = 14;
            this.btnCadUnidadeMedida.Text = "Unidade";
            this.btnCadUnidadeMedida.Tooltip = "Cadastrar Unidade de Medidas para Produtos";
            this.btnCadUnidadeMedida.Click += new System.EventHandler(this.btnCadUnidadeMedida_Click);
            // 
            // ribarCadastroProdutos
            // 
            this.ribarCadastroProdutos.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarCadastroProdutos.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroProdutos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroProdutos.ContainerControlProcessDialogKey = true;
            this.ribarCadastroProdutos.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarCadastroProdutos.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnCadProdutos,
            this.btnCadGrupos,
            this.btnEspecial,
            this.btnComplementoProdutos});
            this.ribarCadastroProdutos.ItemSpacing = 10;
            this.ribarCadastroProdutos.Location = new System.Drawing.Point(464, 0);
            this.ribarCadastroProdutos.Name = "ribarCadastroProdutos";
            this.ribarCadastroProdutos.Size = new System.Drawing.Size(296, 98);
            this.ribarCadastroProdutos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarCadastroProdutos.TabIndex = 1;
            this.ribarCadastroProdutos.Text = "Produtos";
            // 
            // 
            // 
            this.ribarCadastroProdutos.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroProdutos.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroProdutos.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnCadProdutos
            // 
            this.btnCadProdutos.EnableImageAnimation = true;
            this.btnCadProdutos.Image = global::UI.Properties.Resources._7393_128x128;
            this.btnCadProdutos.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCadProdutos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCadProdutos.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCadProdutos.Name = "btnCadProdutos";
            this.btnCadProdutos.SubItemsExpandWidth = 14;
            this.btnCadProdutos.Text = "Produtos";
            this.btnCadProdutos.Tooltip = "Casdastrar Produtos";
            this.btnCadProdutos.Click += new System.EventHandler(this.btnCadProdutos_Click);
            // 
            // btnCadGrupos
            // 
            this.btnCadGrupos.EnableImageAnimation = true;
            this.btnCadGrupos.Image = global::UI.Properties.Resources.extras;
            this.btnCadGrupos.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCadGrupos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCadGrupos.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCadGrupos.Name = "btnCadGrupos";
            this.btnCadGrupos.SubItemsExpandWidth = 14;
            this.btnCadGrupos.Text = "Grupos";
            this.btnCadGrupos.Tooltip = "Cadastrar Grupos de Produtos";
            this.btnCadGrupos.Click += new System.EventHandler(this.btnCadGrupos_Click);
            // 
            // btnEspecial
            // 
            this.btnEspecial.EnableImageAnimation = true;
            this.btnEspecial.Image = global::UI.Properties.Resources._7397_128x128;
            this.btnEspecial.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnEspecial.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEspecial.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnEspecial.Name = "btnEspecial";
            this.btnEspecial.SubItemsExpandWidth = 14;
            this.btnEspecial.Text = "Especiais";
            this.btnEspecial.Tooltip = "Cadastrar Especiais";
            this.btnEspecial.Click += new System.EventHandler(this.btnEspecial_Click);
            // 
            // btnComplementoProdutos
            // 
            this.btnComplementoProdutos.EnableImageAnimation = true;
            this.btnComplementoProdutos.Image = ((System.Drawing.Image)(resources.GetObject("btnComplementoProdutos.Image")));
            this.btnComplementoProdutos.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnComplementoProdutos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnComplementoProdutos.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnComplementoProdutos.Name = "btnComplementoProdutos";
            this.btnComplementoProdutos.SubItemsExpandWidth = 14;
            this.btnComplementoProdutos.Text = "Complemento";
            this.btnComplementoProdutos.Tooltip = "Casdastrar Produtos";
            this.btnComplementoProdutos.Click += new System.EventHandler(this.btnComplementoProdutos_Click);
            // 
            // ribarCadastroPessoas
            // 
            this.ribarCadastroPessoas.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarCadastroPessoas.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroPessoas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroPessoas.ContainerControlProcessDialogKey = true;
            this.ribarCadastroPessoas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ribarCadastroPessoas.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarCadastroPessoas.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnCadClientes,
            this.btnCadUsuarios,
            this.BtnCadFornecedores,
            this.btnEntragador});
            this.ribarCadastroPessoas.ItemSpacing = 10;
            this.ribarCadastroPessoas.Location = new System.Drawing.Point(165, 0);
            this.ribarCadastroPessoas.Name = "ribarCadastroPessoas";
            this.ribarCadastroPessoas.Size = new System.Drawing.Size(299, 98);
            this.ribarCadastroPessoas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarCadastroPessoas.TabIndex = 0;
            this.ribarCadastroPessoas.Text = "Pessoas";
            // 
            // 
            // 
            this.ribarCadastroPessoas.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroPessoas.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroPessoas.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnCadClientes
            // 
            this.btnCadClientes.EnableImageAnimation = true;
            this.btnCadClientes.Image = global::UI.Properties.Resources._503_128x128;
            this.btnCadClientes.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCadClientes.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCadClientes.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCadClientes.Name = "btnCadClientes";
            this.btnCadClientes.SubItemsExpandWidth = 14;
            this.btnCadClientes.Text = "Clientes";
            this.btnCadClientes.Tooltip = "Casdastrar Clientes";
            this.btnCadClientes.Click += new System.EventHandler(this.btnCadClientes_Click);
            // 
            // btnCadUsuarios
            // 
            this.btnCadUsuarios.EnableImageAnimation = true;
            this.btnCadUsuarios.Image = global::UI.Properties.Resources._7834_64x64;
            this.btnCadUsuarios.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCadUsuarios.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCadUsuarios.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCadUsuarios.Name = "btnCadUsuarios";
            this.btnCadUsuarios.SubItemsExpandWidth = 14;
            this.btnCadUsuarios.Text = "Usuários";
            this.btnCadUsuarios.Tooltip = "Casdastrar Usuários";
            this.btnCadUsuarios.Click += new System.EventHandler(this.btnCadUsuarios_Click);
            // 
            // BtnCadFornecedores
            // 
            this.BtnCadFornecedores.EnableImageAnimation = true;
            this.BtnCadFornecedores.Image = global::UI.Properties.Resources._1392677144_provider;
            this.BtnCadFornecedores.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.BtnCadFornecedores.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnCadFornecedores.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.BtnCadFornecedores.Name = "BtnCadFornecedores";
            this.BtnCadFornecedores.SubItemsExpandWidth = 14;
            this.BtnCadFornecedores.Text = "Fornecedores";
            this.BtnCadFornecedores.Tooltip = "Casdastrar Fornecedores";
            this.BtnCadFornecedores.Click += new System.EventHandler(this.BtnCadFornecedores_Click);
            // 
            // btnEntragador
            // 
            this.btnEntragador.EnableImageAnimation = true;
            this.btnEntragador.Image = global::UI.Properties.Resources.ENTREGADOR;
            this.btnEntragador.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnEntragador.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEntragador.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnEntragador.Name = "btnEntragador";
            this.btnEntragador.SubItemsExpandWidth = 14;
            this.btnEntragador.Text = "Entregadores";
            this.btnEntragador.Tooltip = "Casdastrar Entregadores";
            this.btnEntragador.Click += new System.EventHandler(this.btnEntragador_Click);
            // 
            // ribarCadastroSegurança
            // 
            this.ribarCadastroSegurança.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarCadastroSegurança.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroSegurança.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroSegurança.ContainerControlProcessDialogKey = true;
            this.ribarCadastroSegurança.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ribarCadastroSegurança.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarCadastroSegurança.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnPermissaoUsuarios,
            this.btnNivelAcesso});
            this.ribarCadastroSegurança.ItemSpacing = 10;
            this.ribarCadastroSegurança.Location = new System.Drawing.Point(3, 0);
            this.ribarCadastroSegurança.Name = "ribarCadastroSegurança";
            this.ribarCadastroSegurança.Size = new System.Drawing.Size(162, 98);
            this.ribarCadastroSegurança.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarCadastroSegurança.TabIndex = 3;
            this.ribarCadastroSegurança.Text = "Segurança";
            // 
            // 
            // 
            this.ribarCadastroSegurança.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarCadastroSegurança.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarCadastroSegurança.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnPermissaoUsuarios
            // 
            this.btnPermissaoUsuarios.EnableImageAnimation = true;
            this.btnPermissaoUsuarios.Image = global::UI.Properties.Resources.Policeman;
            this.btnPermissaoUsuarios.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.btnPermissaoUsuarios.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPermissaoUsuarios.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPermissaoUsuarios.Name = "btnPermissaoUsuarios";
            this.btnPermissaoUsuarios.SubItemsExpandWidth = 14;
            this.btnPermissaoUsuarios.Text = "Permissões de Usuários";
            this.btnPermissaoUsuarios.Tooltip = "Permissões de Acesso";
            this.btnPermissaoUsuarios.Click += new System.EventHandler(this.btnPermissaoUsuarios_Click);
            // 
            // btnNivelAcesso
            // 
            this.btnNivelAcesso.EnableImageAnimation = true;
            this.btnNivelAcesso.Image = global::UI.Properties.Resources.Security_Reader2;
            this.btnNivelAcesso.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.btnNivelAcesso.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnNivelAcesso.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnNivelAcesso.Name = "btnNivelAcesso";
            this.btnNivelAcesso.SubItemsExpandWidth = 14;
            this.btnNivelAcesso.Text = "Nivel de Acesso";
            this.btnNivelAcesso.Tooltip = "Nivel de Acesso";
            this.btnNivelAcesso.Click += new System.EventHandler(this.btnNivelAcesso_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.ribarRelatoriosProdutos);
            this.ribbonPanel3.Controls.Add(this.ribarRelatoriosPessoas);
            this.ribbonPanel3.Controls.Add(this.ribarRelatoriosFinanceiro);
            this.ribbonPanel3.Controls.Add(this.ribarRelatoriosMovimentos);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 0);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel3.Size = new System.Drawing.Size(1189, 154);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // ribarRelatoriosProdutos
            // 
            this.ribarRelatoriosProdutos.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarRelatoriosProdutos.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosProdutos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosProdutos.ContainerControlProcessDialogKey = true;
            this.ribarRelatoriosProdutos.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarRelatoriosProdutos.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRltProdutosGeral,
            this.btnRltProdutosGrupos,
            this.btnRltProdutoEstoMinimo,
            this.btnRltProdutoEstoMinGrupo,
            this.btnRltProdutosMaisVendido,
            this.btnRltProdutosEntrada,
            this.btnRltProdutosSaida});
            this.ribarRelatoriosProdutos.ItemSpacing = 10;
            this.ribarRelatoriosProdutos.Location = new System.Drawing.Point(529, 0);
            this.ribarRelatoriosProdutos.Name = "ribarRelatoriosProdutos";
            this.ribarRelatoriosProdutos.Size = new System.Drawing.Size(467, 152);
            this.ribarRelatoriosProdutos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarRelatoriosProdutos.TabIndex = 6;
            this.ribarRelatoriosProdutos.Text = "Produtos";
            // 
            // 
            // 
            this.ribarRelatoriosProdutos.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosProdutos.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosProdutos.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnRltProdutosGeral
            // 
            this.btnRltProdutosGeral.EnableImageAnimation = true;
            this.btnRltProdutosGeral.Image = global::UI.Properties.Resources._7393_128x128;
            this.btnRltProdutosGeral.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutosGeral.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutosGeral.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutosGeral.Name = "btnRltProdutosGeral";
            this.btnRltProdutosGeral.SubItemsExpandWidth = 14;
            this.btnRltProdutosGeral.Text = "Produtos";
            this.btnRltProdutosGeral.Tooltip = "Relatório de Produtos";
            this.btnRltProdutosGeral.Click += new System.EventHandler(this.btnRltProdutosGeral_Click);
            // 
            // btnRltProdutosGrupos
            // 
            this.btnRltProdutosGrupos.EnableImageAnimation = true;
            this.btnRltProdutosGrupos.Image = global::UI.Properties.Resources.shopping_cart;
            this.btnRltProdutosGrupos.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutosGrupos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutosGrupos.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutosGrupos.Name = "btnRltProdutosGrupos";
            this.btnRltProdutosGrupos.SubItemsExpandWidth = 14;
            this.btnRltProdutosGrupos.Text = "Produto Grupo";
            this.btnRltProdutosGrupos.Tooltip = "Relatorio de Produtos por Grupos";
            this.btnRltProdutosGrupos.Click += new System.EventHandler(this.btnRltProdutosGrupos_Click);
            // 
            // btnRltProdutoEstoMinimo
            // 
            this.btnRltProdutoEstoMinimo.EnableImageAnimation = true;
            this.btnRltProdutoEstoMinimo.Image = global::UI.Properties.Resources.estoqueMinimo;
            this.btnRltProdutoEstoMinimo.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutoEstoMinimo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutoEstoMinimo.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutoEstoMinimo.Name = "btnRltProdutoEstoMinimo";
            this.btnRltProdutoEstoMinimo.SubItemsExpandWidth = 14;
            this.btnRltProdutoEstoMinimo.Text = "Estoque Critico";
            this.btnRltProdutoEstoMinimo.Tooltip = "Relatorio de Estoque";
            this.btnRltProdutoEstoMinimo.Click += new System.EventHandler(this.btnRltProdutoEstoMinimo_Click);
            // 
            // btnRltProdutoEstoMinGrupo
            // 
            this.btnRltProdutoEstoMinGrupo.EnableImageAnimation = true;
            this.btnRltProdutoEstoMinGrupo.Image = global::UI.Properties.Resources.produtosGrupo;
            this.btnRltProdutoEstoMinGrupo.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutoEstoMinGrupo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutoEstoMinGrupo.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutoEstoMinGrupo.Name = "btnRltProdutoEstoMinGrupo";
            this.btnRltProdutoEstoMinGrupo.SubItemsExpandWidth = 14;
            this.btnRltProdutoEstoMinGrupo.Text = "Estoque Critico Grupo";
            this.btnRltProdutoEstoMinGrupo.Tooltip = "Relatorio de Estoque";
            this.btnRltProdutoEstoMinGrupo.Click += new System.EventHandler(this.btnRltProdutoEstoMinGrupo_Click);
            // 
            // btnRltProdutosMaisVendido
            // 
            this.btnRltProdutosMaisVendido.EnableImageAnimation = true;
            this.btnRltProdutosMaisVendido.Image = global::UI.Properties.Resources.maisVendido;
            this.btnRltProdutosMaisVendido.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutosMaisVendido.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutosMaisVendido.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutosMaisVendido.Name = "btnRltProdutosMaisVendido";
            this.btnRltProdutosMaisVendido.SubItemsExpandWidth = 14;
            this.btnRltProdutosMaisVendido.Text = "Produto + Vendido";
            this.btnRltProdutosMaisVendido.Tooltip = "Produtos mais Vendidos";
            this.btnRltProdutosMaisVendido.Click += new System.EventHandler(this.btnRltProdutosMaisVendido_Click);
            // 
            // btnRltProdutosEntrada
            // 
            this.btnRltProdutosEntrada.EnableImageAnimation = true;
            this.btnRltProdutosEntrada.Image = global::UI.Properties.Resources.shopping_cart_back;
            this.btnRltProdutosEntrada.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutosEntrada.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutosEntrada.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutosEntrada.Name = "btnRltProdutosEntrada";
            this.btnRltProdutosEntrada.SubItemsExpandWidth = 14;
            this.btnRltProdutosEntrada.Text = "Relatório Entrada";
            this.btnRltProdutosEntrada.Tooltip = "Relatório de Entrada de Produtos";
            this.btnRltProdutosEntrada.Click += new System.EventHandler(this.btnRltProdutosEntrada_Click);
            // 
            // btnRltProdutosSaida
            // 
            this.btnRltProdutosSaida.EnableImageAnimation = true;
            this.btnRltProdutosSaida.Image = global::UI.Properties.Resources.shopping_cart_next;
            this.btnRltProdutosSaida.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltProdutosSaida.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltProdutosSaida.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltProdutosSaida.Name = "btnRltProdutosSaida";
            this.btnRltProdutosSaida.SubItemsExpandWidth = 14;
            this.btnRltProdutosSaida.Text = "Relatório Saida";
            this.btnRltProdutosSaida.Tooltip = "Relatório de Saida de Produtos";
            this.btnRltProdutosSaida.Click += new System.EventHandler(this.btnRltProdutosSaida_Click);
            // 
            // ribarRelatoriosPessoas
            // 
            this.ribarRelatoriosPessoas.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarRelatoriosPessoas.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosPessoas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosPessoas.ContainerControlProcessDialogKey = true;
            this.ribarRelatoriosPessoas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ribarRelatoriosPessoas.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarRelatoriosPessoas.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRltClientes,
            this.btnRltUsuarios,
            this.btnRltFornecedores,
            this.btnRltEntregadores});
            this.ribarRelatoriosPessoas.ItemSpacing = 10;
            this.ribarRelatoriosPessoas.Location = new System.Drawing.Point(243, 0);
            this.ribarRelatoriosPessoas.Name = "ribarRelatoriosPessoas";
            this.ribarRelatoriosPessoas.Size = new System.Drawing.Size(286, 152);
            this.ribarRelatoriosPessoas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarRelatoriosPessoas.TabIndex = 5;
            this.ribarRelatoriosPessoas.Text = "Pessoas";
            // 
            // 
            // 
            this.ribarRelatoriosPessoas.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosPessoas.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosPessoas.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnRltClientes
            // 
            this.btnRltClientes.EnableImageAnimation = true;
            this.btnRltClientes.Image = global::UI.Properties.Resources._503_128x128;
            this.btnRltClientes.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltClientes.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltClientes.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltClientes.Name = "btnRltClientes";
            this.btnRltClientes.SubItemsExpandWidth = 14;
            this.btnRltClientes.Text = "Clientes";
            this.btnRltClientes.Tooltip = "Relatório de Clientes";
            this.btnRltClientes.Click += new System.EventHandler(this.btnRltClientes_Click);
            // 
            // btnRltUsuarios
            // 
            this.btnRltUsuarios.EnableImageAnimation = true;
            this.btnRltUsuarios.Image = global::UI.Properties.Resources._7834_64x64;
            this.btnRltUsuarios.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltUsuarios.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltUsuarios.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltUsuarios.Name = "btnRltUsuarios";
            this.btnRltUsuarios.SubItemsExpandWidth = 14;
            this.btnRltUsuarios.Text = "Usuários";
            this.btnRltUsuarios.Tooltip = "Relatório de Usuários";
            this.btnRltUsuarios.Click += new System.EventHandler(this.btnRltUsuarios_Click);
            // 
            // btnRltFornecedores
            // 
            this.btnRltFornecedores.EnableImageAnimation = true;
            this.btnRltFornecedores.Image = global::UI.Properties.Resources._1392677144_provider;
            this.btnRltFornecedores.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltFornecedores.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltFornecedores.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltFornecedores.Name = "btnRltFornecedores";
            this.btnRltFornecedores.SubItemsExpandWidth = 14;
            this.btnRltFornecedores.Text = "Fornecedores";
            this.btnRltFornecedores.Tooltip = "Relatório de Fornecedores";
            this.btnRltFornecedores.Click += new System.EventHandler(this.btnRltFornecedores_Click);
            // 
            // btnRltEntregadores
            // 
            this.btnRltEntregadores.EnableImageAnimation = true;
            this.btnRltEntregadores.Image = global::UI.Properties.Resources.ENTREGADOR;
            this.btnRltEntregadores.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltEntregadores.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltEntregadores.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltEntregadores.Name = "btnRltEntregadores";
            this.btnRltEntregadores.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRltEntregadorAtivo,
            this.btnRltEntregadoresDesligados});
            this.btnRltEntregadores.SubItemsExpandWidth = 14;
            this.btnRltEntregadores.Text = "Entregadores";
            this.btnRltEntregadores.Tooltip = "Relatório de Entregadores";
            this.btnRltEntregadores.Click += new System.EventHandler(this.btnRltEntregadores_Click);
            // 
            // btnRltEntregadorAtivo
            // 
            this.btnRltEntregadorAtivo.EnableImageAnimation = true;
            this.btnRltEntregadorAtivo.Image = global::UI.Properties.Resources.user_accept;
            this.btnRltEntregadorAtivo.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnRltEntregadorAtivo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltEntregadorAtivo.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltEntregadorAtivo.Name = "btnRltEntregadorAtivo";
            this.btnRltEntregadorAtivo.SubItemsExpandWidth = 14;
            this.btnRltEntregadorAtivo.Text = "Entregadores Ativos";
            this.btnRltEntregadorAtivo.Tooltip = "Casdastrar Entregadores";
            this.btnRltEntregadorAtivo.Click += new System.EventHandler(this.btnRltEntregadorAtivo_Click);
            // 
            // btnRltEntregadoresDesligados
            // 
            this.btnRltEntregadoresDesligados.EnableImageAnimation = true;
            this.btnRltEntregadoresDesligados.Image = global::UI.Properties.Resources.user_delete;
            this.btnRltEntregadoresDesligados.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnRltEntregadoresDesligados.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltEntregadoresDesligados.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltEntregadoresDesligados.Name = "btnRltEntregadoresDesligados";
            this.btnRltEntregadoresDesligados.SubItemsExpandWidth = 14;
            this.btnRltEntregadoresDesligados.Text = "Entregadores Desligados";
            this.btnRltEntregadoresDesligados.Tooltip = "Casdastrar Entregadores";
            this.btnRltEntregadoresDesligados.Click += new System.EventHandler(this.btnRltEntregadoresDesligados_Click);
            // 
            // ribarRelatoriosFinanceiro
            // 
            this.ribarRelatoriosFinanceiro.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarRelatoriosFinanceiro.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosFinanceiro.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosFinanceiro.ContainerControlProcessDialogKey = true;
            this.ribarRelatoriosFinanceiro.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarRelatoriosFinanceiro.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRltRetiradas,
            this.btnRltContas});
            this.ribarRelatoriosFinanceiro.Location = new System.Drawing.Point(120, 0);
            this.ribarRelatoriosFinanceiro.Name = "ribarRelatoriosFinanceiro";
            this.ribarRelatoriosFinanceiro.Size = new System.Drawing.Size(123, 152);
            this.ribarRelatoriosFinanceiro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarRelatoriosFinanceiro.TabIndex = 4;
            this.ribarRelatoriosFinanceiro.Text = "Financeiro";
            // 
            // 
            // 
            this.ribarRelatoriosFinanceiro.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosFinanceiro.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosFinanceiro.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnRltRetiradas
            // 
            this.btnRltRetiradas.EnableImageAnimation = true;
            this.btnRltRetiradas.Image = global::UI.Properties.Resources.money;
            this.btnRltRetiradas.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltRetiradas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltRetiradas.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltRetiradas.Name = "btnRltRetiradas";
            this.btnRltRetiradas.SubItemsExpandWidth = 14;
            this.btnRltRetiradas.Text = "Retiradas";
            this.btnRltRetiradas.Tooltip = "Relatório de Retiradas Efetuadas";
            this.btnRltRetiradas.Click += new System.EventHandler(this.btnRltRetiradas_Click);
            // 
            // btnRltContas
            // 
            this.btnRltContas.EnableImageAnimation = true;
            this.btnRltContas.Image = global::UI.Properties.Resources.contas;
            this.btnRltContas.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltContas.ImagePaddingHorizontal = 15;
            this.btnRltContas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltContas.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltContas.Name = "btnRltContas";
            this.btnRltContas.SubItemsExpandWidth = 14;
            this.btnRltContas.Text = "Contas";
            this.btnRltContas.Tooltip = "Relatorios de Contas a Pagar";
            this.btnRltContas.Click += new System.EventHandler(this.btnRltContas_Click);
            // 
            // ribarRelatoriosMovimentos
            // 
            this.ribarRelatoriosMovimentos.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarRelatoriosMovimentos.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosMovimentos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosMovimentos.ContainerControlProcessDialogKey = true;
            this.ribarRelatoriosMovimentos.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarRelatoriosMovimentos.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRltVendas,
            this.btnRltDelivery});
            this.ribarRelatoriosMovimentos.Location = new System.Drawing.Point(3, 0);
            this.ribarRelatoriosMovimentos.Name = "ribarRelatoriosMovimentos";
            this.ribarRelatoriosMovimentos.Size = new System.Drawing.Size(117, 152);
            this.ribarRelatoriosMovimentos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarRelatoriosMovimentos.TabIndex = 3;
            this.ribarRelatoriosMovimentos.Text = "Movimentos";
            // 
            // 
            // 
            this.ribarRelatoriosMovimentos.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarRelatoriosMovimentos.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarRelatoriosMovimentos.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnRltVendas
            // 
            this.btnRltVendas.EnableImageAnimation = true;
            this.btnRltVendas.Image = global::UI.Properties.Resources._1383710091_electronic_billing_machine;
            this.btnRltVendas.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRltVendas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltVendas.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltVendas.Name = "btnRltVendas";
            this.btnRltVendas.SubItemsExpandWidth = 14;
            this.btnRltVendas.Text = "Vendas";
            this.btnRltVendas.Tooltip = "Relatório de Vendas";
            this.btnRltVendas.Click += new System.EventHandler(this.btnRltVendas_Click);
            // 
            // btnRltDelivery
            // 
            this.btnRltDelivery.EnableImageAnimation = true;
            this.btnRltDelivery.Image = global::UI.Properties.Resources.entrega;
            this.btnRltDelivery.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.btnRltDelivery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRltDelivery.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRltDelivery.Name = "btnRltDelivery";
            this.btnRltDelivery.SubItemsExpandWidth = 14;
            this.btnRltDelivery.Text = "Delivery";
            this.btnRltDelivery.Tooltip = "Relatorio Delivery";
            this.btnRltDelivery.Click += new System.EventHandler(this.btnRltDelivery_Click);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel5.Controls.Add(this.ribarFinanceiroFinaceira);
            this.ribbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel5.Location = new System.Drawing.Point(0, 0);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel5.Size = new System.Drawing.Size(1189, 154);
            // 
            // 
            // 
            this.ribbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel5.TabIndex = 5;
            this.ribbonPanel5.Visible = false;
            // 
            // ribarFinanceiroFinaceira
            // 
            this.ribarFinanceiroFinaceira.AutoOverflowEnabled = true;
            this.ribarFinanceiroFinaceira.AutoSizeItems = false;
            // 
            // 
            // 
            this.ribarFinanceiroFinaceira.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarFinanceiroFinaceira.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarFinanceiroFinaceira.ContainerControlProcessDialogKey = true;
            this.ribarFinanceiroFinaceira.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnCadCartoes,
            this.btnPlanoConta,
            this.btnFormaPagamento,
            this.btnContasPagar,
            this.btnRetiradas});
            this.ribarFinanceiroFinaceira.Location = new System.Drawing.Point(3, 0);
            this.ribarFinanceiroFinaceira.Name = "ribarFinanceiroFinaceira";
            this.ribarFinanceiroFinaceira.Size = new System.Drawing.Size(325, 98);
            this.ribarFinanceiroFinaceira.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarFinanceiroFinaceira.TabIndex = 3;
            this.ribarFinanceiroFinaceira.Text = "Financeira";
            // 
            // 
            // 
            this.ribarFinanceiroFinaceira.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarFinanceiroFinaceira.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarFinanceiroFinaceira.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnCadCartoes
            // 
            this.btnCadCartoes.EnableImageAnimation = true;
            this.btnCadCartoes.Image = global::UI.Properties.Resources.credit_cards;
            this.btnCadCartoes.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCadCartoes.ImagePaddingHorizontal = 10;
            this.btnCadCartoes.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCadCartoes.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnCadCartoes.Name = "btnCadCartoes";
            this.btnCadCartoes.SubItemsExpandWidth = 14;
            this.btnCadCartoes.Text = "Cartões";
            this.btnCadCartoes.Tooltip = "Cadastrar Cartões";
            this.btnCadCartoes.Click += new System.EventHandler(this.btnCadCartoes_Click);
            // 
            // btnPlanoConta
            // 
            this.btnPlanoConta.EnableImageAnimation = true;
            this.btnPlanoConta.Image = global::UI.Properties.Resources._1394608632_49;
            this.btnPlanoConta.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.btnPlanoConta.ImagePaddingHorizontal = 15;
            this.btnPlanoConta.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPlanoConta.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPlanoConta.Name = "btnPlanoConta";
            this.btnPlanoConta.SubItemsExpandWidth = 14;
            this.btnPlanoConta.Text = "Plano Contas";
            this.btnPlanoConta.Tooltip = "Cadastrar Plano de Contas";
            this.btnPlanoConta.Click += new System.EventHandler(this.btnPlanoConta_Click);
            // 
            // btnFormaPagamento
            // 
            this.btnFormaPagamento.EnableImageAnimation = true;
            this.btnFormaPagamento.Image = global::UI.Properties.Resources.formaPagamento;
            this.btnFormaPagamento.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnFormaPagamento.ImagePaddingHorizontal = 10;
            this.btnFormaPagamento.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnFormaPagamento.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnFormaPagamento.Name = "btnFormaPagamento";
            this.btnFormaPagamento.SubItemsExpandWidth = 14;
            this.btnFormaPagamento.Text = "Forma de Pagamentos";
            this.btnFormaPagamento.Tooltip = "Cadastrar Forma de Pagamentos";
            this.btnFormaPagamento.Click += new System.EventHandler(this.btnFormaPagamento_Click);
            // 
            // btnContasPagar
            // 
            this.btnContasPagar.EnableImageAnimation = true;
            this.btnContasPagar.Image = global::UI.Properties.Resources.contas;
            this.btnContasPagar.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnContasPagar.ImagePaddingHorizontal = 15;
            this.btnContasPagar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnContasPagar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnContasPagar.Name = "btnContasPagar";
            this.btnContasPagar.SubItemsExpandWidth = 14;
            this.btnContasPagar.Text = "Contas a Pagar";
            this.btnContasPagar.Tooltip = "Cadastrar Contas a Pagar";
            this.btnContasPagar.Click += new System.EventHandler(this.btnContasPagar_Click);
            // 
            // btnRetiradas
            // 
            this.btnRetiradas.EnableImageAnimation = true;
            this.btnRetiradas.Image = global::UI.Properties.Resources._1402550826_Business;
            this.btnRetiradas.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRetiradas.ImagePaddingHorizontal = 20;
            this.btnRetiradas.ImagePaddingVertical = 0;
            this.btnRetiradas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRetiradas.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRetiradas.Name = "btnRetiradas";
            this.btnRetiradas.SubItemsExpandWidth = 14;
            this.btnRetiradas.Text = "Retiradas";
            this.btnRetiradas.Tooltip = "Gerenciar Retiradas de Valores";
            this.btnRetiradas.Click += new System.EventHandler(this.btnRetiradas_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel6.Controls.Add(this.ribarGraficosEntregas);
            this.ribbonPanel6.Controls.Add(this.ribarGraficosVendas);
            this.ribbonPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel6.Location = new System.Drawing.Point(0, 54);
            this.ribbonPanel6.Name = "ribbonPanel6";
            this.ribbonPanel6.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel6.Size = new System.Drawing.Size(1189, 100);
            // 
            // 
            // 
            this.ribbonPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel6.TabIndex = 6;
            this.ribbonPanel6.Visible = false;
            // 
            // ribarGraficosEntregas
            // 
            this.ribarGraficosEntregas.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarGraficosEntregas.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarGraficosEntregas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarGraficosEntregas.ContainerControlProcessDialogKey = true;
            this.ribarGraficosEntregas.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarGraficosEntregas.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnGrafEntregasDiarias,
            this.btnGrafEntregasMensal,
            this.btnGrafEntregasEntregador,
            this.btnGrafEntregasTempo});
            this.ribarGraficosEntregas.Location = new System.Drawing.Point(257, 0);
            this.ribarGraficosEntregas.Name = "ribarGraficosEntregas";
            this.ribarGraficosEntregas.Size = new System.Drawing.Size(233, 98);
            this.ribarGraficosEntregas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarGraficosEntregas.TabIndex = 1;
            this.ribarGraficosEntregas.Text = "Entregas";
            // 
            // 
            // 
            this.ribarGraficosEntregas.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarGraficosEntregas.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnGrafEntregasDiarias
            // 
            this.btnGrafEntregasDiarias.EnableImageAnimation = true;
            this.btnGrafEntregasDiarias.Image = global::UI.Properties.Resources._1411530944_gnumeric;
            this.btnGrafEntregasDiarias.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafEntregasDiarias.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafEntregasDiarias.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafEntregasDiarias.Name = "btnGrafEntregasDiarias";
            this.btnGrafEntregasDiarias.SubItemsExpandWidth = 14;
            this.btnGrafEntregasDiarias.Text = "Diárias";
            this.btnGrafEntregasDiarias.Tooltip = "Gráfico de Entregas Diárias";
            this.btnGrafEntregasDiarias.Click += new System.EventHandler(this.btnGrafEntregasDiarias_Click);
            // 
            // btnGrafEntregasMensal
            // 
            this.btnGrafEntregasMensal.EnableImageAnimation = true;
            this.btnGrafEntregasMensal.Image = global::UI.Properties.Resources._1411530923_chart_pie;
            this.btnGrafEntregasMensal.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafEntregasMensal.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafEntregasMensal.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafEntregasMensal.Name = "btnGrafEntregasMensal";
            this.btnGrafEntregasMensal.SubItemsExpandWidth = 14;
            this.btnGrafEntregasMensal.Text = "Mensal";
            this.btnGrafEntregasMensal.Tooltip = "Gráfico de Entregas Mensais";
            this.btnGrafEntregasMensal.Click += new System.EventHandler(this.btnGrafEntregasMensal_Click);
            // 
            // btnGrafEntregasEntregador
            // 
            this.btnGrafEntregasEntregador.EnableImageAnimation = true;
            this.btnGrafEntregasEntregador.Image = global::UI.Properties.Resources._1411531202_line_chart;
            this.btnGrafEntregasEntregador.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafEntregasEntregador.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafEntregasEntregador.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafEntregasEntregador.Name = "btnGrafEntregasEntregador";
            this.btnGrafEntregasEntregador.SubItemsExpandWidth = 14;
            this.btnGrafEntregasEntregador.Text = "Por Entregador";
            this.btnGrafEntregasEntregador.Tooltip = "Gráfico de Entregadores";
            this.btnGrafEntregasEntregador.Click += new System.EventHandler(this.btnGrafEntregasEntregador_Click);
            // 
            // btnGrafEntregasTempo
            // 
            this.btnGrafEntregasTempo.EnableImageAnimation = true;
            this.btnGrafEntregasTempo.Image = global::UI.Properties.Resources._1411531109_bandwidth_px_png;
            this.btnGrafEntregasTempo.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafEntregasTempo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafEntregasTempo.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafEntregasTempo.Name = "btnGrafEntregasTempo";
            this.btnGrafEntregasTempo.SubItemsExpandWidth = 14;
            this.btnGrafEntregasTempo.Text = "Tempo";
            this.btnGrafEntregasTempo.Tooltip = "Gráfico de Tempo de Entrega";
            this.btnGrafEntregasTempo.Click += new System.EventHandler(this.btnGrafEntregasTempo_Click);
            // 
            // ribarGraficosVendas
            // 
            this.ribarGraficosVendas.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarGraficosVendas.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarGraficosVendas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarGraficosVendas.ContainerControlProcessDialogKey = true;
            this.ribarGraficosVendas.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarGraficosVendas.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnGrafVendasDiarias,
            this.btnGrafVendasMensal,
            this.btnGrafInlocDelivery,
            this.btnGrafCanceladas});
            this.ribarGraficosVendas.Location = new System.Drawing.Point(3, 0);
            this.ribarGraficosVendas.Name = "ribarGraficosVendas";
            this.ribarGraficosVendas.Size = new System.Drawing.Size(254, 98);
            this.ribarGraficosVendas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarGraficosVendas.TabIndex = 0;
            this.ribarGraficosVendas.Text = "Vendas";
            // 
            // 
            // 
            this.ribarGraficosVendas.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarGraficosVendas.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnGrafVendasDiarias
            // 
            this.btnGrafVendasDiarias.EnableImageAnimation = true;
            this.btnGrafVendasDiarias.Image = global::UI.Properties.Resources._1411530971_f_analytics_256_128;
            this.btnGrafVendasDiarias.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafVendasDiarias.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafVendasDiarias.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafVendasDiarias.Name = "btnGrafVendasDiarias";
            this.btnGrafVendasDiarias.SubItemsExpandWidth = 14;
            this.btnGrafVendasDiarias.Text = "Diarias";
            this.btnGrafVendasDiarias.Tooltip = "Gráfico de Vendas Diarias";
            this.btnGrafVendasDiarias.Click += new System.EventHandler(this.btnGrafVendasDiarias_Click);
            // 
            // btnGrafVendasMensal
            // 
            this.btnGrafVendasMensal.EnableImageAnimation = true;
            this.btnGrafVendasMensal.Image = global::UI.Properties.Resources._1411531148_Line_chart;
            this.btnGrafVendasMensal.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafVendasMensal.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafVendasMensal.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafVendasMensal.Name = "btnGrafVendasMensal";
            this.btnGrafVendasMensal.SubItemsExpandWidth = 14;
            this.btnGrafVendasMensal.Text = "Mensais";
            this.btnGrafVendasMensal.Tooltip = "Gráfico de Vendas Mensais";
            this.btnGrafVendasMensal.Click += new System.EventHandler(this.btnGrafVendasMensal_Click);
            // 
            // btnGrafInlocDelivery
            // 
            this.btnGrafInlocDelivery.EnableImageAnimation = true;
            this.btnGrafInlocDelivery.Image = global::UI.Properties.Resources._1411531209_pie_chart;
            this.btnGrafInlocDelivery.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafInlocDelivery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafInlocDelivery.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafInlocDelivery.Name = "btnGrafInlocDelivery";
            this.btnGrafInlocDelivery.SubItemsExpandWidth = 14;
            this.btnGrafInlocDelivery.Text = "Atendimento";
            this.btnGrafInlocDelivery.Tooltip = "Graficos de Tipos de Atendimento";
            this.btnGrafInlocDelivery.Click += new System.EventHandler(this.btnGrafInlocDelivery_Click);
            // 
            // btnGrafCanceladas
            // 
            this.btnGrafCanceladas.EnableImageAnimation = true;
            this.btnGrafCanceladas.Image = global::UI.Properties.Resources._1411530926_pie_chart;
            this.btnGrafCanceladas.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGrafCanceladas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrafCanceladas.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrafCanceladas.Name = "btnGrafCanceladas";
            this.btnGrafCanceladas.SubItemsExpandWidth = 14;
            this.btnGrafCanceladas.Text = "Canceladas";
            this.btnGrafCanceladas.Tooltip = "Gráfico de Vendas Canceladas";
            this.btnGrafCanceladas.Click += new System.EventHandler(this.btnGrafCanceladas_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel4.Controls.Add(this.ribarAperencia);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 54);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel4.Size = new System.Drawing.Size(1189, 100);
            // 
            // 
            // 
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 4;
            this.ribbonPanel4.Visible = false;
            // 
            // ribarAperencia
            // 
            this.ribarAperencia.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribarAperencia.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarAperencia.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarAperencia.ContainerControlProcessDialogKey = true;
            this.ribarAperencia.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribarAperencia.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMetro,
            this.btnOffice2010Black,
            this.btnOffice2010Blue,
            this.btnOffice2010Silver,
            this.btnOffice2007Black,
            this.btnOffice2007Blue,
            this.btnOffice2007Silver,
            this.btnVistaClass,
            this.btnVisualStudio2010,
            this.btnVisualStudio2012Ligth,
            this.btnVisualStudio2012Black,
            this.btnWindowsBlue});
            this.ribarAperencia.Location = new System.Drawing.Point(3, 0);
            this.ribarAperencia.Name = "ribarAperencia";
            this.ribarAperencia.Size = new System.Drawing.Size(1190, 98);
            this.ribarAperencia.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribarAperencia.TabIndex = 0;
            // 
            // 
            // 
            this.ribarAperencia.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribarAperencia.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribarAperencia.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnMetro
            // 
            this.btnMetro.AccessibleRole = System.Windows.Forms.AccessibleRole.ColumnHeader;
            this.btnMetro.EnableImageAnimation = true;
            this.btnMetro.Image = global::UI.Properties.Resources.metro;
            this.btnMetro.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnMetro.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMetro.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnMetro.Name = "btnMetro";
            this.btnMetro.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer5});
            this.btnMetro.SubItemsExpandWidth = 14;
            this.btnMetro.Text = "Metro";
            this.btnMetro.Click += new System.EventHandler(this.btnMetro_Click);
            // 
            // itemContainer5
            // 
            this.itemContainer5.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
            // 
            // 
            // 
            this.itemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.BeginGroup = true;
            this.itemContainer5.FixedSize = new System.Drawing.Size(600, 160);
            this.itemContainer5.MultiLine = true;
            this.itemContainer5.Name = "itemContainer5";
            this.itemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBlackClouds,
            this.btnBlackLilac,
            this.btnBlackSky,
            this.btnBlue,
            this.btnBlueBrown,
            this.btnBorderAux,
            this.btnBrown,
            this.btnChery,
            this.btnDarkBrown,
            this.btnDarkPurple,
            this.btnDarkRed,
            this.btnDefault,
            this.btnEarlyMaroon,
            this.btnEarlyRed,
            this.btnExpresso,
            this.btnFlorestGreen,
            this.btnGrayOrange,
            this.btnGreen,
            this.btnLatter,
            this.btnMaroonSilver,
            this.btnOrange,
            this.btnPowerRed,
            this.btnPurple,
            this.btnRed,
            this.btnRedAmplifield,
            this.btnRust,
            this.btnSilverBlues,
            this.btnSilverGreen,
            this.btnSkyGreen,
            this.btnViStudioDark});
            // 
            // 
            // 
            this.itemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnBlackClouds
            // 
            this.btnBlackClouds.EnableImageAnimation = true;
            this.btnBlackClouds.Image = global::UI.Properties.Resources.blackClouds;
            this.btnBlackClouds.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBlackClouds.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBlackClouds.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBlackClouds.Name = "btnBlackClouds";
            this.btnBlackClouds.SubItemsExpandWidth = 14;
            this.btnBlackClouds.Text = "BlackClouds";
            this.btnBlackClouds.Tooltip = "Relatório de Vendas";
            this.btnBlackClouds.Click += new System.EventHandler(this.btnBlackClouds_Click);
            // 
            // btnBlackLilac
            // 
            this.btnBlackLilac.EnableImageAnimation = true;
            this.btnBlackLilac.Image = global::UI.Properties.Resources.blackLilac;
            this.btnBlackLilac.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBlackLilac.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBlackLilac.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBlackLilac.Name = "btnBlackLilac";
            this.btnBlackLilac.SubItemsExpandWidth = 14;
            this.btnBlackLilac.Text = "BlackLilac";
            this.btnBlackLilac.Tooltip = "Relatório de Vendas";
            this.btnBlackLilac.Click += new System.EventHandler(this.btnBlackLilac_Click);
            // 
            // btnBlackSky
            // 
            this.btnBlackSky.EnableImageAnimation = true;
            this.btnBlackSky.Image = global::UI.Properties.Resources.blackSky;
            this.btnBlackSky.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBlackSky.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBlackSky.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBlackSky.Name = "btnBlackSky";
            this.btnBlackSky.SubItemsExpandWidth = 14;
            this.btnBlackSky.Text = "BlackSky";
            this.btnBlackSky.Tooltip = "Relatório de Vendas";
            this.btnBlackSky.Click += new System.EventHandler(this.btnBlackSky_Click);
            // 
            // btnBlue
            // 
            this.btnBlue.EnableImageAnimation = true;
            this.btnBlue.Image = global::UI.Properties.Resources.Blue;
            this.btnBlue.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBlue.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBlue.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBlue.Name = "btnBlue";
            this.btnBlue.SubItemsExpandWidth = 14;
            this.btnBlue.Text = "Blue";
            this.btnBlue.Tooltip = "Relatório de Vendas";
            this.btnBlue.Click += new System.EventHandler(this.btnBlue_Click);
            // 
            // btnBlueBrown
            // 
            this.btnBlueBrown.EnableImageAnimation = true;
            this.btnBlueBrown.Image = global::UI.Properties.Resources.blueishbrow;
            this.btnBlueBrown.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBlueBrown.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBlueBrown.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBlueBrown.Name = "btnBlueBrown";
            this.btnBlueBrown.SubItemsExpandWidth = 14;
            this.btnBlueBrown.Text = "BlueBrown";
            this.btnBlueBrown.Tooltip = "Relatório de Vendas";
            this.btnBlueBrown.Click += new System.EventHandler(this.btnBlueBrown_Click);
            // 
            // btnBorderAux
            // 
            this.btnBorderAux.EnableImageAnimation = true;
            this.btnBorderAux.Image = global::UI.Properties.Resources.borderaux;
            this.btnBorderAux.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBorderAux.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBorderAux.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBorderAux.Name = "btnBorderAux";
            this.btnBorderAux.SubItemsExpandWidth = 14;
            this.btnBorderAux.Text = "BorderAux";
            this.btnBorderAux.Tooltip = "Relatório de Vendas";
            this.btnBorderAux.Click += new System.EventHandler(this.btnBorderAux_Click);
            // 
            // btnBrown
            // 
            this.btnBrown.EnableImageAnimation = true;
            this.btnBrown.Image = global::UI.Properties.Resources.brown;
            this.btnBrown.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnBrown.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBrown.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnBrown.Name = "btnBrown";
            this.btnBrown.SubItemsExpandWidth = 14;
            this.btnBrown.Text = "Brown";
            this.btnBrown.Tooltip = "Relatório de Vendas";
            // 
            // btnChery
            // 
            this.btnChery.EnableImageAnimation = true;
            this.btnChery.Image = global::UI.Properties.Resources.Cherry;
            this.btnChery.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnChery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnChery.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnChery.Name = "btnChery";
            this.btnChery.SubItemsExpandWidth = 14;
            this.btnChery.Text = "Chery";
            this.btnChery.Tooltip = "Relatório de Vendas";
            this.btnChery.Click += new System.EventHandler(this.btnChery_Click);
            // 
            // btnDarkBrown
            // 
            this.btnDarkBrown.EnableImageAnimation = true;
            this.btnDarkBrown.Image = global::UI.Properties.Resources.darkBrown;
            this.btnDarkBrown.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnDarkBrown.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDarkBrown.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDarkBrown.Name = "btnDarkBrown";
            this.btnDarkBrown.SubItemsExpandWidth = 14;
            this.btnDarkBrown.Text = "DarkBrown";
            this.btnDarkBrown.Tooltip = "Relatório de Vendas";
            this.btnDarkBrown.Click += new System.EventHandler(this.btnDarkBrown_Click);
            // 
            // btnDarkPurple
            // 
            this.btnDarkPurple.EnableImageAnimation = true;
            this.btnDarkPurple.Image = global::UI.Properties.Resources.darkPurple;
            this.btnDarkPurple.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnDarkPurple.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDarkPurple.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDarkPurple.Name = "btnDarkPurple";
            this.btnDarkPurple.SubItemsExpandWidth = 14;
            this.btnDarkPurple.Text = "DarkPurple";
            this.btnDarkPurple.Tooltip = "Relatório de Vendas";
            this.btnDarkPurple.Click += new System.EventHandler(this.btnDarkPurple_Click);
            // 
            // btnDarkRed
            // 
            this.btnDarkRed.EnableImageAnimation = true;
            this.btnDarkRed.Image = global::UI.Properties.Resources.darkRed;
            this.btnDarkRed.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnDarkRed.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDarkRed.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDarkRed.Name = "btnDarkRed";
            this.btnDarkRed.SubItemsExpandWidth = 14;
            this.btnDarkRed.Text = "DarkRed";
            this.btnDarkRed.Tooltip = "Relatório de Vendas";
            this.btnDarkRed.Click += new System.EventHandler(this.btnDarkRed_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.EnableImageAnimation = true;
            this.btnDefault.Image = global::UI.Properties.Resources._default;
            this.btnDefault.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnDefault.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDefault.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.SubItemsExpandWidth = 14;
            this.btnDefault.Text = "Default";
            this.btnDefault.Tooltip = "Relatório de Vendas";
            // 
            // btnEarlyMaroon
            // 
            this.btnEarlyMaroon.EnableImageAnimation = true;
            this.btnEarlyMaroon.Image = global::UI.Properties.Resources.earlyMaroon;
            this.btnEarlyMaroon.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnEarlyMaroon.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEarlyMaroon.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnEarlyMaroon.Name = "btnEarlyMaroon";
            this.btnEarlyMaroon.SubItemsExpandWidth = 14;
            this.btnEarlyMaroon.Text = "EarlyMaroon";
            this.btnEarlyMaroon.Tooltip = "Relatório de Vendas";
            this.btnEarlyMaroon.Click += new System.EventHandler(this.btnEarlyMaroon_Click);
            // 
            // btnEarlyRed
            // 
            this.btnEarlyRed.EnableImageAnimation = true;
            this.btnEarlyRed.Image = global::UI.Properties.Resources.earlyRead;
            this.btnEarlyRed.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnEarlyRed.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEarlyRed.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnEarlyRed.Name = "btnEarlyRed";
            this.btnEarlyRed.SubItemsExpandWidth = 14;
            this.btnEarlyRed.Text = "EarlyRed";
            this.btnEarlyRed.Tooltip = "Relatório de Vendas";
            this.btnEarlyRed.Click += new System.EventHandler(this.btnEarlyRed_Click);
            // 
            // btnExpresso
            // 
            this.btnExpresso.EnableImageAnimation = true;
            this.btnExpresso.Image = global::UI.Properties.Resources.expresso;
            this.btnExpresso.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnExpresso.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnExpresso.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnExpresso.Name = "btnExpresso";
            this.btnExpresso.SubItemsExpandWidth = 14;
            this.btnExpresso.Text = "Expresso";
            this.btnExpresso.Tooltip = "Relatório de Vendas";
            this.btnExpresso.Click += new System.EventHandler(this.btnExpresso_Click);
            // 
            // btnFlorestGreen
            // 
            this.btnFlorestGreen.EnableImageAnimation = true;
            this.btnFlorestGreen.Image = global::UI.Properties.Resources.florestGreen;
            this.btnFlorestGreen.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnFlorestGreen.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnFlorestGreen.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnFlorestGreen.Name = "btnFlorestGreen";
            this.btnFlorestGreen.SubItemsExpandWidth = 14;
            this.btnFlorestGreen.Text = "FlorestGreen";
            this.btnFlorestGreen.Tooltip = "Relatório de Vendas";
            this.btnFlorestGreen.Click += new System.EventHandler(this.btnFlorestGreen_Click);
            // 
            // btnGrayOrange
            // 
            this.btnGrayOrange.EnableImageAnimation = true;
            this.btnGrayOrange.Image = global::UI.Properties.Resources.grayOrange;
            this.btnGrayOrange.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnGrayOrange.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGrayOrange.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGrayOrange.Name = "btnGrayOrange";
            this.btnGrayOrange.SubItemsExpandWidth = 14;
            this.btnGrayOrange.Text = "GrayOrange";
            this.btnGrayOrange.Tooltip = "Relatório de Vendas";
            this.btnGrayOrange.Click += new System.EventHandler(this.btnGrayOrange_Click);
            // 
            // btnGreen
            // 
            this.btnGreen.EnableImageAnimation = true;
            this.btnGreen.Image = global::UI.Properties.Resources.green;
            this.btnGreen.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnGreen.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGreen.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnGreen.Name = "btnGreen";
            this.btnGreen.SubItemsExpandWidth = 14;
            this.btnGreen.Text = "Green";
            this.btnGreen.Tooltip = "Relatório de Vendas";
            this.btnGreen.Click += new System.EventHandler(this.btnGreen_Click);
            // 
            // btnLatter
            // 
            this.btnLatter.EnableImageAnimation = true;
            this.btnLatter.Image = global::UI.Properties.Resources.latte;
            this.btnLatter.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnLatter.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLatter.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnLatter.Name = "btnLatter";
            this.btnLatter.SubItemsExpandWidth = 14;
            this.btnLatter.Text = "Latter";
            this.btnLatter.Tooltip = "Relatório de Vendas";
            this.btnLatter.Click += new System.EventHandler(this.btnLatter_Click);
            // 
            // btnMaroonSilver
            // 
            this.btnMaroonSilver.EnableImageAnimation = true;
            this.btnMaroonSilver.Image = global::UI.Properties.Resources.maroonSilver;
            this.btnMaroonSilver.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnMaroonSilver.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMaroonSilver.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnMaroonSilver.Name = "btnMaroonSilver";
            this.btnMaroonSilver.SubItemsExpandWidth = 14;
            this.btnMaroonSilver.Text = "MaroonSilver";
            this.btnMaroonSilver.Tooltip = "Relatório de Vendas";
            this.btnMaroonSilver.Click += new System.EventHandler(this.btnMaroonSilver_Click);
            // 
            // btnOrange
            // 
            this.btnOrange.EnableImageAnimation = true;
            this.btnOrange.Image = global::UI.Properties.Resources.orange;
            this.btnOrange.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnOrange.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOrange.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOrange.Name = "btnOrange";
            this.btnOrange.SubItemsExpandWidth = 14;
            this.btnOrange.Text = "Orange";
            this.btnOrange.Tooltip = "Relatório de Vendas";
            this.btnOrange.Click += new System.EventHandler(this.btnOrange_Click);
            // 
            // btnPowerRed
            // 
            this.btnPowerRed.EnableImageAnimation = true;
            this.btnPowerRed.Image = global::UI.Properties.Resources.powderRed;
            this.btnPowerRed.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnPowerRed.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPowerRed.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPowerRed.Name = "btnPowerRed";
            this.btnPowerRed.SubItemsExpandWidth = 14;
            this.btnPowerRed.Text = "PowerRed";
            this.btnPowerRed.Tooltip = "Relatório de Vendas";
            this.btnPowerRed.Click += new System.EventHandler(this.btnPowerRed_Click);
            // 
            // btnPurple
            // 
            this.btnPurple.EnableImageAnimation = true;
            this.btnPurple.Image = global::UI.Properties.Resources.Purple;
            this.btnPurple.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnPurple.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPurple.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPurple.Name = "btnPurple";
            this.btnPurple.SubItemsExpandWidth = 14;
            this.btnPurple.Text = "Purple";
            this.btnPurple.Tooltip = "Relatório de Vendas";
            this.btnPurple.Click += new System.EventHandler(this.btnPurple_Click);
            // 
            // btnRed
            // 
            this.btnRed.EnableImageAnimation = true;
            this.btnRed.Image = global::UI.Properties.Resources.red;
            this.btnRed.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnRed.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRed.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRed.Name = "btnRed";
            this.btnRed.SubItemsExpandWidth = 14;
            this.btnRed.Text = "Red";
            this.btnRed.Tooltip = "Relatório de Vendas";
            this.btnRed.Click += new System.EventHandler(this.btnRed_Click);
            // 
            // btnRedAmplifield
            // 
            this.btnRedAmplifield.EnableImageAnimation = true;
            this.btnRedAmplifield.Image = global::UI.Properties.Resources.redAmplifield;
            this.btnRedAmplifield.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnRedAmplifield.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRedAmplifield.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRedAmplifield.Name = "btnRedAmplifield";
            this.btnRedAmplifield.SubItemsExpandWidth = 14;
            this.btnRedAmplifield.Text = "RedAmplifield";
            this.btnRedAmplifield.Tooltip = "Relatório de Vendas";
            this.btnRedAmplifield.Click += new System.EventHandler(this.btnRedAmplifield_Click);
            // 
            // btnRust
            // 
            this.btnRust.EnableImageAnimation = true;
            this.btnRust.Image = global::UI.Properties.Resources.Rust;
            this.btnRust.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnRust.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRust.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRust.Name = "btnRust";
            this.btnRust.SubItemsExpandWidth = 14;
            this.btnRust.Text = "Rust";
            this.btnRust.Tooltip = "Relatório de Vendas";
            this.btnRust.Click += new System.EventHandler(this.btnRust_Click);
            // 
            // btnSilverBlues
            // 
            this.btnSilverBlues.EnableImageAnimation = true;
            this.btnSilverBlues.Image = global::UI.Properties.Resources.silverBlues;
            this.btnSilverBlues.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnSilverBlues.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSilverBlues.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSilverBlues.Name = "btnSilverBlues";
            this.btnSilverBlues.SubItemsExpandWidth = 14;
            this.btnSilverBlues.Text = "SilverBlues";
            this.btnSilverBlues.Tooltip = "Relatório de Vendas";
            this.btnSilverBlues.Click += new System.EventHandler(this.btnSilverBlues_Click);
            // 
            // btnSilverGreen
            // 
            this.btnSilverGreen.EnableImageAnimation = true;
            this.btnSilverGreen.Image = global::UI.Properties.Resources.silverGrenn;
            this.btnSilverGreen.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnSilverGreen.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSilverGreen.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSilverGreen.Name = "btnSilverGreen";
            this.btnSilverGreen.SubItemsExpandWidth = 14;
            this.btnSilverGreen.Text = "SilverGreen";
            this.btnSilverGreen.Tooltip = "Relatório de Vendas";
            this.btnSilverGreen.Click += new System.EventHandler(this.btnSilverGreen_Click);
            // 
            // btnSkyGreen
            // 
            this.btnSkyGreen.EnableImageAnimation = true;
            this.btnSkyGreen.Image = global::UI.Properties.Resources.skyGreen;
            this.btnSkyGreen.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnSkyGreen.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSkyGreen.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSkyGreen.Name = "btnSkyGreen";
            this.btnSkyGreen.SubItemsExpandWidth = 14;
            this.btnSkyGreen.Text = "SkyGreen";
            this.btnSkyGreen.Tooltip = "Relatório de Vendas";
            this.btnSkyGreen.Click += new System.EventHandler(this.btnSkyGreen_Click);
            // 
            // btnViStudioDark
            // 
            this.btnViStudioDark.EnableImageAnimation = true;
            this.btnViStudioDark.Image = global::UI.Properties.Resources.visualStudioDark;
            this.btnViStudioDark.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnViStudioDark.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnViStudioDark.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnViStudioDark.Name = "btnViStudioDark";
            this.btnViStudioDark.SubItemsExpandWidth = 14;
            this.btnViStudioDark.Text = "ViStudioDark";
            this.btnViStudioDark.Tooltip = "Relatório de Vendas";
            this.btnViStudioDark.Click += new System.EventHandler(this.btnViStudioDark_Click);
            // 
            // btnOffice2010Black
            // 
            this.btnOffice2010Black.EnableImageAnimation = true;
            this.btnOffice2010Black.Image = global::UI.Properties.Resources.office2010Black;
            this.btnOffice2010Black.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOffice2010Black.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffice2010Black.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOffice2010Black.Name = "btnOffice2010Black";
            this.btnOffice2010Black.SubItemsExpandWidth = 14;
            this.btnOffice2010Black.Text = "Office 2010 Black";
            this.btnOffice2010Black.Click += new System.EventHandler(this.btnOffice2010Black_Click);
            // 
            // btnOffice2010Blue
            // 
            this.btnOffice2010Blue.EnableImageAnimation = true;
            this.btnOffice2010Blue.Image = global::UI.Properties.Resources.Office2010Blue;
            this.btnOffice2010Blue.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOffice2010Blue.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffice2010Blue.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOffice2010Blue.Name = "btnOffice2010Blue";
            this.btnOffice2010Blue.SubItemsExpandWidth = 14;
            this.btnOffice2010Blue.Text = "Office 2010 Blue";
            this.btnOffice2010Blue.Click += new System.EventHandler(this.btnOffice2010Blue_Click);
            // 
            // btnOffice2010Silver
            // 
            this.btnOffice2010Silver.EnableImageAnimation = true;
            this.btnOffice2010Silver.Image = global::UI.Properties.Resources.Office2010Silver;
            this.btnOffice2010Silver.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOffice2010Silver.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffice2010Silver.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOffice2010Silver.Name = "btnOffice2010Silver";
            this.btnOffice2010Silver.SubItemsExpandWidth = 14;
            this.btnOffice2010Silver.Text = "Office 2010 Silver";
            this.btnOffice2010Silver.Click += new System.EventHandler(this.btnOffice2010Silver_Click);
            // 
            // btnOffice2007Black
            // 
            this.btnOffice2007Black.EnableImageAnimation = true;
            this.btnOffice2007Black.Image = global::UI.Properties.Resources.officeBlack1;
            this.btnOffice2007Black.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOffice2007Black.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffice2007Black.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOffice2007Black.Name = "btnOffice2007Black";
            this.btnOffice2007Black.SubItemsExpandWidth = 14;
            this.btnOffice2007Black.Text = "Office 2007 Black";
            this.btnOffice2007Black.Click += new System.EventHandler(this.btnOffice2007Black_Click);
            // 
            // btnOffice2007Blue
            // 
            this.btnOffice2007Blue.EnableImageAnimation = true;
            this.btnOffice2007Blue.Image = global::UI.Properties.Resources.officeBlue;
            this.btnOffice2007Blue.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOffice2007Blue.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffice2007Blue.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOffice2007Blue.Name = "btnOffice2007Blue";
            this.btnOffice2007Blue.SubItemsExpandWidth = 14;
            this.btnOffice2007Blue.Text = "Office 2007 Blue";
            this.btnOffice2007Blue.Click += new System.EventHandler(this.btnOffice2007Blue_Click);
            // 
            // btnOffice2007Silver
            // 
            this.btnOffice2007Silver.EnableImageAnimation = true;
            this.btnOffice2007Silver.Image = global::UI.Properties.Resources.officeSilver;
            this.btnOffice2007Silver.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOffice2007Silver.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffice2007Silver.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOffice2007Silver.Name = "btnOffice2007Silver";
            this.btnOffice2007Silver.SubItemsExpandWidth = 14;
            this.btnOffice2007Silver.Text = "Office 2007 Silver";
            this.btnOffice2007Silver.Click += new System.EventHandler(this.btnOffice2007Silver_Click);
            // 
            // btnVistaClass
            // 
            this.btnVistaClass.EnableImageAnimation = true;
            this.btnVistaClass.Image = global::UI.Properties.Resources.VistaClass;
            this.btnVistaClass.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnVistaClass.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVistaClass.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVistaClass.Name = "btnVistaClass";
            this.btnVistaClass.SubItemsExpandWidth = 14;
            this.btnVistaClass.Text = "Office Vista Class";
            this.btnVistaClass.Click += new System.EventHandler(this.btnVistaClass_Click);
            // 
            // btnVisualStudio2010
            // 
            this.btnVisualStudio2010.EnableImageAnimation = true;
            this.btnVisualStudio2010.Image = global::UI.Properties.Resources.visualStudio2010;
            this.btnVisualStudio2010.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnVisualStudio2010.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVisualStudio2010.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVisualStudio2010.Name = "btnVisualStudio2010";
            this.btnVisualStudio2010.SubItemsExpandWidth = 14;
            this.btnVisualStudio2010.Text = "Visual Studio 2010";
            this.btnVisualStudio2010.Click += new System.EventHandler(this.btnVisualStudio2010_Click);
            // 
            // btnVisualStudio2012Ligth
            // 
            this.btnVisualStudio2012Ligth.EnableImageAnimation = true;
            this.btnVisualStudio2012Ligth.Image = global::UI.Properties.Resources.visualStudio2012Light;
            this.btnVisualStudio2012Ligth.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnVisualStudio2012Ligth.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVisualStudio2012Ligth.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVisualStudio2012Ligth.Name = "btnVisualStudio2012Ligth";
            this.btnVisualStudio2012Ligth.SubItemsExpandWidth = 14;
            this.btnVisualStudio2012Ligth.Text = "Visual Studio 2012 Ligth";
            this.btnVisualStudio2012Ligth.Click += new System.EventHandler(this.btnVisualStudio2012Ligth_Click);
            // 
            // btnVisualStudio2012Black
            // 
            this.btnVisualStudio2012Black.EnableImageAnimation = true;
            this.btnVisualStudio2012Black.Image = global::UI.Properties.Resources.VisualStudio2012Black;
            this.btnVisualStudio2012Black.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnVisualStudio2012Black.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVisualStudio2012Black.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVisualStudio2012Black.Name = "btnVisualStudio2012Black";
            this.btnVisualStudio2012Black.SubItemsExpandWidth = 14;
            this.btnVisualStudio2012Black.Text = "Visual Studio 2012 Black";
            this.btnVisualStudio2012Black.Click += new System.EventHandler(this.btnVisualStudio2012Black_Click);
            // 
            // btnWindowsBlue
            // 
            this.btnWindowsBlue.EnableImageAnimation = true;
            this.btnWindowsBlue.Image = global::UI.Properties.Resources.windows7Blue;
            this.btnWindowsBlue.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnWindowsBlue.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnWindowsBlue.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnWindowsBlue.Name = "btnWindowsBlue";
            this.btnWindowsBlue.SubItemsExpandWidth = 14;
            this.btnWindowsBlue.Text = "Windows Blue";
            this.btnWindowsBlue.Click += new System.EventHandler(this.btnWindowsBlue_Click);
            // 
            // applicationButton1
            // 
            this.applicationButton1.AutoExpandOnClick = true;
            this.applicationButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.applicationButton1.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.applicationButton1.ImagePaddingHorizontal = 0;
            this.applicationButton1.ImagePaddingVertical = 1;
            this.applicationButton1.Name = "applicationButton1";
            this.applicationButton1.PopupWidth = 400;
            this.applicationButton1.ShowSubItems = false;
            this.applicationButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.applicationButton1.Text = "Menu";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2});
            // 
            // 
            // 
            this.itemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3});
            // 
            // 
            // 
            this.itemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnCadLancheFacil,
            this.btnCadImpressao,
            this.btnMenuBackup,
            this.itemContainer4});
            // 
            // 
            // 
            this.itemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnCadLancheFacil
            // 
            this.btnCadLancheFacil.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCadLancheFacil.Image = global::UI.Properties.Resources.Settings;
            this.btnCadLancheFacil.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCadLancheFacil.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Default;
            this.btnCadLancheFacil.ImagePaddingHorizontal = 10;
            this.btnCadLancheFacil.Name = "btnCadLancheFacil";
            this.btnCadLancheFacil.Text = "Configurações Food Facil";
            this.btnCadLancheFacil.Click += new System.EventHandler(this.btnCadMeuLanche_Click);
            // 
            // btnCadImpressao
            // 
            this.btnCadImpressao.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCadImpressao.Image = global::UI.Properties.Resources.New_Printer;
            this.btnCadImpressao.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCadImpressao.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Default;
            this.btnCadImpressao.ImagePaddingHorizontal = 10;
            this.btnCadImpressao.Name = "btnCadImpressao";
            this.btnCadImpressao.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ckbImpressaoPapel,
            this.ckbImpressaoTela});
            this.btnCadImpressao.Text = "Configuração de Impressão";
            this.btnCadImpressao.Click += new System.EventHandler(this.btnCadImpressao_Click);
            // 
            // ckbImpressaoPapel
            // 
            this.ckbImpressaoPapel.Name = "ckbImpressaoPapel";
            this.ckbImpressaoPapel.Text = "Impressão no Papel";
            this.ckbImpressaoPapel.Click += new System.EventHandler(this.ckbImpressaoPapel_Click);
            // 
            // ckbImpressaoTela
            // 
            this.ckbImpressaoTela.Name = "ckbImpressaoTela";
            this.ckbImpressaoTela.Text = "Impressão na Tela";
            this.ckbImpressaoTela.Click += new System.EventHandler(this.ckbImpressaoTela_Click);
            // 
            // btnMenuBackup
            // 
            this.btnMenuBackup.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMenuBackup.Image = global::UI.Properties.Resources.database_add;
            this.btnMenuBackup.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnMenuBackup.ImagePaddingHorizontal = 10;
            this.btnMenuBackup.Name = "btnMenuBackup";
            this.btnMenuBackup.SubItemsExpandWidth = 24;
            this.btnMenuBackup.Text = "&Backup do Sistema";
            this.btnMenuBackup.Tooltip = "Fazer Backup do Banco de Dados do Sistema";
            this.btnMenuBackup.Click += new System.EventHandler(this.btnMenuBackup_Click);
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnISair});
            // 
            // 
            // 
            this.itemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnISair
            // 
            this.btnISair.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnISair.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnISair.Image = ((System.Drawing.Image)(resources.GetObject("btnISair.Image")));
            this.btnISair.Name = "btnISair";
            this.btnISair.SubItemsExpandWidth = 24;
            this.btnISair.Text = "Sair";
            this.btnISair.Click += new System.EventHandler(this.btnISair_Click);
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Panel = this.ribbonPanel1;
            this.ribbonTabItem1.Text = "Cadastros";
            // 
            // ribbonTabItem2
            // 
            this.ribbonTabItem2.Checked = true;
            this.ribbonTabItem2.Name = "ribbonTabItem2";
            this.ribbonTabItem2.Panel = this.ribbonPanel2;
            this.ribbonTabItem2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1,
            this.buttonItem7,
            this.buttonItem6,
            this.buttonItem4,
            this.buttonItem3});
            this.ribbonTabItem2.Text = "Movimentos";
            // 
            // buttonItem1
            // 
            this.buttonItem1.EnableImageAnimation = true;
            this.buttonItem1.Image = global::UI.Properties.Resources.entrega;
            this.buttonItem1.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem1.ImagePaddingHorizontal = 10;
            this.buttonItem1.ImagePaddingVertical = 2;
            this.buttonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItemsExpandWidth = 14;
            this.buttonItem1.Text = "Delivery";
            this.buttonItem1.Tooltip = "Atendimento Delivery";
            // 
            // buttonItem7
            // 
            this.buttonItem7.EnableImageAnimation = true;
            this.buttonItem7.Image = global::UI.Properties.Resources.ENTREGADOR;
            this.buttonItem7.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.buttonItem7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem7.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItemsExpandWidth = 14;
            this.buttonItem7.Text = "Entregadores";
            this.buttonItem7.Tooltip = "Casdastrar Entregadores";
            // 
            // buttonItem6
            // 
            this.buttonItem6.EnableImageAnimation = true;
            this.buttonItem6.Image = global::UI.Properties.Resources._7393_128x128;
            this.buttonItem6.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.buttonItem6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem6.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.SubItemsExpandWidth = 14;
            this.buttonItem6.Text = "Produtos";
            this.buttonItem6.Tooltip = "Casdastrar Produtos";
            // 
            // buttonItem4
            // 
            this.buttonItem4.EnableImageAnimation = true;
            this.buttonItem4.Image = global::UI.Properties.Resources.entrega;
            this.buttonItem4.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem4.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 14;
            this.buttonItem4.Text = "Delivery";
            this.buttonItem4.Tooltip = "Relatorio Delivery";
            // 
            // buttonItem3
            // 
            this.buttonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem3.Image = global::UI.Properties.Resources.New_Printer;
            this.buttonItem3.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.buttonItem3.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Default;
            this.buttonItem3.ImagePaddingHorizontal = 10;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Text = "Configuração de Impressão";
            // 
            // Financeiro
            // 
            this.Financeiro.Name = "Financeiro";
            this.Financeiro.Panel = this.ribbonPanel5;
            this.Financeiro.Text = "Financeiro";
            // 
            // rtRelatorios
            // 
            this.rtRelatorios.Name = "rtRelatorios";
            this.rtRelatorios.Panel = this.ribbonPanel3;
            this.rtRelatorios.Text = "Relatórios";
            // 
            // rtGraficos
            // 
            this.rtGraficos.Name = "rtGraficos";
            this.rtGraficos.Panel = this.ribbonPanel6;
            this.rtGraficos.Text = "Gráficos";
            // 
            // Stilo
            // 
            this.Stilo.Name = "Stilo";
            this.Stilo.Panel = this.ribbonPanel4;
            this.Stilo.Text = "Aparência";
            // 
            // stiloPrincipal
            // 
            this.stiloPrincipal.ManagerColorTint = System.Drawing.Color.Red;
            this.stiloPrincipal.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.stiloPrincipal.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.Blue;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslUsuarioNome,
            this.tsslIp,
            this.tsslDataAtual});
            this.statusStrip1.Location = new System.Drawing.Point(5, 608);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1314, 35);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslUsuarioNome
            // 
            this.tsslUsuarioNome.BackColor = System.Drawing.Color.Blue;
            this.tsslUsuarioNome.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslUsuarioNome.ForeColor = System.Drawing.Color.White;
            this.tsslUsuarioNome.Image = global::UI.Properties.Resources._1392676071_user;
            this.tsslUsuarioNome.Margin = new System.Windows.Forms.Padding(2);
            this.tsslUsuarioNome.Name = "tsslUsuarioNome";
            this.tsslUsuarioNome.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsslUsuarioNome.Size = new System.Drawing.Size(93, 31);
            this.tsslUsuarioNome.Text = "Usuário: ";
            // 
            // tsslIp
            // 
            this.tsslIp.BackColor = System.Drawing.Color.Blue;
            this.tsslIp.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslIp.ForeColor = System.Drawing.Color.White;
            this.tsslIp.Image = global::UI.Properties.Resources.computer1;
            this.tsslIp.Margin = new System.Windows.Forms.Padding(2);
            this.tsslIp.Name = "tsslIp";
            this.tsslIp.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsslIp.Size = new System.Drawing.Size(113, 31);
            this.tsslIp.Text = "IP Maquina: ";
            // 
            // tsslDataAtual
            // 
            this.tsslDataAtual.BackColor = System.Drawing.Color.Blue;
            this.tsslDataAtual.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslDataAtual.ForeColor = System.Drawing.Color.White;
            this.tsslDataAtual.Image = global::UI.Properties.Resources.Calendar;
            this.tsslDataAtual.Margin = new System.Windows.Forms.Padding(2);
            this.tsslDataAtual.Name = "tsslDataAtual";
            this.tsslDataAtual.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsslDataAtual.Size = new System.Drawing.Size(74, 31);
            this.tsslDataAtual.Text = "Data:";
            // 
            // microChartItem1
            // 
            this.microChartItem1.Name = "microChartItem1";
            // 
            // ofDialog
            // 
            this.ofDialog.Title = "Localizar Imagens";
            // 
            // microChartItem2
            // 
            this.microChartItem2.Name = "microChartItem2";
            // 
            // pbLogoPrincipal
            // 
            this.pbLogoPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogoPrincipal.Location = new System.Drawing.Point(1, 157);
            this.pbLogoPrincipal.Name = "pbLogoPrincipal";
            this.pbLogoPrincipal.Size = new System.Drawing.Size(1322, 448);
            this.pbLogoPrincipal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbLogoPrincipal.TabIndex = 2;
            this.pbLogoPrincipal.TabStop = false;
            // 
            // Principal
            // 
            this.ClientSize = new System.Drawing.Size(1324, 645);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.pbLogoPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DR Sistemas - Food Facil";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Principal_FormClosed);
            this.Load += new System.EventHandler(this.Principal_Load);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel5.ResumeLayout(false);
            this.ribbonPanel6.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoPrincipal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.ApplicationButton applicationButton1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem2;
        private DevComponents.DotNetBar.RibbonBar ribarCadastroPessoas;
        private DevComponents.DotNetBar.ButtonItem btnCadClientes;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        private DevComponents.DotNetBar.ButtonItem btnCadUsuarios;
        private DevComponents.DotNetBar.RibbonBar ribarMovimentoProdutos;
        private DevComponents.DotNetBar.ButtonItem btnEntradaProduto;
        private DevComponents.DotNetBar.RibbonBar ribarCadastroProdutos;
        private DevComponents.DotNetBar.ButtonItem btnCadProdutos;
        private DevComponents.DotNetBar.ButtonItem btnCadGrupos;
        private DevComponents.DotNetBar.ButtonItem btnSaidaProduto;
        private DevComponents.DotNetBar.ButtonItem btnEspecial;
        private DevComponents.DotNetBar.RibbonBar ribarMovimentoVendas;
        private DevComponents.DotNetBar.ButtonItem btnPdv;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonTabItem rtRelatorios;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private DevComponents.DotNetBar.MicroChartItem microChartItem1;
        private System.Windows.Forms.PictureBox pbLogoPrincipal;
        private System.Windows.Forms.OpenFileDialog ofDialog;
        public DevComponents.DotNetBar.StyleManager stiloPrincipal;
        private System.Windows.Forms.ToolStripStatusLabel tsslUsuarioNome;
        private System.Windows.Forms.ToolStripStatusLabel tsslIp;
        private System.Windows.Forms.ToolStripStatusLabel tsslDataAtual;
        private DevComponents.DotNetBar.RibbonBar ribarRelatoriosMovimentos;
        private DevComponents.DotNetBar.ButtonItem btnRltVendas;
        private DevComponents.DotNetBar.MicroChartItem microChartItem2;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private DevComponents.DotNetBar.RibbonBar ribarAperencia;
        private DevComponents.DotNetBar.ButtonItem btnMetro;
        private DevComponents.DotNetBar.ButtonItem btnOffice2010Black;
        private DevComponents.DotNetBar.ButtonItem btnOffice2010Blue;
        private DevComponents.DotNetBar.ButtonItem btnOffice2010Silver;
        private DevComponents.DotNetBar.ButtonItem btnOffice2007Black;
        private DevComponents.DotNetBar.ButtonItem btnOffice2007Blue;
        private DevComponents.DotNetBar.ButtonItem btnOffice2007Silver;
        private DevComponents.DotNetBar.ButtonItem btnVistaClass;
        private DevComponents.DotNetBar.RibbonTabItem Stilo;
        private DevComponents.DotNetBar.ButtonItem btnVisualStudio2010;
        private DevComponents.DotNetBar.ButtonItem btnVisualStudio2012Ligth;
        private DevComponents.DotNetBar.ButtonItem btnVisualStudio2012Black;
        private DevComponents.DotNetBar.ButtonItem btnWindowsBlue;
        private DevComponents.DotNetBar.ItemContainer itemContainer5;
        private DevComponents.DotNetBar.ButtonItem btnBlackClouds;
        private DevComponents.DotNetBar.ButtonItem btnBlackLilac;
        private DevComponents.DotNetBar.ButtonItem btnBlackSky;
        private DevComponents.DotNetBar.ButtonItem btnBlue;
        private DevComponents.DotNetBar.ButtonItem btnBlueBrown;
        private DevComponents.DotNetBar.ButtonItem btnBorderAux;
        private DevComponents.DotNetBar.ButtonItem btnBrown;
        private DevComponents.DotNetBar.ButtonItem btnChery;
        private DevComponents.DotNetBar.ButtonItem btnDarkBrown;
        private DevComponents.DotNetBar.ButtonItem btnDarkPurple;
        private DevComponents.DotNetBar.ButtonItem btnDarkRed;
        private DevComponents.DotNetBar.ButtonItem btnDefault;
        private DevComponents.DotNetBar.ButtonItem btnEarlyMaroon;
        private DevComponents.DotNetBar.ButtonItem btnEarlyRed;
        private DevComponents.DotNetBar.ButtonItem btnExpresso;
        private DevComponents.DotNetBar.ButtonItem btnFlorestGreen;
        private DevComponents.DotNetBar.ButtonItem btnGrayOrange;
        private DevComponents.DotNetBar.ButtonItem btnGreen;
        private DevComponents.DotNetBar.ButtonItem btnLatter;
        private DevComponents.DotNetBar.ButtonItem btnMaroonSilver;
        private DevComponents.DotNetBar.ButtonItem btnOrange;
        private DevComponents.DotNetBar.ButtonItem btnPowerRed;
        private DevComponents.DotNetBar.ButtonItem btnPurple;
        private DevComponents.DotNetBar.ButtonItem btnRed;
        private DevComponents.DotNetBar.ButtonItem btnRedAmplifield;
        private DevComponents.DotNetBar.ButtonItem btnRust;
        private DevComponents.DotNetBar.ButtonItem btnSilverBlues;
        private DevComponents.DotNetBar.ButtonItem btnSilverGreen;
        private DevComponents.DotNetBar.ButtonItem btnSkyGreen;
        private DevComponents.DotNetBar.ButtonItem btnViStudioDark;
        public DevComponents.DotNetBar.ButtonItem BtnCadFornecedores;
        public DevComponents.DotNetBar.ButtonItem btnEntragador;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel5;
        private DevComponents.DotNetBar.RibbonTabItem Financeiro;
        private DevComponents.DotNetBar.RibbonBar ribarFinanceiroFinaceira;
        private DevComponents.DotNetBar.ButtonItem btnContasPagar;
        private DevComponents.DotNetBar.ButtonItem btnPlanoConta;
        private DevComponents.DotNetBar.ButtonItem btnCadCartoes;
        private DevComponents.DotNetBar.ButtonItem btnDelivery;
        private DevComponents.DotNetBar.RibbonBar ribarCadastroDiversos;
        private DevComponents.DotNetBar.ButtonItem btnBairro;
        private DevComponents.DotNetBar.ButtonItem btnFormaPagamento;
        private DevComponents.DotNetBar.ButtonItem btnCalculadora;
        private DevComponents.DotNetBar.RibbonBar ribarMovimentoVisualizar;
        private DevComponents.DotNetBar.ButtonItem btnImprimirPedidos;
        private DevComponents.DotNetBar.ButtonItem btnTempoPedidos;
        private DevComponents.DotNetBar.ButtonItem btnCaixa;
        private DevComponents.DotNetBar.ButtonItem btnStatusDelivery;
        private DevComponents.DotNetBar.ButtonItem btnRetiradas;
        private DevComponents.DotNetBar.ButtonItem btnCadUnidadeMedida;
        private DevComponents.DotNetBar.RibbonBar ribarRelatoriosFinanceiro;
        private DevComponents.DotNetBar.ButtonItem btnRltRetiradas;
        private DevComponents.DotNetBar.ButtonItem btnRltContas;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.RibbonBar ribarRelatoriosPessoas;
        private DevComponents.DotNetBar.ButtonItem btnRltClientes;
        private DevComponents.DotNetBar.ButtonItem btnRltUsuarios;
        public DevComponents.DotNetBar.ButtonItem btnRltFornecedores;
        public DevComponents.DotNetBar.ButtonItem btnRltEntregadores;
        private DevComponents.DotNetBar.RibbonBar ribarRelatoriosProdutos;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutosGeral;
        public DevComponents.DotNetBar.ButtonItem btnRltEntregadorAtivo;
        public DevComponents.DotNetBar.ButtonItem btnRltEntregadoresDesligados;
        public DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutoEstoMinimo;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutosMaisVendido;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutosGrupos;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutosEntrada;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutosSaida;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel6;
        private DevComponents.DotNetBar.RibbonTabItem rtGraficos;
        private DevComponents.DotNetBar.RibbonBar ribarGraficosVendas;
        public DevComponents.DotNetBar.ButtonItem btnGrafVendasDiarias;
        public DevComponents.DotNetBar.ButtonItem btnGrafVendasMensal;
        public DevComponents.DotNetBar.ButtonItem btnGrafInlocDelivery;
        public DevComponents.DotNetBar.ButtonItem btnGrafCanceladas;
        private DevComponents.DotNetBar.RibbonBar ribarGraficosEntregas;
        public DevComponents.DotNetBar.ButtonItem btnGrafEntregasDiarias;
        public DevComponents.DotNetBar.ButtonItem btnGrafEntregasMensal;
        public DevComponents.DotNetBar.ButtonItem btnGrafEntregasEntregador;
        public DevComponents.DotNetBar.ButtonItem btnGrafEntregasTempo;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        public DevComponents.DotNetBar.ButtonItem btnRltDelivery;
        private DevComponents.DotNetBar.RibbonBar ribarCadastroSegurança;
        private DevComponents.DotNetBar.ButtonItem btnPermissaoUsuarios;
        public DevComponents.DotNetBar.ButtonItem btnNivelAcesso;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.ButtonItem btnCadLancheFacil;
        private DevComponents.DotNetBar.ButtonItem btnCadImpressao;
        private DevComponents.DotNetBar.CheckBoxItem ckbImpressaoPapel;
        private DevComponents.DotNetBar.CheckBoxItem ckbImpressaoTela;
        private DevComponents.DotNetBar.ButtonItem btnMenuBackup;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.ButtonItem btnISair;
        private DevComponents.DotNetBar.ButtonItem btnRltProdutoEstoMinGrupo;
        private DevComponents.DotNetBar.ButtonItem btnComplementoProdutos;
    }
}