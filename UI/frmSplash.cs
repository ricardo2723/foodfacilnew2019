using System;
using System.Windows.Forms;
using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmSplash : DevComponents.DotNetBar.Metro.MetroForm
    {
        SerialNegocios serialNegocios = new SerialNegocios();
        TipoImpressaoNegocios impressao = new TipoImpressaoNegocios();

        public static int tipoImpressao = -1;

        public frmSplash()
        {
            InitializeComponent();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            checaInstalacao();
            tipoImpressao = impressao.pegar();
        }

        private void frmSplash_Shown(object sender, EventArgs e)
        {
            int x;

            for (x = 0; x < 30; x++)
            {
                pbSplash.Value++;
                Application.DoEvents(); //Para n�o travar a tela
                System.Threading.Thread.Sleep(90);

                lblPorcentagem.Text = "<font color=\"#FFFFFF\">" + x.ToString() + "% </font>";
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;

        }

        private void checaInstalacao()
        {            
            SerialCollections serialCollections = serialNegocios.chegaSerial();

            if (serialCollections.Count == 0)
            {
                try
                {
                    frmInstallActive objInstAct = new frmInstallActive();

                    if (objInstAct.ShowDialog() == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        MessageBox.Show("� necess�rio a Ativa��o do Sistema para sua Utiliza��o", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        Application.Exit();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("N�o Foi Possivel Instalar o Serial", "AVISO!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Application.Exit();
                }               
            }
        }


        
    }
}