﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadUnidadeMedidaFront : Form
    {
        public frmCadUnidadeMedidaFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvUnidadeMedida.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadUnidadeMedida objFrmExtras = new frmCadUnidadeMedida(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmExtras.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaUnidadeMedida("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarUsuario();
            pesquisaUnidadeMedida("%");            
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisaUnidadeMedida("%" + txtPesquisar.Text + "%");
            txtPesquisar.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvUnidadeMedida.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir essa Unidade de Medida? Todas as informações dessa Unidade de Medida serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                UnidadeMedida unidadeMedidalSelecionado = dgvUnidadeMedida.SelectedRows[0].DataBoundItem as UnidadeMedida; 

                //instanciar a regra de negocios
                UnidadeMedidaNegocios unidadeMedidaNegocios = new UnidadeMedidaNegocios();
                string retorno = unidadeMedidaNegocios.Excluir(unidadeMedidalSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idExtra = Convert.ToInt32(retorno);
                    MessageBox.Show("Unidade de Medida Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaUnidadeMedida("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar uma Unidade de Pesquisa para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "UNIDADE")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaUnidadeMedida(string descri)
        {

            UnidadeMedidaNegocios unidadeMedidaNegocios = new UnidadeMedidaNegocios();
            UnidadeMedidaCollections unidadeMedidaCollections = unidadeMedidaNegocios.ConsultarDescricao(descri);

            dgvUnidadeMedida.DataSource = null;
            dgvUnidadeMedida.DataSource = unidadeMedidaCollections;
            
            dgvUnidadeMedida.Update();
            dgvUnidadeMedida.Refresh();
            
            labelX2.Text = "Unidade de Medida Listados: " + dgvUnidadeMedida.RowCount;

            Generic.msgPesquisa("Unidade de Medida não encontrada", dgvUnidadeMedida.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarUsuario()
        {
            if (dgvUnidadeMedida.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                UnidadeMedida unidadeMedidaSelecionado = dgvUnidadeMedida.SelectedRows[0].DataBoundItem as UnidadeMedida;

                frmCadUnidadeMedida objFrmCadUnidadeMedida = new frmCadUnidadeMedida(AcaoCRUD.Alterar, unidadeMedidaSelecionado);
                DialogResult resultado = objFrmCadUnidadeMedida.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaUnidadeMedida("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar uma Unidade de Medida para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadExtrasFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadExtrasFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvExtras_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        #endregion

        

    }
}
