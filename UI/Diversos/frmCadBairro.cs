﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadBairro : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadBairro(AcaoCRUD acao, Bairro bairro)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Bairro";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Bairro";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = bairro.idBairro.ToString(); 
                txtNome.Text = bairro.nomeBairro.ToUpper();
                txtTaxa.Text = bairro.taxaBairro.ToString();
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirBairro();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarBairro();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadBairro_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirBairro()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) && !String.IsNullOrEmpty(txtTaxa.Text)) 
            {

                Bairro bairro = new Bairro();

                bairro.usuarioId = frmLogin.usuariosLogin.usuarioId;
                bairro.nomeBairro = txtNome.Text;
                try
                {
                    bairro.taxaBairro = Convert.ToDecimal(txtTaxa.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Digite apenas numeros no campo Taxa", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                
                bairro.dataCadBairro = DateTime.Now;

                BairroNegocios bairroNegocios = new BairroNegocios();
                string retorno = bairroNegocios.inserir(bairro);

                try
                {
                    int idBairro = Convert.ToInt32(retorno);
                    MessageBox.Show("Bairro Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }
                else if (String.IsNullOrEmpty(txtTaxa.Text))
                {
                    MessageBox.Show("Campo Taxa é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxa.Focus();
                }

            }
        }

        private void atualizarBairro()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text))
            {

                Bairro bairro = new Bairro();

                bairro.idBairro = Convert.ToInt32(txtCodigo.Text);
                bairro.usuarioId = frmLogin.usuariosLogin.usuarioId;
                bairro.nomeBairro = txtNome.Text;
                try
                {
                    bairro.taxaBairro = Convert.ToDecimal(txtTaxa.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Digite apenas numeros no campo Taxa", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                bairro.dataCadBairro = DateTime.Now;

                BairroNegocios bairroNegocios = new BairroNegocios();
                string retorno = bairroNegocios.Alterar(bairro);

                try
                {
                    int idBairro = Convert.ToInt32(retorno);
                    MessageBox.Show("Bairro Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }
                else if (String.IsNullOrEmpty(txtTaxa.Text))
                {
                    MessageBox.Show("Campo Taxa é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxa.Focus();
                }
                                
            }
        }

        #endregion       
        
    }
}
