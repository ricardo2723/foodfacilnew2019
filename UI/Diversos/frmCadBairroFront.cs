﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadBairroFront : Form
    {
        public frmCadBairroFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvBairro.AutoGenerateColumns = false;
        }
        
        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadBairro objFrmExtras = new frmCadBairro(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmExtras.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaBairro("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarBairro();
            pesquisaBairro("%");            
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisaBairro("%" + txtPesquisar.Text + "%");
            txtPesquisar.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvBairro.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Bairro? Todas as informações desse Bairro serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Bairro bairroSelecionado = dgvBairro.SelectedRows[0].DataBoundItem as Bairro; 

                //instanciar a regra de negocios
                BairroNegocios bairroNegocios = new BairroNegocios();
                string retorno = bairroNegocios.Excluir(bairroSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idExtra = Convert.ToInt32(retorno);
                    MessageBox.Show("Bairro Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaBairro("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Bairro para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "BAIRRO")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaBairro(string descri)
        {

            BairroNegocios bairroNegocios = new BairroNegocios();
            BairroCollections bairroCollections = bairroNegocios.ConsultarNome(descri);

            dgvBairro.DataSource = null;
            dgvBairro.DataSource = bairroCollections;
            
            dgvBairro.Update();
            dgvBairro.Refresh();
            
            labelX2.Text = "Bairro Listados: " + dgvBairro.RowCount;

            Generic.msgPesquisa("Bairro não encontrado", dgvBairro.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarBairro()
        {
            if (dgvBairro.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Bairro bairroSelecionado = dgvBairro.SelectedRows[0].DataBoundItem as Bairro;

                frmCadBairro objFrmCadBairro = new frmCadBairro(AcaoCRUD.Alterar, bairroSelecionado);
                DialogResult resultado = objFrmCadBairro.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaBairro("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Bairro para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadExtrasFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadExtrasFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvExtras_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        #endregion

        

    }
}
