﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadUnidadeMedida : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadUnidadeMedida(AcaoCRUD acao, UnidadeMedida unidadeMedida)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Unidade Medida";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Unidade Medida";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = unidadeMedida.undMedidaId.ToString();
                txtDescricao.Text = unidadeMedida.undMedidaDescricao;               
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirUnidadeMedida();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarUnidadeMedida();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirUnidadeMedida()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                UnidadeMedida unidadeMedida = new UnidadeMedida();

                unidadeMedida.usuarioId = frmLogin.usuariosLogin.usuarioId;
                unidadeMedida.undMedidaDescricao = txtDescricao.Text;
                unidadeMedida.undMedidaDataCad = DateTime.Now;

                UnidadeMedidaNegocios unidadeMedidaNegocios = new UnidadeMedidaNegocios();
                string retorno = unidadeMedidaNegocios.inserir(unidadeMedida);

                try
                {
                    int idUnidadeMedida = Convert.ToInt32(retorno);
                    MessageBox.Show("Unidade de Medida Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }

            }
        }

        private void atualizarUnidadeMedida()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                UnidadeMedida unidadeMedida = new UnidadeMedida();

                unidadeMedida.undMedidaId = Convert.ToInt32(txtCodigo.Text);
                unidadeMedida.usuarioId = frmLogin.usuariosLogin.usuarioId;
                unidadeMedida.undMedidaDescricao = txtDescricao.Text;
                unidadeMedida.undMedidaDataCad = DateTime.Now;

                UnidadeMedidaNegocios unidadeMedidaNegocios = new UnidadeMedidaNegocios();
                string retorno = unidadeMedidaNegocios.Alterar(unidadeMedida);

                try
                {
                    int idUnidadeMedida = Convert.ToInt32(retorno);
                    MessageBox.Show("Unidade de Medida Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }
                                
            }
        }

        #endregion       
        
    }
}
