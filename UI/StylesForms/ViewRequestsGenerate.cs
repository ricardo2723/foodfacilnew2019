﻿using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace UI
{
    public class ViewRequestsGenerate
    {
        VendaCabecalhoNegocios pedidoNegocios = new VendaCabecalhoNegocios();

        VendaCabecalhoCollections pedidosLista;

        public void ControlGenerate(FlowLayoutPanel flp, EventHandler click)
        {
            pedidosLista = pedidoNegocios.BuscarPedidosAbertos();
            int i = 1;

            foreach (var item in pedidosLista)
            {
                Panel pPedidoCorpo = new Panel();
                Panel pPedidoTop = new Panel();
                Label lblNumAtendimento = new Label();
                Label label1 = new Label();
                Label lblNumPedido = new Label();
                Label lblTempo = new Label();
                Label lblAtendimento = new Label();
                Label lblStatus = new Label();
                // 
                // pPedidoCorpo
                // 
                pPedidoCorpo.BackColor = Color.WhiteSmoke;
                pPedidoCorpo.BorderStyle = BorderStyle.FixedSingle;
                pPedidoCorpo.Controls.Add(lblNumPedido);
                pPedidoCorpo.Controls.Add(lblAtendimento);
                pPedidoCorpo.Controls.Add(lblTempo);
                pPedidoCorpo.Controls.Add(lblStatus);
                pPedidoCorpo.Controls.Add(label1);
                pPedidoCorpo.Controls.Add(pPedidoTop);
                pPedidoCorpo.Controls.Add(lblNumAtendimento);
                pPedidoCorpo.Location = new Point(12, 16);
                pPedidoCorpo.Name = "pPedidoCorpo" + i;
                pPedidoCorpo.Size = new Size(164, 108);
                pPedidoCorpo.TabIndex = 0;
                pPedidoCorpo.Click += new EventHandler(click);
                // 
                // pPedidoTop1
                // 
                pPedidoTop.BackColor = Color.DodgerBlue;
                pPedidoTop.Dock = DockStyle.Top;
                pPedidoTop.Location = new Point(0, 0);
                pPedidoTop.Name = "pPedidoTop" + i;
                pPedidoTop.Size = new Size(162, 27);
                pPedidoTop.TabIndex = 1;
                pPedidoTop.Click += new EventHandler(click);
                // 
                // lblNumAtendimento1
                //                 
                lblNumPedido.Location = new Point(3, 14);
                lblNumPedido.Name = "lblNumPedido" + i;
                lblNumPedido.Size = new Size(109, 24);
                lblNumPedido.Visible = false;
                lblNumPedido.Text = item.idVendaCabecalho.ToString();
                // 
                // lblNumAtendimento1
                // 
                lblNumAtendimento.AutoSize = true;
                lblNumAtendimento.Font = new Font("Trebuchet MS", 14.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
                lblNumAtendimento.ForeColor = Color.White;
                lblNumAtendimento.BackColor = Color.DodgerBlue;
                lblNumAtendimento.Location = new Point(3, 1);
                lblNumAtendimento.Name = "lblNumAtendimento" + i;
                lblNumAtendimento.Size = new Size(109, 24);
                lblNumAtendimento.TabIndex = 0;
                lblNumAtendimento.BringToFront();
                lblNumAtendimento.Text = item.nomeAtendimento + " " + item.numeroAtendimento.ToString("000");
                pPedidoTop.Click += new EventHandler(click);
                // 
                // label11
                // 
                label1.AutoSize = true;
                label1.Font = new Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                label1.ForeColor = Color.DarkSlateBlue;
                label1.Location = new Point(7, 34);
                label1.Name = "label1" + i;
                label1.Size = new Size(46, 14);
                label1.TabIndex = 1;
                label1.Text = "Status:";
                pPedidoTop.Click += new EventHandler(click);
                // 
                // lblTempo1
                // 
                lblTempo.AutoSize = true;
                lblTempo.Font = new Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                lblTempo.ForeColor = Color.DarkSlateBlue;
                lblTempo.Location = new Point(7, 56);
                lblTempo.Name = "lblTempo" + i;
                lblTempo.Size = new Size(99, 14);
                lblTempo.TabIndex = 1;
                lblTempo.Text = "Tempo: "; //+ DateTime.Now.Subtract(item.DataAbertura).Hours.ToString("00") + "h" + DateTime.Now.Subtract(item.DataAbertura).Minutes.ToString("00") + "m";
                pPedidoTop.Click += new EventHandler(click);
                // 
                // lblAtendimento1
                // 
                lblAtendimento.AutoSize = true;
                lblAtendimento.Font = new Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                lblAtendimento.ForeColor = Color.DarkSlateBlue;
                lblAtendimento.Location = new Point(7, 78);
                lblAtendimento.Name = "lblAtendimento" + i;
                lblAtendimento.Size = new Size(110, 14);
                lblAtendimento.TabIndex = 1;
                lblAtendimento.Text = "Atendente: " + item.atendenteVendaCabecalho;
                pPedidoTop.Click += new EventHandler(click);
                // 
                // lblStatus1
                // 
                lblStatus.AutoSize = true;
                lblStatus.Font = new Font("Tahoma", 9F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
                lblStatus.ForeColor = Color.Red;
                lblStatus.Location = new Point(53, 34);
                lblStatus.Name = "lblStatus" + i;
                lblStatus.Size = new Size(60, 14);
                lblStatus.TabIndex = 1;
                lblStatus.Text = "Ocupada";

                flp.Controls.Add(pPedidoCorpo);
                i++;
            }
        }
    }
}
