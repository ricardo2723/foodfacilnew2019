﻿using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace UI
{
    public class ViewComplementsCategoriesGenerate
    {
        public void LoadComplements(VendaDetalhes itemPedido, FlowLayoutPanel flp, EventHandler click)
        {
            ProdutosNegocios produtosNegocios = new ProdutosNegocios();
            
            VendaDetalhesNegocios vendaDetalhesNegocios = new VendaDetalhesNegocios();
            VendaDetalhesCollections vendaDetalhesCollections =
                vendaDetalhesNegocios.BuscaItensPedido(itemPedido.idVendaDetalhes);

            ProdutosCollections produtosExtra = produtosNegocios.ConsultarProdutoExtra();
            
            int i = 1;

            foreach (var itemExtras in produtosExtra)
            {
                FlowLayoutPanel flpItemsExtras = new FlowLayoutPanel();

                flpItemsExtras.AutoSize = true;
                flpItemsExtras.BackColor = Color.White;
                flpItemsExtras.Name = "chkExtra" + i;
                flpItemsExtras.FlowDirection = FlowDirection.LeftToRight;

                CheckBox checkBox = new CheckBox();

                checkBox.AutoSize = true;
                checkBox.Font = new Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                checkBox.ForeColor = Color.Black;
                checkBox.Name = "chkExtra" + i;
                checkBox.TabIndex = 3;
                checkBox.Text = itemExtras.descricaoProduto;
                checkBox.UseVisualStyleBackColor = true;
                checkBox.Click += new EventHandler(click);

                foreach (var itemRequest in vendaDetalhesCollections)
                {
                    if (itemExtras.codigoProduto == itemRequest.codigoProduto)
                    {
                        checkBox.Checked = true;
                        break;
                    }
                }

                flpItemsExtras.Controls.Add(checkBox);

                Label lblProdutoCode = new Label();

                lblProdutoCode.Text = itemExtras.codigoProduto.ToString();
                lblProdutoCode.Name = "lblProdutoCode";
                lblProdutoCode.Visible = false;

                flpItemsExtras.Controls.Add(lblProdutoCode);

                NumericUpDown numericUpDown = new NumericUpDown();

                numericUpDown.BorderStyle = BorderStyle.FixedSingle;
                numericUpDown.Cursor = Cursors.Hand;
                numericUpDown.Font = new Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                numericUpDown.Name = "chkExtra" + i; ;
                numericUpDown.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
                numericUpDown.Size = new Size(47, 20);
                numericUpDown.TabIndex = 0;
                numericUpDown.TextAlign = HorizontalAlignment.Center;
                numericUpDown.ThousandsSeparator = true;
                numericUpDown.Value = new decimal(new int[] { 1, 0, 0, 0 });
                numericUpDown.Visible = false;

                flpItemsExtras.Controls.Add(numericUpDown);

                flp.Controls.Add(flpItemsExtras);

                i++;
            }
        }
    }
}
