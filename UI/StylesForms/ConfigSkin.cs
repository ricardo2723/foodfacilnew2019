﻿using MaterialSkin;
using System.Windows.Forms;

namespace UI.StylesForms
{
    public class ConfigSkin
    {
        public static void ThemeForm(Form form, Control label, string titulo)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));

            //FORM PRINCIPAL
            form.FormBorderStyle = FormBorderStyle.None;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            form.Text = "Food Fácil - " + titulo;
            label.Text = titulo;
        }

        public static void MaterialThemeForm(MaterialSkin.Controls.MaterialForm form)
        {
            MaterialSkinManager skinManager;
            //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCadCategorias));

            skinManager = MaterialSkinManager.Instance;
            skinManager.AddFormToManage(form);
            skinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new ColorScheme(Primary.Orange900, Primary.Red900, Primary.Green900, Accent.LightBlue200, TextShade.WHITE);

            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.StartPosition = FormStartPosition.CenterScreen;
            //form.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        }       
    }
}
