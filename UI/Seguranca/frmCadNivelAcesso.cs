﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadNivelAcesso : Form
    {
        AcaoCRUD acaoSelecionada;
        int idNivelAcesso;

        public frmCadNivelAcesso(AcaoCRUD acao, NivelAcesso levelAcess)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Nivel de Acesso";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Nivel de Acesso";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                idNivelAcesso = levelAcess.nivelAcessoId;
                txtDescricao.Text = levelAcess.nivelAcessoDescricao;               
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirNivelAcesso();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarNivelAcesso();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirNivelAcesso()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                NivelAcesso nivelAcesso = new NivelAcesso();

                nivelAcesso.usuarioId = frmLogin.usuariosLogin.usuarioId;
                nivelAcesso.nivelAcessoDescricao = txtDescricao.Text;
                nivelAcesso.dataCadAcessoNivel = DateTime.Now;

                NivelAcessoNegocios nivelAcessoNegocios = new NivelAcessoNegocios();
                string retorno = nivelAcessoNegocios.inserir(nivelAcesso);

                try
                {
                    int idNivelAcesso = Convert.ToInt32(retorno);
                    MessageBox.Show("Nivel de Acesso Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }

            }
        }

        private void atualizarNivelAcesso()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                NivelAcesso nivelAcesso = new NivelAcesso();
                
                nivelAcesso.nivelAcessoId = idNivelAcesso;
                nivelAcesso.usuarioId = frmLogin.usuariosLogin.usuarioId;
                nivelAcesso.nivelAcessoDescricao = txtDescricao.Text;
                nivelAcesso.dataCadAcessoNivel = DateTime.Now;

                NivelAcessoNegocios nivelAcessoNegocios = new NivelAcessoNegocios();
                string retorno = nivelAcessoNegocios.Alterar(nivelAcesso);

                try
                {
                    int retorn = Convert.ToInt32(retorno);
                    MessageBox.Show("Nivel de Acesso Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }
                                
            }
        }

        #endregion       
        
    }
}
