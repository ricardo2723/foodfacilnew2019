﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmPermissaoAdm : Form
    {
        public string administradorRetirada;

        UsuariosCollections usuariosCollections = new UsuariosCollections();
        Usuarios usuarios = new Usuarios();

        public frmPermissaoAdm()
        {
            InitializeComponent();
        }

        private void frmPermissaoAdm_Load(object sender, EventArgs e)
        {
            UsuariosNegocios usuariosNegocios = new UsuariosNegocios();
            usuariosCollections = usuariosNegocios.getUsuariosAdm();

            cbLogin.DataSource = null;
            cbLogin.DataSource = usuariosCollections;
            cbLogin.DisplayMember = "usuarioLogin";
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            int i = 0;

            foreach (var linha in usuariosCollections)
            {
                if (linha.usuarioLogin == cbLogin.Text && linha.usuarioSenha == txtSenha.Text)
                {
                    MessageBox.Show("Permissão Autorizada com Sucesso", "Autorização de Permissão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    administradorRetirada = linha.usuarioLogin;
                    this.DialogResult = DialogResult.Yes;
                    break;
                }
                else
                {
                    i++;

                }
            }
            if (i >= usuariosCollections.Count)
            {
                MessageBox.Show("Login ou Senha Não Conferem", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.No;
            }
            
        }
        
        private void yes()
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void no()
        {
            this.DialogResult = DialogResult.No;
        }

        private void frmPermissaoAdm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnEntrar.PerformClick();
                    break;
            }
        }
    }
}
