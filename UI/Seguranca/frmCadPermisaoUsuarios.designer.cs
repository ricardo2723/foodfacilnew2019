﻿namespace UI
{
    partial class frmCadPermisaoUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadPermisaoUsuarios));
            this.lbNivelACesso = new System.Windows.Forms.ListBox();
            this.lbButons = new System.Windows.Forms.ListBox();
            this.ckbAtivarTudo = new System.Windows.Forms.CheckBox();
            this.ckbCadastrar = new System.Windows.Forms.CheckBox();
            this.ckbAlterar = new System.Windows.Forms.CheckBox();
            this.ckbExcluir = new System.Windows.Forms.CheckBox();
            this.ckbPesquisar = new System.Windows.Forms.CheckBox();
            this.ckbFinanceiro = new System.Windows.Forms.CheckBox();
            this.ckbRelatorios = new System.Windows.Forms.CheckBox();
            this.ckbGraficos = new System.Windows.Forms.CheckBox();
            this.btnNovo = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // lbNivelACesso
            // 
            this.lbNivelACesso.DisplayMember = "nivelAcessoDescricao";
            this.lbNivelACesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNivelACesso.ForeColor = System.Drawing.Color.Blue;
            this.lbNivelACesso.FormattingEnabled = true;
            this.lbNivelACesso.ItemHeight = 16;
            this.lbNivelACesso.Location = new System.Drawing.Point(12, 12);
            this.lbNivelACesso.Name = "lbNivelACesso";
            this.lbNivelACesso.Size = new System.Drawing.Size(232, 260);
            this.lbNivelACesso.TabIndex = 7;
            this.lbNivelACesso.ValueMember = "nivelAcessoId";
            // 
            // lbButons
            // 
            this.lbButons.DisplayMember = "buton";
            this.lbButons.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbButons.ForeColor = System.Drawing.Color.Blue;
            this.lbButons.FormattingEnabled = true;
            this.lbButons.ItemHeight = 16;
            this.lbButons.Items.AddRange(new object[] {
            "CLIENTES",
            "USUARIOS",
            "FORNECEDORES",
            "ENTREGADORES",
            "PRODUTOS",
            "GRUPOS",
            "ESPECIAIS",
            "BAIRRO",
            "UNIDADE",
            "ENTRADA",
            "SAIDA",
            "PDV",
            "DELIVERY",
            "ENTREGAS",
            "PEDIDOS",
            "TEMPO PEDIDO",
            "CAIXA",
            "PERMISSAO USUARIOS",
            "NIVEL ACESSO",
            "CARTOES",
            "PLANO CONTA",
            "FORMA PAGAMENTO",
            "CONTAS PAGAR",
            "RETIRADAS",
            "RELATORIOS",
            "GRAFICOS",
            "CONFIGURAÇOES"});
            this.lbButons.Location = new System.Drawing.Point(250, 12);
            this.lbButons.Margin = new System.Windows.Forms.Padding(5);
            this.lbButons.MultiColumn = true;
            this.lbButons.Name = "lbButons";
            this.lbButons.Size = new System.Drawing.Size(290, 260);
            this.lbButons.TabIndex = 8;
            this.lbButons.ValueMember = "permissaoAcessoId";
            this.lbButons.SelectedIndexChanged += new System.EventHandler(this.lbButons_SelectedIndexChanged);
            // 
            // ckbAtivarTudo
            // 
            this.ckbAtivarTudo.AutoSize = true;
            this.ckbAtivarTudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbAtivarTudo.Location = new System.Drawing.Point(12, 280);
            this.ckbAtivarTudo.Name = "ckbAtivarTudo";
            this.ckbAtivarTudo.Size = new System.Drawing.Size(100, 21);
            this.ckbAtivarTudo.TabIndex = 9;
            this.ckbAtivarTudo.Text = "Ativar Tudo";
            this.ckbAtivarTudo.UseVisualStyleBackColor = true;
            this.ckbAtivarTudo.Click += new System.EventHandler(this.ckbAtivarTudo_Click);
            // 
            // ckbCadastrar
            // 
            this.ckbCadastrar.AutoSize = true;
            this.ckbCadastrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbCadastrar.Location = new System.Drawing.Point(124, 280);
            this.ckbCadastrar.Name = "ckbCadastrar";
            this.ckbCadastrar.Size = new System.Drawing.Size(89, 21);
            this.ckbCadastrar.TabIndex = 10;
            this.ckbCadastrar.Text = "Cadastrar";
            this.ckbCadastrar.UseVisualStyleBackColor = true;
            // 
            // ckbAlterar
            // 
            this.ckbAlterar.AutoSize = true;
            this.ckbAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbAlterar.Location = new System.Drawing.Point(237, 280);
            this.ckbAlterar.Name = "ckbAlterar";
            this.ckbAlterar.Size = new System.Drawing.Size(69, 21);
            this.ckbAlterar.TabIndex = 11;
            this.ckbAlterar.Text = "Alterar";
            this.ckbAlterar.UseVisualStyleBackColor = true;
            // 
            // ckbExcluir
            // 
            this.ckbExcluir.AutoSize = true;
            this.ckbExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbExcluir.Location = new System.Drawing.Point(339, 280);
            this.ckbExcluir.Name = "ckbExcluir";
            this.ckbExcluir.Size = new System.Drawing.Size(68, 21);
            this.ckbExcluir.TabIndex = 12;
            this.ckbExcluir.Text = "Excluir";
            this.ckbExcluir.UseVisualStyleBackColor = true;
            // 
            // ckbPesquisar
            // 
            this.ckbPesquisar.AutoSize = true;
            this.ckbPesquisar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbPesquisar.Location = new System.Drawing.Point(12, 303);
            this.ckbPesquisar.Name = "ckbPesquisar";
            this.ckbPesquisar.Size = new System.Drawing.Size(90, 21);
            this.ckbPesquisar.TabIndex = 13;
            this.ckbPesquisar.Text = "Pesquisar";
            this.ckbPesquisar.UseVisualStyleBackColor = true;
            // 
            // ckbFinanceiro
            // 
            this.ckbFinanceiro.AutoSize = true;
            this.ckbFinanceiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbFinanceiro.Location = new System.Drawing.Point(339, 303);
            this.ckbFinanceiro.Name = "ckbFinanceiro";
            this.ckbFinanceiro.Size = new System.Drawing.Size(85, 21);
            this.ckbFinanceiro.TabIndex = 14;
            this.ckbFinanceiro.Text = "Finaceiro";
            this.ckbFinanceiro.UseVisualStyleBackColor = true;
            // 
            // ckbRelatorios
            // 
            this.ckbRelatorios.AutoSize = true;
            this.ckbRelatorios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbRelatorios.Location = new System.Drawing.Point(124, 303);
            this.ckbRelatorios.Name = "ckbRelatorios";
            this.ckbRelatorios.Size = new System.Drawing.Size(91, 21);
            this.ckbRelatorios.TabIndex = 15;
            this.ckbRelatorios.Text = "Relatórios";
            this.ckbRelatorios.UseVisualStyleBackColor = true;
            // 
            // ckbGraficos
            // 
            this.ckbGraficos.AutoSize = true;
            this.ckbGraficos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbGraficos.Location = new System.Drawing.Point(237, 303);
            this.ckbGraficos.Name = "ckbGraficos";
            this.ckbGraficos.Size = new System.Drawing.Size(80, 21);
            this.ckbGraficos.TabIndex = 16;
            this.ckbGraficos.Text = "Gráficos";
            this.ckbGraficos.UseVisualStyleBackColor = true;
            // 
            // btnNovo
            // 
            this.btnNovo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNovo.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovo.Image = global::UI.Properties.Resources.addQtd_fw;
            this.btnNovo.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnNovo.Location = new System.Drawing.Point(432, 280);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(108, 44);
            this.btnNovo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnNovo.TabIndex = 1;
            this.btnNovo.Text = "Salvar (F2)";
            this.btnNovo.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // frmCadPermisaoUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.ClientSize = new System.Drawing.Size(550, 330);
            this.Controls.Add(this.ckbGraficos);
            this.Controls.Add(this.ckbRelatorios);
            this.Controls.Add(this.ckbFinanceiro);
            this.Controls.Add(this.ckbPesquisar);
            this.Controls.Add(this.ckbExcluir);
            this.Controls.Add(this.ckbAlterar);
            this.Controls.Add(this.ckbCadastrar);
            this.Controls.Add(this.ckbAtivarTudo);
            this.Controls.Add(this.lbButons);
            this.Controls.Add(this.lbNivelACesso);
            this.Controls.Add(this.btnNovo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadPermisaoUsuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Manter Permissões de Usuários";
            this.Load += new System.EventHandler(this.frmCadExtrasFront_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadExtrasFront_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnNovo;
        private System.Windows.Forms.ListBox lbNivelACesso;
        private System.Windows.Forms.ListBox lbButons;
        private System.Windows.Forms.CheckBox ckbAtivarTudo;
        private System.Windows.Forms.CheckBox ckbCadastrar;
        private System.Windows.Forms.CheckBox ckbAlterar;
        private System.Windows.Forms.CheckBox ckbExcluir;
        private System.Windows.Forms.CheckBox ckbPesquisar;
        private System.Windows.Forms.CheckBox ckbFinanceiro;
        private System.Windows.Forms.CheckBox ckbRelatorios;
        private System.Windows.Forms.CheckBox ckbGraficos;
    }
}