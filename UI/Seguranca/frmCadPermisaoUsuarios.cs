﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadPermisaoUsuarios : Form
    {
        int permissaoAcessoId = 0;
        
        public frmCadPermisaoUsuarios()
        {
            InitializeComponent();
        }

        #region BOTÕES

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(lbNivelACesso.SelectedValue) != 0 & lbButons.Text != String.Empty)
            {
                try
                {
                    PermissaoAcesso permissaoAcesso = new PermissaoAcesso();

                    permissaoAcesso.permissaoAcessoId = permissaoAcessoId;
                    permissaoAcesso.nivelAcessoId = Convert.ToInt32(lbNivelACesso.SelectedValue);
                    permissaoAcesso.buton = lbButons.Text;
                    permissaoAcesso.ativo = Convert.ToBoolean(ckbAtivarTudo.CheckState);
                    permissaoAcesso.cadastrar = Convert.ToBoolean(ckbCadastrar.CheckState);
                    permissaoAcesso.alterar = Convert.ToBoolean(ckbAlterar.CheckState);
                    permissaoAcesso.excluir = Convert.ToBoolean(ckbExcluir.CheckState);
                    permissaoAcesso.pesquisar = Convert.ToBoolean(ckbPesquisar.CheckState);
                    permissaoAcesso.relatorios = Convert.ToBoolean(ckbRelatorios.CheckState);
                    permissaoAcesso.graficos = Convert.ToBoolean(ckbGraficos.CheckState);
                    permissaoAcesso.financeiro = Convert.ToBoolean(ckbFinanceiro.CheckState);

                    PermissaoAcessoNegocios permissaoAcessoNegocios = new PermissaoAcessoNegocios();
                    string retorno = permissaoAcessoNegocios.inserir(permissaoAcesso);

                    int returne = Convert.ToInt32(retorno);

                    MessageBox.Show("Permissões Cadastradas com Sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    permissaoAcessoId = 0;

                    trocarCheckBox(false);
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel cadastrar as Permissões", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        #endregion

        #region METODOS

        private void carregaPermissoes()
        {
            PermissaoAcessoNegocios permissaoAcessoNegogios = new PermissaoAcessoNegocios();
            PermissaoAcessoCollections permissaoAcessoCollections = permissaoAcessoNegogios.Consultar(Convert.ToInt32(lbNivelACesso.SelectedValue), lbButons.Text);

            foreach (var item in permissaoAcessoCollections)
            {
                permissaoAcessoId = item.permissaoAcessoId;
                ckbAtivarTudo.Checked = item.ativo;
                ckbCadastrar.Checked = item.cadastrar;
                ckbAlterar.Checked = item.alterar;
                ckbExcluir.Checked = item.excluir;
                ckbPesquisar.Checked = item.pesquisar;
                ckbFinanceiro.Checked = item.financeiro;
                ckbRelatorios.Checked = item.relatorios;
                ckbGraficos.Checked = item.graficos;
            }
        }

        private void trocarCheckBox(bool status)
        {
            ckbAtivarTudo.Checked = status;
            ckbCadastrar.Checked = status;
            ckbAlterar.Checked = status;
            ckbExcluir.Checked = status;
            ckbPesquisar.Checked = status;
            ckbRelatorios.Checked = status;
            ckbFinanceiro.Checked = status;
            ckbGraficos.Checked = status;
        }

        #endregion

        #region AÇÕES

        private void frmCadExtrasFront_Load(object sender, EventArgs e)
        {
            NivelAcessoNegocios nivelAcessoNegocios = new NivelAcessoNegocios();
            NivelAcessoCollections nivelAcessoCollections = nivelAcessoNegocios.ConsultarNivelAcesso("%");

            lbNivelACesso.DataSource = null;
            lbNivelACesso.DataSource = nivelAcessoCollections;

        }

        private void frmCadExtrasFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
            }
        }

        private void lbButons_SelectedIndexChanged(object sender, EventArgs e)
        {
            trocarCheckBox(false);
            carregaPermissoes();
        }


        #endregion

        private void ckbAtivarTudo_Click(object sender, EventArgs e)
        {
            if (ckbAtivarTudo.Checked)
            {
                trocarCheckBox(true);
            }
            else
            {
                trocarCheckBox(false);
            }
        }


    }
}
