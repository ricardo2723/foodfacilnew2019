﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadNivelAcessoFront : Form
    {
        public frmCadNivelAcessoFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvNivelAcesso.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadNivelAcesso objFrmNivelAcesso = new frmCadNivelAcesso(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmNivelAcesso.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaNivelAcesso("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarNivelAcesso();
            pesquisaNivelAcesso("%");            
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisaNivelAcesso("%" + txtPesquisar.Text + "%");
            txtPesquisar.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvNivelAcesso.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Nivel de Acesso? Todas as informações desse Nivel de Acesso serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                NivelAcesso nivelAcessoSelecionado = dgvNivelAcesso.SelectedRows[0].DataBoundItem as NivelAcesso; 

                //instanciar a regra de negocios
                NivelAcessoNegocios nivelAcessoNegocios = new NivelAcessoNegocios();
                string retorno = nivelAcessoNegocios.Excluir(nivelAcessoSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idNivelAcesso = Convert.ToInt32(retorno);
                    MessageBox.Show("Nivel de Acesso Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaNivelAcesso("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Nivel de Acesso para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "NIVEL ACESSO")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaNivelAcesso(string descri)
        {

            NivelAcessoNegocios nivelAcessosNegocios = new NivelAcessoNegocios();
            NivelAcessoCollections nivelAcessosCollections = nivelAcessosNegocios.ConsultarNivelAcesso(descri);

            dgvNivelAcesso.DataSource = null;
            dgvNivelAcesso.DataSource = nivelAcessosCollections;
            
            dgvNivelAcesso.Update();
            dgvNivelAcesso.Refresh();
            
            labelX2.Text = "Nivel de Acesso Listados: " + dgvNivelAcesso.RowCount;

            Generic.msgPesquisa("Nivel de Acesso não encontrado", dgvNivelAcesso.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarNivelAcesso()
        {
            if (dgvNivelAcesso.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                NivelAcesso nivelAcessoSelecionado = dgvNivelAcesso.SelectedRows[0].DataBoundItem as NivelAcesso;

                frmCadNivelAcesso objFrmCadNivelAcesso = new frmCadNivelAcesso(AcaoCRUD.Alterar, nivelAcessoSelecionado);
                DialogResult resultado = objFrmCadNivelAcesso.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaNivelAcesso("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Nivel de Acesso para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadNivelAcessoFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadNivelAcessoFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvNivelAcesso_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }
        #endregion
    }
}
