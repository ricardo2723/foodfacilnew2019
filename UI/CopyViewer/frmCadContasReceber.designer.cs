﻿namespace Apresentação
{
    partial class frmCadContasReceber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadContasReceber));
            this.label13 = new System.Windows.Forms.Label();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.dtpDataPagamentoConta = new System.Windows.Forms.DateTimePicker();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDescricaoConta = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescricaoTipoConta = new System.Windows.Forms.TextBox();
            this.cbTipoConta = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtValorConta = new System.Windows.Forms.TextBox();
            this.dtpDataVencimento = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtValorPagoConta = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbPagar = new System.Windows.Forms.RadioButton();
            this.rbEmAberto = new System.Windows.Forms.RadioButton();
            this.line1 = new DevComponents.DotNetBar.Controls.Line();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(157, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Data do Pagamento";
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::Apresentação.Properties.Resources.salva;
            this.btnSalvar.Location = new System.Drawing.Point(16, 185);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(137, 34);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 8;
            this.btnSalvar.Text = "  Salvar (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.bntSalvar_Click);
            // 
            // dtpDataPagamentoConta
            // 
            this.dtpDataPagamentoConta.Enabled = false;
            this.dtpDataPagamentoConta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataPagamentoConta.Location = new System.Drawing.Point(159, 152);
            this.dtpDataPagamentoConta.Name = "dtpDataPagamentoConta";
            this.dtpDataPagamentoConta.RightToLeftLayout = true;
            this.dtpDataPagamentoConta.Size = new System.Drawing.Size(130, 20);
            this.dtpDataPagamentoConta.TabIndex = 7;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::Apresentação.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnFechar.Location = new System.Drawing.Point(517, 185);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(101, 34);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 9;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>  Fechar (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Código Tipo Conta";
            // 
            // txtDescricaoConta
            // 
            this.txtDescricaoConta.Location = new System.Drawing.Point(12, 84);
            this.txtDescricaoConta.MaxLength = 100;
            this.txtDescricaoConta.Name = "txtDescricaoConta";
            this.txtDescricaoConta.Size = new System.Drawing.Size(379, 20);
            this.txtDescricaoConta.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(120, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Descrição Tipo Conta";
            // 
            // txtDescricaoTipoConta
            // 
            this.txtDescricaoTipoConta.Location = new System.Drawing.Point(120, 32);
            this.txtDescricaoTipoConta.MaxLength = 200;
            this.txtDescricaoTipoConta.Name = "txtDescricaoTipoConta";
            this.txtDescricaoTipoConta.ReadOnly = true;
            this.txtDescricaoTipoConta.Size = new System.Drawing.Size(330, 20);
            this.txtDescricaoTipoConta.TabIndex = 1;
            // 
            // cbTipoConta
            // 
            this.cbTipoConta.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbTipoConta.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoConta.BackColor = System.Drawing.Color.White;
            this.cbTipoConta.DisplayMember = "codPlanoConta";
            this.cbTipoConta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoConta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTipoConta.FormattingEnabled = true;
            this.cbTipoConta.Location = new System.Drawing.Point(12, 31);
            this.cbTipoConta.Name = "cbTipoConta";
            this.cbTipoConta.Size = new System.Drawing.Size(102, 21);
            this.cbTipoConta.TabIndex = 0;
            this.cbTipoConta.ValueMember = "idPlanoConta";
            this.cbTipoConta.SelectedIndexChanged += new System.EventHandler(this.cbTipoConta_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Descrição da Conta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(394, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Valor";
            // 
            // txtValorConta
            // 
            this.txtValorConta.Location = new System.Drawing.Point(397, 84);
            this.txtValorConta.MaxLength = 200;
            this.txtValorConta.Name = "txtValorConta";
            this.txtValorConta.Size = new System.Drawing.Size(98, 20);
            this.txtValorConta.TabIndex = 4;
            // 
            // dtpDataVencimento
            // 
            this.dtpDataVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataVencimento.Location = new System.Drawing.Point(501, 84);
            this.dtpDataVencimento.Name = "dtpDataVencimento";
            this.dtpDataVencimento.RightToLeftLayout = true;
            this.dtpDataVencimento.Size = new System.Drawing.Size(117, 20);
            this.dtpDataVencimento.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(499, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Data do Vencimento";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "Valor Pago";
            // 
            // txtValorPagoConta
            // 
            this.txtValorPagoConta.Enabled = false;
            this.txtValorPagoConta.Location = new System.Drawing.Point(16, 152);
            this.txtValorPagoConta.MaxLength = 200;
            this.txtValorPagoConta.Name = "txtValorPagoConta";
            this.txtValorPagoConta.Size = new System.Drawing.Size(137, 20);
            this.txtValorPagoConta.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbPagar);
            this.groupBox1.Controls.Add(this.rbEmAberto);
            this.groupBox1.Location = new System.Drawing.Point(456, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 40);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status da Conta";
            // 
            // rbPagar
            // 
            this.rbPagar.AutoSize = true;
            this.rbPagar.Location = new System.Drawing.Point(106, 17);
            this.rbPagar.Name = "rbPagar";
            this.rbPagar.Size = new System.Drawing.Size(53, 17);
            this.rbPagar.TabIndex = 1;
            this.rbPagar.Text = "Pagar";
            this.rbPagar.UseVisualStyleBackColor = true;
            this.rbPagar.Click += new System.EventHandler(this.rbPaga_Click);
            // 
            // rbEmAberto
            // 
            this.rbEmAberto.AutoSize = true;
            this.rbEmAberto.Checked = true;
            this.rbEmAberto.Location = new System.Drawing.Point(17, 17);
            this.rbEmAberto.Name = "rbEmAberto";
            this.rbEmAberto.Size = new System.Drawing.Size(74, 17);
            this.rbEmAberto.TabIndex = 0;
            this.rbEmAberto.TabStop = true;
            this.rbEmAberto.Text = "Em Aberto";
            this.rbEmAberto.UseVisualStyleBackColor = true;
            this.rbEmAberto.Click += new System.EventHandler(this.rbEmAberto_Click);
            // 
            // line1
            // 
            this.line1.Location = new System.Drawing.Point(16, 111);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(602, 23);
            this.line1.TabIndex = 39;
            this.line1.Text = "line1";
            // 
            // frmCadContasReceber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(627, 233);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtValorPagoConta);
            this.Controls.Add(this.dtpDataVencimento);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtValorConta);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbTipoConta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDescricaoTipoConta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDescricaoConta);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.dtpDataPagamentoConta);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.label13);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadContasReceber";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Contas";
            this.Load += new System.EventHandler(this.frmCadContasReceber_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadContasReceber_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private System.Windows.Forms.DateTimePicker dtpDataPagamentoConta;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDescricaoConta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescricaoTipoConta;
        private System.Windows.Forms.ComboBox cbTipoConta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtValorConta;
        private System.Windows.Forms.DateTimePicker dtpDataVencimento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtValorPagoConta;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbPagar;
        private System.Windows.Forms.RadioButton rbEmAberto;
        private DevComponents.DotNetBar.Controls.Line line1;
    }
}