﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace Apresentação
{
    public partial class frmCadContasPagarFront : Form
    {
        public frmCadContasPagarFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvClientes.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadContasPagars objfrmCadContasPagars = new frmCadContasPagars(AcaoCRUD.Inserir, null);
            DialogResult resultado = objfrmCadContasPagars.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaClienteNome("%");
            }            
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarClientes();           
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (rbNome.Checked)
            {
                pesquisaClienteNome(txtPesquisa.Text);
            }
            else if (rbCpf.Checked)
            {
                pesquisaClienteCpf(txtPesquisa.Text);
            }
            else if (rbLogin.Checked)
            {
                pesquisaClienteCnpj(txtPesquisa.Text);
            }

            txtPesquisa.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvClientes.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Cliente? Todos as informações desse Cliente serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Clientes clientesSelecionado = dgvClientes.SelectedRows[0].DataBoundItem as Clientes; 

                //instanciar a regra de negocios
                ClientesNegocios clientesNegocios = new ClientesNegocios();
                string retorno = clientesNegocios.Excluir(clientesSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idCliente = Convert.ToInt32(retorno);
                    MessageBox.Show("Cliente Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaClienteNome("%" + txtPesquisa.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Cliente para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void pesquisaClienteNome(string nome)
        {

            ClientesNegocios clientesNegocios = new ClientesNegocios();
            ClientesCollections clientesCollections = clientesNegocios.ConsultarPorNome(txtPesquisa.Text);

            dgvClientes.DataSource = null;
            dgvClientes.DataSource = clientesCollections;
            
            dgvClientes.Update();
            dgvClientes.Refresh();
            
            labelX2.Text = "Clientes Listados: " + dgvClientes.RowCount;

            if (dgvClientes.RowCount > 0)
            {
                btnAlterar.Enabled = true;
                btnExcluir.Enabled = true;
            }
            else
            {
                MessageBox.Show("Cliente não encontrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            txtPesquisa.Text = null;
        }

        private void pesquisaClienteCpf(string cpf)
        {
            ClientesNegocios clientesNegocios = new ClientesNegocios();
            ClientesCollections clientesCollections = clientesNegocios.ConsultarPorCpf(txtPesquisa.Text);

            dgvClientes.DataSource = null;
            dgvClientes.DataSource = clientesCollections;

            dgvClientes.Update();
            dgvClientes.Refresh();

            labelX2.Text = "Clientes Listados: " + dgvClientes.RowCount;

            if (dgvClientes.RowCount > 0)
            {
                btnAlterar.Enabled = true;
                btnExcluir.Enabled = true;
            }
            else
            {
                MessageBox.Show("Cliente não encontrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            txtPesquisa.Text = null;
        }

        private void pesquisaClienteCnpj(string cnpj)
        {
            ClientesNegocios clientesNegocios = new ClientesNegocios();
            ClientesCollections clientesCollections = clientesNegocios.ConsultarPorCnpj(txtPesquisa.Text);

            dgvClientes.DataSource = null;
            dgvClientes.DataSource = clientesCollections;

            dgvClientes.Update();
            dgvClientes.Refresh();

            labelX2.Text = "Clientes Listados: " + dgvClientes.RowCount;

            if (dgvClientes.RowCount > 0)
            {
                btnAlterar.Enabled = true;
                btnExcluir.Enabled = true;
            }
            else
            {
                MessageBox.Show("Cliente não encontrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void alterarClientes()
        {
            if (dgvClientes.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Clientes clientesSelecionado = dgvClientes.SelectedRows[0].DataBoundItem as Clientes; 
                
                frmCadContasPagars objfrmCadContasPagars = new frmCadContasPagars(AcaoCRUD.Alterar, clientesSelecionado);
                DialogResult resultado = objfrmCadContasPagars.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaClienteNome("%");
                }
                
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Cliente para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadContasPagarFront_Load(object sender, EventArgs e)
        {
            btnAlterar.Enabled = false;
            btnExcluir.Enabled = false;
        }

        private void frmCadContasPagarFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvClientes_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        #endregion

        

    }
}
