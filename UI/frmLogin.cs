using System;
using System.Windows.Forms;
using System.Management;
using Microsoft.Win32;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmLogin : DevComponents.DotNetBar.Metro.MetroForm
    {
        static public Usuarios usuariosLogin = new Usuarios();
        static public MeuLanche meuLanche = new MeuLanche();
        static public PermissaoAcessoCollections permissaoAcessoCollectionsLogin = new PermissaoAcessoCollections();
        
        MeuLancheNegocios meuLancheNegocios = new MeuLancheNegocios();
        
        string Registro, Registro1;

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            getReg();

            MeuLancheCollections meuLancheCollections = meuLancheNegocios.ConsultarPorNome();
            
            foreach (var item in meuLancheCollections)
            {
                meuLanche.nomeMeuLanche = item.nomeMeuLanche;
                meuLanche.cnpjMeuLanche = item.cnpjMeuLanche;
                meuLanche.inscricaoEstadualMeuLanche = item.inscricaoEstadualMeuLanche;
                meuLanche.enderecoMeuLanche = item.enderecoMeuLanche + "  " + item.complementoMeuLanche + " " + item.bairroMeuLanche;
                meuLanche.municipioMeuLanche = item.municipioMeuLanche;
                meuLanche.ufMeuLanche = item.ufMeuLanche;
                meuLanche.localLogoMeuLanche = item.localLogoMeuLanche;
            }
        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnEntrar.PerformClick();
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            usuariosLogin.usuarioLogin = txtLogin.Text;
            usuariosLogin.usuarioSenha = txtSenha.Text;

            UsuariosNegocios usuariosNegocios = new UsuariosNegocios();
            UsuariosCollections usuariosCollections = usuariosNegocios.ChecaLogin(usuariosLogin);

            if (usuariosCollections.Count > 0)
            {
                foreach (var linha in usuariosCollections)
                {
                    usuariosLogin.usuarioId = Convert.ToInt32(linha.usuarioId);
                    usuariosLogin.usuarioNome = linha.usuarioNome.ToUpper();
                    usuariosLogin.usuarioLogin = linha.usuarioLogin.ToUpper();
                    usuariosLogin.usuarioSenha = linha.usuarioSenha;
                    usuariosLogin.usuarioCpf = linha.usuarioCpf;
                    usuariosLogin.nivelAcessoId = Convert.ToInt32(linha.nivelAcessoId);
                    usuariosLogin.nivelAcessoDescricao = linha.nivelAcessoDescricao.ToUpper();
                }

                PermissaoAcessoNegocios permissaoAcessoNegocio = new PermissaoAcessoNegocios();
                permissaoAcessoCollectionsLogin = permissaoAcessoNegocio.ConsultarNivelId(usuariosLogin.nivelAcessoId);
                
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Usu�rio e Senha n�o Coincidem", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtSenha.Text = null;
                return;
            }
        }

        private string GetVolumeSerial(string strDriveLetter)
        {
            try
            {
                if ((string.IsNullOrEmpty(strDriveLetter) | strDriveLetter == null))
                {
                    strDriveLetter = "C";
                }
                ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
                disk.Get();
                return disk["VolumeSerialNumber"].ToString();
            }
            catch (Exception)
            {
                return "";
            }

        }

        private string getBSN()
        {
            string bsn = "";
            try
            {
                ManagementObjectCollection list = null;
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from  Win32_BaseBoard");

                list = searcher.Get();

                foreach (ManagementObject mo in list)
                {
                    bsn = mo["SerialNumber"].ToString();
                }

                return bsn;
            }
            catch (Exception)
            {
                return "";
            }
        }

        private void getReg()
        {
            if (GetVolumeSerial("C") == chegaRegistro() && getBSN() == chegaRegistro1())
            {

            }
            else
            {
                MessageBox.Show("Para a Utiliza��o do Sistema e necess�rio sua ATIVA��O, contate o Propriet�rio do Sistema para obter uma ATIVA��O. ## Fones: (92) 8162-9178 / (92) 9450-9178 ## Francisco Ricardo ## Analista Desenvolvedor ##", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                btnCancelar.PerformClick();
            }
        }

        private string chegaRegistro()
        {
            try
            {
                // cria uma refer�cnia para a chave de registro Software
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Names");

                // realiza a leitura do registro
                return Registro = rk.GetValue("registro").ToString();
                               
            }
            catch (Exception)
            {
                MessageBox.Show("N�o foi possivel acessar o sistema", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
        }

        private string chegaRegistro1()
        {
            try
            {
                // cria uma refer�cnia para a chave de registro Software
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Names");

                // realiza a leitura do registro
                return Registro1 = rk.GetValue("registro1").ToString();

            }
            catch (Exception erro)
            {
                MessageBox.Show("N�o foi possivel acessar o sistema" + erro.Message);
                return "";
            }
        }
    }
}
