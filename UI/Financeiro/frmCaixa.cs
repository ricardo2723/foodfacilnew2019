﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace UI
{
    public partial class frmCaixa : Form
    {
       Caixa caixa = new Caixa();

        public frmCaixa()
        {
            InitializeComponent();
        }

        public int codDel;

        private void frmCaixa_Load(object sender, EventArgs e)
        {
            txtData.ReadOnly = true;
            txtData.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        }

        private void txtValor_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnAbrirCaixa.PerformClick();
                    break;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAbrirCaixa_Click(object sender, EventArgs e)
        {
            string retorno;

            try
            {
                try
                {
                    caixa.valorCaixaAbre = Convert.ToDecimal(txtValor.Text.Replace('.',','));
                }
                catch (Exception)
                {
                    MessageBox.Show("O valor e Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtValor.Text = "";
                    txtValor.Focus();
                    return;
                }

                caixa.dataCaixa = DateTime.Now.Date;
                caixa.horaCaixaAbre = DateTime.Now;
                caixa.statusCaixa = 1;
                caixa.usuarioId = frmLogin.usuariosLogin.usuarioId;

                DialogResult openCaixa = MessageBox.Show("Processo de Abertura de Caixa Iniciado. Valor: R$ " + caixa.valorCaixaAbre.ToString("N"), "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (openCaixa == DialogResult.Yes)
                {
                    CaixaNegocios caixaNegocios = new CaixaNegocios();
                    retorno = caixaNegocios.inserir(caixa);

                    int checaRetorno = Convert.ToInt32(retorno);
                    Principal.caixa = checaRetorno;

                    MessageBox.Show("Caixa Aberto com Sucesso.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes; 
                }
                else
                {
                    return;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao abrir Caixa. Detalhes: " + ex, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

       
    }
}
