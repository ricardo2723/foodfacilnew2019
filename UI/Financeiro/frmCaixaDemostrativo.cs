﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;
using Relatorios;

namespace UI
{
    public partial class frmCaixaDemostrativo : Form
    {
        CaixaNegocios caixaNegocios = new CaixaNegocios();
        Caixa caixa = new Caixa();

        public frmCaixaDemostrativo()
        {
            InitializeComponent();
        }

        private void frmCaixaDemostrativo_Load(object sender, EventArgs e)
        {
            decimal total = 0;

            CaixaCollections caixaCollections = caixaNegocios.Consultar();

            foreach (var item in caixaCollections)
            {
                lblCodigoAbertura.Text = item.idCaixa.ToString("000000");
                lblDataAbertura.Text = item.dataCaixa.ToString("dd/MM/yyyy");
                lblHoraAbertura.Text = item.horaCaixaAbre.ToString("HH:mm:ss");
                lblDataFechamento.Text = item.dataCaixa.ToString("dd/MM/yyyy");
                lblAbertoPor.Text = item.nomeUsuario;
                lblCaixaAbertoCom.Text = item.valorCaixaAbre.ToString("N");

                caixa.dataCaixa = item.dataCaixa;
            }

            caixaCollections = caixaNegocios.pagamentosCaixa(caixa.dataCaixa, Principal.caixa);

            foreach (var item in caixaCollections)
            {
                if (item.descricaoCaixa == "CARTÃO")
                {
                    lblCartao.Text = item.totalCaixa.ToString("N");
                }
                else if (item.descricaoCaixa == "DINHEIRO")
                {
                    lblDinheiro.Text = item.totalCaixa.ToString("N");
                }
                else if (item.descricaoCaixa == "TICKET")
                {
                    lblTicket.Text = item.totalCaixa.ToString("N");
                }

                total += item.totalCaixa;                
            }

            caixaCollections = caixaNegocios.descontosCaixa(caixa.dataCaixa, Principal.caixa);

            foreach (var item in caixaCollections)
            {
                lblDesconto.Text = item.totalDesconto.ToString("N");
            }

            lblValorDelivery.Text = caixaNegocios.deliveryCaixa(caixa.dataCaixa, Principal.caixa).ToString("N");

            lblRetirada.Text = caixaNegocios.Retiradas(caixa.dataCaixa, Principal.caixa).ToString("N");

            lblValorCaixa.Text = ((Convert.ToDecimal(lblCaixaAbertoCom.Text) + total + Convert.ToDecimal(lblValorDelivery.Text)) - Convert.ToDecimal(lblRetirada.Text)).ToString("N");

            lblValorVendas.Text = total.ToString("N");
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            caixa.idCaixa = Convert.ToInt32(lblCodigoAbertura.Text);
            caixa.horaCaixaAbre = Convert.ToDateTime(lblHoraAbertura.Text);
            caixa.dataCaixa = Convert.ToDateTime(lblDataAbertura.Text);
            caixa.nomeUsuario = lblAbertoPor.Text;
            caixa.valorRetirada = Convert.ToDecimal(lblRetirada.Text);
            caixa.dinheiro = Convert.ToDecimal(lblDinheiro.Text);
            caixa.cartao = Convert.ToDecimal(lblCartao.Text);
            caixa.desconto = Convert.ToDecimal(lblDesconto.Text);
            caixa.ticket = Convert.ToDecimal(lblTicket.Text);
            caixa.totalCaixa = Convert.ToDecimal(lblValorCaixa.Text);
            caixa.valorCaixaFecha = Convert.ToDecimal(lblValorVendas.Text);
            caixa.horaCaixaFecha = Convert.ToDateTime(DateTime.Now);
            caixa.abertoCom = Convert.ToDecimal(lblCaixaAbertoCom.Text);

            frmRltImprimirCaixa objImprimirCaixa = new frmRltImprimirCaixa(caixa, frmLogin.meuLanche);
            objImprimirCaixa.ShowDialog();
        }

        private void btnFechaCaixa_Click(object sender, EventArgs e)
        {
            string retorno;

            caixa.horaCaixaFecha = DateTime.Now;
            caixa.statusCaixa = 0;
            caixa.valorCaixaFecha = Convert.ToDecimal(lblValorCaixa.Text);
            caixa.idCaixa = Convert.ToInt32(lblCodigoAbertura.Text);

            DialogResult closeCaixa = MessageBox.Show("Processo de Fechamento de Caixa Iniciado. Valor em Caixa: R$ " + caixa.valorCaixaFecha.ToString("N"), "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (closeCaixa == DialogResult.Yes)
            {
                CaixaNegocios caixaNegocios = new CaixaNegocios();
                retorno = caixaNegocios.Alterar(caixa);

                int checaRetorno = Convert.ToInt32(retorno);

                DialogResult printCaixa = MessageBox.Show("Deseja imprimir o Demonstrativo do Caixa? ", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (printCaixa == DialogResult.Yes)
                {
                    btnImprimir.PerformClick();                     
                }

                MessageBox.Show("Caixa Finalizado com Sucesso.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.Yes;
            }
            else
            {
                return;
            }
        }

        private void frmCaixaDemostrativo_KeyDown(object sender, KeyEventArgs e)
        {
              
        }

        private void frmCaixaDemostrativo_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    btnFechaCaixa.PerformClick();
                    break;
                case Keys.F10:
                    btnImprimir.PerformClick();
                    break;
            } 
        }

    }
}
