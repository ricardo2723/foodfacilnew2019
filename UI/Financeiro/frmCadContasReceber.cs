﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadContasReceber : Form
    {
        AcaoCRUD acaoSelecionada;
        PlanoContaCollections planoContaCollections = new PlanoContaCollections();
        PlanoConta planoConta = new PlanoConta();
        PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
        string status = "0";
        int idConta;

        public frmCadContasReceber(AcaoCRUD acao, ContasPagar contasPagar)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            planoContaCollections = planoContaNegocios.ConsultarDescricao("%");

            cbTipoConta.DataSource = null;
            cbTipoConta.DataSource = planoContaCollections;

            if (acao == AcaoCRUD.Alterar)
            {
                idConta = contasPagar.idContaPagar;
                checaStaus(contasPagar.statusContaPagar);
                cbTipoConta.SelectedValue = contasPagar.idPlanoConta;
                txtDescricaoConta.Text = contasPagar.nomeContaPagar;
                txtValorConta.Text = Convert.ToDecimal(contasPagar.valorContaPagar).ToString("N");
                dtpDataVencimento.Value = contasPagar.vencimentoContaPagar;
                status = contasPagar.statusContaPagar;
                txtValorPagoConta.Text = contasPagar.valorPagoContaPagar.ToString("N");

                if (contasPagar.statusContaPagar != "EM ABERTO")
                {
                    dtpDataPagamentoConta.Value = contasPagar.pagamentoContaPagar;
                }

                btnSalvar.Text = "  Salvar Alteração (F12)";
            }
            else if (acao == AcaoCRUD.Inserir)
            {
                groupBox1.Enabled = false;
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
        }

        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirConta();
            }
            else if (acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarConta();
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadContasReceber_Load(object sender, EventArgs e)
        {
            if (rbPagar.Checked)
            {
                frmPermissaoAdm objPermissao = new frmPermissaoAdm();
                DialogResult permissao = objPermissao.ShowDialog();

                if (permissao != DialogResult.Yes)
                {
                    MessageBox.Show("E Necessário um Administrador para Alterar Contas Pagas", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Close();
                }
            }
        }

        private void frmCadContasReceber_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void cbTipoConta_SelectedIndexChanged(object sender, EventArgs e)
        {
            planoContaCollections = planoContaNegocios.ConsultarCod(Convert.ToInt32(cbTipoConta.Text));

            foreach (var item in planoContaCollections)
            {
                txtDescricaoTipoConta.Text = item.descricaoPlanoConta;
            }
            txtDescricaoConta.Focus();
        }

        private void rbEmAberto_Click(object sender, EventArgs e)
        {
            status = "0";
            txtValorPagoConta.Enabled = false;
            txtValorPagoConta.Text = string.Empty;
            dtpDataPagamentoConta.Enabled = false;
        }

        private void rbPaga_Click(object sender, EventArgs e)
        {
            status = "1";
            txtValorPagoConta.Enabled = true;
            dtpDataPagamentoConta.Enabled = true;
            txtValorPagoConta.Focus();
        }

        #endregion

        #region METODOS

        private void inserirConta()
        {
            try
            {
                if
               (!String.IsNullOrEmpty(cbTipoConta.Text) &&
               !String.IsNullOrEmpty(txtDescricaoConta.Text) &&
               !String.IsNullOrEmpty(txtValorConta.Text))
                {

                    ContasPagar contasPagar = new ContasPagar();

                    contasPagar.idPlanoConta = Convert.ToInt32(cbTipoConta.SelectedValue);
                    contasPagar.nomeContaPagar = txtDescricaoConta.Text;
                    contasPagar.valorContaPagar = Convert.ToDecimal(txtValorConta.Text);
                    contasPagar.vencimentoContaPagar = dtpDataVencimento.Value;
                    contasPagar.statusContaPagar = status;
                    contasPagar.dataCadContaPagar = DateTime.Now;
                    contasPagar.usuarioId = frmLogin.usuariosLogin.usuarioId;

                    ContasPagarNegocios contasPagarNegocios = new ContasPagarNegocios();
                    string retorno = contasPagarNegocios.inserir(contasPagar);

                    int idContasPagar = Convert.ToInt32(retorno);
                    MessageBox.Show("Conta Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                else
                {
                    if (String.IsNullOrEmpty(cbTipoConta.Text))
                    {
                        MessageBox.Show("Campo Código Tipo Conta é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cbTipoConta.Focus();
                    }

                    else if (String.IsNullOrEmpty(txtDescricaoConta.Text))
                    {
                        MessageBox.Show("Campo Descrição da Conta é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDescricaoConta.Focus();
                    }

                    else if (String.IsNullOrEmpty(txtValorConta.Text))
                    {
                        MessageBox.Show("Campo Valor é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtValorConta.Focus();
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possivel Inserir os Dados. Verifique se os campos de valores estão com somente numeros", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorConta.Focus();
                return;
            }

        }

        private void atualizarConta()
        {
            try
            {
                if
               (!String.IsNullOrEmpty(cbTipoConta.Text) &&
               !String.IsNullOrEmpty(txtDescricaoConta.Text) &&
               !String.IsNullOrEmpty(txtValorPagoConta.Text) &&
               !String.IsNullOrEmpty(txtValorConta.Text))
                {

                    ContasPagar contasPagar = new ContasPagar();

                    contasPagar.idContaPagar = idConta;
                    contasPagar.idPlanoConta = Convert.ToInt32(cbTipoConta.SelectedValue);
                    contasPagar.nomeContaPagar = txtDescricaoConta.Text;
                    contasPagar.valorContaPagar = Convert.ToDecimal(txtValorConta.Text);
                    contasPagar.vencimentoContaPagar = dtpDataVencimento.Value;
                    contasPagar.statusContaPagar = status;
                    contasPagar.dataCadContaPagar = DateTime.Now;
                    contasPagar.usuarioId = frmLogin.usuariosLogin.usuarioId;

                    ContasPagarNegocios contasPagarNegocios = new ContasPagarNegocios();
                    string retorno = contasPagarNegocios.Alterar(contasPagar);
                    int idContasPagar = Convert.ToInt32(retorno);

                    contasPagar.valorPagoContaPagar = Convert.ToDecimal(txtValorPagoConta.Text);
                    contasPagar.pagamentoContaPagar = dtpDataPagamentoConta.Value;

                    if (rbPagar.Checked)
                    {
                        retorno = contasPagarNegocios.inserirPagamento(contasPagar);
                        idContasPagar = Convert.ToInt32(retorno);
                    }

                    MessageBox.Show("Conta alterada com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;

                }
                else
                {
                    if (String.IsNullOrEmpty(cbTipoConta.Text))
                    {
                        MessageBox.Show("Campo Código Tipo Conta é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cbTipoConta.Focus();
                    }

                    else if (String.IsNullOrEmpty(txtDescricaoConta.Text))
                    {
                        MessageBox.Show("Campo Descrição da Conta é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDescricaoConta.Focus();
                    }

                    else if (String.IsNullOrEmpty(txtValorPagoConta.Text))
                    {
                        MessageBox.Show("Campo Valor Pago é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtValorPagoConta.Focus();
                    }

                    else if (String.IsNullOrEmpty(txtValorConta.Text))
                    {
                        MessageBox.Show("Campo Valor é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtValorConta.Focus();
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possivel Inserir os Dados. Verifique se os campos de valores estão com somente numeros", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtValorConta.Focus();
                return;
            }
        }

        private void checaStaus(string status)
        {
            if (status == "PAGO")
            {
                rbPagar.Checked = true;
            }
            else if (status == "EM ABERTO")
            {
                rbEmAberto.Checked = true;
            }
            else
            {
                MessageBox.Show("Não foi possivel selecionar o Status essa janela será fechada", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
        }

        #endregion


    }
}
