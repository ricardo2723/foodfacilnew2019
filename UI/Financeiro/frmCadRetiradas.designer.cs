﻿namespace UI
{
    partial class frmCadRetiradas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadRetiradas));
            this.label1 = new System.Windows.Forms.Label();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.txtValor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnFechar = new DevComponents.DotNetBar.ButtonX();
            this.txtMotivo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Valor:";
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::UI.Properties.Resources.salva;
            this.btnSalvar.Location = new System.Drawing.Point(14, 135);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(136, 34);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.TabIndex = 2;
            this.btnSalvar.Text = "  Salvar Alteração (F12)";
            this.btnSalvar.Click += new System.EventHandler(this.bntSalvar_Click);
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.txtValor.Border.Class = "TextBoxBorder";
            this.txtValor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValor.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtValor.Location = new System.Drawing.Point(15, 29);
            this.txtValor.MaxLength = 100;
            this.txtValor.Name = "txtValor";
            this.txtValor.PreventEnterBeep = true;
            this.txtValor.Size = new System.Drawing.Size(136, 20);
            this.txtValor.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Image = global::UI.Properties.Resources.fecharPed_fw;
            this.btnFechar.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnFechar.Location = new System.Drawing.Point(188, 135);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(101, 34);
            this.btnFechar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechar.TabIndex = 3;
            this.btnFechar.Text = "<font color=\"#ED1C24\"><b>  Fechar (Esc)</b><font color=\"#ED1C24\"></font></font>";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // txtMotivo
            // 
            this.txtMotivo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            // 
            // 
            // 
            this.txtMotivo.Border.Class = "TextBoxBorder";
            this.txtMotivo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMotivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMotivo.Location = new System.Drawing.Point(15, 73);
            this.txtMotivo.MaxLength = 100;
            this.txtMotivo.Multiline = true;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.PreventEnterBeep = true;
            this.txtMotivo.Size = new System.Drawing.Size(274, 56);
            this.txtMotivo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Motivo";
            // 
            // frmCadRetiradas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 181);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtMotivo);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadRetiradas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Cadastro de Retiradas";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCadExtras_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValor;
        private DevComponents.DotNetBar.ButtonX btnFechar;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMotivo;
        private System.Windows.Forms.Label label2;
    }
}