﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadPlanoConta : Form
    {
        AcaoCRUD acaoSelecionada;
        public int idPlano;

        public frmCadPlanoConta(AcaoCRUD acao, PlanoConta planoConta)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Plano de Contas";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Plano de Contas";
                btnSalvar.Text = "  Salvar Alteração (F12)";
                idPlano = planoConta.idPlanoConta;
                txtCodigo.Enabled = false;
                txtCodigo.Text = planoConta.codPlanoConta.ToString(); 
                txtDescricao.Text = planoConta.descricaoPlanoConta.ToUpper();
                
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirPlanoConta();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarPlanoConta();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirPlanoConta()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text)) 
            {

                PlanoConta planoConta = new PlanoConta();

                planoConta.usuarioId = frmLogin.usuariosLogin.usuarioId;
                try
                {
                    planoConta.codPlanoConta = Convert.ToInt32(txtCodigo.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Para Inserir o Código do Plano de Conta é necessário digitar apenas numeros", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                planoConta.descricaoPlanoConta = txtDescricao.Text;
                planoConta.dataCadPlanoConta = DateTime.Now;

                PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
                string retorno = planoContaNegocios.inserir(planoConta);

                try
                {
                    int idPlanoConta = Convert.ToInt32(retorno);
                    MessageBox.Show("Plano de Conta Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }                
            }
        }

        private void atualizarPlanoConta()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                PlanoConta planoConta = new PlanoConta();

                planoConta.codPlanoConta = Convert.ToInt32(txtCodigo.Text);
                planoConta.usuarioId = frmLogin.usuariosLogin.usuarioId;
                planoConta.descricaoPlanoConta = txtDescricao.Text;
                planoConta.dataCadPlanoConta = DateTime.Now;
                planoConta.idPlanoConta = idPlano;

                PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
                string retorno = planoContaNegocios.Alterar(planoConta);

                try
                {
                    int idPlanoConta = Convert.ToInt32(retorno);
                    MessageBox.Show("Plano de Conta Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }                            
            }
        }

        #endregion       
        
    }
}
