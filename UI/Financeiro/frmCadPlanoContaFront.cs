﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadPlanoContaFront : Form
    {
        public frmCadPlanoContaFront()
        {
            InitializeComponent();
            //Não gerar Linhas Automaticas
            dgvPlanoConta.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadPlanoConta objFrmPlanoConta = new frmCadPlanoConta(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmPlanoConta.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaPlanoContaDescricao("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarPlanoConta();         
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (rbDescricao.Checked)
            {
                pesquisaPlanoContaDescricao("%" + txtPesquisar.Text + "%");
                txtPesquisar.Text = "";
            }
            else if (rbCodigo.Checked)
            {
                pesquisaPlanoContaCodigo(txtPesquisar.Text);
                txtPesquisar.Text = "";
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvPlanoConta.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Plano de Conta? Todas as informações desse Plano de Conta serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                PlanoConta planoContaSelecionado = dgvPlanoConta.SelectedRows[0].DataBoundItem as PlanoConta; 

                //instanciar a regra de negocios
                PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
                string retorno = planoContaNegocios.Excluir(planoContaSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idPlanoConta = Convert.ToInt32(retorno);
                    MessageBox.Show("Plano de Conta Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaPlanoContaDescricao("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Plano de Conta para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "PLANO CONTA")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaPlanoContaDescricao(string descri)
        {

            PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
            PlanoContaCollections planoContaCollections = planoContaNegocios.ConsultarDescricao(descri);

            dgvPlanoConta.DataSource = null;
            dgvPlanoConta.DataSource = planoContaCollections;
            
            dgvPlanoConta.Update();
            dgvPlanoConta.Refresh();
            
            labelX2.Text = "Plano de Conta Listados: " + dgvPlanoConta.RowCount;

            Generic.msgPesquisa("Plano Conta não encontrado", dgvPlanoConta.RowCount);

            txtPesquisar.Text = null;
        }

        private void pesquisaPlanoContaCodigo(string codigo)
        {
            int cod;
            try
            {
                cod = Convert.ToInt32(codigo);
            }
            catch (Exception)
            {
                MessageBox.Show("Para Pesquisar pelo Código do Plano de Conta é necessário digitar apenas numeros", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
            PlanoContaCollections planoContaCollections = planoContaNegocios.ConsultarCod(cod);

            dgvPlanoConta.DataSource = null;
            dgvPlanoConta.DataSource = planoContaCollections;

            dgvPlanoConta.Update();
            dgvPlanoConta.Refresh();

            labelX2.Text = "Plano de Conta Listados: " + dgvPlanoConta.RowCount;

            Generic.msgPesquisa("Plano Conta não encontrado", dgvPlanoConta.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarPlanoConta()
        {
            if (dgvPlanoConta.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                PlanoConta planoContaSelecionado = dgvPlanoConta.SelectedRows[0].DataBoundItem as PlanoConta;

                frmCadPlanoConta objFrmCadPlanoConta = new frmCadPlanoConta(AcaoCRUD.Alterar, planoContaSelecionado);
                DialogResult resultado = objFrmCadPlanoConta.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaPlanoContaDescricao("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Plano de Conta para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadExtrasFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadExtrasFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvPlanoConta_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbCodigo_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbDescricao_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        #endregion

    }
}