﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadContasPagarFront : Form
    {
        public frmCadContasPagarFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvContaPagar.AutoGenerateColumns = false;

            PlanoContaNegocios planoContaNegocios = new PlanoContaNegocios();
            PlanoContaCollections planoContaCollections = planoContaNegocios.ConsultarDescricao("%");

            cbTipoConta.DataSource = null;
            cbTipoConta.DataSource = planoContaCollections;
            cbTipoConta.DisplayMember = "descricaoPlanoConta";
            cbTipoConta.ValueMember = "idPlanoConta";            
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadContasPagar objfrmCadContasPagars = new frmCadContasPagar(AcaoCRUD.Inserir, null);
            objfrmCadContasPagars.ShowDialog();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarContasPagar();           
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (rbTipo.Checked)
            {
                pesquisaTipo();
            }
            else if (rbStatus.Checked)
            {
                pesquisaStatus();
            }
            else if (rbData.Checked)
            {
                pesquisaData();
            }
            else if (rbCodTipoConta.Checked)
            {
                pesquisaTipoCodigo();
            }  
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvContaPagar.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir essa Conta? Todos as informações desse Cliente serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                ContasPagar contasPagarSelecionado = dgvContaPagar.SelectedRows[0].DataBoundItem as ContasPagar; 

                //instanciar a regra de negocios
                ContasPagarNegocios contaPagarNegocios = new ContasPagarNegocios();
                string retorno = contaPagarNegocios.Excluir(contasPagarSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idCliente = Convert.ToInt32(retorno);
                    MessageBox.Show("Conta Excluida com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaTipo();
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar uma Conta para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "CONTAS PAGAR")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;                    
                }
            }
        }

        private void pesquisaTipo()
        {
            ContasPagarNegocios contaPagarNegocios = new ContasPagarNegocios();
            ContasPagarCollections contaPagarCollections = contaPagarNegocios.ConsultarPorTipo(Convert.ToInt32(cbTipoConta.SelectedValue));

            dgvContaPagar.DataSource = null;
            dgvContaPagar.DataSource = contaPagarCollections;
            
            dgvContaPagar.Update();
            dgvContaPagar.Refresh();

            labelX2.Text = "Contas Listadas:" + dgvContaPagar.RowCount;

            Generic.msgPesquisa("Conta não encontrada", dgvContaPagar.RowCount);
        }

        private void pesquisaTipoCodigo()
        {
            try
            {
                ContasPagarNegocios contaPagarNegocios = new ContasPagarNegocios();
                ContasPagarCollections contaPagarCollections = contaPagarNegocios.ConsultarPorTipoCodigo(Convert.ToInt32(txtCodigoTipoConta.Text));

                dgvContaPagar.DataSource = null;
                dgvContaPagar.DataSource = contaPagarCollections;

                dgvContaPagar.Update();
                dgvContaPagar.Refresh();

                labelX2.Text = "Contas Listadas: : " + dgvContaPagar.RowCount;

                Generic.msgPesquisa("Conta não encontrada", dgvContaPagar.RowCount);
            }
            catch (Exception)
            {
                MessageBox.Show("Error ao Pesquisar Tipo da Conta, verifique se possui apenas numeros no campo Codigo Tipo Conta", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtCodigoTipoConta.Focus();
                return;
            }
            
        }

        private void pesquisaStatus()
        {
            ContasPagarNegocios contaPagarNegocios = new ContasPagarNegocios();
            ContasPagarCollections contaPagarCollections = contaPagarNegocios.ConsultarPorStatus(checaStatusRetorno(cbStatusConta.SelectedIndex));

            dgvContaPagar.DataSource = null;
            dgvContaPagar.DataSource = contaPagarCollections;

            dgvContaPagar.Update();
            dgvContaPagar.Refresh();

            labelX2.Text = "Contas Listadas: " + dgvContaPagar.RowCount;

            if (dgvContaPagar.RowCount < 0)
            {
                MessageBox.Show("Conta não encontrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void pesquisaData()
        {
            ContasPagarNegocios contaPagarNegocios = new ContasPagarNegocios();
            ContasPagarCollections contaPagarCollections = contaPagarNegocios.ConsultarPorData(dtpVencimentoConta.Value);

            dgvContaPagar.DataSource = null;
            dgvContaPagar.DataSource = contaPagarCollections;

            dgvContaPagar.Update();
            dgvContaPagar.Refresh();

            labelX2.Text = "Contas Listadas: : " + dgvContaPagar.RowCount;

            Generic.msgPesquisa("Conta não encontrada", dgvContaPagar.RowCount);

        }

        private void alterarContasPagar()
        {
            if (dgvContaPagar.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                ContasPagar contasPagarSelecionado = dgvContaPagar.SelectedRows[0].DataBoundItem as ContasPagar;

                if (contasPagarSelecionado.valorPagoContaPagar <= 0 && contasPagarSelecionado.statusContaPagar == "PAGO")
                {
                    ContasPagarNegocios contasPagarNegocios= new ContasPagarNegocios();
                    ContasPagarCollections contasPagarCollections = contasPagarNegocios.ConsultarContasPagas(contasPagarSelecionado.idContaPagar);

                    foreach (var item in contasPagarCollections)
                    {
                        contasPagarSelecionado.valorPagoContaPagar = item.valorPagoContaPagar;
                        contasPagarSelecionado.pagamentoContaPagar = item.pagamentoContaPagar;
                    }
                }
                frmCadContasPagar objfrmCadContasPagar = new frmCadContasPagar(AcaoCRUD.Alterar, contasPagarSelecionado);
                objfrmCadContasPagar.ShowDialog();                
            }
            else
            {
                MessageBox.Show("E necessário Selecionar uma Conta para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private bool checaStatusRetorno(int status)
        {
            bool retorno;

            if (status == 0)
            {
                return retorno = true;
            }
            else
            {
                return retorno = false;
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadContasPagarFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
            cbStatusConta.Enabled = false;
            dtpVencimentoConta.Enabled = false;
            txtCodigoTipoConta.Enabled = false;
        }

        private void frmCadContasPagarFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }
        
        private void txtCodigoTipoConta_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvContaPagar_MouseClick(object sender, MouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbTipo_Click(object sender, EventArgs e)
        {
            cbTipoConta.Enabled = true;
            cbStatusConta.Enabled = false;
            dtpVencimentoConta.Enabled = false;
            txtCodigoTipoConta.Enabled = false;
        }

        private void rbStatus_Click(object sender, EventArgs e)
        {
            cbTipoConta.Enabled = false;
            cbStatusConta.Enabled = true;
            dtpVencimentoConta.Enabled = false;
            txtCodigoTipoConta.Enabled = false;
        }

        private void rbData_Click(object sender, EventArgs e)
        {
            cbTipoConta.Enabled = false;
            cbStatusConta.Enabled = false;
            dtpVencimentoConta.Enabled = true;
            txtCodigoTipoConta.Enabled = false;
        }

        private void rbCodTipoConta_Click(object sender, EventArgs e)
        {
            txtCodigoTipoConta.Enabled = true;
            cbTipoConta.Enabled = false;
            cbStatusConta.Enabled = false;
            dtpVencimentoConta.Enabled = false;
        }

        #endregion
        
    }
}
