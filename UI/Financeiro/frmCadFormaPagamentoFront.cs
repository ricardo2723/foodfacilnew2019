﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadFormaPagamentoFront : Form
    {
        public frmCadFormaPagamentoFront()
        {
            InitializeComponent();

            //Não gerar Linhas Automaticas
            dgvFormaPagamento.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadFormaPagamento objFrmFormaPagamento = new frmCadFormaPagamento(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmFormaPagamento.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaFormaPagamento("%");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarFormaPagamento();
            pesquisaFormaPagamento("%");            
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisaFormaPagamento("%" + txtPesquisar.Text + "%");
            txtPesquisar.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvFormaPagamento.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse FormaPagamento? Todas as informações desse FormaPagamento serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                FormaPagamento formaPagamentoSelecionado = dgvFormaPagamento.SelectedRows[0].DataBoundItem as FormaPagamento; 

                //instanciar a regra de negocios
                FormaPagamentoNegocios formaPagamentoNegocios = new FormaPagamentoNegocios();
                string retorno = formaPagamentoNegocios.Excluir(formaPagamentoSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idExtra = Convert.ToInt32(retorno);
                    MessageBox.Show("FormaPagamento Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaFormaPagamento("%" + txtPesquisar.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Forma de Pagamento para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "FORMA PAGAMENTO")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }
        
        private void pesquisaFormaPagamento(string descri)
        {

            FormaPagamentoNegocios formaPagamentosNegocios = new FormaPagamentoNegocios();
            FormaPagamentoCollections formaPagamentosCollections = formaPagamentosNegocios.ConsultarDescricao(descri);

            dgvFormaPagamento.DataSource = null;
            dgvFormaPagamento.DataSource = formaPagamentosCollections;
            
            dgvFormaPagamento.Update();
            dgvFormaPagamento.Refresh();
            
            labelX2.Text = "Forma de Pagamento Listados: " + dgvFormaPagamento.RowCount;

            Generic.msgPesquisa("Forma de Pagamento não encontrado", dgvFormaPagamento.RowCount);

            txtPesquisar.Text = null;
        }

        private void alterarFormaPagamento()
        {
            if (dgvFormaPagamento.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                FormaPagamento formaPagamentoSelecionado = dgvFormaPagamento.SelectedRows[0].DataBoundItem as FormaPagamento;

                frmCadFormaPagamento objFrmCadFormaPagamento = new frmCadFormaPagamento(AcaoCRUD.Alterar, formaPagamentoSelecionado);
                DialogResult resultado = objFrmCadFormaPagamento.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaFormaPagamento("%");
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um FormaPagamento para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadFormaPagamentoFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadFormaPagamentoFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvFormaPagamento_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisar.Focus();
            txtPesquisar.Text = "";
        }

        #endregion
       
    }
}
