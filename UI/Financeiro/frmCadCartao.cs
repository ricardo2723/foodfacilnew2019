﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadCartao : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadCartao(AcaoCRUD acao, Cartao cartao)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Cartão";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Cartão";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = cartao.idCartao.ToString(); 
                txtNome.Text = cartao.nomeCartao.ToUpper();
                txtTaxa.Text = cartao.taxaCartao.ToString();
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirCartao();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarCartao();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirCartao()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) && !String.IsNullOrEmpty(txtTaxa.Text)) 
            {

                Cartao cartao = new Cartao();

                cartao.usuarioId = frmLogin.usuariosLogin.usuarioId;
                cartao.nomeCartao = txtNome.Text;
                try
                {
                    cartao.taxaCartao = Convert.ToDecimal(txtTaxa.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Digite apenas numeros no campo Taxa", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                
                cartao.dataCadCartao = DateTime.Now;

                CartaoNegocios cartaoNegocios = new CartaoNegocios();
                string retorno = cartaoNegocios.inserir(cartao);

                try
                {
                    int idCartao = Convert.ToInt32(retorno);
                    MessageBox.Show("Cartão Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }
                else if (String.IsNullOrEmpty(txtTaxa.Text))
                {
                    MessageBox.Show("Campo Taxa é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxa.Focus();
                }

            }
        }

        private void atualizarCartao()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text))
            {

                Cartao cartao = new Cartao();

                cartao.idCartao = Convert.ToInt32(txtCodigo.Text);
                cartao.usuarioId = frmLogin.usuariosLogin.usuarioId;
                cartao.nomeCartao = txtNome.Text;
                try
                {
                    cartao.taxaCartao = Convert.ToDecimal(txtTaxa.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Digite apenas numeros no campo Taxa", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                cartao.dataCadCartao = DateTime.Now;

                CartaoNegocios cartaoNegocios = new CartaoNegocios();
                string retorno = cartaoNegocios.Alterar(cartao);

                try
                {
                    int idCartao = Convert.ToInt32(retorno);
                    MessageBox.Show("Cartão Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }
                else if (String.IsNullOrEmpty(txtTaxa.Text))
                {
                    MessageBox.Show("Campo Taxa é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxa.Focus();
                }
                                
            }
        }

        #endregion       
        
    }
}
