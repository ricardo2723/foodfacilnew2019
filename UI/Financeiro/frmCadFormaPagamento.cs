﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadFormaPagamento : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadFormaPagamento(AcaoCRUD acao, FormaPagamento formaPagamento)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar FormaPagamento";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar FormaPagamento";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = formaPagamento.formaPagamentoId.ToString();
                txtDescricao.Text = formaPagamento.formaPagamentoDescricao;               
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirFormaPagamento();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarFormaPagamento();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirFormaPagamento()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                FormaPagamento formaPagamento = new FormaPagamento();

                formaPagamento.usuarioId = frmLogin.usuariosLogin.usuarioId;
                formaPagamento.formaPagamentoDescricao = txtDescricao.Text;
                formaPagamento.formaPagamentoDataCad = DateTime.Now;

                FormaPagamentoNegocios formaPagamentoNegocios = new FormaPagamentoNegocios();
                string retorno = formaPagamentoNegocios.inserir(formaPagamento);

                try
                {
                    int idFormaPagamento = Convert.ToInt32(retorno);
                    MessageBox.Show("Forma de Pagamento Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }

            }
        }

        private void atualizarFormaPagamento()
        {
            if
               (!String.IsNullOrEmpty(txtDescricao.Text))
            {

                FormaPagamento formaPagamento = new FormaPagamento();

                formaPagamento.formaPagamentoId = Convert.ToInt32(txtCodigo.Text);
                formaPagamento.usuarioId = frmLogin.usuariosLogin.usuarioId;
                formaPagamento.formaPagamentoDescricao = txtDescricao.Text;
                formaPagamento.formaPagamentoDataCad = DateTime.Now;

                FormaPagamentoNegocios formaPagamentoNegocios = new FormaPagamentoNegocios();
                string retorno = formaPagamentoNegocios.Alterar(formaPagamento);

                try
                {
                    int idFormaPagamento = Convert.ToInt32(retorno);
                    MessageBox.Show("Forma de Pagamento Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    MessageBox.Show("Campo Descrição é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricao.Focus();
                }
                                
            }
        }

        #endregion       
        
    }
}
