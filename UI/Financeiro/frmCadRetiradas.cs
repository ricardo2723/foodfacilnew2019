﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadRetiradas : Form
    {
        public frmCadRetiradas()
        {
            InitializeComponent();
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            inserirRetiradas();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadExtras_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirRetiradas()
        {
            frmPermissaoAdm objPermissao = new frmPermissaoAdm();
            DialogResult permissao = objPermissao.ShowDialog();

            if (permissao == DialogResult.Yes)
            {
                if
                   (!String.IsNullOrEmpty(txtValor.Text) &&
                    !String.IsNullOrEmpty(txtValor.Text) &&
                    Principal.caixa <= 0)
                {

                    Retiradas retiradas = new Retiradas();

                    retiradas.idUsuario = frmLogin.usuariosLogin.usuarioId;
                    try
                    {
                        retiradas.valorRetirada = Convert.ToDecimal(txtValor.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("No Campo Valor e Obrigatório Somente Numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        txtValor.Text = "";
                        txtValor.Focus();
                        return;
                    }
                    retiradas.motivoRetirada = txtMotivo.Text;
                    retiradas.administradorRetirada = objPermissao.administradorRetirada;
                    retiradas.dataRetirada = DateTime.Now.Date;
                    retiradas.idCaixaRetirada = Principal.caixa;

                    RetiradasNegocios retiradasNegocios = new RetiradasNegocios();
                    string retorno = retiradasNegocios.inserir(retiradas);

                    try
                    {
                        int idRetiradas = Convert.ToInt32(retorno);
                        MessageBox.Show("Retirada concluida com Sucesso", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.Yes;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(txtValor.Text))
                    {
                        MessageBox.Show("Campo Valor é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtValor.Focus();
                    }
                    if (String.IsNullOrEmpty(txtMotivo.Text))
                    {
                        MessageBox.Show("Campo Motivo é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtMotivo.Focus();
                    }

                    if (Principal.caixa <= 0)
                    {
                        MessageBox.Show("O Caixa ainda não foi Aberto Impossivel fazer uma Retirada", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                }
            }
            else
            {
                MessageBox.Show("E necessário Autorização de um Administrador para efetuar Retiradas.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        #endregion

    }
}
