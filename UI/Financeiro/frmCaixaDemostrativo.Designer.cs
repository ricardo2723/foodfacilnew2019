﻿namespace UI
{
    partial class frmCaixaDemostrativo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCaixaDemostrativo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDataAbertura = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblHoraAbertura = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblDataFechamento = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblAbertoPor = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblCaixaAbertoCom = new System.Windows.Forms.Label();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lblCodigoAbertura = new System.Windows.Forms.Label();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.lblValorDelivery = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.lblRetirada = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.lblValorCaixa = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lblValorVendas = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblTicket = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lblCartao = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblDinheiro = new System.Windows.Forms.Label();
            this.btnFechaCaixa = new DevComponents.DotNetBar.ButtonX();
            this.btnImprimir = new DevComponents.DotNetBar.ButtonX();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panelEx1.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblDataAbertura);
            this.groupBox1.Location = new System.Drawing.Point(127, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(120, 44);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data de Abertura";
            // 
            // lblDataAbertura
            // 
            this.lblDataAbertura.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataAbertura.ForeColor = System.Drawing.Color.Blue;
            this.lblDataAbertura.Location = new System.Drawing.Point(4, 18);
            this.lblDataAbertura.Name = "lblDataAbertura";
            this.lblDataAbertura.Size = new System.Drawing.Size(109, 23);
            this.lblDataAbertura.TabIndex = 0;
            this.lblDataAbertura.Text = "11/11/1111";
            this.lblDataAbertura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblHoraAbertura);
            this.groupBox2.Location = new System.Drawing.Point(253, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(120, 44);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hora da Abertura";
            // 
            // lblHoraAbertura
            // 
            this.lblHoraAbertura.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraAbertura.ForeColor = System.Drawing.Color.Blue;
            this.lblHoraAbertura.Location = new System.Drawing.Point(8, 18);
            this.lblHoraAbertura.Name = "lblHoraAbertura";
            this.lblHoraAbertura.Size = new System.Drawing.Size(102, 23);
            this.lblHoraAbertura.TabIndex = 0;
            this.lblHoraAbertura.Text = "00:00:00";
            this.lblHoraAbertura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblDataFechamento);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Location = new System.Drawing.Point(379, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(138, 44);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data do Fechamento";
            // 
            // lblDataFechamento
            // 
            this.lblDataFechamento.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataFechamento.ForeColor = System.Drawing.Color.Red;
            this.lblDataFechamento.Location = new System.Drawing.Point(6, 18);
            this.lblDataFechamento.Name = "lblDataFechamento";
            this.lblDataFechamento.Size = new System.Drawing.Size(126, 23);
            this.lblDataFechamento.TabIndex = 0;
            this.lblDataFechamento.Text = "11/11/1111";
            this.lblDataFechamento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblAbertoPor);
            this.groupBox4.Location = new System.Drawing.Point(7, 73);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(332, 44);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Caixa Aberto Por";
            // 
            // lblAbertoPor
            // 
            this.lblAbertoPor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAbertoPor.ForeColor = System.Drawing.Color.Black;
            this.lblAbertoPor.Location = new System.Drawing.Point(6, 18);
            this.lblAbertoPor.Name = "lblAbertoPor";
            this.lblAbertoPor.Size = new System.Drawing.Size(320, 23);
            this.lblAbertoPor.TabIndex = 0;
            this.lblAbertoPor.Text = "USUÁRIO";
            this.lblAbertoPor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblCaixaAbertoCom);
            this.groupBox5.Location = new System.Drawing.Point(345, 73);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(172, 44);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Caixa Aberto com";
            // 
            // lblCaixaAbertoCom
            // 
            this.lblCaixaAbertoCom.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaixaAbertoCom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblCaixaAbertoCom.Location = new System.Drawing.Point(6, 18);
            this.lblCaixaAbertoCom.Name = "lblCaixaAbertoCom";
            this.lblCaixaAbertoCom.Size = new System.Drawing.Size(160, 23);
            this.lblCaixaAbertoCom.TabIndex = 0;
            this.lblCaixaAbertoCom.Text = "0,00";
            this.lblCaixaAbertoCom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.groupBox13);
            this.panelEx1.Controls.Add(this.groupBox1);
            this.panelEx1.Controls.Add(this.groupBox5);
            this.panelEx1.Controls.Add(this.groupBox2);
            this.panelEx1.Controls.Add(this.groupBox4);
            this.panelEx1.Controls.Add(this.groupBox3);
            this.panelEx1.Location = new System.Drawing.Point(5, 6);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(523, 132);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 3;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.lblCodigoAbertura);
            this.groupBox13.Location = new System.Drawing.Point(7, 13);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(114, 44);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Código da Abertura";
            // 
            // lblCodigoAbertura
            // 
            this.lblCodigoAbertura.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoAbertura.ForeColor = System.Drawing.Color.Blue;
            this.lblCodigoAbertura.Location = new System.Drawing.Point(6, 18);
            this.lblCodigoAbertura.Name = "lblCodigoAbertura";
            this.lblCodigoAbertura.Size = new System.Drawing.Size(94, 23);
            this.lblCodigoAbertura.TabIndex = 0;
            this.lblCodigoAbertura.Text = "000000";
            this.lblCodigoAbertura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.groupBox14);
            this.groupPanel1.Controls.Add(this.groupBox12);
            this.groupPanel1.Controls.Add(this.groupBox11);
            this.groupPanel1.Controls.Add(this.groupBox10);
            this.groupPanel1.Controls.Add(this.groupBox9);
            this.groupPanel1.Controls.Add(this.groupBox8);
            this.groupPanel1.Controls.Add(this.groupBox7);
            this.groupPanel1.Controls.Add(this.groupBox6);
            this.groupPanel1.Location = new System.Drawing.Point(5, 145);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(523, 155);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 4;
            this.groupPanel1.Text = "Valores de Vendas";
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Transparent;
            this.groupBox14.Controls.Add(this.lblValorDelivery);
            this.groupBox14.Location = new System.Drawing.Point(124, 69);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(109, 58);
            this.groupBox14.TabIndex = 4;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Valor Delivery";
            // 
            // lblValorDelivery
            // 
            this.lblValorDelivery.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorDelivery.ForeColor = System.Drawing.Color.Black;
            this.lblValorDelivery.Location = new System.Drawing.Point(6, 16);
            this.lblValorDelivery.Name = "lblValorDelivery";
            this.lblValorDelivery.Size = new System.Drawing.Size(97, 37);
            this.lblValorDelivery.TabIndex = 0;
            this.lblValorDelivery.Text = "0,00";
            this.lblValorDelivery.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Transparent;
            this.groupBox12.Controls.Add(this.lblRetirada);
            this.groupBox12.Location = new System.Drawing.Point(388, 13);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(126, 44);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Valor de Retiradas";
            // 
            // lblRetirada
            // 
            this.lblRetirada.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRetirada.ForeColor = System.Drawing.Color.Blue;
            this.lblRetirada.Location = new System.Drawing.Point(6, 18);
            this.lblRetirada.Name = "lblRetirada";
            this.lblRetirada.Size = new System.Drawing.Size(111, 23);
            this.lblRetirada.TabIndex = 0;
            this.lblRetirada.Text = "0,00";
            this.lblRetirada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Transparent;
            this.groupBox11.Controls.Add(this.lblValorCaixa);
            this.groupBox11.Location = new System.Drawing.Point(354, 69);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(160, 61);
            this.groupBox11.TabIndex = 3;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Valor em Caixa";
            // 
            // lblValorCaixa
            // 
            this.lblValorCaixa.Font = new System.Drawing.Font("Arial", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorCaixa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblValorCaixa.Location = new System.Drawing.Point(6, 16);
            this.lblValorCaixa.Name = "lblValorCaixa";
            this.lblValorCaixa.Size = new System.Drawing.Size(154, 42);
            this.lblValorCaixa.TabIndex = 0;
            this.lblValorCaixa.Text = "0,00";
            this.lblValorCaixa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Transparent;
            this.groupBox10.Controls.Add(this.lblValorVendas);
            this.groupBox10.Location = new System.Drawing.Point(239, 69);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(109, 58);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Valor das Vendas";
            // 
            // lblValorVendas
            // 
            this.lblValorVendas.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorVendas.ForeColor = System.Drawing.Color.Black;
            this.lblValorVendas.Location = new System.Drawing.Point(6, 16);
            this.lblValorVendas.Name = "lblValorVendas";
            this.lblValorVendas.Size = new System.Drawing.Size(97, 37);
            this.lblValorVendas.TabIndex = 0;
            this.lblValorVendas.Text = "0,00";
            this.lblValorVendas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.lblDesconto);
            this.groupBox9.Location = new System.Drawing.Point(4, 69);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(114, 44);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Valor de Descontos";
            // 
            // lblDesconto
            // 
            this.lblDesconto.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesconto.ForeColor = System.Drawing.Color.Red;
            this.lblDesconto.Location = new System.Drawing.Point(6, 18);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(102, 23);
            this.lblDesconto.TabIndex = 0;
            this.lblDesconto.Text = "0,00";
            this.lblDesconto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.lblTicket);
            this.groupBox8.Location = new System.Drawing.Point(262, 13);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(117, 44);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Valor em Ticket";
            // 
            // lblTicket
            // 
            this.lblTicket.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTicket.ForeColor = System.Drawing.Color.Blue;
            this.lblTicket.Location = new System.Drawing.Point(6, 18);
            this.lblTicket.Name = "lblTicket";
            this.lblTicket.Size = new System.Drawing.Size(105, 23);
            this.lblTicket.TabIndex = 0;
            this.lblTicket.Text = "0,00";
            this.lblTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.lblCartao);
            this.groupBox7.Location = new System.Drawing.Point(141, 13);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(115, 44);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Valor em Cartão";
            // 
            // lblCartao
            // 
            this.lblCartao.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartao.ForeColor = System.Drawing.Color.Blue;
            this.lblCartao.Location = new System.Drawing.Point(6, 18);
            this.lblCartao.Name = "lblCartao";
            this.lblCartao.Size = new System.Drawing.Size(103, 23);
            this.lblCartao.TabIndex = 0;
            this.lblCartao.Text = "0,00";
            this.lblCartao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.lblDinheiro);
            this.groupBox6.Location = new System.Drawing.Point(4, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(131, 44);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Valor em Dinheiro";
            // 
            // lblDinheiro
            // 
            this.lblDinheiro.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDinheiro.ForeColor = System.Drawing.Color.Blue;
            this.lblDinheiro.Location = new System.Drawing.Point(6, 18);
            this.lblDinheiro.Name = "lblDinheiro";
            this.lblDinheiro.Size = new System.Drawing.Size(119, 23);
            this.lblDinheiro.TabIndex = 0;
            this.lblDinheiro.Text = "0,00";
            this.lblDinheiro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFechaCaixa
            // 
            this.btnFechaCaixa.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFechaCaixa.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFechaCaixa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechaCaixa.Image = global::UI.Properties.Resources._1383708463_checkmark;
            this.btnFechaCaixa.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnFechaCaixa.Location = new System.Drawing.Point(5, 306);
            this.btnFechaCaixa.Name = "btnFechaCaixa";
            this.btnFechaCaixa.Size = new System.Drawing.Size(240, 44);
            this.btnFechaCaixa.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFechaCaixa.TabIndex = 9;
            this.btnFechaCaixa.Text = "Realizar Fechamento (F9)";
            this.btnFechaCaixa.TextColor = System.Drawing.Color.Green;
            this.btnFechaCaixa.Click += new System.EventHandler(this.btnFechaCaixa_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnImprimir.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnImprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Image = global::UI.Properties.Resources.printer;
            this.btnImprimir.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnImprimir.Location = new System.Drawing.Point(251, 306);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(277, 44);
            this.btnImprimir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnImprimir.TabIndex = 8;
            this.btnImprimir.Text = "<font color=\"#0A11FF\"><b>Imprimir Demonstrativo (F10)</b></font>";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // frmCaixaDemostrativo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 364);
            this.Controls.Add(this.btnFechaCaixa);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.panelEx1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCaixaDemostrativo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Food Facil - Demostrativo Caixa";
            this.Load += new System.EventHandler(this.frmCaixaDemostrativo_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCaixaDemostrativo_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmCaixaDemostrativo_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panelEx1.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDataAbertura;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblHoraAbertura;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblDataFechamento;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblAbertoPor;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblCaixaAbertoCom;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label lblValorCaixa;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lblValorVendas;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lblTicket;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lblCartao;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblDinheiro;
        private DevComponents.DotNetBar.ButtonX btnFechaCaixa;
        private DevComponents.DotNetBar.ButtonX btnImprimir;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label lblRetirada;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label lblCodigoAbertura;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label lblValorDelivery;
    }
}