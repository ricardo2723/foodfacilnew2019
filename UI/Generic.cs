﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UI
{
    public static class Generic
    {
        public static void limparCampos(Control crtl)
        {
            //Faz um laço para todos os controles passados no parâmetro
            foreach (Control ctrl in crtl.Controls)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ctrl.Text = "";
                }
                if (ctrl is ComboBox)
                {
                    ctrl.Text = null;
                }
                if (ctrl is MaskedTextBox)
                {
                    ctrl.Text = null;
                }
            }
        }

        public static int checaInteiro(string cod)
        {
            try
            {
                int cod1 = Convert.ToInt32(cod);

                return cod1;
            }
            catch (Exception)
            {
                MessageBox.Show("Código do Produto Inválido. É obrigatório somente numeros Inteiros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
        }

        public static void checaDecimal(string cod)
        {
            try
            {
                decimal cod1 = Convert.ToDecimal(cod);
            }
            catch (Exception)
            {
                MessageBox.Show("O valor e Inválido. É obrigatório somente numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        public static void habilitarCampos(Control crtl)
        {
            //Faz um laço para todos os controles passados no parâmetro
            foreach (Control ctrl in crtl.Controls)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ctrl.Enabled = true;
                }
                if (ctrl is ComboBox)
                {
                    ctrl.Enabled = true;
                }
                if (ctrl is MaskedTextBox)
                {
                    ctrl.Enabled = true;
                }

            }
        }

        public static string formatCodigo(string text)
        {
            string getText;

            if (text.Length == 1)
            {
                getText = "00000" + text;
            }
            else if (text.Length == 2)
            {
                getText = "0000" + text;
            }
            else if (text.Length == 3)
            {
                getText = "000" + text;
            }
            else if (text.Length == 4)
            {
                getText = "00" + text;
            }
            else if (text.Length == 5)
            {
                getText = "0" + text;
            }
            else if (text.Length == 6)
            {
                getText = text;
            }
            else
            {
                MessageBox.Show("Exerceu o numero de Caracteres", " Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                getText = "";
            }

            return getText;

        }

        public static string formatItens(int item)
        {
            string getString;

            if (item <= 9)
            {
                getString = "00";
            }
            else if (item >= 10 || item <= 99)
            {
                getString = "0";
            }
            else if (item > 99)
            {
                getString = "";
            }
            else
            {
                MessageBox.Show("Os itens exerceu o numero de Caracteres", " Alerta", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                getString = "";
            }

            return getString;
        }

        public static decimal totalGeralDgv(DataGridView dgv)
        {
            string total;
            decimal ValorTotal = 0;

            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                DataGridViewRow d = dgv.Rows[i];
                //d.Cells[0].Value = ClassPdvProdutos.formatItens(i) + "" + (i + 1);

                total = d.Cells[4].Value.ToString();

                ValorTotal += Convert.ToDecimal(total.Trim(new char[] { ' ', 'R', '$' }));
            }

            return ValorTotal;
        }

        public static void butonn(string but, Button btn)
        {
            switch (but)
            {
                case "btnPedM1":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM2":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM3":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM4":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM5":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM6":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM7":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM8":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM9":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM10":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM11":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM12":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM13":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM14":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM15":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM16":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM17":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM18":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM19":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM20":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM21":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM22":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM23":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM24":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM25":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM26":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM27":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM28":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM29":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM30":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM31":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM32":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM33":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM34":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM35":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedM36":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB1":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB2":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB3":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB4":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB5":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB6":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB7":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB8":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB9":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB10":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB11":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB12":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB13":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB14":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB15":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB16":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB17":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedB18":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC1":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC2":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC3":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC4":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC5":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC6":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC7":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC8":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC9":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC10":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC11":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC12":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC13":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC14":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC15":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC16":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC17":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
                case "btnPedC18":
                    btn.BackColor = System.Drawing.Color.Red;
                    break;
            }

        }

        public static void salvarBkp()
        {
            try
            {
                string fileName = "LancheFacil" + DateTime.Now.Date.ToShortDateString().Replace("/", "") + ".bak";
                string sourcePath = "C:\\Program Files\\Microsoft SQL Server\\MSSQL11.SQLEXPRESS\\MSSQL\\Backup";
                string targetPath = Application.StartupPath.ToString() + @"\Backup\";
                
                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                string destFile = System.IO.Path.Combine(targetPath, fileName);

                // To copy a folder's contents to a new location:
                // Create a new target folder, if necessary.
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);

                // To copy all the files in one directory to another directory.
                // Get the files in the source folder. (To recursively iterate through
                // all subfolders under the current directory, see
                // "How to: Iterate Through a Directory Tree.")
                // Note: Check for target path was performed previously
                //       in this code example.
            /*    if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcePath);

                    // Copy the files and overwrite destination files if they already exist.
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        fileName = System.IO.Path.GetFileName(s);
                        destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }
                else
                {
                    Console.WriteLine("O Arquivo Não Existe");
                }*/
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ao Salvar Arquivo de Backup: " + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public static int checaImpressao()
        {
            int statusPrint = 0;

            if (frmSplash.tipoImpressao == 0)
                statusPrint = 3;
            else if (frmSplash.tipoImpressao == 1)
                statusPrint = 2;
            else
            {
                MessageBox.Show("Error ao Enviar Pedido tipo de Impressão não configurado", "Atenção",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.Exit();
            }
            return statusPrint;
        }

        public static void msgPesquisa(string msg, int retorno)
        {
            if (retorno <= 0)
            {
                MessageBox.Show(msg, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
