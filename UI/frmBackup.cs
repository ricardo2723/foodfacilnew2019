﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;

using AcessoBancoDados;

namespace UI
{
    public partial class frmBackup : Form
    {
        public frmBackup()
        {
            InitializeComponent();
        }

        private void frmBackup_Load(object sender, EventArgs e)
        {
            circularProgress1.IsRunning = true;
            bwBackup.RunWorkerAsync();                                   
        }

        private void bwBackup_DoWork(object sender, DoWorkEventArgs e)
        {
           while (!bwBackup.CancellationPending)
            {
                bwBackup.ReportProgress(0, "Criando Backup do Banco de Dados...");
                Thread.Sleep(500);
                backupDBase();
                bwBackup.CancelAsync();
            }
        }

        private void bwBackup_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMenssagem.Text = e.UserState.ToString();
        }

        private void bwBackup_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.Exit();
        }

        private void frmBackup_FormClosed(object sender, FormClosedEventArgs e)
        {
            bwBackup.CancelAsync();
        }

        private void backupDBase()
        {
            if (!System.IO.Directory.Exists(Application.StartupPath.ToString() + "\\Backup"))
            {
                //POSTERIORMENTE CRIAR UMA JANELA PARA CONFIGURAR LOCAL DO BANCO DE DADOS
                System.IO.Directory.CreateDirectory(Application.StartupPath.ToString() + "\\Backup");
            }

            //string localDb = "backup database lancheFacil to disk = '" + destdir + "\\LancheFacil" + DateTime.Now.Date.ToShortDateString().Replace("/", "") + ".bak'";
            //string localDb = "backup database lancheFacil to disk = " + "'" + Application.StartupPath.ToString() + "\\Backup\\LancheFacil" + DateTime.Now.Date.ToShortDateString().Replace("/", "") + ".bak'";
            string localDb = "backup database lancheFacil to disk = " + "'C:\\Program Files\\Microsoft SQL Server\\MSSQL11.SQLEXPRESS\\MSSQL\\Backup\\LancheFacil" + DateTime.Now.Date.ToShortDateString().Replace("/", "") + ".bak'";
            
            BackupDB bkp = new BackupDB();

            int retorno = bkp.IniciaBackup(localDb);

            if (retorno == 1)
            {
                MessageBox.Show("Error ao Fazer Backup", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            Generic.salvarBkp();
        }
    }
}