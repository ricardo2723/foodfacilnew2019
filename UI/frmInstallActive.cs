﻿using System;
using System.Windows.Forms;

using Negocios;

namespace UI
{
    public partial class frmInstallActive : Form
    {
        SerialNegocios serialNegocios = new SerialNegocios();

        string instalacao, ativacao;

        public frmInstallActive()
        {
            InitializeComponent();
        }

        private void btnAtivarSistema_Click(object sender, EventArgs e)
        {
            if (txtInstalacao.Text == "" || txtAtivacao.Text == "")
            {
                return;
            }

            instalacao = txtInstalacao.Text;
            ativacao = txtAtivacao.Text;
            try
            {
                if (ativacao == gerarSerialAtivacao(instalacao))
                {
                    string retorno = serialNegocios.inserir(instalacao, ativacao);

                    int ret = Convert.ToInt32(retorno);

                    this.DialogResult = DialogResult.Yes;
                }
                else
                {
                    MessageBox.Show("ATIVAÇÃO INVÁLIDA, PROVIDENCIE UM SERIAL VÁLIDO", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRO AO TENTAR VALIDAR O SISTEMA: " + ex, "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }            
        }

        private string gerarSerialAtivacao(string install)
        {
            try
            {
                int pri, sec, ter, qua, qui;
                string ativ;

                pri = Convert.ToInt32(install.Substring(0, 6)) + 999999;
                sec = Convert.ToInt32(install.Substring(7, 6)) + 999999;
                ter = Convert.ToInt32(install.Substring(14, 6)) + 999999;
                qua = Convert.ToInt32(install.Substring(21, 6)) + 999999;
                qui = Convert.ToInt32(install.Substring(28, 6)) + 999999;

                ativ = pri.ToString("000000") + "-" + sec.ToString("000000") + "-" + ter.ToString("000000") + "-" + qua.ToString("000000") + "-" + qui.ToString("000000");
                return ativ;
            }
            catch (Exception)
            {
                MessageBox.Show("Serial Inválido", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }

        }
    }
}
