﻿using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace UI.GenericFunctions
{
    public class MoveForm
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        public extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        public extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        public static void MouseMoveForm(Form thisForm)
        {
            ReleaseCapture();
            SendMessage(thisForm.Handle, 0x112, 0xf012, 0);
        }
    }
}
