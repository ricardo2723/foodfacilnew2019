﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.GenericFunctions
{
    public enum AcaoGrud
    {
        Insert = 1,
        Update = 2,
        CashBoxOpen = 3,
        CashBoxClose = 4,
        GetProduct = 5,
        ExistPedido = 6,
        NoExistPedido = 7,
        ProdutoNaoManufaturadoAtualizaEstoque = 8,
        ProdutoManufaturadoAtualizaEstoque = 9,
        ProdutoManufaturadoAtualizaNaoControlaEstoque = 10
    }
}
