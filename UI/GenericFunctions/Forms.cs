﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.GenericFunctions
{
    public class Forms
    {
        public static DialogResult OpenFormResult(object formOpen)
        {
            Form form = formOpen as Form;
            DialogResult resultado = form.ShowDialog();
            return resultado;
        }

        public static void OpenFormGeneric(object formOpen)
        {
            Form form = formOpen as Form;
            form.ShowDialog();
        }

        public static void OpenFormNoDialog(object formOpen)
        {
            Form form = formOpen as Form;
            form.Show();
        }

        public static void MinimizerForm(Form form)
        {
            form.WindowState = FormWindowState.Minimized;
        }
    }
}
