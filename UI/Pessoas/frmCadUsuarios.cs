﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;


namespace UI
{
    public partial class frmCadUsuarios : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadUsuarios(AcaoCRUD acao, Usuarios usuarios)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            NivelAcessoNegocios nivelAcessoNegocios = new NivelAcessoNegocios();
            NivelAcessoCollections nivelAcessoCollections = nivelAcessoNegocios.ConsultarNivelAcesso("%");

            cbNivelAcesso.DataSource = null;
            cbNivelAcesso.DataSource = nivelAcessoCollections;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Usuarios";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Usuarios";
                btnSalvar.Text = "  Salvar Alteração (F12)";
                
                txtCodigo.Text = usuarios.usuarioId.ToString();
                txtNome.Text = usuarios.usuarioNome;
                txtLogin.Text = usuarios.usuarioLogin;
                txtCpf.Text = usuarios.usuarioCpf;
                txtSenha.Text = usuarios.usuarioSenha;
                cbNivelAcesso.Text = usuarios.nivelAcessoDescricao;
                cbNivelAcesso.SelectedValue = usuarios.nivelAcessoId;
            }
        }

        #region BOTÕES

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirUsuario();
            }
            else if(acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarUsuario();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region AÇÕES

        private void frmCadUsuarios_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirUsuario()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtLogin.Text) &&
               !String.IsNullOrEmpty(txtSenha.Text) &&
               !String.IsNullOrEmpty(cbNivelAcesso.Text))
            {

                Usuarios usuarios = new Usuarios();
                
                usuarios.usuarioNome = txtNome.Text;
                usuarios.usuarioLogin = txtLogin.Text;
                usuarios.usuarioCpf = txtCpf.Text;
                usuarios.usuarioSenha = txtSenha.Text;
                usuarios.nivelAcessoId = Convert.ToInt32(cbNivelAcesso.SelectedValue);
                usuarios.usuarioDataCad = DateTime.Now.Date;
                usuarios.usuarioCadUsuarioId = frmLogin.usuariosLogin.usuarioId;

                UsuariosNegocios usuariosNegocios = new UsuariosNegocios();
                string retorno = usuariosNegocios.inserir(usuarios);

                try
                {
                    int idUsuario = Convert.ToInt32(retorno);
                    MessageBox.Show("Usuário Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;                  
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }                
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Produto / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtLogin.Text))
                {
                    MessageBox.Show("Campo Login é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtLogin.Focus();
                }

                else if (String.IsNullOrEmpty(txtSenha.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSenha.Focus();
                }

                else if (String.IsNullOrEmpty(cbNivelAcesso.Text))
                {
                    MessageBox.Show("Campo Nivel de Acesso é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbNivelAcesso.Focus();
                }
            }
        }

        private void atualizarUsuario()
        {
            if(!String.IsNullOrEmpty(txtNome.Text) && !String.IsNullOrEmpty(txtLogin.Text) && !String.IsNullOrEmpty(txtSenha.Text) && !String.IsNullOrEmpty(cbNivelAcesso.Text))
            {
                Usuarios usuarios = new Usuarios();

                usuarios.usuarioId = Convert.ToInt32(txtCodigo.Text);
                usuarios.usuarioNome = txtNome.Text;
                usuarios.usuarioLogin = txtLogin.Text;
                usuarios.usuarioCpf = txtCpf.Text;
                usuarios.usuarioSenha = txtSenha.Text;
                usuarios.nivelAcessoId = Convert.ToInt32(cbNivelAcesso.SelectedValue);
                usuarios.usuarioDataCad = DateTime.Now.Date;
                usuarios.usuarioCadUsuarioId = frmLogin.usuariosLogin.usuarioId;

                UsuariosNegocios usuariosNegocios = new UsuariosNegocios();
                string retorno = usuariosNegocios.Alterar(usuarios);

                try
                {
                    int idUsuario = Convert.ToInt32(retorno);
                    MessageBox.Show("Usuário Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Produto / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtLogin.Text))
                {
                    MessageBox.Show("Campo Login é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtLogin.Focus();
                }

                else if (String.IsNullOrEmpty(txtSenha.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSenha.Focus();
                }

                else if (String.IsNullOrEmpty(cbNivelAcesso.Text))
                {
                    MessageBox.Show("Campo Nivel de Acesso é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbNivelAcesso.Focus();
                }
            }
        }

        #endregion
    }
}
