﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadClientes : Form
    {
        AcaoCRUD acaoSelecionada;
        BairroCollections bairroCollections = new BairroCollections();
        Bairro bairro = new Bairro();
        BairroNegocios bairroNegocios = new BairroNegocios();

        public frmCadClientes(AcaoCRUD acao, Clientes clientes)
        {
            InitializeComponent();
            
            acaoSelecionada = acao;

            bairroCollections = bairroNegocios.ConsultarNome("%");

            cbBairro.DataSource = null;
            cbBairro.DataSource = bairroCollections;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Clientes";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Clientes";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = clientes.idCliente.ToString();
                txtNome.Text = clientes.nomeCliente.ToUpper();
                txtCelular.Text = clientes.celularCliente.ToUpper();
                txtEmail.Text = clientes.emailCliente.ToUpper();
                txtCpf.Text = clientes.cpfCliente.ToUpper();
                txtCnpj.Text = clientes.cnpjCliente.ToUpper();
                txtRg.Text = clientes.identidadeCliente.ToUpper();
                txtTelefone.Text = clientes.telefoneCliente.ToUpper();
                txtCep.Text = clientes.cepCliente.ToUpper();
                txtEndereco.Text = clientes.enderecoCliente.ToUpper();
                txtComplemento.Text = clientes.complementoCliente.ToUpper();
                cbBairro.Text = clientes.bairroCliente.ToUpper();
                txtCidade.Text = clientes.municipioCliente.ToUpper();
                cbUf.Text = clientes.ufCliente.ToUpper();
                dtpDataNascimento.Value =  clientes.dataNascimentoCliente;
                txtMaisInformacoes.Text = clientes.pontoReferenciaCliente.ToUpper();
            }
        }
        
        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirCliente();
            }
            else if (acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarCliente();
            }
        }

        private void btnAddBairro_Click(object sender, EventArgs e)
        {
            frmCadBairro objFrmExtras = new frmCadBairro(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmExtras.ShowDialog();
            
            if (resultado == DialogResult.Yes)
            {
                bairroCollections = bairroNegocios.ConsultarNome("%");

                cbBairro.DataSource = null;
                cbBairro.DataSource = bairroCollections;
                cbBairro.DisplayMember = "nomeBairro";
                
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadClientes_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void frmCadClientes_Load(object sender, EventArgs e)
        {
                                  
        }

        #endregion

        #region METODOS

        private void inserirCliente()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtCelular.Text) &&
               !String.IsNullOrEmpty(cbBairro.Text))
            {

                Clientes clientes = new Clientes();

                clientes.nomeCliente = txtNome.Text;
                clientes.celularCliente = txtCelular.Text;
                clientes.emailCliente = txtEmail.Text;
                clientes.cpfCliente = txtCpf.Text;
                clientes.cnpjCliente = txtCnpj.Text;
                clientes.identidadeCliente = txtRg.Text;
                clientes.telefoneCliente = txtTelefone.Text;
                clientes.cepCliente = txtCep.Text;
                clientes.enderecoCliente = txtEndereco.Text;
                clientes.complementoCliente = txtComplemento.Text;
                clientes.bairroCliente = cbBairro.Text;
                clientes.municipioCliente = txtCidade.Text;
                clientes.ufCliente = cbUf.Text;
                clientes.dataNascimentoCliente = dtpDataNascimento.Value;
                clientes.pontoReferenciaCliente = txtMaisInformacoes.Text;
                clientes.dataCadastroCliente = DateTime.Now;
                clientes.usuarioId = frmLogin.usuariosLogin.usuarioId;

                ClientesNegocios clientesNegocios = new ClientesNegocios();
                string retorno = clientesNegocios.inserir(clientes);

                try
                {
                    int idClientes = Convert.ToInt32(retorno);
                    MessageBox.Show("Cliente Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Cliente / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(cbBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbBairro.Focus();
                }

                else if (String.IsNullOrEmpty(txtCelular.Text))
                {
                    MessageBox.Show("Campo Celular é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCelular.Focus();
                }
            }
        }

        private void atualizarCliente()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtCelular.Text) &&
               !String.IsNullOrEmpty(cbBairro.Text))
            {

                Clientes clientes = new Clientes();

                clientes.idCliente = Convert.ToInt32(txtCodigo.Text);
                clientes.nomeCliente = txtNome.Text;
                clientes.celularCliente = txtCelular.Text;
                clientes.emailCliente = txtEmail.Text;
                clientes.cpfCliente = txtCpf.Text;
                clientes.cnpjCliente = txtCnpj.Text;
                clientes.identidadeCliente = txtRg.Text;
                clientes.telefoneCliente = txtTelefone.Text;
                clientes.cepCliente = txtCep.Text;
                clientes.enderecoCliente = txtEndereco.Text;
                clientes.complementoCliente = txtComplemento.Text;
                clientes.bairroCliente = cbBairro.Text;
                clientes.municipioCliente = txtCidade.Text;
                clientes.ufCliente = cbUf.Text;
                clientes.dataNascimentoCliente = dtpDataNascimento.Value;
                clientes.pontoReferenciaCliente = txtMaisInformacoes.Text;
                clientes.dataCadastroCliente = DateTime.Now;
                clientes.usuarioId = frmLogin.usuariosLogin.usuarioId;

                ClientesNegocios clientesNegocios = new ClientesNegocios();
                string retorno = clientesNegocios.Alterar(clientes);

                try
                {
                    int idClientes = Convert.ToInt32(retorno);
                    MessageBox.Show("Cliente alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Cliente / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(cbBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbBairro.Focus();
                }

                else if (String.IsNullOrEmpty(txtCelular.Text))
                {
                    MessageBox.Show("Campo Celular é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCelular.Focus();
                }
            }
        }

        #endregion

        

    }
}
