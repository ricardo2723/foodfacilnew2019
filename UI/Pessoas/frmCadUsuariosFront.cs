﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadUsuariosFront : Form
    {       
        public frmCadUsuariosFront()
        {
            InitializeComponent();
            //Não gerar Linhas Automaticas
            dgvUsuarios.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadUsuarios objFrmCadUsuarios = new frmCadUsuarios(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmCadUsuarios.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaUsuarioNome("%");
            }            
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarUsuario();           
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (rbNome.Checked)
            {
                pesquisaUsuarioNome(txtPesquisa.Text);
            }
            else if (rbCpf.Checked)
            {
                pesquisaUsuarioCpf(txtPesquisa.Text);
            }
            else if (rbLogin.Checked)
            {
                pesquisaUsuarioLogin(txtPesquisa.Text);
            }

            txtPesquisa.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Usuario? Todos as informações desse Usuario serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Usuarios usuarioSelecionado = dgvUsuarios.SelectedRows[0].DataBoundItem as Usuarios; 

                //instanciar a regra de negocios
                UsuariosNegocios usuariosNegocios = new UsuariosNegocios();
                string retorno = usuariosNegocios.Excluir(usuarioSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idUsuario = Convert.ToInt32(retorno);
                    MessageBox.Show("Usuario Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaUsuarioNome("%" + txtPesquisa.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Usuario para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "USUARIOS")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaUsuarioNome(string nome)
        {
            UsuariosNegocios usuarioNegocios = new UsuariosNegocios();
            UsuariosCollections usuariosCollections = usuarioNegocios.ConsultarPorNome(txtPesquisa.Text);

            dgvUsuarios.DataSource = null;
            dgvUsuarios.DataSource = usuariosCollections;
            
            dgvUsuarios.Update();
            dgvUsuarios.Refresh();
            
            labelX2.Text = "Usuarios Listados: " + dgvUsuarios.RowCount;

            Generic.msgPesquisa("Usuario não encontrado", dgvUsuarios.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaUsuarioCpf(string cpf)
        {
            UsuariosNegocios usuarioNegocios = new UsuariosNegocios();
            UsuariosCollections usuariosCollections = usuarioNegocios.ConsultarPorCpf(txtPesquisa.Text);

            dgvUsuarios.DataSource = null;
            dgvUsuarios.DataSource = usuariosCollections;

            dgvUsuarios.Update();
            dgvUsuarios.Refresh();

            labelX2.Text = "Usuarios Listados: " + dgvUsuarios.RowCount;

            Generic.msgPesquisa("Usuario não encontrado", dgvUsuarios.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaUsuarioLogin(string login)
        {
            UsuariosNegocios usuarioNegocios = new UsuariosNegocios();
            UsuariosCollections usuariosCollections = usuarioNegocios.ConsultarPorLogin(txtPesquisa.Text);

            dgvUsuarios.DataSource = null;
            dgvUsuarios.DataSource = usuariosCollections;

            dgvUsuarios.Update();
            dgvUsuarios.Refresh();

            labelX2.Text = "Usuarios Listados: " + dgvUsuarios.RowCount;

            Generic.msgPesquisa("Usuario não encontrado", dgvUsuarios.RowCount);

            txtPesquisa.Text = null;
        }

        private void alterarUsuario()
        {
            if (dgvUsuarios.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Usuarios usuarioSelecionado = dgvUsuarios.SelectedRows[0].DataBoundItem as Usuarios; 
                
                frmCadUsuarios objFrmCadUsuarios = new frmCadUsuarios(AcaoCRUD.Alterar, usuarioSelecionado);
                DialogResult resultado = objFrmCadUsuarios.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaUsuarioNome("%");
                }
                
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Usuario para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadUsuariosFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadUsuariosFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvUsuarios_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        #endregion
    }
}
