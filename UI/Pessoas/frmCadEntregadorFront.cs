﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadEntregadorFront : Form
    {
        public frmCadEntregadorFront()
        {
            InitializeComponent();
            //Não gerar Linhas Automaticas
            dgvEntregador.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadEntregador objFrmCadEntregador = new frmCadEntregador(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmCadEntregador.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaEntregadorNome("%");
            }            
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarEntregador();           
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (rbNome.Checked)
            {
                pesquisaEntregadorNome(txtPesquisa.Text);
            }
            else if (rbCpf.Checked)
            {
                pesquisaEntregadorCpf(txtPesquisa.Text);
            }
            else if (rbApelido.Checked)
            {
                pesquisaEntregadorApelido(txtPesquisa.Text);
            }
            txtPesquisa.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvEntregador.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Entregador? Todos as informações desse Entregador serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Entregador clientesSelecionado = dgvEntregador.SelectedRows[0].DataBoundItem as Entregador; 

                //instanciar a regra de negocios
                EntregadorNegocios clientesNegocios = new EntregadorNegocios();
                string retorno = clientesNegocios.Excluir(clientesSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idEntregador = Convert.ToInt32(retorno);
                    MessageBox.Show("Entregador Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaEntregadorNome("%" + txtPesquisa.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Entregador para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "ENTREGADORES")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaEntregadorNome(string nome)
        {

            EntregadorNegocios entregadorNegocios = new EntregadorNegocios();
            EntregadorCollections entregadorCollections = entregadorNegocios.ConsultarPorNome(txtPesquisa.Text);

            dgvEntregador.DataSource = null;
            dgvEntregador.DataSource = entregadorCollections;
            
            dgvEntregador.Update();
            dgvEntregador.Refresh();
            
            labelX2.Text = "Entregador Listados: " + dgvEntregador.RowCount;

            Generic.msgPesquisa("Entregador não encontrado", dgvEntregador.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaEntregadorApelido(string nome)
        {

            EntregadorNegocios entregadorNegocios = new EntregadorNegocios();
            EntregadorCollections entregadorCollections = entregadorNegocios.ConsultarPorApelido(txtPesquisa.Text);

            dgvEntregador.DataSource = null;
            dgvEntregador.DataSource = entregadorCollections;

            dgvEntregador.Update();
            dgvEntregador.Refresh();

            labelX2.Text = "Entregador Listados: " + dgvEntregador.RowCount;

            Generic.msgPesquisa("Entregador não encontrado", dgvEntregador.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaEntregadorCpf(string cpf)
        {
            EntregadorNegocios clientesNegocios = new EntregadorNegocios();
            EntregadorCollections clientesCollections = clientesNegocios.ConsultarPorCpf(txtPesquisa.Text);

            dgvEntregador.DataSource = null;
            dgvEntregador.DataSource = clientesCollections;

            dgvEntregador.Update();
            dgvEntregador.Refresh();

            labelX2.Text = "Entregador Listados: " + dgvEntregador.RowCount;

            Generic.msgPesquisa("Entregador não encontrado", dgvEntregador.RowCount);

            txtPesquisa.Text = null;
        }

        private void alterarEntregador()
        {
            if (dgvEntregador.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Entregador clientesSelecionado = dgvEntregador.SelectedRows[0].DataBoundItem as Entregador; 
                
                frmCadEntregador objFrmCadEntregador = new frmCadEntregador(AcaoCRUD.Alterar, clientesSelecionado);
                DialogResult resultado = objFrmCadEntregador.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaEntregadorNome("%");
                }
                
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Entregador para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadEntregadorFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadEntregadorFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvEntregador_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        #endregion

        

    }
}
