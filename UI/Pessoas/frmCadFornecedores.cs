﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace UI
{
    public partial class frmCadFornecedores : Form
    {
        AcaoCRUD acaoSelecionada;

        public frmCadFornecedores(AcaoCRUD acao, Fornecedores fornecedores)
        {
            InitializeComponent();

            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Fornecedores";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Fornecedores";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                txtCodigo.Text = fornecedores.idFornecedor.ToString();
                txtNome.Text = fornecedores.nomeFornecedor;
                txtFax.Text = fornecedores.faxFornecedor;
                txtEmail.Text = fornecedores.emailFornecedor;
                txtContato.Text = fornecedores.contatoFornecedor;
                txtCnpj.Text = fornecedores.cnpjFornecedor;
                txtInscEstadual.Text = fornecedores.inscricaoEstadualFornecedor;
                txtTelefone.Text = fornecedores.telefoneFornecedor;
                txtCep.Text = fornecedores.cepFornecedor;
                txtEndereco.Text = fornecedores.enderecoFornecedor;
                txtComplemento.Text = fornecedores.complementoFornecedor;
                txtBairro.Text = fornecedores.bairroFornecedor;
                txtCidade.Text = fornecedores.municipioFornecedor;
                cbUf.Text = fornecedores.ufFornecedor;
                txtMaisInformacoes.Text = fornecedores.pontoReferenciaFornecedor;
                txtCpf.Text = fornecedores.cpfFornecedor;
            }
        }

        #region BOTÕES

        private void bntFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirFornecedor();
            }
            else if (acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarFornecedor();
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadClienteInf_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.bntFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirFornecedor()
        {
            if (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtTelefone.Text) &&
               !String.IsNullOrEmpty(txtBairro.Text))
            {

                Fornecedores fornecedores = new Fornecedores();

                fornecedores.nomeFornecedor = txtNome.Text;
                fornecedores.faxFornecedor = txtFax.Text;
                fornecedores.emailFornecedor = txtEmail.Text;
                fornecedores.contatoFornecedor = txtContato.Text;
                fornecedores.cnpjFornecedor = txtCnpj.Text;
                fornecedores.inscricaoEstadualFornecedor = txtInscEstadual.Text;
                fornecedores.telefoneFornecedor = txtTelefone.Text;
                fornecedores.cepFornecedor = txtCep.Text;
                fornecedores.enderecoFornecedor = txtEndereco.Text;
                fornecedores.complementoFornecedor = txtComplemento.Text;
                fornecedores.bairroFornecedor = txtBairro.Text;
                fornecedores.municipioFornecedor = txtCidade.Text;
                fornecedores.ufFornecedor = cbUf.Text;
                fornecedores.pontoReferenciaFornecedor = txtMaisInformacoes.Text;
                fornecedores.cpfFornecedor = txtCpf.Text;
                fornecedores.dataCadastroFornecedor = DateTime.Now;
                fornecedores.usuarioId = frmLogin.usuariosLogin.usuarioId;

                FornecedoresNegocios fornecedoresNegocios = new FornecedoresNegocios();
                string retorno = fornecedoresNegocios.inserir(fornecedores);

                try
                {
                    int idFornecedores = Convert.ToInt32(retorno);
                    MessageBox.Show("Fornecedores Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Cliente / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBairro.Focus();
                }

                else if (String.IsNullOrEmpty(txtFax.Text))
                {
                    MessageBox.Show("Campo Celular é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtFax.Focus();
                }
            }
        }

        private void atualizarFornecedor()
        {
            if (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtTelefone.Text) &&
               !String.IsNullOrEmpty(txtBairro.Text))
            {

                Fornecedores fornecedores = new Fornecedores();

                fornecedores.idFornecedor = Convert.ToInt32(txtCodigo.Text);
                fornecedores.nomeFornecedor = txtNome.Text;
                fornecedores.faxFornecedor = txtFax.Text;
                fornecedores.emailFornecedor = txtEmail.Text;
                fornecedores.contatoFornecedor = txtContato.Text;
                fornecedores.cnpjFornecedor = txtCnpj.Text;
                fornecedores.inscricaoEstadualFornecedor = txtInscEstadual.Text;
                fornecedores.telefoneFornecedor = txtTelefone.Text;
                fornecedores.cepFornecedor = txtCep.Text;
                fornecedores.enderecoFornecedor = txtEndereco.Text;
                fornecedores.complementoFornecedor = txtComplemento.Text;
                fornecedores.bairroFornecedor = txtBairro.Text;
                fornecedores.municipioFornecedor = txtCidade.Text;
                fornecedores.ufFornecedor = cbUf.Text;
                fornecedores.pontoReferenciaFornecedor = txtMaisInformacoes.Text;
                fornecedores.cpfFornecedor = txtCpf.Text;
                fornecedores.dataCadastroFornecedor = DateTime.Now;
                fornecedores.usuarioId = frmLogin.usuariosLogin.usuarioId;

                FornecedoresNegocios fornecedoresNegocios = new FornecedoresNegocios();
                string retorno = fornecedoresNegocios.Alterar(fornecedores);

                try
                {
                    int idFornecedores = Convert.ToInt32(retorno);
                    MessageBox.Show("Fornecedor Alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Cliente / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBairro.Focus();
                }

                else if (String.IsNullOrEmpty(txtTelefone.Text))
                {
                    MessageBox.Show("Campo Telefone é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTelefone.Focus();
                }
            }
        }

        #endregion

    }
}
