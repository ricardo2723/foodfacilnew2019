﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadEntregador : Form
    {
        AcaoCRUD acaoSelecionada;
        Entregador entregador = new Entregador();
        
        public frmCadEntregador(AcaoCRUD acao, Entregador entregado)
        {
            InitializeComponent();
            
            acaoSelecionada = acao;

            if (acao == AcaoCRUD.Inserir)
            {
                this.Text = "DRSis - Food Facil - Cadastrar Entregador";
                btnSalvar.Text = "  Salvar Novo (F12)";
            }
            else if (acao == AcaoCRUD.Alterar)
            {
                this.Text = "DRSis - Food Facil - Alterar Entregador";
                btnSalvar.Text = "  Salvar Alteração (F12)";

                entregador.idEntregador = entregado.idEntregador;
                txtNome.Text = entregado.nomeEntregador.ToUpper();
                txtApelido.Text = entregado.apelidoEntregador;
                txtCelular.Text = entregado.celularEntregador.ToUpper();
                txtEmail.Text = entregado.emailEntregador.ToUpper();
                txtCpf.Text = entregado.cpfEntregador.ToUpper();
                cbStatus.SelectedIndex = checaStatusRetorno(entregado.statusEntregador);
                txtRg.Text = entregado.identidadeEntregador.ToUpper();
                txtTelefone.Text = entregado.telefoneEntregador.ToUpper();
                txtCep.Text = entregado.cepEntregador.ToUpper();
                txtEndereco.Text = entregado.enderecoEntregador.ToUpper();
                txtComplemento.Text = entregado.complementoEntregador.ToUpper();
                txtBairro.Text = entregado.bairroEntregador.ToUpper();
                txtCidade.Text = entregado.municipioEntregador.ToUpper();
                cbUf.Text = entregado.ufEntregador.ToUpper();
                dtpDataNascimento.Value =  entregado.dataNascimentoEntregador;
                txtPorcentagem.Text = entregado.porcentEntregador.ToString("N");
            }
        }
        
        #region BOTÕES

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntSalvar_Click(object sender, EventArgs e)
        {
            if (acaoSelecionada.Equals(AcaoCRUD.Inserir))
            {
                inserirEntregador();
            }
            else if (acaoSelecionada.Equals(AcaoCRUD.Alterar))
            {
                atualizarEntregador();
            }
        }


        #endregion

        #region AÇÕES

        private void frmCadEntregador_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F12:
                    this.btnSalvar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        #endregion

        #region METODOS

        private void inserirEntregador()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtCelular.Text) &&
               !String.IsNullOrEmpty(txtPorcentagem.Text) &&
               !String.IsNullOrEmpty(cbStatus.Text) &&
               !String.IsNullOrEmpty(txtBairro.Text))
            {
                entregador.nomeEntregador = txtNome.Text;
                entregador.apelidoEntregador = txtApelido.Text;
                entregador.celularEntregador = txtCelular.Text;
                entregador.emailEntregador = txtEmail.Text;
                entregador.cpfEntregador = txtCpf.Text;
                entregador.statusEntregador = checaStatusInsert(cbStatus.SelectedIndex);
                entregador.identidadeEntregador = txtRg.Text;
                entregador.telefoneEntregador = txtTelefone.Text;
                entregador.cepEntregador = txtCep.Text;
                entregador.enderecoEntregador = txtEndereco.Text;
                entregador.complementoEntregador = txtComplemento.Text;
                entregador.bairroEntregador = txtBairro.Text;
                entregador.municipioEntregador = txtCidade.Text;
                entregador.ufEntregador = cbUf.Text;
                entregador.dataNascimentoEntregador = dtpDataNascimento.Value;

                try
                {
                    entregador.porcentEntregador = Convert.ToDecimal(txtPorcentagem.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("É Obrigatório Somentes numeros e separadores", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtPorcentagem.Focus();
                }
                                
                entregador.dataCadastroEntregador = DateTime.Now;
                entregador.usuarioId = frmLogin.usuariosLogin.usuarioId;

                EntregadorNegocios entregadorNegocios = new EntregadorNegocios();
                string retorno = entregadorNegocios.inserir(entregador);

                try
                {
                    int idEntregador = Convert.ToInt32(retorno);
                    MessageBox.Show("Entregador Inserido com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Inserir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Entregador / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBairro.Focus();
                }

                else if (String.IsNullOrEmpty(txtCelular.Text))
                {
                    MessageBox.Show("Campo Celular é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCelular.Focus();
                }

                else if (String.IsNullOrEmpty(cbStatus.Text))
                {
                    MessageBox.Show("Campo Status é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbStatus.Focus();
                }

                else if (String.IsNullOrEmpty(txtCelular.Text))
                {
                    MessageBox.Show("O campo Porcentagem e Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPorcentagem.Focus();
                }
            }
        }

        private void atualizarEntregador()
        {
            if
               (!String.IsNullOrEmpty(txtNome.Text) &&
               !String.IsNullOrEmpty(txtEndereco.Text) &&
               !String.IsNullOrEmpty(txtCelular.Text) &&
               !String.IsNullOrEmpty(cbStatus.Text) &&
               !String.IsNullOrEmpty(txtBairro.Text))
            {

                entregador.nomeEntregador = txtNome.Text;
                entregador.apelidoEntregador = txtApelido.Text;
                entregador.celularEntregador = txtCelular.Text;
                entregador.emailEntregador = txtEmail.Text;
                entregador.cpfEntregador = txtCpf.Text;
                entregador.identidadeEntregador = txtRg.Text;
                entregador.telefoneEntregador = txtTelefone.Text;
                entregador.cepEntregador = txtCep.Text;
                entregador.enderecoEntregador = txtEndereco.Text;
                entregador.complementoEntregador = txtComplemento.Text;
                entregador.bairroEntregador = txtBairro.Text;
                entregador.municipioEntregador = txtCidade.Text;
                entregador.ufEntregador = cbUf.Text;
                entregador.statusEntregador = checaStatusInsert(cbStatus.SelectedIndex);
                entregador.dataNascimentoEntregador = dtpDataNascimento.Value;
                try
                {
                    entregador.porcentEntregador = Convert.ToDecimal(txtPorcentagem.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("É Obrigatório Somentes numeros e separadores", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtPorcentagem.Focus();
                }

                entregador.usuarioId = frmLogin.usuariosLogin.usuarioId;

                EntregadorNegocios entregadorNegocios = new EntregadorNegocios();
                string retorno = entregadorNegocios.Alterar(entregador);

                try
                {
                    int idEntregador = Convert.ToInt32(retorno);
                    MessageBox.Show("Entregador alterado com Sucesso no Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel Alterar. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtNome.Text))
                {
                    MessageBox.Show("Campo Nome do Entregador / Razão Social é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNome.Focus();
                }

                else if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    MessageBox.Show("Campo Endereço é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEndereco.Focus();
                }

                else if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    MessageBox.Show("Campo Bairro é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBairro.Focus();
                }

                else if (String.IsNullOrEmpty(cbStatus.Text))
                {
                    MessageBox.Show("Campo Status é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbStatus.Focus();
                }

                else if (String.IsNullOrEmpty(txtCelular.Text))
                {
                    MessageBox.Show("Campo Celular é Obrigatório", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCelular.Focus();
                }
            }
        }

        private int checaStatusRetorno(bool status)
        {
            int retorno;

            if (status)
            {
                return retorno = 0;
            }
            else
            {
                return retorno = 1;
            }
        }

        private bool checaStatusInsert(int status)
        {
            if (status == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

    }
}
