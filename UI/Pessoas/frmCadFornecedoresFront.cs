﻿using System;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios; 

namespace UI
{
    public partial class frmCadFornecedoresFront : Form
    {
        
        public frmCadFornecedoresFront()
        {
            InitializeComponent();
            //Não gerar Linhas Automaticas
            dgvFornecedor.AutoGenerateColumns = false;
        }

        #region BOTÕES

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmCadFornecedores objFrmCadFornecedores = new frmCadFornecedores(AcaoCRUD.Inserir, null);
            DialogResult resultado = objFrmCadFornecedores.ShowDialog();
            if (resultado == DialogResult.Yes)
            {
                pesquisaFornecedorNome("%");
            }            
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            alterarFornecedores();           
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (rbNome.Checked)
            {
                pesquisaFornecedorNome(txtPesquisa.Text);
            }
            else if (rbCpf.Checked)
            {
                pesquisaFornecedorCpf(txtPesquisa.Text);
            }
            else if (rbLogin.Checked)
            {
                pesquisaFornecedorCnpj(txtPesquisa.Text);
            }

            txtPesquisa.Text = "";
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (dgvFornecedor.RowCount > 0)
            {
                DialogResult excluir = MessageBox.Show("Deseja Realmente Excluir esse Fornecedor? Todos as informações desse Fornecedor serão apagadas do Banco de Dados.", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                if (excluir == DialogResult.No)
                {
                    return;                    
                }

                //Pegar o usuario Selecionado no Grid
                Fornecedores fornecedorSelecionado = dgvFornecedor.SelectedRows[0].DataBoundItem as Fornecedores; 

                //instanciar a regra de negocios
                FornecedoresNegocios fornecedorNegocios = new FornecedoresNegocios();
                string retorno = fornecedorNegocios.Excluir(fornecedorSelecionado);

                //Verificar se a exclusão for com sucesso
                //Se o retorno for o numero 1 deu tudo certo, senão vem a menssagem de erro
                try
                {
                    int idFornecedor = Convert.ToInt32(retorno);
                    MessageBox.Show("Fornecedor Excluido com Sucesso do Banco de Dados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pesquisaFornecedorNome("%" + txtPesquisa.Text + "%");
                }
                catch (Exception)
                {
                    MessageBox.Show("Não foi possivel excluir. Detalhes: " + retorno, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Fornecedor para Excluir", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region METODOS

        private void checaPermissaoUsuario()
        {
            foreach (var item in frmLogin.permissaoAcessoCollectionsLogin)
            {
                if (item.buton == "FORNECEDORES")
                {
                    btnAlterar.Enabled = item.alterar;
                    btnNovo.Enabled = item.cadastrar;
                    btnPesquisar.Enabled = item.pesquisar;
                    btnExcluir.Enabled = item.excluir;
                }
            }
        }

        private void pesquisaFornecedorNome(string nome)
        {

            FornecedoresNegocios fornecedorNegocios = new FornecedoresNegocios();
            FornecedoresCollections fornecedorCollections = fornecedorNegocios.ConsultarPorNome(txtPesquisa.Text);

            dgvFornecedor.DataSource = null;
            dgvFornecedor.DataSource = fornecedorCollections;
            
            dgvFornecedor.Update();
            dgvFornecedor.Refresh();
            
            labelX2.Text = "Fornecedores Listados: " + dgvFornecedor.RowCount;

            Generic.msgPesquisa("Fornecedor não encontrado", dgvFornecedor.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaFornecedorCpf(string cpf)
        {
            FornecedoresNegocios fornecedorNegocios = new FornecedoresNegocios();
            FornecedoresCollections fornecedorCollections = fornecedorNegocios.ConsultarPorCpf(txtPesquisa.Text);

            dgvFornecedor.DataSource = null;
            dgvFornecedor.DataSource = fornecedorCollections;

            dgvFornecedor.Update();
            dgvFornecedor.Refresh();

            labelX2.Text = "Fornecedores Listados: " + dgvFornecedor.RowCount;

            Generic.msgPesquisa("Fornecedor não encontrado", dgvFornecedor.RowCount);

            txtPesquisa.Text = null;
        }

        private void pesquisaFornecedorCnpj(string cnpj)
        {
            FornecedoresNegocios fornecedorNegocios = new FornecedoresNegocios();
            FornecedoresCollections fornecedorCollections = fornecedorNegocios.ConsultarPorCnpj(txtPesquisa.Text);

            dgvFornecedor.DataSource = null;
            dgvFornecedor.DataSource = fornecedorCollections;

            dgvFornecedor.Update();
            dgvFornecedor.Refresh();

            labelX2.Text = "Fornecedores Listados: " + dgvFornecedor.RowCount;

            Generic.msgPesquisa("Fornecedor não encontrado", dgvFornecedor.RowCount);
        }

        private void alterarFornecedores()
        {
            if (dgvFornecedor.RowCount > 0)
            {
                //Pegar o usuario Selecionado no Grid
                Fornecedores fornecedorSelecionado = dgvFornecedor.SelectedRows[0].DataBoundItem as Fornecedores;

                frmCadFornecedores objFrmCadFornecedores = new frmCadFornecedores(AcaoCRUD.Alterar, fornecedorSelecionado);
                DialogResult resultado = objFrmCadFornecedores.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    pesquisaFornecedorNome("%");
                }                
            }
            else
            {
                MessageBox.Show("E necessário Selecionar um Fornecedor para Alterar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region AÇÕES

        private void frmCadFornecedoresFront_Load(object sender, EventArgs e)
        {
            checaPermissaoUsuario();
        }

        private void frmCadFornecedoresFront_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    this.btnNovo.PerformClick();
                    break;
                case Keys.F3:
                    this.btnExcluir.PerformClick();
                    break;
                case Keys.F4:
                    this.btnAlterar.PerformClick();
                    break;
                case Keys.F5:
                    this.btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    this.btnFechar.PerformClick();
                    break;
            }
        }

        private void txtPesquisa_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dgvFornecedores_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnAlterar.PerformClick();
        }

        private void rbNome_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbCpf_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        private void rbLogin_Click(object sender, EventArgs e)
        {
            txtPesquisa.Focus();
            txtPesquisa.Text = "";
        }

        #endregion

        

    }
}
