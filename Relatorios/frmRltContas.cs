﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltContas : Form
    {
        ReportDataSource reportDataSource = new ReportDataSource();
        MeuLanche meuLanche = new MeuLanche();
        relatorioContas tipo;
        bool status;
        DateTime inicial;
        DateTime final;

        public frmRltContas(relatorioContas getTipo, DateTime start, DateTime end, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            inicial = start;
            final = end;
        }

        public frmRltContas(relatorioContas getTipo, DateTime start, DateTime end, MeuLanche meulanche, bool statu)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            status = statu;
            inicial = start;
            final = end;
        }

        private void frmRltContas_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case relatorioContas.Geral:
                    contasGeral();
                    break;
                case relatorioContas.EmAberto:
                    contasEmAberto();
                    break;
                case relatorioContas.Pagas:
                    contasPagas();
                    break;
                case relatorioContas.Vencida:
                    contasVencidas();
                    break;                
            }
        }

        private void contasGeral()
        {
            rptvContas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            p[1] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[2] = new ReportParameter("dataFinal", final.ToShortDateString());
            
            reportDataSource.Name = "ContaGeral";
            reportDataSource.Value = this.ContaGeralBindingSource;
            rptvContas.LocalReport.DataSources.Add(reportDataSource);
            rptvContas.LocalReport.ReportEmbeddedResource = "Relatorios.rltContasGeral.rdlc";
            rptvContas.LocalReport.SetParameters(p);
            this.ContaGeralTableAdapter.Geral(this.dsRelatorios.ContaGeral, inicial, final);
            rptvContas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvContas.RefreshReport();
        }

        private void contasEmAberto()
        {
            rptvContas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            p[1] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[2] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "ContaGeral";
            reportDataSource.Value = this.ContaGeralBindingSource;
            rptvContas.LocalReport.DataSources.Add(reportDataSource);
            rptvContas.LocalReport.ReportEmbeddedResource = "Relatorios.rltContasGeral.rdlc";
            rptvContas.LocalReport.SetParameters(p);
            this.ContaGeralTableAdapter.EmAberto(this.dsRelatorios.ContaGeral, inicial, final);
            rptvContas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvContas.RefreshReport();
        }

        private void contasPagas()
        {
            rptvContas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[4];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            p[1] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[2] = new ReportParameter("dataFinal", final.ToShortDateString());
            p[3] = new ReportParameter("status", "Pagas");

            reportDataSource.Name = "ContaGeral";
            reportDataSource.Value = this.ContaGeralBindingSource;
            rptvContas.LocalReport.DataSources.Add(reportDataSource);
            rptvContas.LocalReport.ReportEmbeddedResource = "Relatorios.rltContasGeral.rdlc";
            rptvContas.LocalReport.SetParameters(p);
            this.ContaGeralTableAdapter.Pagas(this.dsRelatorios.ContaGeral, inicial, final);
            rptvContas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvContas.RefreshReport();
        }

        private void contasVencidas()
        {
            rptvContas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[4];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            p[1] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[2] = new ReportParameter("dataFinal", final.ToShortDateString());
            p[3] = new ReportParameter("status", "Vencidas");

            reportDataSource.Name = "ContaGeral";
            reportDataSource.Value = this.ContaGeralBindingSource;
            rptvContas.LocalReport.DataSources.Add(reportDataSource);
            rptvContas.LocalReport.ReportEmbeddedResource = "Relatorios.rltContasGeral.rdlc";
            rptvContas.LocalReport.SetParameters(p);
            this.ContaGeralTableAdapter.Vencidas(this.dsRelatorios.ContaGeral, inicial, final);
            rptvContas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvContas.RefreshReport();
        }
    }
}
