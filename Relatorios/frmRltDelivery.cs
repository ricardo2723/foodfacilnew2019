﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltDelivery : Form
    {
        MeuLanche meuLanche = new MeuLanche();

        relatorioDelivery tipo;
        DateTime dataInicial, dataFinal;
        string entregador;

        public frmRltDelivery(relatorioDelivery getTipo, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;

            dataInicial = inicial;
            dataFinal = final;
        }

        public frmRltDelivery(relatorioDelivery getTipo, string nomeEntregador, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            entregador = nomeEntregador;
            dataInicial = inicial;
            dataFinal = final;
        }

        private void frmRltDelivery_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case relatorioDelivery.DeliveryGeral:
                    deliveryGeral();
                    break;
                case relatorioDelivery.DeliveryEntregasDiarias:
                    deliveryDiario();
                    break;
                case relatorioDelivery.DeliveryEntregasMensal:
                    deliveryMensal();
                    break;
                case relatorioDelivery.DeliveryEntregador:
                    deliveryEntregador();
                    break;
                case relatorioDelivery.DeliveryEntregadorGeral:
                    deliveryEntregadorGeral();
                    break;
                case relatorioDelivery.DeliveryCancelada:
                    deliveryCancelada();
                    break;
            }
        }

        private void deliveryGeral()
        {
            rptvDelivery.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryGeral.rdlc";

            rptvDelivery.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "Entregas";
            reportDataSource.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource);
            this.DeliveryGeralTableAdapter.EntregasGeral(this.dsRelatorios.DeliveryGeral, dataInicial, dataFinal);
            rptvDelivery.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvDelivery.RefreshReport();
        }

        private void deliveryDiario()
        {
            rptvDelivery.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryDiarias.rdlc";

            rptvDelivery.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "Entregas";
            reportDataSource.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource);
            this.DeliveryGeralTableAdapter.EntregasDiarias(this.dsRelatorios.DeliveryGeral, dataInicial, dataFinal);
            rptvDelivery.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvDelivery.RefreshReport();
        }

        private void deliveryMensal()
        {
            rptvDelivery.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryMensal.rdlc";

            rptvDelivery.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "Entregas";
            reportDataSource.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource);
            this.DeliveryGeralTableAdapter.EntregasMensal(this.dsRelatorios.DeliveryGeral, dataInicial, dataFinal);
            rptvDelivery.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvDelivery.RefreshReport();
        }

        private void deliveryEntregador()
        {
            rptvDelivery.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryEntregador.rdlc";

            rptvDelivery.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "Entregas";
            reportDataSource.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource);
            this.DeliveryGeralTableAdapter.Entregador(this.dsRelatorios.DeliveryGeral, dataInicial, dataFinal, entregador);
            rptvDelivery.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvDelivery.RefreshReport();
        }

        private void deliveryEntregadorGeral()
        {
            rptvDelivery.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryEntregadorGeral.rdlc";

            rptvDelivery.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "Entregas";
            reportDataSource.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource);
            this.DeliveryGeralTableAdapter.EntregadorGeral(this.dsRelatorios.DeliveryGeral, dataInicial, dataFinal);
            rptvDelivery.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvDelivery.RefreshReport();
        }

        private void deliveryCancelada()
        {
            rptvDelivery.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryCancelada.rdlc";

            rptvDelivery.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "Entregas";
            reportDataSource.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource);
            this.DeliveryGeralTableAdapter.EntregaCancelada(this.dsRelatorios.DeliveryGeral, dataInicial, dataFinal);
            rptvDelivery.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvDelivery.RefreshReport();
        }       
    }
}
