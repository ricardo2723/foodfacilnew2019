﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using Microsoft.Reporting.WinForms;
using ObjetoTransferencia;
using Negocios;
using ReportParameter = Microsoft.Reporting.WinForms.ReportParameter;


namespace Relatorios
{
    public partial class frmRltImprimirCupom : Form
    {
        VendaCabecalho vendaCabecalho = new VendaCabecalho();
        MeuLanche meuLanche = new MeuLanche();
        public bool pararcronometro;
        imprimiCupom tipo;
        string nomeCliente;

        public frmRltImprimirCupom(VendaCabecalho vendasCabecalhos)
        {
            InitializeComponent();
            vendaCabecalho = vendasCabecalhos;
        }

        public frmRltImprimirCupom(VendaCabecalho vendaCabecalhos, imprimiCupom getTipo, MeuLanche meuLanche1)
        {
            InitializeComponent();

            vendaCabecalho = vendaCabecalhos;
            tipo = getTipo;
            meuLanche = meuLanche1;
        }

        public frmRltImprimirCupom(VendaCabecalho vendaCabecalhos, imprimiCupom getTipo, string cliente)
        {
            InitializeComponent();

            vendaCabecalho = vendaCabecalhos;
            tipo = getTipo;
            nomeCliente = cliente;
        }

        private void frmRltImprimirCupom_Load(object sender, EventArgs e)
        {
            circularProgress1.IsRunning = true;
            bgwImprimiCupom.RunWorkerAsync();
        }

        private void AutoPrint()
        {
            AutoPrintCls autoPrinte = new AutoPrintCls(rptvImprimirCupom.LocalReport);
            autoPrinte.Print();
        }

        private void AutoPrintDelivery()
        {
            AutoPrintCls autoPrinte = new AutoPrintCls(rptvImprimirCupom.LocalReport);
            autoPrinte.PrinterSettings.Copies = 2;
            autoPrinte.Print();
        }

        public void imprimirPedido()
        {
            ReportParameter[] p = new ReportParameter[4];
            p[0] = new ReportParameter("numeroVenda", vendaCabecalho.idVendaCabecalho.ToString());
            p[1] = new ReportParameter("tipoAtendimento", vendaCabecalho.nomeAtendimento);
            p[2] = new ReportParameter("atendenteVenda", vendaCabecalho.atendenteVendaCabecalho);
            p[3] = new ReportParameter("cliente", nomeCliente);
            
            rptvImprimirCupom.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirPedido.rdlc";

            rptvImprimirCupom.LocalReport.SetParameters(p);

            GruposNegocios gruposNegocios = new GruposNegocios();
            GruposCollections gruposCollections = gruposNegocios.ConsultarDescricao("%");

            foreach (var item in gruposCollections)
            {
                ReportDataSource reportDataSource = new ReportDataSource();

                reportDataSource.Name = "ImprimirPedidoPapel";
                reportDataSource.Value = this.ImprimirPedidoPapelBindingSource;
                this.rptvImprimirCupom.LocalReport.DataSources.Add(reportDataSource);
                this.imprimirPedidoPapelTableAdapter.ImprimirPedidoPapel(this.dsRelatorios.ImprimirPedidoPapel, vendaCabecalho.idVendaCabecalho, item.grupoId);

                if (this.dsRelatorios.ImprimirPedidoPapel.Count > 0 && item.grupoId != 5)
                {
                    AutoPrint();    
                }
                
            }

            
        }

        private void imprimirCupom()
        {
            ReportParameter[] p = new ReportParameter[14];
            p[0] = new ReportParameter("numeroVenda", vendaCabecalho.idVendaCabecalho.ToString());
            p[1] = new ReportParameter("tipoAtendimento", vendaCabecalho.nomeAtendimento);
            p[2] = new ReportParameter("atendenteVenda", vendaCabecalho.atendenteVendaCabecalho);
            p[3] = new ReportParameter("troco", vendaCabecalho.troco);
            p[4] = new ReportParameter("dinheiro", vendaCabecalho.dinheiro);
            p[5] = new ReportParameter("cartao", vendaCabecalho.cartao);
            p[6] = new ReportParameter("desconto", vendaCabecalho.descontoVendaCabecalho.ToString());
            p[7] = new ReportParameter("ticket", vendaCabecalho.ticket);
            p[8] = new ReportParameter("nomeEmpresa", meuLanche.nomeMeuLanche);
            p[9] = new ReportParameter("endereco", meuLanche.enderecoMeuLanche + " " + meuLanche.complementoMeuLanche + " " + meuLanche.bairroMeuLanche);
            p[10] = new ReportParameter("cidade", meuLanche.municipioMeuLanche);
            p[11] = new ReportParameter("uf", meuLanche.ufMeuLanche);
            p[12] = new ReportParameter("cnpj", meuLanche.cnpjMeuLanche);
            p[13] = new ReportParameter("ie", meuLanche.inscricaoEstadualMeuLanche);

            rptvImprimirCupom.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirCupom.rdlc";

            rptvImprimirCupom.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ImprimirPedido";
            reportDataSource.Value = this.ImprimirCupomBindingSource;
            this.rptvImprimirCupom.LocalReport.DataSources.Add(reportDataSource);
            this.imprimirPedidosTableAdapter.ImprimirPedido(this.dsRelatorios.ImprimirPedidos, vendaCabecalho.idVendaCabecalho);
            
            AutoPrint();
        }

        private void reimprimirCupom()
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            VendaCabecalhoCollections vendaCabecalhoCollections = vendaCabecalhoNegocios.getTipoPagamento(vendaCabecalho.idVendaCabecalho);

            foreach (var item in vendaCabecalhoCollections)
            {
                if (item.descricaoVCTipoPagamento == "DINHEIRO")
                {
                    vendaCabecalho.dinheiro = item.valorVCTipoPagamento.ToString();   
                }
                if (item.descricaoVCTipoPagamento == "CARTÃO")
                {
                    vendaCabecalho.cartao = item.valorVCTipoPagamento.ToString();
                }
            }

            ReportParameter[] p = new ReportParameter[12];
            p[0] = new ReportParameter("numeroVenda", vendaCabecalho.idVendaCabecalho.ToString());
            p[1] = new ReportParameter("tipoAtendimento", vendaCabecalho.nomeAtendimento);
            p[2] = new ReportParameter("atendenteVenda", vendaCabecalho.atendenteVendaCabecalho);
            p[3] = new ReportParameter("dinheiro", vendaCabecalho.dinheiro);
            p[4] = new ReportParameter("cartao", vendaCabecalho.cartao);
            p[5] = new ReportParameter("desconto", vendaCabecalho.descontoVendaCabecalho.ToString());
            p[6] = new ReportParameter("nomeEmpresa", meuLanche.nomeMeuLanche);
            p[7] = new ReportParameter("endereco", meuLanche.enderecoMeuLanche + " " + meuLanche.complementoMeuLanche + " " + meuLanche.bairroMeuLanche);
            p[8] = new ReportParameter("cidade", meuLanche.municipioMeuLanche);
            p[9] = new ReportParameter("uf", meuLanche.ufMeuLanche);
            p[10] = new ReportParameter("cnpj", meuLanche.cnpjMeuLanche);
            p[11] = new ReportParameter("ie", meuLanche.inscricaoEstadualMeuLanche);
            rptvImprimirCupom.LocalReport.ReportEmbeddedResource = "Relatorios.rltReimprimirCupom.rdlc";

            rptvImprimirCupom.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ImprimirPedido";
            reportDataSource.Value = this.ImprimirCupomBindingSource;
            this.rptvImprimirCupom.LocalReport.DataSources.Add(reportDataSource);
            this.imprimirPedidosTableAdapter.ImprimirPedido(this.dsRelatorios.ImprimirPedidos, vendaCabecalho.idVendaCabecalho);

            AutoPrint();
        }

        private void imprimirCupomDelivery()
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            VendaCabecalhoCollections vendaCabecalhoCollections = vendaCabecalhoNegocios.getTipoPagamento(vendaCabecalho.idVendaCabecalho);

            foreach (var item in vendaCabecalhoCollections)
            {
                if (item.descricaoVCTipoPagamento == "DINHEIRO")
                {
                    vendaCabecalho.dinheiro = item.valorVCTipoPagamento.ToString();
                }
                if (item.descricaoVCTipoPagamento == "CARTÃO")
                {
                    vendaCabecalho.cartao = item.valorVCTipoPagamento.ToString();
                }
                if (item.descricaoVCTipoPagamento == "TICKET")
                {
                    vendaCabecalho.ticket = item.valorVCTipoPagamento.ToString();
                }
            }

            ReportParameter[] p = new ReportParameter[11];
            p[0] = new ReportParameter("numeroVenda", vendaCabecalho.idVendaCabecalho.ToString());
            p[1] = new ReportParameter("atendenteVenda", vendaCabecalho.atendenteVendaCabecalho);
            p[2] = new ReportParameter("dinheiro", vendaCabecalho.dinheiro);
            p[3] = new ReportParameter("cartao", vendaCabecalho.cartao);
            p[4] = new ReportParameter("ticket", vendaCabecalho.ticket);
            p[5] = new ReportParameter("nomeEmpresa", meuLanche.nomeMeuLanche);
            p[6] = new ReportParameter("endereco", meuLanche.enderecoMeuLanche + " " + meuLanche.complementoMeuLanche + " " + meuLanche.bairroMeuLanche);
            p[7] = new ReportParameter("cidade", meuLanche.municipioMeuLanche);
            p[8] = new ReportParameter("uf", meuLanche.ufMeuLanche);
            p[9] = new ReportParameter("cnpj", meuLanche.cnpjMeuLanche);
            p[10] = new ReportParameter("ie", meuLanche.inscricaoEstadualMeuLanche);

            rptvImprimirCupom.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirCupomDelivery.rdlc";

            rptvImprimirCupom.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ReimprimeDelivery";
            reportDataSource.Value = this.ImprimirDeliveryBindingSource;
            this.rptvImprimirCupom.LocalReport.DataSources.Add(reportDataSource);
            this.reimprimeCupomDeliveryTableAdapter.DetalhesPedidos(this.dsRelatorios.ReimprimeCupomDelivery, vendaCabecalho.idVendaCabecalho);

            AutoPrintDelivery();
        }

        private void reimprimirCupomDelivery()
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            VendaCabecalhoCollections vendaCabecalhoCollections = vendaCabecalhoNegocios.getTipoPagamento(vendaCabecalho.idVendaCabecalho);

            foreach (var item in vendaCabecalhoCollections)
            {
                if (item.descricaoVCTipoPagamento == "DINHEIRO")
                {
                    vendaCabecalho.dinheiro = item.valorVCTipoPagamento.ToString();
                }
                if (item.descricaoVCTipoPagamento == "CARTÃO")
                {
                    vendaCabecalho.cartao = item.valorVCTipoPagamento.ToString();
                }
                if (item.descricaoVCTipoPagamento == "TICKET")
                {
                    vendaCabecalho.ticket = item.valorVCTipoPagamento.ToString();
                }
            }

            ReportParameter[] p = new ReportParameter[11];
            p[0] = new ReportParameter("numeroVenda", vendaCabecalho.idVendaCabecalho.ToString());
            p[1] = new ReportParameter("atendenteVenda", vendaCabecalho.atendenteVendaCabecalho);
            p[2] = new ReportParameter("dinheiro", vendaCabecalho.dinheiro);
            p[3] = new ReportParameter("cartao", vendaCabecalho.cartao);
            p[4] = new ReportParameter("ticket", vendaCabecalho.ticket);
            p[5] = new ReportParameter("nomeEmpresa", meuLanche.nomeMeuLanche);
            p[6] = new ReportParameter("endereco", meuLanche.enderecoMeuLanche + " " + meuLanche.complementoMeuLanche + " " + meuLanche.bairroMeuLanche);
            p[7] = new ReportParameter("cidade", meuLanche.municipioMeuLanche);
            p[8] = new ReportParameter("uf", meuLanche.ufMeuLanche);
            p[9] = new ReportParameter("cnpj", meuLanche.cnpjMeuLanche);
            p[10] = new ReportParameter("ie", meuLanche.inscricaoEstadualMeuLanche);

            rptvImprimirCupom.LocalReport.ReportEmbeddedResource = "Relatorios.rltReimprimirCupomDelivery.rdlc";

            rptvImprimirCupom.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ReimprimeDelivery";
            reportDataSource.Value = this.ImprimirDeliveryBindingSource;
            this.rptvImprimirCupom.LocalReport.DataSources.Add(reportDataSource);
            this.reimprimeCupomDeliveryTableAdapter.DetalhesPedidos(this.dsRelatorios.ReimprimeCupomDelivery, vendaCabecalho.idVendaCabecalho);
            AutoPrintDelivery();
        }

        private void checaRelatrio()
        {
            if (tipo == imprimiCupom.imprime)
            {
                imprimirCupom();
            }
            else if (tipo == imprimiCupom.reimprime)
            {
                reimprimirCupom();
            }
            else if (tipo == imprimiCupom.reimprimeDelivery)
            {
                reimprimirCupomDelivery();
            }
            else if (tipo == imprimiCupom.imprimeDelivery)
            {
                imprimirCupomDelivery();
            }
            else if (tipo == imprimiCupom.imprimePedido)
            {
                imprimirPedido();
            }
        }

        private void bgwImprimiCupom_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!bgwImprimiCupom.CancellationPending)
            {
                bgwImprimiCupom.ReportProgress(0, "Imprimindo Cupom....");
                Thread.Sleep(500);
                checaRelatrio();
                bgwImprimiCupom.CancelAsync();
            }
        }

        private void bgwImprimiCupom_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMenssagem.Text = e.UserState.ToString();
        }

        private void bgwImprimiCupom_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblMenssagem.Text = "Impressão Finalizada";
            Close();
        }

        private void frmRltImprimirCupom_FormClosed(object sender, FormClosedEventArgs e)
        {
            bgwImprimiCupom.CancelAsync();
        }
    }
}
