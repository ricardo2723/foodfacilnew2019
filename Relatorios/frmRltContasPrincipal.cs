﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace Relatorios
{
    public partial class frmRltContasPrincipal : Form
    {
        MeuLanche meuLanche = new MeuLanche();

        public frmRltContasPrincipal(MeuLanche meulanche)
        {
            InitializeComponent();
            meuLanche = meulanche;
        }

        private void rbGeral_Click(object sender, EventArgs e)
        {
            dtpEmAbertoInicial.Enabled = false;
            dtpEmAbertoFinal.Enabled = false;
            dtpPagaInicial.Enabled = false;
            dtpPagaFinal.Enabled = false;
            dtpVencimento.Enabled = false;
            
            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void rbEmAberto_Click(object sender, EventArgs e)
        {
            dtpPagaInicial.Enabled = false;
            dtpPagaFinal.Enabled = false;
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
            dtpVencimento.Enabled = false;
            
            dtpEmAbertoInicial.Enabled = true;
            dtpEmAbertoFinal.Enabled = true;
        }

        private void rbPagas_Click(object sender, EventArgs e)
        {
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
            dtpEmAbertoInicial.Enabled = false;
            dtpEmAbertoFinal.Enabled = false;
            dtpVencimento.Enabled = false;
            
            dtpPagaInicial.Enabled = true;
            dtpPagaFinal.Enabled = true;
        }

        private void rbVencimento_Click(object sender, EventArgs e)
        {
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
            dtpEmAbertoInicial.Enabled = false;
            dtpEmAbertoFinal.Enabled = false;
            dtpPagaInicial.Enabled = false;
            dtpPagaFinal.Enabled = false;

            dtpVencimento.Enabled = true;
        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (rbGeral.Checked)
            {
                frmRltContas objContas = new frmRltContas(relatorioContas.Geral, dtpGeralInicial.Value, dtpGeralFinal.Value, meuLanche);
                objContas.ShowDialog();
            }
            else if (rbEmAberto.Checked)
            {
                frmRltContas objContas = new frmRltContas(relatorioContas.EmAberto, dtpEmAbertoInicial.Value, dtpEmAbertoFinal.Value, meuLanche, false);
                objContas.ShowDialog();
            }
            else if (rbPagas.Checked)
            {
                frmRltContas objContas = new frmRltContas(relatorioContas.Pagas, dtpPagaInicial.Value, dtpPagaFinal.Value, meuLanche, true);
                objContas.ShowDialog();
            }
            else if (rbVencimento.Checked)
            {
                frmRltContas objContas = new frmRltContas(relatorioContas.Vencida, dtpVencimento.Value, dtpVencimento.Value, meuLanche);
                objContas.ShowDialog();
            }
        }
        // APESAR DE MOSTRAR APENAS UM COMPONENTE TODOS OS DTP ESTAO COM COM KEYDOWN ATIVADO
        private void dtpGeralInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }        
    }
}
