﻿namespace Relatorios
{
    partial class frmRltImprimirCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltImprimirCaixa));
            this.rptvImprimirCaixa = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rptvImprimirCaixa
            // 
            this.rptvImprimirCaixa.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Value = null;
            this.rptvImprimirCaixa.LocalReport.DataSources.Add(reportDataSource1);
            this.rptvImprimirCaixa.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirCaixa.rdlc";
            this.rptvImprimirCaixa.Location = new System.Drawing.Point(0, 0);
            this.rptvImprimirCaixa.Name = "rptvImprimirCaixa";
            this.rptvImprimirCaixa.Size = new System.Drawing.Size(1350, 683);
            this.rptvImprimirCaixa.TabIndex = 0;
            // 
            // frmRltImprimirCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 683);
            this.Controls.Add(this.rptvImprimirCaixa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltImprimirCaixa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Imprimir Demostrativo do Caixa";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltImprimirCaixa_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvImprimirVenda;
        private Microsoft.Reporting.WinForms.ReportViewer rptvImprimirCaixa;
        


    }
}