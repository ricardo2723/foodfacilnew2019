﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltProdutosEntradaSaida : Form
    {
        MeuLanche meuLanche = new MeuLanche();
        int tipoRelatorio;

        public frmRltProdutosEntradaSaida(int tipo, MeuLanche meulanche)
        {
            InitializeComponent();

            meuLanche = meulanche;

            tipoRelatorio = tipo;

            if (tipo == 1)
            {
                gpEntradaSaida.Text = "Relatório de Entrada de Produtos";
            }
            else if (tipo == 2)
            {
                gpEntradaSaida.Text = "Relatório de Saida de Produtos";
            }
        }


        private void rbGeral_Click(object sender, EventArgs e)
        {
            txtCodigoProduto.Enabled = false;
            dtpCodigoInicial.Enabled = false;
            dtpCodigoFinal.Enabled = false;

            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void rbProduto_Click(object sender, EventArgs e)
        {
            txtCodigoProduto.Enabled = true;
            txtCodigoProduto.Focus();
            dtpCodigoInicial.Enabled = true;
            dtpCodigoFinal.Enabled = true;

            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
        }

        private void frmRltProdutosEntradas_Load(object sender, EventArgs e)
        {
            txtCodigoProduto.Enabled = false;
            dtpCodigoInicial.Enabled = false;
            dtpCodigoFinal.Enabled = false;

            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            switch (tipoRelatorio)
            {
                case 1:
                    gpEntradaSaida.Text = "Relatório de Entrada de Produtos";
                    if (rbGeral.Checked)
                    {
                        frmRltProdutos objProdutos = new frmRltProdutos(relatorioProdutos.EntradaGeral, dtpGeralInicial.Value, dtpGeralFinal.Value, meuLanche);
                        objProdutos.ShowDialog();
                    }
                    else if (rbProduto.Checked)
                    {
                        try
                        {
                            int codigo = Convert.ToInt32(txtCodigoProduto.Text);

                            frmRltProdutos objProdutos = new frmRltProdutos(relatorioProdutos.EntradaCodigo, codigo, dtpCodigoInicial.Value, dtpCodigoFinal.Value, meuLanche);
                            objProdutos.ShowDialog();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("É obrigatorio somente numeros no campo Código.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    break;

                case 2:

                    if (rbGeral.Checked)
                    {
                        frmRltProdutos objProdutos = new frmRltProdutos(relatorioProdutos.SaidaGeral, dtpGeralInicial.Value, dtpGeralFinal.Value, meuLanche);
                        objProdutos.ShowDialog();
                    }
                    else if (rbProduto.Checked)
                    {
                        try
                        {
                            int codigo = Convert.ToInt32(txtCodigoProduto.Text);

                            frmRltProdutos objProdutos = new frmRltProdutos(relatorioProdutos.SaidaCodigo, codigo, dtpCodigoInicial.Value, dtpCodigoFinal.Value, meuLanche);
                            objProdutos.ShowDialog();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("É obrigatorio somente numeros no campo Código.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    break;
            }
        }

        private void dtpGeralInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }
    }
}
