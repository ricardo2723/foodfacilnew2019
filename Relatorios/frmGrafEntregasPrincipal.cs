﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace Relatorios
{
    public partial class frmGrafEntregasPrincipal : Form
    {
       GraficosEntregas tipo;

        public frmGrafEntregasPrincipal(GraficosEntregas getTipo)
        {
            InitializeComponent();
            tipo = getTipo;
        }        

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (tipo == GraficosEntregas.Diaria)
            {
                frmGrafEntregas objGrafVendas = new frmGrafEntregas(GraficosEntregas.Diaria, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }
            else if (tipo == GraficosEntregas.Mensal)
            {
                frmGrafEntregas objGrafVendas = new frmGrafEntregas(GraficosEntregas.Mensal, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }

            else if (tipo == GraficosEntregas.Entregador)
            {
                frmGrafEntregas objGrafVendas = new frmGrafEntregas(GraficosEntregas.Entregador, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }

            else if (tipo == GraficosEntregas.Tempo)
            {
                frmGrafEntregas objGrafVendas = new frmGrafEntregas(GraficosEntregas.Tempo, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }
            
            else if (tipo == GraficosEntregas.Canceladas)
            {
                frmGrafEntregas objGrafVendas = new frmGrafEntregas(GraficosEntregas.Canceladas, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }
        }

        private void dtpInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }

        private void dtpFinal_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }

             
    }
}
