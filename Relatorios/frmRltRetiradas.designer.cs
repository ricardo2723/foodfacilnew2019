﻿namespace Relatorios
{
    partial class frmRltRetiradas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltRetiradas));
            this.rptvRetiradas = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.RetiradasGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.retiradaGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.RetiradaGeralTableAdapter();
            this.RetiradasUsuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.retiradaUsuarioTableAdapter = new Relatorios.dsRelatoriosTableAdapters.RetiradaUsuarioTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetiradasGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetiradasUsuarioBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rptvRetiradas
            // 
            this.rptvRetiradas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptvRetiradas.Location = new System.Drawing.Point(0, 0);
            this.rptvRetiradas.Name = "rptvRetiradas";
            this.rptvRetiradas.Size = new System.Drawing.Size(1044, 451);
            this.rptvRetiradas.TabIndex = 0;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RetiradasGeralBindingSource
            // 
            this.RetiradasGeralBindingSource.DataMember = "RetiradaGeral";
            this.RetiradasGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // retiradaGeralTableAdapter
            // 
            this.retiradaGeralTableAdapter.ClearBeforeFill = true;
            // 
            // RetiradasUsuarioBindingSource
            // 
            this.RetiradasUsuarioBindingSource.DataMember = "RetiradaUsuario";
            this.RetiradasUsuarioBindingSource.DataSource = this.dsRelatorios;
            // 
            // retiradaUsuarioTableAdapter
            // 
            this.retiradaUsuarioTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltRetiradas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 451);
            this.Controls.Add(this.rptvRetiradas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltRetiradas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Retiradas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltRetiradas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetiradasGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetiradasUsuarioBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvRetiradas;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource RetiradasGeralBindingSource;
        private dsRelatoriosTableAdapters.RetiradaGeralTableAdapter retiradaGeralTableAdapter;
        private System.Windows.Forms.BindingSource RetiradasUsuarioBindingSource;
        private dsRelatoriosTableAdapters.RetiradaUsuarioTableAdapter retiradaUsuarioTableAdapter;
    }
}