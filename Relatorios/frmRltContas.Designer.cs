﻿namespace Relatorios
{
    partial class frmRltContas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltContas));
            this.rptvContas = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.ContaGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ContaGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ContaGeralTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContaGeralBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rptvContas
            // 
            this.rptvContas.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Clientes";
            reportDataSource1.Value = null;
            this.rptvContas.LocalReport.DataSources.Add(reportDataSource1);
            this.rptvContas.LocalReport.ReportEmbeddedResource = "Relatorios.rltClientesGeral.rdlc";
            this.rptvContas.Location = new System.Drawing.Point(0, 0);
            this.rptvContas.Name = "rptvContas";
            this.rptvContas.Size = new System.Drawing.Size(984, 473);
            this.rptvContas.TabIndex = 0;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ContaGeralBindingSource
            // 
            this.ContaGeralBindingSource.DataMember = "ContaGeral";
            this.ContaGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // ContaGeralTableAdapter
            // 
            this.ContaGeralTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltContas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 473);
            this.Controls.Add(this.rptvContas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltContas";
            this.Text = "DRSis - Lanche Fácil - Relatório de Contas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltContas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContaGeralBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvContas;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource ContaGeralBindingSource;
        private dsRelatoriosTableAdapters.ContaGeralTableAdapter ContaGeralTableAdapter;            
    }
}