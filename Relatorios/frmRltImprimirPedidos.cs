﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;
using Negocios;

namespace Relatorios
{
    public partial class frmRltImprimirPedidos : Form
    {
        public int numeroVenda;

        public frmRltImprimirPedidos(VendaDetalhes vendaDetalhes, MeuLanche meuLanche)
        {
            InitializeComponent();
            
            numeroVenda = vendaDetalhes.idVendaCabecalhoDetalhes;

            ReportParameter[] p = new ReportParameter[8];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("numeroVenda", vendaDetalhes.idVendaCabecalhoDetalhes.ToString());
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("tipoAtendimento", vendaDetalhes.tipoAtendimento);
            p[2] = new ReportParameter("nomeEmpresa", meuLanche.nomeMeuLanche);
            p[3] = new ReportParameter("endereco", meuLanche.enderecoMeuLanche + " " + meuLanche.complementoMeuLanche + " " + meuLanche.bairroMeuLanche);
            p[4] = new ReportParameter("cidade", meuLanche.municipioMeuLanche);
            p[5] = new ReportParameter("uf", meuLanche.ufMeuLanche);
            p[6] = new ReportParameter("cnpj", meuLanche.cnpjMeuLanche);
            p[7] = new ReportParameter("ie", meuLanche.inscricaoEstadualMeuLanche);

            rptvImprimirPedido.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirPedidos.rdlc";
            rptvImprimirPedido.LocalReport.SetParameters(p);            
        }

        private void frmRltImprimirPedidos_Load(object sender, EventArgs e)
        {
            this.ImprimirPedidosTableAdapter.ImprimirPedido(this.dsRelatorios.ImprimirPedidos, numeroVenda);
            
            rptvImprimirPedido.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvImprimirPedido.RefreshReport();
        }         
    }
}
