﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace Relatorios
{
    public partial class frmGrafVendasPrincipal : Form
    {
        GraficosVendas tipo;

        public frmGrafVendasPrincipal(GraficosVendas getTipo)
        {
            InitializeComponent();
            tipo = getTipo;
        }        

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (tipo == GraficosVendas.Diaria)
            {
                frmGrafVendas objGrafVendas = new frmGrafVendas(GraficosVendas.Diaria, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }
            else if (tipo == GraficosVendas.Mensal)
            {
                frmGrafVendas objGrafVendas = new frmGrafVendas(GraficosVendas.Mensal, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }
            else if (tipo == GraficosVendas.InLocDelivery)
            {
                frmGrafVendas objGrafVendas = new frmGrafVendas(GraficosVendas.InLocDelivery, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }

            else if (tipo == GraficosVendas.Canceladas)
            {
                frmGrafVendas objGrafVendas = new frmGrafVendas(GraficosVendas.Canceladas, dtpInicial.Value, dtpFinal.Value);
                objGrafVendas.ShowDialog();
            }
        }

        private void dtpInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }

        private void dtpFinal_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }        
    }
}
