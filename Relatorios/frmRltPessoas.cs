﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltPessoas : Form
    {
        ReportDataSource reportDataSource = new ReportDataSource();
        MeuLanche meuLanche = new MeuLanche();
        relatorioPessoas tipo;
        int status;

        public frmRltPessoas(relatorioPessoas getTipo, MeuLanche meulanche, int statu)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            status = statu;
        }

        public frmRltPessoas(relatorioPessoas getTipo, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
        }

        private void frmRltClientes_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case relatorioPessoas.Clientes:
                    Clientes();
                    break;
                case relatorioPessoas.Usuarios:
                    Usuarios();
                    break;
                case relatorioPessoas.Fornecedores:
                    Fornecedores();
                    break;
                case relatorioPessoas.EntregadoresGeral:
                    Entregadores();
                    break;
                case relatorioPessoas.EntregadoresAtivos:
                    EntregadoresAtivos();
                    break;
                case relatorioPessoas.EntregadoresDesligados:
                    EntregadoresDesligados();
                    break;
            }
        }

        private void Clientes()
        {
            rptvPessoas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);

            rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltClientesGeral.rdlc";
            rptvPessoas.LocalReport.SetParameters(p);

            this.ClientesTableAdapter.Clientes(this.dsRelatorios.Clientes);
            rptvPessoas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvPessoas.RefreshReport();
        }

        private void Usuarios()
        {
            rptvPessoas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            
            reportDataSource.Name = "UsuariosGeral";
            reportDataSource.Value = this.UsuariosBindingSource;
            rptvPessoas.LocalReport.DataSources.Add(reportDataSource);
            rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltUsuariosGeral.rdlc";
            rptvPessoas.LocalReport.SetParameters(p);
            UsuariosTableAdapter.UsuariosGeral(this.dsRelatorios.Usuarios);
            rptvPessoas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvPessoas.RefreshReport();
        }

        private void Fornecedores()
        {
            rptvPessoas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);

            reportDataSource.Name = "FornecedoresGeral";
            reportDataSource.Value = this.FornecedoresBindingSource;
            rptvPessoas.LocalReport.DataSources.Add(reportDataSource);
            rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltFornecedoresGeral.rdlc";
            rptvPessoas.LocalReport.SetParameters(p);
            FornecedoresTableAdapter.Fornecedores(this.dsRelatorios.Fornecedores);
            rptvPessoas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvPessoas.RefreshReport();
        }

        private void Entregadores()
        {
            rptvPessoas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);

            reportDataSource.Name = "EntregadoresGeral";
            reportDataSource.Value = this.EntregadoresGeralBindingSource;
            rptvPessoas.LocalReport.DataSources.Add(reportDataSource);
            rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltEntregadoresGeral.rdlc";
            rptvPessoas.LocalReport.SetParameters(p);
            EntregadoresGeralTableAdapter.EntregadoresGeral(this.dsRelatorios.Entregadores);
            rptvPessoas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvPessoas.RefreshReport();
        }

        private void EntregadoresAtivos()
        {
            rptvPessoas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);

            reportDataSource.Name = "EntregadoresStatus";
            reportDataSource.Value = this.EntregadoresGeralBindingSource;
            rptvPessoas.LocalReport.DataSources.Add(reportDataSource);
            rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltEntregadoresStatus.rdlc";
            rptvPessoas.LocalReport.SetParameters(p);
            EntregadoresGeralTableAdapter.EntregadoresStatus(this.dsRelatorios.Entregadores, status);
            rptvPessoas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvPessoas.RefreshReport();
        }

        private void EntregadoresDesligados()
        {
            rptvPessoas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);

            reportDataSource.Name = "EntregadoresStatus";
            reportDataSource.Value = this.EntregadoresStatusBindingSource;
            rptvPessoas.LocalReport.DataSources.Add(reportDataSource);
            rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltEntregadoresStatus.rdlc";
            rptvPessoas.LocalReport.SetParameters(p);
            EntregadoresGeralTableAdapter.EntregadoresStatus(this.dsRelatorios.Entregadores, status);
            rptvPessoas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvPessoas.RefreshReport();
        }
    }
}
