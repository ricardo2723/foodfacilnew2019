﻿namespace Relatorios
{
    partial class frmRltPessoas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltPessoas));
            this.ClientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.rptvPessoas = new Microsoft.Reporting.WinForms.ReportViewer();
            this.FornecedoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ClientesTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ClientesTableAdapter();
            this.UsuariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.UsuariosTableAdapter = new Relatorios.dsRelatoriosTableAdapters.UsuariosTableAdapter();
            this.FornecedoresTableAdapter = new Relatorios.dsRelatoriosTableAdapters.FornecedoresTableAdapter();
            this.EntregadoresGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EntregadoresGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.EntregadoresTableAdapter();
            this.EntregadoresStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ClientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FornecedoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntregadoresGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntregadoresStatusBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ClientesBindingSource
            // 
            this.ClientesBindingSource.DataMember = "Clientes";
            this.ClientesBindingSource.DataSource = this.dsRelatorios;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rptvPessoas
            // 
            this.rptvPessoas.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Clientes";
            reportDataSource1.Value = this.ClientesBindingSource;
            this.rptvPessoas.LocalReport.DataSources.Add(reportDataSource1);
            this.rptvPessoas.LocalReport.ReportEmbeddedResource = "Relatorios.rltClientesGeral.rdlc";
            this.rptvPessoas.Location = new System.Drawing.Point(0, 0);
            this.rptvPessoas.Name = "rptvPessoas";
            this.rptvPessoas.Size = new System.Drawing.Size(984, 473);
            this.rptvPessoas.TabIndex = 0;
            // 
            // FornecedoresBindingSource
            // 
            this.FornecedoresBindingSource.DataMember = "Fornecedores";
            this.FornecedoresBindingSource.DataSource = this.dsRelatorios;
            // 
            // ClientesTableAdapter
            // 
            this.ClientesTableAdapter.ClearBeforeFill = true;
            // 
            // UsuariosBindingSource
            // 
            this.UsuariosBindingSource.DataMember = "Usuarios";
            this.UsuariosBindingSource.DataSource = this.dsRelatorios;
            // 
            // UsuariosTableAdapter
            // 
            this.UsuariosTableAdapter.ClearBeforeFill = true;
            // 
            // FornecedoresTableAdapter
            // 
            this.FornecedoresTableAdapter.ClearBeforeFill = true;
            // 
            // EntregadoresGeralBindingSource
            // 
            this.EntregadoresGeralBindingSource.DataMember = "Entregadores";
            this.EntregadoresGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // EntregadoresGeralTableAdapter
            // 
            this.EntregadoresGeralTableAdapter.ClearBeforeFill = true;
            // 
            // EntregadoresStatusBindingSource
            // 
            this.EntregadoresStatusBindingSource.DataMember = "Entregadores";
            this.EntregadoresStatusBindingSource.DataSource = this.dsRelatorios;
            // 
            // frmRltPessoas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 473);
            this.Controls.Add(this.rptvPessoas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltPessoas";
            this.Text = "DRSis - Lanche Fácil - Relatório de Pessoas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltClientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ClientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FornecedoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsuariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntregadoresGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntregadoresStatusBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvPessoas;
        private System.Windows.Forms.BindingSource ClientesBindingSource;
        private dsRelatorios dsRelatorios;
        private dsRelatoriosTableAdapters.ClientesTableAdapter ClientesTableAdapter;
        private System.Windows.Forms.BindingSource UsuariosBindingSource;
        private dsRelatoriosTableAdapters.UsuariosTableAdapter UsuariosTableAdapter;
        private System.Windows.Forms.BindingSource FornecedoresBindingSource;
        private dsRelatoriosTableAdapters.FornecedoresTableAdapter FornecedoresTableAdapter;
        private System.Windows.Forms.BindingSource EntregadoresGeralBindingSource;
        private dsRelatoriosTableAdapters.EntregadoresTableAdapter EntregadoresGeralTableAdapter;
        private System.Windows.Forms.BindingSource EntregadoresStatusBindingSource;
    }
}