﻿namespace Relatorios
{
    partial class frmRltImprimirPedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltImprimirPedidos));
            this.ImprimirPedidosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.rptvImprimirPedido = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ImprimirPedidosTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ImprimirPedidosTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirPedidosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            this.SuspendLayout();
            // 
            // ImprimirPedidosBindingSource
            // 
            this.ImprimirPedidosBindingSource.DataMember = "ImprimirPedidos";
            this.ImprimirPedidosBindingSource.DataSource = this.dsRelatorios;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rptvImprimirPedido
            // 
            this.rptvImprimirPedido.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "ImprimirPedido";
            reportDataSource1.Value = this.ImprimirPedidosBindingSource;
            this.rptvImprimirPedido.LocalReport.DataSources.Add(reportDataSource1);
            this.rptvImprimirPedido.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirPedidos.rdlc";
            this.rptvImprimirPedido.Location = new System.Drawing.Point(0, 0);
            this.rptvImprimirPedido.Name = "rptvImprimirPedido";
            this.rptvImprimirPedido.Size = new System.Drawing.Size(1350, 683);
            this.rptvImprimirPedido.TabIndex = 0;
            // 
            // ImprimirPedidosTableAdapter
            // 
            this.ImprimirPedidosTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltImprimirPedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 683);
            this.Controls.Add(this.rptvImprimirPedido);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltImprimirPedidos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Imprimir Pedidos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltImprimirPedidos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirPedidosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvImprimirVenda;
        private Microsoft.Reporting.WinForms.ReportViewer rptvImprimirPedido;
        private System.Windows.Forms.BindingSource ImprimirPedidosBindingSource;
        private dsRelatorios dsRelatorios;
        private dsRelatoriosTableAdapters.ImprimirPedidosTableAdapter ImprimirPedidosTableAdapter;
        


    }
}