﻿namespace Relatorios
{
    partial class frmGrafVendasPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGrafVendasPrincipal));
            this.btnCarregaRelatorio = new DevComponents.DotNetBar.ButtonX();
            this.gpEntradaSaida = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dtpFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpInicial = new System.Windows.Forms.DateTimePicker();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.gpEntradaSaida.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarregaRelatorio
            // 
            this.btnCarregaRelatorio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCarregaRelatorio.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCarregaRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregaRelatorio.Location = new System.Drawing.Point(363, 112);
            this.btnCarregaRelatorio.Name = "btnCarregaRelatorio";
            this.btnCarregaRelatorio.Size = new System.Drawing.Size(113, 23);
            this.btnCarregaRelatorio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCarregaRelatorio.TabIndex = 8;
            this.btnCarregaRelatorio.Text = "Carregar Relatório";
            this.btnCarregaRelatorio.Click += new System.EventHandler(this.btnCarregaRelatorio_Click);
            // 
            // gpEntradaSaida
            // 
            this.gpEntradaSaida.BackColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpEntradaSaida.Controls.Add(this.dtpFinal);
            this.gpEntradaSaida.Controls.Add(this.dtpInicial);
            this.gpEntradaSaida.Controls.Add(this.labelX2);
            this.gpEntradaSaida.Controls.Add(this.labelX1);
            this.gpEntradaSaida.Location = new System.Drawing.Point(12, 44);
            this.gpEntradaSaida.Name = "gpEntradaSaida";
            this.gpEntradaSaida.Size = new System.Drawing.Size(464, 57);
            // 
            // 
            // 
            this.gpEntradaSaida.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpEntradaSaida.Style.BackColorGradientAngle = 90;
            this.gpEntradaSaida.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpEntradaSaida.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderBottomWidth = 1;
            this.gpEntradaSaida.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpEntradaSaida.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderLeftWidth = 1;
            this.gpEntradaSaida.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderRightWidth = 1;
            this.gpEntradaSaida.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderTopWidth = 1;
            this.gpEntradaSaida.Style.CornerDiameter = 4;
            this.gpEntradaSaida.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpEntradaSaida.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpEntradaSaida.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpEntradaSaida.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpEntradaSaida.TabIndex = 7;
            // 
            // dtpFinal
            // 
            this.dtpFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFinal.Location = new System.Drawing.Point(316, 15);
            this.dtpFinal.Name = "dtpFinal";
            this.dtpFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpFinal.TabIndex = 3;
            this.dtpFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFinal_KeyDown);
            // 
            // dtpInicial
            // 
            this.dtpInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInicial.Location = new System.Drawing.Point(103, 15);
            this.dtpInicial.Name = "dtpInicial";
            this.dtpInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpInicial.TabIndex = 1;
            this.dtpInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpInicial_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Blue;
            this.labelX2.Location = new System.Drawing.Point(250, 14);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "Data Final:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Blue;
            this.labelX1.Location = new System.Drawing.Point(32, 14);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Data Inicial:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Blue;
            this.labelX3.Location = new System.Drawing.Point(12, 12);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(464, 23);
            this.labelX3.TabIndex = 9;
            this.labelX3.Text = "Período das Vendas";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // frmGrafVendasPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 145);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.btnCarregaRelatorio);
            this.Controls.Add(this.gpEntradaSaida);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGrafVendasPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Gráfico de Vendas";
            this.gpEntradaSaida.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCarregaRelatorio;
        private DevComponents.DotNetBar.Controls.GroupPanel gpEntradaSaida;
        private System.Windows.Forms.DateTimePicker dtpFinal;
        private System.Windows.Forms.DateTimePicker dtpInicial;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;

    }
}