﻿namespace Relatorios
{
    partial class frmRltRetiradasPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltRetiradasPrincipal));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cbUsuario = new System.Windows.Forms.ComboBox();
            this.dtpGeralFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpGeralInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpAtendenteInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpAtendenteFinal = new System.Windows.Forms.DateTimePicker();
            this.rbUsuario = new System.Windows.Forms.RadioButton();
            this.rbGeral = new System.Windows.Forms.RadioButton();
            this.btnCarregaRelatorio = new DevComponents.DotNetBar.ButtonX();
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.getUsuarioRetiradaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.getUsuarioRetiradaTableAdapter = new Relatorios.dsRelatoriosTableAdapters.GetUsuarioRetiradaTableAdapter();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getUsuarioRetiradaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.cbUsuario);
            this.groupPanel1.Controls.Add(this.dtpGeralFinal);
            this.groupPanel1.Controls.Add(this.dtpGeralInicial);
            this.groupPanel1.Controls.Add(this.dtpAtendenteInicial);
            this.groupPanel1.Controls.Add(this.dtpAtendenteFinal);
            this.groupPanel1.Controls.Add(this.rbUsuario);
            this.groupPanel1.Controls.Add(this.rbGeral);
            this.groupPanel1.Location = new System.Drawing.Point(12, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(522, 182);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 22;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Blue;
            this.labelX5.Location = new System.Drawing.Point(35, 140);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(120, 23);
            this.labelX5.TabIndex = 33;
            this.labelX5.Text = "Nome do Atendente:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Blue;
            this.labelX3.Location = new System.Drawing.Point(253, 101);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 32;
            this.labelX3.Text = "Data Final:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Blue;
            this.labelX4.Location = new System.Drawing.Point(35, 101);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 31;
            this.labelX4.Text = "Data Inicial:";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Blue;
            this.labelX2.Location = new System.Drawing.Point(253, 30);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 30;
            this.labelX2.Text = "Data Final:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Blue;
            this.labelX1.Location = new System.Drawing.Point(35, 30);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 29;
            this.labelX1.Text = "Data Inicial:";
            // 
            // cbUsuario
            // 
            this.cbUsuario.DisplayMember = "nomeLoginUsuario";
            this.cbUsuario.FormattingEnabled = true;
            this.cbUsuario.Location = new System.Drawing.Point(163, 142);
            this.cbUsuario.Name = "cbUsuario";
            this.cbUsuario.Size = new System.Drawing.Size(175, 21);
            this.cbUsuario.TabIndex = 28;
            this.cbUsuario.ValueMember = "idUsuario";
            this.cbUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralFinal
            // 
            this.dtpGeralFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralFinal.Location = new System.Drawing.Point(329, 32);
            this.dtpGeralFinal.Name = "dtpGeralFinal";
            this.dtpGeralFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralFinal.TabIndex = 24;
            this.dtpGeralFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralInicial
            // 
            this.dtpGeralInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralInicial.Location = new System.Drawing.Point(116, 32);
            this.dtpGeralInicial.Name = "dtpGeralInicial";
            this.dtpGeralInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralInicial.TabIndex = 23;
            this.dtpGeralInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpAtendenteInicial
            // 
            this.dtpAtendenteInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpAtendenteInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAtendenteInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAtendenteInicial.Location = new System.Drawing.Point(117, 103);
            this.dtpAtendenteInicial.Name = "dtpAtendenteInicial";
            this.dtpAtendenteInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpAtendenteInicial.TabIndex = 26;
            this.dtpAtendenteInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpAtendenteFinal
            // 
            this.dtpAtendenteFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpAtendenteFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAtendenteFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAtendenteFinal.Location = new System.Drawing.Point(329, 103);
            this.dtpAtendenteFinal.Name = "dtpAtendenteFinal";
            this.dtpAtendenteFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpAtendenteFinal.TabIndex = 27;
            this.dtpAtendenteFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // rbUsuario
            // 
            this.rbUsuario.AutoSize = true;
            this.rbUsuario.BackColor = System.Drawing.Color.Transparent;
            this.rbUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbUsuario.Location = new System.Drawing.Point(17, 67);
            this.rbUsuario.Name = "rbUsuario";
            this.rbUsuario.Size = new System.Drawing.Size(68, 19);
            this.rbUsuario.TabIndex = 25;
            this.rbUsuario.Text = "Usuário";
            this.rbUsuario.UseVisualStyleBackColor = false;
            this.rbUsuario.Click += new System.EventHandler(this.rbAtendente_Click);
            // 
            // rbGeral
            // 
            this.rbGeral.AutoSize = true;
            this.rbGeral.BackColor = System.Drawing.Color.Transparent;
            this.rbGeral.Checked = true;
            this.rbGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGeral.Location = new System.Drawing.Point(17, 3);
            this.rbGeral.Name = "rbGeral";
            this.rbGeral.Size = new System.Drawing.Size(55, 19);
            this.rbGeral.TabIndex = 22;
            this.rbGeral.TabStop = true;
            this.rbGeral.Text = "Geral";
            this.rbGeral.UseVisualStyleBackColor = false;
            this.rbGeral.Click += new System.EventHandler(this.rbGeral_Click);
            // 
            // btnCarregaRelatorio
            // 
            this.btnCarregaRelatorio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCarregaRelatorio.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCarregaRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregaRelatorio.Location = new System.Drawing.Point(421, 200);
            this.btnCarregaRelatorio.Name = "btnCarregaRelatorio";
            this.btnCarregaRelatorio.Size = new System.Drawing.Size(113, 23);
            this.btnCarregaRelatorio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCarregaRelatorio.TabIndex = 23;
            this.btnCarregaRelatorio.Text = "Carregar Relatório";
            this.btnCarregaRelatorio.Click += new System.EventHandler(this.btnCarregaRelatorio_Click);
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // getUsuarioRetiradaBindingSource
            // 
            this.getUsuarioRetiradaBindingSource.DataMember = "GetUsuarioRetirada";
            this.getUsuarioRetiradaBindingSource.DataSource = this.dsRelatorios;
            // 
            // getUsuarioRetiradaTableAdapter
            // 
            this.getUsuarioRetiradaTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltRetiradasPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 230);
            this.Controls.Add(this.btnCarregaRelatorio);
            this.Controls.Add(this.groupPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltRetiradasPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Retiradas";
            this.Load += new System.EventHandler(this.frmRltRetiradasPrincipal_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getUsuarioRetiradaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.ComboBox cbUsuario;
        private System.Windows.Forms.DateTimePicker dtpGeralFinal;
        private System.Windows.Forms.DateTimePicker dtpGeralInicial;
        private System.Windows.Forms.DateTimePicker dtpAtendenteInicial;
        private System.Windows.Forms.DateTimePicker dtpAtendenteFinal;
        private System.Windows.Forms.RadioButton rbUsuario;
        private System.Windows.Forms.RadioButton rbGeral;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnCarregaRelatorio;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource getUsuarioRetiradaBindingSource;
        private dsRelatoriosTableAdapters.GetUsuarioRetiradaTableAdapter getUsuarioRetiradaTableAdapter;
    }
}