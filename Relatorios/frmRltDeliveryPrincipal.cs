﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace Relatorios
{
    public partial class frmRltDeliveryPrincipal : Form
    {
        MeuLanche meuLanche = new MeuLanche();

        public frmRltDeliveryPrincipal(MeuLanche meulanche)
        {
            InitializeComponent();
            meuLanche = meulanche;
        }

        private void frmRltDeliveryPrincipal_Load(object sender, EventArgs e)
        {
            EntregadorNegocios entregadorNegocios = new EntregadorNegocios();

            EntregadorCollections entregadorCollections = entregadorNegocios.ConsultarPorNome("%");

            cbEntregador.DataSource = null;
            cbEntregador.DataSource = entregadorCollections;

            cbEntregador.Enabled = false;
            dtpEntregadorInicial.Enabled = false;
            dtpEntregadorFinal.Enabled = false;

            dtpEntregasInicial.Enabled = false;
            dtpEntregasFinal.Enabled = false;

        }

        private void ckbDiarias_Click(object sender, EventArgs e)
        {
            ckbMensal.CheckState = CheckState.Unchecked;
        }

        private void ckbMensal_Click(object sender, EventArgs e)
        {
            ckbDiarias.CheckState = CheckState.Unchecked;
        }

        private void rbGeral_Click(object sender, EventArgs e)
        {
            cbEntregador.Enabled = false;
            dtpEntregadorInicial.Enabled = false;
            dtpEntregadorFinal.Enabled = false;
            dtpEntregasInicial.Enabled = false;
            dtpEntregasFinal.Enabled = false;

            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void rbEntregador_Click(object sender, EventArgs e)
        {
            dtpEntregasInicial.Enabled = false;
            dtpEntregasFinal.Enabled = false;
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;

            cbEntregador.Enabled = true;
            cbEntregador.Focus();
            dtpEntregadorInicial.Enabled = true;
            dtpEntregadorFinal.Enabled = true;
        }

        private void rbEntregas_Click(object sender, EventArgs e)
        {
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
            cbEntregador.Enabled = false;
            dtpEntregadorInicial.Enabled = false;
            dtpEntregadorFinal.Enabled = false;

            dtpEntregasInicial.Enabled = true;
            dtpEntregasFinal.Enabled = true;
        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (rbGeral.Checked)
            {
                frmRltDelivery objDelivery = new frmRltDelivery(relatorioDelivery.DeliveryGeral, dtpGeralInicial.Value, dtpGeralFinal.Value, meuLanche);
                objDelivery.ShowDialog();
            }
            else if (rbEntregador.Checked)
            {
                if (ckbTodosEntregadores.Checked)
                {
                    frmRltDelivery objDelivery = new frmRltDelivery(relatorioDelivery.DeliveryEntregadorGeral, dtpEntregadorInicial.Value, dtpEntregadorFinal.Value, meuLanche);
                    objDelivery.ShowDialog();
                }
                else
                {
                    frmRltDelivery objDelivery = new frmRltDelivery(relatorioDelivery.DeliveryEntregador, cbEntregador.Text.ToUpper(), dtpEntregadorInicial.Value, dtpEntregadorFinal.Value, meuLanche);
                    objDelivery.ShowDialog();
                }

            }
            else if (rbEntregas.Checked)
            {
                if (ckbDiarias.Checked)
                {
                    frmRltDelivery objDelivery = new frmRltDelivery(relatorioDelivery.DeliveryEntregasDiarias, dtpEntregasInicial.Value, dtpEntregasFinal.Value, meuLanche);
                    objDelivery.ShowDialog();
                }
                else if (ckbMensal.Checked)
                {
                    frmRltDelivery objDelivery = new frmRltDelivery(relatorioDelivery.DeliveryEntregasMensal, dtpEntregasInicial.Value, dtpEntregasFinal.Value, meuLanche);
                    objDelivery.ShowDialog();
                }
            }
            else if (rbCancelada.Checked)
            {
                frmRltDelivery objDelivery = new frmRltDelivery(relatorioDelivery.DeliveryCancelada, dtpCanceladaInicial.Value, dtpCanceladaFinal.Value, meuLanche);
                objDelivery.ShowDialog();
            }
        }

        private void dtpGeralInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }
    }
}
