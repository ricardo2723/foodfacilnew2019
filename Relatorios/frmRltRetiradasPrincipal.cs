﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace Relatorios
{    
    public partial class frmRltRetiradasPrincipal : Form
    {
        MeuLanche meuLache = new MeuLanche();

        public frmRltRetiradasPrincipal(MeuLanche meulanche)
        {
            InitializeComponent();
            meuLache = meulanche;
        }

        private void frmRltRetiradasPrincipal_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dsRelatorios.GetUsuarioRetirada' table. You can move, or remove it, as needed.
            //this.getUsuarioRetiradaTableAdapter.GetUsuarioRetirada(this.dsRelatorios.GetUsuarioRetirada);
            RetiradasNegocios retiradaNegocios = new RetiradasNegocios();
            RetiradasCollections retiradasCollections = retiradaNegocios.GetUsuarioRetirada();

            cbUsuario.DataSource = null;
            cbUsuario.DataSource = retiradasCollections;

            cbUsuario.Enabled = false;
            dtpAtendenteInicial.Enabled = false;
            dtpAtendenteFinal.Enabled = false;

            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void rbGeral_Click(object sender, EventArgs e)
        {
            cbUsuario.Enabled = false;
            dtpAtendenteInicial.Enabled = false;
            dtpAtendenteFinal.Enabled = false;

            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void rbAtendente_Click(object sender, EventArgs e)
        {
            cbUsuario.Enabled = true;
            cbUsuario.Focus();
            dtpAtendenteInicial.Enabled = true;
            dtpAtendenteFinal.Enabled = true;

            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (rbGeral.Checked)
            {
                frmRltRetiradas objRetiradas = new frmRltRetiradas(relatorioRetirada.RetiradaGeral, dtpGeralInicial.Value, dtpGeralFinal.Value, meuLache);
                objRetiradas.ShowDialog();
            }
            else if (rbUsuario.Checked)
            {
                frmRltRetiradas objRetiradas = new frmRltRetiradas(relatorioRetirada.RetiradaUsuario, Convert.ToInt32(cbUsuario.SelectedValue), dtpAtendenteInicial.Value, dtpAtendenteFinal.Value, meuLache);
                objRetiradas.ShowDialog();
            }
        }

        private void dtpGeralInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }

    }
}
