﻿namespace Relatorios
{
    partial class frmRltVendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltVendas));
            this.rptvVendas = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.VendasGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vendasGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.VendasGeralTableAdapter();
            this.VendasAtendenteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vendasAtendenteTableAdapter = new Relatorios.dsRelatoriosTableAdapters.VendasAtendenteTableAdapter();
            this.VendasGrupoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vendasGrupoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.VendasGrupoTableAdapter();
            this.VendasProdutoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vendasProdutoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.VendasProdutoTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasAtendenteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasGrupoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasProdutoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rptvVendas
            // 
            this.rptvVendas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptvVendas.Location = new System.Drawing.Point(0, 0);
            this.rptvVendas.Name = "rptvVendas";
            this.rptvVendas.Size = new System.Drawing.Size(1044, 451);
            this.rptvVendas.TabIndex = 0;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // VendasGeralBindingSource
            // 
            this.VendasGeralBindingSource.DataMember = "VendasGeral";
            this.VendasGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // vendasGeralTableAdapter
            // 
            this.vendasGeralTableAdapter.ClearBeforeFill = true;
            // 
            // VendasAtendenteBindingSource
            // 
            this.VendasAtendenteBindingSource.DataMember = "VendasAtendente";
            this.VendasAtendenteBindingSource.DataSource = this.dsRelatorios;
            // 
            // vendasAtendenteTableAdapter
            // 
            this.vendasAtendenteTableAdapter.ClearBeforeFill = true;
            // 
            // VendasGrupoBindingSource
            // 
            this.VendasGrupoBindingSource.DataMember = "VendasGrupo";
            this.VendasGrupoBindingSource.DataSource = this.dsRelatorios;
            // 
            // vendasGrupoTableAdapter
            // 
            this.vendasGrupoTableAdapter.ClearBeforeFill = true;
            // 
            // VendasProdutoBindingSource
            // 
            this.VendasProdutoBindingSource.DataMember = "VendasProduto";
            this.VendasProdutoBindingSource.DataSource = this.dsRelatorios;
            // 
            // vendasProdutoTableAdapter
            // 
            this.vendasProdutoTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltVendas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 451);
            this.Controls.Add(this.rptvVendas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltVendas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Vendas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltVendas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasAtendenteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasGrupoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendasProdutoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvVendas;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource VendasGeralBindingSource;
        private dsRelatoriosTableAdapters.VendasGeralTableAdapter vendasGeralTableAdapter;
        private System.Windows.Forms.BindingSource VendasAtendenteBindingSource;
        private dsRelatoriosTableAdapters.VendasAtendenteTableAdapter vendasAtendenteTableAdapter;
        private System.Windows.Forms.BindingSource VendasGrupoBindingSource;
        private dsRelatoriosTableAdapters.VendasGrupoTableAdapter vendasGrupoTableAdapter;
        private System.Windows.Forms.BindingSource VendasProdutoBindingSource;
        private dsRelatoriosTableAdapters.VendasProdutoTableAdapter vendasProdutoTableAdapter;
    }
}