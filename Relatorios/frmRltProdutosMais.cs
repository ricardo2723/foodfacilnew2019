﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltProdutosMais : Form
    {
        MeuLanche meuLanche = new MeuLanche();

        public frmRltProdutosMais(MeuLanche meulanche)
        {
            InitializeComponent();
            meuLanche = meulanche;
        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            frmRltProdutos objProdutosMais = new frmRltProdutos(relatorioProdutos.MaisVendido, dtpInicial.Value, dtpFinal.Value, meuLanche);
            objProdutosMais.ShowDialog();
        }

        private void dtpInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }    
   }
}
