﻿namespace Relatorios
{
    partial class frmRltDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltDelivery));
            this.DeliveryGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.rptvDelivery = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DeliveryGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.DeliveryGeralTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            this.SuspendLayout();
            // 
            // DeliveryGeralBindingSource
            // 
            this.DeliveryGeralBindingSource.DataMember = "DeliveryGeral";
            this.DeliveryGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rptvDelivery
            // 
            this.rptvDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Entregas";
            reportDataSource1.Value = this.DeliveryGeralBindingSource;
            this.rptvDelivery.LocalReport.DataSources.Add(reportDataSource1);
            this.rptvDelivery.LocalReport.ReportEmbeddedResource = "Relatorios.rltDeliveryGeral.rdlc";
            this.rptvDelivery.Location = new System.Drawing.Point(0, 0);
            this.rptvDelivery.Name = "rptvDelivery";
            this.rptvDelivery.Size = new System.Drawing.Size(1044, 451);
            this.rptvDelivery.TabIndex = 0;
            // 
            // DeliveryGeralTableAdapter
            // 
            this.DeliveryGeralTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 451);
            this.Controls.Add(this.rptvDelivery);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltDelivery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios das Deliverys";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltDelivery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvDelivery;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource DeliveryGeralBindingSource;
        private dsRelatoriosTableAdapters.DeliveryGeralTableAdapter DeliveryGeralTableAdapter;
    }
}