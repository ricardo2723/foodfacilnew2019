﻿namespace Relatorios
{
    partial class frmRltDeliveryPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltDeliveryPrincipal));
            this.btnCarregaRelatorio = new DevComponents.DotNetBar.ButtonX();
            this.gpEntradaSaida = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.line3 = new DevComponents.DotNetBar.Controls.Line();
            this.dtpCanceladaInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpCanceladaFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.rbCancelada = new System.Windows.Forms.RadioButton();
            this.ckbTodosEntregadores = new System.Windows.Forms.CheckBox();
            this.line2 = new DevComponents.DotNetBar.Controls.Line();
            this.line1 = new DevComponents.DotNetBar.Controls.Line();
            this.ckbMensal = new System.Windows.Forms.CheckBox();
            this.ckbDiarias = new System.Windows.Forms.CheckBox();
            this.dtpEntregasInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpEntregasFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.label = new DevComponents.DotNetBar.LabelX();
            this.rbEntregas = new System.Windows.Forms.RadioButton();
            this.cbEntregador = new System.Windows.Forms.ComboBox();
            this.dtpGeralFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpGeralInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpEntregadorInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpEntregadorFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.rbEntregador = new System.Windows.Forms.RadioButton();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rbGeral = new System.Windows.Forms.RadioButton();
            this.gpEntradaSaida.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarregaRelatorio
            // 
            this.btnCarregaRelatorio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCarregaRelatorio.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCarregaRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregaRelatorio.Location = new System.Drawing.Point(347, 402);
            this.btnCarregaRelatorio.Name = "btnCarregaRelatorio";
            this.btnCarregaRelatorio.Size = new System.Drawing.Size(113, 23);
            this.btnCarregaRelatorio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCarregaRelatorio.TabIndex = 8;
            this.btnCarregaRelatorio.Text = "Carregar Relatório";
            this.btnCarregaRelatorio.Click += new System.EventHandler(this.btnCarregaRelatorio_Click);
            // 
            // gpEntradaSaida
            // 
            this.gpEntradaSaida.BackColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpEntradaSaida.Controls.Add(this.line3);
            this.gpEntradaSaida.Controls.Add(this.dtpCanceladaInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpCanceladaFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX8);
            this.gpEntradaSaida.Controls.Add(this.labelX9);
            this.gpEntradaSaida.Controls.Add(this.rbCancelada);
            this.gpEntradaSaida.Controls.Add(this.ckbTodosEntregadores);
            this.gpEntradaSaida.Controls.Add(this.line2);
            this.gpEntradaSaida.Controls.Add(this.line1);
            this.gpEntradaSaida.Controls.Add(this.ckbMensal);
            this.gpEntradaSaida.Controls.Add(this.ckbDiarias);
            this.gpEntradaSaida.Controls.Add(this.dtpEntregasInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpEntregasFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX7);
            this.gpEntradaSaida.Controls.Add(this.label);
            this.gpEntradaSaida.Controls.Add(this.rbEntregas);
            this.gpEntradaSaida.Controls.Add(this.cbEntregador);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralFinal);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpEntregadorInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpEntregadorFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX5);
            this.gpEntradaSaida.Controls.Add(this.labelX3);
            this.gpEntradaSaida.Controls.Add(this.labelX4);
            this.gpEntradaSaida.Controls.Add(this.rbEntregador);
            this.gpEntradaSaida.Controls.Add(this.labelX2);
            this.gpEntradaSaida.Controls.Add(this.labelX1);
            this.gpEntradaSaida.Controls.Add(this.rbGeral);
            this.gpEntradaSaida.Location = new System.Drawing.Point(6, 16);
            this.gpEntradaSaida.Name = "gpEntradaSaida";
            this.gpEntradaSaida.Size = new System.Drawing.Size(454, 380);
            // 
            // 
            // 
            this.gpEntradaSaida.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpEntradaSaida.Style.BackColorGradientAngle = 90;
            this.gpEntradaSaida.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpEntradaSaida.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderBottomWidth = 1;
            this.gpEntradaSaida.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpEntradaSaida.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderLeftWidth = 1;
            this.gpEntradaSaida.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderRightWidth = 1;
            this.gpEntradaSaida.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderTopWidth = 1;
            this.gpEntradaSaida.Style.CornerDiameter = 4;
            this.gpEntradaSaida.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpEntradaSaida.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpEntradaSaida.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpEntradaSaida.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpEntradaSaida.TabIndex = 7;
            // 
            // line3
            // 
            this.line3.ForeColor = System.Drawing.Color.Silver;
            this.line3.Location = new System.Drawing.Point(14, 289);
            this.line3.Name = "line3";
            this.line3.Size = new System.Drawing.Size(422, 14);
            this.line3.TabIndex = 39;
            this.line3.Text = "line3";
            this.line3.Thickness = 2;
            // 
            // dtpCanceladaInicial
            // 
            this.dtpCanceladaInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpCanceladaInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCanceladaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCanceladaInicial.Location = new System.Drawing.Point(114, 337);
            this.dtpCanceladaInicial.Name = "dtpCanceladaInicial";
            this.dtpCanceladaInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpCanceladaInicial.TabIndex = 33;
            this.dtpCanceladaInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpCanceladaFinal
            // 
            this.dtpCanceladaFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpCanceladaFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCanceladaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCanceladaFinal.Location = new System.Drawing.Point(326, 337);
            this.dtpCanceladaFinal.Name = "dtpCanceladaFinal";
            this.dtpCanceladaFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpCanceladaFinal.TabIndex = 35;
            this.dtpCanceladaFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.Blue;
            this.labelX8.Location = new System.Drawing.Point(260, 336);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(75, 23);
            this.labelX8.TabIndex = 36;
            this.labelX8.Text = "Data Final:";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Blue;
            this.labelX9.Location = new System.Drawing.Point(42, 336);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(75, 23);
            this.labelX9.TabIndex = 34;
            this.labelX9.Text = "Data Inicial:";
            // 
            // rbCancelada
            // 
            this.rbCancelada.AutoSize = true;
            this.rbCancelada.BackColor = System.Drawing.Color.Transparent;
            this.rbCancelada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCancelada.Location = new System.Drawing.Point(14, 308);
            this.rbCancelada.Name = "rbCancelada";
            this.rbCancelada.Size = new System.Drawing.Size(84, 19);
            this.rbCancelada.TabIndex = 32;
            this.rbCancelada.Text = "Cancelada";
            this.rbCancelada.UseVisualStyleBackColor = false;
            // 
            // ckbTodosEntregadores
            // 
            this.ckbTodosEntregadores.AutoSize = true;
            this.ckbTodosEntregadores.Location = new System.Drawing.Point(314, 212);
            this.ckbTodosEntregadores.Name = "ckbTodosEntregadores";
            this.ckbTodosEntregadores.Size = new System.Drawing.Size(122, 17);
            this.ckbTodosEntregadores.TabIndex = 31;
            this.ckbTodosEntregadores.Text = "Todos Entregadores";
            this.ckbTodosEntregadores.UseVisualStyleBackColor = true;
            // 
            // line2
            // 
            this.line2.ForeColor = System.Drawing.Color.Silver;
            this.line2.Location = new System.Drawing.Point(14, 161);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(422, 14);
            this.line2.TabIndex = 30;
            this.line2.Text = "line2";
            this.line2.Thickness = 2;
            // 
            // line1
            // 
            this.line1.ForeColor = System.Drawing.Color.Silver;
            this.line1.Location = new System.Drawing.Point(14, 60);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(422, 14);
            this.line1.TabIndex = 29;
            this.line1.Text = "line1";
            this.line1.Thickness = 2;
            // 
            // ckbMensal
            // 
            this.ckbMensal.AutoSize = true;
            this.ckbMensal.Location = new System.Drawing.Point(106, 106);
            this.ckbMensal.Name = "ckbMensal";
            this.ckbMensal.Size = new System.Drawing.Size(60, 17);
            this.ckbMensal.TabIndex = 28;
            this.ckbMensal.Text = "Mensal";
            this.ckbMensal.UseVisualStyleBackColor = true;
            this.ckbMensal.Click += new System.EventHandler(this.ckbMensal_Click);
            // 
            // ckbDiarias
            // 
            this.ckbDiarias.AutoSize = true;
            this.ckbDiarias.Checked = true;
            this.ckbDiarias.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbDiarias.Location = new System.Drawing.Point(42, 106);
            this.ckbDiarias.Name = "ckbDiarias";
            this.ckbDiarias.Size = new System.Drawing.Size(58, 17);
            this.ckbDiarias.TabIndex = 27;
            this.ckbDiarias.Text = "Diarias";
            this.ckbDiarias.UseVisualStyleBackColor = true;
            this.ckbDiarias.Click += new System.EventHandler(this.ckbDiarias_Click);
            // 
            // dtpEntregasInicial
            // 
            this.dtpEntregasInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEntregasInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntregasInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEntregasInicial.Location = new System.Drawing.Point(114, 133);
            this.dtpEntregasInicial.Name = "dtpEntregasInicial";
            this.dtpEntregasInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpEntregasInicial.TabIndex = 23;
            this.dtpEntregasInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpEntregasFinal
            // 
            this.dtpEntregasFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEntregasFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntregasFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEntregasFinal.Location = new System.Drawing.Point(326, 133);
            this.dtpEntregasFinal.Name = "dtpEntregasFinal";
            this.dtpEntregasFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpEntregasFinal.TabIndex = 25;
            this.dtpEntregasFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.Blue;
            this.labelX7.Location = new System.Drawing.Point(260, 132);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(75, 23);
            this.labelX7.TabIndex = 26;
            this.labelX7.Text = "Data Final:";
            // 
            // label
            // 
            this.label.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.label.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.ForeColor = System.Drawing.Color.Blue;
            this.label.Location = new System.Drawing.Point(42, 132);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(75, 23);
            this.label.TabIndex = 24;
            this.label.Text = "Data Inicial:";
            // 
            // rbEntregas
            // 
            this.rbEntregas.AutoSize = true;
            this.rbEntregas.BackColor = System.Drawing.Color.Transparent;
            this.rbEntregas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEntregas.Location = new System.Drawing.Point(14, 80);
            this.rbEntregas.Name = "rbEntregas";
            this.rbEntregas.Size = new System.Drawing.Size(74, 19);
            this.rbEntregas.TabIndex = 22;
            this.rbEntregas.Text = "Entregas";
            this.rbEntregas.UseVisualStyleBackColor = false;
            this.rbEntregas.Click += new System.EventHandler(this.rbEntregas_Click);
            // 
            // cbEntregador
            // 
            this.cbEntregador.DisplayMember = "apelidoEntregador";
            this.cbEntregador.FormattingEnabled = true;
            this.cbEntregador.Location = new System.Drawing.Point(42, 235);
            this.cbEntregador.Name = "cbEntregador";
            this.cbEntregador.Size = new System.Drawing.Size(394, 21);
            this.cbEntregador.TabIndex = 14;
            this.cbEntregador.ValueMember = "idEntregador";
            this.cbEntregador.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralFinal
            // 
            this.dtpGeralFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralFinal.Location = new System.Drawing.Point(326, 32);
            this.dtpGeralFinal.Name = "dtpGeralFinal";
            this.dtpGeralFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralFinal.TabIndex = 3;
            this.dtpGeralFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralInicial
            // 
            this.dtpGeralInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralInicial.Location = new System.Drawing.Point(113, 32);
            this.dtpGeralInicial.Name = "dtpGeralInicial";
            this.dtpGeralInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralInicial.TabIndex = 1;
            this.dtpGeralInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpEntregadorInicial
            // 
            this.dtpEntregadorInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEntregadorInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntregadorInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEntregadorInicial.Location = new System.Drawing.Point(114, 261);
            this.dtpEntregadorInicial.Name = "dtpEntregadorInicial";
            this.dtpEntregadorInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpEntregadorInicial.TabIndex = 8;
            this.dtpEntregadorInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpEntregadorFinal
            // 
            this.dtpEntregadorFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEntregadorFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntregadorFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEntregadorFinal.Location = new System.Drawing.Point(326, 261);
            this.dtpEntregadorFinal.Name = "dtpEntregadorFinal";
            this.dtpEntregadorFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpEntregadorFinal.TabIndex = 10;
            this.dtpEntregadorFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Blue;
            this.labelX5.Location = new System.Drawing.Point(42, 206);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(120, 23);
            this.labelX5.TabIndex = 13;
            this.labelX5.Text = "Nome do Entregador";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Blue;
            this.labelX3.Location = new System.Drawing.Point(260, 260);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 11;
            this.labelX3.Text = "Data Final:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Blue;
            this.labelX4.Location = new System.Drawing.Point(42, 260);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 9;
            this.labelX4.Text = "Data Inicial:";
            // 
            // rbEntregador
            // 
            this.rbEntregador.AutoSize = true;
            this.rbEntregador.BackColor = System.Drawing.Color.Transparent;
            this.rbEntregador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEntregador.Location = new System.Drawing.Point(14, 180);
            this.rbEntregador.Name = "rbEntregador";
            this.rbEntregador.Size = new System.Drawing.Size(86, 19);
            this.rbEntregador.TabIndex = 7;
            this.rbEntregador.Text = "Entregador";
            this.rbEntregador.UseVisualStyleBackColor = false;
            this.rbEntregador.Click += new System.EventHandler(this.rbEntregador_Click);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Blue;
            this.labelX2.Location = new System.Drawing.Point(260, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "Data Final:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Blue;
            this.labelX1.Location = new System.Drawing.Point(42, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Data Inicial:";
            // 
            // rbGeral
            // 
            this.rbGeral.AutoSize = true;
            this.rbGeral.BackColor = System.Drawing.Color.Transparent;
            this.rbGeral.Checked = true;
            this.rbGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGeral.Location = new System.Drawing.Point(14, 3);
            this.rbGeral.Name = "rbGeral";
            this.rbGeral.Size = new System.Drawing.Size(55, 19);
            this.rbGeral.TabIndex = 0;
            this.rbGeral.TabStop = true;
            this.rbGeral.Text = "Geral";
            this.rbGeral.UseVisualStyleBackColor = false;
            this.rbGeral.Click += new System.EventHandler(this.rbGeral_Click);
            // 
            // frmRltDeliveryPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 431);
            this.Controls.Add(this.btnCarregaRelatorio);
            this.Controls.Add(this.gpEntradaSaida);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltDeliveryPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios das Deliverys";
            this.Load += new System.EventHandler(this.frmRltDeliveryPrincipal_Load);
            this.gpEntradaSaida.ResumeLayout(false);
            this.gpEntradaSaida.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCarregaRelatorio;
        private DevComponents.DotNetBar.Controls.GroupPanel gpEntradaSaida;
        private System.Windows.Forms.DateTimePicker dtpGeralFinal;
        private System.Windows.Forms.DateTimePicker dtpGeralInicial;
        private System.Windows.Forms.DateTimePicker dtpEntregadorInicial;
        private System.Windows.Forms.DateTimePicker dtpEntregadorFinal;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.RadioButton rbEntregador;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.RadioButton rbGeral;
        private System.Windows.Forms.ComboBox cbEntregador;
        private System.Windows.Forms.CheckBox ckbMensal;
        private System.Windows.Forms.CheckBox ckbDiarias;
        private System.Windows.Forms.DateTimePicker dtpEntregasInicial;
        private System.Windows.Forms.DateTimePicker dtpEntregasFinal;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX label;
        private System.Windows.Forms.RadioButton rbEntregas;
        private DevComponents.DotNetBar.Controls.Line line2;
        private DevComponents.DotNetBar.Controls.Line line1;
        private System.Windows.Forms.CheckBox ckbTodosEntregadores;
        private DevComponents.DotNetBar.Controls.Line line3;
        private System.Windows.Forms.DateTimePicker dtpCanceladaInicial;
        private System.Windows.Forms.DateTimePicker dtpCanceladaFinal;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.RadioButton rbCancelada;

    }
}