﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjetoTransferencia;
using Negocios;

namespace Relatorios
{
    public partial class frmRltVendaPrincipal : Form
    {
        MeuLanche meuLanche = new MeuLanche();

        public frmRltVendaPrincipal(MeuLanche meulanche)
        {
            InitializeComponent();
            meuLanche = meulanche;
        }

        private void rbGeral_Click(object sender, EventArgs e)
        {
            cbAtendente.Enabled = false;
            dtpAtendenteInicial.Enabled = false;
            dtpAtendenteFinal.Enabled = false;
            cbGrupos.Enabled = false;
            dtpGrupoInicial.Enabled = false;
            dtpGrupoFinal.Enabled = false;
            txtCodigoProduto.Enabled = false;
            dtpProdutoInicial.Enabled = false;
            dtpProdutoFinal.Enabled = false;

            dtpGeralInicial.Enabled = true;
            dtpGeralFinal.Enabled = true;
        }

        private void rbAtendente_Click(object sender, EventArgs e)
        {
            cbGrupos.Enabled = false;
            dtpGrupoInicial.Enabled = false;
            dtpGrupoFinal.Enabled = false;
            txtCodigoProduto.Enabled = false;
            dtpProdutoInicial.Enabled = false;
            dtpProdutoFinal.Enabled = false;
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;

            cbAtendente.Enabled = true;
            cbAtendente.Focus();
            dtpAtendenteInicial.Enabled = true;
            dtpAtendenteFinal.Enabled = true;
        }

        private void rbGrupos_Click(object sender, EventArgs e)
        {
            txtCodigoProduto.Enabled = false;
            dtpProdutoInicial.Enabled = false;
            dtpProdutoFinal.Enabled = false;
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
            cbAtendente.Enabled = false;
            dtpAtendenteInicial.Enabled = false;
            dtpAtendenteFinal.Enabled = false;

            cbGrupos.Enabled = true;
            cbGrupos.Focus();
            dtpGrupoInicial.Enabled = true;
            dtpGrupoFinal.Enabled = true;
        }

        private void rbProduto_Click(object sender, EventArgs e)
        {
            dtpGeralInicial.Enabled = false;
            dtpGeralFinal.Enabled = false;
            cbAtendente.Enabled = false;
            dtpAtendenteInicial.Enabled = false;
            dtpAtendenteFinal.Enabled = false;
            cbGrupos.Enabled = false;
            dtpGrupoInicial.Enabled = false;
            dtpGrupoFinal.Enabled = false;

            txtCodigoProduto.Enabled = true;
            txtCodigoProduto.Focus();
            dtpProdutoInicial.Enabled = true;
            dtpProdutoFinal.Enabled = true;
        }

        private void frmRltVendaPrincipal_Load(object sender, EventArgs e)
        {
            VendaCabecalhoNegocios vendaCabecalhoNegocios = new VendaCabecalhoNegocios();
            GruposNegocios gruposNegocios = new GruposNegocios();

            VendaCabecalhoCollections vendaCabecalhoCollectionsAtendente = vendaCabecalhoNegocios.GetAtendenteVenda();
            GruposCollections gruposCollections = gruposNegocios.ConsultarDescricao("%");

            cbAtendente.DataSource = null;
            cbAtendente.DataSource = vendaCabecalhoCollectionsAtendente;

            cbGrupos.DataSource = null;
            cbGrupos.DataSource = gruposCollections;

            cbAtendente.Enabled = false;
            dtpAtendenteInicial.Enabled = false;
            dtpAtendenteFinal.Enabled = false;

            cbGrupos.Enabled = false;
            dtpGrupoInicial.Enabled = false;
            dtpGrupoFinal.Enabled = false;
            dtpProdutoInicial.Enabled = false;
            dtpProdutoFinal.Enabled = false;

        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (rbGeral.Checked)
            {
                frmRltVendas objVendas = new frmRltVendas(relatorioVendas.VendaGeral, dtpGeralInicial.Value, dtpGeralFinal.Value, meuLanche);
                objVendas.ShowDialog();
            }
            else if (rbAtendente.Checked)
            {
                frmRltVendas objVendas = new frmRltVendas(relatorioVendas.VendaAtendente, cbAtendente.Text.ToUpper(), dtpAtendenteInicial.Value, dtpAtendenteFinal.Value, meuLanche);
                objVendas.ShowDialog();
            }
            else if (rbGrupos.Checked)
            {
                frmRltVendas objVendas = new frmRltVendas(relatorioVendas.VendaGrupos, Convert.ToInt32(cbGrupos.SelectedValue), dtpGrupoInicial.Value, dtpGrupoFinal.Value, meuLanche);
                objVendas.ShowDialog();
            }
            else if (rbProduto.Checked)
            {
                try
                {
                    int codProd = Convert.ToInt32(txtCodigoProduto.Text);

                    frmRltVendas objVendas = new frmRltVendas(relatorioVendas.VendaCodigoProduto, codProd, dtpProdutoInicial.Value, dtpProdutoFinal.Value, meuLanche);
                    objVendas.ShowDialog();
                }
                catch (Exception)
                {
                    MessageBox.Show("É Obrigatório somente numeros no campo Codigo do Produto", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCodigoProduto.Focus();
                }

            }

        }

        private void dtpGeralInicial_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }
    }
}
