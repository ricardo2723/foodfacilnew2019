﻿namespace Relatorios
{
    public enum relatorioPessoas
    {
        Clientes = 1,
        Usuarios = 2,
        Fornecedores = 3,
        EntregadoresGeral = 4,
        EntregadoresAtivos = 5,
        EntregadoresDesligados = 6,
    }

    public enum relatorioProdutos
    {
        ProdutoGeral = 1,
        ProdutoMinimo = 2,
        MaisVendido = 3,
        PorGrupo = 4,
        EntradaGeral = 5,
        EntradaCodigo = 6,
        SaidaGeral = 7,
        SaidaCodigo = 8,
        ProdutoMinGrupo = 9
    }

    public enum relatorioVendas
    {
        VendaGeral = 1,
        VendaAtendente = 2,
        VendaGrupos = 3,
        VendaCodigoProduto = 4
    }

    public enum relatorioDelivery
    {
        DeliveryGeral = 1,
        DeliveryEntregasDiarias = 2,
        DeliveryEntregasMensal = 3,
        DeliveryEntregador = 4,
        DeliveryEntregadorGeral = 5,
        DeliveryCancelada = 6
    }

    public enum relatorioRetirada
    {
        RetiradaGeral = 1,
        RetiradaUsuario = 2
    }

    public enum imprimiCupom
    {
        imprime = 1,
        reimprime = 2,
        imprimeDelivery = 3,
        reimprimeDelivery = 4,
        imprimePedido = 5
    }

    public enum relatorioContas
    {
        Geral = 1,
        Pagas = 2,
        EmAberto = 3,
        Vencida = 4
    }

    public enum GraficosVendas
    {
        Diaria = 1,
        Mensal = 2,
        InLocDelivery = 3,
        Canceladas = 4
    }

    public enum GraficosEntregas
    {
        Diaria = 1,
        Mensal = 2,
        Canceladas = 3,
        Entregador = 4,
        Tempo = 5
    }
}
