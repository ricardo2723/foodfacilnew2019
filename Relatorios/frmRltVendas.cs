﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltVendas : Form
    {
        MeuLanche meuLanche = new MeuLanche();

        relatorioVendas tipo;
        DateTime dataInicial, dataFinal;
        string atendente;
        int grupoCod;

        public frmRltVendas(relatorioVendas getTipo, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;

            dataInicial = inicial;
            dataFinal = final;
        }

        public frmRltVendas(relatorioVendas getTipo, string nomeAtendente, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            atendente = nomeAtendente;
            dataInicial = inicial;
            dataFinal = final;
        }

        public frmRltVendas(relatorioVendas getTipo, int grupoId, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            grupoCod = grupoId;
            dataInicial = inicial;
            dataFinal = final;
        }

        private void frmRltVendas_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case relatorioVendas.VendaGeral:
                    vendaGeral();
                    break;
                case relatorioVendas.VendaAtendente:
                    vendaAtendente();
                    break;
                case relatorioVendas.VendaGrupos:
                    vendaGrupos();
                    break;
                case relatorioVendas.VendaCodigoProduto:
                    vendaCodProduto();
                    break;
            }
        }

        private void vendaGeral()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.rltVendasGeral.rdlc";

            rptvVendas.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "VendasGeral";
            reportDataSource.Value = this.VendasGeralBindingSource;
            this.rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            this.vendasGeralTableAdapter.VendasGeral(this.dsRelatorios.VendasGeral, dataInicial, dataFinal);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }

        private void vendaAtendente()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[4];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("atendente", atendente);
            p[3] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.rltVendasAtendente.rdlc";

            rptvVendas.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "VendasAtendente";
            reportDataSource.Value = this.VendasAtendenteBindingSource;
            this.rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            this.vendasAtendenteTableAdapter.VendasAtendente(this.dsRelatorios.VendasAtendente, atendente, dataInicial, dataFinal);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }

        private void vendaGrupos()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.rltVendasGrupo.rdlc";

            rptvVendas.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "VendaGrupo";
            reportDataSource.Value = this.VendasGrupoBindingSource;
            this.rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            this.vendasGrupoTableAdapter.VendaGrupo(this.dsRelatorios.VendasGrupo, grupoCod, dataInicial, dataFinal);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }

        private void vendaCodProduto()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.rltVendasProduto.rdlc";

            rptvVendas.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "VendasProduto";
            reportDataSource.Value = this.VendasProdutoBindingSource;
            this.rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            this.vendasProdutoTableAdapter.VendasProduto(this.dsRelatorios.VendasProduto, grupoCod, dataInicial, dataFinal);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }
    }
}
