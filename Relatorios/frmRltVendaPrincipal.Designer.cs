﻿namespace Relatorios
{
    partial class frmRltVendaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltVendaPrincipal));
            this.btnCarregaRelatorio = new DevComponents.DotNetBar.ButtonX();
            this.gpEntradaSaida = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtCodigoProduto = new System.Windows.Forms.TextBox();
            this.dtpProdutoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpProdutoFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.rbProduto = new System.Windows.Forms.RadioButton();
            this.cbGrupos = new System.Windows.Forms.ComboBox();
            this.dtpGrupoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpGrupoFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.label = new DevComponents.DotNetBar.LabelX();
            this.rbGrupos = new System.Windows.Forms.RadioButton();
            this.cbAtendente = new System.Windows.Forms.ComboBox();
            this.dtpGeralFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpGeralInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpAtendenteInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpAtendenteFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.rbAtendente = new System.Windows.Forms.RadioButton();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rbGeral = new System.Windows.Forms.RadioButton();
            this.gpEntradaSaida.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarregaRelatorio
            // 
            this.btnCarregaRelatorio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCarregaRelatorio.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCarregaRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregaRelatorio.Location = new System.Drawing.Point(666, 278);
            this.btnCarregaRelatorio.Name = "btnCarregaRelatorio";
            this.btnCarregaRelatorio.Size = new System.Drawing.Size(113, 23);
            this.btnCarregaRelatorio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCarregaRelatorio.TabIndex = 8;
            this.btnCarregaRelatorio.Text = "Carregar Relatório";
            this.btnCarregaRelatorio.Click += new System.EventHandler(this.btnCarregaRelatorio_Click);
            // 
            // gpEntradaSaida
            // 
            this.gpEntradaSaida.BackColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpEntradaSaida.Controls.Add(this.txtCodigoProduto);
            this.gpEntradaSaida.Controls.Add(this.dtpProdutoInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpProdutoFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX9);
            this.gpEntradaSaida.Controls.Add(this.labelX10);
            this.gpEntradaSaida.Controls.Add(this.labelX11);
            this.gpEntradaSaida.Controls.Add(this.rbProduto);
            this.gpEntradaSaida.Controls.Add(this.cbGrupos);
            this.gpEntradaSaida.Controls.Add(this.dtpGrupoInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpGrupoFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX6);
            this.gpEntradaSaida.Controls.Add(this.labelX7);
            this.gpEntradaSaida.Controls.Add(this.label);
            this.gpEntradaSaida.Controls.Add(this.rbGrupos);
            this.gpEntradaSaida.Controls.Add(this.cbAtendente);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralFinal);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpAtendenteInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpAtendenteFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX5);
            this.gpEntradaSaida.Controls.Add(this.labelX3);
            this.gpEntradaSaida.Controls.Add(this.labelX4);
            this.gpEntradaSaida.Controls.Add(this.rbAtendente);
            this.gpEntradaSaida.Controls.Add(this.labelX2);
            this.gpEntradaSaida.Controls.Add(this.labelX1);
            this.gpEntradaSaida.Controls.Add(this.rbGeral);
            this.gpEntradaSaida.Location = new System.Drawing.Point(6, 16);
            this.gpEntradaSaida.Name = "gpEntradaSaida";
            this.gpEntradaSaida.Size = new System.Drawing.Size(773, 256);
            // 
            // 
            // 
            this.gpEntradaSaida.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpEntradaSaida.Style.BackColorGradientAngle = 90;
            this.gpEntradaSaida.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpEntradaSaida.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderBottomWidth = 1;
            this.gpEntradaSaida.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpEntradaSaida.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderLeftWidth = 1;
            this.gpEntradaSaida.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderRightWidth = 1;
            this.gpEntradaSaida.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderTopWidth = 1;
            this.gpEntradaSaida.Style.CornerDiameter = 4;
            this.gpEntradaSaida.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpEntradaSaida.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpEntradaSaida.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpEntradaSaida.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpEntradaSaida.TabIndex = 7;
            // 
            // txtCodigoProduto
            // 
            this.txtCodigoProduto.Location = new System.Drawing.Point(132, 211);
            this.txtCodigoProduto.Name = "txtCodigoProduto";
            this.txtCodigoProduto.Size = new System.Drawing.Size(175, 20);
            this.txtCodigoProduto.TabIndex = 28;
            this.txtCodigoProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpProdutoInicial
            // 
            this.dtpProdutoInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpProdutoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProdutoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpProdutoInicial.Location = new System.Drawing.Point(426, 211);
            this.dtpProdutoInicial.Name = "dtpProdutoInicial";
            this.dtpProdutoInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpProdutoInicial.TabIndex = 23;
            this.dtpProdutoInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpProdutoFinal
            // 
            this.dtpProdutoFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpProdutoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProdutoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpProdutoFinal.Location = new System.Drawing.Point(638, 211);
            this.dtpProdutoFinal.Name = "dtpProdutoFinal";
            this.dtpProdutoFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpProdutoFinal.TabIndex = 25;
            this.dtpProdutoFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Blue;
            this.labelX9.Location = new System.Drawing.Point(42, 210);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(96, 23);
            this.labelX9.TabIndex = 27;
            this.labelX9.Text = "Código Produto:";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.Blue;
            this.labelX10.Location = new System.Drawing.Point(572, 210);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(75, 23);
            this.labelX10.TabIndex = 26;
            this.labelX10.Text = "Data Final:";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.Blue;
            this.labelX11.Location = new System.Drawing.Point(354, 210);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(75, 23);
            this.labelX11.TabIndex = 24;
            this.labelX11.Text = "Data Inicial:";
            // 
            // rbProduto
            // 
            this.rbProduto.AutoSize = true;
            this.rbProduto.BackColor = System.Drawing.Color.Transparent;
            this.rbProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbProduto.Location = new System.Drawing.Point(14, 179);
            this.rbProduto.Name = "rbProduto";
            this.rbProduto.Size = new System.Drawing.Size(68, 19);
            this.rbProduto.TabIndex = 22;
            this.rbProduto.Text = "Produto";
            this.rbProduto.UseVisualStyleBackColor = false;
            this.rbProduto.Click += new System.EventHandler(this.rbProduto_Click);
            // 
            // cbGrupos
            // 
            this.cbGrupos.DisplayMember = "grupoDescricao";
            this.cbGrupos.FormattingEnabled = true;
            this.cbGrupos.Location = new System.Drawing.Point(84, 151);
            this.cbGrupos.Name = "cbGrupos";
            this.cbGrupos.Size = new System.Drawing.Size(175, 21);
            this.cbGrupos.TabIndex = 21;
            this.cbGrupos.ValueMember = "grupoId";
            this.cbGrupos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGrupoInicial
            // 
            this.dtpGrupoInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGrupoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGrupoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGrupoInicial.Location = new System.Drawing.Point(426, 151);
            this.dtpGrupoInicial.Name = "dtpGrupoInicial";
            this.dtpGrupoInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpGrupoInicial.TabIndex = 16;
            this.dtpGrupoInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGrupoFinal
            // 
            this.dtpGrupoFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGrupoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGrupoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGrupoFinal.Location = new System.Drawing.Point(638, 151);
            this.dtpGrupoFinal.Name = "dtpGrupoFinal";
            this.dtpGrupoFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpGrupoFinal.TabIndex = 18;
            this.dtpGrupoFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.Blue;
            this.labelX6.Location = new System.Drawing.Point(42, 150);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(120, 23);
            this.labelX6.TabIndex = 20;
            this.labelX6.Text = "Grupo:";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.Blue;
            this.labelX7.Location = new System.Drawing.Point(572, 150);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(75, 23);
            this.labelX7.TabIndex = 19;
            this.labelX7.Text = "Data Final:";
            // 
            // label
            // 
            this.label.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.label.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.ForeColor = System.Drawing.Color.Blue;
            this.label.Location = new System.Drawing.Point(354, 150);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(75, 23);
            this.label.TabIndex = 17;
            this.label.Text = "Data Inicial:";
            // 
            // rbGrupos
            // 
            this.rbGrupos.AutoSize = true;
            this.rbGrupos.BackColor = System.Drawing.Color.Transparent;
            this.rbGrupos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGrupos.Location = new System.Drawing.Point(14, 115);
            this.rbGrupos.Name = "rbGrupos";
            this.rbGrupos.Size = new System.Drawing.Size(65, 19);
            this.rbGrupos.TabIndex = 15;
            this.rbGrupos.Text = "Grupos";
            this.rbGrupos.UseVisualStyleBackColor = false;
            this.rbGrupos.Click += new System.EventHandler(this.rbGrupos_Click);
            // 
            // cbAtendente
            // 
            this.cbAtendente.DisplayMember = "atend";
            this.cbAtendente.FormattingEnabled = true;
            this.cbAtendente.Location = new System.Drawing.Point(160, 87);
            this.cbAtendente.Name = "cbAtendente";
            this.cbAtendente.Size = new System.Drawing.Size(175, 21);
            this.cbAtendente.TabIndex = 14;
            this.cbAtendente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralFinal
            // 
            this.dtpGeralFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralFinal.Location = new System.Drawing.Point(326, 32);
            this.dtpGeralFinal.Name = "dtpGeralFinal";
            this.dtpGeralFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralFinal.TabIndex = 3;
            this.dtpGeralFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralInicial
            // 
            this.dtpGeralInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralInicial.Location = new System.Drawing.Point(113, 32);
            this.dtpGeralInicial.Name = "dtpGeralInicial";
            this.dtpGeralInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralInicial.TabIndex = 1;
            this.dtpGeralInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpAtendenteInicial
            // 
            this.dtpAtendenteInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpAtendenteInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAtendenteInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAtendenteInicial.Location = new System.Drawing.Point(426, 87);
            this.dtpAtendenteInicial.Name = "dtpAtendenteInicial";
            this.dtpAtendenteInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpAtendenteInicial.TabIndex = 8;
            this.dtpAtendenteInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpAtendenteFinal
            // 
            this.dtpAtendenteFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpAtendenteFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAtendenteFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAtendenteFinal.Location = new System.Drawing.Point(638, 87);
            this.dtpAtendenteFinal.Name = "dtpAtendenteFinal";
            this.dtpAtendenteFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpAtendenteFinal.TabIndex = 10;
            this.dtpAtendenteFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Blue;
            this.labelX5.Location = new System.Drawing.Point(42, 86);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(120, 23);
            this.labelX5.TabIndex = 13;
            this.labelX5.Text = "Nome do Atendente:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Blue;
            this.labelX3.Location = new System.Drawing.Point(572, 86);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 11;
            this.labelX3.Text = "Data Final:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Blue;
            this.labelX4.Location = new System.Drawing.Point(354, 86);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 9;
            this.labelX4.Text = "Data Inicial:";
            // 
            // rbAtendente
            // 
            this.rbAtendente.AutoSize = true;
            this.rbAtendente.BackColor = System.Drawing.Color.Transparent;
            this.rbAtendente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAtendente.Location = new System.Drawing.Point(14, 60);
            this.rbAtendente.Name = "rbAtendente";
            this.rbAtendente.Size = new System.Drawing.Size(80, 19);
            this.rbAtendente.TabIndex = 7;
            this.rbAtendente.Text = "Atendente";
            this.rbAtendente.UseVisualStyleBackColor = false;
            this.rbAtendente.Click += new System.EventHandler(this.rbAtendente_Click);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Blue;
            this.labelX2.Location = new System.Drawing.Point(260, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "Data Final:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Blue;
            this.labelX1.Location = new System.Drawing.Point(42, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Data Inicial:";
            // 
            // rbGeral
            // 
            this.rbGeral.AutoSize = true;
            this.rbGeral.BackColor = System.Drawing.Color.Transparent;
            this.rbGeral.Checked = true;
            this.rbGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGeral.Location = new System.Drawing.Point(14, 3);
            this.rbGeral.Name = "rbGeral";
            this.rbGeral.Size = new System.Drawing.Size(55, 19);
            this.rbGeral.TabIndex = 0;
            this.rbGeral.TabStop = true;
            this.rbGeral.Text = "Geral";
            this.rbGeral.UseVisualStyleBackColor = false;
            this.rbGeral.Click += new System.EventHandler(this.rbGeral_Click);
            // 
            // frmRltVendaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 307);
            this.Controls.Add(this.btnCarregaRelatorio);
            this.Controls.Add(this.gpEntradaSaida);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltVendaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Vendas";
            this.Load += new System.EventHandler(this.frmRltVendaPrincipal_Load);
            this.gpEntradaSaida.ResumeLayout(false);
            this.gpEntradaSaida.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCarregaRelatorio;
        private DevComponents.DotNetBar.Controls.GroupPanel gpEntradaSaida;
        private System.Windows.Forms.DateTimePicker dtpGeralFinal;
        private System.Windows.Forms.DateTimePicker dtpGeralInicial;
        private System.Windows.Forms.DateTimePicker dtpAtendenteInicial;
        private System.Windows.Forms.DateTimePicker dtpAtendenteFinal;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.RadioButton rbAtendente;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.RadioButton rbGeral;
        private System.Windows.Forms.ComboBox cbAtendente;
        private System.Windows.Forms.DateTimePicker dtpProdutoInicial;
        private System.Windows.Forms.DateTimePicker dtpProdutoFinal;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.RadioButton rbProduto;
        private System.Windows.Forms.ComboBox cbGrupos;
        private System.Windows.Forms.DateTimePicker dtpGrupoInicial;
        private System.Windows.Forms.DateTimePicker dtpGrupoFinal;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX label;
        private System.Windows.Forms.RadioButton rbGrupos;
        private System.Windows.Forms.TextBox txtCodigoProduto;

    }
}