﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltImprimirCaixa : Form
    {
        public frmRltImprimirCaixa(Caixa caixa, MeuLanche meuLanche)
        {
            InitializeComponent();

            ReportParameter[] p = new ReportParameter[19];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("codigoCaixa", caixa.idCaixa.ToString());
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("dataAbre", caixa.dataCaixa.ToString("dd/MM/yyyy"));
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("abertoPor", caixa.nomeUsuario);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("retirada", caixa.valorRetirada.ToString());
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("dinheiro", caixa.dinheiro.ToString());
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("cartao", caixa.cartao.ToString());
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("desconto", caixa.desconto.ToString());
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("ticket", caixa.ticket.ToString());
            p[8] = new Microsoft.Reporting.WinForms.ReportParameter("valorVendas", caixa.valorCaixaFecha.ToString());
            p[9] = new Microsoft.Reporting.WinForms.ReportParameter("valorCaixa", caixa.totalCaixa.ToString());
            p[10] = new Microsoft.Reporting.WinForms.ReportParameter("dataHoraFecha", caixa.horaCaixaFecha.ToString());
            p[11] = new Microsoft.Reporting.WinForms.ReportParameter("abertoCom", caixa.abertoCom.ToString());
            p[12] = new Microsoft.Reporting.WinForms.ReportParameter("horaAbre", caixa.horaCaixaAbre.ToString("HH:MM:ss"));
            p[13] = new Microsoft.Reporting.WinForms.ReportParameter("nomeEmpresa", meuLanche.nomeMeuLanche);
            p[14] = new Microsoft.Reporting.WinForms.ReportParameter("endereco", meuLanche.enderecoMeuLanche + " " + meuLanche.complementoMeuLanche + " " + meuLanche.bairroMeuLanche);
            p[15] = new Microsoft.Reporting.WinForms.ReportParameter("cidade", meuLanche.municipioMeuLanche);
            p[16] = new Microsoft.Reporting.WinForms.ReportParameter("uf", meuLanche.ufMeuLanche);
            p[17] = new Microsoft.Reporting.WinForms.ReportParameter("cnpj", meuLanche.cnpjMeuLanche);
            p[18] = new Microsoft.Reporting.WinForms.ReportParameter("ie", meuLanche.inscricaoEstadualMeuLanche);

            rptvImprimirCaixa.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirCaixa.rdlc";
            rptvImprimirCaixa.LocalReport.SetParameters(p);
        }

        private void frmRltImprimirCaixa_Load(object sender, EventArgs e)
        {
            rptvImprimirCaixa.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvImprimirCaixa.RefreshReport();                                   
        }
            
    }
}
