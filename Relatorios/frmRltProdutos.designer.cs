﻿namespace Relatorios
{
    partial class frmRltProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltProdutos));
            this.rptvProdutos = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.ProdutoGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProdutoGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoGeralTableAdapter();
            this.ProdutoMinimoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProdutoMinimoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoMinimoTableAdapter();
            this.ProdutosMaisVendidoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uspRelatorioProdutoMaisVendidoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.produtoMaisVendidoTableAdapter();
            this.ProdutoPorGrupoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.produtoPorGrupoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.produtoPorGrupoTableAdapter();
            this.ProdutoEntradaGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.produtoEntradaGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoEntradaGeralTableAdapter();
            this.ProdutoEntradaCodigoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.produtoEntradaCodigoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoEntradaCodigoTableAdapter();
            this.ProdutoSaidaCodigoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProdutoSaidaGeralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.produtoSaidaCodigoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoSaidaCodigoTableAdapter();
            this.produtoSaidaGeralTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoSaidaGeralTableAdapter();
            this.ProdutoMinimoGrupoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.produtoMinimoGrupoTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ProdutoMinimoGrupoTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoMinimoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutosMaisVendidoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoPorGrupoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoEntradaGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoEntradaCodigoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoSaidaCodigoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoSaidaGeralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoMinimoGrupoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rptvProdutos
            // 
            this.rptvProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptvProdutos.Location = new System.Drawing.Point(0, 0);
            this.rptvProdutos.Name = "rptvProdutos";
            this.rptvProdutos.Size = new System.Drawing.Size(984, 473);
            this.rptvProdutos.TabIndex = 0;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ProdutoGeralBindingSource
            // 
            this.ProdutoGeralBindingSource.DataMember = "ProdutoGeral";
            this.ProdutoGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // ProdutoGeralTableAdapter
            // 
            this.ProdutoGeralTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutoMinimoBindingSource
            // 
            this.ProdutoMinimoBindingSource.DataMember = "ProdutoMinimo";
            this.ProdutoMinimoBindingSource.DataSource = this.dsRelatorios;
            // 
            // ProdutoMinimoTableAdapter
            // 
            this.ProdutoMinimoTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutosMaisVendidoBindingSource
            // 
            this.ProdutosMaisVendidoBindingSource.DataMember = "ProdutoMaisVendido";
            this.ProdutosMaisVendidoBindingSource.DataSource = this.dsRelatorios;
            // 
            // uspRelatorioProdutoMaisVendidoTableAdapter
            // 
            this.uspRelatorioProdutoMaisVendidoTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutoPorGrupoBindingSource
            // 
            this.ProdutoPorGrupoBindingSource.DataMember = "ProdutoPorGrupo";
            this.ProdutoPorGrupoBindingSource.DataSource = this.dsRelatorios;
            // 
            // produtoPorGrupoTableAdapter
            // 
            this.produtoPorGrupoTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutoEntradaGeralBindingSource
            // 
            this.ProdutoEntradaGeralBindingSource.DataMember = "ProdutoEntradaGeral";
            this.ProdutoEntradaGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // produtoEntradaGeralTableAdapter
            // 
            this.produtoEntradaGeralTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutoEntradaCodigoBindingSource
            // 
            this.ProdutoEntradaCodigoBindingSource.DataMember = "ProdutoEntradaCodigo";
            this.ProdutoEntradaCodigoBindingSource.DataSource = this.dsRelatorios;
            // 
            // produtoEntradaCodigoTableAdapter
            // 
            this.produtoEntradaCodigoTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutoSaidaCodigoBindingSource
            // 
            this.ProdutoSaidaCodigoBindingSource.DataMember = "ProdutoSaidaCodigo";
            this.ProdutoSaidaCodigoBindingSource.DataSource = this.dsRelatorios;
            // 
            // ProdutoSaidaGeralBindingSource
            // 
            this.ProdutoSaidaGeralBindingSource.DataMember = "ProdutoSaidaGeral";
            this.ProdutoSaidaGeralBindingSource.DataSource = this.dsRelatorios;
            // 
            // produtoSaidaCodigoTableAdapter
            // 
            this.produtoSaidaCodigoTableAdapter.ClearBeforeFill = true;
            // 
            // produtoSaidaGeralTableAdapter
            // 
            this.produtoSaidaGeralTableAdapter.ClearBeforeFill = true;
            // 
            // ProdutoMinimoGrupoBindingSource
            // 
            this.ProdutoMinimoGrupoBindingSource.DataMember = "ProdutoMinimoGrupo";
            this.ProdutoMinimoGrupoBindingSource.DataSource = this.dsRelatorios;
            // 
            // produtoMinimoGrupoTableAdapter
            // 
            this.produtoMinimoGrupoTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 473);
            this.Controls.Add(this.rptvProdutos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Produtos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRltProdutos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoMinimoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutosMaisVendidoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoPorGrupoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoEntradaGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoEntradaCodigoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoSaidaCodigoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoSaidaGeralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoMinimoGrupoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvProdutos;
        private System.Windows.Forms.BindingSource ProdutoGeralBindingSource;
        private dsRelatorios dsRelatorios;
        private dsRelatoriosTableAdapters.ProdutoGeralTableAdapter ProdutoGeralTableAdapter;
        private System.Windows.Forms.BindingSource ProdutoMinimoBindingSource;
        private dsRelatoriosTableAdapters.ProdutoMinimoTableAdapter ProdutoMinimoTableAdapter;
        private System.Windows.Forms.BindingSource ProdutosMaisVendidoBindingSource;
        private dsRelatoriosTableAdapters.produtoMaisVendidoTableAdapter uspRelatorioProdutoMaisVendidoTableAdapter;
        private System.Windows.Forms.BindingSource ProdutoPorGrupoBindingSource;
        private dsRelatoriosTableAdapters.produtoPorGrupoTableAdapter produtoPorGrupoTableAdapter;
        private System.Windows.Forms.BindingSource ProdutoEntradaGeralBindingSource;
        private dsRelatoriosTableAdapters.ProdutoEntradaGeralTableAdapter produtoEntradaGeralTableAdapter;
        private System.Windows.Forms.BindingSource ProdutoEntradaCodigoBindingSource;
        private dsRelatoriosTableAdapters.ProdutoEntradaCodigoTableAdapter produtoEntradaCodigoTableAdapter;
        private System.Windows.Forms.BindingSource ProdutoSaidaCodigoBindingSource;
        private System.Windows.Forms.BindingSource ProdutoSaidaGeralBindingSource;
        private dsRelatoriosTableAdapters.ProdutoSaidaCodigoTableAdapter produtoSaidaCodigoTableAdapter;
        private dsRelatoriosTableAdapters.ProdutoSaidaGeralTableAdapter produtoSaidaGeralTableAdapter;
        private System.Windows.Forms.BindingSource ProdutoMinimoGrupoBindingSource;
        private dsRelatoriosTableAdapters.ProdutoMinimoGrupoTableAdapter produtoMinimoGrupoTableAdapter;

    }
}