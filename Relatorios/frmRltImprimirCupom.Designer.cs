﻿namespace Relatorios
{
    partial class frmRltImprimirCupom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltImprimirCupom));
            this.lblMenssagem = new DevComponents.DotNetBar.LabelX();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.rptvImprimirCupom = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bgwImprimiCupom = new System.ComponentModel.BackgroundWorker();
            this.ImprimirPedidoPapelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.ImprimirCupomBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imprimirPedidosTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ImprimirPedidosTableAdapter();
            this.ImprimirDeliveryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reimprimeCupomDeliveryTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ReimprimeCupomDeliveryTableAdapter();
            this.imprimirPedidoPapelTableAdapter = new Relatorios.dsRelatoriosTableAdapters.ImprimirPedidoPapelTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirPedidoPapelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirCupomBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirDeliveryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMenssagem
            // 
            // 
            // 
            // 
            this.lblMenssagem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMenssagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenssagem.ForeColor = System.Drawing.Color.Blue;
            this.lblMenssagem.Location = new System.Drawing.Point(84, 27);
            this.lblMenssagem.Name = "lblMenssagem";
            this.lblMenssagem.SingleLineColor = System.Drawing.Color.Blue;
            this.lblMenssagem.Size = new System.Drawing.Size(271, 23);
            this.lblMenssagem.TabIndex = 3;
            // 
            // circularProgress1
            // 
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.Location = new System.Drawing.Point(12, 12);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressColor = System.Drawing.Color.Blue;
            this.circularProgress1.Size = new System.Drawing.Size(66, 52);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 2;
            // 
            // rptvImprimirCupom
            // 
            this.rptvImprimirCupom.LocalReport.ReportEmbeddedResource = "Relatorios.rltImprimirCupom.rdlc";
            this.rptvImprimirCupom.Location = new System.Drawing.Point(12, 82);
            this.rptvImprimirCupom.Name = "rptvImprimirCupom";
            this.rptvImprimirCupom.Size = new System.Drawing.Size(396, 310);
            this.rptvImprimirCupom.TabIndex = 4;
            // 
            // bgwImprimiCupom
            // 
            this.bgwImprimiCupom.WorkerReportsProgress = true;
            this.bgwImprimiCupom.WorkerSupportsCancellation = true;
            this.bgwImprimiCupom.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwImprimiCupom_DoWork);
            this.bgwImprimiCupom.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwImprimiCupom_ProgressChanged);
            this.bgwImprimiCupom.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwImprimiCupom_RunWorkerCompleted);
            // 
            // ImprimirPedidoPapelBindingSource
            // 
            this.ImprimirPedidoPapelBindingSource.DataMember = "ImprimirPedidoPapel";
            this.ImprimirPedidoPapelBindingSource.DataSource = this.dsRelatorios;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ImprimirCupomBindingSource
            // 
            this.ImprimirCupomBindingSource.DataMember = "ImprimirPedidos";
            this.ImprimirCupomBindingSource.DataSource = this.dsRelatorios;
            // 
            // imprimirPedidosTableAdapter
            // 
            this.imprimirPedidosTableAdapter.ClearBeforeFill = true;
            // 
            // ImprimirDeliveryBindingSource
            // 
            this.ImprimirDeliveryBindingSource.DataMember = "ReimprimeCupomDelivery";
            this.ImprimirDeliveryBindingSource.DataSource = this.dsRelatorios;
            // 
            // reimprimeCupomDeliveryTableAdapter
            // 
            this.reimprimeCupomDeliveryTableAdapter.ClearBeforeFill = true;
            // 
            // imprimirPedidoPapelTableAdapter
            // 
            this.imprimirPedidoPapelTableAdapter.ClearBeforeFill = true;
            // 
            // frmRltImprimirCupom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 71);
            this.Controls.Add(this.rptvImprimirCupom);
            this.Controls.Add(this.lblMenssagem);
            this.Controls.Add(this.circularProgress1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltImprimirCupom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Imprimir Cupom";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRltImprimirCupom_FormClosed);
            this.Load += new System.EventHandler(this.frmRltImprimirCupom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirPedidoPapelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirCupomBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImprimirDeliveryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblMenssagem;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private Microsoft.Reporting.WinForms.ReportViewer rptvImprimirCupom;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource ImprimirCupomBindingSource;
        private dsRelatoriosTableAdapters.ImprimirPedidosTableAdapter imprimirPedidosTableAdapter;
        private System.ComponentModel.BackgroundWorker bgwImprimiCupom;
        private System.Windows.Forms.BindingSource ImprimirDeliveryBindingSource;
        private dsRelatoriosTableAdapters.ReimprimeCupomDeliveryTableAdapter reimprimeCupomDeliveryTableAdapter;
        private System.Windows.Forms.BindingSource ImprimirPedidoPapelBindingSource;
        private dsRelatoriosTableAdapters.ImprimirPedidoPapelTableAdapter imprimirPedidoPapelTableAdapter;
    }
}