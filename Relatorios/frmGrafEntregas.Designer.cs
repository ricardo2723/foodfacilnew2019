﻿namespace Relatorios
{
    partial class frmGrafEntregas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGrafEntregas));
            this.GraficoEntregasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsRelatorios = new Relatorios.dsRelatorios();
            this.GraficoVendasCanceladasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rptvEntregas = new Microsoft.Reporting.WinForms.ReportViewer();
            this.GraficoInlocDeliveryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GraficoVendasMensalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GraficoVendasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GraficoVendasTableAdapter = new Relatorios.dsRelatoriosTableAdapters.GraficoVendasTableAdapter();
            this.GraficoVendasMensalTableAdapter = new Relatorios.dsRelatoriosTableAdapters.GraficoVendasMensalTableAdapter();
            this.GraficoInlocDeliveryTableAdapter = new Relatorios.dsRelatoriosTableAdapters.GraficoInlocDeliveryTableAdapter();
            this.GraficoVendasCanceladasTableAdapter = new Relatorios.dsRelatoriosTableAdapters.GraficoVendasCanceladasTableAdapter();
            this.GraficoEntregasTableAdapter = new Relatorios.dsRelatoriosTableAdapters.GraficoEntregasTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoEntregasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoVendasCanceladasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoInlocDeliveryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoVendasMensalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoVendasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // GraficoEntregasBindingSource
            // 
            this.GraficoEntregasBindingSource.DataMember = "GraficoEntregas";
            this.GraficoEntregasBindingSource.DataSource = this.dsRelatorios;
            // 
            // dsRelatorios
            // 
            this.dsRelatorios.DataSetName = "dsRelatorios";
            this.dsRelatorios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // GraficoVendasCanceladasBindingSource
            // 
            this.GraficoVendasCanceladasBindingSource.DataMember = "GraficoVendasCanceladas";
            this.GraficoVendasCanceladasBindingSource.DataSource = this.dsRelatorios;
            // 
            // rptvEntregas
            // 
            this.rptvEntregas.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "GraficosEntregas";
            reportDataSource1.Value = this.GraficoEntregasBindingSource;
            this.rptvEntregas.LocalReport.DataSources.Add(reportDataSource1);
            this.rptvEntregas.LocalReport.ReportEmbeddedResource = "Relatorios.grafEntregasDiarias.rdlc";
            this.rptvEntregas.Location = new System.Drawing.Point(0, 0);
            this.rptvEntregas.Name = "rptvEntregas";
            this.rptvEntregas.Size = new System.Drawing.Size(984, 473);
            this.rptvEntregas.TabIndex = 0;
            // 
            // GraficoInlocDeliveryBindingSource
            // 
            this.GraficoInlocDeliveryBindingSource.DataMember = "GraficoInlocDelivery";
            this.GraficoInlocDeliveryBindingSource.DataSource = this.dsRelatorios;
            // 
            // GraficoVendasMensalBindingSource
            // 
            this.GraficoVendasMensalBindingSource.DataMember = "GraficoVendasMensal";
            this.GraficoVendasMensalBindingSource.DataSource = this.dsRelatorios;
            // 
            // GraficoVendasBindingSource
            // 
            this.GraficoVendasBindingSource.DataMember = "GraficoVendas";
            this.GraficoVendasBindingSource.DataSource = this.dsRelatorios;
            // 
            // GraficoVendasTableAdapter
            // 
            this.GraficoVendasTableAdapter.ClearBeforeFill = true;
            // 
            // GraficoVendasMensalTableAdapter
            // 
            this.GraficoVendasMensalTableAdapter.ClearBeforeFill = true;
            // 
            // GraficoInlocDeliveryTableAdapter
            // 
            this.GraficoInlocDeliveryTableAdapter.ClearBeforeFill = true;
            // 
            // GraficoVendasCanceladasTableAdapter
            // 
            this.GraficoVendasCanceladasTableAdapter.ClearBeforeFill = true;
            // 
            // GraficoEntregasTableAdapter
            // 
            this.GraficoEntregasTableAdapter.ClearBeforeFill = true;
            // 
            // frmGrafEntregas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 473);
            this.Controls.Add(this.rptvEntregas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGrafEntregas";
            this.Text = "DRSis - Lanche Fácil - Gráficos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmGrafEntregas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GraficoEntregasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoVendasCanceladasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoInlocDeliveryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoVendasMensalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraficoVendasBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptvEntregas;
        private dsRelatorios dsRelatorios;
        private System.Windows.Forms.BindingSource GraficoVendasBindingSource;
        private dsRelatoriosTableAdapters.GraficoVendasTableAdapter GraficoVendasTableAdapter;
        private System.Windows.Forms.BindingSource GraficoVendasMensalBindingSource;
        private dsRelatoriosTableAdapters.GraficoVendasMensalTableAdapter GraficoVendasMensalTableAdapter;
        private System.Windows.Forms.BindingSource GraficoInlocDeliveryBindingSource;
        private dsRelatoriosTableAdapters.GraficoInlocDeliveryTableAdapter GraficoInlocDeliveryTableAdapter;
        private System.Windows.Forms.BindingSource GraficoVendasCanceladasBindingSource;
        private dsRelatoriosTableAdapters.GraficoVendasCanceladasTableAdapter GraficoVendasCanceladasTableAdapter;
        private System.Windows.Forms.BindingSource GraficoEntregasBindingSource;
        private dsRelatoriosTableAdapters.GraficoEntregasTableAdapter GraficoEntregasTableAdapter;            
    }
}