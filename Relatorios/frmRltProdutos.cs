﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltProdutos : Form
    {
        MeuLanche meuLanche = new MeuLanche();
        relatorioProdutos tipo;
        DateTime dataInicial, dataFinal;
        int grupo, codi;

        public frmRltProdutos(relatorioProdutos getTipo, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
        }

        public frmRltProdutos(relatorioProdutos getTipo, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            dataInicial = inicial;
            dataFinal = final;
        }

        public frmRltProdutos(relatorioProdutos getTipo, int cod, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            codi = cod;
            dataInicial = inicial;
            dataFinal = final;
        }

        public frmRltProdutos(relatorioProdutos getTipo, int grupoId, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            grupo = grupoId;
            meuLanche = meulanche;
        }

        private void frmRltProdutos_Load(object sender, EventArgs e)        
        {
            switch (tipo)
            {
                case relatorioProdutos.ProdutoGeral:
                    ProdutoGeral();
                    break;
                case relatorioProdutos.ProdutoMinimo:
                    produtoMinimo();
                    break;
                case relatorioProdutos.MaisVendido:
                    produtoMaisVendido();
                    break;
                case relatorioProdutos.PorGrupo:
                    produtoPorGrupo();
                    break;
                case relatorioProdutos.EntradaGeral:
                    produtoEntradaGeral();
                    break;
                case relatorioProdutos.EntradaCodigo:
                    produtoEntradaCodigo();
                    break;
                case relatorioProdutos.SaidaCodigo:
                    produtoSaidaCodigo();
                    break;
                case relatorioProdutos.SaidaGeral:
                    produtoSaidaGeral();
                    break;
                case relatorioProdutos.ProdutoMinGrupo:
                    produtoMinimoGrupo();
                    break;
            }
                  
        }

        private void ProdutoGeral()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            
 
            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoGeral";
            reportDataSource.Value = this.ProdutoGeralBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosGeral.rdlc";
            this.ProdutoGeralTableAdapter.ProdutoGeral(this.dsRelatorios.ProdutoGeral);
            rptvProdutos.LocalReport.SetParameters(p);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);
            
            this.rptvProdutos.RefreshReport();
        }

        private void produtoMinimo()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            
            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoMinimo";
            reportDataSource.Value = this.ProdutoMinimoBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosMinimo.rdlc";
            this.ProdutoMinimoTableAdapter.ProdutoMinimo(this.dsRelatorios.ProdutoMinimo);
            rptvProdutos.LocalReport.SetParameters(p);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);
            
            this.rptvProdutos.RefreshReport();
        }

        private void produtoMinimoGrupo()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoMinimoGrupo";
            reportDataSource.Value = this.ProdutoMinimoGrupoBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosMinimoGrupo.rdlc";
            this.produtoMinimoGrupoTableAdapter.ProdutoMinimoGrupo(this.dsRelatorios.ProdutoMinimoGrupo, grupo);
            rptvProdutos.LocalReport.SetParameters(p);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        private void produtoMaisVendido()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            
            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoMaisVendido";
            reportDataSource.Value = this.ProdutosMaisVendidoBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosMaisVendido.rdlc";
            this.uspRelatorioProdutoMaisVendidoTableAdapter.ProdutoMaisVendido(this.dsRelatorios.ProdutoMaisVendido, dataInicial, dataFinal);
            rptvProdutos.LocalReport.SetParameters(p);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        private void produtoPorGrupo()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[1];
            p[0] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            
            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoPorGrupo";
            reportDataSource.Value = this.ProdutoPorGrupoBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosPorGrupo.rdlc";
            this.produtoPorGrupoTableAdapter.ProdutoPorGrupo(this.dsRelatorios.ProdutoPorGrupo, grupo);
            rptvProdutos.LocalReport.SetParameters(p);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        private void produtoEntradaGeral()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosEntradaGeral.rdlc";            
            rptvProdutos.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoEntradaGeral";
            reportDataSource.Value = this.ProdutoEntradaGeralBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.produtoEntradaGeralTableAdapter.ProdutoEntradaGeral(this.dsRelatorios.ProdutoEntradaGeral, dataInicial, dataFinal);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        private void produtoEntradaCodigo()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosEntradaCodigo.rdlc";
            
            rptvProdutos.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoEntradaCodigo";
            reportDataSource.Value = this.ProdutoEntradaCodigoBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.produtoEntradaCodigoTableAdapter.ProdutoEntradaCodigo(this.dsRelatorios.ProdutoEntradaCodigo, codi, dataInicial, dataFinal);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        private void produtoSaidaGeral()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosSaidaGeral.rdlc";

            rptvProdutos.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoSaidaGeral";
            reportDataSource.Value = this.ProdutoSaidaGeralBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.produtoSaidaGeralTableAdapter.ProdutoSaidaGeral(this.dsRelatorios.ProdutoSaidaGeral, dataInicial, dataFinal);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        private void produtoSaidaCodigo()
        {
            rptvProdutos.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvProdutos.LocalReport.ReportEmbeddedResource = "Relatorios.rltProdutosSaidaCodigo.rdlc";

            rptvProdutos.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "ProdutoSaidaCodigo";
            reportDataSource.Value = this.ProdutoSaidaCodigoBindingSource;
            this.rptvProdutos.LocalReport.DataSources.Add(reportDataSource);
            this.produtoSaidaCodigoTableAdapter.ProdutoSaidaCodigo(this.dsRelatorios.ProdutoSaidaCodigo, codi, dataInicial, dataFinal);
            rptvProdutos.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvProdutos.RefreshReport();
        }

        
    }
}
