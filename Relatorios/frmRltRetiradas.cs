﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltRetiradas : Form
    {
        relatorioRetirada tipo;
        MeuLanche meuLanche = new MeuLanche();

        DateTime dataInicial, dataFinal;
        int usuario;

        public frmRltRetiradas(relatorioRetirada getTipo, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            dataInicial = inicial;
            dataFinal = final;
        }

        public frmRltRetiradas(relatorioRetirada getTipo, int idUsuario, DateTime inicial, DateTime final, MeuLanche meulanche)
        {
            InitializeComponent();

            tipo = getTipo;
            meuLanche = meulanche;
            usuario = idUsuario;
            dataInicial = inicial;
            dataFinal = final;
        }

        private void frmRltRetiradas_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case relatorioRetirada.RetiradaGeral:
                    RetiradaGeral();
                    break;
                case relatorioRetirada.RetiradaUsuario:
                    RetiradaUsuario();
                    break;
            }
        }

        private void RetiradaGeral()
        {
            rptvRetiradas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[3];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvRetiradas.LocalReport.ReportEmbeddedResource = "Relatorios.rltRetiradasGeral.rdlc";

            rptvRetiradas.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "RetiradaGeral";
            reportDataSource.Value = this.RetiradasGeralBindingSource;
            this.rptvRetiradas.LocalReport.DataSources.Add(reportDataSource);
            this.retiradaGeralTableAdapter.RetiradaGeral(this.dsRelatorios.RetiradaGeral, dataInicial, dataFinal);
            rptvRetiradas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvRetiradas.RefreshReport();
        }

        private void RetiradaUsuario()
        {
            rptvRetiradas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[4];
            p[0] = new ReportParameter("dataInicial", dataInicial.ToString("dd/MM/yyyy"));
            p[1] = new ReportParameter("dataFinal", dataFinal.ToString("dd/MM/yyyy"));
            p[2] = new ReportParameter("atendente", usuario.ToString());
            p[3] = new ReportParameter("logo", meuLanche.localLogoMeuLanche);
            this.rptvRetiradas.LocalReport.ReportEmbeddedResource = "Relatorios.rltRetiradaUsuario.rdlc";

            rptvRetiradas.LocalReport.SetParameters(p);

            ReportDataSource reportDataSource = new ReportDataSource();

            reportDataSource.Name = "RetiradasUsuarios";
            reportDataSource.Value = this.RetiradasUsuarioBindingSource;
            this.rptvRetiradas.LocalReport.DataSources.Add(reportDataSource);
            this.retiradaUsuarioTableAdapter.RetiradaUsuario(this.dsRelatorios.RetiradaUsuario, usuario, dataInicial, dataFinal);
            rptvRetiradas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvRetiradas.RefreshReport();
        }
    }
}
