﻿using System;
using System.Windows.Forms;

using Negocios;
using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmRltProdutoPorGrupo : Form
    {
        MeuLanche meuLanche = new MeuLanche();
        private int num1;

        public frmRltProdutoPorGrupo(MeuLanche meulanche)
        {
            InitializeComponent();

            meuLanche = meulanche;
            carregaGrupos();
        }

        public frmRltProdutoPorGrupo(MeuLanche meulanche, int num)
        {
            InitializeComponent();
            meuLanche = meulanche;
            num1 = num;
            carregaGrupos();

        }

        private void btnCarregaRelatorio_Click(object sender, EventArgs e)
        {
            if (num1 == 1)
            {
                frmRltProdutos objRltProdutosGeral = new frmRltProdutos(relatorioProdutos.ProdutoMinGrupo, Convert.ToInt32(cbGrupo.SelectedValue), meuLanche);
                objRltProdutosGeral.ShowDialog(); ;
            }
            else
            {
                frmRltProdutos objProdutosGrupos = new frmRltProdutos(relatorioProdutos.PorGrupo, Convert.ToInt32(cbGrupo.SelectedValue), meuLanche);
                objProdutosGrupos.ShowDialog();   
            }
            
        }

        private void carregaGrupos()
        {
            GruposNegocios gruposNegocios = new GruposNegocios();
            GruposCollections gruposCollections = gruposNegocios.ConsultarDescricao("%");

            cbGrupo.DataSource = null;
            cbGrupo.DataSource = gruposCollections;
        }

        private void cbGrupo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCarregaRelatorio.PerformClick();
                    break;
            }
        }    
   }
}
