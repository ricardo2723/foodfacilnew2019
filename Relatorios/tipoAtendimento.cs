﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relatorios
{
    public class tipoAtendimento
    {
        static public string checaTipoAtendimento(string tipo)
        {
            string retorno = "";
            string getTipo = tipo.Substring(0, 1);

            switch (getTipo)
            {
                case "M":
                    if (tipo.Length == 2)
                    {
                        retorno = "MESA 0" + tipo.Substring(1);
                    }
                    else
                    {
                        retorno = "MESA " + tipo.Substring(1);
                    }

                    break;

                case "B":
                    if (tipo.Length == 2)
                    {
                        retorno = "BALCÃO 0" + tipo.Substring(1);
                    }
                    else
                    {
                        retorno = "BALCÃO " + tipo.Substring(1);
                    }

                    break;

                case "C":
                    if (tipo.Length == 2)
                    {
                        retorno = "CARRO 0" + tipo.Substring(1);
                    }
                    else
                    {
                        retorno = "CARRO " + tipo.Substring(1);
                    }

                    break;
            }
            return retorno;
        }
    }
}
