﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmGrafEntregas : Form
    {
        ReportDataSource reportDataSource = new ReportDataSource();
        GraficosEntregas tipo;
        DateTime inicial;
        DateTime final;

        public frmGrafEntregas(GraficosEntregas getTipo, DateTime start, DateTime end)
        {
            InitializeComponent();

            tipo = getTipo;
            inicial = start;
            final = end;
        }
        
        private void frmGrafEntregas_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case GraficosEntregas.Diaria:
                    entregasDiarias();
                    break;
                case GraficosEntregas.Mensal:
                    entregasMensal();
                    break;
                case GraficosEntregas.Entregador:
                    entregasEntregador();
                    break;
                case GraficosEntregas.Tempo:
                    entregasTempo();
                    break;
            }
        }

        private void entregasDiarias()
        {
            rptvEntregas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());
            
            reportDataSource.Name = "GraficosEntregas";
            reportDataSource.Value = this.GraficoEntregasBindingSource;
            rptvEntregas.LocalReport.DataSources.Add(reportDataSource);
            rptvEntregas.LocalReport.ReportEmbeddedResource = "Relatorios.grafEntregasDiarias.rdlc";
            rptvEntregas.LocalReport.SetParameters(p);
            this.GraficoEntregasTableAdapter.EntregasDiarias(this.dsRelatorios.GraficoEntregas, inicial, final);
            rptvEntregas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvEntregas.RefreshReport();
        }

        private void entregasMensal()
        {
            rptvEntregas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "GraficosEntregas";
            reportDataSource.Value = this.GraficoEntregasBindingSource;
            rptvEntregas.LocalReport.DataSources.Add(reportDataSource);
            rptvEntregas.LocalReport.ReportEmbeddedResource = "Relatorios.grafEntregasMensal.rdlc";
            rptvEntregas.LocalReport.SetParameters(p);
            this.GraficoEntregasTableAdapter.EntregasMensal(this.dsRelatorios.GraficoEntregas, inicial, final);
            rptvEntregas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvEntregas.RefreshReport();
        }

        private void entregasEntregador()
        {
            rptvEntregas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "GraficosEntregas";
            reportDataSource.Value = this.GraficoEntregasBindingSource;
            rptvEntregas.LocalReport.DataSources.Add(reportDataSource);
            rptvEntregas.LocalReport.ReportEmbeddedResource = "Relatorios.grafEntregasEntregador.rdlc";
            rptvEntregas.LocalReport.SetParameters(p);
            this.GraficoEntregasTableAdapter.EntregasEntregador(this.dsRelatorios.GraficoEntregas, inicial, final);
            rptvEntregas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvEntregas.RefreshReport();
        }

        private void entregasTempo()
        {
            rptvEntregas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "GraficosEntregas";
            reportDataSource.Value = this.GraficoEntregasBindingSource;
            rptvEntregas.LocalReport.DataSources.Add(reportDataSource);
            rptvEntregas.LocalReport.ReportEmbeddedResource = "Relatorios.grafEntregasTempo.rdlc";
            rptvEntregas.LocalReport.SetParameters(p);
            this.GraficoEntregasTableAdapter.EntregasTempo(this.dsRelatorios.GraficoEntregas, inicial, final);
            rptvEntregas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvEntregas.RefreshReport();
        }

        private void canceladas()
        {
            rptvEntregas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "VendasCanceladas";
            reportDataSource.Value = this.GraficoVendasCanceladasBindingSource;
            rptvEntregas.LocalReport.DataSources.Add(reportDataSource);
            rptvEntregas.LocalReport.ReportEmbeddedResource = "Relatorios.grafVendasCanceladas.rdlc";
            rptvEntregas.LocalReport.SetParameters(p);
            this.GraficoVendasCanceladasTableAdapter.Fill(this.dsRelatorios.GraficoVendasCanceladas, inicial, final);
            rptvEntregas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvEntregas.RefreshReport();
        }        
    }
}
