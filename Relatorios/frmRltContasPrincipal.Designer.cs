﻿namespace Relatorios
{
    partial class frmRltContasPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltContasPrincipal));
            this.btnCarregaRelatorio = new DevComponents.DotNetBar.ButtonX();
            this.gpEntradaSaida = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.line3 = new DevComponents.DotNetBar.Controls.Line();
            this.dtpVencimento = new System.Windows.Forms.DateTimePicker();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.rbVencimento = new System.Windows.Forms.RadioButton();
            this.line2 = new DevComponents.DotNetBar.Controls.Line();
            this.line1 = new DevComponents.DotNetBar.Controls.Line();
            this.dtpPagaInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpPagaFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.label = new DevComponents.DotNetBar.LabelX();
            this.rbPagas = new System.Windows.Forms.RadioButton();
            this.dtpGeralFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpGeralInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpEmAbertoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpEmAbertoFinal = new System.Windows.Forms.DateTimePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.rbEmAberto = new System.Windows.Forms.RadioButton();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rbGeral = new System.Windows.Forms.RadioButton();
            this.gpEntradaSaida.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarregaRelatorio
            // 
            this.btnCarregaRelatorio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCarregaRelatorio.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCarregaRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregaRelatorio.Location = new System.Drawing.Point(666, 296);
            this.btnCarregaRelatorio.Name = "btnCarregaRelatorio";
            this.btnCarregaRelatorio.Size = new System.Drawing.Size(113, 23);
            this.btnCarregaRelatorio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCarregaRelatorio.TabIndex = 8;
            this.btnCarregaRelatorio.Text = "Carregar Relatório";
            this.btnCarregaRelatorio.Click += new System.EventHandler(this.btnCarregaRelatorio_Click);
            // 
            // gpEntradaSaida
            // 
            this.gpEntradaSaida.BackColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpEntradaSaida.Controls.Add(this.line3);
            this.gpEntradaSaida.Controls.Add(this.dtpVencimento);
            this.gpEntradaSaida.Controls.Add(this.labelX6);
            this.gpEntradaSaida.Controls.Add(this.rbVencimento);
            this.gpEntradaSaida.Controls.Add(this.line2);
            this.gpEntradaSaida.Controls.Add(this.line1);
            this.gpEntradaSaida.Controls.Add(this.dtpPagaInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpPagaFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX7);
            this.gpEntradaSaida.Controls.Add(this.label);
            this.gpEntradaSaida.Controls.Add(this.rbPagas);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralFinal);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpEmAbertoInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpEmAbertoFinal);
            this.gpEntradaSaida.Controls.Add(this.labelX3);
            this.gpEntradaSaida.Controls.Add(this.labelX4);
            this.gpEntradaSaida.Controls.Add(this.rbEmAberto);
            this.gpEntradaSaida.Controls.Add(this.labelX2);
            this.gpEntradaSaida.Controls.Add(this.labelX1);
            this.gpEntradaSaida.Controls.Add(this.rbGeral);
            this.gpEntradaSaida.Location = new System.Drawing.Point(6, 10);
            this.gpEntradaSaida.Name = "gpEntradaSaida";
            this.gpEntradaSaida.Size = new System.Drawing.Size(773, 280);
            // 
            // 
            // 
            this.gpEntradaSaida.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpEntradaSaida.Style.BackColorGradientAngle = 90;
            this.gpEntradaSaida.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpEntradaSaida.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderBottomWidth = 1;
            this.gpEntradaSaida.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpEntradaSaida.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderLeftWidth = 1;
            this.gpEntradaSaida.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderRightWidth = 1;
            this.gpEntradaSaida.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderTopWidth = 1;
            this.gpEntradaSaida.Style.CornerDiameter = 4;
            this.gpEntradaSaida.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpEntradaSaida.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpEntradaSaida.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpEntradaSaida.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpEntradaSaida.TabIndex = 7;
            // 
            // line3
            // 
            this.line3.ForeColor = System.Drawing.Color.Silver;
            this.line3.Location = new System.Drawing.Point(8, 197);
            this.line3.Name = "line3";
            this.line3.Size = new System.Drawing.Size(737, 8);
            this.line3.TabIndex = 27;
            this.line3.Text = "line3";
            this.line3.Thickness = 2;
            // 
            // dtpVencimento
            // 
            this.dtpVencimento.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpVencimento.Location = new System.Drawing.Point(157, 244);
            this.dtpVencimento.Name = "dtpVencimento";
            this.dtpVencimento.Size = new System.Drawing.Size(110, 21);
            this.dtpVencimento.TabIndex = 23;
            this.dtpVencimento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.Blue;
            this.labelX6.Location = new System.Drawing.Point(39, 240);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(124, 23);
            this.labelX6.TabIndex = 24;
            this.labelX6.Text = "Data do Vencimento:";
            // 
            // rbVencimento
            // 
            this.rbVencimento.AutoSize = true;
            this.rbVencimento.BackColor = System.Drawing.Color.Transparent;
            this.rbVencimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVencimento.Location = new System.Drawing.Point(11, 206);
            this.rbVencimento.Name = "rbVencimento";
            this.rbVencimento.Size = new System.Drawing.Size(90, 19);
            this.rbVencimento.TabIndex = 22;
            this.rbVencimento.Text = "Vencimento";
            this.rbVencimento.UseVisualStyleBackColor = false;
            this.rbVencimento.Click += new System.EventHandler(this.rbVencimento_Click);
            // 
            // line2
            // 
            this.line2.ForeColor = System.Drawing.Color.Silver;
            this.line2.Location = new System.Drawing.Point(11, 121);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(737, 8);
            this.line2.TabIndex = 21;
            this.line2.Text = "line2";
            this.line2.Thickness = 2;
            // 
            // line1
            // 
            this.line1.ForeColor = System.Drawing.Color.Silver;
            this.line1.Location = new System.Drawing.Point(11, 58);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(737, 8);
            this.line1.TabIndex = 20;
            this.line1.Text = "line1";
            this.line1.Thickness = 2;
            // 
            // dtpPagaInicial
            // 
            this.dtpPagaInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpPagaInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPagaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPagaInicial.Location = new System.Drawing.Point(113, 165);
            this.dtpPagaInicial.Name = "dtpPagaInicial";
            this.dtpPagaInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpPagaInicial.TabIndex = 16;
            this.dtpPagaInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpPagaFinal
            // 
            this.dtpPagaFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpPagaFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPagaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPagaFinal.Location = new System.Drawing.Point(326, 165);
            this.dtpPagaFinal.Name = "dtpPagaFinal";
            this.dtpPagaFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpPagaFinal.TabIndex = 18;
            this.dtpPagaFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.Blue;
            this.labelX7.Location = new System.Drawing.Point(260, 164);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(75, 23);
            this.labelX7.TabIndex = 19;
            this.labelX7.Text = "Data Final:";
            // 
            // label
            // 
            this.label.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.label.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.ForeColor = System.Drawing.Color.Blue;
            this.label.Location = new System.Drawing.Point(42, 164);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(75, 23);
            this.label.TabIndex = 17;
            this.label.Text = "Data Inicial:";
            // 
            // rbPagas
            // 
            this.rbPagas.AutoSize = true;
            this.rbPagas.BackColor = System.Drawing.Color.Transparent;
            this.rbPagas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPagas.Location = new System.Drawing.Point(14, 130);
            this.rbPagas.Name = "rbPagas";
            this.rbPagas.Size = new System.Drawing.Size(101, 19);
            this.rbPagas.TabIndex = 15;
            this.rbPagas.Text = "Contas Pagas";
            this.rbPagas.UseVisualStyleBackColor = false;
            this.rbPagas.Click += new System.EventHandler(this.rbPagas_Click);
            // 
            // dtpGeralFinal
            // 
            this.dtpGeralFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralFinal.Location = new System.Drawing.Point(326, 32);
            this.dtpGeralFinal.Name = "dtpGeralFinal";
            this.dtpGeralFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralFinal.TabIndex = 3;
            this.dtpGeralFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralInicial
            // 
            this.dtpGeralInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralInicial.Location = new System.Drawing.Point(113, 32);
            this.dtpGeralInicial.Name = "dtpGeralInicial";
            this.dtpGeralInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralInicial.TabIndex = 1;
            this.dtpGeralInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpEmAbertoInicial
            // 
            this.dtpEmAbertoInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEmAbertoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEmAbertoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEmAbertoInicial.Location = new System.Drawing.Point(113, 94);
            this.dtpEmAbertoInicial.Name = "dtpEmAbertoInicial";
            this.dtpEmAbertoInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpEmAbertoInicial.TabIndex = 8;
            this.dtpEmAbertoInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpEmAbertoFinal
            // 
            this.dtpEmAbertoFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEmAbertoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEmAbertoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEmAbertoFinal.Location = new System.Drawing.Point(326, 94);
            this.dtpEmAbertoFinal.Name = "dtpEmAbertoFinal";
            this.dtpEmAbertoFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpEmAbertoFinal.TabIndex = 10;
            this.dtpEmAbertoFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Blue;
            this.labelX3.Location = new System.Drawing.Point(260, 93);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 11;
            this.labelX3.Text = "Data Final:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Blue;
            this.labelX4.Location = new System.Drawing.Point(42, 93);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 9;
            this.labelX4.Text = "Data Inicial:";
            // 
            // rbEmAberto
            // 
            this.rbEmAberto.AutoSize = true;
            this.rbEmAberto.BackColor = System.Drawing.Color.Transparent;
            this.rbEmAberto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEmAberto.Location = new System.Drawing.Point(14, 68);
            this.rbEmAberto.Name = "rbEmAberto";
            this.rbEmAberto.Size = new System.Drawing.Size(123, 19);
            this.rbEmAberto.TabIndex = 7;
            this.rbEmAberto.Text = "Contas Em Aberto";
            this.rbEmAberto.UseVisualStyleBackColor = false;
            this.rbEmAberto.Click += new System.EventHandler(this.rbEmAberto_Click);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Blue;
            this.labelX2.Location = new System.Drawing.Point(260, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "Data Final:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Blue;
            this.labelX1.Location = new System.Drawing.Point(42, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Data Inicial:";
            // 
            // rbGeral
            // 
            this.rbGeral.AutoSize = true;
            this.rbGeral.BackColor = System.Drawing.Color.Transparent;
            this.rbGeral.Checked = true;
            this.rbGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGeral.Location = new System.Drawing.Point(14, 3);
            this.rbGeral.Name = "rbGeral";
            this.rbGeral.Size = new System.Drawing.Size(55, 19);
            this.rbGeral.TabIndex = 0;
            this.rbGeral.TabStop = true;
            this.rbGeral.Text = "Geral";
            this.rbGeral.UseVisualStyleBackColor = false;
            this.rbGeral.Click += new System.EventHandler(this.rbGeral_Click);
            // 
            // frmRltContasPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 326);
            this.Controls.Add(this.btnCarregaRelatorio);
            this.Controls.Add(this.gpEntradaSaida);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltContasPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Contas";
            this.gpEntradaSaida.ResumeLayout(false);
            this.gpEntradaSaida.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCarregaRelatorio;
        private DevComponents.DotNetBar.Controls.GroupPanel gpEntradaSaida;
        private System.Windows.Forms.DateTimePicker dtpGeralFinal;
        private System.Windows.Forms.DateTimePicker dtpGeralInicial;
        private System.Windows.Forms.DateTimePicker dtpEmAbertoInicial;
        private System.Windows.Forms.DateTimePicker dtpEmAbertoFinal;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.RadioButton rbEmAberto;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.RadioButton rbGeral;
        private System.Windows.Forms.RadioButton rbPagas;
        private System.Windows.Forms.DateTimePicker dtpPagaInicial;
        private System.Windows.Forms.DateTimePicker dtpPagaFinal;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX label;
        private DevComponents.DotNetBar.Controls.Line line2;
        private DevComponents.DotNetBar.Controls.Line line1;
        private DevComponents.DotNetBar.Controls.Line line3;
        private System.Windows.Forms.DateTimePicker dtpVencimento;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.RadioButton rbVencimento;

    }
}