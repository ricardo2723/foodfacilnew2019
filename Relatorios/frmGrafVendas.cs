﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using ObjetoTransferencia;

namespace Relatorios
{
    public partial class frmGrafVendas : Form
    {
        ReportDataSource reportDataSource = new ReportDataSource();
        GraficosVendas tipo;
        DateTime inicial;
        DateTime final;

        public frmGrafVendas(GraficosVendas getTipo, DateTime start, DateTime end)
        {
            InitializeComponent();

            tipo = getTipo;
            inicial = start;
            final = end;
        }
        
        private void frmGrafVendas_Load(object sender, EventArgs e)
        {
            switch (tipo)
            {
                case GraficosVendas.Diaria:
                    vendasDiarias();
                    break;
                case GraficosVendas.Mensal:
                    vendasMensal();
                    break;
                case GraficosVendas.InLocDelivery:
                    locDelivery();
                    break;
                case GraficosVendas.Canceladas:
                    canceladas();
                    break; 
            }
        }

        private void vendasDiarias()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());
            
            reportDataSource.Name = "Vendas";
            reportDataSource.Value = this.GraficoVendasBindingSource;
            rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.grafVendasDiarias.rdlc";
            rptvVendas.LocalReport.SetParameters(p);
            this.GraficoVendasTableAdapter.Diarias(this.dsRelatorios.GraficoVendas, inicial, final);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }

        private void vendasMensal()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "VendasMensal";
            reportDataSource.Value = this.GraficoVendasMensalBindingSource;
            rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.grafVendasMensal.rdlc";
            rptvVendas.LocalReport.SetParameters(p);
            this.GraficoVendasMensalTableAdapter.Fill(this.dsRelatorios.GraficoVendasMensal, inicial, final);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }

        private void locDelivery()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "VendasInlocDelivery";
            reportDataSource.Value = this.GraficoInlocDeliveryBindingSource;
            rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.grafInlocDelivery.rdlc";
            rptvVendas.LocalReport.SetParameters(p);
            this.GraficoInlocDeliveryTableAdapter.Fill(this.dsRelatorios.GraficoInlocDelivery, inicial, final);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }

        private void canceladas()
        {
            rptvVendas.LocalReport.EnableExternalImages = true;

            ReportParameter[] p = new ReportParameter[2];
            p[0] = new ReportParameter("dataInicial", inicial.ToShortDateString());
            p[1] = new ReportParameter("dataFinal", final.ToShortDateString());

            reportDataSource.Name = "VendasCanceladas";
            reportDataSource.Value = this.GraficoVendasCanceladasBindingSource;
            rptvVendas.LocalReport.DataSources.Add(reportDataSource);
            rptvVendas.LocalReport.ReportEmbeddedResource = "Relatorios.grafVendasCanceladas.rdlc";
            rptvVendas.LocalReport.SetParameters(p);
            this.GraficoVendasCanceladasTableAdapter.Fill(this.dsRelatorios.GraficoVendasCanceladas, inicial, final);
            rptvVendas.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);

            this.rptvVendas.RefreshReport();
        }        
    }
}
