﻿namespace Relatorios
{
    partial class frmRltProdutosEntradaSaida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRltProdutosEntradaSaida));
            this.rbGeral = new System.Windows.Forms.RadioButton();
            this.gpEntradaSaida = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dtpGeralFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpGeralInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpCodigoInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpCodigoFinal = new System.Windows.Forms.DateTimePicker();
            this.txtCodigoProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.rbProduto = new System.Windows.Forms.RadioButton();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btnCarregaRelatorio = new DevComponents.DotNetBar.ButtonX();
            this.gpEntradaSaida.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbGeral
            // 
            this.rbGeral.AutoSize = true;
            this.rbGeral.BackColor = System.Drawing.Color.Transparent;
            this.rbGeral.Checked = true;
            this.rbGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGeral.Location = new System.Drawing.Point(14, 3);
            this.rbGeral.Name = "rbGeral";
            this.rbGeral.Size = new System.Drawing.Size(55, 19);
            this.rbGeral.TabIndex = 0;
            this.rbGeral.TabStop = true;
            this.rbGeral.Text = "Geral";
            this.rbGeral.UseVisualStyleBackColor = false;
            this.rbGeral.Click += new System.EventHandler(this.rbGeral_Click);
            // 
            // gpEntradaSaida
            // 
            this.gpEntradaSaida.BackColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpEntradaSaida.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpEntradaSaida.Controls.Add(this.dtpGeralFinal);
            this.gpEntradaSaida.Controls.Add(this.dtpGeralInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpCodigoInicial);
            this.gpEntradaSaida.Controls.Add(this.dtpCodigoFinal);
            this.gpEntradaSaida.Controls.Add(this.txtCodigoProduto);
            this.gpEntradaSaida.Controls.Add(this.labelX5);
            this.gpEntradaSaida.Controls.Add(this.labelX3);
            this.gpEntradaSaida.Controls.Add(this.labelX4);
            this.gpEntradaSaida.Controls.Add(this.rbProduto);
            this.gpEntradaSaida.Controls.Add(this.labelX2);
            this.gpEntradaSaida.Controls.Add(this.labelX1);
            this.gpEntradaSaida.Controls.Add(this.rbGeral);
            this.gpEntradaSaida.Location = new System.Drawing.Point(12, 16);
            this.gpEntradaSaida.Name = "gpEntradaSaida";
            this.gpEntradaSaida.Size = new System.Drawing.Size(522, 182);
            // 
            // 
            // 
            this.gpEntradaSaida.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpEntradaSaida.Style.BackColorGradientAngle = 90;
            this.gpEntradaSaida.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpEntradaSaida.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderBottomWidth = 1;
            this.gpEntradaSaida.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpEntradaSaida.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderLeftWidth = 1;
            this.gpEntradaSaida.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderRightWidth = 1;
            this.gpEntradaSaida.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpEntradaSaida.Style.BorderTopWidth = 1;
            this.gpEntradaSaida.Style.CornerDiameter = 4;
            this.gpEntradaSaida.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpEntradaSaida.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpEntradaSaida.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpEntradaSaida.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpEntradaSaida.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpEntradaSaida.TabIndex = 1;
            // 
            // dtpGeralFinal
            // 
            this.dtpGeralFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralFinal.Location = new System.Drawing.Point(326, 32);
            this.dtpGeralFinal.Name = "dtpGeralFinal";
            this.dtpGeralFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralFinal.TabIndex = 3;
            this.dtpGeralFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpGeralInicial
            // 
            this.dtpGeralInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpGeralInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGeralInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGeralInicial.Location = new System.Drawing.Point(113, 32);
            this.dtpGeralInicial.Name = "dtpGeralInicial";
            this.dtpGeralInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpGeralInicial.TabIndex = 1;
            this.dtpGeralInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpCodigoInicial
            // 
            this.dtpCodigoInicial.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpCodigoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCodigoInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCodigoInicial.Location = new System.Drawing.Point(153, 137);
            this.dtpCodigoInicial.Name = "dtpCodigoInicial";
            this.dtpCodigoInicial.Size = new System.Drawing.Size(110, 21);
            this.dtpCodigoInicial.TabIndex = 8;
            this.dtpCodigoInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // dtpCodigoFinal
            // 
            this.dtpCodigoFinal.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpCodigoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCodigoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCodigoFinal.Location = new System.Drawing.Point(365, 137);
            this.dtpCodigoFinal.Name = "dtpCodigoFinal";
            this.dtpCodigoFinal.Size = new System.Drawing.Size(110, 21);
            this.dtpCodigoFinal.TabIndex = 10;
            this.dtpCodigoFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // txtCodigoProduto
            // 
            this.txtCodigoProduto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtCodigoProduto.Border.Class = "TextBoxBorder";
            this.txtCodigoProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigoProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoProduto.ForeColor = System.Drawing.Color.Black;
            this.txtCodigoProduto.Location = new System.Drawing.Point(156, 95);
            this.txtCodigoProduto.Name = "txtCodigoProduto";
            this.txtCodigoProduto.PreventEnterBeep = true;
            this.txtCodigoProduto.Size = new System.Drawing.Size(149, 21);
            this.txtCodigoProduto.TabIndex = 12;
            this.txtCodigoProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpGeralInicial_KeyDown);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Blue;
            this.labelX5.Location = new System.Drawing.Point(42, 94);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(120, 23);
            this.labelX5.TabIndex = 13;
            this.labelX5.Text = "Código do Produto:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Blue;
            this.labelX3.Location = new System.Drawing.Point(299, 136);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 11;
            this.labelX3.Text = "Data Final:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Blue;
            this.labelX4.Location = new System.Drawing.Point(81, 136);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 9;
            this.labelX4.Text = "Data Inicial:";
            // 
            // rbProduto
            // 
            this.rbProduto.AutoSize = true;
            this.rbProduto.BackColor = System.Drawing.Color.Transparent;
            this.rbProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbProduto.Location = new System.Drawing.Point(14, 67);
            this.rbProduto.Name = "rbProduto";
            this.rbProduto.Size = new System.Drawing.Size(68, 19);
            this.rbProduto.TabIndex = 7;
            this.rbProduto.Text = "Produto";
            this.rbProduto.UseVisualStyleBackColor = false;
            this.rbProduto.Click += new System.EventHandler(this.rbProduto_Click);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Blue;
            this.labelX2.Location = new System.Drawing.Point(260, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "Data Final:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Blue;
            this.labelX1.Location = new System.Drawing.Point(42, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Data Inicial:";
            // 
            // btnCarregaRelatorio
            // 
            this.btnCarregaRelatorio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCarregaRelatorio.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCarregaRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarregaRelatorio.Location = new System.Drawing.Point(421, 205);
            this.btnCarregaRelatorio.Name = "btnCarregaRelatorio";
            this.btnCarregaRelatorio.Size = new System.Drawing.Size(113, 23);
            this.btnCarregaRelatorio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCarregaRelatorio.TabIndex = 6;
            this.btnCarregaRelatorio.Text = "Carregar Relatório";
            this.btnCarregaRelatorio.Click += new System.EventHandler(this.btnCarregaRelatorio_Click);
            // 
            // frmRltProdutosEntradaSaida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 238);
            this.Controls.Add(this.btnCarregaRelatorio);
            this.Controls.Add(this.gpEntradaSaida);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRltProdutosEntradaSaida";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DRSis - Lanche Fácil - Relatórios de Produtos";
            this.Load += new System.EventHandler(this.frmRltProdutosEntradas_Load);
            this.gpEntradaSaida.ResumeLayout(false);
            this.gpEntradaSaida.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rbGeral;
        private DevComponents.DotNetBar.Controls.GroupPanel gpEntradaSaida;
        private System.Windows.Forms.DateTimePicker dtpGeralInicial;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.DateTimePicker dtpGeralFinal;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnCarregaRelatorio;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigoProduto;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.DateTimePicker dtpCodigoFinal;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.DateTimePicker dtpCodigoInicial;
        private System.Windows.Forms.RadioButton rbProduto;
    }
}