/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */ 
USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoAbreVenda]    Script Date: 03/03/2019 16:25:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspVendaCabecalhoAbreVenda]
	@atendenteVendaCabecalho varchar(50),
	@idAtendimentoTipo	int,
	@statusVendaCabecalho	int,
	@usuarioId int				
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				INSERT INTO VendasCabecalho
					(
						idAtendimentoTipo,
						statusVendaCabecalho,
						dataVendaCabecalho,
						usuarioId,
						atendenteVendaCabecalho
				
					)
					VALUES
					(
						@idAtendimentoTipo,
						@statusVendaCabecalho,
						GETDATE(),
						@usuarioId,
						@atendenteVendaCabecalho
					)
				
					select @@IDENTITY as Retorno;
				
										
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */ 

/* ADICIONAR RELACIONAMENTO ENTRE A TABELA VENDACABECALHO E A TABELA ATENDIMENTOTIPO*/
/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendasCabecalho
	DROP CONSTRAINT FK_VendasCabecalho_VendasCabecalho_TipoAtendimento
GO
ALTER TABLE dbo.AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendasCabecalho ADD CONSTRAINT
	FK_VendasCabecalho_AtendimentoTipo FOREIGN KEY
	(
	idAtendimentoTipo
	) REFERENCES dbo.AtendimentoTipo
	(
	idAtendimento
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.VendasCabecalho SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */ 

/* CRIA��O DA VIEW */
USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaPedidosAbertos]    Script Date: 03/03/2019 16:32:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_BuscaPedidosAbertos]
as
select a.nomeAtendimento, v.* from VendasCabecalho v left join AtendimentoTipo a on v.idAtendimentoTipo = a.idAtendimento where v.statusVendaCabecalho = 1


GO

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */ 
/* ALTERA��O DA TABELA VENDACABECALHO NO CAMPO IDATENDIMENTOTIPO */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.VendasCabecalho.idAtendimentoTipoo', N'Tmp_idAtendimentoTipo_5', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.VendasCabecalho.Tmp_idAtendimentoTipo_5', N'idAtendimentoTipo', 'COLUMN' 
GO
ALTER TABLE dbo.VendasCabecalho SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usuarios SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AtendimentoTipo ADD
	usuarioId int NULL,
	dataCadastro datetime NULL
GO
ALTER TABLE dbo.AtendimentoTipo ADD CONSTRAINT
	FK_AtendimentoTipo_Usuarios FOREIGN KEY
	(
	usuarioId
	) REFERENCES dbo.Usuarios
	(
	usuarioId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspBairroCrudInserir]    Script Date: 03/03/2019 22:44:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspAtendimentoCrudInserir]
	@atendimento	varchar(45),
	@nomeAtendimento	varchar(45),
	@buton varchar(45),
	@usuarioId	int		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT idAtendimento FROM AtendimentoTipo WHERE nomeAtendimento = @nomeAtendimento or atendimento = @atendimento))
				RAISERROR('O Tipo de Atendimento j� est� cadastrado na Base de Dados.',14,1);
						
			INSERT INTO AtendimentoTipo
			(
				atendimento,
				nomeAtendimento,
				buton,
				usuarioId,
				dataCadastro
			)
			VALUES
			(
				@atendimento,
				@nomeAtendimento,
				@buton,
				@usuarioId,
				GETDATE()	
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

GO


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspBairroCrudAlterar]    Script Date: 03/03/2019 23:08:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspAtendimentoTipoCrudAlterar]
	@idAtendimento int,
	@atendimento	varchar(45),
	@nomeAtendimento	varchar(45),
	@buton varchar(45),
	@usuarioId	int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			
			IF(EXISTS(SELECT idAtendimento FROM AtendimentoTipo WHERE atendimento = @atendimento and nomeAtendimento = @nomeAtendimento and idAtendimento != @idAtendimento))
				RAISERROR('O Tipo de Atendimento j� est� Cadastrado no Bando de Dados.',14,1);

			UPDATE 
				AtendimentoTipo
			SET		
				atendimento = @atendimento,
				nomeAtendimento = @nomeAtendimento,
				buton = @buton,
				usuarioId = @usuarioId,
				dataCadastro = GETDATE()
						
			WHERE
				idAtendimento = @idAtendimento
			
			SELECT @idAtendimento AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspBairroCrudExcluir]    Script Date: 03/03/2019 23:15:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspAtendimentoTipoCrudExcluir]
	@idAtendimento int
AS
BEGIN
	
	DELETE FROM
		AtendimentoTipo
	WHERE
		idAtendimento = @idAtendimento
	
	SELECT @idAtendimento AS retorno
END

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.AtendimentoTipo.atendimento', N'Tmp_atendimentoSigla', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.AtendimentoTipo.Tmp_atendimentoSigla', N'atendimentoSigla', 'COLUMN' 
GO
ALTER TABLE dbo.AtendimentoTipo
	DROP COLUMN buton
GO
ALTER TABLE dbo.AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendasCabecalho
	DROP CONSTRAINT FK_VendasCabecalho_AtendimentoTipo
GO
ALTER TABLE dbo.AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_VendasCabecalho
	(
	idVendaCabecalho int NOT NULL IDENTITY (1, 1),
	idCliente int NULL,
	idCartao int NULL,
	AtendimentoTipo varchar(50) NULL,
	dataVendaCabecalho date NULL,
	horaVendaCabecalho time(7) NULL,
	valorVendaCabecalho decimal(18, 2) NULL,
	descontoVendaCabecalho decimal(18, 2) NULL,
	totalVendaCabecalho decimal(18, 2) NULL,
	statusVendaCabecalho int NULL,
	usuarioId int NULL,
	atendenteVendaCabecalho varchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_VendasCabecalho SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_VendasCabecalho ON
GO
IF EXISTS(SELECT * FROM dbo.VendasCabecalho)
	 EXEC('INSERT INTO dbo.Tmp_VendasCabecalho (idVendaCabecalho, idCliente, idCartao, AtendimentoTipo, dataVendaCabecalho, horaVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, statusVendaCabecalho, usuarioId, atendenteVendaCabecalho)
		SELECT idVendaCabecalho, idCliente, idCartao, CONVERT(varchar(50), idAtendimentoTipo), dataVendaCabecalho, horaVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, statusVendaCabecalho, usuarioId, atendenteVendaCabecalho FROM dbo.VendasCabecalho WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_VendasCabecalho OFF
GO
ALTER TABLE dbo.VendaDetalhes
	DROP CONSTRAINT FK_VendaDetalhes_VendasCabecalho
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos
	DROP CONSTRAINT FK_VendaCabecalhoTipoPagamentos_VendasCabecalho
GO
ALTER TABLE dbo.TempoVendas
	DROP CONSTRAINT FK_TempoVendas_VendasCabecalho
GO
ALTER TABLE dbo.Entregas
	DROP CONSTRAINT FK_Entregas_VendasCabecalho
GO
ALTER TABLE dbo.EntregaCancelada
	DROP CONSTRAINT FK_EntregaCancelada_VendasCabecalho
GO
DROP TABLE dbo.VendasCabecalho
GO
EXECUTE sp_rename N'dbo.Tmp_VendasCabecalho', N'VendasCabecalho', 'OBJECT' 
GO
ALTER TABLE dbo.VendasCabecalho ADD CONSTRAINT
	PK_VendasCabecalho PRIMARY KEY CLUSTERED 
	(
	idVendaCabecalho
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EntregaCancelada ADD CONSTRAINT
	FK_EntregaCancelada_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalho
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EntregaCancelada SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Entregas ADD CONSTRAINT
	FK_Entregas_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalho
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Entregas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TempoVendas ADD CONSTRAINT
	FK_TempoVendas_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoTempo
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TempoVendas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos ADD CONSTRAINT
	FK_VendaCabecalhoTipoPagamentos_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoTipoPagamento
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendaDetalhes ADD CONSTRAINT
	FK_VendaDetalhes_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoDetalhes
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.VendaDetalhes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

delete from AtendimentoTipo where idAtendimento > 0

insert into AtendimentoTipo (AtendimentoSigla, nomeAtendimento, usuarioId, dataCadastro) values ('M', 'MESA', 1, GETDATE())
insert into AtendimentoTipo (AtendimentoSigla, nomeAtendimento, usuarioId, dataCadastro) values ('B', 'BALC�O', 1, GETDATE())
insert into AtendimentoTipo (AtendimentoSigla, nomeAtendimento, usuarioId, dataCadastro) values ('C', 'CARRO', 1, GETDATE())

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspAtendimentoCrudInserir]    Script Date: 04/03/2019 19:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspAtendimentoCrudInserir]
	@atendimentoSigla	varchar(4),
	@nomeAtendimento	varchar(45),
	@usuarioId	int		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT idAtendimento FROM AtendimentoTipo WHERE nomeAtendimento = @nomeAtendimento or atendimentoSigla = @atendimentoSigla))
				RAISERROR('O Tipo de Atendimento j� est� cadastrado na Base de Dados.',14,1);
						
			INSERT INTO AtendimentoTipo
			(
				atendimentoSigla,
				nomeAtendimento,
				usuarioId,
				dataCadastro
			)
			VALUES
			(
				@atendimentoSigla,
				@nomeAtendimento,
				@usuarioId,
				GETDATE()	
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspAtendimentoTipoCrudAlterar]    Script Date: 04/03/2019 19:19:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspAtendimentoTipoCrudAlterar]
	@idAtendimento int,
	@atendimentoSigla	varchar(4),
	@nomeAtendimento	varchar(45),
	@usuarioId	int	
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN				
			
			IF(EXISTS(SELECT idAtendimento FROM AtendimentoTipo WHERE atendimentoSigla = @atendimentoSigla and nomeAtendimento = @nomeAtendimento and idAtendimento != @idAtendimento))
				RAISERROR('O Tipo de Atendimento j� est� Cadastrado no Bando de Dados.',14,1);

			UPDATE 
				AtendimentoTipo
			SET		
				atendimentoSigla = @atendimentoSigla,
				nomeAtendimento = @nomeAtendimento,
				usuarioId = @usuarioId,
				dataCadastro = GETDATE()
						
			WHERE
				idAtendimento = @idAtendimento
			
			SELECT @idAtendimento AS retorno
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AtendimentoTipo
	DROP CONSTRAINT FK_AtendimentoTipo_Usuarios
GO
ALTER TABLE dbo.Usuarios SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AtendimentoTipo
	(
	idAtendimento int NOT NULL IDENTITY (1, 1),
	atendimentoSigla varchar(4) NULL,
	nomeAtendimento varchar(10) NULL,
	usuarioId int NULL,
	dataCadastro datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_AtendimentoTipo ON
GO
IF EXISTS(SELECT * FROM dbo.AtendimentoTipo)
	 EXEC('INSERT INTO dbo.Tmp_AtendimentoTipo (idAtendimento, atendimentoSigla, nomeAtendimento, usuarioId, dataCadastro)
		SELECT idAtendimento, atendimentoSigla, nomeAtendimento, usuarioId, dataCadastro FROM dbo.AtendimentoTipo WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_AtendimentoTipo OFF
GO
DROP TABLE dbo.AtendimentoTipo
GO
EXECUTE sp_rename N'dbo.Tmp_AtendimentoTipo', N'AtendimentoTipo', 'OBJECT' 
GO
ALTER TABLE dbo.AtendimentoTipo ADD CONSTRAINT
	PK_AtendimentoTipo PRIMARY KEY CLUSTERED 
	(
	idAtendimento
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.AtendimentoTipo ADD CONSTRAINT
	FK_AtendimentoTipo_Usuarios FOREIGN KEY
	(
	usuarioId
	) REFERENCES dbo.Usuarios
	(
	usuarioId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoAbreVenda]    Script Date: 04/03/2019 19:42:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspVendaCabecalhoAbreVenda]
	@atendenteVendaCabecalho varchar(50),
	@atendimentoTipo	varchar(4),
	@statusVendaCabecalho	int,
	@usuarioId int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				INSERT INTO VendasCabecalho
					(
						atendimentoTipo,
						statusVendaCabecalho,
						dataVendaCabecalho,
						usuarioId,
						atendenteVendaCabecalho
				
					)
					VALUES
					(
						@atendimentoTipo,
						@statusVendaCabecalho,
						GETDATE(),
						@usuarioId,
						@atendenteVendaCabecalho
					)
				
					select @@IDENTITY as Retorno;
				
										
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaPedidosAbertos]    Script Date: 04/03/2019 20:08:22 ******/
DROP VIEW [dbo].[vw_BuscaPedidosAbertos]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_VendasCabecalho
	(
	idVendaCabecalho int NOT NULL IDENTITY (1, 1),
	idCliente int NULL,
	idCartao int NULL,
	idAtendimentoTipo int NULL,
	numeroAtendimento int NULL,
	dataVendaCabecalho date NULL,
	horaVendaCabecalho time(7) NULL,
	valorVendaCabecalho decimal(18, 2) NULL,
	descontoVendaCabecalho decimal(18, 2) NULL,
	totalVendaCabecalho decimal(18, 2) NULL,
	statusVendaCabecalho int NULL,
	usuarioId int NULL,
	atendenteVendaCabecalho varchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_VendasCabecalho SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_VendasCabecalho ON
GO
IF EXISTS(SELECT * FROM dbo.VendasCabecalho)
	 EXEC('INSERT INTO dbo.Tmp_VendasCabecalho (idVendaCabecalho, idCliente, idCartao, idAtendimentoTipo, dataVendaCabecalho, horaVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, statusVendaCabecalho, usuarioId, atendenteVendaCabecalho)
		SELECT idVendaCabecalho, idCliente, idCartao, CONVERT(int, AtendimentoTipo), dataVendaCabecalho, horaVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, statusVendaCabecalho, usuarioId, atendenteVendaCabecalho FROM dbo.VendasCabecalho WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_VendasCabecalho OFF
GO
ALTER TABLE dbo.EntregaCancelada
	DROP CONSTRAINT FK_EntregaCancelada_VendasCabecalho
GO
ALTER TABLE dbo.Entregas
	DROP CONSTRAINT FK_Entregas_VendasCabecalho
GO
ALTER TABLE dbo.TempoVendas
	DROP CONSTRAINT FK_TempoVendas_VendasCabecalho
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos
	DROP CONSTRAINT FK_VendaCabecalhoTipoPagamentos_VendasCabecalho
GO
ALTER TABLE dbo.VendaDetalhes
	DROP CONSTRAINT FK_VendaDetalhes_VendasCabecalho
GO
DROP TABLE dbo.VendasCabecalho
GO
EXECUTE sp_rename N'dbo.Tmp_VendasCabecalho', N'VendasCabecalho', 'OBJECT' 
GO
ALTER TABLE dbo.VendasCabecalho ADD CONSTRAINT
	PK_VendasCabecalho PRIMARY KEY CLUSTERED 
	(
	idVendaCabecalho
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.VendasCabecalho ADD CONSTRAINT
	FK_VendasCabecalho_AtendimentoTipo FOREIGN KEY
	(
	idAtendimentoTipo
	) REFERENCES dbo.AtendimentoTipo
	(
	idAtendimento
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendaDetalhes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos ADD CONSTRAINT
	FK_VendaCabecalhoTipoPagamentos_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoTipoPagamento
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TempoVendas ADD CONSTRAINT
	FK_TempoVendas_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoTempo
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TempoVendas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Entregas ADD CONSTRAINT
	FK_Entregas_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalho
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Entregas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EntregaCancelada ADD CONSTRAINT
	FK_EntregaCancelada_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalho
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EntregaCancelada SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoAbreVenda]    Script Date: 04/03/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspVendaCabecalhoAbreVenda]
	@atendenteVendaCabecalho varchar(50),
	@idAtendimentoTipo	int,
	@numeroAtendimento	int,
	@statusVendaCabecalho	int,
	@usuarioId int
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
				INSERT INTO VendasCabecalho
					(
						idAtendimentoTipo,
						numeroAtendimento,
						statusVendaCabecalho,
						dataVendaCabecalho,
						horaVendaCabecalho,
						usuarioId,
						atendenteVendaCabecalho
				
					)
					VALUES
					(
						@idAtendimentoTipo,
						@numeroAtendimento,
						@statusVendaCabecalho,
						GETDATE(),
						CONVERT(CHAR(8),GETDATE(),108),
						@usuarioId,
						@atendenteVendaCabecalho
					)
				
					select @@IDENTITY as Retorno;
				
										
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* CRIA��O DA VIEW */
USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaPedidosAbertos]    Script Date: 03/03/2019 16:32:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_BuscaPedidosAbertos]
as
select a.nomeAtendimento, v.* from VendasCabecalho v left join AtendimentoTipo a on v.idAtendimentoTipo = a.idAtendimento where v.statusVendaCabecalho = 1


GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspUsuarioListaNome]    Script Date: 06/03/2019 20:18:30 ******/
DROP PROCEDURE [dbo].[uspUsuarioListaNome]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaUsuariosNome]    Script Date: 06/03/2019 20:27:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_BuscaUsuariosNome]
as

SELECT u.*, n.nivelAcessoDescricao FROM Usuarios u left join NivelAcesso n on u.nivelAcessoId = n.nivelAcessoId

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaPedidosAbertos]    Script Date: 07/03/2019 18:59:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_VendaDetalhesListaItens]
as
SELECT 
		idVendaDetalhes,
		idVendaCabecalhoDetalhes,
		p.codigoProduto,
		p.descricaoProduto,
		especialVendaDetalhes,
		extraVendaDetalhes,
		quantidadeVendaDetalhes,
		valorUnitarioVendaDetalhes,
		valorTotalVendaDetalhes,
		dataEnvioVendaDetalhes,
		statusVendaDetalhes,
		atendenteVendaCabecalho
	FROM 
		VendaDetalhes vd
	LEFT join
		Produto p
	on
		p.codigoProduto = vd.codigoProduto	
	LEFT join
		VendasCabecalho vc
	on
		vc.idVendaCabecalho = vd.idVendaCabecalhoDetalhes

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspVendaDetalhesListaItens]    Script Date: 07/03/2019 19:05:51 ******/
DROP PROCEDURE [dbo].[uspVendaDetalhesListaItens]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Produto ADD
	tipoProduto char(1) NULL
GO
ALTER TABLE dbo.Produto SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

update Produto set tipoProduto = 'E' where grupoId = 5
update Produto set tipoProduto = 'P' where grupoId != 5

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_VendaDetalhesListaItens]    Script Date: 07/03/2019 20:28:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_VendaCabecalhoProdutosListaCod]
as
			SELECT
					codigoProduto, 
					descricaoProduto,
					valorVendaProduto,
					controlaEstoqueProduto,
					estoqueProduto,
					estoqueCriticoProduto,
					manufaturadoProduto,
					grupoDescricao,
					tipoProduto
				FROM 
					Produto p
				left JOIN
					Grupo g
				ON
					p.grupoId = g.grupoId


GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspVendaCabecalhoProdutosListaCod]    Script Date: 07/03/2019 20:30:41 ******/
DROP PROCEDURE [dbo].[uspVendaCabecalhoProdutosListaCod]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_VendaCabecalhoProdutosListaCod]    Script Date: 08/03/2019 18:30:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_ProdutosManufaturadoListaCod]
as
	SELECT 
			codProduto,
			codProdutoManufaturado,
			quantidadeProdutoManufaturado,
			descricaoProduto,
			valorCompraProduto,
			valorVendaProduto,
			estoqueProduto
			
		FROM 
			ProdutoManufaturado pm
		left join
		Produto p
		on pm.codProdutoManufaturado = p.codigoProduto

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspProdutosManufaturadoListaCod]    Script Date: 08/03/2019 18:33:02 ******/
DROP PROCEDURE [dbo].[uspProdutosManufaturadoListaCod]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendasCabecalho
	DROP CONSTRAINT FK_VendasCabecalho_AtendimentoTipo
GO
ALTER TABLE dbo.AtendimentoTipo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_VendasCabecalho
	(
	idVendaCabecalho int NOT NULL IDENTITY (1, 1),
	idCliente int NULL,
	idCartao int NULL,
	idAtendimentoTipo int NULL,
	numeroAtendimento int NULL,
	numeroPessoas int NULL,
	dataVendaCabecalho date NULL,
	horaVendaCabecalho time(7) NULL,
	valorVendaCabecalho decimal(18, 2) NULL,
	descontoVendaCabecalho decimal(18, 2) NULL,
	totalVendaCabecalho decimal(18, 2) NULL,
	statusVendaCabecalho int NULL,
	usuarioId int NULL,
	atendenteVendaCabecalho varchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_VendasCabecalho SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_VendasCabecalho ON
GO
IF EXISTS(SELECT * FROM dbo.VendasCabecalho)
	 EXEC('INSERT INTO dbo.Tmp_VendasCabecalho (idVendaCabecalho, idCliente, idCartao, idAtendimentoTipo, numeroAtendimento, dataVendaCabecalho, horaVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, statusVendaCabecalho, usuarioId, atendenteVendaCabecalho)
		SELECT idVendaCabecalho, idCliente, idCartao, idAtendimentoTipo, numeroAtendimento, dataVendaCabecalho, horaVendaCabecalho, valorVendaCabecalho, descontoVendaCabecalho, totalVendaCabecalho, statusVendaCabecalho, usuarioId, atendenteVendaCabecalho FROM dbo.VendasCabecalho WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_VendasCabecalho OFF
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos
	DROP CONSTRAINT FK_VendaCabecalhoTipoPagamentos_VendasCabecalho
GO
ALTER TABLE dbo.TempoVendas
	DROP CONSTRAINT FK_TempoVendas_VendasCabecalho
GO
ALTER TABLE dbo.Entregas
	DROP CONSTRAINT FK_Entregas_VendasCabecalho
GO
ALTER TABLE dbo.EntregaCancelada
	DROP CONSTRAINT FK_EntregaCancelada_VendasCabecalho
GO
DROP TABLE dbo.VendasCabecalho
GO
EXECUTE sp_rename N'dbo.Tmp_VendasCabecalho', N'VendasCabecalho', 'OBJECT' 
GO
ALTER TABLE dbo.VendasCabecalho ADD CONSTRAINT
	PK_VendasCabecalho PRIMARY KEY CLUSTERED 
	(
	idVendaCabecalho
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.VendasCabecalho ADD CONSTRAINT
	FK_VendasCabecalho_AtendimentoTipo FOREIGN KEY
	(
	idAtendimentoTipo
	) REFERENCES dbo.AtendimentoTipo
	(
	idAtendimento
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EntregaCancelada ADD CONSTRAINT
	FK_EntregaCancelada_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalho
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EntregaCancelada SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Entregas ADD CONSTRAINT
	FK_Entregas_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalho
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Entregas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TempoVendas ADD CONSTRAINT
	FK_TempoVendas_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoTempo
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TempoVendas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos ADD CONSTRAINT
	FK_VendaCabecalhoTipoPagamentos_VendasCabecalho FOREIGN KEY
	(
	idVendaCabecalhoTipoPagamento
	) REFERENCES dbo.VendasCabecalho
	(
	idVendaCabecalho
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.VendaCabecalhoTipoPagamentos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

update VendasCabecalhos set numeroPessoas = 1

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaPedidosAbertos]    Script Date: 16/03/2019 18:08:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_EspeciaisListaNome]
as
SELECT 
		*		
	FROM 
		Especiais 
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspEspeciaisListaNome]    Script Date: 16/03/2019 18:13:07 ******/
DROP PROCEDURE [dbo].[uspEspeciaisListaNome]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* Para impedir poss�veis problemas de perda de dados, analise este script detalhadamente antes de execut�-lo fora do contexto do designer de banco de dados.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Grupo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usuarios SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Especiais
	(
	especiaisId int NOT NULL IDENTITY (1, 1),
	idGrupo int NULL,
	especiaisDescricao varchar(50) NOT NULL,
	especiaisDataCad datetime NOT NULL,
	usuarioId int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Especiais SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Especiais ON
GO
IF EXISTS(SELECT * FROM dbo.Especiais)
	 EXEC('INSERT INTO dbo.Tmp_Especiais (especiaisId, especiaisDescricao, especiaisDataCad, usuarioId)
		SELECT especiaisId, especiaisDescricao, especiaisDataCad, usuarioId FROM dbo.Especiais WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Especiais OFF
GO
DROP TABLE dbo.Especiais
GO
EXECUTE sp_rename N'dbo.Tmp_Especiais', N'Especiais', 'OBJECT' 
GO
ALTER TABLE dbo.Especiais ADD CONSTRAINT
	PK_Especiais PRIMARY KEY CLUSTERED 
	(
	especiaisId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Especiais ADD CONSTRAINT
	FK_Especiais_Usuarios FOREIGN KEY
	(
	usuarioId
	) REFERENCES dbo.Usuarios
	(
	usuarioId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Especiais ADD CONSTRAINT
	FK_Especiais_Grupos FOREIGN KEY
	(
	idGrupo
	) REFERENCES dbo.Grupo
	(
	grupoId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspEspeciaisCrudInserir]    Script Date: 16/03/2019 20:01:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspEspeciaisCrudInserir]
	@especiaisIdGrupo int,
	@especiaisDescricao	varchar(50),
	@especiaisDataCad datetime,
	@usuarioId int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT especiaisId FROM Especiais WHERE especiaisDescricao = @especiaisDescricao))
				RAISERROR('O Especial ja est� cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Especiais
			(
				idGrupo,
				especiaisDescricao,
				especiaisDataCad,
				usuarioId
			)
			VALUES
			(
				@especiaisIdGrupo,
				@especiaisDescricao,
				@especiaisDataCad,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspEspeciaisCrudInserir]    Script Date: 16/03/2019 20:08:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspEspeciaisCrudInserir]
	@especiaisIdGrupo int,
	@especiaisDescricao	varchar(50),
	@especiaisDataCad datetime,
	@usuarioId int
		
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT especiaisId FROM Especiais WHERE especiaisDescricao = @especiaisDescricao AND idGrupo = @especiaisIdGrupo))
				RAISERROR('O Especial ja est� cadastrado no Bando de Dados.',14,1);
						
			INSERT INTO Especiais
			(
				idGrupo,
				especiaisDescricao,
				especiaisDataCad,
				usuarioId
			)
			VALUES
			(
				@especiaisIdGrupo,
				@especiaisDescricao,
				@especiaisDataCad,
				@usuarioId		
			)
			
			SELECT @@IDENTITY AS retorno;
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

UPDATE Especiais SET idGrupo = 1 where especiaisId > 0

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_EspeciaisListaNome]    Script Date: 16/03/2019 20:25:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[vw_EspeciaisListaNome]
as
SELECT 
		e.*, g.grupoDescricao, g.grupoId		
	FROM 
		Especiais e
	left join 
		Grupo g
	on e.idGrupo = g.grupoId

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_BuscaPedidosAbertos]    Script Date: 17/03/2019 16:17:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_GruposLista]
as
SELECT 
		*		
	FROM 
		Grupo 
	WHERE 
		grupoDescricao <> 'Extras'

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  StoredProcedure [dbo].[uspGruposCrudInserir]    Script Date: 17/03/2019 16:20:17 ******/
DROP PROCEDURE [dbo].[uspGruposLista]
GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_VendaCabecalhoProdutosListaCod]    Script Date: 17/03/2019 17:11:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER view [dbo].[vw_VendaCabecalhoProdutosListaCod]
as
			SELECT
					codigoProduto, 
					descricaoProduto,
					valorVendaProduto,
					controlaEstoqueProduto,
					estoqueProduto,
					estoqueCriticoProduto,
					manufaturadoProduto,
					grupoDescricao,
					tipoProduto,
					p.grupoId
				FROM 
					Produto p
				left JOIN
					Grupo g
				ON
					p.grupoId = g.grupoId

GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO

/****** Object:  View [dbo].[vw_VendaDetalhesListaItens]    Script Date: 02/05/2019 21:04:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[vw_VendaDetalhesListaItens]
as
SELECT 
		idVendaDetalhes,
		idVendaCabecalhoDetalhes,
		p.codigoProduto,
		p.descricaoProduto,
		especialVendaDetalhes,
		extraVendaDetalhes,
		quantidadeVendaDetalhes,
		valorUnitarioVendaDetalhes,
		valorTotalVendaDetalhes,
		dataEnvioVendaDetalhes,
		statusVendaDetalhes,
		atendenteVendaCabecalho, 
		tipoProduto
	FROM 
		VendaDetalhes vd
	LEFT join
		Produto p
	on
		p.codigoProduto = vd.codigoProduto	
	LEFT join
		VendasCabecalho vc
	on
		vc.idVendaCabecalho = vd.idVendaCabecalhoDetalhes


GO

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspProdutosCrudInserir]    Script Date: 05/05/2019 21:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspProdutosCrudInserir]
	@codigoProduto	int,
	@undMedidaId	int,
	@idFornecedor	int,
	@grupoId	int,
	@descricaoProduto	varchar(100),
	@valorCompraProduto	decimal(18, 2),
	@valorVendaProduto	decimal(18, 2),
	@estoqueProduto	decimal(18, 2),
	@estoqueCriticoProduto	decimal(18, 2),
	@dataCadastroProduto	datetime,
	@controlaEstoqueProduto	char(1),
	@manufaturadoProduto	char(1),
	@usuarioId	int,
	@tipoProduto char
					
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			IF(EXISTS(SELECT codigoProduto FROM Produto WHERE codigoProduto = @codigoProduto or descricaoProduto = @descricaoProduto))
				RAISERROR('O C�digo ou a Descri��o do Produto ja est� cadastrado no Bando de Dados.',14,1);
			
			INSERT INTO Produto
			(
				codigoProduto,
				undMedidaId,
				idFornecedor,
				grupoId,
				descricaoProduto,
				valorCompraProduto,
				valorVendaProduto,
				estoqueProduto,
				estoqueCriticoProduto,	
				dataCadastroProduto,
				controlaEstoqueProduto,
				manufaturadoProduto,
				usuarioId,
				tipoProduto
			)
			VALUES
			(
				@codigoProduto,
				@undMedidaId,
				@idFornecedor,
				@grupoId,
				@descricaoProduto,
				@valorCompraProduto,
				@valorVendaProduto,
				@estoqueProduto,
				@estoqueCriticoProduto,	
				@dataCadastroProduto,
				@controlaEstoqueProduto,
				@manufaturadoProduto,
				@usuarioId,
				@tipoProduto
			)
			
			SELECT @codigoProduto AS retorno;
			
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END




/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaNome]    Script Date: 06/05/2019 19:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspProdutosListaNome] 
	@descricaoProduto	varchar(100),
	@tipoProduto char(1)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		descricaoProduto LIKE '%' + @descricaoProduto + '%'  and tipoProduto = @tipoProduto ORDER BY codigoProduto ASC
END



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaCodigo]    Script Date: 06/05/2019 20:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspProdutosListaCodigo]
	@codigoProduto	int,
	@tipoProduto char(1)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		codigoProduto = @codigoProduto and tipoProduto = @tipoProduto ORDER BY codigoProduto ASC
END

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaGrupo]    Script Date: 06/05/2019 20:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspProdutosListaGrupo]
	@grupoDescricao	varchar(50),
	@tipoProduto char(1)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		grupoDescricao LIKE '%' + @grupoDescricao + '%' and tipoProduto = @tipoProduto ORDER BY grupoDescricao ASC
END

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaCod]    Script Date: 06/05/2019 20:16:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspProdutosListaCod]
	@codigoProduto	int,
	@tipoProduto char(1)
AS
BEGIN
	
	BEGIN TRY
		BEGIN TRAN
		
			--if((select COUNT(codigoProduto) from Produto where codigoProduto = @codigoProduto and manufaturadoProduto = 'N') > 0)
			--begin
				
				SELECT 
					descricaoProduto,
					valorCompraProduto,
					valorVendaProduto
				FROM 
					Produto 
				WHERE 
					codigoProduto = @codigoProduto and manufaturadoProduto = 'N' and tipoProduto = @tipoProduto
			--end
			
			--else
				--begin
					--RAISERROR('1',14,1);
				--end
			
		COMMIT TRAN
	END TRY
	
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT ERROR_MESSAGE() AS Retorno;
	END CATCH
END

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

USE [lancheFacil]
GO
/****** Object:  StoredProcedure [dbo].[uspProdutosListaNomeExtra]    Script Date: 06/05/2019 20:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspProdutosListaNomeExtra] 
	@descricaoProduto	varchar(100)
AS
BEGIN
	SELECT 
		*		
	FROM 
		Produto
	join
		UndMedida 
	on 
	UndMedida.undMedidaId = Produto.undMedidaId
	
	join
		Grupo 
	on 
	Grupo.grupoId = Produto.grupoId
	
	join
		Fornecedor 
	on 
	Fornecedor.idFornecedor = Produto.idFornecedor
	
	WHERE 
		descricaoProduto LIKE '%' + @descricaoProduto + '%' and tipoProduto = 'E' ORDER BY codigoProduto ASC
END


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
